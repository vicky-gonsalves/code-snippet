import { browser, element, by } from 'protractor';

export class RecentSearches {

  navigateTo() {
    return browser.get('/search/recent');
  }

  getPageHeading() {
    let heading = element(by.tagName('h1'));
    return heading.getText();
  }

  search(searchFor, db) {
    browser.get('/search/basic');
    browser.sleep(1000);
    this.setSearchInputText(searchFor);
    this.selectDatabase(db);
    this.executeSearch();
  }

  executeSearch() {
    let searchBtn = element(by.id('searchForBtn'));
    return searchBtn.click();
  }

  getSearchInput() {
    return element(by.id('searchForInput'));
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  openDatabaseSelectorAccordion() {
    let accordion = element(by.css('.db-open-link'));
    accordion.click();
  }

  getDBCheckBoxes(){
    return element.all(by.css('.dbOptions')).all(by.css('label.checkbox-inline'));
  }

  checkSelectedDB(dbOrder) {
    this.openDatabaseSelectorAccordion();
    return this.getDBCheckBoxes().get(dbOrder).element(by.tagName('input'));
  }

  selectDatabase(db){
    this.openDatabaseSelectorAccordion();
    this.getDBCheckBoxes().get(6).click(); //deselest all
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === db) {
          checkbox.click();
        }
      });
    });
  }

  getSelectedDBs() {
    this.openDatabaseSelectorAccordion();
    return element.all(by.css('input[ng-reflect-model=true]')).count();
  }

  getFriendlyQuery(type) {
    if (type == 'search result') {
      return element(by.css('friendly-query > span'));
    }
    return element.all(by.css('friendly-query > span')).get(0);
  }

  getRecentSearchDBs() {
    return element.all(by.css('span.search-db-list')).getText();
  }

  getSeachTotalRecords() {
    return element(by.css('.recentSearchActionBar>span'));
  }

  getRecentSearchTotalRecords() {
    return element(by.css('span.c-gray'));
  }

  getTitleLink() {
    return element.all(by.css('article a')).get(0);
  }

  getButtonsCount() {
    return element.all(by.css('.ctrl-btn-grp')).get(0).all(by.css('button')).count();
  }

  editRecentSearch() {
    return element.all(by.css('.ctrl-btn-grp')).get(0).all(by.css('.edit')).click();
  }

  saveRecentSearch() {
    return element.all(by.css('.ctrl-btn-grp')).get(0).all(by.css('.save')).click();
  }

  deleteRecentSearch() {
    return element.all(by.css('.ctrl-btn-grp')).get(0).all(by.css('.delete')).click();
  }

  getPermalinkButton() {
    return element.all(by.css('.ctrl-btn-grp')).get(0).all(by.css('.permalink')).click();
  }

  setEmailAlert() {
    return element.all(by.css('.ctrl-btn-grp')).get(0).all(by.css('.alertBtn')).click();
  }

  getRSS() {
    return element.all(by.css('.ctrl-btn-grp')).get(0).all(by.css('.rss')).click();
  }

  saveSearch (name) {
    this.saveRecentSearch().then(() =>{
      browser.sleep(2000);
      let input = element(by.css('save-search-modal input'));
      input.clear();
      input.sendKeys(name);
      return element(by.css('save-search-modal .btn-success')).click();
    });
  }

  getRecordList() {
    return element.all(by.css('.record-list'));
  }

  getSavedNameValue(index) {
    return this.getRecordList().get(index).element(by.css('.run-search > span'));
  }

  getSavedQueryValue(index) {
    return this.getRecordList().get(index).element(by.css('friendly-query > span'));
  }

  getSavedDatabaseValue(index) {
    return this.getRecordList().get(index).element(by.css('.databases'));
  }

  getPermalink() {
    return element(by.css('permalink-input input'));
  }

  getAlertQueryValue(index) {
    return this.getRecordList().get(index).element(by.css('.search-query'));
  }

  getSearchId (index) {
    return element.all(by.css('.searchId')).get(index).getAttribute("textContent");
  }

  getCombineSearchesBox() {
    return element.all(by.css('combine-search-box'));
  }

  selectSearch(index) {
    return element.all(by.css('label input')).get(index);
  }

  getSearchesFromCombineBox() {
    return this.getCombineSearchesBox().get(1).all(by.css('.selection > ul li p'));
  }

  getSearchToDelete(index) {
    return this.getCombineSearchesBox().get(1).all(by.css('.selection > ul i'));
  }

  changeOperator() {
    return element.all(by.css('.searchOperator')).get(1).all(by.css('option')).get(1).click();
  }

  getRunSearchButton() {
    return element.all(by.css('.runSearch')).get(1).click();
  }

  getSearchBoxFriendlyQuery(index) {
    return this.getCombineSearchesBox().get(1).all(by.css('.selection > ul li p friendly-query')).get(index);
  }

  getClearAllButton() {
    return this.getCombineSearchesBox().get(1).element(by.css('combine-search-box a'));
  }

  getSavedSearchesCount() {
     return element.all(by.css('article')).count();
  }

  deleteSearch(index) {
    return element.all(by.css('.delete-search')).get(index).click();
  }

  cofirmDeletion() {
    return element(by.css('delete-confirmation-modal .btn-success')).click();
  }
}
