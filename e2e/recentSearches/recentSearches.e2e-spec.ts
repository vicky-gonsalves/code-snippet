import { RecentSearches } from './recentSearches.po';
import { browser } from 'protractor';

describe('Recent Searches Page', function() {
  let page: RecentSearches ;

  beforeAll(() => {
    page = new RecentSearches ();
    page.search('play', 'PsycTESTS');
    browser.sleep(4000);
    page.navigateTo();
    browser.manage().timeouts().implicitlyWait(9000);
  });

  it('should have Recent Searches heading', () => {
    expect(page.getPageHeading()).toEqual('Recent Searches');
  });

  it('should add the recent search to the Recent Searches page', () => {
    let typeOfPage = 'search result'
    let query;
    let totalRecords;
    page.getSeachTotalRecords().getText().then(count => {
      totalRecords = count;
    });
     page.getFriendlyQuery(typeOfPage).getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
      query = text;
    }).then(() => {
      page.navigateTo();
      page.getFriendlyQuery('recent searches').getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
        expect(text).toBe(query);
      });
      page.getRecentSearchTotalRecords().getText().then(count => {
        expect(count.trim()).toBe(totalRecords);
      });
      page.getRecentSearchDBs().then(dbs => {
        expect(dbs.toString().replace(',','')).toBe('PsycTESTS,PsycTHERAPY');
      });
      page.getButtonsCount().then( count => {
         expect(count).toBe(6);
      });
    });
  });

  it('should run the search when title is clicked', () => {
    let query;
    page.getFriendlyQuery('recent searches').getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
      query = text;
    }).then(() =>{
      page.getTitleLink().click().then(() => {
        expect(browser.getCurrentUrl()).toContain('/search/results');
        page.getFriendlyQuery('search result').getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
          expect(text).toBe(query);
        });
      });
    });
  });

  it('should edit the recent search', () => {
    page.navigateTo();
    page.editRecentSearch().then(() => {
      expect(browser.getCurrentUrl()).toContain('/search/basic');
      browser.sleep(1000);
      page.getSearchInput().getAttribute('ng-reflect-model').then(text => {
        expect(text).toBe('play');
      });
      page.getSelectedDBs().then(count => {
        expect(count).toBe(1);
      });
    });
  });

  it('should save recent search', () => {
    page.navigateTo();
    let typeOfPage = 'recent searches'
    let name = 'recent searches test';
    let query;
    page.getFriendlyQuery(typeOfPage).getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
      query = text;
    }).then(() =>{
      page.saveSearch(name);
      browser.sleep(4000);
      page.getSavedQueryValue(0).getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
        expect(text).toBe(query);
        expect(page.getSavedNameValue(0).getText()).toBe(name);
        expect(page.getSavedDatabaseValue(0).getText()).toBe('PsycTESTS, PsycTHERAPY');
      }).then(() => {
        page.deleteSearch(0).then(() =>{
          browser.sleep(1000);
          page.cofirmDeletion();
        });
      });
    });
  });

  it('should set Email Alert for recent search', () => {
    page.navigateTo();
    let typeOfPage = 'recent searches'
    let query;
    page.getFriendlyQuery(typeOfPage).getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
      query = text.replace(/<(?:.|\n)*?>/g, '');
    }).then(() =>{
      page.setEmailAlert().then(() => {
        browser.sleep(3000);
        expect(page.getAlertQueryValue(0).getText()).toBe(query);
      });
    });
  });

  it('should open RSS Feed', () => {
    let searchId;
    page.navigateTo();
    page.getSearchId(0).then(id => {
      searchId = id;
      page.getRSS();
      browser.sleep(2000);
    }).then(() => {
      browser.getAllWindowHandles().then(function (handles) {
        let newWindowHandle = handles[handles.length-1]; // this is the RSS window
        browser.driver.switchTo().window(newWindowHandle).then(function () {
          browser.getCurrentUrl().then( url =>{
            expect(url).toContain(searchId);
          });
        });
        browser.driver.close().then(() => {
          browser.switchTo().window(handles[0]);
        });
      });
    });
  });

  it('should display correct Permalink', () => {
    let permalink;
    let query;
    page.navigateTo();
    page.getFriendlyQuery('').getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
      query = text;
    }).then(() => {
      page.getPermalinkButton().then(() =>{
        page.getPermalink().getAttribute('ng-reflect-value').then(link => {
          permalink = link.replace('http://localhost:9001', '');
        }).then(() => {
          browser.get(permalink);
          browser.sleep(4000);
          page.getFriendlyQuery('search result').getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
            expect(text).toBe(query);
          });
        });
      });
    });
  });

  it('should have Combine Searches Box', () => {
    page.navigateTo();
    expect(page.getCombineSearchesBox().count()).toBeGreaterThan(0);
  });

  it('should add/remove terms for search', () => {
    // add recent search to the combine search box
    page.selectSearch(0).click().then(()=>{
      expect(page.getSearchesFromCombineBox().count()).toBe(page.getSavedSearchesCount());
    }).then(() => {
      // remove recent search from the combine search box
      page.selectSearch(0).click();
      expect(page.getSearchesFromCombineBox().count()).toBe(0);
    });
  });

  it('should remove terms from the Combine Search Box', () => {
    // add one recent search to the combine search box
    page.selectSearch(1).click();
    expect(page.getSearchesFromCombineBox().count()).toBe(1);
    // remove search from the the combine search box (by clicking on the red X)
    page.getSearchToDelete(0).click();
    expect(page.getSearchesFromCombineBox().count()).toBe(0);
    expect(page.selectSearch(1).getAttribute('ng-reflect-model')).toBeNull();
  });

  it('should remove all selected searches when the \'clear all\' button is clicked', () => {
    //add all recent searches to the combine box
    page.navigateTo();
    page.selectSearch(0).click();
    expect(page.getSearchesFromCombineBox().count()).toBe(page.getSavedSearchesCount());
    page.getClearAllButton().click()
    expect(page.getSearchesFromCombineBox().count()).toBe(0);
  });

  it('should combine searches using OR', () => {
    page.search('happy', 'PsycBOOKS');
    browser.sleep(1000);
    page.navigateTo();
    let firstQuery, secondQuery;
    browser.sleep(3000);
    page.selectSearch(1).click();
    page.selectSearch(2).click();
    page.getSearchBoxFriendlyQuery(0).getText().then(text => {
      firstQuery = text.replace(/<(?:.|\n)*?>/g, '');
    });
    page.getSearchBoxFriendlyQuery(1).getText().then(text => {
      secondQuery = text.replace(/<(?:.|\n)*?>/g, '');
    });
    page.changeOperator();
    page.getRunSearchButton().then(() => {
      expect(browser.getCurrentUrl()).toContain('/search/results');
    }).then(() => {
      page.getFriendlyQuery('search result').getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
        let query = text.replace(/<(?:.|\n)*?>/g, '');
        expect(firstQuery +' OR ' + secondQuery).toBe(query);
      });
    });
  });
});
