import { browser } from 'protractor';
import { DOILandingPB } from './doiLandingPB.po';

describe('DOI Landing - PB', () => {
  let page: DOILandingPB;
  beforeAll(() => {
    browser.manage().timeouts().implicitlyWait(3000);
    page = new DOILandingPB();
    page.setUserAndEntitlements();
    page.navigate();
  });

  it('should display the book links.', () => page.checkBookLinks());
  it('should display the book chapter details.', () => page.checkDetails());
  it('should add the book chapter to cart.', () => page.addChapterToCart());
  it('should not display (Add To Cart) button, for the master book item.', () => page.hideAddToCartForBook());
  it('should navigate to record display.', () => page.goToRecordDisplay());

  afterAll(() => page.resetUserAndEntitlements());
});
