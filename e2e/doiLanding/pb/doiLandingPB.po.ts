import { browser, element, by } from 'protractor';

export class DOILandingPB {
  navigate(doi?: string) {
    let url = doi ? '/doiLanding?doi=' + doi : '/doiLanding?doi=10.1037/14189-003';
    browser.get(url);
    browser.sleep(2000);
  }

  checkBookLinks() {
    expect(element(by.css('book-info .links li:first-child a')).getAttribute('href')).toContain('www.apa.org/pubs/books/index.aspx');
    expect(element(by.css('book-info .links li:last-child a')).getAttribute('href')).toContain('/PsycBOOKS/toc/14189');
  }

  checkDetails() {
    let sections = ['meta', 'title', 'authors', 'citation', 'abstract'];
    for(let section of sections) {
      expect(element(by.css('pb-details .' + section)).getText()).not.toBe('');
    }
  }

  addChapterToCart() {
    let button = element(by.css('add-to-cart button'));
    button.click();
    browser.sleep(2000);
    expect(button.getText()).toBe('ADDED TO CART');
  }

  goToRecordDisplay() {
    let title = element(by.css('.title a'));
    title.getText().then(text => {
      title.click();
      browser.sleep(4000);
      expect(element(by.css('h1')).getText()).toBe(text);
    });
  }

  hideAddToCartForBook() {
    this.navigate('10.1037/14189-000');
    expect(element(by.css('add-to-cart')).isPresent()).toBeFalsy();
  }

  setUserAndEntitlements() {
    this.setUserAsAnonymous();
  }

  resetUserAndEntitlements() {
    this.setUserAsAnonymous(true);
  }

  private setUserAsAnonymous(revert?: boolean) {
    browser.get('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    if (revert) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');

      element(by.css('identitymanagement input[type=submit]')).click();
      browser.sleep(1000);

      element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
      for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
        element(by.id(thing)).click();
      }
      element(by.css('entitlements input[type=submit]')).click();

    } else {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');

      element(by.css('identitymanagement input[type=submit]')).click();
    }

    browser.sleep(1000);
  }
}
