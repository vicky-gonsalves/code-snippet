import { browser, element, by } from 'protractor';

export class DOILanding {
  navigate(doi?: string) {
    let url = doi ? '/doiLanding?doi=' + doi : '/doiLanding?doi=10.1037/hea0000401';
    browser.get(url);
    browser.sleep(2000);
  }

  checkPage() {
    expect(element(by.css('doi-landing')).isDisplayed()).toBeTruthy();
  }

  checkIdentityBar() {
    expect(element.all(by.css('identitybar li')).count()).toBe(1);
    expect(element(by.css('identitybar li')).getText()).toBe('Login');
  }

  checkHeader() {
    expect(element(by.css('searchmenu')).isPresent()).toBeFalsy();
    expect(element(by.css('browsemenu')).isPresent()).toBeFalsy();
    expect(element(by.css('thesaurusmenu')).isPresent()).toBeFalsy();
    expect(element(by.css('personalization')).isPresent()).toBeFalsy();
  }

  checkNavigationLinks() {
    expect(element(by.css('links li:first-child a')).getAttribute('href')).toContain('/search');
    expect(element(by.css('links li:nth-child(2) a')).getAttribute('href')).toContain('/MyPsycNET/alerts');
    expect(element(by.css('links li:last-child a')).getAttribute('href')).toContain('my.apa.org');
  }

  tryInvalidRecord() {
    this.navigate('10.1037/hea000040');
    browser.sleep(5000);
    expect(element(by.css('doiLanding')).isPresent()).toBeFalsy();
  }

  accessAsMember() {
    this.resetUserAndEntitlements();
    this.navigate();
    browser.sleep(5000);
    expect(element(by.css('doiLanding')).isPresent()).toBeFalsy();
  }

  setUserAndEntitlements() {
    this.setUserAsAnonymous();
  }

  resetUserAndEntitlements() {
    this.setUserAsAnonymous(true);
  }

  private setUserAsAnonymous(revert?: boolean) {
    browser.get('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    if (revert) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');

      element(by.css('identitymanagement input[type=submit]')).click();
      browser.sleep(1000);

      element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
      for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
        element(by.id(thing)).click();
      }
      element(by.css('entitlements input[type=submit]')).click();

    } else {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');

      element(by.css('identitymanagement input[type=submit]')).click();
    }

    browser.sleep(1000);
  }
}
