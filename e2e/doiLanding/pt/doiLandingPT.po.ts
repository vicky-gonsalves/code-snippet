import { browser, element, by } from 'protractor';

export class DOILandingPT {
  navigate(doi?: string) {
    browser.get('/doiLanding?doi=10.1037/t54303-000');
    browser.sleep(2000);
  }

  checkPTNote() {
    expect(element(by.css('.pt-message')).isDisplayed()).toBeTruthy();
    expect(element(by.css('.pt-message a')).getAttribute('href')).toContain('www.apa.org/pubs/databases/psyctests/');
  }

  checkSignInLink() {
    expect(element(by.css('.sign-in')).getAttribute('href')).toContain('/user/login');
  }

  checkDetails() {
    let sections = ['meta', 'title', 'created-by', 'authors', 'citation', 'abstract', 'test-available', 'permissions'];
    for(let section of sections) {
      expect(element(by.css('pt-details .' + section)).getText()).not.toBe('');
    }
  }

  goToRecordDisplay() {
    let title = element(by.css('.title a'));
    title.getText().then(text => {
      title.click();
      browser.sleep(4000);
      expect(element(by.css('h1')).getText()).toContain(text);
    });
  }

  setUserAndEntitlements() {
    this.setUserAsAnonymous();
  }

  resetUserAndEntitlements() {
    this.setUserAsAnonymous(true);
  }

  private setUserAsAnonymous(revert?: boolean) {
    browser.get('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    if (revert) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');

      element(by.css('identitymanagement input[type=submit]')).click();
      browser.sleep(1000);

      element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
      for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
        element(by.id(thing)).click();
      }
      element(by.css('entitlements input[type=submit]')).click();

    } else {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');

      element(by.css('identitymanagement input[type=submit]')).click();
    }

    browser.sleep(1000);
  }
}
