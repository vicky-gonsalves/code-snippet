import { browser } from 'protractor';
import { DOILandingPT } from './doiLandingPT.po';

describe('DOI Landing - PT', () => {
  let page: DOILandingPT;
  beforeAll(() => {
    browser.manage().timeouts().implicitlyWait(3000);
    page = new DOILandingPT();
    page.setUserAndEntitlements();
    page.navigate();
  });

  it('should display the PT subscription note.', () => page.checkPTNote());
  it('should display the left-side (Sign in) link.', () => page.checkSignInLink());
  it('should display the test details.', () => page.checkDetails());
  it('should navigate to record display.', () => page.goToRecordDisplay());

  afterAll(() => page.resetUserAndEntitlements());
});
