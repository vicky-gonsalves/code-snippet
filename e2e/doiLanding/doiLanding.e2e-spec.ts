import { browser } from 'protractor';
import { DOILanding } from './doiLanding.po';

describe('DOI Landing', () => {
  let page: DOILanding;
  beforeAll(() => {
    page = new DOILanding();
    page.setUserAndEntitlements();
    page.navigate();
  });

  it('should load the right page.', () => page.checkPage());
  it('should hide the identity bar elements (except: "Login").', () => page.checkIdentityBar());
  it('should hide the header menus.', () => page.checkHeader());
  it('should display the left-side main links.', () => page.checkNavigationLinks());
  it('should redirect to home, when the record is not found.', () => page.tryInvalidRecord());
  it('should not display the page for non-guest users.', () => page.accessAsMember());
});
