import { browser, element, by } from 'protractor';

export class DOILandingPA {
  navigate() {
    browser.get('/doiLanding?doi=10.1037/ccp0000146');
    browser.sleep(2000);
  }

  checkJournalLinks() {
    expect(element(by.css('journal-info .links li:first-child a')).getAttribute('href')).toContain('www.apa.org/pubs/journals/ccp');
    expect(element(by.css('journal-info .links li:nth-child(2) a')).getAttribute('href')).toContain('/PsycARTICLES/journal/ccp');
    expect(element(by.css('journal-info .links li:last-child a')).getAttribute('href')).toContain('rightslinkdev.apa.org/index.cfm?uid=2016-44084-001&docType=j');
    expect(element(by.css('.rss a')).getAttribute('href')).toContain('content.apa.org/journals/ccp.rss');
  }

  checkDetails() {
    let sections = ['meta', 'title', 'authors', 'citation', 'abstract', 'impact-statement'];
    for(let section of sections) {
      expect(element(by.css('pa-details .' + section)).getText()).not.toBe('');
    }
  }

  addArticleToCart() {
    let button = element(by.css('add-to-cart button'));
    button.click();
    browser.sleep(2000);
    expect(button.getText()).toBe('ADDED TO CART');
  }

  goToRecordDisplay() {
    let title = element(by.css('.title a'));
    title.getText().then(text => {
      title.click();
      browser.sleep(4000);
      expect(element(by.css('h1')).getText()).toBe(text);
    });
  }

  setUserAndEntitlements() {
    this.setUserAsAnonymous();
  }

  resetUserAndEntitlements() {
    this.setUserAsAnonymous(true);
  }

  private setUserAsAnonymous(revert?: boolean) {
    browser.get('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    if (revert) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');

      element(by.css('identitymanagement input[type=submit]')).click();
      browser.sleep(1000);

      element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
      for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
        element(by.id(thing)).click();
      }
      element(by.css('entitlements input[type=submit]')).click();

    } else {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');

      element(by.css('identitymanagement input[type=submit]')).click();
    }

    browser.sleep(1000);
  }
}
