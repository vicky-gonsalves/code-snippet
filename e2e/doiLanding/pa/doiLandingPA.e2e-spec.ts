import { browser } from 'protractor';
import { DOILandingPA } from './doiLandingPA.po';

describe('DOI Landing - PA', () => {
  let page: DOILandingPA;
  beforeAll(() => {
    browser.manage().timeouts().implicitlyWait(3000);
    page = new DOILandingPA();
    page.setUserAndEntitlements();
    page.navigate();
  });

  it('should display the journal links.', () => page.checkJournalLinks());
  it('should display the article details.', () => page.checkDetails());
  it('should add the article to cart.', () => page.addArticleToCart());
  it('should navigate to record display.', () => page.goToRecordDisplay());

  afterAll(() => page.resetUserAndEntitlements());
});
