import { browser, element, by } from 'protractor';

export class Localization {
  private lang = 'en';

  navigate(path: string) {
    browser.get(path);
    browser.sleep(2000);
    this.selectLanguage('de');
  }

  checkBasicSearch() {
    this.navigate('/search/basic');
    let data = { admincenter: 'Admin-Zentrum', contact: 'Kontakt', help: 'Hilfe', disclaimer: 'Dementi', pdfrequire: 'PDF-Dokumente erfordern', 'trending': 'Trendverlauf' };
    this.performLoopCheck(data);

    // Recent Searches
    expect(element(by.css('personalization li:first-child')).getText()).toBe('Zuletzt ausgeführte Suchen');

    // Search
    expect(element(by.css('searchmenu #searchString')).getText()).toBe('Suchen'.toUpperCase());
    element(by.css('searchmenu #searchString')).click();
    // Advanced Search
    expect(element(by.css('searchmenu .list-view li:nth-child(2)')).getText()).toBe('Erweiterte Suche');

    // Browse
    expect(element(by.css('browsemenu #searchString')).getText()).toBe('Durchsuchen'.toUpperCase());
    element(by.css('browsemenu #searchString')).click();
    // Tests and Measures
    expect(element(by.css('browsemenu .list-view li:nth-child(4) .db-name')).getText()).toContain('Tests und Messinstrumente');
  }

  checkAdvancedSearchForm() {
    this.navigate('/search/advanced');

    // Select Databases
    expect(element(by.css('.db-label')).getText()).toBe('Datenbanken auswählen');

    element(by.id('fields')).click();
    browser.sleep(1000);
    let data = {
      AnyField: 'Alle Bereiche', JournalTitle: 'Titel des Fachmagazins', TestNames: 'Testname', Location: 'Geografischer Standort', impact: 'Impact-Erklärung',
      openaccess: 'Offen Zugang', date: 'DATUM', present: 'bis aktuell'
    };
    this.performLoopCheck(data);

    // Action: select the (Methodology) filter to display its options.
    let filter = element(by.id('deep_filters_1'));
    filter.click();

    // Methodology
    expect(filter.element(by.css('option:nth-child(6)')).getText()).toBe('Methodologie');
    filter.element(by.css('option:nth-child(6)')).click();
    browser.sleep(1000);

    // Brain Imaging
    expect(element(by.css('#filter_entries_1 option:first-child')).getText()).toBe('Brain-Imaging');
    // Followup Study
    expect(element(by.css('#filter_entries_1 option:nth-child(6)')).getText()).toBe('Nachfolgestudie');
  }

  checkCitedReferencesForm() {
    this.navigate('/search/citedRefs');

    // Cited References
    expect(element(by.css('.searchHeading')).getText()).toBe('Zitierte Referenzen');
    // Source
    expect(element(by.id('sourcelabel')).getText()).toBe('Quelle');
  }

  checkBrowsePA() {
    this.navigate('/PsycARTICLES');

    let data = { bytitle: 'Titel', bytopic: 'Thema', mostdownloaded: 'Am häufigsten heruntergeladene' };
    this.performLoopCheck(data);

    // (Jump to) placeholder
    expect(element.all(by.css('jumpto')).first().element(by.css('#jumpTo')).getAttribute('placeholder')).toBe('Springen zu...');

    // Impact Factor
    expect(element.all(by.css('.impact-factor')).first().getText()).toBe('Impact-Faktor');

    element(by.id('bytopic')).click();
    browser.sleep(4000);
    // Journal History
    expect(element.all(by.css('.topic-group:nth-child(2) .journal-history')).first().getText()).toBe('Fachjournal-Historie');

    // Move into Journal Home
    this.navigate('/PsycARTICLES/journal/ort/');
    browser.sleep(5000);

    // Online First
    expect(element(by.id('onlinefirst')).getText()).toBe('Online-First');

    // Society
    expect(element(by.id('society')).getText()).toBe('Gesellschaft');

    // Permissions
    expect(element.all(by.css('.permission-caption')).first().getText()).toBe('Genehmigungen');

    element(by.id('issueType')).click();
    // Latest Issue
    expect(element(by.css('#issueType option:last-child')).getText()).toBe('Letzte Ausgabe');
  }

  checkBrowsePB() {
    this.navigate('/PsycBOOKS');

    // Latest Release
    expect(element(by.id('bylatestrelease')).getText()).toBe('Letzte Veröffentlichung');

    // Classic Books
    expect(element(by.css('browsepbfilterbox div:nth-child(4) .checkbox')).getText()).toBe('Klassische Bücher');

    // TOC: Chapter PDFs
    expect(element.all(by.css('.toc')).first().getText()).toContain('Inhaltsverzeichnis: Kapitel-PDFs');

    // Add To My List
    expect(element.all(by.css('add-to-my-list .caption')).first().getText()).toContain('Zu eigener Liste hinzufügen');
  }

  checkBrowsePC() {
    this.navigate('/PsycCRITIQUES');

    // Associate Editors
    expect(element(by.css('browsepceditorialboard li:nth-child(2)')).getText()).toBe('Mitredakteure/-innen');

    // Reviewer
    expect(element(by.id('reviewer')).getText()).toBe('Rezensent/-in');

    // Previous
    expect(element.all(by.css('.visible-md pagerpagination')).first().element(by.id('previous')).getText()).toBe('Zurück');

    // Full-Text HTML
    expect(element.all(by.css('.ft-caption')).first().getText()).toBe('Volltext als HTML');
  }

  checkBrowsePT() {
    this.navigate('/PsycTESTS');

    // Test Name & Acronym
    expect(element(by.id('acronym-caption')).getText()).toBe('Testname & Akronym');

    // Summary
    expect(element.all(by.css('ptbuttonbar .buttonLink:first-child .caption')).first().getText()).toBe('Zusammenfassung');
  }

  checkBrowsePH() {
    this.navigate('/PsycTHERAPY');

    let data = { byapproach: 'Therapieansatz', bytherapistname: 'Therapeut/-in' };
    this.performLoopCheck(data);
  }

  checkBrowsePE() {
    this.navigate('/PsycEXTRA');

    let data = { bycontentowner: 'Inhaltseigentümer/-in', bydoctype: 'Dokumenttyp' };
    this.performLoopCheck(data);
  }

  checkThesaurus() {
    this.navigate('/thesaurus');
    browser.sleep(2000);

    // Look Up Term
    expect(element(by.id('lookupterm')).getText()).toBe('Begriff suchen');

    // Add to search using
    expect(element(by.css('.hide-for-mobile .addtosearchusing')).getText()).toContain('Zur Suche hinzufügen mit');

    // use
    expect(element.all(by.css('.accordionContainer')).get(8).getText()).toContain('verwenden');

    // Related Term
    let termContainer = element.all(by.css('.accordionContainer')).first();
    termContainer.all(by.css('a:first-child')).first().click();
    browser.sleep(1000);
    expect(termContainer.element(by.css('.relatedterm')).getText()).toBe('Verwandter Begriff');
  }

  checkMyList() {
    this.navigate('/search/mylist');
    browser.sleep(3000);

    // References
    expect(element.all(by.css('.references')).first().getText()).toContain('Referenzen');

    // Show All Abstracts
    expect(element(by.id('showallabstracts')).getText()).toBe('Alle Abstracts anzeigen');

    // Order Added
    let sortBy = element.all(by.css('sort-by')).first();
    sortBy.element(by.css('select')).click();
    expect(sortBy.element(by.css('select option:last-child')).getText()).toBe('Bestellung hinzugefügt');
  }

  checkMyPN() {
    this.navigate('/MyPsycNET/savedSearches');

    // My Profile
    expect(element(by.css('.nav-tabs li:first-child')).getText()).toBe('Mein Profil');

    // Saved Searches
    expect(element(by.css('.nav-tabs li:last-child')).getText()).toBe('Gespeicherte Suchen');

    // Query
    expect(element(by.css('.saved-search-table th:nth-child(2)')).getText()).toBe('Suchanfrage');
  }

  checkGeneralPages() {
    this.navigate('/general/contactUs');
    let data: any = { customerservice: 'Kundendienst, Training oder Suchhilfe', orderpacks: 'Um elektronische Zugangspakete zu bestellen' };
    this.performLoopCheck(data);


    this.navigate('/general/feedback');
    data = { wevalueyourfeedback: 'Wir freuen uns über Feedback!', searchproblem: 'Problem bei der Suche', institutionlabel: 'Institution' };
    this.performLoopCheck(data);

    this.navigate('/general/tos');
    data = { serviceshutdown: 'Wie ist unangemessene Nutzung, die zur Einstellung aller Leistungen führt, definiert?', subscribers: 'Abonnenten von APA-Datenbankpaketen.' };
    this.performLoopCheck(data);

    this.navigate('/general/disclaimer');
    data = { acknowledge: 'Sie bestätigen, dass die APA keinerlei Haftung für das Screening oder die Prüfung von Inhalten bzw. Benutzerinhalten übernimmt, die von anderen Parteien als der APA stammen, und dass die APA das Recht hat, aber nicht verpflichtet ist, nach eigenem Ermessen Inhalte bzw. Benutzerinhalte, die auf der Website zur Verfügung gestellt werden, zu prüfen, abzulehnen, überwachen, bearbeiten oder zu entfernen. Die APA schließt ausdrücklich jegliche Verantwortung oder Haftung Ihnen oder anderen Personen oder Stellen gegenüber bezüglich der Prüfung oder Nichtprüfung solcher Inhalte bzw. Benutzerinhalte aus. Sie bestätigen, dass Sie allein das gesamte Risiko in Verbindung mit der Nutzung von Inhalten bzw. Benutzerinhalten übernehmen.' };
    this.performLoopCheck(data);
  }

  checkSearchResult() {
    this.navigate('/search/basic');
    element(by.id('searchForInput')).sendKeys('happy');
    browser.sleep(1000);

    element(by.id('searchForBtn')).click();
    browser.sleep(3000);

    let data = { editBtn: 'Suche bearbeiten', searchwithin: 'In Ergebnissen Suchen' };
    this.performLoopCheck(data);

    // Publication Type
    expect(element(by.css('.filter-display:first-child')).getText()).toContain('');

    element(by.css('.topNav search-results-nav')).element(by.css('.export.btn')).click();
    let exportModal = element(by.css('#TopNav export-ris-modal'));
    browser.sleep(1000);

    // Selected Records
    expect(exportModal.element(by.css('.selectedrecords')).getText()).toBe('Ausgewählte Einträge');
    // Reference Software
    expect(exportModal.element(by.css('.export-type.btn')).getText()).toContain('Referenz-Software');
  }

  checkRecordDisplay() {
    this.navigate('/record/2004-16329-000');

    let data = { citation: 'Zitation', uid: 'Eindeutige Kennung', bookchapterstoc: 'Buch-Inhaltsverzeichnis: Kapitel-PDFs', database: 'Datenbank' };
    this.performLoopCheck(data);
  }

  private selectLanguage(langCode: string) {
    if (this.lang !== langCode) {
      element(by.id('localization-dropdown')).click();
      browser.sleep(1000);
      element(by.id(langCode)).click();
      this.lang = langCode;
      browser.sleep(2000);
    }
  }

  private performLoopCheck(data) {
    for (let id in data) {
      expect(element(by.id(id)).getText()).toBe(data[id]);
    }
  }
}
