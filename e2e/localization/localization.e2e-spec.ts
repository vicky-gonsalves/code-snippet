import { browser } from 'protractor';
import { Localization } from './localization.po';

describe('Localization', () => {
  let page: Localization;

  beforeAll(() => {
    browser.manage().timeouts().implicitlyWait(5000);
    page = new Localization();
  });

  it('should translate (Header), (Footer) and (Basic Search).', () => page.checkBasicSearch());
  it('should translate (Advanced Search).', () => page.checkAdvancedSearchForm());
  it('should translate (Cited References).', () => page.checkCitedReferencesForm());
  it('should translate (Browse PA).', () => page.checkBrowsePA());
  it('should translate (Browse PB).', () => page.checkBrowsePB());
  it('should translate (Browse PC).', () => page.checkBrowsePC());
  it('should translate (Browse PT).', () => page.checkBrowsePT());
  it('should translate (Browse PH).', () => page.checkBrowsePH());
  it('should translate (Browse PE).', () => page.checkBrowsePE());
  it('should translate (Thesaurus).', () => page.checkThesaurus());
  it('should translate (My List).', () => page.checkMyList());
  it('should translate (My PsycNET).', () => page.checkMyPN());
  it('should translate the general pages.', () => page.checkGeneralPages());
  it('should translate (Search Result).', () => page.checkSearchResult());
  it('should translate (Record Display).', () => page.checkRecordDisplay());
});
