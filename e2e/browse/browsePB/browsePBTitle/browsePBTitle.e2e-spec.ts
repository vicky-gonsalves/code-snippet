import { BrowsePBTitle } from './browsePBTitle.po';
import { browser } from 'protractor';

describe('Browse PsycBook Title Page', function() {
  let page: BrowsePBTitle;

  beforeEach(() => {
    browser.manage().timeouts().implicitlyWait(9000);
    page = new BrowsePBTitle();
    page.navigateTo();
  });

  it('should have PsycBOOKS heading', () => {
    expect(page.getPageHeading()).toEqual('PsycBOOKS');
  });

   it('should have \'Browse by Title\' in breadcrumbs', () => {
    expect(page.getBreadcrumbsCompnentTitle().getText()).toBe('Browse by Title');
  });

  it('should have browseaz component', () => {
    expect(page.getBrowseAZ().count()).toBeGreaterThan(0);
  });

  it('should have paginationLimit component', () => {
    expect(page.getPaginationLimit().count()).toBeGreaterThan(0);
  });

  it('should have jumpto component', () => {
    expect(page.getJumpto().count()).toBeGreaterThan(0);
  });

  it('should display correct number of elements based on the selected limit at first loading', () => {
    let limit: number;
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedTitlesCount().then(count => {
        expect(count).toEqual(limit);
      });
    });
  });

  it('should display pagination component if records are more than display limit', () => {
    let totalItems: number;
    let limit: number;
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l=> {
      limit = parseInt(_l);
      page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records=> {
        totalItems = parseInt(records);
        expect(!!page.getPaginationLink().count()).toBe(totalItems > limit);
      });
    });
  });

  it('should display correct number of elements when the limit change to be 50', () => {
    let limit: number;
    page.changeLimitValue();
    browser.sleep(3000);
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedTitlesCount().then(count => {
        expect(+count).toBe(limit);
      });
    });
  });

  it('should not display pagination component when letter Q is selected from the alphabet strip', () => {
    let totalItems: number;
    let limit: number;
    page.getSelectedLetter(17); //Select letter Q from the alphabet strip
    expect(page.getPaginationComponent().isDisplayed()).toBe(false);
  });

  it('should display records starting with B when letter B is selected from the alphabet strip', () => {
    page.getSelectedLetter(2);
    browser.sleep(1000);
    page.getTitles().each(topic=> {
      topic.getText().then(name=> {
        let replacedName = name.replace(/A |An |The /gi,'');
        expect(replacedName.charAt(0).toUpperCase()).toBe('B');
      });
    });
  });

  it('should display No records found when letter X is selected from the alphabet strip', () => {
    page.getSelectedLetter(24); //Select letter X from the alphabet strip
    browser.sleep(1000);
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should open the correct record page', () => {
    page.getBookUID().then(uid => {
      page.openBookRecord();
      browser.sleep(1000);
      expect(browser.getCurrentUrl()).toContain('/record/'+ uid);
    });
  });

  it('should open the correct TOC page', () => {
    page.getBookPBID().then(pbid => {
      page.openTOCpage();
      expect(browser.getCurrentUrl()).toContain('/toc/'+ pbid);
    });
  });

  it('all show checkbox should be selected at the first', () => {
    page.getSelectedShowFilter().each( btn => {
      expect(btn.getAttribute('checked')).toBe('true');
     });
  });

  it('should display \'No record found\' when "Encyclopedia" checkbox is slected only and letter O', () => {
    page.deSelectedCheckBox();
    page.getSelectedLetter(15); //Select letter O from the alphabet strip
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should search topic by jumpto input', () => {
   let searchFor = 'body';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(1000);
    page.getTitles().each(topic=> {
      topic.getText().then(name=> {
        expect(name.toLowerCase().indexOf(searchFor)).toBe(0);
      });
    });
  });

  it('should dipslay No record found  when user enters \'numbers\' in the jumpto input', () => {
    let searchFor = 'numbers';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    browser.wait(page.executeSearch(),9000).then(()=> {
      expect(page.getEmptyPage().isDisplayed()).toBe(true);
    });
  });

  it('should navigate correct', () => {
    page.selectBrowseByLateseRelease().then(() => {
        browser.wait(page.selectBrowseByTitle(), 1000).then(() => {
         expect(browser.getCurrentUrl()).toContain('/title');
      });
    });
  });

  it('should add book to My List correctly', () => {
    let countBefore, countAfter;
    browser.sleep(1000);
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.addBookToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore+1);
      });
    });
  });

  it('should remove book from My List correctly', () => {
    let countBefore, countAfter;
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.addBookToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore-1);
      });
    });
  });

  it('should display APA Books instead of PsycBOOKS at breadcrumbs if user has not PB access but has book collection ', () => {
    page.removePBPermission();
    browser.sleep(1000);
    expect(page.getBreadcrumbsPageTitle().getText()).toBe('APA Books');
  });

  it('should display one record if user has not PB access but has book collection \'PBC2016\' and letter A is selected from alphabet strip', () => {
    page.getBookCollectionPermission();
    page.getSelectedLetter(1);  //Select letter A
    browser.sleep(1000);
    page.getDisplayedTitlesCount().then(count => {
      expect(+count).toBe(1);
    });
  });

  afterAll(() =>  {
    page.revertPermissionChanges();
    browser.sleep(1000);
  });
});
