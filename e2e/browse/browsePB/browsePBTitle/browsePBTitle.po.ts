import { browser, element, by } from 'protractor';

export class BrowsePBTitle {

  navigateTo(path?: string) {
    return (path && path.length) ? browser.get(path) : browser.get('/PsycBOOKS/title');
  }

  getPageHeading() {
    let heading = element(by.tagName('h1'));
    return heading.getText();
  }

  getBreadcrumbsCompnentTitle() {
    return element.all(by.css('breadcrumbs a')).get(1);
  }

  getBreadcrumbsPageTitle() {
    return element.all(by.css('breadcrumbs a')).get(0);
  }

  getBrowseAZ() {
    return element.all(by.css('browseaz'));
  }

  getPaginationLimit() {
    return element.all(by.css('paginationlimit'));
  }

  getJumpto() {
    return element.all(by.css('jumpto'));
  }

  getDisplayedTitlesCount() {
    return element.all(by.css('article')).count();
  }

  getDisplayedLimit() {
    return element.all(by.css('paginationlimit select')).get(0).getAttribute('ng-reflect-model');
  }

  changeLimitValue() {
    let limit = element.all(by.css('paginationlimit option')).get(1);
    return limit.click();
  }

  getPaginationComponent() {
    return element.all(by.css('numberedpagination')).first();
  }

  getPaginationLink() {
    return this.getPaginationComponent().all(by.css('div>ul>li>a'));
  }

  getSelectedLetter(orderLetter) {
    let browseButton =  element.all(by.css('browseaz')).get(0).element(by.css('li:nth-child('+orderLetter+') a'));
    return browseButton.click();
  }

  getTitles() {
    return element.all(by.css('article a'));
  }

  getEmptyPage() {
    return element(by.css('.emptyState'));
  }

  openBookRecord() {
    let bookTitle = element.all(by.css('article a')).get(0);
    return bookTitle.click();
  }

  getBookUID() {
    return element.all(by.css('.bookUID')).get(0).getAttribute("textContent");
  }

  getBookPBID() {
    return element.all(by.css('.bookPBID')).get(0).getAttribute("textContent");
 }

  openTOCpage() {
    let bookChapters = element.all(by.css('.buttonBar>button')).get(0);
    return bookChapters.click();
  }

  getSelectedShowFilter() {
    return element.all(by.css('browsepbfilterbox input'));
  }

  deSelectedCheckBox() {
    //deselect the first thress show checkBoxes (APA Books, APA Handbooks, Classic Books)
    element.all(by.css('browsepbfilterbox input')).get(0).click();
    element.all(by.css('browsepbfilterbox input')).get(1).click();
    element.all(by.css('browsepbfilterbox input')).get(2).click();
  }

  private getSearchInput() {
    return this.getJumpto().first().element(by.css('input[name="jumpTo"]'));
  }

  getSearchInputText() {
    let input = this.getSearchInput();
    return input.getAttribute('value');
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  executeSearch() {
    let searchBtn = this.getJumpto().first().element(by.css('button'));
    return searchBtn.click();
  }

  selectBrowseByTitle() {
    let newBrowse = element(by.id('bytitle'));
     return newBrowse.click();
  }

  selectBrowseByLateseRelease() {
    let newBrowse = element(by.id('bylatestrelease'));
    return newBrowse.click();
  }

  getMyListCount() {
    return element(by.css('personalization .badge')).getText();
  }

  addBookToMyList() {
    return element.all(by.css('add-to-my-list button')).get(0).click();
  }

  getMyListLabel() {
    return element.all(by.css('add-to-my-list button .caption')).get(0).click();
  }

  removePBPermission() {
    this.navigateTo('/admin');
    element(by.id('PB')).click();
    element(by.id('PBC2001')).click();  //add book collection
    element(by.css('entitlements input[type=submit]')).click();
    this.navigateTo();
  }

  getBookCollectionPermission() {
    this.navigateTo('/admin');
    element(by.id('PBC2001')).click();  //remove previous book collection
    element(by.id('PBC2016')).click();  //add book collection
    element(by.css('entitlements input[type=submit]')).click();
    this.navigateTo();
  }

  revertPermissionChanges() {
    this.navigateTo('/admin');
    element(by.id('PB')).click();
    element(by.id('PBC2016')).click();
    element(by.css('entitlements input[type=submit]')).click();
  }
}
