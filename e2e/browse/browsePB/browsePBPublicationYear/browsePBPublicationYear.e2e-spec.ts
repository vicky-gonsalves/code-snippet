import { BrowsePBPublicationYear } from './browsePBPublicationYear.po';
import { browser } from 'protractor';

describe('Browse PsycBook Publication Year Page', function() {
  let page: BrowsePBPublicationYear;

  beforeEach(() => {
    browser.manage().timeouts().implicitlyWait(9000);
    page = new BrowsePBPublicationYear();
    page.navigateTo();
  });

  it('should have PsycBOOKS heading', () => {
    expect(page.getPageHeading()).toEqual('PsycBOOKS');
  });

  it('should have \'Browse by Publication Year\' in breadcrumbs', () => {
    expect(page.getBreadcrumbsCompnentTitle().getText()).toBe('Browse by Publication Year');
  });

  it('should have \'Browse by Publication Year\' heading', () => {
    let header = page.getComponentHeading();
    expect(header.isPresent()).toBe(true);
    expect(header.getText()).toContain('Browse by Publication Year');
  });

  it('should expand and collapse some range of years', () => {
    page.expandYear(1);
    expect(page.getExpandedData().isPresent()).toBe(true);
    page.collapseYear(1);
    expect(page.getExpandedData().isPresent()).toBe(false);
  });

   it('should display max 10 list of years in each range (tested on the first 30 range)', () => {
     page.collapseYear(0);
    page.getYearsDisplayed().each((accordion, index) => {
      if(index <= 30) {
        accordion.click().then(() => {
          let count = page.getYear(index).count();
          expect(count).toBeLessThan(11)
        });
      }
    });
  });

  it('should open the selected year (2017) correctly', () => {
    page.openSelectedYear(0,1); //open the first year at the first accordion (2017)
    expect(browser.getCurrentUrl()).toContain('2017');
    browser.sleep(1500);
    expect(page.getBreadcrumbsYear().getText()).toBe('2017');
    expect(page.getSelectedPageHeading().getText()).toContain('2017');
  });

  it('should display books published in the selected year (2017)', () => {
    page.openSelectedYear(0,1); //open the first year at the first accordion (2017)
    page.getBooksData().each( data => {
      expect(data.getText()).toContain('2017');
     });
  });

  it('should have paginationLimit component at the selected year (2016)', () => {
    page.openSelectedYear(0,2); //open the second year at the first accordion (2017)
    expect(page.getPaginationLimit().count()).toBeGreaterThan(0);
  });

  it('should have jumpto component at the selected year (2016)', () => {
    page.openSelectedYear(0,2);
    expect(page.getJumpto().count()).toBeGreaterThan(0);
  });

  it('should display correct number of books based on the selected limit at first loading of the selected year (2016)', () => {
    let limit: number;
    page.openSelectedYear(0,2);
    browser.sleep(1500);
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedBooksCount().then(count => {
        expect(count).toBeLessThan(limit+1);
      });
    });
  });

  it('should display pagination component if records are more than display limit at the selected year (2016)', () => {
    let totalItems: number;
    let limit: number;
    page.openSelectedYear(0,2);
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l=> {
      limit = parseInt(_l);
      page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records=> {
        totalItems = parseInt(records);
        expect(!!page.getPaginationLink().count()).toBe(totalItems > limit);
      });
    });
  });

  it('should display correct number of books when the limit change to be 50 at the selected year (2016)', () => {
    let limit: number;
    page.openSelectedYear(0,2);
    page.changeLimitValue();
    browser.sleep(1000);
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedBooksCount().then(count => {
        expect(+count).toBeLessThan(limit+1);
      });
    });
  });

  it('all show checkbox should be selected at the first at the selected year (2016)', () => {
    page.openSelectedYear(0,2);
    page.getSelectedShowFilter().each( btn => {
      expect(btn.getAttribute('checked')).toBe('true');
     });
  });

  it('should display \'No record found\' when "Encyclopedia" checkbox is slected only at the selected year (2016)', () => {
    page.openSelectedYear(0,2);
    page.deSelectedCheckBox();
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should search topic by jumpto input at the selected year (2016)', () => {
    page.openSelectedYear(0,2);
    let searchFor = 'therapy';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(2000);
    page.getTitles().each(topic=> {
      topic.getText().then(name=> {
        expect(name.toLowerCase().indexOf(searchFor)).toBe(0);
      });
    });
  });

  it('should dipslay No record found  when user enters \'numbers\' in the jumpto input at the selected year (2016)', () => {
    page.openSelectedYear(0,2);
    let searchFor = 'numbers';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    browser.wait(page.executeSearch(),9000).then(()=> {
      expect(page.getEmptyPage().isDisplayed()).toBe(true);
    });
  });

  it('should navigate correct', () => {
    page.selectBrowseByLateseRelease().then(() => {
        browser.wait(page.selectBrowseByPublicationYear(), 1000).then(() => {
         expect(browser.getCurrentUrl()).toContain('/pubyear');
      });
    });
  });

  it('should open the correct record page at the selected year (2016)', () => {
    page.openSelectedYear(0,2);
    page.getBookUID().then(uid => {
      page.openBookRecord();
      browser.sleep(1000);
      expect(browser.getCurrentUrl()).toContain('/record/'+ uid);
    });
  });

  it('should open the correct TOC page at the selected year (2016)', () => {
    page.openSelectedYear(0,2);
    page.getBookPBID().then(pbid => {
      page.openTOCpage();
      expect(browser.getCurrentUrl()).toContain('/toc/'+ pbid);
    });
  });

  it('should add book to My List correctly at the selected year (2016)', () => {
    let countBefore, countAfter;
    page.openSelectedYear(0,2);
    browser.sleep(1000);
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.addBookToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore+1);
      });
    });
  });

  it('should remove book from My List correctly at the selected year (2016)', () => {
    let countBefore, countAfter;
    page.openSelectedYear(0,2);
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.removeBookToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore-1);
      });
    });
  });

  it('should display APA Books instead of PsycBOOKS at breadcrumbs if user has not PB access but has book collection ', () => {
    page.removePBPermission();
    browser.sleep(1000);
    expect(page.getBreadcrumbsPageTitle().getText()).toBe('APA Books');
  });

  it('should display one accordion and one year (2001) if user has not PB access but has book collection \'PBC2001\'', () => {
    page.getAccordionRange().count().then(count => {
      expect(+count).toBe(1);
    });
    page.getAccordionRange().getText().then(text => {
      expect(text.toString()).toContain('2001 - 2000');
      expect(page.getDisplayedYear().then(text => text.toString())).toContain('2001');
    });
  });

  afterAll(() =>  {
    page.revertPermissionChanges();
    browser.sleep(1000);
  });
});
