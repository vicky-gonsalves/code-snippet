import { browser, element, by } from 'protractor';

export class BrowsePBPublicationYear {

  navigateTo(path?: string) {
    return (path && path.length) ? browser.get(path) : browser.get('/PsycBOOKS/pubyear');
  }

  getPageHeading() {
    let heading = element(by.tagName('h1'));
    return heading.getText();
  }

  getBreadcrumbsPageTitle() {
    return element.all(by.css('breadcrumbs a')).get(0);
  }

  getBreadcrumbsCompnentTitle() {
    return element.all(by.css('breadcrumbs a')).get(1);
  }

  getBreadcrumbsYear() {
    return element.all(by.css('breadcrumbs a')).get(2);
  }

  getComponentHeading() {
    return element(by.css('.browseHeading'));
  }

  getBottomHeading() {
    return element(by.css('.browseBottomHeading'));
  }

  expandYear(index) {
    return element.all(by.css('.accordionHeading > a')).get(index).click();
  }

  collapseYear(index) {
    return element.all(by.css('.accordionHeading > a')).get(index).click();
  }

  getExpandedData() {
    return element.all(by.css('.accordionContainer > ul')).get(1);
  }

  getYearsDisplayed() {
    return element.all(by.css('.accordionContainer .accordionHeading'));
  }

  getYear(index) {
    return element.all(by.css('.accordionContainer > ul')).get(index).all(by.css('li'));
  }

  openSelectedYear(accordionOrder, yearOrder) {
    return element.all(by.css('.accordionContainer > ul > li:nth-child('+yearOrder+') a')).get(accordionOrder).click();
  }

  getSelectedPageHeading() {
    return element.all(by.css('strong')).get(0);
  }

  getBooksData() {
     return element.all(by.css('span.resultData'));
  }

  getPaginationLimit() {
    return element.all(by.css('paginationlimit'));
  }

  getJumpto() {
    return element.all(by.css('jumpto'));
  }

  getDisplayedBooksCount() {
    return element.all(by.css('article')).count();
  }

  getDisplayedLimit() {
    return element.all(by.css('paginationlimit select')).get(0).getAttribute('ng-reflect-model');
  }

  changeLimitValue() {
    let limit = element.all(by.css('paginationlimit option')).get(1);
    return limit.click();
  }

  getPaginationComponent() {
    return element.all(by.css('numberedpagination')).first();
  }

  getPaginationLink() {
    return this.getPaginationComponent().all(by.css('div>ul>li>a'));
  }

  getSelectedShowFilter() {
    return element.all(by.css('browsepbfilterbox input'));
  }

  deSelectedCheckBox() {
    //deselect the first thress show checkBoxes (APA Books, APA Handbooks, Classic Books)
    element.all(by.css('browsepbfilterbox input')).get(0).click();
    element.all(by.css('browsepbfilterbox input')).get(1).click();
    element.all(by.css('browsepbfilterbox input')).get(2).click();
  }

  private getSearchInput() {
    return this.getJumpto().first().element(by.css('input[name="jumpTo"]'));
  }

  getSearchInputText() {
    let input = this.getSearchInput();
    return input.getAttribute('value');
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  executeSearch() {
    let searchBtn = this.getJumpto().first().element(by.css('button'));
    return searchBtn.click();
  }

  selectBrowseByPublicationYear() {
    let newBrowse = element(by.id('bypublicationyear'));
     return newBrowse.click();
  }

  selectBrowseByLateseRelease() {
    let newBrowse = element(by.id('bylatestrelease'));
    return newBrowse.click();
  }

  getEmptyPage() {
    return element(by.css('.emptyState'));
  }

  getTitles() {
    return element.all(by.css('article a'));
  }

  openBookRecord() {
    let bookTitle = element.all(by.css('article a')).get(0);
    return bookTitle.click();
  }

  getBookUID() {
    return element.all(by.css('.bookUID')).get(0).getAttribute("textContent");
  }

  getBookPBID() {
    return element.all(by.css('.bookPBID')).get(0).getAttribute("textContent");
 }

  openTOCpage() {
    let bookChapters = element.all(by.css('.buttonBar>button')).get(0);
    return bookChapters.click();
  }

  getMyListCount() {
    return element(by.css('personalization .badge')).getText();
  }

  addBookToMyList() {
    return element.all(by.css('add-to-my-list button')).get(0).click();
  }

  removeBookToMyList() {
    return element.all(by.css('add-to-my-list button')).get(0).click();
  }

  getMyListLabel() {
    return element.all(by.css('add-to-my-list button .caption')).get(0).click();
  }

  removePBPermission() {
    this.navigateTo('/admin');
    element(by.id('PB')).click();
    element(by.id('PBC2001')).click();  //add book collection
    element(by.css('entitlements input[type=submit]')).click();
    this.navigateTo();
  }

  getAccordionRange() {
    return element.all(by.css('.accordionHeading strong'));
  }

  getDisplayedYear() {
    return element.all(by.css('.accordionContainer ul>li>span>a')).getAttribute("textContent");
  }

  revertPermissionChanges() {
    this.navigateTo('/admin');
    element(by.id('PB')).click();
    element(by.id('PBC2001')).click();  //remove book collection
    element(by.css('entitlements input[type=submit]')).click();
  }
}
