import { browser, element, by } from 'protractor';

export class BrowsePBReleaseDate {

  navigateTo(path?: string) {
    return (path && path.length) ? browser.get(path) : browser.get('/PsycBOOKS/releasedate');
  }

  getPageHeading() {
    let heading = element(by.tagName('h1'));
    return heading.getText();
  }

  getBreadcrumbsPageTitle() {
    return element.all(by.css('breadcrumbs a')).get(0);
  }

  getBreadcrumbsCompnentTitle() {
    return element.all(by.css('breadcrumbs a')).get(1);
  }

  getBreadcrumbsYear() {
    return element.all(by.css('breadcrumbs a')).get(2);
  }

  getComponentHeading() {
    return element(by.css('.browseHeading'));
  }

  expandYear(index) {
    return element.all(by.css('.accordionHeading > a')).get(index).click();
  }

  collapseYear(index) {
    return element.all(by.css('.accordionHeading > a')).get(index).click();
  }

  getExpandedData() {
    return element.all(by.css('.accordionContainer > ul')).get(1);
  }

  getYearsDisplayed() {
    return element.all(by.css('.accordionContainer .accordionHeading'));
  }

  getMonth(index) {
    return element.all(by.css('.accordionContainer > ul')).get(index).all(by.css('li'));
  }

  openSelectedMonth(accordionOrder, monthOrder) {
    return element(by.css('.accordionContainer:nth-child('+accordionOrder+') > ul > li:nth-child('+monthOrder+') a')).click();
  }

  getSelectedPageHeading() {
    return element.all(by.css('strong')).get(0);
  }

  getPaginationComponent() {
    return element.all(by.css('pagerpagination')).first();
  }

  getPaginationLink() {
    return this.getPaginationComponent().all(by.css('div>ul>li'));
  }

  getSelectedShowFilter() {
    return element.all(by.css('browsepbfilterbox input'));
  }

  deSelectedCheckBox() {
    //deselect the first thress show checkBoxes (APA Books, APA Handbooks, Classic Books)
    element.all(by.css('browsepbfilterbox input')).get(0).click();
    element.all(by.css('browsepbfilterbox input')).get(1).click();
    element.all(by.css('browsepbfilterbox input')).get(2).click();
  }

  getEmptyPage() {
    return element(by.css('.emptyState'));
  }

  selectBrowseByReleaseDate() {
    let newBrowse = element(by.id('byreleasedate'));
     return newBrowse.click();
  }

  selectBrowseByLateseRelease() {
    let newBrowse = element(by.id('bylatestrelease'));
    return newBrowse.click();
  }

  openBookRecord() {
    let bookTitle = element.all(by.css('article a')).get(0);
    return bookTitle.click();
  }

  getBookUID() {
    return element.all(by.css('.bookUID')).get(0).getAttribute("textContent");
  }

  getBookPBID() {
    return element.all(by.css('.bookPBID')).get(0).getAttribute("textContent");
 }

  openTOCpage() {
    let bookChapters = element.all(by.css('.buttonBar>button')).get(0);
    return bookChapters.click();
  }

  getMyListCount() {
    return element(by.css('personalization .badge')).getText();
  }

  addBookToMyList() {
    return element.all(by.css('add-to-my-list button')).get(0).click();
  }

  removeBookToMyList() {
    return element.all(by.css('add-to-my-list button')).get(0).click();
  }

  getMyListLabel() {
    return element.all(by.css('add-to-my-list button .caption')).get(0).click();
  }

  removePBPermission() {
    this.navigateTo('/admin');
    element(by.id('PB')).click();
    element(by.id('PBC2001')).click();  //add book collection
    element(by.css('entitlements input[type=submit]')).click();
    this.navigateTo();
  }

  revertPermissionChanges() {
    this.navigateTo('/admin');
    element(by.id('PB')).click();
    element(by.id('PBC2001')).click();  //remove book collection
    element(by.css('entitlements input[type=submit]')).click();
  }
}
