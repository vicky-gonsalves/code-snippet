import { BrowsePBReleaseDate } from './browsePBReleaseDate.po';
import { browser } from 'protractor';

describe('Browse PsycBook Release Date Page', function() {
  let page: BrowsePBReleaseDate;

  beforeEach(() => {
    browser.manage().timeouts().implicitlyWait(9000);
    page = new BrowsePBReleaseDate();
    page.navigateTo();
  });

  it('should have PsycBOOKS heading', () => {
    expect(page.getPageHeading()).toEqual('PsycBOOKS');
  });

  it('should have \'Browse by Release Date\' in breadcrumbs', () => {
    expect(page.getBreadcrumbsCompnentTitle().getText()).toBe('Browse by Release Date');
  });

  it('should have \'Browse by Release Date\' heading', () => {
    let header = page.getComponentHeading();
    expect(header.isPresent()).toBe(true);
    expect(header.getText()).toContain('Browse by Release Date');
  });

  it('should expand and collapse any year', () => {
    page.expandYear(1);
    browser.sleep(1000);
    expect(page.getExpandedData().isPresent()).toBe(true);
    page.collapseYear(1);
    browser.sleep(1000);
    expect(page.getExpandedData().isPresent()).toBe(false);
  });

   it('should display max 12 list of months in each year', () => {
     page.collapseYear(0);
     page.getYearsDisplayed().each((accordion, index) => {
      accordion.click().then(() => {
        browser.sleep(1500);
        let count = page.getMonth(index).count();
        expect(count).toBeLessThan(13)
      });
    });
  });

  it('should open the selected month (January) at a specific year (2017) correctly', () => {
    page.openSelectedMonth(1,1); //open the first month at the first year (2017)
    expect(browser.getCurrentUrl()).toContain('2017/january');
    browser.sleep(1500);
    expect(page.getBreadcrumbsYear().getText()).toBe('2017 January');
    expect(page.getSelectedPageHeading().getText()).toContain('2017 - January');
  });

  it('should display pagination component if records are more than 25 at the selected year - month (2014 July)', () => {
    let totalItems: number;
    let limit: number;
    page.expandYear(3);
    browser.sleep(1500);
    page.openSelectedMonth(4,7);
      limit = 25;
      page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records=> {
        totalItems = parseInt(records);
        expect(!!page.getPaginationLink().count()).toBe(totalItems > limit);
      });
  });

  it('all show checkbox should be selected at the first at the selected year - month (2017 January)', () => {
    page.openSelectedMonth(1,1); //open the first month at the first year (2017)
    page.getSelectedShowFilter().each( btn => {
      expect(btn.getAttribute('checked')).toBe('true');
     });
  });

  it('should display \'No record found\' when "Encyclopedia" checkbox is slected only at the selected year - month (2017 January)', () => {
    page.openSelectedMonth(1,1); //open the first month at the first year (2017)
    page.deSelectedCheckBox();
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should navigate correct', () => {
    page.selectBrowseByLateseRelease().then(() => {
        browser.wait(page.selectBrowseByReleaseDate(), 1000).then(() => {
         expect(browser.getCurrentUrl()).toContain('/releasedate');
      });
    });
  });

  it('should open the correct record page at the selected year-month (2017 January)', () => {
    page.openSelectedMonth(1,1); //open the first month at the first year (2017)
    page.getBookUID().then(uid => {
      page.openBookRecord();
      browser.sleep(1000);
      expect(browser.getCurrentUrl()).toContain('/record/'+ uid);
    });
  });

  it('should open the correct TOC page at the selected year - month (2017 January)', () => {
    page.openSelectedMonth(1,1); //open the first month at the first year (2017)
    page.getBookPBID().then(pbid => {
      page.openTOCpage();
      expect(browser.getCurrentUrl()).toContain('/toc/'+ pbid);
    });
  });

  it('should add book to My List correctly at the selected year - month (2017 January)', () => {
    let countBefore, countAfter;
    page.openSelectedMonth(1,1); //open the first month at the first year (2017)
    browser.sleep(1000);
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.addBookToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore+1);
      });
    });
  });

  it('should remove book from My List correctly at the selected year - month (2017 January)', () => {
    let countBefore, countAfter;
    page.openSelectedMonth(1,1); //open the first month at the first year (2017)
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.removeBookToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore-1);
      });
    });
  });

  it('should display APA Books instead of PsycBOOKS at breadcrumbs if user has not PB access but has book collection Permission', () => {
    page.removePBPermission();
    browser.sleep(1000);
    expect(page.getBreadcrumbsPageTitle().getText()).toBe('APA Books');
  });

  afterAll(() =>  {
    page.revertPermissionChanges();
    browser.sleep(1000);
  });
});
