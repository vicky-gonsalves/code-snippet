import { browser, element, by } from 'protractor';

export class BrowsePBAuthor {

  navigateTo() {
    return browser.get('/PsycBOOKS/author');
  }

  getPageHeading() {
    let heading = element(by.tagName('h1'));
    return heading.getText();
  }

  getBreadcrumbs() {
    let breadcrumbs = element.all(by.css('breadcrumbs ol>li>a'));
    return breadcrumbs;
  }

 getBrowseAZ() {
    return element.all(by.css('browseaz'));
  }

  getPaginationLimit() {
    return element.all(by.css('paginationlimit'));
  }

  getJumpto() {
    return element.all(by.css('jumpto'));
  }

  getDisplayedBooksCount() {
    return element.all(by.css('.accordionContainer')).count();
  }

  getDisplayedLimit() {
    return element.all(by.css('paginationlimit select')).get(0).getAttribute('ng-reflect-model');
  }

  changeLimitValue() {
    let limit = element.all(by.css('paginationlimit option')).get(1);
    return limit.click();
  }

  getSelectedLetter(orderLetter) {
    let browseButton =  element.all(by.css('browseaz')).get(0).element(by.css('li:nth-child('+orderLetter+') a'));
    return browseButton.click();
  }

  getPaginationComponent() {
    return element.all(by.css('termBasedPagination')).first();
  }

  getPaginationLink() {
    return this.getPaginationComponent().all(by.css('div>ul>li>a'));
  }

  getAuthorNames() {
    return element.all(by.css('.accordionContainer strong'));
  }

  getEmptyPage() {
    return element(by.css('.emptyState'));
  }

  openSecondAuthorContent() {
    return element.all(by.css('.accordionHeading a')).get(1).click();
  }

  getAuthorContent() {
    return element.all(by.css('.accordionContainer')).get(1).element(by.css('ul'));
  }

  openBookRecord() {
    let bookTitle = element(by.css('.accordionContainer>ul>li a'));//element.all(by.css('.accordionContainer ul')).get(1).element(by.css('li:first-child a'));
    return bookTitle.click();
  }

  getBookUID() {
    return element.all(by.css('.bookUID')).get(0).getAttribute("textContent");
  }

  getBookPBID() {
    return element.all(by.css('.bookPBID')).get(0).getAttribute("textContent");
 }

  openTOCpage() {
    let bookChapters = element.all(by.css('.buttonBar button')).get(0);
    return bookChapters.click();
  }

  getSelectedShowFilter() {
    return element.all(by.css('browsepbfilterbox input'));
  }

  deSelectedCheckBox() {
    //deselect the first thress show checkBoxes (APA Books, APA Handbooks, Encyclopedias)
    element.all(by.css('browsepbfilterbox input')).get(0).click();
    element.all(by.css('browsepbfilterbox input')).get(1).click();
    element.all(by.css('browsepbfilterbox input')).get(2).click();
  }

  private getSearchInput() {
    return this.getJumpto().first().element(by.css('input[name="jumpTo"]'));
  }

  getSearchInputText() {
    let input = this.getSearchInput();
    return input.getAttribute('value');
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  executeSearch() {
    let searchBtn = this.getJumpto().first().element(by.css('button'));
    return searchBtn.click();
  }

  selectBrowseByAuthor() {
    let newBrowse = element(by.id('byauthor'));
     return newBrowse.click();
  }

  selectBrowseByLateseRelease() {
    let newBrowse = element(by.id('bylatestrelease'));
    return newBrowse.click();
  }

  getMyListCount() {
    return element(by.css('personalization .badge')).getText();
  }

  addBookToMyList() {
    return element.all(by.css('add-to-my-list button')).get(0).click();
  }

  getMyListLabel() {
    return element.all(by.css('add-to-my-list button .caption')).get(0).click();
  }

  getNextPageLabel() {
    return element.all(by.css('termBasedPagination ul li:first-child a span:last-child')).get(1).getText();
  }

  navigateToNextPage() {
    return element.all(by.css('termBasedPagination ul li:first-child a')).get(1).click();
  }

  getPrevPageLabel() {
    return element.all(by.css('termBasedPagination ul li:first-child a span:first-child')).get(1).getText();
  }

  navigateToPrevPage() {
    return element.all(by.css('termBasedPagination ul li:first-child a')).get(1).click();
  }
}
