import { BrowsePBAuthor } from './browsePBAuthor.po';
import { browser } from 'protractor';

describe('Browse PsycBook Author Page', function() {
  let page: BrowsePBAuthor;

  beforeEach(() => {
    page = new BrowsePBAuthor();
    page.navigateTo();
    browser.manage().timeouts().implicitlyWait(9000);
  });

  it('should have PsycBOOKS heading', () => {
    expect(page.getPageHeading()).toEqual('PsycBOOKS');
  });

 it('should have \'Browse by Author\' in breadcrumbs', () => {
    let breadcrumbs = page.getBreadcrumbs();
    browser.sleep(1000);
    expect(breadcrumbs.get(0).getText()).toBe('PsycBOOKS');
    expect(breadcrumbs.get(1).getText()).toBe('Browse by Author');
  });

  it('should have browseaz component', () => {
    expect(page.getBrowseAZ().count()).toBeGreaterThan(0);
  });

  it('should have paginationLimit component', () => {
    expect(page.getPaginationLimit().count()).toBeGreaterThan(0);
  });

  it('should have jumpto component', () => {
    expect(page.getJumpto().count()).toBeGreaterThan(0);
  });

  it('should display correct number of elements based on the selected limit at first loading', () => {
    let limit: number;
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedBooksCount().then(count => {
        expect(count).toEqual(limit);
      });
    });
  });

  it('should display pagination component if records are more than display limit', () => {
    let totalItems: number;
    let limit: number;
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l=> {
      limit = parseInt(_l);
      page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records=> {
        totalItems = parseInt(records);
        expect(!!page.getPaginationLink().count()).toBe(totalItems > limit);
      });
    });
  });

  it('should not display pagination component when letter Q is selected from the alphabet strip', () => {
    let totalItems: number;
    let limit: number;
    page.getSelectedLetter(17); //Select letter Q from the alphabet strip
    expect(page.getPaginationComponent().isDisplayed()).toBe(false);
  });

  it('should display records starting with B when letter B is selected from the alphabet strip', () => {
    page.getSelectedLetter(2);
    browser.sleep(1000);
    page.getAuthorNames().each(author=> {
      author.getText().then(name=> {
        expect(name.charAt(0)).toBe('B');
      });
    });
  });

  it('should display No record found when letter X is selected from the alphabet strip', () => {
    page.getSelectedLetter(24); //Select letter X from the alphabet strip
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should display correct number of elements when the limit change to be 50', () => {
    let limit: number;
    page.changeLimitValue();
    browser.sleep(1000);
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedBooksCount().then(count => {
        expect(+count).toEqual(limit);
      });
    });
  });

  it('should load content for second Author at first page (Abbot, Francis Ellingwood) when is clicked', () => {
    page.openSecondAuthorContent();
    expect(page.getAuthorContent()).toBeTruthy();
  });

  it('should open the correct record page', () => {
    page.getBookUID().then(uid => {
      page.openBookRecord();
      browser.sleep(1000);
      expect(browser.getCurrentUrl()).toContain('/record/'+ uid);
    });
  });

  it('should open the correct TOC page', () => {
    page.getBookPBID().then(pbid => {
      page.openTOCpage();
      expect(browser.getCurrentUrl()).toContain('/toc/'+ pbid);
    });
  });

  it('all show checkbox should be selected at the first', () => {
    page.getSelectedShowFilter().each( btn => {
      expect(btn.getAttribute('checked')).toBe('true');
     });
  });

  it('should display three records when "Encyclopedia" checkbox is slected only and letter O', () => {
    page.deSelectedCheckBox();
    page.getSelectedLetter(15); //Select letter O from the alphabet strip
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should search record by jumpto input', () => {
   let searchFor = 'john';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(1000);
    page.getAuthorNames().each(author=> {
      author.getText().then(name=> {
        expect(name.toLowerCase().indexOf(searchFor)).toBe(0);
      });
    });
  });

  it('should dipslay No record found  when user enters \'test\' in the jumpto input', () => {
    let searchFor = 'test';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    browser.wait(page.executeSearch(),9000).then(()=> {
      expect(page.getEmptyPage().isDisplayed()).toBe(true);
    });
  });

  it('should navigate correct', () => {
    page.selectBrowseByLateseRelease().then(() => {
        browser.wait(page.selectBrowseByAuthor(), 1000).then(() => {
         expect(browser.getCurrentUrl()).toContain('/author');
      });
    });
  });

  it('should add book to My List correctly', () => {
    let countBefore, countAfter;
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      browser.wait(page.addBookToMyList(),3000).then(() => {
        page.getMyListCount().then(countA => {
          countAfter = parseInt(countA);
          expect(countAfter).toEqual(countBefore+1);
        });
      });
    });
  });

  it('should remove book from My List correctly', () => {
    let countBefore, countAfter;
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      browser.wait(page.addBookToMyList(),3000).then(() => {
        page.getMyListCount().then(countA => {
          countAfter = parseInt(countA);
           expect(countAfter).toEqual(countBefore-1);
        });
      });
    });
  });

  it('should navigate to the next page correctly', () => {
    let nextRecord;
    page.getNextPageLabel().then(text => {
      nextRecord = text;
      browser.wait(page.navigateToNextPage(),3000).then(() => {
        page.getAuthorNames().first().getText().then(name => {
          expect(name).toContain(nextRecord);
        });
      });
    });
  });

  it('should navigate to the previous page correctly', () => {
    let prevLabel;
    browser.wait(page.navigateToNextPage(),3000).then(() => {
      page.getPrevPageLabel().then(text => {
        prevLabel = text;
        browser.wait(page.navigateToPrevPage(),3000).then(() => {
          page.getAuthorNames().last().getText().then(name => {
            expect(name).toContain(prevLabel);
          });
        });
      });
    });
  });
});

