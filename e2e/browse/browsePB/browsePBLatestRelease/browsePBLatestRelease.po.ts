import { browser, element, by } from 'protractor';

export class BrowsePBLatestRelease {

  navigateTo() {
    return browser.get('/PsycBOOKS/latest');
  }

  getPageHeading() {
    return [element(by.css('.browseHeading1')), element(by.css('.browseHeading2'))];
  }

  getBottomHeading() {
    return [element(by.css('.browseBottomHeading2')), element(by.css('.browseBottomHeading2'))]
  }

 getBooksCount() {
    return element(by.css('.origTotalItems')).getText();
  }

  getDisplayedBooksCount() {
    return element.all(by.css('article')).count();
  }

  openBookRecord() {
    let bookTitle = element.all(by.css('article span a')).get(0);
    return bookTitle.click();
  }

  getBookUID() {
    return element.all(by.css('.bookUID')).get(0).getAttribute("textContent");
  }

 openTOCpage() {
    let bookChapters = element.all(by.css('.buttonBar button')).get(0);
     return bookChapters.click();
 }

 getBookPBID() {
   return element.all(by.css('.bookPBID')).get(0).getAttribute("textContent");
 }

 getSelectedShowFilter() {
   return element.all(by.css('browsepbfilterbox input'));
 }

 deSelectedFirstCheckBox() {
   //deselect the first show checkBox (APA Books)
   let unselectedItem = element.all(by.css('browsepbfilterbox input')).get(0);
   return unselectedItem.click();
 }

  getEmptyPage() {
    return element(by.css('.emptyState'));
  }

  selectBrowseByAuthor() {
    let newBrowse = element(by.id('byauthor'));
     return newBrowse.click();
  }

  selectBrowseByLateseRelease() {
    let newBrowse = element(by.id('bylatestrelease'));
    return newBrowse.click();
  }
}
