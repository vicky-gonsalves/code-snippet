import { BrowsePBLatestRelease } from './browsePBLatestRelease.po';
import { browser } from 'protractor';


describe('Browse PsycBook Latest Release Page', function() {
  let page: BrowsePBLatestRelease;

  beforeEach(() => {
    browser.manage().timeouts().implicitlyWait(9000);
    page = new BrowsePBLatestRelease();
    page.navigateTo();
  });

  it('should have heading contains Latest Release', () => {
    let headers = page.getPageHeading();
    //expect(headers[0].isPresent()).toBe(false);
    expect(headers[1].isPresent()).toBe(true);
    expect(headers[1].getText()).toContain('Latest Release');
  });

  it('should not have bottom heading for 1 record', () => {
    let bottomHeaders = page.getBottomHeading();
    expect(bottomHeaders[0].isDisplayed()).toBe(false);
    expect(bottomHeaders[1].isDisplayed()).toBe(false);
  });

  it('should contain correct number of elements displayed', () => {
    let journaliItemsCount: number;
    page.getBooksCount().then(count => {
      journaliItemsCount = parseInt(count);
    });
    page.getDisplayedBooksCount().then(count => {
      expect(count).toEqual(journaliItemsCount);
    });
  });

  it('should open the correct record page', () => {
    page.getBookUID().then(uid => {
      browser.wait(page.openBookRecord(), 1000).then(() => {
        let fullUrl ='/record/'+ uid;
        expect(browser.getCurrentUrl()).toContain(fullUrl);
      });
    });
  });

  it('should open the correct TOC page', () => {
    page.getBookPBID().then(pbid => {
      page.openTOCpage();
      let fullUrl ='/toc/'+ pbid;
      expect(browser.getCurrentUrl()).toContain(fullUrl);
    });
  });

   it('all show choeckbox should be selected at the first', () => {
     page.getSelectedShowFilter().each( btn => {
       expect(btn.getAttribute('checked')).toBe('true');
     });
  });

  it('should display no record found when "APA Books" checkbox deselected', () => {
   browser.wait(page.deSelectedFirstCheckBox(), 3000).then(() => {
      expect(page.getEmptyPage().isDisplayed()).toBe(true);
    });
  });

  it('should navigate correct', () => {
    page.selectBrowseByAuthor().then(() => {
        browser.wait(page.selectBrowseByLateseRelease(), 1000).then(() => {
         expect(browser.getCurrentUrl()).toContain('/latest');
      });
    });
  });
});
