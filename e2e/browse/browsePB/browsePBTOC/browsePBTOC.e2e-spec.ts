import { BrowsePBTOC } from './browsePBTOC.po';
import { browser } from 'protractor';

describe('Browse PsycBook TOC Page', function() {
  let page: BrowsePBTOC;

  beforeEach(() => {
    browser.manage().timeouts().implicitlyWait(9000);
    page = new BrowsePBTOC();
    page.navigateTo();
  });

  it('should have \'PsycBOOKS\' and book title in breadcrumbs', () => {
    browser.sleep(1000);
    page.getBreadcrumbs().get(0).getText().then( text => {
      expect(text).toEqual('PsycBOOKS');
    });

    page.getBreadcrumbs().get(1).getText().then( text => {
      page.getBookTitle().getText().then ( title => {
        text = text.replace(/\...$/g, "");
        expect(title).toContain(text);
      });
    });
  });

  it('should have book title', () => {
    expect(page.getBookTitle().isDisplayed()).toBe(true);
  });

  it('should have email button', () => {
    expect(page.getEmailButton().isDisplayed()).toBe(true);
  });

  it('should have share button', () => {
    expect(page.getShareButton().isDisplayed()).toBe(true);
  });

  it('should share content with google+', () => {
    page.openShareMenu();
    page.openGooglePlus();
    browser.sleep(1000);
    browser.getAllWindowHandles().then(function (handles) {
      let newWindowHandle = handles[1]; // this is a share window
      browser.switchTo().window(newWindowHandle).then(function () {
        browser.getCurrentUrl().then( url =>{
          let result = (url.indexOf('https://plus.google.com/share?url=') === 0) || (url.indexOf('https://accounts.google.com/ServiceLogin') === 0);
          expect(result).toBeTruthy()
        });
      });
      browser.driver.close();
      browser.switchTo().window(handles[0]);
    });
  });

  it('should have image placeholder', () => {
    page.getImage().isPresent().then( result => {
      if(result) {
        expect(page.getImage().isPresent()).toBe(true);
      }
      else {
        expect(page.getFigure().isPresent()).toBe(true);
      }
    });
  });

  it('should have citation and abstract', () => {
    expect(page.getCitation().count()).toBe(2);
  });

  it('should have Matter Links (Front and Back)', () => {
    expect(page.getMatterLinks().count()).toBe(2);
  });

  it('should open Front Matter', () => {
    let links= page.getMatterLinks();
    page.getBookUID().then(uid => {
      links.get(0).click();
      browser.getAllWindowHandles().then(function (handles) {
        let newWindowHandle = handles[1]; // this is the full text window
        browser.switchTo().window(newWindowHandle).then(function () {
          expect(browser.getCurrentUrl()).toContain('/fulltext/'+ uid +'-FRM.pdf');
        });
      browser.driver.close();
      browser.switchTo().window(handles[0]);
      });
    });
  });

  it('should open Back Matter', () => {
    let links= page.getMatterLinks();
    page.getBookUID().then(uid => {
      links.get(1).click();
      browser.getAllWindowHandles().then(function (handles) {
        let newWindowHandle = handles[1]; // this is the full text window
        browser.switchTo().window(newWindowHandle).then(function () {
          expect(browser.getCurrentUrl()).toContain('/fulltext/'+ uid +'-BKM.pdf');
        });
      browser.driver.close();
      browser.switchTo().window(handles[0]);
      });
    });
  });

  it('should have chapters heading', () => {
    expect(page.getChaptersHeading().getText()).toContain('Chapters');
  });

  it('should display all chapters', () => {
    let count: number;
    page.getChaptersCount().then(value => {
      count = parseInt(value);
      page.getDisplayedChaptersCount().then(displayedCount => {
        expect(displayedCount).toEqual(count);
      });
    });
  });

  it('should open the correct record when the name of chapter is clicked', () => {
    page.getChapterUID().then(uid => {
      page.openFirstChapterRecord();
      browser.sleep(1000);
      expect(browser.getCurrentUrl()).toContain('/record/'+ uid);
    });
  });

  it('should have pdf link', () => {
    expect(page.getPDFLink().isDisplayed()).toBe(true);
  });

  it('should open pdf', () => {
    page.getChapterUID().then(uid => {
      page.getPDFLink().click();
      browser.getAllWindowHandles().then(function (handles) {
        let newWindowHandle = handles[1]; // this is the full text window
        browser.switchTo().window(newWindowHandle).then(function () {
          expect(browser.getCurrentUrl()).toContain('/fulltext/'+ uid +'.pdf');
        });
      browser.driver.close();
      browser.switchTo().window(handles[0]);
      });
    });
  });

  it('should load abstract', () => {
    page.openAbstract();
    expect(page.getAbstractDetails().getText()).toBeTruthy();
  });

  it('should return user to search page if he has not PB access', () => {
    page.removePBPermission();
    browser.sleep(1500);
    expect(browser.getCurrentUrl()).toContain('/search');
  });

  afterAll(() =>  {
    page.revertPermissionChanges();
    browser.sleep(1000);
  });
});
