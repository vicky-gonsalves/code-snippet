import { browser, element, by } from 'protractor';

export class BrowsePBTOC {

  navigateTo(path?: string) {
    return (path && path.length) ? browser.get(path) : browser.get('/PsycBOOKS/toc/11916');
  }

  getBreadcrumbs() {
    return element.all(by.css('breadcrumbs a'));
  }

  getBookTitle() {
    return element(by.css('h3'));
  }

  getEmailButton() {
    return element(by.css('a.emailButton'));
  }

  getShareButton() {
    return element(by.css('.btn-group span'));
  }

  openShareMenu() {
    return element(by.id('social-dropdown')).click();
  }

  openGooglePlus() {
    return element(by.css('social a:nth-child(3)')).click();
  }

  getImage() {
    return element(by.css('img'));
  }

  getFigure() {
    return element(by.css('figure'));
  }

  getCitation() {
    return element.all(by.css('.description .resultData'));
  }

  getMatterLinks() {
    return element.all(by.css('.bookMatterLinks a'));
  }

  getChaptersHeading() {
    return element(by.css('h4'));
  }

  getBookUID() {
    return element.all(by.css('.bookUID')).get(0).getAttribute("textContent");
  }

  getChaptersCount() {
    return element(by.css('.chaptersCount')).getAttribute("textContent");;
  }

  getDisplayedChaptersCount() {
    return element.all(by.css('.chapterItem')).count();
  }

  openFirstChapterRecord() {
    return element.all(by.css('.chapterItem .link')).get(0).click();
  }

  getChapterUID() {
    return element.all(by.css('.chapterUID')).get(0).getAttribute("textContent");
  }

  getPDFLink() {
    return element.all(by.css('.buttonBar > a')).get(0);
  }

  openAbstract() {
    return element.all(by.css('.buttonBar > button')).get(0).click();
  }

  getAbstractDetails() {
    return  element.all(by.css('.buttonBar > button > span')).get(0)
  }

  removePBPermission() {
    this.navigateTo('/admin');
    element(by.id('PB')).click();
    element(by.css('entitlements input[type=submit]')).click();
    this.navigateTo();
  }

  revertPermissionChanges() {
    this.navigateTo('/admin');
    element(by.id('PB')).click();
    element(by.css('entitlements input[type=submit]')).click();
  }
}
