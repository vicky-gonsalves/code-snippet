import { BrowsePBTopic } from './browsePBTopic.po';
import { browser,element, by } from 'protractor';

describe('Browse PsycBook Topic Page', function() {
  let page: BrowsePBTopic;

  beforeEach(() => {
    browser.manage().timeouts().implicitlyWait(90000);
    page = new BrowsePBTopic();
    page.navigateTo();
  });

  it('should have PsycBOOKS heading', () => {
    expect(page.getPageHeading()).toEqual('PsycBOOKS');
  });

   it('should have \'Browse by Topic\' in breadcrumbs', () => {
    expect(page.getBreadcrumbsCompnentTitle().getText()).toBe('Browse by Topic');
  });

  it('should have browseaz component', () => {
    expect(page.getBrowseAZ().count()).toBeGreaterThan(0);
  });

  it('should have paginationLimit component', () => {
    expect(page.getPaginationLimit().count()).toBeGreaterThan(0);
  });

  it('should have jumpto component', () => {
    expect(page.getJumpto().count()).toBeGreaterThan(0);
  });

  it('should display correct number of elements based on the selected limit at first loading', () => {
    let limit: number;
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedTopicsCount().then(count => {
        expect(count).toEqual(limit);
      });
    });
  });

  it('should display pagination component if records are more than display limit', () => {
    let totalItems: number;
    let limit: number;
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l=> {
      limit = parseInt(_l);
      page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records=> {
        totalItems = parseInt(records);
        expect(!!page.getPaginationLink().count()).toBe(totalItems > limit);
      });
    });
  });

  it('should display correct number of elements when the limit change to be 50', () => {
    let limit: number;
    page.changeLimitValue();
    browser.sleep(1000);
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedTopicsCount().then(count => {
        expect(+count).toEqual(limit);
      });
    });
  });

  it('should not display pagination component when letter O is selected from the alphabet strip', () => {
    let totalItems: number;
    let limit: number;
    page.getSelectedLetter(15); //Select letter O from the alphabet strip
    expect(page.getPaginationComponent().isDisplayed()).toBe(false);
  });

  it('should display records starting with B when letter B is selected from the alphabet strip', () => {
    page.getSelectedLetter(2);
    browser.sleep(1000);
    page.getTopicNames().each(topic=> {
      topic.getText().then(name=> {
        expect(name.charAt(0)).toBe('B');
      });
    });
  });

  it('should display No record found when letter X is selected from the alphabet strip', () => {
    page.getSelectedLetter(24); //Select letter X from the alphabet strip
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should load content for first topic \'Academic Learning & Achievement \' when is clicked', () => {
    let title;
    page.openFirstTopic();
    page.getTopicContentTitle().then(text =>{
      expect(text).toContain('Academic Learning & Achievement');
    });
  });


  it('should open the correct record page at first topic', () => {
    page.openFirstTopic();
    page.getBookUID().then(uid => {
      page.openBookRecord();
      browser.sleep(1000);
      expect(browser.getCurrentUrl()).toContain('/record/'+ uid);
    });
  });

  it('should open the correct TOC page at first topic', () => {
    page.openFirstTopic();
    page.getBookPBID().then(pbid => {
      page.openTOCpage();
      expect(browser.getCurrentUrl()).toContain('/toc/'+ pbid);
    });
  });

  it('all show checkbox should be selected at the first', () => {
    page.getSelectedShowFilter().each( btn => {
      expect(btn.getAttribute('checked')).toBe('true');
     });
  });

  it('should display \'No record found\' when "Encyclopedia" checkbox is slected only and letter O', () => {
    page.deSelectedCheckBox();
    page.getSelectedLetter(15); //Select letter O from the alphabet strip
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should search topic by jumpto input', () => {
   let searchFor = 'eat';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(1000);
    page.getTopicNames().each(topic=> {
      topic.getText().then(name=> {
        expect(name.toLowerCase().indexOf(searchFor)).toBe(0);
      });
    });
  });

  it('should dipslay No record found  when user enters \'john\' in the jumpto input', () => {
    let searchFor = 'john';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    browser.wait(page.executeSearch(),9000).then(()=> {
      expect(page.getEmptyPage().isDisplayed()).toBe(true);
    });
  });

  it('should navigate correct', () => {
    page.selectBrowseByLateseRelease().then(() => {
        browser.wait(page.selectBrowseByTopic(), 1000).then(() => {
         expect(browser.getCurrentUrl()).toContain('/topic');
      });
    });
  });

  it('should add book to My List correctly', () => {
    let countBefore, countAfter;
    page.openFirstTopic();
    browser.sleep(1000);
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.addBookToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore+1);
      });
    });
  });

  it('should remove book from My List correctly', () => {
    let countBefore, countAfter;
    page.openFirstTopic();
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.addBookToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore-1);
      });
    });
  });

  it('should display APA Books instead of PsycBOOKS at breadcrumbs if user has not PB access but has book collection ', () => {
    page.removePBPermission();
    browser.sleep(1500);
    expect(page.getBreadcrumbsPageTitle().getText()).toBe('APA Books');
  });

  afterAll(() =>  {
    page.revertPermissionChanges();
    browser.sleep(1000);
  });
});
