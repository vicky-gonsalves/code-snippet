import {BrowsePTPage} from './browsePTTestName.po';
import {browser} from 'protractor';

describe('Browse PsycTESTS Page - Test Name', function () {
  let page: BrowsePTPage;

  beforeEach(() => {
    browser.manage().timeouts().implicitlyWait(90000);
    page = new BrowsePTPage();
    page.navigateTo();
  });

  it('should have breadcrumbs', () => {
    let bradcrumbs = page.getBreadcrumbs();
    expect(bradcrumbs.first().getText()).toEqual('PsycTESTS');
    expect(bradcrumbs.last().getText()).toEqual('Browse by Test Name and Acronym');
  });

  it('should have PsycTESTS heading', () => {
    expect(page.getPageHeading()).toEqual('PsycTESTS');
  });

  it('should have description', () => {
    expect(page.getPageDescription()).toEqual('Browse structured information about psychological tests and measures as well as full-text tests.');
  });

  it('should have BrowseBy filter heading', () => {
    expect(page.getBrowsePTByBoxHeading()).toEqual('Browse By');
  });

  it('should have BrowseBy filters', () => {
    expect(page.getBrowsePTByBoxFilters().count()).toEqual(5);
  });

  it('should have Show filter heading', () => {
    expect(page.getShowFilterHeading()).toEqual('Show');
  });

  it('should have Show filters', () => {
    expect(page.getShowFilters().count()).toEqual(2);
  });

  it('should have both show filters selected Initially', () => {
    expect(page.getShowFiltersInputs().count()).toEqual(2);
  });

  it('should show warning modal if none of the show filter is selected', () => {
    browser.wait(page.unSelectShowFilters(), 2000).then(() => {
      expect(page.getModal().count()).toBe(1);
    });
  });

  it('should have browseaz component', () => {
    expect(page.getBrowseAZ().count()).toBeGreaterThan(0);
  });

  it('should have paginationLimit component', () => {
    expect(page.getPaginationLimit().count()).toBeGreaterThan(0);
  });

  it('should have jumpto component', () => {
    expect(page.getJumpto().count()).toBeGreaterThan(0);
  });

  it('should display correct number of records', () => {
    let limit: number;
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l=> {
      limit = parseInt(_l);
      expect(page.getArticlesDisplayed().count()).toBe(limit);
    });
  });

  it('should display pagination component if records are more than display limit', () => {
    let totalItems: number;
    let limit: number;
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l=> {
      limit = parseInt(_l);
      page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records=> {
        totalItems = parseInt(records);
        expect(!!page.getPaginationLink().count()).toBe(totalItems > limit);
      });
    });
  });

  it('should display records starting with A when browseAZ clicked', () => {
    page.getBrowseAZButtons().first().click().then(()=> {
      page.getArticlesText().each(record=> {
        record.getText().then(text=> {
          expect(text.charAt(0).toLowerCase()).toBe('a');
        })
      })
    });
  });

  it('should display records starting with Z when browseAZ clicked', () => {
    page.getBrowseAZButtons().last().click().then(()=> {
      page.getArticlesText().each(record=> {
        record.getText().then(text=> {
          expect(text.charAt(0).toLowerCase()).toBe('z');
        })
      })
    });
  });

  it('should search record by jumpto input', () => {
    let searchFor = 'psychology';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    page.getArticlesText().each(record=> {
      record.getText().then(text=> {
        expect(text.toLowerCase().indexOf(searchFor)).toBe(0);
      })
    })
  });

  it('should have ptbuttonbar component', () => {
    let searchFor = 'bac-c';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    expect(page.getPtbuttonbar().count()).toBeTruthy();
  });

  it('should load summary', () => {
    let searchFor = 'bac-c';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    page.getSummary().click();
    expect(page.getSummaryDetails().getText()).toBeTruthy();
  });

  it('should load Test Versions', () => {
    let searchFor = 'bac-c';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    page.getTestVersions().click();
    expect(page.getTestVersionsDetails().count()).toBeTruthy();
  });

  it('should have pdf link', () => {
    let searchFor = 'bac-c';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    expect(page.getPDFLink().getText()).toEqual('(PDF)');
  });

  it('should have Add to My List Button', () => {
    let searchFor = 'bac-c';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    expect(page.getAddToMyList()).toBeTruthy();
  });

  it('should block non-entitled organizational users from viewing the page.', () => page.checkAccessIsDenied('organization'));
  it('should block individual users from viewing the page.', () => page.checkAccessIsDenied('individual'));
  it('should block guest users from viewing the page.', () => page.checkAccessIsDenied());
});
