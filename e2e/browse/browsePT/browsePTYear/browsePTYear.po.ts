import {browser, element, by} from 'protractor';

export class BrowsePTPage {
  navigateTo() {
    return browser.get('/PsycTESTS/pubyear');
  }

  getPageHeading() {
    let heading = element(by.css('browsepagesubheader h1'));
    return heading.getText();
  }

  getPageDescription() {
    let desc = element(by.css('browsepagesubheader p'));
    return desc.getText();
  }

  getBreadcrumbs() {
    let breadcrumbs = element.all(by.css('.breadcrumb li a'));
    return breadcrumbs;
  }

  getBrowsePTByBoxHeading() {
    let browseByHeading = element(by.css('browseptbybox .card h1'));
    return browseByHeading.getText();
  }

  getBrowsePTByBoxFilters() {
    return element.all(by.css('browseptbybox .card a span'));
  }

  getShowFilterHeading() {
    let showFiltersHeading = element(by.css('browseptfilterbox .card h1'));
    return showFiltersHeading.getText();
  }

  getShowFilters() {
    return element.all(by.css('browseptfilterbox .card div'));
  }

  getShowFiltersInputs() {
    return element.all(by.css('browseptfilterbox .card div input[type="checkbox"]:checked'));
  }

  unSelectShowFilters() {
    let filtersInputs = element.all(by.css('browseptfilterbox .card div input[type="checkbox"]'));
    filtersInputs.get(0).click();
    return filtersInputs.get(1).click();
  }

  getModal() {
    return element.all(by.css('browseptfilterbox .modal.active'));
  }

  getNavBar() {
    return element(by.css('.card strong'));
  }

  getViewToggle() {
    return element.all(by.css('.card viewtoggle'));
  }

  getViewToggleButton() {
    return this.getViewToggle().first().element(by.css('a'));
  }

  getAccordion() {
    return element.all(by.css('.accordionContainer'));
  }

  getList() {
    return element.all(by.css('.listView'));
  }

  getYearsDisplayed() {
    return element.all(by.css('.accordionContainer .accordionHeading'));
  }

  getYear(index) {
    return element.all(by.css('.smallView')).get(index).all(by.css('li'));
  }

  getFirstYear() {
    return this.getYear(0).first();
  }

  getArticlesText() {
    return element.all(by.css('.accordionContainer .accordionHeading>a>strong'));
  }

  getBrowseAZ() {
    return element.all(by.css('browseaz'));
  }

  getBrowseAZButtons() {
    return this.getBrowseAZ().first().all(by.css('div>ul>li>a'));
  }

  getPaginationLimit() {
    return element.all(by.css('paginationlimit'));
  }

  getPaginationComponent() {
    return element.all(by.css('termBasedPagination')).first();
  }

  getPaginationLink() {
    return this.getPaginationComponent().all(by.css('div>ul>li>a'));
  }

  getArticlesDisplayed() {
    return element.all(by.css('article'));
  }

  getPtbuttonbar() {
    return element.all(by.css('article')).first().all(by.css('ptbuttonbar'));
  }

  getSummary() {
    return this.getPtbuttonbar().first().all(by.css('div>a')).first();
  }

  getTestVersions() {
    return this.getPtbuttonbar().first().all(by.css('div>a')).get(1);
  }

  getPDFLink() {
    return this.getPtbuttonbar().first().all(by.css('div>a')).get(2).element(by.css('span'));
  }

  getAddToMyList() {
    return this.getPtbuttonbar().first().all(by.css('add-to-my-list'));
  }

  getSummaryDetails() {
    return this.getPtbuttonbar().first().element(by.css('.summary'));
  }

  getTestVersionsDetails() {
    return this.getPtbuttonbar().first().all(by.css('.testversions>article'));
  }

  getJumpto() {
    return element.all(by.css('jumpto'));
  }

  private getSearchInput() {
    return this.getJumpto().first().element(by.css('input[name="jumpTo"]'));
  }

  executeSearch() {
    let searchBtn = this.getJumpto().first().element(by.css('button'));
    return searchBtn.click();
  }

  getSearchInputText() {
    let input = this.getSearchInput();
    return input.getAttribute('value');
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  checkAccessIsDenied(role?: string) {
    if(role === 'organization') {
      this.togglePTEntitlement();
    } else if (role === 'individual') {
      this.togglePTEntitlement();
      this.setUser('12345', '-1');
    } else {
      this.setUser('-1');
    }

    this.navigateTo();
    expect(element(by.css('no-access')).isDisplayed()).toBeTruthy();

    if(role === 'organization') {
      this.togglePTEntitlement();
    } else if (role === 'individual') {
      this.togglePTEntitlement();
      this.setUser('12345', '329008');
    } else {
      this.restoreDefaultUserFromGuest();
    }
  }

  private togglePTEntitlement() {
    browser.get('/admin');
    browser.sleep(2000);

    element(by.id('PT')).click();
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(2000);
  }

  private setUser(userId: string, orgId?: string) {
    browser.get('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys(userId);

    if (userId === '-1' || orgId === '-1') {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');
    } else if (orgId && orgId.length) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys(orgId);
    }

    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }

  private restoreDefaultUserFromGuest() {
    this.setUser('12345', '329008');

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
    browser.sleep(1000);
    for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
      element(by.id(thing)).click();
    }
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(2000);
  }
}
