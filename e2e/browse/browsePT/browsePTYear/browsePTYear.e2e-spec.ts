import {BrowsePTPage} from './browsePTYear.po';
import {browser} from 'protractor';

describe('Browse PsycTESTS Page - Year', function () {
  let page: BrowsePTPage;

  beforeEach(() => {
    page = new BrowsePTPage();
    page.navigateTo();
    browser.manage().timeouts().implicitlyWait(9000);
  });

  it('should have breadcrumbs', () => {
    let bradcrumbs = page.getBreadcrumbs();
    expect(bradcrumbs.first().getText()).toEqual('PsycTESTS');
    expect(bradcrumbs.last().getText()).toEqual('Browse by Year');
  });

  it('should have PsycTESTS heading', () => {
    expect(page.getPageHeading()).toEqual('PsycTESTS');
  });

  it('should have description', () => {
    expect(page.getPageDescription()).toEqual('Browse structured information about psychological tests and measures as well as full-text tests.');
  });

  it('should have BrowseBy filter heading', () => {
    expect(page.getBrowsePTByBoxHeading()).toEqual('Browse By');
  });

  it('should have BrowseBy filters', () => {
    expect(page.getBrowsePTByBoxFilters().count()).toEqual(5);
  });

  it('should have Show filter heading', () => {
    expect(page.getShowFilterHeading()).toEqual('Show');
  });

  it('should have Show filters', () => {
    expect(page.getShowFilters().count()).toEqual(2);
  });

  it('should have both show filters selected Initially', () => {
    expect(page.getShowFiltersInputs().count()).toEqual(2);
  });

  it('should show warning modal if none of the show filter is selected', () => {
    browser.wait(page.unSelectShowFilters(), 2000).then(() => {
      expect(page.getModal().count()).toBe(1);
    });
  });

  it('should have Browse by Year nav bar heading', () => {
    expect(page.getNavBar().getText()).toEqual('Browse by Year');
  });

  it('should have viewtoggle component', () => {
    expect(page.getViewToggle().count()).toBeGreaterThan(0);
  });

  it('should switch view if viewtoggle clicked', () => {
    expect(page.getAccordion().count()).toBeGreaterThan(0);
    page.getViewToggleButton().click().then(() => {
      expect(page.getAccordion().count()).toBe(0);
      expect(page.getList().count()).toBeGreaterThan(0);
      page.getViewToggleButton().click().then(() => {
        expect(page.getAccordion().count()).toBeGreaterThan(0);
        expect(page.getList().count()).toBe(0);
      })
    })
  });

  it('should display max 10 list of years in each accordion', () => {
    page.getYearsDisplayed().each((accordion, index) => {
      if (index == 0) {
        accordion.click(); //closing first accordion;
      }
      accordion.click().then(() => {
        let count = page.getYear(index).count();
        expect(count).toBeLessThan(11)
      })
    })
  });

  it('should open the correct page when year is clicked', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
      })
    })
  });

  it('should have browseaz component', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
        expect(page.getBrowseAZ().count()).toBeGreaterThan(0);
      })
    })
  });

  it('should have paginationLimit component', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
        expect(page.getPaginationLimit().count()).toBeGreaterThan(0);
      })
    })
  });

  it('should have jumpto component', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
        expect(page.getJumpto().count()).toBeGreaterThan(0);
      })
    });
  });

  it('should display correct number of records', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
        let limit: number;
        page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l => {
          limit = parseInt(_l);
          expect(page.getArticlesDisplayed().count()).toBe(limit);
        });
      })
    })
  });

  it('should display pagination component if records are more than display limit', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
        let totalItems: number;
        let limit: number;
        page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l => {
          limit = parseInt(_l);
          page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records => {
            totalItems = parseInt(records);
            expect(!!page.getPaginationLink().count()).toBe(totalItems > limit);
          });
        });
      })
    })
  });

  it('should display records starting with A when browseAZ clicked', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
        page.getBrowseAZButtons().first().click().then(() => {
          page.getArticlesText().each(record => {
            record.getText().then(text => {
              expect(text.charAt(0).toLowerCase()).toBe('a');
            })
          })
        });
      })
    })
  });

  it('should display records starting with Z when browseAZ clicked', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
        browser.sleep(3000);
        page.getBrowseAZButtons().last().click().then(() => {
          page.getArticlesText().each(record => {
            record.getText().then(text => {
              expect(text.charAt(0).toLowerCase()).toBe('z');
            })
          })
        });
      })
    })
  });

  it('should search record by jumpto input', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
        browser.sleep(3000);
        let searchFor = 'Abusive Supervision Scale--Adapted';
        page.setSearchInputText(searchFor);
        expect(page.getSearchInputText()).toEqual(searchFor);
        page.executeSearch();
        page.getArticlesText().each(record => {
          record.getText().then(text => {
            expect(text.toLowerCase().indexOf(searchFor)).toBe(0);
          })
        })
      })
    })
  });

  it('should have ptbuttonbar component', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
        browser.sleep(3000);
        let searchFor = 'Abusive Supervision Scale--Adapted';
        page.setSearchInputText(searchFor);
        expect(page.getSearchInputText()).toEqual(searchFor);
        page.executeSearch();
        expect(page.getPtbuttonbar().count()).toBeTruthy();
      })
    })
  });

  it('should load summary', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
        browser.sleep(3000);
        let searchFor = 'Abusive Supervision Scale--Adapted';
        page.setSearchInputText(searchFor);
        expect(page.getSearchInputText()).toEqual(searchFor);
        page.executeSearch();
        page.getSummary().click();
        expect(page.getSummaryDetails().getText()).toBeTruthy();
      })
    })
  });

  it('should load Test Versions', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
        browser.sleep(3000);
        let searchFor = 'Abusive Supervision Scale--Adapted';
        page.setSearchInputText(searchFor);
        expect(page.getSearchInputText()).toEqual(searchFor);
        page.executeSearch();
        page.getTestVersions().click();
        expect(page.getTestVersionsDetails().count()).toBeTruthy();
      })
    })
  });

  it('should have pdf link', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
        browser.sleep(3000);
        let searchFor = 'Abusive Supervision Scale--Adapted';
        page.setSearchInputText(searchFor);
        expect(page.getSearchInputText()).toEqual(searchFor);
        page.executeSearch();
        expect(page.getPDFLink().getText()).toEqual('(PDF)');
      })
    })
  });

  it('should have Add to My List Button', () => {
    page.getFirstYear().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/\/pubyear\/\d{4}/g).test(url);
        expect(regexTest).toBe(true);
        browser.sleep(3000);
        let searchFor = 'Abusive Supervision Scale--Adapted';
        page.setSearchInputText(searchFor);
        expect(page.getSearchInputText()).toEqual(searchFor);
        page.executeSearch();
        expect(page.getAddToMyList()).toBeTruthy();
      })
    })
  });

  it('should block non-entitled organizational users from viewing the page.', () => page.checkAccessIsDenied('organization'));
  it('should block individual users from viewing the page.', () => page.checkAccessIsDenied('individual'));
  it('should block guest users from viewing the page.', () => page.checkAccessIsDenied());
});
