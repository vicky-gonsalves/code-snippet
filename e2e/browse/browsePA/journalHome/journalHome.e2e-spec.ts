import { browser } from 'protractor';
import { JournalHome } from './journalHome.po';

describe('Journal Home', () => {
  let page: JournalHome;

  beforeEach(() => {
    page = new JournalHome();
    page.navigateToJournal('amp');

    browser.manage().timeouts().implicitlyWait(5000);
  });

  it('should open the correct page.', () => {
    page.isInstanceDisplayed().then(isDisplayed => expect(isDisplayed).toBeTruthy());
  });

  it('should display volume and issue after loading the page.', () => {
    let matcher = /(?:\d{4}, )?Volume \d*, Issue \d*(, Suppl (\w*))?/g;

    // Wait until the initial journal issue articles are loaded, and the breadcrumb's (loading) message vanishes.
    browser.sleep(4000);

    let bcLinks = page.getBreadcrumbLinks();
    bcLinks.count().then(count => expect(count).toBe(3));

    bcLinks.first().getText().then(text => expect(text).toBe('PsycARTICLES'));
    bcLinks.last().getText().then(text => expect(text).toMatch(matcher));
    page.getIssueMeta().getText().then(text => expect(text).toMatch(matcher));
  });

  it('should use (Abstract) toggle button, within the button bar.', () => {
    page.toggleAbstract().getText().then(text => expect(text.toLowerCase()).toBe('abstract'));
    page.toggleAbstract().isPresent().then(isPresent => expect(isPresent).toBeFalsy);
  });

  it('should use (Impact Statement) toggle button, within the button bar.', () => {
    page.navigateToJournal('ccp');
    page.toggleImpactStatement().getText().then(text => expect(text.toLowerCase()).toBe('impact statement'));
    page.toggleImpactStatement().isPresent().then(isPresent => expect(isPresent).toBeFalsy);
  });

  it('should have a link that ends with (.pdf) for (PDF) button, within the button bar.', () => {
    let suffix = /.*\.pdf/g;
    page.getPdfButtonLink().then(link => expect(link.toLowerCase()).toMatch(suffix));
  });

  it('should resolve the (Full-Text HTML) link, within the button bar.', () => {
    page.navigateToFullTextHTML().then(instancesCount => expect(instancesCount).toBe(1));
  });

  it('should resolve the (Cited By) link, within the button bar.', () => {
    page.navigateToCitedBy().then(instancesCount => expect(instancesCount).toBe(1));
  });

  it('should have a correctly set (Permissions) link, within the button bar.', () => {
    let matcher = /http:\/\/rightslinkdev.apa.org\/index\.cfm\?uid=(\d{4}-\d{5}-\d{3})&docType=j/g;
    page.getPermissionsLink().then(link => expect(link).toMatch(matcher));
  });

  it('should have a working (Add To My List) button, within the button bar.', () => {
    page.navigateToJournal('ror');

    // Add article.
    page.getBadgeValue().then(beforeAdd => {
      page.toggleAddToMyList();
      page.getBadgeValue().then(afterAdd => {
        expect(+afterAdd).toBe(+beforeAdd + 1);

        // Remove article.
        page.toggleAddToMyList();
        page.getBadgeValue().then(afterRemove => expect(+afterRemove).toBe(+afterAdd - 1));
      });
    });
  });

  it('should have a title link, that resolves to the detailed record of the article.', () => {
    page.navigateToRecordDetail().then(instancesCount => expect(instancesCount).toBe(1));
  });

  it('should not perform a search within journal, without a search term.', () => {
    let fq = page.performSearchWithinJournal({});
    fq.isPresent().then(isPresent => {
      expect(isPresent).toBeFalsy();
    });
  });

  it('should perform a search within journal correctly.', () => {
    let search = {
      term: 'stress',
      issueType: 2, // Online-First
      fulltext: true
    };

    let fq = page.performSearchWithinJournal(search);
    fq.getText().then(text => {
      expect(text).toContain(search.term);
      expect(text).toContain('IsOFP: true'); // Based on the issue type
    });
  });

  it('should access a journal snapshot, which does not require clicking the (more) link.', () => {
    page.openBasicSnapshot(success => expect(success).toBeTruthy());
  });

  it('should access a journal snapshot, which is listed after clicking the (more) link.', () => {
    page.openMoreSnapshot(success => expect(success).toBeTruthy());
  });

  it('should change the current volume / issue.', () => {
    page.getIssueMeta().getText().then(beforeText => {
      page.changeVolumeAndIssue(2, 3);
      page.getIssueMeta().getText().then(afterText => {
        expect(beforeText.length).toBeGreaterThan(0);
        expect(afterText.length).toBeGreaterThan(0);
        expect(beforeText === afterText).toBeFalsy();
      });
    });
  });

  it('should display the Journal History, within the (About the Journal) section.', () => {
    page.getJournalHistory().getText().then(text => expect(text).toBe('Canadian Journal of Psychology/Revue canadienne de psychologie, Vols. 1-46, 1947-1992'));
  });

  it('should use the breadcrumb middle link to navigate to the journal initial view.', () => {
    let matcher = /(?:\d{4}, )?Volume \d*, Issue \d*(, Suppl (\w*))?/g;

    page.goToMostDownloaded();
    page.getBreadcrumbLinks().last().getText().then(text => {
      expect(text.toLowerCase()).toContain('most downloaded');
      page.getBreadcrumbLinks().get(1).click();
      page.getBreadcrumbLinks().last().getText().then(text => expect(text).toMatch(matcher));
    });
  });

  it('should toggle the facets (left-side) pane.', () => {
    // Toggle it off.
    page.toggleFacetsPane().then(isDisplayed => {
      expect(isDisplayed).toBeFalsy();

      // Toggle it back on.
      page.toggleFacetsPane().then(isDisplayed => expect(isDisplayed).toBeTruthy());
    });
  });

  it('should display special issue header.', () => {
    let items = page.getSpecialIssue();
    items[0].getText().then(text => expect(text).toMatch(/Special Issue: .*/g));
    items[1].getText().then(text => expect(text.length).toBeGreaterThan(0));
  });

  it('should display online-first articles.', () => {
    let onlinefirst = page.displayOnlineFirst();

    // At the time of writing this case: 40 records were available within (ort) journal.
    // Two cases are available here:
    // First case: pagination limit = 25, with pagination.
    // Second case: pagination limit = 50, no pagination.

    // Check for pagination existence.
    onlinefirst[0].pagersCount.then(count => expect(count).toBeGreaterThan(0));
    onlinefirst[1].pagersCount.then(count => expect(count).toBe(0));

    // Check for the compliance of displayed records count with the pagination limit.
    onlinefirst[0].pageItemsCount[0].then(p1Count => {
      expect(p1Count).toBeLessThan(1 + onlinefirst[0].limit);

      onlinefirst[0].pageItemsCount[1].then(p2Count => {
        expect(p2Count).toBeLessThan(1 + onlinefirst[0].limit);

        onlinefirst[1].pageItemsCount[0].then(allCount => {
          expect(allCount).toBeLessThan(1 + onlinefirst[1].limit);

          // All records should be displayed, regardless of the pagination limit.
          expect(p1Count + p2Count).toBe(allCount);
        });
      });
    });
  });

  it('should have an actionable top button bar.', () => {
    // Link buttons
    let links = page.getButtonBarLinks();
    links.email.then(emailLink => expect(emailLink.substring(emailLink.indexOf('mailto'))).toMatch(/mailto:\?(?:subject=.*){1}&(?:body=.*){1}?/g));
    links.rss.then(rssLink => expect(rssLink).toMatch(/http:\/\/content.apa.org\/journals\/\w{3}.rss/g));

    // TODO: rewrite the alerts test case, after its action is implemented!
    console.log('NOTICE: the alerts action within (Journal Home) is not implemented!');

    // Social buttons
    let socials = ['facebook', 'twitter', 'plus'];
    for (let item of socials) {
      page.activateSocialLink(item, success => expect(success).toBeTruthy());
    }
  });

  it('should display this page for non-authenticated users.', () => {
    page.setUserAsAnonymous();
    page.navigateToJournal('amp');
    page.isInstancePresent().then(isPresent => {
      expect(isPresent).toBeTruthy();

      // Revert user changes before ending.
      page.setUserAsAnonymous(true);
    });
  });

  it('should access this page when the user has a print subscription.', () => {
    page.toggleEntitlements();

    page.navigateToJournal('arc');
    page.isInstanceDisplayed().then(isDisplayed => {
      expect(isDisplayed).toBeTruthy();

      page.navigateToJournal('zsp');
      page.isInstanceDisplayed().then(isDisplayed => {
        expect(isDisplayed).toBeTruthy();

        // Revert entitlements changes before ending.
        page.toggleEntitlements();
      });
    });
  });
});
