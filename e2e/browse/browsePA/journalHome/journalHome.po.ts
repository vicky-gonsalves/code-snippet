import { browser, element, by } from 'protractor';

export class JournalHome {
  navigate(path: string) {
    return browser.get(path);
  }

  navigateToJournal(code) {
    return browser.get('/PsycARTICLES/journal/' + code);
  }

  isInstanceDisplayed() {
    return element(by.css('journalhome')).isDisplayed();
  }

  isInstancePresent() {
    return element(by.css('journalhome')).isPresent();
  }

  getBreadcrumbLinks() {
    return element.all(by.css('breadcrumbs a'));
  }

  getIssueMeta() {
    return element(by.css('browsebyissue strong'));
  }

  toggleAbstract() {
    element.all(by.css('.abstractLink')).first().click();
    return element(by.css('journalhome .resultData strong'));
  }

  toggleImpactStatement() {
    element.all(by.css('.impactStatementLink')).first().click();
    return element(by.css('journalhome .resultData strong'));
  }

  getPdfButtonLink() {
    return element.all(by.css('.fullTextLink')).first().getAttribute('href');
  }

  navigateToFullTextHTML() {
    element.all(by.css('.fullTextHTMLLink')).first().click();
    return element.all(by.css('fullTextHTML')).count();
  }

  navigateToCitedBy() {
    // Wait until the initial journal issue articles are loaded, before using the (Most Cited) snapshot.
    browser.sleep(4000);

    element(by.css('journalsnapshotbox .accordionContainer')).click();
    element(by.css('journalsnapshotbox .basic-links a:nth-child(2)')).click();
    element.all(by.css('.citedByLink')).first().click();
    return element.all(by.css('cited-by')).count();
  }

  getPermissionsLink() {
    return element.all(by.css('.permissionsLink')).first().getAttribute('href');
  }

  toggleAddToMyList() {
    element.all(by.css('add-to-my-list')).first().click();

    // Wait until the action is persisted.
    browser.sleep(2000);
  }

  getBadgeValue() {
    return element(by.css('.navigation-menu .badge')).getText();
  }

  navigateToRecordDetail() {
    element.all(by.css('.article-title')).first().click();
    return element.all(by.css('recorddisplay')).count();
  }

  performSearchWithinJournal(search) {
    if (search) {
      if (search.term) {
        element(by.id('term')).sendKeys(search.term);
      }
      if (search.issueType) {
        element(by.id('issueType')).click();
        element(by.id('issueType')).element(by.css('option:nth-child(' + search.issueType + ')')).click();
      }
      if (search.fulltext) {
        element(by.id('fulltext_search')).click();
      }
    }

    element(by.css('form button')).click();
    return element(by.css('searchresults friendly-query'));
  }

  openBasicSnapshot(cb) {
    // Wait until the initial journal issue articles are loaded, before using the (Most Downloaded) snapshot.
    browser.sleep(4000);

    element(by.css('journalsnapshotbox .accordionContainer')).click();
    element(by.css('journalsnapshotbox .basic-links a:first-child')).click();

    element.all(by.css('browsebymostdl article')).count().then(firstCount => {
      element(by.css('browsebymostdl paginationlimit')).click();
      element(by.css('browsebymostdl paginationlimit option:nth-child(2)')).click();

      element.all(by.css('browsebymostdl article')).count().then(secondCount => {
        let success = (firstCount <= 5) && (secondCount <= 10);
        cb(success);
      });
    });
  }

  openMoreSnapshot(cb) {
    // Wait until the initial journal issue articles are loaded, before using the (Most Frequent PsycINFO Classification Codes) snapshot.
    browser.sleep(4000);

    element(by.css('journalsnapshotbox .accordionContainer')).click();
    element(by.css('journalsnapshotbox .more')).click();
    element(by.css('journalsnapshotbox .more-links a:last-child')).click();

    element.all(by.css('browsebymostfreqclassification .record a')).count().then(firstCount => {
      element(by.css('browsebymostfreqclassification paginationlimit')).click();
      element(by.css('browsebymostfreqclassification paginationlimit option:last-child')).click();
      element.all(by.css('browsebymostfreqclassification .record a')).count().then(lastCount => {
        let success = (firstCount <= 5) && (lastCount <= 25);
        cb(success);
      });
    });
  }

  changeVolumeAndIssue(volumeItem, issueItem) {
    let volume = element(by.css('.volume-wrapper:nth-child(' + volumeItem + ')'));
    volume.element(by.css('.accordionHeading a')).click();
    volume.element(by.css('.issue-item:nth-child(' + issueItem + ') a')).click();
  }

  getJournalHistory() {
    this.navigateToJournal('cep');
    element(by.css('journalhistory')).click();
    return element(by.css('journalhistory li'));
  }

  goToMostDownloaded() {
    // Wait until the initial journal issue articles are loaded, before using the (Most Downloaded) snapshot.
    browser.sleep(4000);

    element(by.css('journalsnapshotbox .accordionContainer')).click();
    element(by.css('journalsnapshotbox .basic-links a:first-child')).click();
  }

  toggleFacetsPane() {
    element(by.css('.facet-toggle')).click();
    return element(by.css('.facets')).isDisplayed();
  }

  getSpecialIssue() {
    this.navigateToJournal('ort');
    this.changeVolumeAndIssue(21, 1);
    return [element(by.css('journalspecialissuebar h4')), element(by.css('journalspecialissuebar h4 span:first-child'))];
  }

  displayOnlineFirst() {
    this.navigateToJournal('ort');
    element(by.css('onlinefirstbox a')).click();

    let onlineFirst = element(by.css('browsebyonlinefirst'));

    // The records limit is (25), and pagination is enabled, store the relevant details.
    let view25 = {
      limit: 25,
      pagersCount: onlineFirst.all(by.css('numberedpagination div')).count(),
      pageItemsCount: [onlineFirst.all(by.css('journalrecordlist article')).count()]
    };

    // The records limit is (25): go to page 2, and push its items count.
    onlineFirst.all(by.css('numberedpagination:nth-child(2) li:nth-child(2)')).click();
    view25.pageItemsCount.push(onlineFirst.all(by.css('journalrecordlist article')).count());


    // Change the records limit to 50, and store the resulting changes.
    element(by.css('paginationlimit select')).click();
    element(by.css('paginationlimit select option:last-child')).click();

    let view50 = {
      limit: 50,
      pagersCount: onlineFirst.all(by.css('numberedpagination div')).count(),
      pageItemsCount: [onlineFirst.all(by.css('journalrecordlist article')).count()]
    };

    return [view25, view50];
  }

  getButtonBarLinks() {
    return {
      email: element(by.css('.buttonBar li:first-child > a')).getAttribute('href'),
      rss: element(by.css('.buttonBar li:last-child > a')).getAttribute('href')
    };
  }

  activateSocialLink(name, cb) {
    let names = ['facebook', 'twitter', 'plus'];
    let userLinksPrefixList = ['https://facebook.com/sharer/sharer.php?u=', 'https://twitter.com/intent/tweet/?text=', 'https://plus.google.com/share?url='];
    let nonUserLinksPrefixList = ['https://www.facebook.com/login.php', 'https://twitter.com/intent/tweet/?text=', 'https://accounts.google.com/ServiceLogin'];

    let index = names.indexOf(name);

    element(by.css('.buttonBar li:nth-child(3)')).click();
    element(by.css('social a:nth-child(' + (index + 1) + ')')).click();
    browser.sleep(1000);

    browser.getAllWindowHandles().then(handles => {
      let primary = handles[0];
      let secondary = handles[handles.length - 1];

      browser.switchTo().window(secondary).then(() => {
        browser.getCurrentUrl().then(url => {
          // For the selected social media: either the URL matches for someone who is a logged-in user, or as an anonymous user.
          let result = (url.indexOf(userLinksPrefixList[index]) === 0) || (url.indexOf(nonUserLinksPrefixList[index]) === 0);

          browser.driver.close();
          browser.switchTo().window(primary);
          cb(result);
        });
      });
    });
  }

  setUserAsAnonymous(revert?: boolean) {
    this.navigate('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    if (revert) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');
      element(by.css('identitymanagement input[type=submit]')).click();

      browser.sleep(1000);

      element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
      for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
        element(by.id(thing)).click();
      }
      element(by.css('entitlements input[type=submit]')).click();

    } else {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');
      element(by.css('identitymanagement input[type=submit]')).click();
    }

    browser.sleep(1000);
  }

  toggleEntitlements() {
    this.navigate('/admin');

    element(by.id('PA')).click();
    element(by.id('dhe')).click();
    element(by.id('zsp')).click();

    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(1000);
  }
}
