import { browser, element, by } from 'protractor';

export class BrowsePAAlpha {
  navigate(path?: string) {
    return (path && path.length) ? browser.get(path) : browser.get('/PsycARTICLES/title');
  }

  getInstancesCount() {
    return element.all(by.css('browsepaalpha')).count();
  }

  getJournalTitles() {
    return element.all(by.css('journal .journal-title'));
  }

  getBreadcrumbLinks() {
    return element.all(by.css('breadcrumbs a'));
  }

  selectLetterB() {
    element.all(by.css('browseaz:first-child a')).get(1).click();
  }

  selectLetterX() {
    element.all(by.css('browseaz:first-child a')).get(23).click();
  }

  searchWithJumpTo(term: string) {
    element.all(by.css('jumpto input')).get(0).sendKeys(term);
    element.all(by.css('jumpto button')).get(0).click();
  }

  getEmptyRecordsMessage() {
    return element.all(by.css('.emptyState')).get(0).getText();
  }

  getJournalHistory() {
    element(by.css('journalhistory a')).click();
    return element.all(by.css('journalhistory li'));
  }

  navigateAround() {
    element(by.css('browsebybox a:nth-child(3)')).click();
    element(by.css('browsebybox a:nth-child(2)')).click();
  }

  goToJournalHome() {
    element(by.css('journal .journal-title')).click();
    return element(by.css('journalactionbar h1'));
  }

  subscribeAsOrganization() {
    this.navigate('/admin');
    element(by.id('PA')).click();
    element(by.id('xlm')).click();
    element(by.id('spy')).click();
    element(by.css('entitlements input[type=submit]')).click();

    // Waiting for the data to be saved, because of the network latency!
    browser.sleep(1000);

    this.navigate();
    return ['Journal of Experimental Psychology: Learning, Memory, and Cognition', 'Sport, Exercise, and Performance Psychology'];
  }

  subscribeAsIndividual() {
    this.navigate('/admin');

    element(by.id('xlm')).click();
    element(by.id('spy')).click();
    element(by.css('entitlements input[type=submit]')).click()

    // Waiting for the data to be saved, because of the network latency!
    browser.sleep(1000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');
    element(by.css('identitymanagement input[type=submit]')).click();

    // Waiting for the data to be saved, because of the network latency!
    browser.sleep(1000);

    this.navigate();
  }

  getMySubscriptions() {
    return element(by.id('my-subscriptions')).element(by.css('.emptyState')).getText();
  }

  getUnsubscribedJournals() {
    return element(by.id('non-subscribed')).all(by.css('.journal-title'));
  }

  resetEntitlements() {
    this.navigate('/admin');
    element(by.id('PA')).click();
    element(by.css('entitlements input[type=submit]')).click()

    // Waiting for the data to be saved, because of the network latency!
    browser.sleep(1000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');
    element(by.css('identitymanagement input[type=submit]')).click();

    // Waiting for the data to be saved, because of the network latency!
    browser.sleep(1000);
  }
}
