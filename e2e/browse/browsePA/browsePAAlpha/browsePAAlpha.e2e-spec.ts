import { browser } from 'protractor';
import { BrowsePAAlpha } from './browsePAAlpha.po';

describe('Browse PA By Title', () => {
  let page: BrowsePAAlpha;

  beforeEach(() => {
    page = new BrowsePAAlpha();
    page.navigate();

    // When the execution is slower than expected, due to network latency, consider enabling the following setting!
    browser.manage().timeouts().implicitlyWait(10000);
  });

  it('should display the correct page.', () => {
    page.getInstancesCount().then(count => expect(count).toBe(1));
  });

  it('should have correct breadcrumb links.', () => {
    let links = ['PsycARTICLES', 'Browse by Title'];
    page.getBreadcrumbLinks().each(link => {
      link.getText().then(text => expect(links).toContain(text));
    });
  });

  it('should load all the journals.', () => {
    let journals = page.getJournalTitles();

    journals.count().then(count => {
      expect(count).toBe(119);

      journals.get(0).getText().then(title => expect(title.toLowerCase().indexOf('a')).toBe(0));
      journals.get(count - 1).getText().then(title => expect(title.toLowerCase().indexOf('z')).toBe(0));
    });
  });

  it('should display titles that start with "B", after clicking that letter in the alphabet strip.', () => {
    page.selectLetterB();

    page.getJournalTitles().each(item => {
      item.getText().then(title => expect(title.indexOf('B')).toBe(0));
    })
  });

  it('should handle the empty results case, for letter selection within the alphabet strip.', () => {
    page.selectLetterX();
    page.getEmptyRecordsMessage().then(message => expect(message.toLowerCase()).toContain('no records found'));
  });

  it('should use (jump-to) search for filtering the journal titles.', () => {
    page.searchWithJumpTo('Can');
    page.getJournalTitles().get(0).getText().then(title => expect(title.indexOf('Can')).toBe(0));
  });

  it('should handle the empty results case, for a (jump-to) search.', () => {
    page.searchWithJumpTo('test');
    page.getEmptyRecordsMessage().then(message => expect(message.toLowerCase()).toContain('no records found'));
  });

  it('should display journal history details.', () => {
    page.searchWithJumpTo('Journal of Consulting and Clinical Psychology');
    let history = page.getJournalHistory();
    history.count().then(count => expect(count).toBe(1));
    history.get(0).getText().then(details => expect(details).toBe('Journal of Consulting Psychology, Vols. 1-31, 1937-1967'));
  });

  it('should display this component, after back-and-forth navigation.', () => {
    page.navigateAround();
    page.getInstancesCount().then(count => expect(count).toBe(1));
  });

  it('should link to details of a journal, which is shown within an instance of (JournalHome) component.', () => {
    let title = 'Journal of Consulting and Clinical Psychology';
    page.searchWithJumpTo(title);
    page.goToJournalHome().getText().then(text => expect(text.indexOf(title)).toBe(0));
  });

  it('should display specific journal subscriptions for an organization.', () => {
    let subscription = page.subscribeAsOrganization();
    let titles = page.getJournalTitles();
    titles.count().then(count => expect(count).toBe(2));
    titles.each(title => title.getText().then(text => expect(subscription).toContain(text)));
  });

  it('should display the journals which the individual user has subscribed to, and unsubscribed from.', () => {
    page.subscribeAsIndividual();
    page.getMySubscriptions().then(message => expect(message.toLowerCase()).toContain('no records found'));
    page.getUnsubscribedJournals().count().then(count => expect(count).toBe(119));
  });

  afterAll(() => {
    page.resetEntitlements();
  });
});
