import { browser, element, by } from 'protractor';

export class BrowsePATopic {
  navigate(path?: string) {
    return (path && path.length) ? browser.get(path) : browser.get('/PsycARTICLES/topic');
  }

  getPageInstancesCount() {
    return element.all(by.css('browsepatopic')).count();
  }

  getAllJournals() {
    element(by.css('.all-group .accordionHeading')).click();
    return element.all(by.css('#allJournalsContent .journal-title'));
  }

  getTopics() {
    return element.all(by.css('.topic-group'));
  }

  getDefaultTopicJournals() {
    return element.all(by.css('.topic-group:nth-child(2) .journal-title'));
  }

  expandTopic() {
    element(by.css('.topic-group:nth-child(4) .accordionHeading')).click();
    return element.all(by.css('.topic-group:nth-child(4) .journal-title'));
  }

  collapseTopic() {
    element(by.css('.topic-group:nth-child(4) .accordionHeading')).click();
    return element.all(by.css('.topic-group:nth-child(4) .journal-title'));
  }

  getBreadcrumbLinks() {
    return element.all(by.css('breadcrumbs li:last-child a'));
  }

  getJournalHistoryFromTopic() {
    element(by.css('.topic-group:nth-child(8) .accordionHeading')).click();
    element.all(by.css('.topic-group:nth-child(8) journalhistory strong')).get(0).click();
    return element.all(by.css('journalhistory li'));
  }

  getJournalHistoryFromAll() {
    element(by.css('.all-group .accordionHeading')).click();
    element.all(by.css('.all-group journalhistory strong')).get(0).click();
    return element.all(by.css('journalhistory li'));
  }

  navigateAround() {
    element(by.css('browsebybox a:nth-child(2)')).click();
    element(by.css('browsebybox a:nth-child(3)')).click();
  }

  goToJournalHomeViaTopic() {
    element.all(by.css('.topic-group')).get(0).element(by.css('li:nth-child(3) a')).click();
    return element(by.css('journalactionbar')).element(by.css('h1')).getText();
  }

  goToJournalHomeViaAll() {
    element(by.css('.all-group .accordionHeading')).click();
    element(by.css('.all-group li:nth-child(3) a')).click();
    return element(by.css('journalactionbar')).element(by.css('h1')).getText();
  }

  subscribeAsOrganization() {
    this.navigate('/admin');
    element(by.id('PA')).click();
    element(by.id('xlm')).click();
    element(by.id('spy')).click();
    element(by.css('entitlements input[type=submit]')).click();
    this.navigate();

    return ['Journal of Experimental Psychology: Learning, Memory, and Cognition', 'Sport, Exercise, and Performance Psychology'];
  }

  getAllJournalsByTopics() {
    element.all(by.css('.topic-group .accordionHeading')).each(topic => topic.click());
    return element.all(by.css('.topic-group .journal-title'));
  }

  subscribeAsIndividual() {
    this.navigate('/admin');

    element(by.id('xlm')).click();
    element(by.id('spy')).click();
    element(by.css('entitlements input[type=submit]')).click()

    // Waiting for the data to be saved, because of network latency!
    browser.sleep(1000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');
    element(by.css('identitymanagement input[type=submit]')).click();

    // Waiting for the data to be saved, because of network latency!
    browser.sleep(1000);

    this.navigate();
  }

  resetEntitlements() {
    this.navigate('/admin');

    element(by.id('PA')).click();
    element(by.css('entitlements input[type=submit]')).click()

    // Waiting for the data to be saved, because of network latency!
    browser.sleep(1000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');
    element(by.css('identitymanagement input[type=submit]')).click();

    // Waiting for the data to be saved, because of network latency!
    browser.sleep(1000);
  }
}
