import { browser } from 'protractor';
import { BrowsePATopic } from './browsePATopic.po';

describe('Browse PA By Topic', () => {
  let page: BrowsePATopic;

  beforeEach(() => {
    page = new BrowsePATopic();
    page.navigate();

    // When the execution is slower than expected, due to network latency, consider enabling the following setting!
    browser.manage().timeouts().implicitlyWait(10000);
  });

  it('should display the correct page.', () => {
    page.getPageInstancesCount().then(count => expect(count).toBe(1));
  });

  it('should load all the journals, and list the topics.', () => {
    let journals = page.getAllJournals();
    journals.count().then(count => {
      expect(count).toBeGreaterThan(0);
      journals.get(0).getText().then(text => expect(text.toLowerCase().indexOf('a')).toBe(0));
      journals.get(count - 1).getText().then(text => expect(text.toLowerCase().indexOf('z')).toBe(0));
    });

    let topics = page.getTopics();
    topics.count().then(count => expect(count).toBeGreaterThan(0));
  });

  it('should expand the second topic after page load.', () => {
    let journals = page.getDefaultTopicJournals();
    journals.count().then(count => expect(count).toBeGreaterThan(0));
  });

  it('should expand and collapse some topic, while checking the displayed journals status.', () => {
    let journals = page.expandTopic();

    journals.count().then(count => expect(count).toBeGreaterThan(0));

    journals.each(journal => {
      journal.isDisplayed().then(displayed => expect(displayed).toBeTruthy());
    });

    page.collapseTopic().each(hiddenTitle => {
      hiddenTitle.isDisplayed().then(displayed => expect(displayed).toBeFalsy);
    });
  });

  it('should list the right title within the breadcrumb.', () => {
    let links = ['PsycARTICLES', 'Browse by Topic'];
    let breadcrumb = page.getBreadcrumbLinks();
    breadcrumb.each(item => {
      item.getText().then(text => expect(links).toContain(text));
    });
  });

  it('should show the journal history, for a topic-item.', () => {
    let history = page.getJournalHistoryFromTopic().get(0);
    history.getText().then(text => expect(text).toBe('Cultural Diversity and Mental Health, Vols. 1-4, 1995-1998'));
  });

  it('should show the journal history, for an item within (All Journals).', () => {
    let history = page.getJournalHistoryFromAll();
    let records = [
      'Journal of Comparative and Physiological Psychology, Vols. 40-96, 1947-1982',
      'Journal of Comparative Psychology, Vols. 1-39, 1921-1946'
    ];

    history.count().then(count => expect(count).toBe(records.length));
    history.each(record => {
      record.getText().then(text => expect(records).toContain(text));
    });
  });

  it('should display this component, after back-and-forth navigation.', () => {
    page.navigateAround();
    page.getPageInstancesCount().then(count => expect(count).toBe(1));
  });

  it('should navigate to journal details page, via (Topic).', () => {
    page.goToJournalHomeViaTopic().then(title => expect(title.indexOf('Dreaming')).toBe(0));
  });

  it('should navigate to journal details page, via (All Journals).', () => {
    page.goToJournalHomeViaAll().then(title => expect(title.indexOf('Archives of Scientific Psychology')).toBe(0));
  });

  it('should display the journal subscriptions, for organization user.', () => {
    let journals = page.subscribeAsOrganization();

    page.getAllJournals().count().then(count => expect(count).toBe(journals.length));
    page.getAllJournalsByTopics().each(journal => {
      journal.getText().then(title => expect(journals).toContain(title));
    });
  });

  it('should display all the journals, for individual user who has no subscriptions.', () => {
    page.subscribeAsIndividual();

    let journals = page.getAllJournals();
    journals.count().then(count => {
      expect(count).toBeGreaterThan(0);
      journals.get(0).getText().then(text => expect(text.toLowerCase().indexOf('a')).toBe(0));
      journals.get(count - 1).getText().then(text => expect(text.toLowerCase().indexOf('z')).toBe(0));
    });
  });

  afterAll(() =>  {
    page.resetEntitlements();
  });
});
