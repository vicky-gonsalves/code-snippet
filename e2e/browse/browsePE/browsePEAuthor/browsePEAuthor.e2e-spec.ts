import { BrowsePEAuthor } from './browsePEAuthor.po';
import { browser } from 'protractor';

describe('Browse PsycEXTRA Author Page', function() {
  let page: BrowsePEAuthor;

  beforeEach(() => {
    page = new BrowsePEAuthor();
    page.navigateTo();
    browser.manage().timeouts().implicitlyWait(9000);
  });

  it('should have PsycEXTRA heading', () => {
    expect(page.getPageHeading()).toEqual('PsycEXTRA');
  });

 it('should have \'Browse by Author\' in breadcrumbs', () => {
    let breadcrumbs = page.getBreadcrumbs();
    browser.sleep(1000);
    expect(breadcrumbs.get(0).getText()).toBe('PsycEXTRA');
    expect(breadcrumbs.get(1).getText()).toBe('Browse by Author');
  });

  it('should have browseaz component', () => {
    expect(page.getBrowseAZ().count()).toBeGreaterThan(0);
  });

  it('should have paginationLimit component', () => {
    expect(page.getPaginationLimit().count()).toBeGreaterThan(0);
  });

  it('should have jumpto component', () => {
    expect(page.getJumpto().count()).toBeGreaterThan(0);
  });

  it('should display correct number of elements based on the selected limit at first loading', () => {
    let limit: number;
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedRecordsCount().then(count => {
        expect(count).toEqual(limit);
      });
    });
  });

  it('should display pagination component if records are more than display limit', () => {
    let totalItems: number;
    let limit: number;
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l=> {
      limit = parseInt(_l);
      page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records=> {
        totalItems = parseInt(records);
        expect(!!page.getPaginationLink().count()).toBe(totalItems > limit);
      });
    });
  });

  it('should display records starting with B when letter B is selected from the alphabet strip', () => {
    browser.sleep(1000);
    page.getSelectedLetter(2);
    browser.sleep(1000);
    page.getAuthorNames().each(author=> {
      author.getText().then(name=> {
        expect(name.charAt(0)).toBe('B');
      });
    });
  });

  it('should display correct number of elements when the limit change to be 50', () => {
    let limit: number;
    page.changeLimitValue();
    browser.sleep(2000);
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedRecordsCount().then(count => {
        expect(+count).toEqual(limit);
      });
    });
  });

  it('should load content for second Author at first page (. Hills, Marcia D) when it is clicked', () => {
    page.openSecondAuthorContent();
    expect(page.getAuthorContent()).toBeTruthy();
  });

  it('should open the correct record page', () => {
    page.getRecordUID().then(uid => {
      page.openRecord();
      browser.sleep(1000);
      expect(browser.getCurrentUrl()).toContain('/record/'+ uid);
    });
  });

  it('should search record by jumpto input', () => {
    let searchFor = 'john';
    browser.sleep(1000);
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(1000);
    page.getAuthorNames().each(author=> {
      author.getText().then(name=> {
        expect(name.toLowerCase().indexOf(searchFor)).toBe(0);
      });
    });
  });

  it('should dipslay No record found  when user enters \'cancer\' in the jumpto input', () => {
    let searchFor = 'cancer';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    browser.wait(page.executeSearch(),9000).then(()=> {
      expect(page.getEmptyPage().isDisplayed()).toBe(true);
    });
  });

  it('should navigate correct', () => {
    page.selectBrowseByPublicationYear().then(() => {
        browser.wait(page.selectBrowseByAuthor(), 1000).then(() => {
         expect(browser.getCurrentUrl()).toContain('/author');
      });
    });
  });

  it('should have pdf link', () => {
    expect(page.getPDFLink().isDisplayed()).toBe(true);
  });

  it('should open pdf', () => {
    page.getRecordUID().then(uid => {
      page.getPDFLink().click();
      browser.getAllWindowHandles().then(function (handles) {
        let newWindowHandle = handles[1]; // this is the full text window
        browser.switchTo().window(newWindowHandle).then(function () {
          expect(browser.getCurrentUrl()).toContain('/fulltext/'+ uid +'.pdf');
        });
      browser.driver.close();
      browser.switchTo().window(handles[0]);
      });
    });
  });

  it('should add record to My List correctly', () => {
    let countBefore, countAfter;
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      browser.wait(page.addRecordToMyList(),3000).then(() => {
        page.getMyListCount().then(countA => {
          countAfter = parseInt(countA);
          expect(countAfter).toEqual(countBefore+1);
        });
      });
    });
  });

  it('should remove record from My List correctly', () => {
    let countBefore, countAfter;
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      browser.wait(page.removeRecordFromMyList(),3000).then(() => {
        page.getMyListCount().then(countA => {
          countAfter = parseInt(countA);
           expect(countAfter).toEqual(countBefore-1);
        });
      });
    });
  });

  it('should navigate to the next page correctly', () => {
    let nextRecord;
    page.getNextPageLabel().then(text => {
      nextRecord = text;
      browser.wait(page.navigateToNextPage(),3000).then(() => {
        page.getAuthorNames().first().getText().then(name => {
          expect(name).toContain(nextRecord);
        });
      });
    });
  });

  it('should navigate to the previous page correctly', () => {
    let prevLabel;
    browser.wait(page.navigateToNextPage(),3000).then(() => {
      page.getPrevPageLabel().then(text => {
        prevLabel = text;
        browser.wait(page.navigateToPrevPage(),3000).then(() => {
          page.getAuthorNames().last().getText().then(name => {
            expect(name).toContain(prevLabel);
          });
        });
      });
    });
  });

  it('should return user to search page if he has not PE access', () => {
    page.removePEPermission();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/search');
  });

  it('should navigate to buy page if user is individual without PE access', () => {
    page.setUserAsIndividual();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/buy/PE');
  });

  afterAll(() =>  {
    page.revertPermissionChanges();
    browser.sleep(1000);
  });
});

