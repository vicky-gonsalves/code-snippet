import { browser, element, by } from 'protractor';

export class BrowsePEAuthor {

  navigateTo(path?: string) {
    return (path && path.length) ? browser.get(path) : browser.get('/PsycEXTRA/author');
  }

  getPageHeading() {
    let heading = element(by.tagName('h1'));
    return heading.getText();
  }

  getBreadcrumbs() {
    let breadcrumbs = element.all(by.css('breadcrumbs ol>li>a'));
    return breadcrumbs;
  }

 getBrowseAZ() {
    return element.all(by.css('browseaz'));
  }

  getPaginationLimit() {
    return element.all(by.css('paginationlimit'));
  }

  getJumpto() {
    return element.all(by.css('jumpto'));
  }

  getDisplayedRecordsCount() {
    return element.all(by.css('.accordionContainer')).count();
  }

  getDisplayedLimit() {
    return element.all(by.css('paginationlimit select')).get(0).getAttribute('ng-reflect-model');
  }

  changeLimitValue() {
    let limit = element.all(by.css('paginationlimit option')).get(1);
    return limit.click();
  }

  getSelectedLetter(orderLetter) {
    let browseButton =  element.all(by.css('browseaz')).get(0).element(by.css('li:nth-child('+orderLetter+') a'));
    return browseButton.click();
  }

  getPaginationComponent() {
    return element.all(by.css('termBasedPagination')).first();
  }

  getPaginationLink() {
    return this.getPaginationComponent().all(by.css('div>ul>li>a'));
  }

  getAuthorNames() {
    return element.all(by.css('.accordionContainer strong'));
  }

  getEmptyPage() {
    return element(by.css('.emptyState'));
  }

  openSecondAuthorContent() {
    return element.all(by.css('.accordionHeading a')).get(1).click();
  }

  getAuthorContent() {
    return element.all(by.css('.accordionContainer')).get(1).element(by.css('ul'));
  }

  openRecord() {
    let Title = element(by.css('.accordionContainer>ul>li>span>a'));
    return Title.click();
  }

  getRecordUID() {
    return element.all(by.css('.recordUID')).get(0).getAttribute("textContent");
  }

  private getSearchInput() {
    return this.getJumpto().first().element(by.css('input[name="jumpTo"]'));
  }

  getSearchInputText() {
    let input = this.getSearchInput();
    return input.getAttribute('value');
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  executeSearch() {
    let searchBtn = this.getJumpto().first().element(by.css('button'));
    return searchBtn.click();
  }

  selectBrowseByAuthor() {
    let newBrowse = element(by.id('byauthor'));
    return newBrowse.click();
  }

  selectBrowseByPublicationYear() {
    let newBrowse = element(by.id('bypubyear'));
    return newBrowse.click();
  }

  getPDFLink() {
    return element.all(by.css('.buttonBar > a')).get(0);
  }

  getMyListCount() {
    return element(by.css('personalization .badge')).getText();
  }

  addRecordToMyList() {
    return element.all(by.css('add-to-my-list button')).get(0).click();
  }

  removeRecordFromMyList() {
    return element.all(by.css('add-to-my-list button')).get(0).click();
  }

  getMyListLabel() {
    return element.all(by.css('add-to-my-list button .caption')).get(0).click();
  }

  getNextPageLabel() {
    return element.all(by.css('termBasedPagination ul li:first-child a span:last-child')).get(1).getText();
  }

  navigateToNextPage() {
    return element.all(by.css('termBasedPagination ul li:first-child a')).get(1).click();
  }

  getPrevPageLabel() {
    return element.all(by.css('termBasedPagination ul li:first-child a span:first-child')).get(1).getText();
  }

  navigateToPrevPage() {
    return element.all(by.css('termBasedPagination ul li:first-child a')).get(1).click();
  }

  removePEPermission() {
    this.navigateTo('/admin');
    element(by.id('PE')).click();
    element(by.css('entitlements input[type=submit]')).click();
    this.navigateTo();
  }

  setUserAsIndividual() {
    //user is individual with no PE access permission
    this.navigateTo('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
    this.navigateTo();
  }

  revertPermissionChanges() {
    this.navigateTo('/admin');
    element(by.id('PE')).click();
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(1000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }
}
