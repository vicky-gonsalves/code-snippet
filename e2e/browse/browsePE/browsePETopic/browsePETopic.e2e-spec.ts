import { BrowsePETopic } from './browsePETopic.po';
import { browser } from 'protractor';

describe('Browse PsycExtra Topic Page', function() {
  let page: BrowsePETopic;

  beforeEach(() => {
    browser.manage().timeouts().implicitlyWait(90000);
    page = new BrowsePETopic();
    page.navigateTo();
  });

  it('should have PsycExtra heading', () => {
    expect(page.getPageHeading()).toEqual('PsycEXTRA');
  });

   it('should have \'Browse by Topic\' in breadcrumbs', () => {
    expect(page.getBreadcrumbsCompnentTitle().getText()).toBe('Browse by Topic');
  });

  it('should have browseaz component', () => {
    expect(page.getBrowseAZ().count()).toBeGreaterThan(0);
  });

  it('should have paginationLimit component', () => {
    expect(page.getPaginationLimit().count()).toBeGreaterThan(0);
  });

  it('should have jumpto component', () => {
    expect(page.getJumpto().count()).toBeGreaterThan(0);
  });

  it('should display correct number of elements based on the selected limit at first loading', () => {
    let limit: number;
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedTopicsCount().then(count => {
        expect(count).toEqual(limit);
      });
    });
  });

  it('should display pagination component if records are more than display limit', () => {
    let totalItems: number;
    let limit: number;
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l=> {
      limit = parseInt(_l);
      page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records=> {
        totalItems = parseInt(records);
        expect(!!page.getPaginationLink().count()).toBe(totalItems > limit);
      });
    });
  });

  it('should display correct number of elements when the limit change to be 50', () => {
    let limit: number;
    page.changeLimitValue();
    browser.sleep(1000);
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedTopicsCount().then(count => {
        expect(+count).toEqual(limit);
      });
    });
  });

  it('should not display pagination component when letter O is selected from the alphabet strip', () => {
    let totalItems: number;
    let limit: number;
    page.getSelectedLetter(15); //Select letter O from the alphabet strip
    expect(page.getPaginationComponent().isDisplayed()).toBe(false);
  });

  it('should display records starting with B when letter B is selected from the alphabet strip', () => {
    page.getSelectedLetter(2);
    browser.sleep(1000);
    page.getTopicNames().each(topic=> {
      topic.getText().then(name=> {
        expect(name.charAt(0)).toBe('B');
      });
    });
  });

  it('should display No record found when letter X is selected from the alphabet strip', () => {
    page.getSelectedLetter(24); //Select letter X from the alphabet strip
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should load content for first topic \'Academic Learning & Achievement \' when it is clicked', () => {
    let title;
    page.openFirstTopic();
    page.getTopicContentTitle().then(text =>{
      expect(text).toContain('Academic Learning & Achievement');
    });
  });

  it('should open the correct record page at first topic', () => {
    page.openFirstTopic();
    page.getRecordUID().then(uid => {
      page.openExtraRecord();
      browser.sleep(1000);
      expect(browser.getCurrentUrl()).toContain('/record/'+ uid);
    });
  });

  it('should search topic by jumpto input', () => {
   let searchFor = 'criminal';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(1000);
    page.getTopicNames().each(topic=> {
      topic.getText().then(name=> {
        expect(name.toLowerCase().indexOf(searchFor)).toBe(0);
      });
    });
  });

  it('should dipslay No record found  when user enters \'therapy\' in the jumpto input', () => {
    let searchFor = 'therapy';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    browser.wait(page.executeSearch(),9000).then(()=> {
      expect(page.getEmptyPage().isDisplayed()).toBe(true);
    });
  });

  it('should search inside topic by jumpto input', () => {
   let searchFor = 'test';
   page.openFirstTopic();
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(2000);
    page.getTitlesInsideTopic().each(title=> {
      title.getText().then(name=> {
        expect(name.toLowerCase().indexOf(searchFor)).toBe(0);
      });
    });
  });

  it('should dipslay No record found  when user enters \'eat\' in the jumpto input at specific topic', () => {
    let searchFor = 'eat';
    page.openFirstTopic();
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(1000);
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should display records starting with J when letter J is selected inside topic from the alphabet strip', () => {
    page.openFirstTopic();
    browser.sleep(1000);
    page.getSelectedLetter(10);
    browser.sleep(1000);
    page.getTitlesInsideTopic().each(title=> {
      title.getText().then(name=> {
        expect(name.charAt(0)).toBe('J');
      });
    });
  });

  it('should have pdf link', () => {
    page.openFirstTopic();
    expect(page.getPDFLink().isDisplayed()).toBe(true);
  });

  it('should open pdf', () => {
    page.openFirstTopic();
    page.getRecordUID().then(uid => {
      page.getPDFLink().click();
      browser.getAllWindowHandles().then(function (handles) {
        let newWindowHandle = handles[1]; // this is the full text window
        browser.switchTo().window(newWindowHandle).then(function () {
          expect(browser.getCurrentUrl()).toContain('/fulltext/'+ uid +'.pdf');
        });
      browser.driver.close();
      browser.switchTo().window(handles[0]);
      });
    });
  });

  it('should add record to My List correctly', () => {
    let countBefore, countAfter;
    page.openFirstTopic();
    browser.sleep(1000);
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.addRecordToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore+1);
      });
    });
  });

  it('should remove record from My List correctly', () => {
    let countBefore, countAfter;
    page.openFirstTopic();
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.removeRecordToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore-1);
      });
    });
  });

  it('should navigate correct', () => {
    page.selectBrowseByPublicationYear().then(() => {
        browser.wait(page.selectBrowseByTopic(), 1000).then(() => {
         expect(browser.getCurrentUrl()).toContain('/topic');
      });
    });
  });

  it('should return user to search page if he has not PE access', () => {
    page.removePEPermission();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/search');
  });

  it('should navigate to buy page if user is individual without PE access', () => {
    page.setUserAsIndividual();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/buy/PE');
  });

  afterAll(() =>  {
    page.revertPermissionChanges();
    browser.sleep(1000);
  });
});
