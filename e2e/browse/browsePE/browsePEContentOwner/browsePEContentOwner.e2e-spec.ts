import { BrowsePEContentOwner } from './browsePEContentOwner.po';
import { browser } from 'protractor';

describe('Browse PsycExtra Content Owner Page', function() {
  let page: BrowsePEContentOwner;

  beforeEach(() => {
    browser.manage().timeouts().implicitlyWait(90000);
    page = new BrowsePEContentOwner();
    page.navigateTo();
  });

  it('should have PsycExtra heading', () => {
    expect(page.getPageHeading()).toEqual('PsycEXTRA');
  });

  it('should have \'Browse by Content Owner\' in breadcrumbs', () => {
    expect(page.getBreadcrumbsCompnentTitle().getText()).toBe('Browse by Content Owner');
  });

  it('should have browseaz component', () => {
    expect(page.getBrowseAZ().count()).toBeGreaterThan(0);
  });

  it('should have paginationLimit component', () => {
    expect(page.getPaginationLimit().count()).toBeGreaterThan(0);
  });

  it('should have jumpto component', () => {
    expect(page.getJumpto().count()).toBeGreaterThan(0);
  });

  it('should display correct number of elements based on the selected limit at first loading', () => {
    let limit: number;
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedRecordsCount().then(count => {
        expect(count).toEqual(limit);
      });
    });
  });

  it('should display pagination component if records are more than display limit', () => {
    let totalItems: number;
    let limit: number;
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l=> {
      limit = parseInt(_l);
      page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records=> {
        totalItems = parseInt(records);
        expect(!!page.getPaginationLink().count()).toBe(totalItems > limit);
      });
    });
  });

  it('should display correct number of elements when the limit change to be 50', () => {
    let limit: number;
    page.changeLimitValue();
    browser.sleep(2000);
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedRecordsCount().then(count => {
        expect(+count).toEqual(limit);
      });
    });
  });

  it('should not display pagination component when letter Y is selected from the alphabet strip', () => {
    let totalItems: number;
    let limit: number;
    page.getSelectedLetter(25); //Select letter Y from the alphabet strip
    expect(page.getPaginationComponent().isDisplayed()).toBe(false);
  });

  it('should display records of content owner starting with B when letter B is selected from the alphabet strip', () => {
    browser.sleep(1000);
    page.getSelectedLetter(2);
    browser.sleep(1000);
    page.getContentNames().each(title=> {
      title.getText().then(name=> {
        expect(name.charAt(0)).toBe('B');
      });
    });
  });

  it('should display No record found when letter X is selected from the alphabet strip', () => {
    page.getSelectedLetter(24); //Select letter X from the alphabet strip
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should open the correct record page', () => {
    page.getRecordUID().then(uid => {
      page.openExtraRecord();
      browser.sleep(1000);
      expect(browser.getCurrentUrl()).toContain('/record/'+ uid);
    });
  });

  it('should search content owner by jumpto input', () => {
    browser.sleep(1000);
    let searchFor = 'can';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(1000);
    page.getContentNames().each(topic=> {
      topic.getText().then(name=> {
        expect(name.toLowerCase().indexOf(searchFor)).toBe(0);
      });
    });
  });

  it('should dipslay No record found  when user enters \'criminal\' in the jumpto input', () => {
    browser.sleep(1000);
    let searchFor = 'criminal';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    browser.wait(page.executeSearch(),9000).then(()=> {
      expect(page.getEmptyPage().isDisplayed()).toBe(true);
    });
  });


  it('should have pdf link', () => {
    expect(page.getPDFLink().isDisplayed()).toBe(true);
  });

  it('should open pdf', () => {
    page.getRecordUID().then(uid => {
      page.getPDFLink().click();
      browser.getAllWindowHandles().then(function (handles) {
        let newWindowHandle = handles[1]; // this is the full text window
        browser.switchTo().window(newWindowHandle).then(function () {
          expect(browser.getCurrentUrl()).toContain('/fulltext/'+ uid +'.pdf');
        });
      browser.driver.close();
      browser.switchTo().window(handles[0]);
      });
    });
  });

  it('should add record to My List correctly', () => {
    let countBefore, countAfter;
    browser.sleep(1000);
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.addRecordToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore+1);
      });
    });
  });

  it('should remove record from My List correctly', () => {
    let countBefore, countAfter;
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.removeRecordFromMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore-1);
      });
    });
  });

  it('should navigate correct', () => {
    page.selectBrowseByPublicationYear().then(() => {
        browser.wait(page.selectBrowseByContentOwner(), 1000).then(() => {
         expect(browser.getCurrentUrl()).toContain('/contentowner');
      });
    });
  });

  it('should return user to search page if he has not PE access', () => {
    page.removePEPermission();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/search');
  });

  it('should navigate to buy page if user is individual without PE access', () => {
    page.setUserAsIndividual();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/buy/PE');
  });

  afterAll(() =>  {
    page.revertPermissionChanges();
    browser.sleep(1000);
  });
});
