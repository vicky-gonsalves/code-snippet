import { browser, element, by } from 'protractor';

export class BrowsePEDocType {

  navigateTo(path?: string) {
    return (path && path.length) ? browser.get(path) : browser.get('/PsycEXTRA/doctype');
  }

  getPageHeading() {
    let heading = element(by.tagName('h1'));
    return heading.getText();
  }

  getBreadcrumbsCompnentTitle() {
    return element.all(by.css('breadcrumbs a')).get(1);
  }

  getBreadcrumbsPageTitle() {
    return element.all(by.css('breadcrumbs a')).get(0);
  }

  getDisplayedTypesCount() {
    return element.all(by.css('article')).count();
  }

  openFirstType() {
    return element.all(by.css('article a')).get(0).click();
  }

  getTypeContentTitle() {
    return element(by.css('.list h3')).getText();
  }

  getBrowseAZ() {
    return element.all(by.css('browseaz'));
  }

  getPaginationLimit() {
    return element.all(by.css('paginationlimit'));
  }

  getJumpto() {
    return element.all(by.css('jumpto'));
  }

  getDisplayedLimit() {
    return element.all(by.css('paginationlimit select')).get(0).getAttribute('ng-reflect-model');
  }

  changeLimitValue() {
    let limit = element.all(by.css('paginationlimit option')).get(1);
    return limit.click();
  }

  getPaginationComponent() {
    return element.all(by.css('numberedpagination')).first();
  }

  getPaginationLink() {
    return this.getPaginationComponent().all(by.css('div>ul>li>a'));
  }

  getSelectedLetter(orderLetter) {
    let browseButton =  element.all(by.css('browseaz')).get(0).element(by.css('li:nth-child('+orderLetter+') a'));
    return browseButton.click();
  }

  getRecordsTitle() {
    return element.all(by.css('article .titleLink'));
  }

  getEmptyPage() {
    return element(by.css('.emptyState'));
  }

  openExtraRecord() {
    let bookTitle = element.all(by.css('article a')).get(0);
    return bookTitle.click();
  }

  getRecordUID() {
    return element.all(by.css('.recordUID')).get(0).getAttribute("textContent");
  }

  private getSearchInput() {
    return this.getJumpto().first().element(by.css('input[name="jumpTo"]'));
  }

  getSearchInputText() {
    let input = this.getSearchInput();
    return input.getAttribute('value');
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  executeSearch() {
    let searchBtn = this.getJumpto().first().element(by.css('button'));
    return searchBtn.click();
  }

  getPDFLink() {
    return element.all(by.css('.buttonBar > a')).get(0);
  }

  selectBrowseByDocType() {
    let newBrowse = element(by.id('bydoctype'));
     return newBrowse.click();
  }

  selectBrowseByPublicationYear() {
    let newBrowse = element(by.id('bypubyear'));
    return newBrowse.click();
  }

  getMyListCount() {
    return element(by.css('personalization .badge')).getText();
  }

  addRecordToMyList() {
    return element.all(by.css('add-to-my-list button')).get(0).click();
  }

  removeRecordFromMyList() {
    return element.all(by.css('add-to-my-list button')).get(0).click();
  }

  getMyListLabel() {
    return element.all(by.css('add-to-my-list button .caption')).get(0).click();
  }

  removePEPermission() {
    this.navigateTo('/admin');
    element(by.id('PE')).click();
    element(by.css('entitlements input[type=submit]')).click();
    this.navigateTo();
  }

  setUserAsIndividual() {
    //user is individual with no PE access permission
    this.navigateTo('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
    this.navigateTo();
  }

  revertPermissionChanges() {
    this.navigateTo('/admin');
    element(by.id('PE')).click();
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(1000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }
}
