import { BrowsePEDocType } from './browsePEDocType.po';
import { browser } from 'protractor';

describe('Browse PsycExtra Document Type Page', function() {
  let page: BrowsePEDocType;

  beforeEach(() => {
    browser.manage().timeouts().implicitlyWait(90000);
    page = new BrowsePEDocType();
    page.navigateTo();
  });

  it('should have PsycExtra heading', () => {
    expect(page.getPageHeading()).toEqual('PsycEXTRA');
  });

   it('should have \'Browse by Document Type\' in breadcrumbs', () => {
    expect(page.getBreadcrumbsCompnentTitle().getText()).toBe('Browse by Document Type');
  });

  it('should display 19 types', () => {
    page.getDisplayedTypesCount().then(count => {
      expect(count).toEqual(19);
    });
  });

  it('should load content for first type \'Amicus Brief\' when it is clicked', () => {
    let title;
    page.openFirstType();
    page.getTypeContentTitle().then(text =>{
      expect(text).toContain('Amicus Brief');
    });
  });

  it('should have browseaz component at the first Type', () => {
    page.openFirstType();
    expect(page.getBrowseAZ().count()).toBeGreaterThan(0);
  });

  it('should have paginationLimit component at the first Type', () => {
    page.openFirstType();
    expect(page.getPaginationLimit().count()).toBeGreaterThan(0);
  });

  it('should have jumpto component at the first Type', () => {
    page.openFirstType();
    expect(page.getJumpto().count()).toBeGreaterThan(0);
  });

  it('should display pagination component if records are more than display limit at the first Type', () => {
    let totalItems: number;
    let limit: number;
    page.openFirstType();
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l=> {
      limit = parseInt(_l);
      page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records=> {
        totalItems = parseInt(records);
        expect(!!page.getPaginationLink().count()).toBe(totalItems > limit);
      });
    });
  });

  it('should display correct number of elements when the limit change to be 50 at the first Type', () => {
    let limit: number;
    page.openFirstType();
    browser.sleep(1000);
    page.changeLimitValue();
    browser.sleep(1000);
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedTypesCount().then(count => {
        expect(+count).toEqual(limit);
      });
    });
  });

  it('should not display pagination component when letter O is selected from the alphabet strip at the first type', () => {
    let totalItems: number;
    let limit: number;
    page.openFirstType();
    page.getSelectedLetter(15); //Select letter O from the alphabet strip
    expect(page.getPaginationComponent().isDisplayed()).toBe(false);
  });

  it('should display records starting with B when letter B is selected from the alphabet strip at the first type', () => {
    page.openFirstType();
    page.getSelectedLetter(2);
    browser.sleep(2000);
    page.getRecordsTitle().each(title=> {
      title.getText().then(name=> {
        expect(name.charAt(0)).toBe('B');
      });
    });
  });

  it('should display No record found when letter X is selected from the alphabet strip at the first type', () => {
    page.openFirstType();
    page.getSelectedLetter(24); //Select letter X from the alphabet strip
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should open the correct record page at first type', () => {
    page.openFirstType();
    page.getRecordUID().then(uid => {
      page.openExtraRecord();
      browser.sleep(1000);
      expect(browser.getCurrentUrl()).toContain('/record/'+ uid);
    });
  });

  it('should search inside type by jumpto input', () => {
   let searchFor = 'bo';
   page.openFirstType();
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(2000);
    page.getRecordsTitle().each(title=> {
      title.getText().then(name=> {
        expect(name.toLowerCase().indexOf(searchFor)).toBe(0);
      });
    });
  });

  it('should dipslay No record found  when user enters \'test\' in the jumpto input at first type', () => {
    let searchFor = 'test';
    page.openFirstType();
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(1000);
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should have pdf link', () => {
    page.openFirstType();
    expect(page.getPDFLink().isDisplayed()).toBe(true);
  });

  it('should open pdf', () => {
    page.openFirstType();
    page.getRecordUID().then(uid => {
      page.getPDFLink().click();
      browser.getAllWindowHandles().then(function (handles) {
        let newWindowHandle = handles[1]; // this is the full text window
        browser.switchTo().window(newWindowHandle).then(function () {
          expect(browser.getCurrentUrl()).toContain('/fulltext/'+ uid +'.pdf');
        });
      browser.driver.close();
      browser.switchTo().window(handles[0]);
      });
    });
  });

  it('should add record to My List correctly', () => {
    let countBefore, countAfter;
    page.openFirstType();
    browser.sleep(1000);
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.addRecordToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore+1);
      });
    });
  });

  it('should remove record from My List correctly', () => {
    let countBefore, countAfter;
    page.openFirstType();
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.removeRecordFromMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore-1);
      });
    });
  });

  it('should navigate correct', () => {
    page.selectBrowseByPublicationYear().then(() => {
        browser.wait(page.selectBrowseByDocType(), 1000).then(() => {
         expect(browser.getCurrentUrl()).toContain('/doctype');
      });
    });
  });

  it('should return user to search page if he has not PE access', () => {
    page.removePEPermission();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/search');
  });

  it('should navigate to buy page if user is individual without PE access', () => {
    page.setUserAsIndividual();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/buy/PE');
  });

  afterAll(() =>  {
    page.revertPermissionChanges();
    browser.sleep(1000);
  });
});
