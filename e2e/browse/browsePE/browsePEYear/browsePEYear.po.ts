import { browser, element, by } from 'protractor';

export class BrowsePEYear {

  navigateTo(path?: string) {
    return (path && path.length) ? browser.get(path) : browser.get('/PsycEXTRA/pubyear');
  }

  getPageHeading() {
    let heading = element(by.tagName('h1'));
    return heading.getText();
  }

  getBreadcrumbsPageTitle() {
    return element.all(by.css('breadcrumbs a')).get(0);
  }

  getBreadcrumbsCompnentTitle() {
    return element.all(by.css('breadcrumbs a')).get(1);
  }

  getBreadcrumbsYear() {
    return element.all(by.css('breadcrumbs a')).get(2);
  }

  getComponentHeading() {
    return element.all(by.css('div>strong')).get(0);
  }

  getBottomHeading() {
    return element.all(by.css('div>strong')).get(1);
  }

  getViewToggle() {
    return element.all(by.css('.card viewtoggle'));
  }

  getViewToggleButton() {
    return this.getViewToggle().first().element(by.css('a'));
  }

  getAccordion() {
    return element.all(by.css('.accordionContainer'));
  }

  getList() {
    return element.all(by.css('.listView'));
  }

  expandYear(index) {
    return element.all(by.css('.accordionHeading > a')).get(index).click();
  }

  collapseYear(index) {
    return element.all(by.css('.accordionHeading > a')).get(index).click();
  }

  getExpandedData() {
    return element.all(by.css('.accordionContainer > ul')).get(1);
  }

  getYearsDisplayed() {
    return element.all(by.css('.accordionContainer .accordionHeading'));
  }

  getYear(index) {
    return element.all(by.css('.accordionContainer > ul')).get(index).all(by.css('li'));
  }

  openSelectedYear(accordionOrder, yearOrder) {
    return element.all(by.css('.accordionContainer > ul > li:nth-child('+yearOrder+') a')).get(accordionOrder).click();
  }

  getRecordsTitle() {
     return element.all(by.css('article .titleLink'));
  }

  getPaginationLimit() {
    return element.all(by.css('paginationlimit'));
  }

  getJumpto() {
    return element.all(by.css('jumpto'));
  }

  getDisplayedRecordsCount() {
    return element.all(by.css('article')).count();
  }

  getDisplayedLimit() {
    return element.all(by.css('paginationlimit select')).get(0).getAttribute('ng-reflect-model');
  }

  changeLimitValue() {
    let limit = element.all(by.css('paginationlimit option')).get(1);
    return limit.click();
  }

  getPaginationComponent() {
    return element.all(by.css('numberedpagination')).first();
  }

  getPaginationLink() {
    return this.getPaginationComponent().all(by.css('div>ul>li>a'));
  }

  private getSearchInput() {
    return this.getJumpto().first().element(by.css('input[name="jumpTo"]'));
  }

  getSearchInputText() {
    let input = this.getSearchInput();
    return input.getAttribute('value');
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  executeSearch() {
    let searchBtn = this.getJumpto().first().element(by.css('button'));
    return searchBtn.click();
  }

  selectBrowseByPublicationYear() {
    let newBrowse = element(by.id('bypubyear'));
     return newBrowse.click();
  }

  selectBrowseByTopic() {
    let newBrowse = element(by.id('bytopic'));
    return newBrowse.click();
  }

  getEmptyPage() {
    return element(by.css('.emptyState'));
  }

  getTitles() {
    return element.all(by.css('article .titleLink'));
  }

  openRecord() {
    let bookTitle = element.all(by.css('article .titleLink')).get(0);
    return bookTitle.click();
  }

  getRecordUID() {
    return element.all(by.css('.recordUID')).get(0).getAttribute("textContent");
  }

  getMyListCount() {
    return element(by.css('personalization .badge')).getText();
  }

  addRecordToMyList() {
    return element.all(by.css('add-to-my-list button')).get(0).click();
  }

  removeRecordToMyList() {
    return element.all(by.css('add-to-my-list button')).get(0).click();
  }

  getMyListLabel() {
    return element.all(by.css('add-to-my-list button .caption')).get(0).click();
  }

  getPDFLink() {
    return element.all(by.css('.buttonBar > a')).get(0);
  }

  removePEPermission() {
    this.navigateTo('/admin');
    element(by.id('PE')).click();
    element(by.css('entitlements input[type=submit]')).click();
    this.navigateTo();
  }

  setUserAsIndividual() {
    //user is individual with no PE access permission
    this.navigateTo('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
    this.navigateTo();
  }

  revertPermissionChanges() {
    this.navigateTo('/admin');
    element(by.id('PE')).click();
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(1000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }
}
