import { BrowsePEYear } from './browsePEYear.po';
import { browser } from 'protractor';

describe('Browse PsycEXTRA Year Page', function() {
  let page: BrowsePEYear;

  beforeEach(() => {
    browser.manage().timeouts().implicitlyWait(9000);
    page = new BrowsePEYear();
    page.navigateTo();
  });

  it('should have PsycEXTRA heading', () => {
    expect(page.getPageHeading()).toEqual('PsycEXTRA');
  });

  it('should have \'Browse by Year\' in breadcrumbs', () => {
    expect(page.getBreadcrumbsCompnentTitle().getText()).toBe('Browse by Year');
  });

  it('should have \'Browse by Year\' heading', () => {
    let header = page.getComponentHeading();
    expect(header.isPresent()).toBe(true);
    expect(header.getText()).toContain('Browse by Year');
  });

  it('should have viewtoggle component', () => {
    expect(page.getViewToggle().count()).toBeGreaterThan(0);
  });

  it('should switch view if viewtoggle clicked', () => {
    expect(page.getAccordion().count()).toBeGreaterThan(0);
    page.getViewToggleButton().click().then(() => {
      expect(page.getAccordion().count()).toBe(0);
      expect(page.getList().count()).toBeGreaterThan(0);
      page.getViewToggleButton().click().then(() => {
        expect(page.getAccordion().count()).toBeGreaterThan(0);
        expect(page.getList().count()).toBe(0);
      })
    })
  });

  it('should expand and collapse some range of years', () => {
    page.expandYear(1);
    expect(page.getExpandedData().isPresent()).toBe(true);
    page.collapseYear(1);
    expect(page.getExpandedData().isPresent()).toBe(false);
  });

   it('should display max 10 list of years in each range (tested on the first 10 ranges)', () => {
     page.collapseYear(0);
    page.getYearsDisplayed().each((accordion, index) => {
      if(index <= 10) {
        accordion.click().then(() => {
          let count = page.getYear(index).count();
          expect(count).toBeLessThan(11)
        });
      }
    });
  });

  it('should open the selected year (2016) correctly', () => {
    page.openSelectedYear(0,1); //open the first year at the first accordion (2016)
    expect(browser.getCurrentUrl()).toContain('2016');
    browser.sleep(1500);
    expect(page.getBreadcrumbsYear().getText()).toBe('2016');
  });

  it('should display records published in the selected year (2016)', () => {
    page.openSelectedYear(0,1); //open the first year at the first accordion (2016)
    page.getRecordsTitle().each( title => {
      expect(title.getText()).toContain('2016');
     });
  });

  it('should have paginationLimit component at the selected year (2016)', () => {
    page.openSelectedYear(0,1); //open the first year at the first accordion (2017)
    expect(page.getPaginationLimit().count()).toBeGreaterThan(0);
  });

  it('should have jumpto component at the selected year (2016)', () => {
    page.openSelectedYear(0,1);
    expect(page.getJumpto().count()).toBeGreaterThan(0);
  });

  it('should display correct number of records based on the selected limit at first loading of the selected year (2016)', () => {
    let limit: number;
    page.openSelectedYear(0,1);
    browser.sleep(1000);
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedRecordsCount().then(count => {
        expect(count).toBeLessThan(limit+1);
      });
    });
  });

  it('should display pagination component if records are more than display limit at the selected year (2016)', () => {
    let totalItems: number;
    let limit: number;
    page.openSelectedYear(0,1);
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l=> {
      limit = parseInt(_l);
      page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records=> {
        totalItems = parseInt(records);
        expect(!!page.getPaginationLink().count()).toBe(totalItems > limit);
      });
    });
  });

  it('should display correct number of records when the limit change to be 50 at the selected year (2016)', () => {
    let limit: number;
    page.openSelectedYear(0,1);
    page.changeLimitValue();
    browser.sleep(1000);
    page.getDisplayedLimit().then(value => {
      limit = parseInt(value);
      page.getDisplayedRecordsCount().then(count => {
        expect(+count).toBeLessThan(limit+1);
      });
    });
  });

  it('should search topic by jumpto input at the selected year (2016)', () => {
    page.openSelectedYear(0,1);
    let searchFor = 'general';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(2000);
    page.getTitles().each(topic=> {
      topic.getText().then(name=> {
        expect(name.toLowerCase().indexOf(searchFor)).toBe(0);
      });
    });
  });

  it('should dipslay No record found  when user enters \'numbers\' in the jumpto input at the selected year (2016)', () => {
    page.openSelectedYear(0,1);
    let searchFor = 'numbers';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(1000);
    expect(page.getEmptyPage().isDisplayed()).toBe(true);
  });

  it('should open the correct record page at the selected year (2016)', () => {
    page.openSelectedYear(0,1);
    page.getRecordUID().then(uid => {
      page.openRecord();
      browser.sleep(1000);
      expect(browser.getCurrentUrl()).toContain('/record/'+ uid);
    });
  });

  it('should add record to My List correctly at the selected year (2016)', () => {
    let countBefore, countAfter;
    page.openSelectedYear(0,1);
    browser.sleep(1000);
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.addRecordToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore+1);
      });
    });
  });

  it('should remove record from My List correctly at the selected year (2016)', () => {
    let countBefore, countAfter;
    page.openSelectedYear(0,1);
    page.getMyListCount().then(countB =>{
      countBefore = parseInt(countB);
      page.removeRecordToMyList();
      browser.sleep(3000);
      page.getMyListCount().then(countA => {
        countAfter = parseInt(countA);
        expect(countAfter).toEqual(countBefore-1);
      });
    });
  });

  it('should have pdf link at the selected year (2016)', () => {
    page.openSelectedYear(0,1);
    expect(page.getPDFLink().isDisplayed()).toBe(true);
  });

  it('should open pdf at the selected year (2016)', () => {
    page.openSelectedYear(0,1);
    page.getRecordUID().then(uid => {
      page.getPDFLink().click();
      browser.getAllWindowHandles().then(function (handles) {
        let newWindowHandle = handles[1]; // this is the full text window
        browser.switchTo().window(newWindowHandle).then(function () {
          expect(browser.getCurrentUrl()).toContain('/fulltext/'+ uid +'.pdf');
        });
      browser.driver.close();
      browser.switchTo().window(handles[0]);
      });
    });
  });

  it('should navigate correct', () => {
    page.selectBrowseByTopic().then(() => {
        browser.wait(page.selectBrowseByPublicationYear(), 1000).then(() => {
         expect(browser.getCurrentUrl()).toContain('/pubyear');
      });
    });
  });

  it('should return user to search page if he has not PE access', () => {
    page.removePEPermission();
    browser.sleep(1500);
    expect(browser.getCurrentUrl()).toContain('/search');
  });

  it('should navigate to buy page if user is individual without PE access', () => {
    page.setUserAsIndividual();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/buy/PE');
  });

  afterAll(() =>  {
    page.revertPermissionChanges();
    browser.sleep(1000);
  });

});
