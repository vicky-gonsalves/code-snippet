import { BrowsePCReleaseDate } from './browsePCReleaseDate.po';
import { browser } from 'protractor';

describe('Browse PC by Release Date', () => {
  let page: BrowsePCReleaseDate;

  beforeEach(() => {
    page = new BrowsePCReleaseDate();
    page.navigate();

    browser.manage().timeouts().implicitlyWait(5000);
  });

  it('should load the correct initial page.', () => {
    page.isPageLoaded(isDisplayed => expect(isDisplayed).toBeTruthy());
  });

  it('should access years and months to get the release details.', () => {
    page.browseYearsAndWeeks(success => expect(success).toBeTruthy());
  });

  it('should access years and months, back and forth.', () => {
    page.switchBetweenYearsAndWeeks(success => expect(success).toBeTruthy());
  });

  it('should have records.', () => {
    page.getRecordsCount(count => expect(count).toBeGreaterThan(0));
  });

  it('should have an active menu-item within (Browse By) box.', () => {
    page.getBrowseByActiveItem(text => expect(text).toBe('Release Date'));
  });

  it('should have a release metadata.', () => {
    page.getReleaseMetaData(text => expect(text).toMatch(/\w* \d{1,2}, \d{4}: Volume \d{1,3}, Issue \d{1,2}/g));
  });

  it('should have a working release pagination.', () => {
    page.goToPreviousRelease(firstSuccess => {
      page.goToNextRelease(nextSuccess => expect(firstSuccess && nextSuccess).toBeTruthy());
    });
  });

  it('should display the record details.', () => {
    page.goToRecordDisplay(isDisplayed => expect(isDisplayed).toBeTruthy());
  });

  it('should display Full-Text HTML.', () => {
    page.goToFullTextHTML(isDisplayed => expect(isDisplayed).toBeTruthy());
  });

  it('should have a working (Add to My List) button.', () => {
    page.addToMyList(success => expect(success).toBeTruthy());
  });

  it('should redirect any individual users, who do not have (PC) entitlement, to the (Options to Buy) page for (PC).', () => {
    page.accessAsIndividualUser(success => expect(success).toBeTruthy());
  });

  it('should redirect any guest users, to the (Options to Buy) page for (PC).', () => {
    page.accessAsGuestUser(success => expect(success).toBeTruthy());
  });

  it('should not allow any organization users, who do not have (PC) entitlement, to access this page.', () => {
    page.accessAsOrganizationUser(success => expect(success).toBeFalsy());
  });
});
