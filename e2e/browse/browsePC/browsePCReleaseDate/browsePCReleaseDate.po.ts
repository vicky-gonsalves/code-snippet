import { browser, element, by } from 'protractor';

export class BrowsePCReleaseDate {
  navigate(path?: string) {
    browser.get(path || '/PsycCRITIQUES/releasedate/issue/20050112');
  }

  isPageLoaded(cb) {
    this.navigate('/PsycCRITIQUES/releasedate');
    browser.sleep(3000);

    element(by.css('browsepcreleasedate')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  browseYearsAndWeeks(cb) {
    this.navigate('/PsycCRITIQUES/releasedate');
    browser.sleep(3000);

    element(by.css('.year-item:nth-child(5) a')).getText().then(yearText => {
      yearText = yearText.substr(0, yearText.indexOf(' ('));
      element(by.css('.year-item:nth-child(5) a')).click();
      browser.sleep(1000);

      element(by.css('.week-item:nth-child(8) a')).getText().then(weekText => {
        weekText = weekText.substr(0, weekText.indexOf(' ('));
        element(by.css('.week-item:nth-child(8) a')).click();
        element(by.css('browsepclatestrelease .release-meta')).getText().then(metaText => {
          metaText = metaText.replace(',', '');
          cb( (weekText.indexOf(yearText) >= 0) && (metaText.indexOf(weekText) === 0) );
        });
      });
    });
  }

  switchBetweenYearsAndWeeks(cb) {
    this.navigate('/PsycCRITIQUES/releasedate');
    browser.sleep(3000);

    element.all(by.css('.year-item')).count().then(yearsCount => {
      element(by.css('.year-item:nth-child(5) a')).click();
      browser.sleep(1000);

      element.all(by.css('.week-item')).count().then(weeksCount => {
        element(by.css('.backLink')).click();
        browser.sleep(1000);

        element.all(by.css('.year-item')).count().then(yearsCountAgain => {
          cb(+yearsCount && (yearsCount === yearsCountAgain) && +weeksCount);
        });
      });
    });
  }

  getRecordsCount(cb) {
    element.all(by.css('article')).count().then(count => cb(count));
  }

  getBrowseByActiveItem(cb) {
    element(by.css('browsebyboxpc .active')).getText().then(text => cb(text));
  }

  getReleaseMetaData(cb) {
    element(by.css('.release-meta')).getText().then(text => cb(text));
  }

  goToPreviousRelease(cb) {
    element(by.css('.release-meta')).getText().then(currentRelease => {
      element.all(by.css('.visible-md > pagerpagination li:first-child a')).first().click();
      browser.sleep(2000);

      element(by.css('.release-meta')).getText().then(previousRelease => {
        cb(currentRelease !== previousRelease);
      });
    });
  }

  goToNextRelease(cb) {
    element(by.css('.release-meta')).getText().then(currentRelease => {
      element.all(by.css('.visible-md > pagerpagination li:last-child a')).first().click();
      browser.sleep(2000);

      element(by.css('.release-meta')).getText().then(nextRelease => {
        cb(currentRelease !== nextRelease);
      });
    });
  }

  goToRecordDisplay(cb) {
    element.all(by.css('.review-title')).first().click();
    element(by.css('recorddisplay')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  goToFullTextHTML(cb) {
    element.all(by.css('.fullTextLink')).first().click();
    element(by.css('fulltexthtml')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  addToMyList(cb) {
    element.all(by.css('add-to-my-list')).first().click();
    browser.sleep(2000);

    this.getBadgeValue().then(newCount => {
      element.all(by.css('add-to-my-list')).first().click();
      browser.sleep(2000);

      this.getBadgeValue().then(oldCount => cb((+newCount === (+oldCount) + 1) || (+newCount === (+oldCount) - 1)));
    });
  }

  accessAsGuestUser(cb) {
    this.setUserAsAnonymous();
    this.navigate();
    element(by.css('day-pass-option .pc.title')).isDisplayed().then(success => {
      this.setUserAsAnonymous(true);
      cb(success);
    });
  }

  accessAsIndividualUser(cb) {
    this.setUserAsIndividual();
    this.navigate();
    element(by.css('day-pass-option .pc.title')).isDisplayed().then(success => {
      this.setUserAsIndividual(true);
      cb(success);
    });
  }

  accessAsOrganizationUser(cb) {
    this.togglePCEntitlement();
    this.navigate();
    element(by.css('day-pass-option .pc.title')).isPresent().then(success => {
      this.togglePCEntitlement();
      cb(success);
    });
  }

  private getBadgeValue() {
    return element(by.css('.navigation-menu .badge')).getText();
  }

  private setUserAsAnonymous(revert?: boolean) {
    this.navigate('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    if (revert) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');
      element(by.css('identitymanagement input[type=submit]')).click();

      browser.sleep(1000);

      element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
      for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
        element(by.id(thing)).click();
      }
      element(by.css('entitlements input[type=submit]')).click();

    } else {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');
      element(by.css('identitymanagement input[type=submit]')).click();
    }

    browser.sleep(1000);
  }

  private setUserAsIndividual(revert?: boolean) {
    this.navigate('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    let value = revert ? '329008' : '-1';

    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys(value);
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);

    this.togglePCEntitlement();
  }

  private togglePCEntitlement() {
    this.navigate('/admin');

    element(by.id('PC')).click();
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(1000);
  }
}
