import { browser, element, by } from 'protractor';

export class BrowsePCReviewByTitle {
  navigate(path?: string) {
    browser.get(path || '/PsycCRITIQUES/reviewetitle');
  }

  isPageLoaded(cb) {
    element(by.css('browsepcreviewbytitle')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  getRecordsCount(cb) {
    element.all(by.css('article')).count().then(count => cb(count));
  }

  recordsWithinLimitUsingPagination(cb) {
    element.all(by.css('article')).count().then(count => {
      element.all(by.css('paginationlimit select > option:first-child')).first().getText().then(limitText => {
        element.all(by.css('.visible-md numberedpagination')).first().isDisplayed().then(displayed => cb(displayed && count === +limitText));
      });
    });
  }

  getBrowseByActiveItem(cb) {
    element(by.css('browsebyboxpc .active')).getText().then(text => cb(text));
  }

  usePagination(cb) {
    element(by.css('article:first-child .review-title')).getText().then(page1FirstTitle => {
      element.all(by.css('.visible-md numberedpagination')).first().element(by.id('page-2')).click();
      element(by.css('article:first-child .review-title')).getText().then(page2FirstTitle => cb(page1FirstTitle !== page2FirstTitle));
    });
  }

  useAlphaStrip(cb) {
    let prefixIsMatched = true;

    // Letter (K)
    element.all(by.css('browseaz.visible-sm li:nth-child(11) a')).first().click();
    browser.sleep(1000);

    element(by.css('article:first-child .review-title')).getText().then(firstTitle => {
      prefixIsMatched = (prefixIsMatched && firstTitle.indexOf('K') === 0);

      element(by.css('article:last-child .review-title')).getText().then(lastTitle => {
        prefixIsMatched = (prefixIsMatched && lastTitle.indexOf('K') === 0);
        cb(prefixIsMatched);
      });
    });
  }

  usePaginationLimit(cb) {
    let select = element.all(by.css('paginationlimit select')).first();
    select.click();

    let lastOption = select.element(by.css('option:last-child'));
    lastOption.getText().then(optionCount => {
      lastOption.click();
      browser.sleep(1000);

      element.all(by.css('article')).count().then(itemsCount => cb(itemsCount <= +optionCount));
    });
  }

  useJumpToFiltering(cb) {
    let searchTerm = 'Ubiquitous Aggression';
    element.all(by.css('jumpto input')).get(0).sendKeys(searchTerm);
    element.all(by.css('jumpto button')).get(0).click();
    browser.sleep(1000);

    element(by.css('article:first-child .review-title')).getText().then(firstTitle => {
      element.all(by.css('article:last-child .review-title')).last().getText().then(lastTitle => {
        let isMatched = (firstTitle.indexOf(searchTerm) === 0) && (lastTitle.indexOf(searchTerm) === 0);
        cb(isMatched);
      });
    });
  }

  goToRecordDisplay(cb) {
    element(by.css('article:first-child .review-title')).click();
    element(by.css('recorddisplay')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  goToFullTextHTML(cb) {
    element(by.css('article:first-child .fullTextLink')).click();
    element(by.css('fulltexthtml')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  addToMyList(cb) {
    element(by.css('article:first-child add-to-my-list')).click();
    browser.sleep(2000);

    this.getBadgeValue().then(newCount => {
      element.all(by.css('article:first-child add-to-my-list')).first().click();
      browser.sleep(2000);

      this.getBadgeValue().then(oldCount => cb( (+newCount === (+oldCount) + 1) || (+newCount === (+oldCount) - 1) ));
    });
  }

  accessAsGuestUser(cb) {
    this.setUserAsAnonymous();
    this.navigate();
    element(by.css('day-pass-option .pc.title')).isDisplayed().then(success => {
      this.setUserAsAnonymous(true);
      cb(success);
    });
  }

  accessAsIndividualUser(cb) {
    this.setUserAsIndividual();
    this.navigate();
    element(by.css('day-pass-option .pc.title')).isDisplayed().then(success => {
      this.setUserAsIndividual(true);
      cb(success);
    });
  }

  accessAsOrganizationUser(cb) {
    this.togglePCEntitlement();
    this.navigate();
    element(by.css('advanced')).isPresent().then(() => {
      this.togglePCEntitlement();
      cb(false);
    });
  }

  private getBadgeValue() {
    return element(by.css('.navigation-menu .badge')).getText();
  }

  private setUserAsAnonymous(revert?: boolean) {
    this.navigate('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    if (revert) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');
      element(by.css('identitymanagement input[type=submit]')).click();

      browser.sleep(1000);

      element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
      for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
        element(by.id(thing)).click();
      }
      element(by.css('entitlements input[type=submit]')).click();

    } else {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');
      element(by.css('identitymanagement input[type=submit]')).click();
    }

    browser.sleep(1000);
  }

  private setUserAsIndividual(revert?: boolean) {
    this.navigate('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    let value = revert ? '329008' : '-1';

    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys(value);
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);

    this.togglePCEntitlement();
  }

  private togglePCEntitlement() {
    this.navigate('/admin');

    element(by.id('PC')).click();
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(1000);
  }
}
