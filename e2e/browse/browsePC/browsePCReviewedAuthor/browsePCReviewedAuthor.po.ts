import { browser, element, by } from 'protractor';

export class BrowsePCReviewedAuthor {
  navigate(path?: string) {
    browser.get(path || '/PsycCRITIQUES/reviewedauthor');
  }

  isPageLoaded(cb) {
    element(by.css('browsePCReviewedAuthor')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  getRecordsCount(cb) {
    element.all(by.css('.reviewer')).count().then(count => cb(count));
  }

  getBrowseByActiveItem(cb) {
    element(by.css('browsebyboxpc .active')).getText().then(text => cb(text));
  }

  usePagination(cb) {
    let nextPage = element.all(by.css('.visible-md termbasedpagination a:last-child .term')).first();
    nextPage.getText().then(nextAuthor => {
      nextAuthor = nextAuthor.replace('...', '');
      nextPage.click();
      browser.sleep(1000);

      element(by.css('.reviewer:first-child a')).getText().then(Page2FirstAuthor => {

        let previousPage = element.all(by.css('.visible-md termbasedpagination a:first-child .term')).first();
        previousPage.getText().then(previousAuthor => {
          previousAuthor = previousAuthor.replace('...', '');
          previousPage.click();
          browser.sleep(1000);

          element(by.css('.reviewer:last-child a')).getText().then(Page1LastAuthor => {
            cb((Page1LastAuthor.indexOf(previousAuthor) === 0) && (Page2FirstAuthor.indexOf(nextAuthor) === 0));
          });
        });
      });
    });
  }

  useAlphaStrip(cb) {
    let prefixIsMatched = true;

    // Letter (K)
    element.all(by.css('browseaz.visible-md li:nth-child(11) a')).first().click();
    browser.sleep(1000);

    element.all(by.css('.reviewer a')).first().getText().then(firstText => {
      prefixIsMatched = (prefixIsMatched && firstText.indexOf('K') === 0);

      element.all(by.css('.reviewer a')).last().getText().then(lastText => {
        prefixIsMatched = (prefixIsMatched && lastText.indexOf('K') === 0);
        cb(prefixIsMatched);
      });
    });
  }

  usePaginationLimit(cb) {
    let select = element.all(by.css('paginationlimit select')).first();
    select.click();

    let lastOption = select.element(by.css('option:last-child'));
    lastOption.getText().then(optionCount => {
      lastOption.click();
      browser.sleep(1000);

      element.all(by.css('.reviewer a')).count().then(itemsCount => cb(itemsCount <= +optionCount));
    });
  }

  useJumpToFiltering(cb) {
    let searchTerm = 'Quick';
    element.all(by.css('jumpto input')).get(0).sendKeys(searchTerm);
    element.all(by.css('jumpto button')).get(0).click();
    browser.sleep(1000);

    element.all(by.css('.reviewer a')).first().getText().then(firstText => {
      element.all(by.css('.reviewer a')).last().getText().then(lastText => {
        let isMatched = (firstText.indexOf(searchTerm) === 0) && (lastText.indexOf(searchTerm) === 0);
        cb(isMatched);
      });
    });
  }

  goToRecordDisplay(cb) {
    element.all(by.css('.reviewer a')).first().click();
    element(by.css('.reviewer .reviewer-works .resultData')).click();
    element(by.css('recorddisplay')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  goToFullTextHTML(cb) {
    element.all(by.css('.reviewer a')).first().click();
    element(by.css('.reviewer .reviewer-works .fullTextLink')).click();
    element(by.css('fulltexthtml')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  addToMyList(cb) {
    element.all(by.css('.reviewer a')).first().click();
    element(by.css('.reviewer .reviewer-works add-to-my-list')).click();
    browser.sleep(2000);

    this.getBadgeValue().then(newCount => {
      element.all(by.css('.reviewer .reviewer-works add-to-my-list')).first().click();
      browser.sleep(2000);

      this.getBadgeValue().then(oldCount => cb( (+newCount === (+oldCount) + 1) || (+newCount === (+oldCount) - 1) ));
    });
  }

  accessAsGuestUser(cb) {
    this.setUserAsAnonymous();
    this.navigate();
    element(by.css('day-pass-option .pc.title')).isDisplayed().then(success => {
      this.setUserAsAnonymous(true);
      cb(success);
    });
  }

  accessAsIndividualUser(cb) {
    this.setUserAsIndividual();
    this.navigate();
    element(by.css('day-pass-option .pc.title')).isDisplayed().then(success => {
      this.setUserAsIndividual(true);
      cb(success);
    });
  }

  accessAsOrganizationUser(cb) {
    this.togglePCEntitlement();
    this.navigate();
    element(by.css('day-pass-option .pc.title')).isPresent().then(success => {
      this.togglePCEntitlement();
      cb(success);
    });
  }

  private getBadgeValue() {
    return element(by.css('.navigation-menu .badge')).getText();
  }

  private setUserAsAnonymous(revert?: boolean) {
    this.navigate('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    if (revert) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');
      element(by.css('identitymanagement input[type=submit]')).click();

      browser.sleep(1000);

      element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
      for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
        element(by.id(thing)).click();
      }
      element(by.css('entitlements input[type=submit]')).click();

    } else {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');
      element(by.css('identitymanagement input[type=submit]')).click();
    }

    browser.sleep(1000);
  }

  private setUserAsIndividual(revert?: boolean) {
    this.navigate('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    let value = revert ? '329008' : '-1';

    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys(value);
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);

    this.togglePCEntitlement();
  }

  private togglePCEntitlement() {
    this.navigate('/admin');

    element(by.id('PC')).click();
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(1000);
  }
}
