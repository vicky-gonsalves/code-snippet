import { browser } from 'protractor';
import { BrowsePCReviewedAuthor } from './browsePCReviewedAuthor.po';

describe('Browse PC by Reviewed Item Author', () => {
  let page: BrowsePCReviewedAuthor;

  beforeEach(() => {
    page = new BrowsePCReviewedAuthor();
    page.navigate();
    browser.manage().timeouts().implicitlyWait(5000);
  });

  it('should load the correct page.', () => {
    page.isPageLoaded(isDisplayed => expect(isDisplayed).toBeTruthy());
  });

  it('should have records.', () => {
    page.getRecordsCount(count => expect(count).toBeGreaterThan(0));
  });

  it('should have an active menu-item within (Browse By) box.', () => {
    page.getBrowseByActiveItem(text => expect(text).toBe('Reviewed Item Author'));
  });

  it('should have a working pagination.', () => {
    page.usePagination(success => expect(success).toBeTruthy());
  });

  it('should have a working alphabet strip.', () => {
    page.useAlphaStrip(success => expect(success).toBeTruthy());
  });

  it('should have a working pagination limit.', () => {
    page.usePaginationLimit(success => expect(success).toBeTruthy());
  });

  it('should have a working jump-to filtering.', () => {
    page.useJumpToFiltering(success => expect(success).toBeTruthy());
  });

  it('should display the record details.', () => {
    page.goToRecordDisplay(isDisplayed => expect(isDisplayed).toBeTruthy());
  });

  it('should display Full-Text HTML.', () => {
    page.goToFullTextHTML(isDisplayed => expect(isDisplayed).toBeTruthy());
  });

  it('should have a working (Add to My List) button.', () => {
    page.addToMyList(success => expect(success).toBeTruthy());
  });

  it('should redirect any individual users, who do not have (PC) entitlement, to the (Options to Buy) page for (PC).', () => {
    page.accessAsIndividualUser(success => expect(success).toBeTruthy());
  });

  it('should redirect any guest users, to the (Options to Buy) page for (PC).', () => {
    page.accessAsGuestUser(success => expect(success).toBeTruthy());
  });

  it('should not allow any organization users, who do not have (PC) entitlement, to access this page.', () => {
    page.accessAsOrganizationUser(success => expect(success).toBeFalsy());
  });
});
