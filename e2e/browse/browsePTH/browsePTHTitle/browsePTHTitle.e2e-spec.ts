import {BrowsePTHPage} from './browsePTHTitle.po';
import {browser} from 'protractor';

describe('Browse PsycTherapy Page - Title', function () {
  let page: BrowsePTHPage;

  beforeEach(() => {
    browser.manage().timeouts().implicitlyWait(90000);
    page = new BrowsePTHPage();
    page.navigateTo();
  });

  it('should have breadcrumbs', () => {
    let bradcrumbs = page.getBreadcrumbs();
    expect(bradcrumbs.first().getText()).toEqual('PsycTHERAPY');
    expect(bradcrumbs.last().getText()).toEqual('Browse by Title');
  });

  it('should have PsycTHERAPY heading', () => {
    expect(page.getPageHeading()).toEqual('PsycTHERAPY');
  });

  it('should have description', () => {
    expect(page.getPageDescription()).toEqual('Streaming psychotherapy demonstration videos. Create playlists, clips, search transcripts of videos, and more.');
  });

  it('should have BrowseBy filter heading', () => {
    expect(page.getBrowsePTHByBoxHeading()).toEqual('Browse By');
  });

  it('should have BrowseBy filters', () => {
    expect(page.getBrowsePTByBoxFilters().count()).toEqual(4);
  });

  it('should have browseaz component', () => {
    expect(page.getBrowseAZ().count()).toBeGreaterThan(0);
  });

  it('should have paginationLimit component', () => {
    expect(page.getPaginationLimit().count()).toBeGreaterThan(0);
  });

  it('should have jumpto component', () => {
    expect(page.getJumpto().count()).toBeGreaterThan(0);
  });

  it('should display correct number of records', () => {
    let limit: number;
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l => {
      limit = parseInt(_l);
      expect(page.getArticlesDisplayed().count()).toBe(limit);
    });
  });

  it('should display pagination component if records are more than display limit', () => {
    let totalItems: number;
    let limit: number;
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l => {
      limit = parseInt(_l);
      page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records => {
        totalItems = parseInt(records);
        expect(!!page.getPaginationLink().count()).toBe(totalItems > limit);
      });
    });
  });

  it('should display records starting with A when browseAZ clicked', () => {
    page.getBrowseAZButtons().first().click().then(() => {
      browser.sleep(3000);
      page.getArticlesText().each(record => {
        record.getText().then(text => {
          expect(text.charAt(2).toLowerCase()).toBe('a');
        })
      })
    });
  });

  it('should display records starting with B when browseAZ clicked', () => {
    page.getBrowseAZButtons().get(1).click().then(() => {
      browser.sleep(3000);
      page.getArticlesText().each(record => {
        record.getText().then(text => {
          expect(text.charAt(2).toLowerCase()).toBe('b');
        })
      })
    });
  });

  it('should search record by jumpto input', () => {
    let searchFor = 'career and vocational counseling, client #3';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(3000);
    page.getArticlesText().each(record => {
      record.getText().then(text => {
        expect(text.toLowerCase().indexOf(searchFor)).toBe(2);
      })
    })
  });

  it('should open the correct page when link is clicked', () => {
    let searchFor = 'career and vocational counseling, client #3';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(3000);
    page.getArticlesText().first().click().then(record => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/psyctherapy.apa.org\/apa\/view\/777700412-001/g).test(url);
        expect(regexTest).toBe(true);
      })
    })
  });

  it('should block non-entitled organizational users from viewing the page.', () => page.checkAccessIsDenied('organization'));
  it('should block individual users from viewing the page.', () => page.checkAccessIsDenied('individual'));
  it('should block guest users from viewing the page.', () => page.checkAccessIsDenied());
});
