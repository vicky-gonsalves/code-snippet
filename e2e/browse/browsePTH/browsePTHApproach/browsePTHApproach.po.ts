import {browser, element, by} from 'protractor';

export class BrowsePTHPage {
  navigateTo() {
    return browser.get('/PsycTHERAPY/approach');
  }

  getPageHeading() {
    let heading = element(by.css('browsepagesubheader h1'));
    return heading.getText();
  }

  getPageDescription() {
    let desc = element(by.css('browsepagesubheader p'));
    return desc.getText();
  }

  getBreadcrumbs() {
    let breadcrumbs = element.all(by.css('.breadcrumb li a'));
    return breadcrumbs;
  }

  getBrowsePTHByBoxHeading() {
    let browseByHeading = element(by.css('browsepthbybox .card h2'));
    return browseByHeading.getText();
  }

  getBrowsePTByBoxFilters() {
    return element.all(by.css('browsepthbybox .card a span'));
  }

  getBrowseAZ() {
    return element.all(by.css('browseaz'));
  }

  getBrowseAZButtons() {
    return this.getBrowseAZ().first().all(by.css('div>ul>li>a'));
  }

  getPaginationLimit() {
    return element.all(by.css('paginationlimit'));
  }

  getPaginationComponent() {
    return element.all(by.css('termBasedPagination')).first();
  }

  getPaginationLink() {
    return this.getPaginationComponent().all(by.css('div>ul>li>a'));
  }

  getArticlesDisplayed() {
    return element.all(by.css('.accordionContainer .accordionHeading'));
  }

  getArticlesText() {
    return element.all(by.css('.accordionContainer .accordionHeading>a>strong'));
  }

  getArticlesLink() {
    return element.all(by.css('.accordionContainer .accordionHeading>a'));
  }

  getFirstLink() {
    return element.all(by.css('.accordionContainer ul')).first().all(by.css('li>div>a'));
  }

  getJumpto() {
    return element.all(by.css('jumpto'));
  }

  private getSearchInput() {
    return this.getJumpto().first().element(by.css('input[name="jumpTo"]'));
  }

  executeSearch() {
    let searchBtn = this.getJumpto().first().element(by.css('button'));
    return searchBtn.click();
  }

  getSearchInputText() {
    let input = this.getSearchInput();
    return input.getAttribute('value');
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  checkAccessIsDenied(role?: string) {
    if(role === 'organization') {
      this.togglePHEntitlement();
    } else if (role === 'individual') {
      this.togglePHEntitlement();
      this.setUser('12345', '-1');
    } else {
      this.setUser('-1');
    }

    this.navigateTo();
    expect(element(by.css('no-access')).isDisplayed()).toBeTruthy();

    if(role === 'organization') {
      this.togglePHEntitlement();
    } else if (role === 'individual') {
      this.togglePHEntitlement();
      this.setUser('12345', '329008');
    } else {
      this.restoreDefaultUserFromGuest();
    }
  }

  private togglePHEntitlement() {
    browser.get('/admin');
    browser.sleep(2000);

    element(by.id('PH')).click();
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(2000);
  }

  private setUser(userId: string, orgId?: string) {
    browser.get('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys(userId);

    if (userId === '-1' || orgId === '-1') {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');
    } else if (orgId && orgId.length) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys(orgId);
    }

    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }

  private restoreDefaultUserFromGuest() {
    this.setUser('12345', '329008');

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
    browser.sleep(1000);
    for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
      element(by.id(thing)).click();
    }
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(2000);
  }
}
