import { browser, element, by } from 'protractor';

export class Thesaurus {

  navigateTo() {
    return browser.get('/thesaurus');
  }

  getPageHeading() {
    let heading = element(by.tagName('h1'));
    return heading.getText();
  }

  getLookUp() {
    return element(by.css('.lookUp'));
  }

  getBrowseAZ() {
    return element.all(by.css('browseaz'));
  }

  getPaginationComponent() {
    return element.all(by.css('termBasedPagination')).first();
  }

  getPaginationLink() {
    return this.getPaginationComponent().all(by.css('div>ul>li>a'));
  }

  getSelectedLetter(orderLetter) {
    let browseButton =  element.all(by.css('browseaz')).get(0).element(by.css('li:nth-child('+orderLetter+') a'));
    return browseButton.click();
  }

  getTermsText() {
    return element.all(by.css('.accordionContainer .term'));
  }

  getTermsDisplayed() {
    return element.all(by.css('.accordionHeading > a:first-child'));
  }

  getInnerDetails(index) {
    return element.all(by.css('.innerLayout')).get(index);
  }

  private getSearchInput() {
    return this.getLookUp().element(by.css('input[name="term"]'));
  }

  getSearchInputText() {
    let input = this.getSearchInput();
    return input.getAttribute('ng-reflect-model');
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  executeSearch() {
    let searchBtn = this.getLookUp().element(by.css('button'));
    return searchBtn.click();
  }

  getRelatedTerm(index) {
    return element.all(by.css('.inlineAnchor:nth-child(2)')).get(index);
  }

  getNextPageLabel() {
    return element.all(by.css('termBasedPagination ul li:first-child a span:last-child')).get(1).getText();
  }

  navigateToNextPage() {
    return element.all(by.css('termBasedPagination ul li:first-child a')).get(1).click();
  }

  getPrevPageLabel() {
    return element.all(by.css('termBasedPagination ul li:first-child a span:first-child')).get(1).getText();
  }

  navigateToPrevPage() {
    return element.all(by.css('termBasedPagination ul li:first-child a')).get(1).click();
  }

  getSearchBoxComponent() {
    return element.all(by.css('search-box-thesaurus'));
  }

  getTermsFromSearchBox() {
    return this.getSearchBoxComponent().get(1).all(by.css('.selection > ul li span'));
  }

  selectTerm(index) {
    return element.all(by.css('label>input')).get(index);
  }

  selectMainTerm(index) {
    return element.all(by.css('span>input')).get(index);
  }

  getTermToDelete(index) {
    return this.getSearchBoxComponent().get(1).all(by.css('.selection > ul i'));
  }

  getAddToSearchButton() {
    return this.getSearchBoxComponent().get(1).element(by.css('button'));
  }

  getClearAllButton() {
    return this.getSearchBoxComponent().get(1).element(by.css('a'));
  }

  getNarrowerTerms() {
    return element.all(by.css('.narrowerTerm input'));
  }

  getNarrowerButton() {
    return element.all(by.css('.rightAligned'));
  }

  getDisplayedOperator() {
    return this.getSearchBoxComponent().get(1).all(by.css('select')).get(0).getAttribute('ng-reflect-model');
  }

  changeOperatorValue() {
    let operator = this.getSearchBoxComponent().get(1).all(by.css('option')).get(1);
    return operator.click();
  }

  formulateSearchTerm(searchTerm, newTerm) {
    if(newTerm && newTerm.length) {
      newTerm = '{'+newTerm+'} ';
    }
    if(searchTerm && searchTerm.length && newTerm && newTerm.length) {
      searchTerm += 'op' + ' ' + newTerm;
    } else if(!searchTerm && !searchTerm.length && newTerm && newTerm.length) {
       searchTerm = newTerm;
    }
    return searchTerm;
  }

  getSearchText() {
    return element(by.id('searchField')).getAttribute('ng-reflect-model');
  }

  getSearchField() {
    return element(by.id('fields')).getAttribute('ng-reflect-model');
  }

  removeOrganiztionIdintity() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
    this.navigateTo();
  }

  removeIndividualIdintity() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
    this.navigateTo();
  }

  setOrganiztionIdintity() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }

  getAllEntitlements() {
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
    let entitlements = ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH', 'ASC'];
    for (let entitlement of entitlements) {
      element(by.id(entitlement)).click();
    }
    element(by.css('entitlements input[type=submit]')).click();
  }

  removeEntitlements() {
    this.getAllEntitlements();
    browser.sleep(1000);
    this.navigateTo();
  }

  revertIdentityAndEntitlements() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');
    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
    this.getAllEntitlements();
    browser.sleep(1000);
  }
}
