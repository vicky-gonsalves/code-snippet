import { Thesaurus  } from './thesaurus.po';
import { browser } from 'protractor';

describe('Thesaurus Page', function() {
  let page: Thesaurus ;

  beforeEach(() => {
    page = new Thesaurus ();
    page.navigateTo();
    browser.manage().timeouts().implicitlyWait(9000);
  });

  it('should have APA Thesaurus heading', () => {
    expect(page.getPageHeading()).toEqual('APA Thesaurus');
  });

  it('should have Look Up Term Box', () => {
    expect(page.getLookUp().isDisplayed()).toBe(true);
  });

  it('should have browseaz component', () => {
    expect(page.getBrowseAZ().count()).toBeGreaterThan(0);
  });

  it('should display pagination component if records are more than 15', () => {
    let totalItems: number;
    page.getPaginationComponent().getAttribute('ng-reflect-total-items').then(records=> {
      totalItems = parseInt(records);
      expect(!!page.getPaginationLink().count()).toBe(totalItems > 15);
    });
  });

  it('should not display pagination component when letter X is selected from the alphabet strip', () => {
    page.getSelectedLetter(24); //Select letter X from the alphabet strip
    expect(page.getPaginationComponent().isDisplayed()).toBe(false);
  });

  // it('should display records starting with X when letter X is selected from the alphabet strip', () => {
  //   page.getSelectedLetter(24);
  //   browser.sleep(1000);
  //   page.getTermsText().each(term=> {
  //     term.getText().then(text=> {
  //       expect(text.charAt(0)).toBe('X');
  //     });
  //   });
  // });

  it('should expand and collapse Terms', () => {
    page.getTermsDisplayed().each((term, index) => {
      term.click().then(() => {
        expect(page.getInnerDetails(index).getAttribute('ng-reflect-hidden')).toBe(null);
      });
    });
  });

  it('should search record by Look Up search box', () => {
   let searchFor = 'child';
    page.setSearchInputText(searchFor);
    expect(page.getSearchInputText()).toEqual(searchFor);
    page.executeSearch();
    browser.sleep(1000);
    page.getTermsText().each(term=> {
      term.getText().then(text=> {
        expect(text.toLowerCase().indexOf(searchFor)).toBeGreaterThan(-1);
      });
    });
  });

  it('should open the correct term when related term is clicked', () => {
    page.getTermsDisplayed().get(0).click();
    page.getRelatedTerm(0).getText().then(term => {
      page.getRelatedTerm(0).click();
      page.getTermsText().get(0).getText().then(relatedTerm =>{
        expect(relatedTerm).toBe(term.trim());
      });
    });
  });

  it('should navigate to the next page correctly', () => {
    let nextRecord;
    page.getNextPageLabel().then(text => {
      nextRecord = text;
      browser.wait(page.navigateToNextPage(),3000).then(() => {
        page.getTermsText().first().getText().then(term => {
          expect(term).toContain(nextRecord);
        });
      });
    });
  });

  it('should navigate to the previous page correctly', () => {
    let prevLabel;
    browser.wait(page.navigateToNextPage(),3000).then(() => {
      page.getPrevPageLabel().then(text => {
        prevLabel = text;
        browser.wait(page.navigateToPrevPage(),3000).then(() => {
          page.getTermsText().last().getText().then(term => {
            expect(term).toContain(prevLabel);
          });
        });
      });
    });
  });

  it('should have Selected Terms Box', () => {
    expect(page.getSearchBoxComponent().count()).toBeGreaterThan(0);
  });

  it('should add/remove terms for search', () => {
    page.getTermsDisplayed().get(0).click();
    // add 3 related terms to search
    page.selectTerm(0).click();
    page.selectTerm(1).click();
    page.selectTerm(2).click();
    expect(page.getTermsFromSearchBox().count()).toBe(3);
    // remove 3 terms from search
    page.selectTerm(0).click();
    page.selectTerm(1).click();
    page.selectTerm(2).click();
    expect(page.getTermsFromSearchBox().count()).toBe(0);
  });

  it('should remove terms from the Selected Terms Box', () => {
    page.getTermsDisplayed().get(0).click();
    // add one related term to search
    page.selectTerm(0).click();
    expect(page.getTermsFromSearchBox().count()).toBe(1);
    // remove term from the search box (by clicking on the red X)
    page.getTermToDelete(0).click();
    expect(page.getTermsFromSearchBox().count()).toBe(0);
    expect(page.selectTerm(0).getAttribute('ng-reflect-model')).toBe('false');
  });

  it('should navigate to search page when \'ADD TO SEARCH\' button clicked', () => {
    page.getTermsDisplayed().get(0).click();
    // add 2 related terms to search
    page.selectTerm(0).click();
    page.selectTerm(1).click();
    page.getAddToSearchButton().click();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/search');
  });

  it('should remove all selected term when the \'clear all\' button is clicked', () => {
    page.getTermsDisplayed().get(0).click();
    // add 2 related terms to search
    page.selectTerm(0).click();
    page.selectTerm(1).click();
    page.selectTerm(2).click();
    expect(page.getTermsFromSearchBox().count()).toBe(3);
    page.getClearAllButton().click();
    expect(page.getTermsFromSearchBox().count()).toBe(0);
  });

  it('should formulate the terms for search and populate the advanced search form correctly', () => {
    let searchTerm ='';
    page.getTermsDisplayed().get(0).click();
    // add 2 related terms to search with AND operator
    page.selectTerm(0).click();
    page.selectTerm(1).click();
    page.getTermsFromSearchBox().each(term =>{
      term.getText().then(text=> {
        searchTerm = page.formulateSearchTerm(searchTerm, text);
      });
    }).then(() => {
        page.changeOperatorValue();
        page.getDisplayedOperator().then(op =>{
          searchTerm =searchTerm.replace ('op',op);
          page.getAddToSearchButton().click();
          browser.sleep(2000);
          expect(page.getSearchField()).toBe("IndexTerms");
          expect(page.getSearchText()).toBe(searchTerm);
        });
      });
  });

  it('should add term and narrower terms to search if narrower button is clicked', () => {
    page.getNarrowerButton().click();
    expect(page.selectMainTerm(4).getAttribute('ng-reflect-model')).toBe('true');
    page.getNarrowerTerms().each(term => {
      expect(term.getAttribute('ng-reflect-model')).toBe('true');
    });
  });

  it('should not allow any individual users to access this page.', () => {
    page.removeOrganiztionIdintity();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/search');
  });

  it('should not allow any guest users to access this page.', () => {
    page.removeOrganiztionIdintity();
    page.removeIndividualIdintity();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/search');
  });

  it('should not allow any orqanizational users  who do not have any entitlement to access this page.', () => {
    page.setOrganiztionIdintity();
    page.removeEntitlements();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/search');
  });

  afterAll(() =>  {
    page.revertIdentityAndEntitlements();
  });
});
