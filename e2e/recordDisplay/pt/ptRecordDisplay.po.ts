import { browser, element, by } from 'protractor';

export class PTRecordDisplay {
  navigate(uid?: string) {
    browser.get('/record/' + (uid || '9999-54303-001'));
    browser.sleep(2000);
  }

  checkPage() {
    expect(element(by.css('recorddisplay')).isDisplayed()).toBeTruthy();
    expect(element(by.css('pt-record-display')).isDisplayed()).toBeTruthy();
  }

  checkGeneralInfo(isLimited?: boolean) {
    if(isLimited) {
      this.navigateToDOI('10.1037/t04933-000');
    } else {
      this.navigate('9999-04933-000');
    }
    expect(element(by.css('pt-record-info')).isDisplayed()).toBeTruthy();
    expect(element(by.css('pt-record-info .authors')).isDisplayed()).toBeTruthy();
    expect(element.all(by.css('pt-record-info .construct')).count()).toBeGreaterThan(0);
    expect(element(by.css('pt-record-info .instrument')).isDisplayed()).toBeTruthy();
    expect(element(by.css('pt-record-info .purpose')).isDisplayed).toBeTruthy();
    expect(element(by.css('pt-record-info .admin-method')).isDisplayed()).toBeTruthy();
  }

  checkSummary() {
    expect(element(by.css('pt-record-info .summary-title')).getText()).toContain('Summary');
    expect(element(by.css('pt-record-info .summary-content')).getText()).not.toBe('');
  }

  checkOtherVersions(isLimited?: boolean) {
    if(isLimited) {
      this.navigateToDOI('10.1037/t54303-000');
      expect(element.all(by.css('pt-record-info .versions')).count()).toBeGreaterThan(0);
    } else {
      this.navigate();
      expect(element.all(by.css('pt-record-info .versions')).count()).toBeGreaterThan(0);

      element(by.css('h1')).getText().then(title => {
        element.all(by.css('pt-record-info .versions')).last().element(by.css('a')).click();
        browser.sleep(2000);

        this.checkPage();
        expect(element(by.css('h1')).getText()).not.toBe(title);
      });
    }
  }

  checkPDF() {
    expect(element(by.css('.hide-for-mobile button-box a.pdf')).getAttribute('href')).toContain('.pdf');
  }

  checkSupportDoc() {
    this.navigate('9999-04933-000');
    expect(element(by.css('a.support-doc')).getAttribute('href')).toMatch(/http\:\/\/supp.apa.org\/psyctests\/supporting\/([0-9]{9})/g)
  }

  checkDetailTabs() {
    this.navigate();
    expect(element(by.css('pt-tabs-component')).isDisplayed()).toBeTruthy();
    expect(element.all(by.css('test-records-accordion')).count()).toBeGreaterThan(0);
  }

  useEmailDialog(expectedCountText?: string) {
    expectedCountText = expectedCountText || '1';
    element(by.id('emailBtn')).click();
    browser.sleep(1000);

    expect(element(by.css('email-record-modal .selected-records-count')).getText()).toBe(expectedCountText);
    element(by.css('email-record-modal .close')).click();
    browser.sleep(1000);
  }

  usePrintDialog(expectedCountText?: string) {
    expectedCountText = expectedCountText || '1';
    element(by.id('printBtn')).click();
    browser.sleep(1000);

    expect(element(by.css('print-record-modal .selected-records-count')).getText()).toBe(expectedCountText);
    element(by.css('print-record-modal .close')).click();
    browser.sleep(1000);
  }

  checkReportedIn() {
    expect(element(by.css('test-records-accordion .reported-in')).getText()).not.toBe('');
  }

  toggleFullRecord() {
    element(by.css('.testRecords .accordionHeading a')).click();
    browser.sleep(2000);

    expect(element(by.css('full-record-display')).isDisplayed()).toBeTruthy();

    let terms = element.all(by.css('dt'));
    let values = element.all(by.css('dd'));
    terms.each(e => expect(e.getText()).not.toBe(''));
    values.each(e => expect(e.getText()).not.toBe(''));
  }

  toggleReferences() {
    element(by.css('.citedRefs .accordionHeading a')).click();
    browser.sleep(2000);

    let citedRefs = element(by.css('cited-references'));
    expect(citedRefs.isDisplayed()).toBeTruthy();
    citedRefs.element(by.id('countDisplayed')).getText().then(countDisplayed => {
      expect(element.all(by.css('cited-references article')).count()).toBe(+countDisplayed);
    });
  }

  searchForClassificationCode() {
    element(by.css('.classification-code:last-child a')).getText().then(text => {
      element(by.css('.classification-code:last-child a')).click();
      browser.sleep(3000);

      expect(element(by.css('friendly-query')).getText()).toContain(text);
    });
  }

  interactWithTabs() {
    this.navigate('9999-04933-000');

    element(by.css('.test-tab:last-child .tests-count')).getText().then(lastTabCount => {
      expect(element.all(by.css('test-records-accordion')).count()).toBe(+lastTabCount);

      element(by.css('.test-tab:first-child .tests-count')).getText().then(firstTabCount => {
        element(by.css('.test-tab:first-child a')).click();
        browser.sleep(1000);
        expect(element.all(by.css('test-records-accordion')).count()).toBe(+firstTabCount);
      });
    });
  }

  checkTestSelection() {
    this.navigate('9999-04933-000');
    element(by.css('select-all input[type="checkbox"]')).click();
    element.all(by.css('all-records input[type="checkbox"]')).each(item => expect(item.isSelected()).toBeTruthy());

    element.all(by.css('all-records input[type="checkbox"]')).count().then(recordsCount => {
      this.useEmailDialog('' + recordsCount);

      element.all(by.css('all-records input[type="checkbox"]')).first().click();
      expect(element(by.css('select-all input[type="checkbox"]')).isSelected()).toBeFalsy();
      this.usePrintDialog('' + (recordsCount - 1));
    });
  }

  checkNoneSelected() {
    this.navigate('9999-04933-000');
    browser.sleep(3000);

    element(by.id('emailBtn')).click();
    browser.sleep(1000);
    expect(element(by.css('.hide-for-mobile email-record-modal > div')).getAttribute('aria-hidden')).not.toBe('true');
    expect(element(by.css('.alertMsg')).isDisplayed()).toBeTruthy();

    browser.sleep(4000);

    element(by.id('printBtn')).click();
    browser.sleep(1000);
    expect(element(by.css('.hide-for-mobile print-record-modal > div')).getAttribute('aria-hidden')).not.toBe('true');
    expect(element(by.css('.alertMsg')).isDisplayed()).toBeTruthy();
    browser.sleep(4000);
  }

  checkAccessIsDenied(role?: string) {
    if(role === 'organization') {
      this.togglePTEntitlement();
    } else if (role === 'individual') {
      this.togglePTEntitlement();
      this.setUser('12345', '-1');
    } else {
      this.setUser('-1');
    }

    this.navigate();
    expect(element(by.css('no-access')).isDisplayed()).toBeTruthy();

    if(role === 'organization') {
      this.togglePTEntitlement();
    } else if (role === 'individual') {
      this.togglePTEntitlement();
      this.setUser('12345', '329008');
    } else {
      this.restoreDefaultUserFromGuest();
    }
  }

  checkDoiBasedDisplay() {
    this.togglePTEntitlement();

    this.navigateToDOI('10.1037/t54303-000');
    this.checkGeneralInfo(true);
    this.checkSummary();
    browser.sleep(1000);

    this.checkOtherVersions(true);


    this.togglePTEntitlement();
  }

  private togglePTEntitlement() {
    browser.get('/admin');
    browser.sleep(2000);

    element(by.id('PT')).click();
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(2000);
  }

  private setUser(userId: string, orgId?: string) {
    browser.get('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys(userId);

    if (userId === '-1' || orgId === '-1') {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');
    } else if (orgId && orgId.length) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys(orgId);
    }

    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }

  private restoreDefaultUserFromGuest() {
    this.setUser('12345', '329008');

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
    browser.sleep(1000);
    for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
      element(by.id(thing)).click();
    }
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(2000);
  }

  private navigateToDOI(doi: string) {
    browser.get('/doi/' + doi);
    browser.sleep(2000);
  }
}
