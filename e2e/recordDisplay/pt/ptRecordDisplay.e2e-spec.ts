import { browser } from 'protractor';
import { PTRecordDisplay } from './ptRecordDisplay.po';

describe('Record Display - for PT', () => {
  let page: PTRecordDisplay;

  beforeAll(() => {
    browser.manage().timeouts().implicitlyWait(5000);
    page = new PTRecordDisplay();
    page.navigate();
  });

  it('should load the correct page.', () => page.checkPage());
  it('should list the general information.', () => page.checkGeneralInfo());
  it('should list the summary.', () => page.checkSummary());
  it('should display a record, using the (Other Versions) list.', () => page.checkOtherVersions());
  it('should have a (PDF) link.', () => page.checkPDF());
  it('should have a (Supporting Documentation) link.', () => page.checkSupportDoc());
  it('should display detailed test tabs.', () => page.checkDetailTabs());
  it('should have a working (email) dialog.', () => page.useEmailDialog());
  it('should have a working (print) dialog.', () => page.usePrintDialog());
  it('should display the (Reported in) field, within the detailed test section.', () => page.checkReportedIn());
  it('should toggle the full record of the detailed test section.', () => page.toggleFullRecord());
  it('should toggle the references of the detailed test section.', () => page.toggleReferences());
  it('should perform a search, using the (PsycTEST Classification) field of the full record.', () => page.searchForClassificationCode());

  it('should have working multiple test-tabs.', () => page.interactWithTabs());
  it('should select some or all detailed test records.', () => page.checkTestSelection());
  it('should not access (email) or (print) dialogs, when there is multiple detailed records, and none of them is selected.', () => page.checkNoneSelected());

  it('should display a limited record using DOI link, for a non-entitled organizational user.', () => page.checkDoiBasedDisplay());
  it('should prevent access with a message, for a non-entitled organizational user.', () => page.checkAccessIsDenied('organization'));
  it('should prevent access with a message, for an individual user.', () => page.checkAccessIsDenied('individual'));
  it('should prevent access with a message, for a guest user.', () => page.checkAccessIsDenied());
});
