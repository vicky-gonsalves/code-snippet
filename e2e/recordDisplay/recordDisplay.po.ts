import { browser, element, by } from 'protractor';

export class RecordDisplay {
  navigate(uid?: string) {
    browser.get('/record/' + (uid || '1987-13085-001'));
    browser.sleep(2000);
  }

  checkPage() {
    element(by.css('recordDisplay')).isDisplayed().then(isDisplayed => expect(isDisplayed).toBeTruthy());
  }

  checkCitation(forExtra?: boolean) {
    let selector = forExtra ? 'citation-pe' : 'citation';
    element(by.css(selector)).isDisplayed().then(isDisplayed => expect(isDisplayed).toBeTruthy());
    element(by.css(selector)).element(by.css('.citation-text')).getText().then(text => expect(text.length).toBeGreaterThan(0));
  }

  checkAbstract() {
    element(by.css('abstract')).isDisplayed().then(isDisplayed => expect(isDisplayed).toBeTruthy());
    element(by.css('abstract p')).getText().then(text => expect(text.length).toBeGreaterThan(0));
  }

  checkFullRecord() {
    let terms = element.all(by.css('dt'));
    let values = element.all(by.css('dd'));

    expect(terms.count()).toBe(values.count());
    terms.each(e => expect(e.getText()).not.toBe(''));
    values.each(e => expect(e.getText()).not.toBe(''));

    element(by.css('full-record-display-accordion .accordionHeading a')).click();
    expect(element(by.css('full-record-display')).isDisplayed()).toBeFalsy();

    element(by.css('full-record-display-accordion .accordionHeading a')).click();
    expect(element(by.css('full-record-display')).isDisplayed()).toBeTruthy();
  }

  toggleReferences() {
    element(by.css('cited-references-accordion h4 a')).click();
    element(by.css('cited-references #countDisplayed')).getText().then(countDisplayed => {
      expect(element(by.css('cited-references #countFound')).getText()).toBe(countDisplayed);
      expect(element.all(by.css('cited-references article')).count()).toBe(+countDisplayed);
    });
  }

  checkAbout() {
    expect(element(by.css('about')).isDisplayed()).toBeTruthy();
  }

  checkButtonBox() {
    expect(element(by.css('.hide-for-mobile button-box')).isDisplayed()).toBeTruthy();
  }

  checkRelatedContent() {
    expect(element(by.css('related-content')).isDisplayed()).toBeTruthy();
    expect(element.all(by.css('related-content .list-group-item')).count()).toBeGreaterThan(1);
  }

  checkIndexTerms() {
    expect(element(by.css('index-terms')).isDisplayed()).toBeTruthy();
    expect(element.all(by.css('index-terms a')).count()).toBeGreaterThan(0);
  }

  checkExport() {
    element(by.id('exportBtn')).click();
    browser.sleep(1000);

    expect(element(by.css('export-ris-modal')).isDisplayed()).toBeTruthy();
    expect(element(by.css('export-ris-modal .selected-records-count')).getText()).toBe('1');
    element(by.css('export-ris-modal .close')).click();
    browser.sleep(1000);
  }

  toggleAddToMyList() {
    element(by.css('personalization .badge')).getText().then(countBefore => {
      element(by.css('add-to-my-list')).click();
      browser.sleep(3000);

      element(by.css('personalization .badge')).getText().then(countAfter => {
        element(by.css('add-to-my-list')).click();
        browser.sleep(3000);

        let success = ((+countBefore + 1) === (+countAfter)) || ((+countBefore - 1) === (+countAfter));
        expect(success).toBeTruthy();
      });
    });
  }

  checkEmail() {
    element(by.id('emailBtn')).click();
    browser.sleep(1000);

    expect(element(by.css('email-record-modal')).isDisplayed()).toBeTruthy();
    expect(element(by.css('email-record-modal .selected-records-count')).getText()).toBe('1');
    element(by.css('email-record-modal .close')).click();
    browser.sleep(1000);
  }

  checkPrint() {
    element(by.id('printBtn')).click();
    browser.sleep(1000);

    expect(element(by.css('print-record-modal')).isDisplayed()).toBeTruthy();
    expect(element(by.css('print-record-modal .selected-records-count')).getText()).toBe('1');
    element(by.css('print-record-modal .close')).click();
    browser.sleep(1000);
  }

  checkRecordMeta() {
    expect(element.all(by.css('.record-label')).count()).toBeGreaterThan(0);
    element.all(by.css('.record-label')).each(e => expect(e.getText).not.toBe(''));
  }

  searchForAuthor(fromDetails?: boolean) {
    this.navigate();
    let selector = fromDetails ? 'full-record-display .authorField' : 'citation';
    element(by.css(selector)).element(by.css('span:first-child a')).getText().then(author => {
      element(by.css(selector)).element(by.css('span:first-child a')).click();
      expect(element(by.css('searchresults')).isDisplayed()).toBeTruthy();
      element(by.css('friendly-query')).getText().then(fq => expect(fq.indexOf(author)).not.toBe(-1));
    });
  }

  goToJournalTOC() {
    this.navigate();
    element(by.css('about a')).click();
    expect(element(by.css('journalhome')).isDisplayed()).toBeTruthy();
  }

  goToBookTOC() {
    this.navigate('2016-09984-031');

    element(by.css('links .list-group-item')).click();
    browser.sleep(3000);

    browser.getAllWindowHandles().then(handles => {
      let primary = handles[0];
      let secondary = handles[handles.length - 1];

      browser.switchTo().window(secondary).then(() => {
        expect(element(by.css('browsepbtoc')).isDisplayed()).toBeTruthy();
        browser.driver.close();
        browser.switchTo().window(primary);
      });
    });
  }

  checkPDF() {
    this.navigate();
    expect(element(by.css('.hide-for-mobile button-box a.pdf')).getAttribute('href')).toContain('.pdf');

    this.toggleEntitlement('PA');
    this.navigate();
    expect(element(by.css('.hide-for-mobile button-box .pdf')).isPresent()).toBeFalsy();
    this.toggleEntitlement('PA');
  }

  checkArticlePurchasePDF() {
    this.setUser('12345', '-1');

    this.toggleEntitlement('PA');
    this.navigate();

    this.checkBuyOptionForRecord();
    this.toggleEntitlement('PA');
    this.setUser('12345', '329008');
  }

  checkBookChapterPurchasePDF() {
    this.setUser('12345', '-1');
    this.toggleEntitlement('PB');
    this.navigate('2016-09984-031');

    this.checkBuyOptionForRecord();
    this.toggleEntitlement('PB');

    this.setUser('12345', '329008');
  }

  checkFT() {
    this.navigate();

    element(by.css('.hide-for-mobile a.ft-html')).click();
    browser.sleep(1000);
    expect(element(by.css('fulltexthtml')).isDisplayed()).toBeTruthy();
  }

  checkCitedBy() {
    this.navigate();

    element(by.css('.hide-for-mobile a.cited-by')).click();
    browser.sleep(1000);
    expect(element(by.css('cited-by')).isDisplayed()).toBeTruthy();
  }

  displayRelatedRecord() {
    this.navigate();
    let recordLink = element(by.css('related-content .list-group-item:nth-child(2) a'));
    recordLink.getText().then(title => {
      recordLink.click();
      browser.sleep(3000);

      browser.getAllWindowHandles().then(handles => {
        let primary = handles[0];
        let secondary = handles[handles.length - 1];

        browser.switchTo().window(secondary).then(() => {
          expect(element(by.css('h1')).getText()).toBe(title);
          browser.driver.close();
          browser.switchTo().window(primary);
        });
      });
    });
  }

  checkMoreRelated() {
    this.navigate();
    element(by.css('related-content .list-group-item:nth-child(2) a')).getText().then(title => {
      element(by.css('related-content .more a')).click();
      browser.sleep(3000);

      expect(element.all(by.css('searchresults .result-item')).count()).toBeGreaterThan(0);
    });
  }

  searchForIndexTerm(fromDetails?: boolean) {
    this.navigate();
    let selector = fromDetails ? 'full-record-display .index-term:first-child a .term-text' : 'index-terms li:first-child a';

    element(by.css(selector)).getText().then(term => {
      element(by.css(selector)).click();
      browser.sleep(3000);
      element(by.css('searchresults friendly-query')).getText().then(fq => {
        expect(fq.indexOf('Index Terms')).not.toBe(-1);
        expect(fq.indexOf(term)).not.toBe(-1);
      });
    });
  }

  searchForClassificationCode() {
    this.navigate('2016-49269-012');
    element(by.css('.classification-code:last-child a')).getText().then(text => {
      element(by.css('.classification-code:last-child a')).click();
      browser.sleep(3000);

      expect(element(by.css('friendly-query')).getText()).toContain(text);
    });
  }

  checkNonRecordMessage() {
    this.navigate('abcd');
    expect(element(by.css('.not-found')).isDisplayed()).toBeTruthy();
    expect(element(by.css('.record-details')).isPresent()).toBeFalsy();
  }

  translateAbstract() {
    this.navigate('2016-41702-003');

    element(by.css('abstract p')).getText().then(english => {
      element(by.css('abstract select')).click();
      element(by.css('abstract select option:last-child')).click();
      browser.sleep(1000);
      expect(element(by.css('abstract p')).getText()).not.toBe(english);
    });
  }

  checkSearchResultsBar() {
    this.getHereFromResultsPage();
    expect(element(by.css('.search-results-bar')).isDisplayed()).toBeTruthy();

    element(by.css('h1')).getText().then(firstTitle => {
      element.all(by.css('.pagiantionArrow')).last().click();
      expect(element(by.css('h1')).getText()).not.toBe(firstTitle);

      element(by.css('.paginationBackLink')).click();
      expect(element(by.css('searchresults')).isDisplayed()).toBeTruthy();
    });
  }

  accessAsGuest() {
    // This one checks the PA record, other methods work on the other entitlements in a similar way.
    this.setUser('-1');
    this.navigate();

    expect(element(by.css('record-action-bar')).isDisplayed()).toBeTruthy();
    this.checkCitation();
    this.checkAbstract();
    this.checkRecordMeta();

    this.checkAbout();
    this.checkButtonBox();
    this.checkRelatedContent();

    expect(element(by.css('full-record-display')).isPresent()).toBeFalsy();
    expect(element(by.css('cited-references')).isPresent()).toBeFalsy();

    this.restoreDefaultUserFromGuest();
  }

  accessWithoutPI() {
    this.toggleEntitlement('PI');
    this.navigate('2016-19423-017');

    expect(element(by.css('record-action-bar')).isDisplayed()).toBeTruthy();
    expect(element(by.css('links')).isDisplayed()).toBeTruthy();
    this.checkCitation();
    this.checkAbstract();
    this.checkRecordMeta();

    expect(element(by.css('about')).isPresent()).toBeFalsy();
    expect(element(by.css('related-content')).isPresent()).toBeFalsy();
    expect(element(by.css('full-record-display')).isPresent()).toBeFalsy();
    expect(element(by.css('cited-references')).isPresent()).toBeFalsy();

    this.toggleEntitlement('PI');
  }

  accessWithoutPA() {
    this.toggleEntitlement('PA');
    this.navigate();

    expect(element(by.css('record-action-bar')).isDisplayed()).toBeTruthy();
    this.checkCitation();
    this.checkAbstract();
    this.checkRecordMeta();

    this.checkAbout();
    this.checkButtonBox();
    this.checkRelatedContent();

    expect(element(by.css('full-record-display')).isPresent()).toBeFalsy();
    expect(element(by.css('cited-references')).isPresent()).toBeFalsy();

    this.toggleEntitlement('PA');
  }

  accessWithoutPB() {
    this.toggleEntitlement('PB');
    this.navigate('2009-06878-002');

    expect(element(by.css('record-action-bar')).isDisplayed()).toBeTruthy();
    this.checkCitation();
    this.checkAbstract();
    this.checkRecordMeta();

    expect(element(by.css('links')).isDisplayed()).toBeTruthy();
    this.checkButtonBox();
    this.checkRelatedContent();

    expect(element(by.css('full-record-display')).isPresent()).toBeFalsy();
    expect(element(by.css('cited-references')).isPresent()).toBeFalsy();

    this.toggleEntitlement('PB');
  }

  accessWithoutPC() {
    this.toggleEntitlement('PC');
    this.navigate('2016-48647-001');

    expect(element(by.css('record-action-bar')).isDisplayed()).toBeTruthy();
    this.checkCitation();
    this.checkAbstract();
    this.checkRecordMeta();

    expect(element(by.css('about')).isPresent()).toBeFalsy();
    expect(element(by.css('related-content')).isPresent()).toBeFalsy();
    expect(element(by.css('full-record-display')).isPresent()).toBeFalsy();
    expect(element(by.css('cited-references')).isPresent()).toBeFalsy();

    this.toggleEntitlement('PC');
  }

  accessWithoutPE() {
    this.toggleEntitlement('PE');
    this.navigate('559442014-001');

    expect(element(by.css('record-action-bar')).isDisplayed()).toBeTruthy();
    this.checkCitation(true);
    this.checkAbstract();
    this.checkRecordMeta();

    expect(element(by.css('about')).isPresent()).toBeFalsy();
    expect(element(by.css('related-content')).isPresent()).toBeFalsy();
    expect(element(by.css('full-record-display')).isPresent()).toBeFalsy();
    expect(element(by.css('cited-references')).isPresent()).toBeFalsy();

    this.toggleEntitlement('PE');
  }

  private checkBuyOptionForRecord() {
    browser.sleep(2000);
    expect(element(by.css('a.pdf')).isPresent()).toBeFalsy();
    element(by.css('a.purchase')).click();
    browser.sleep(1000);
    expect(element(by.css('uid-option')).isDisplayed()).toBeTruthy();
  }

  private getHereFromResultsPage() {
    browser.get('/search/basic');
    browser.sleep(1000);

    element(by.id('searchForInput')).sendKeys('happy');
    element(by.id('searchForBtn')).click();
    browser.sleep(5000);

    element(by.css('.result-item:first-child .article-title')).click();
    browser.sleep(2000);
  }

  private restoreDefaultUserFromGuest() {
    this.setUser('12345', '329008');

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
    browser.sleep(1000);
    for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
      element(by.id(thing)).click();
    }
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(2000);
  }

  private setUser(userId: string, orgId?: string) {
    browser.get('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys(userId);

    if (userId === '-1' || orgId === '-1') {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');
    } else if (orgId && orgId.length) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys(orgId);
    }

    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }

  private toggleEntitlement(entitlement) {
    browser.get('/admin');
    browser.sleep(2000);

    element(by.id(entitlement)).click();
    element(by.css('entitlements input[type=submit]')).click();
    browser.sleep(2000);
  }
}
