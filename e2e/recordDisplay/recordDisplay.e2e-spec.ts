import { browser } from 'protractor';
import { RecordDisplay } from './recordDisplay.po';

// Some test specs need extended time before completion.
jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

describe('Record Display [Non-PT]', () => {
  let page: RecordDisplay;

  beforeAll(() => {
    browser.manage().timeouts().implicitlyWait(5000);
    page = new RecordDisplay();
    page.navigate();
  });

  it('should load the correct page.', () => page.checkPage());
  it('should display the citation.', () => page.checkCitation());
  it('should display the abstract.', () => page.checkAbstract());
  it('should toggle the full record, its fields should not have blank values.', () => page.checkFullRecord());
  it('should toggle the references list.', () => page.toggleReferences());
  it('should display the (about) section.', () => page.checkAbout());
  it('should have a button-box.', () => page.checkButtonBox());
  it('should display (related content) section.', () => page.checkRelatedContent());
  it('should display (index terms).', () => page.checkIndexTerms());
  it('should access the (export) dialog correctly.', () => page.checkExport());
  it('should toggle (Add to My List).', () => page.toggleAddToMyList());
  it('should access the (email) dialog correctly.', () => page.checkEmail());
  it('should access the (print) dialog correctly.', () => page.checkPrint());
  it('should display the record metadata.', () => page.checkRecordMeta());

  it('should search for an author via the citation links.', () => page.searchForAuthor());
  it('should search for an author via the (Author) field (within the record details).', () => page.searchForAuthor(true));

  it('should navigate to Journal TOC (For PA records).', () => page.goToJournalTOC());
  it('should navigate to Book TOC (For PB records).', () => page.goToBookTOC());

  it('should have a link to (Full Text HTML).', () => page.checkFT());
  it('should have a link to (Cited By).', () => page.checkCitedBy());

  it('should navigate to related records.', () => page.displayRelatedRecord());
  it('should access more records, from the related records section.', () => page.checkMoreRelated());
  it('should search for an index term via its side section.', () => page.searchForIndexTerm());
  it('should search for an index term, using the (Index Terms) field (within the record details).', () => page.searchForIndexTerm(true));
  it('should search for a classfication code, within the record details.', () => page.searchForClassificationCode());

  it('should display a message, when no record is found.', () => page.checkNonRecordMessage());

  it('should have a link to (PDF).', () => page.checkPDF());
  it('should have a link to (Purchase PDF) for PA.', () => page.checkArticlePurchasePDF());
  it('should have a link to (Purchase PDF) for PB.', () => page.checkBookChapterPurchasePDF());

  it('should translate the abstract, for a non-English record.', () => page.translateAbstract());
  it('should use the search-results toolbar, upon being called from there.', () => page.checkSearchResultsBar());

  it('should display the right content, for a user who does not have the (PI) entitlement.', () => page.accessWithoutPI());
  it('should display the right content, for a user who does not have the (PA) entitlement.', () => page.accessWithoutPA());
  it('should display the right content, for a user who does not have the (PB) entitlement.', () => page.accessWithoutPB());
  it('should display the right content, for a user who does not have the (PC) entitlement.', () => page.accessWithoutPC());
  it('should display the right content, for a user who does not have the (PE) entitlement.', () => page.accessWithoutPE());
  it('should display the right content, for a guest user.', () => page.accessAsGuest());
});
