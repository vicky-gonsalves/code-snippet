import { browser } from 'protractor';
import { fulltextPA } from './fulltextPA.po';

describe('fulltextPA', () => {
  let page: fulltextPA;

  beforeEach(() => {
    page = new fulltextPA();
    page.navigateToJournal('2016-17382-001.html');

    browser.manage().timeouts().implicitlyWait(5000);
  });

  it('should open the correct page.', () => {
    page.isInstanceDisplayed().then(isDisplayed => expect(isDisplayed).toBeTruthy());
  });

  it('should have Action Bar.', () => {
    page.isActionBarDisplayed().then(isDisplayed => expect(isDisplayed).toBeTruthy());
  });

  it('should have Toggle Dictionary Link.', () => {
    page.isToggleDictionaryDisplayed().then(isDisplayed => expect(isDisplayed).toBeTruthy());
  });

  it('clicking Toggle Dictionary Off Link should turn off Dictionary link ', () => {

    expect(page.getDictionaryLinks().count()).toBe(0);

  });


  it('should have a left panel.', () => {
    page.isLeftPanelDisplayed().then(isDisplayed => expect(isDisplayed).toBeTruthy());
  });

  it('should have Outline in left nav.', () => {
    //page.isMoreReviewsDisplayed().then(isPresent => expect(isPresent).toBeTruthy());
    expect(page.getOutline().count()).toBeGreaterThan(0);
  });

  it('should have a right panel.', () => {
    //page.isMoreReviewsDisplayed().then(isPresent => expect(isPresent).toBeTruthy());
    page.isRightPanelDisplayed().then(isDisplayed => expect(isDisplayed).toBeTruthy());
  });

  it('should have PA Issue link.', () => {
    //page.isRelatedReviewsDisplayed().then(isfound => expect(isfound).toBeTruthy());
    expect(page.getIssue().count()).toBeGreaterThan(0);
  });

  it('should have Related Content.', () => {
    //page.isRelatedReviewsDisplayed().then(isfound => expect(isfound).toBeTruthy());
    expect(page.getRelatedContent().count()).toBeGreaterThan(0);
  });

});
