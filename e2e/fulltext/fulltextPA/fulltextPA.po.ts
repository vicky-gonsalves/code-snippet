import { browser, element, by } from 'protractor';

export class fulltextPA {
  navigate(path: string) {
    return browser.get(path);
  }

  navigateToJournal(code) {
    return browser.get('/fulltext/' + code);
  }

  isInstanceDisplayed() {

      return element(by.css('.articleTitleGroup')).isDisplayed();

  }
  isActionBarDisplayed() {

    return element(by.css('ftactionbar')).isDisplayed();

  }

  isToggleDictionaryDisplayed() {

    return element(by.css('fttoggleDictionary')).isDisplayed();

  }


  isLeftPanelDisplayed() {

    return element(by.css('ftleftnav')).isDisplayed();

  }

  getIssue() {
    return element.all(by.css('ftjournalissuepaginator .metaData'));
  }

  getDictionaryLinks() {
    //return element.all(by.css('.dictionaryTerm .docDashedBottom'));

    element(by.id('toggleDictionary')).click();
    browser.sleep(1000);
    return element.all(by.css('.dictionaryTerm .docDashedBottom'));
  }


  getRelatedContent() {
    return element.all(by.css('related-content .list-group-item'));
  }

  getOutline() {
    return element.all(by.css('li.outline'));
  }

  isRightPanelDisplayed() {

    return element(by.css('ftrightnav')).isDisplayed();

  }


  isInstancePresent() {
    return element(by.css('fulltext-container')).isPresent();
  }
}
