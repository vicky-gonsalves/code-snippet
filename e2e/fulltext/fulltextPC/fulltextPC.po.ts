import { browser, element, by } from 'protractor';

export class fulltextPC {
  navigate(path: string) {
    return browser.get(path);
  }

  navigateToJournal(code) {
    return browser.get('/fulltext/' + code);
  }

  isInstanceDisplayed() {
  return element(by.css('.psqTopGroup')).isDisplayed();
}

  getMoreReviewsBy() {
    return element.all(by.css('ft-pc-reviews-by .ft-reviewby-container'));
  }

  getMoreReviews() {
    return element.all(by.css('ftpcmorereviews .more-reviews-list'));
  }

  getIssue() {
    return element.all(by.css('ftpcissuepaginator .lined-list'));
  }

  getActionBar() {
    return element.all(by.css('ftactionbar'));
  }


}

