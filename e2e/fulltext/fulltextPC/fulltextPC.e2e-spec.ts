import { browser } from 'protractor';
import { fulltextPC } from './fulltextPC.po';

describe('fulltextPC', () => {
  let page: fulltextPC;

  beforeEach(() => {
    page = new fulltextPC();
    page.navigateToJournal('2016-48647-001.html');

    browser.manage().timeouts().implicitlyWait(5000);
  });

  it('should open the correct page.', () => {
    page.isInstanceDisplayed().then(isDisplayed => expect(isDisplayed).toBeTruthy());
  });

  it('should have an Action Bar.', () => {
    //page.isRelatedReviewsDisplayed().then(isfound => expect(isfound).toBeTruthy());
    expect(page.getActionBar().count()).toBeGreaterThan(0);
  });

  it('should have More Reviews By Author.', () => {
    //page.isMoreReviewsDisplayed().then(isPresent => expect(isPresent).toBeTruthy());
    expect(page.getMoreReviewsBy().count()).toBeGreaterThan(0);
  });

  it('should have More Book & Media Reviews.', () => {
    //page.isRelatedReviewsDisplayed().then(isfound => expect(isfound).toBeTruthy());
    expect(page.getMoreReviews().count()).toBeGreaterThan(0);
  });

  it('should have PC Issue link.', () => {
    //page.isRelatedReviewsDisplayed().then(isfound => expect(isfound).toBeTruthy());
    expect(page.getIssue().count()).toBeGreaterThan(0);
  });

});
