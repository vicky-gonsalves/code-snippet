import { browser, element, by } from 'protractor';

export class CitedBy {
  navigate(path?: string) {
    browser.get(path || '/search/citedBy/2010-00090894');
  }

  checkPageIsDislpayed(cb) {
    element(by.css('cited-by')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  checkResultsCount(cb) {
    element(by.css('.result-summary .results-count')).getText().then(text => cb(+text > 0));
  }

  checkResultStatement(cb) {
    element(by.css('.result-summary')).getText().then(text => cb(text.match(/This document is cited \d{1,} times in APA PsycNET/g).length));
  }

  toggleSelectAll(cb) {
    element(by.css('search-results-nav:first-child select-all input[type="checkbox"]')).click();
    let selectorInputs = element.all(by.css('.result-item input[type="checkbox"]'));
    selectorInputs.first().isSelected().then(firstIsSelected => {
      selectorInputs.last().isSelected().then(secondIsSelected => {
        let result = firstIsSelected && secondIsSelected;
        element(by.css('search-results-nav:first-child select-all input[type="checkbox"]')).click();

        selectorInputs = element.all(by.css('.result-item input[type="checkbox"]'));
        selectorInputs.first().isSelected().then(firstIsSelected => {
          selectorInputs.last().isSelected().then(secondIsSelected => {
            result = result && (!firstIsSelected && !secondIsSelected);
            cb(result);
          });
        });
      });
    });
  }

  openExportModal(cb) {
    element(by.css('.result-item:first-child input[type="checkbox"]')).click();
    element(by.css('.result-item:last-child input[type="checkbox"]')).click();

    element(by.css('search-results-nav:first-child button.export')).click();
    browser.sleep(500);

    element(by.css('search-results-nav:first-child export-ris-modal .selected-records-count')).getText().then(text => {
      element(by.css('search-results-nav:first-child export-ris-modal button.close')).click();
      browser.sleep(500);

      element(by.css('.result-item:first-child input[type="checkbox"]')).click();
      element(by.css('.result-item:last-child input[type="checkbox"]')).click();
      cb(+text === 2);
    });
  }

  openEmailModal(cb) {
    element(by.css('.result-item:first-child input[type="checkbox"]')).click();
    element(by.css('.result-item:last-child input[type="checkbox"]')).click();

    element(by.css('search-results-nav:first-child button.email')).click();
    browser.sleep(500);

    element(by.css('search-results-nav:first-child email-record-modal .selected-records-count')).getText().then(text => {
      element(by.css('search-results-nav:first-child email-record-modal button.close')).click();
      browser.sleep(500);

      element(by.css('.result-item:first-child input[type="checkbox"]')).click();
      element(by.css('.result-item:last-child input[type="checkbox"]')).click();
      cb(+text === 2);
    });
  }

  openPrintModal(cb) {
    element(by.css('.result-item:first-child input[type="checkbox"]')).click();
    element(by.css('.result-item:last-child input[type="checkbox"]')).click();

    element(by.css('search-results-nav:first-child button.print')).click();
    browser.sleep(500);

    element(by.css('search-results-nav:first-child print-record-modal .selected-records-count')).getText().then(text => {
      element(by.css('search-results-nav:first-child print-record-modal button.close')).click();
      browser.sleep(500);

      element(by.css('.result-item:first-child input[type="checkbox"]')).click();
      element(by.css('.result-item:last-child input[type="checkbox"]')).click();
      cb(+text === 2);
    });
  }

  useAddToMyListWithinToolbar(cb) {
    element(by.css('.result-item:first-child input[type="checkbox"]')).click();
    element(by.css('.result-item:last-child input[type="checkbox"]')).click();

    element(by.css('.navigation-menu .badge')).getText().then(beforeAddText => {
      element(by.css('search-results-nav:first-child button.batch-add')).click();
      browser.sleep(2000);

      element(by.css('.navigation-menu .badge')).getText().then(afterAddText => {
        element(by.css('.result-item:first-child input[type="checkbox"]')).click();
        element(by.css('.result-item:first-child add-to-my-list')).click();
        element(by.css('.result-item:last-child input[type="checkbox"]')).click();
        element(by.css('.result-item:last-child add-to-my-list')).click();
        browser.sleep(2000);
        cb(+afterAddText === +beforeAddText + 2);
      });
    });
  }

  changePaginationLimit(cb) {
    element.all(by.css('.result-item')).count().then(countBefore => {
      element(by.css('search-results-nav:first-child paginationlimit select')).click();
      element(by.css('search-results-nav:first-child paginationlimit select option:last-child')).click();

      element.all(by.css('.result-item')).count().then(countAfter => {
        console.log('Pagination limit:', 'before=' + countBefore, 'after=' + countAfter);
        cb(+countBefore === 25 && +countAfter === 50);
      });
    });
  }

  changePage(cb) {
    let paginator = element(by.css('search-results-pagination:first-child numberedpagination.hidden-xs'));
    element(by.css('.result-item:first-child .article-title')).getText().then(page1FirstText => {
      paginator.element(by.id('page-2')).click();
      element(by.css('.result-item:first-child .article-title')).getText().then(page2FirstText => {
        cb(page1FirstText !== page2FirstText);
      });
    });
  }

  goToRecordDisplay(cb) {
    element(by.css('.result-item:first-child .article-title')).click();
    element(by.css('recorddisplay')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  toggleItemAbstract(cb) {
    element(by.css('.result-item:nth-child(18)')).element(by.id('abstract')).click();
    element(by.css('.result-item:nth-child(18) searchresults-buttonbar .resultData strong')).getText().then(title => {
      element(by.css('.result-item:nth-child(18) searchresults-buttonbar .resultData span')).getText().then(detail => {
        element(by.css('.result-item:nth-child(18)')).element(by.id('abstract')).click();
        cb(title === 'Abstract' && detail.length);
      });
    });
  }

  toggleItemReferences(cb) {
    element(by.css('.result-item:nth-child(18)')).element(by.id('references')).click();
    element(by.css('.result-item:nth-child(18) searchresults-buttonbar .resultData strong')).getText().then(title => {
      element.all(by.css('.result-item:nth-child(18) searchresults-buttonbar .ref-item')).count().then(count => {
        element(by.css('.result-item:nth-child(18)')).element(by.id('references')).click();
        cb(title === 'References' && count);
      });
    });
  }

  displayItemCitations(cb) {
    element(by.css('.result-item:nth-child(18) .article-title')).getText().then(title => {
      element(by.css('.result-item:nth-child(18)')).element(by.id('references')).click();
      element(by.css('.result-summary')).getText().then(summary => cb(summary.indexOf(title)) >= 0);
    });
  }

  toggleItemAddToMyList(cb) {
    element(by.css('.navigation-menu .badge')).getText().then(beforeAdd => {
      element(by.css('.result-item:first-child add-to-my-list')).click();
      browser.sleep(2000);
      element(by.css('.navigation-menu .badge')).getText().then(afterAdd => {
        element(by.css('.result-item:first-child add-to-my-list')).click();
        cb(+afterAdd === (+beforeAdd + 1) || +afterAdd === (+beforeAdd - 1));
      });
    });
  }

  displayNonCited(cb) {
    // results count = 0
    this.navigate('/search/citedBy/2016-40242-000');
    element(by.css('.result-summary')).getText().then(text => {
      element.all(by.css('-result-item')).count().then(count => {
        cb(count === 0 && text.match(/This document is cited 0 times in APA PsycNET/g).length);
      });
    });
  }
}
