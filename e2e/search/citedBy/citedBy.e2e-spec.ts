import { browser } from 'protractor';
import { CitedBy } from './citedBy.po';

describe('Cited By', () => {
  let page: CitedBy;
  beforeAll(() => {
    browser.manage().timeouts().implicitlyWait(5000);
    page = new CitedBy();
    page.navigate();
  });

  it('should display the correct page.', () => page.checkPageIsDislpayed(success => expect(success).toBeTruthy()));
  it('should have results.', () => page.checkResultsCount(success => expect(success).toBeTruthy()));
  it('should display results count and record citation.', () => page.checkResultStatement(success => expect(success).toBeTruthy()));
  it('should toggle (Select All).', () => page.toggleSelectAll(success => expect(success).toBeTruthy()));
  it('should display (export) dialog, after selecting some result items.', () => page.openExportModal(success => expect(success).toBeTruthy()));
  it('should display (email) dialog, after selecting some result items.', () => page.openEmailModal(success => expect(success).toBeTruthy()));
  it('should display (print) dialog, after selecting some result items.', () => page.openPrintModal(success => expect(success).toBeTruthy()));
  it('should use (Add to My List) within the toolbar, after selecting some result items.', () => page.useAddToMyListWithinToolbar(success => expect(success).toBeTruthy()));
  it('should change pagination limit.', () => page.changePaginationLimit(success => expect(success).toBeTruthy()));
  it('should change pagination.', () => page.changePage(success => expect(success).toBeTruthy()));
  it('should toggle (Abstract) for a result item.', () => page.toggleItemAbstract(success => expect(success).toBeTruthy()));
  it('should toggle (References) for a result item.', () => page.toggleItemReferences(success => expect(success).toBeTruthy()));
  it('should show citations of some result item, using its (Cited By) button.', () => page.displayItemCitations(success => expect(success).toBeTruthy()));
  it('should toggle (Add to My List) within a result item.', () => page.toggleItemAddToMyList(success => expect(success).toBeTruthy()));

  it('should navigate to the record display, for some result item.', () => page.goToRecordDisplay(success => expect(success).toBeTruthy()));
  it('should display a message for records with no citations.', () => page.displayNonCited(success => expect(success).toBeTruthy()));
});
