import { browser, element, by } from 'protractor';

export class CitedRefs {
  private data = {
    lastName: 'Baron',
    firstName: 'R',
    title: 'moderator–mediator variable distinction',
    source: '"Journal of Personality and Social Psychology"',
    year: '1986'
  };

  navigate(path?: string) {
    browser.get(path || '/search/citedRefs');
  }

  clearForm(cb) {
    this.navigate();

    element(by.id('lastName')).sendKeys(this.data.lastName);
    browser.sleep(500);

    element(by.css('form button[type="button"]')).click();
    browser.sleep(500);

    element(by.id('lastName')).getText().then(text => cb(text === ''));
  }

  performSearch(cb) {
    this.navigate();
    let fqParts = ['Cited Last Name: Baron', 'Cited First Name: R', 'Cited Title: moderator–mediator variable distinction', 'Cited Source: "Journal of Personality and Social Psychology"', 'Cited Year: 1986']

    for (let key in this.data) {
      element(by.id(key)).sendKeys(this.data[key]);
    }

    element(by.css('button[type="submit"]')).click();

    element(by.css('friendly-query')).getText().then(text => {
      let success = true;
      for (let part of fqParts) {
        success = success && (text.indexOf(part) >= 0);
      }
      cb(success);
    });
  }

  editSearch(cb) {
    element(by.css('.searchoptions button:first-child')).click();
    browser.sleep(1000);

    let formData = { lastName: '', firstName: '', title: '', source: '', year: '' };

    for (let fieldId in this.data) {
      formData[fieldId] = element(by.id(fieldId)).getText();
    }

    setTimeout(() => {
      console.log('Data', formData);
      cb(this.data === formData);
    }, 2000);
  }
}
