import { browser } from 'protractor';
import { CitedRefs } from './citedRefs.po';

describe('Cited References Search Form', () => {
  let page: CitedRefs;

  beforeEach(() => {
    page = new CitedRefs();
    browser.manage().timeouts().implicitlyWait(5000);
  });

  it('should clear the form', () => page.clearForm(success => expect(success).toBeTruthy()));
  it('should perform a new search', () => page.performSearch(success => expect(success).toBeTruthy()));
  it('should edit an existing search', () => page.editSearch(success => expect(success).toBeTruthy()));
});
