import { browser, element, by } from 'protractor';
import * as _ from 'lodash';

export class AdvancedSearchForm {

  private searchField = 'searchField';
  private fields = 'fields';

  navigateTo() {
    return browser.get('/search/advanced');
  }

  getPageHeading() {
    let heading = element(by.css('.searchHeading'));
    return heading.getText();
  }

  getSearchInputText() {
    let input = this.getSearchInput();
    return input.getAttribute('value');
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  executeSearch(){
    let searchBtn = element(by.id('searchForBtn'));
    return searchBtn.click();
  }

  private getSearchInput() {
    return element(by.id(this.searchField));
  }

  private getAnyField(){
    return element(by.id(this.fields));
  }

  openDatabaseSelectorAccordion() {
    let accordion = element(by.css('.db-open-link'));
    accordion.click();
  }

  getDBCheckBoxes(){
    return element.all(by.css('.dbOptions')).all(by.css('label.checkbox-inline'));
  }

  clearForm(){
    let clearForm = element(by.className('clear-form'));
    clearForm.click();
  }

  getComponentById(compId){
    //fields
    return element(by.id(compId));
  }

  clickPsycTHERAPY(){
    let psycTheraphy = element(by.css('.PH-link'));
    psycTheraphy.click();
  }

  selectAPAFullTextOnly(){
    element.all(by.css('.search-type')).filter( (elem, index)=>{
      return elem.element(by.tagName('span')).getText().then(function(text){
        return text === "APA Full Text";
      })
    }).click();
  }

  getTrendingMenuItems(){
    return element(by.tagName('advanced-trending')).element(by.css('.card')).all(by.tagName('div'));
  }

  memoryTrending(){
    this.getTrendingMenuItems().filter( function(elem,index){
      return elem.element(by.tagName('a')).getText().then(function(text) {
        return text === 'Memory';
      });
    }).click();
  }

  getTypeaheadContainer(){
    return element(by.tagName('typeahead-container'));
  }

  getSearchOptionCheckBoxes(){
    return element.all(by.css('.search-type'));
  }

  prepareOptionsVerification(){
    browser.sleep(1000);

    this.openDatabaseSelectorAccordion();

    //clear all goes here
    this.getDBCheckBoxes().then( function(checkboxes){
      checkboxes[6].click();
    });

    browser.sleep(500);

    let checkes = this.getSearchOptionCheckBoxes();

    expect(checkes.count()).toBe(0);
  }

  selectPsycINFO(){
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === 'PsycINFO') {
          checkbox.click();
        }
      });
    });
  }

  selectPsycARTICLES(){
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === 'PsycARTICLES') {
          checkbox.click();
        }
      });
    });
  }

  selectPsycBOOKS(){
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === 'PsycBOOKS') {
          checkbox.click();
        }
      });
    });
  }

  selectPsycTESTS(){
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === 'PsycTESTS') {
          checkbox.click();
        }
      });
    });
  }

  selectPsycCRITIQUES(){
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === 'PsycCRITIQUES') {
          checkbox.click();
        }
      });
    });
  }

  selectPsycEXTRA(){
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === 'PsycEXTRA') {
          checkbox.click();
        }
      });
    });
  }

  verifyOptions(optionsArray){
    let options = this.getSearchOptionCheckBoxes();
    expect(options.count()).toBe(optionsArray.length);
    this.getSearchOptionCheckBoxes().each((check)=>{
      check.element(by.tagName('span')).getText().then((text) =>{
        expect(optionsArray.indexOf(text) != -1).toBe(true);
      });
    });
  }

  checkOperator(id){

    expect(this.getComponentById(id).isPresent()).toBe(true);

    this.getComponentById(id).click();

    let options = this.getComponentById(id).all(by.tagName('option'));
    let operators = ["AND", "OR", "NOT"];

    options.each((optn)=> {
      expect(optn.isDisplayed()).toBe(true);

      optn.getText().then((text) =>{
        expect(operators.indexOf(text) != -1).toBe(true);
      });
    });

    // select OR
    this.getComponentById(id).all(by.tagName('option')).get(1).click();

    expect(this.getComponentById(id).getAttribute('value')).toEqual("OR");
  }

  checkAnyField(id, fliedOptions){

    expect(this.getComponentById(id).isPresent()).toBe(true);

    this.getComponentById(id).click();

    let options = this.getComponentById(id).all(by.tagName('option'));

    options.each((optn)=> {
      expect(optn.isDisplayed()).toBe(true);
      optn.getText().then((text) =>{
        expect(fliedOptions.indexOf(text) != -1).toBe(true);
      });
    });
  }

  getFilterComponent(){
    return element(by.id('deep_filters_1'));
  }

  addFilterRow(){
    element(by.id('add_filter_row')).click();
  }

  //'filter_entries_1'
  hasFilterSubTypeComponentEnabled(id){
    expect(element(by.id(id)).isDisplayed()).toBe(true);
  }

  clearFilters(){
    element(by.id('clear_filters')).click();
  }

  checkFilterOptions(id, fliedOptions){

    expect(this.getComponentById(id).isPresent()).toBe(true);

    this.getComponentById(id).click();

    let options = this.getComponentById(id).all(by.tagName('option'));

    options.each((optn)=> {
      expect(optn.isDisplayed()).toBe(true);
      optn.getText().then((text) =>{
        expect(fliedOptions.indexOf(text) != -1).toBe(true);
      });
    });

    // select some options
    this.getComponentById(id).all(by.tagName('option')).get(1).click();

    expect(this.getComponentById(id).getAttribute('value')).not.toEqual('');
  }
}

export class LookupsAndRecentSearches {
  navigate() {
    browser.get('/search/advanced');
    browser.sleep(2000);
  }

  checkLookup(name?: string) {
    name = name || 'author';
    let lookups = [
      { name: 'author', id: 'authorLookup', text: 'Author Lookup', field: 'Author' },
      { name: 'journal', id: 'journalLookup', text: 'Journal Lookup', field: 'JournalTitle' },
      { name: 'test', id: 'testLookup', text: 'Test Lookup', field: 'TestNames' },
    ];

    let selectedCase = _.find(lookups, item => { return item.name === name; });

    element(by.id(selectedCase.id)).click();
    browser.sleep(10000);

    expect(element(by.css('lookup-modal .modal-title')).getText()).toBe(selectedCase.text);

    element(by.css('lookup-modal article:last-child a')).getText().then(text => {
      text = (selectedCase.name === 'author') ? text.substr(0, text.lastIndexOf('(')).trim() : text.trim();

      element(by.css('lookup-modal article:last-child input[type="checkbox"]')).click();
      expect(element(by.css('.selection span')).getText()).toContain(text);

      element(by.css('lookup-modal button.btn-primary')).click();
      browser.sleep(1000);

      expect(element(by.id('searchField')).getAttribute('ng-reflect-model')).toBe('{' + text + '}');
      expect(element(by.id('fields')).getAttribute('ng-reflect-model')).toBe(selectedCase.field);
    });
  }

  checkRecentSearches() {
    let trendingLink = element(by.css('advanced-trending .trending-item:last-child a'));

    trendingLink.getText().then(text => {
      trendingLink.click();
      browser.sleep(2000);

      element(by.css('.edit.btn')).click();
      browser.sleep(2000);

      expect(element.all(by.css('adv-recent a')).first().getText()).toBe('Any Field: ' + text);
    });
  }
}
