import { AdvancedSearchForm, LookupsAndRecentSearches } from './advanced.po';
import { browser, by } from 'protractor';

describe('Advanced Search form', function() {
  let page:AdvancedSearchForm;

  beforeEach(() => {
    page = new AdvancedSearchForm();
    page.navigateTo();
    browser.manage().timeouts().implicitlyWait(1000);
  });

  it('should have Advanced Search heading', () => {
    expect(page.getPageHeading()).toEqual('Advanced Search');
  });

  it('should open and close database selector accordion', () => {
    expect(browser.getCurrentUrl()).toContain('/search/advanced');

    page.openDatabaseSelectorAccordion();

    expect(page.getDBCheckBoxes().count()).toEqual(7);

  });

  it('should open PsycTHERAPY in new tab', () => {
    page.openDatabaseSelectorAccordion();

    page.clickPsycTHERAPY();

    browser.sleep(1000);

    browser.getAllWindowHandles().then(function (handles) {

      let newWindowHandle = handles[1]; // this is your new window

      browser.sleep(1000);
      browser.switchTo().window(newWindowHandle).then(function () {
        expect(browser.getCurrentUrl()).toContain('psyctherapy.apa.org');
      });

      browser.driver.close();
      browser.switchTo().window(handles[0]);

    });

  });

  it('should select clear all selected DATABASE', () => {

    page.openDatabaseSelectorAccordion();

    //clear all goes here
    page.getDBCheckBoxes().then( function(checkboxes){
      checkboxes[6].click();
    });

    browser.sleep(1000);

    page.getDBCheckBoxes().then( function(checkboxes){
      for(var check of checkboxes){
        expect(check.isSelected()).toBeFalsy();
      }
    });

  });

  it('should perform a any field search', () => {
    let searchFor = 'psychology';

    browser.sleep(1000);
    page.setSearchInputText(searchFor);

    // make sure text exists within input
    expect(page.getSearchInputText()).toEqual(searchFor);

    // execute the search and make sure it goes to the results page
    page.executeSearch();
    expect(browser.getCurrentUrl()).toContain('/search/results?');

  });

  it('should perform a search with APA Full-Text Only option', () => {

    page.selectAPAFullTextOnly();

    let searchFor = 'psychology';

    browser.sleep(1000);
    page.setSearchInputText(searchFor);

    // make sure text exists within input
    expect(page.getSearchInputText()).toEqual(searchFor);

    // execute the search and make sure it goes to the results page
    page.executeSearch();
    expect(browser.getCurrentUrl()).toContain('/search/results?');

  });

  it('should check trending menu', () => {

    let trend = page.getTrendingMenuItems();

    trend.each((trd)=>{
      expect(trd.isDisplayed()).toBe(true);
    });

    expect(trend.count()).toBe(5);

  });

  it('should open and select Memory trending', () => {

    let trend = page.getTrendingMenuItems();

    trend.each((trd) => {
      expect(trd.isDisplayed()).toBe(true);
    });

    page.memoryTrending();

    expect(browser.getCurrentUrl()).toContain('/search/results?');
  });

  it('should don\'t display search options with none DB selection', () => {

    page.prepareOptionsVerification();
  });

  it('should show and hide Typeahead/ suggestion list', () => {
    let searchFor = 'psychology';

    browser.sleep(1000);

    page.setSearchInputText(searchFor);

    browser.sleep(3000);

    expect(page.getTypeaheadContainer().isDisplayed()).toBe(true);

    page.setSearchInputText('dfdffg');

    browser.sleep(1000);

    expect(page.getTypeaheadContainer().isPresent()).toBe(false);
  });

  describe('Advanced Search form - Search Operator and Any Field Selection', function() {
    beforeEach(() => {
      browser.manage().timeouts().implicitlyWait(1000);
    });

    it('should check, show and hide Operators option and select any one among the list', () => {
      page.checkOperator("operator_0");
    });

    it('should check, show and hide Any Field option and select any one among the list - With ALL DB', () => {
      let fields = ["Any Field", "Author", "Journal Title", "Book Title", "Test Name", "Keywords", "First Page", "Title", "_________________", "Abstract", "Affiliation", "Author of Reviewed Item", "Conference", "Correction Date", "Correspondence", "DOI Number", "Geographic Location", "Grant/Sponsorship", "Index Terms", "ISBN", "ISSN", "Language", "MeSH", "Publication Date", "Publisher", "PubMed ID", "Release Date", "Test Construct", "Tests & Measures", "Title of Reviewed Item", "Unique Identifier", "Year of Reviewed Item"];
      page.checkAnyField("fields", fields);
    });

    it('should check, show and hide Any Field option and select any one among the list - With out DB Selections ', () => {
      page.prepareOptionsVerification();
      let fields = ["Any Field", "Keywords", "_________________", "DOI Number", "Index Terms", "Language", "Unique Identifier"];
      page.checkAnyField("fields", fields);
    });


    it('should check, show and hide Any Field option and select any one among the list - With PI ', () => {
      page.prepareOptionsVerification();

      page.selectPsycINFO();
      let fields = ["Any Field", "Author", "Journal Title", "Book Title", "Keywords", "First Page", "Title", "_________________", "Abstract", "Affiliation", "Author of Reviewed Item", "Conference", "Correction Date", "Correspondence", "DOI Number", "Geographic Location", "Grant/Sponsorship", "Index Terms", "ISBN", "ISSN", "Language", "MeSH", "Publication Date", "Publisher", "PubMed ID", "Release Date", "Tests & Measures", "Title of Reviewed Item", "Unique Identifier", "Year of Reviewed Item"];
      page.checkAnyField("fields", fields);

      page.selectPsycINFO();
    });

    it('should check, show and hide Any Field option and select any one among the list - With PA ', () => {
      page.prepareOptionsVerification();

      page.selectPsycARTICLES();
      let fields = ["Any Field", "Author", "Journal Title", "Keywords", "First Page", "Title", "Full Text", "_________________", "Abstract", "Affiliation", "Author of Reviewed Item", "Conference", "Correction Date", "Correspondence", "DOI Number", "Geographic Location", "Grant/Sponsorship", "Index Terms", "ISSN", "Language", "MeSH", "Publication Date", "Publisher", "PubMed ID", "Release Date", "Tests & Measures", "Title of Reviewed Item", "Unique Identifier", "Year of Reviewed Item"];
      page.checkAnyField("fields", fields);

      page.selectPsycARTICLES();
    });

    it('should check, show and hide Any Field option and select any one among the list - With PB ', () => {
      page.prepareOptionsVerification();

      page.selectPsycBOOKS();
      let fields = ["Any Field", "Author", "Book Title", "Keywords", "First Page", "Title", "_________________", "Abstract", "Affiliation", "Conference", "Correction Date", "Correspondence", "DOI Number", "Geographic Location", "Grant/Sponsorship", "Index Terms", "ISBN", "ISSN", "Language", "MeSH", "Publication Date", "Publisher", "PubMed ID", "Release Date", "Tests & Measures", "Unique Identifier"];
      page.checkAnyField("fields", fields);

      page.selectPsycBOOKS();
    });

    it('should check, show and hide Any Field option and select any one among the list - With PC ', () => {
      page.prepareOptionsVerification();

      page.selectPsycCRITIQUES();
      let fields = ["Any Field", "AUTHOR_OF_REVIEW", "Keywords", "First Page", "Title of Review", "_________________", "Abstract", "Affiliation", "Author of Reviewed Item", "Correction Date", "Correspondence", "DOI Number", "Geographic Location", "Grant/Sponsorship", "Index Terms", "ISSN", "Language", "MeSH", "Publisher", "PubMed ID", "Release Date", "Tests & Measures", "Title of Reviewed Item", "Unique Identifier", "Year of Review", "Year of Reviewed Item"];
      page.checkAnyField("fields", fields);

      page.selectPsycCRITIQUES();
    });

    it('should check, show and hide Any Field option and select any one among the list - With PT ', () => {
      page.prepareOptionsVerification();

      page.selectPsycTESTS();
      let fields = ["Any Field", "Author", "Journal Title", "Test Name", "Keywords", "_________________", "Administration Time", "Correction Date", "DOI Number", "Index Terms", "Language", "MeSH", "Population", "Publisher", "Purpose", "Release Date", "Setting", "Source Document Citation", "Test Construct", "Unique Identifier"];
      page.checkAnyField("fields", fields);

      page.selectPsycTESTS();
    });

    it('should check, show and hide Any Field option and select any one among the list - With PE ', () => {
      page.prepareOptionsVerification();

      page.selectPsycEXTRA();
      let fields = ["Any Field", "Author", "Keywords", "First Page", "Title", "_________________", "Abstract", "Affiliation", "Conference", "Content Owner/Distributor", "Correction Date", "Correspondence", "Document Type ID", "DOI Number", "Geographic Location", "Grant/Sponsorship", "Index Terms", "ISBN", "ISSN", "Language", "MeSH", "Publication Date", "PubMed ID", "Release Date", "Tests & Measures", "Unique Identifier"];
      page.checkAnyField("fields", fields);

      page.selectPsycEXTRA();
    });

  });

  describe('Advanced Search form - Date', function() {

    beforeEach(() => {
      browser.manage().timeouts().implicitlyWait(1000);
    });

    it('should select year selection list ', () => {

      let years_selection = page.getComponentById('years_selection');

      expect(years_selection.isDisplayed()).toBe(true);

      years_selection.click();

      expect(page.getComponentById('years_selection').getAttribute('checked')).toBeTruthy();

      expect(page.getComponentById('years_range').getAttribute('checked')).toBeFalsy();

      expect(page.getComponentById('recently_added').getAttribute('checked')).toBeFalsy();

      page.checkAnyField('years_range_select', ["All years", "1 year ago", "3 years ago", "5 years ago"]);

    });

    it('should select year range list ', () => {

      let years_selection = page.getComponentById('years_range');

      expect(years_selection.isDisplayed()).toBe(true);

      years_selection.click();

      expect(page.getComponentById('years_range').getAttribute('checked')).toBeTruthy();

      expect(page.getComponentById('years_selection').getAttribute('checked')).toBeFalsy();

      expect(page.getComponentById('recently_added').getAttribute('checked')).toBeFalsy();

    });

    it('should select recently added list ', () => {

      let years_selection = page.getComponentById('recently_added');

      expect(years_selection.isDisplayed()).toBe(true);

      years_selection.click();

      expect(page.getComponentById('recently_added').getAttribute('checked')).toBeTruthy();

      expect(page.getComponentById('years_selection').getAttribute('checked')).toBeFalsy();

      expect(page.getComponentById('years_range').getAttribute('checked')).toBeFalsy();

      expect(page.getComponentById('recent_add_select').isPresent()).toBe(true);

      page.getComponentById('recent_add_select').click();
      let fliedOptions = ["in the last 7 days", "in the last 14 days", "in the last 30 days"];
      let options = page.getComponentById('recent_add_select').all(by.tagName('option'));

      options.each((optn)=> {
        expect(optn.isDisplayed()).toBe(true);
        optn.getText().then((text) =>{
          expect(fliedOptions.indexOf(text) != -1).toBe(true);
        });
      });

      page.getComponentById('recent_add_select').all(by.tagName('option')).get(1).click();

      expect(page.getComponentById('recent_add_select').getAttribute('value')).toEqual(fliedOptions[1].toLowerCase());

    });

    it('should select recently added list with latest option when only one DB is selected', () => {

      page.prepareOptionsVerification();

      page.selectPsycEXTRA();

      let years_selection = page.getComponentById('recently_added');

      expect(years_selection.isDisplayed()).toBe(true);

      years_selection.click();

      expect(page.getComponentById('recently_added').getAttribute('checked')).toBeTruthy();

      expect(page.getComponentById('years_selection').getAttribute('checked')).toBeFalsy();

      expect(page.getComponentById('years_range').getAttribute('checked')).toBeFalsy();

      page.getComponentById('recent_add_select').click();
      let options = page.getComponentById('recent_add_select').all(by.tagName('option'));

      expect(options.count()).toBe(4);

      page.selectPsycEXTRA();

    });

  });

  describe('Advanced Search form - Search Type and DB selection', function() {

    beforeEach(() => {
      //clear selection
      page.prepareOptionsVerification();
      browser.manage().timeouts().implicitlyWait(1000);
    });

    it('should display options with some custom DB selection -- Select PI alone', () => {

      page.selectPsycINFO();

      browser.sleep(1000);

      page.verifyOptions(["Peer-Reviewed", "Impact Statement", "Open Access"]);

      //clear PI selection
      page.selectPsycINFO();
    });

    it('should display options with some custom DB selection -- Select PA alone', ()=>{

      page.selectPsycARTICLES();
      page.verifyOptions(["Impact Statement", "Open Access"]);

      //clear selection
      page.selectPsycARTICLES();
    });

    it('should display options with some custom DB selection -- Select PB alone', ()=>{

      page.selectPsycBOOKS();

      page.verifyOptions(["Classic Book", "Conference Proceedings", "Handbook/Manual", "Reference Book", "Textbook/Study Guide", "Authored", "Edited", "General Public", "Professional & Research"]);

      //clear selection
      page.selectPsycBOOKS();
    });

    it('should display options with some custom DB selection -- Select PC alone', ()=>{

      page.selectPsycCRITIQUES();

      page.verifyOptions([]);

      //clear selection
      page.selectPsycCRITIQUES();
    });

    it('should display options with some custom DB selection -- Select PE alone', ()=>{

      page.selectPsycEXTRA();

      page.verifyOptions(["APA Full Text"]);

      //clear selection
      page.selectPsycEXTRA();
    });

    it('should display options with some custom DB selection -- Select PT alone', ()=>{

      page.selectPsycTESTS();

      page.verifyOptions(["Test Available"]);

      //clear selection
      page.selectPsycTESTS();
    });

    it('should display options with some custom DB selection -- Select PA & PB alone', ()=>{

      page.selectPsycARTICLES();
      page.selectPsycBOOKS();
      page.verifyOptions(["Peer-Reviewed", "Impact Statement", "Open Access"]);

      //clear selection
      page.selectPsycARTICLES();
      page.selectPsycBOOKS();
    });

    it('should display options with some custom DB selection -- Select PA & PC alone', ()=>{

      page.selectPsycARTICLES();
      page.selectPsycCRITIQUES();
      page.verifyOptions(["Peer-Reviewed", "Impact Statement", "Open Access"]);

      //clear selection
      page.selectPsycARTICLES();
      page.selectPsycCRITIQUES();
    });

    it('should display options with some custom DB selection -- Select PB & PC alone', ()=>{

      page.selectPsycCRITIQUES();
      page.selectPsycBOOKS();
      page.verifyOptions([]);

      //clear selection
      page.selectPsycCRITIQUES();
      page.selectPsycBOOKS();
    });

    it('should display options with some custom DB selection -- Select PI, PB & PC alone', ()=>{

      page.selectPsycINFO();
      page.selectPsycBOOKS();
      page.selectPsycCRITIQUES();

      page.verifyOptions(["APA Full Text", "Peer-Reviewed", "Impact Statement", "Open Access"]);

      //clear selection
      page.selectPsycINFO();
      page.selectPsycBOOKS();
      page.selectPsycCRITIQUES();
    });

    it('should display options with some custom DB selection -- Select PI & PA alone', ()=>{

      page.selectPsycINFO();
      page.selectPsycARTICLES();
      page.verifyOptions(["APA Full Text", "Peer-Reviewed", "Impact Statement", "Open Access"]);

      //clear selection
      page.selectPsycINFO();
      page.selectPsycARTICLES();
    });

    it('should display options with some custom DB selection -- Select PI & PT alone', ()=>{

      page.selectPsycINFO();
      page.selectPsycTESTS();
      page.verifyOptions(["Peer-Reviewed", "Impact Statement", "Open Access", "Test Available"]);

      //clear selection
      page.selectPsycINFO();
      page.selectPsycTESTS();
    });

    it('should display options with some custom DB selection -- Select PI & PE alone', ()=>{

      page.selectPsycINFO();
      page.selectPsycEXTRA();
      page.verifyOptions(["APA Full Text", "Peer-Reviewed", "Impact Statement", "Open Access"]);

      //clear selection
      page.selectPsycINFO();
      page.selectPsycEXTRA();
    });


    it('should display options with some custom DB selection -- Select PT & PE alone', ()=>{

      page.selectPsycTESTS();
      page.selectPsycEXTRA();
      page.verifyOptions(["Test Available", "APA Full Text"]);

      //clear selection
      page.selectPsycTESTS();
      page.selectPsycEXTRA();
    });

    it('should display options with some custom DB selection -- Select PB & PE alone', ()=>{

      page.selectPsycBOOKS();
      page.selectPsycEXTRA();
      page.verifyOptions(["APA Full Text"]);

      //clear selection
      page.selectPsycBOOKS();
      page.selectPsycEXTRA();
    });

    it('should display options with some custom DB selection -- Select PA, PC & PE alone', ()=>{

      page.selectPsycARTICLES();
      page.selectPsycCRITIQUES();
      page.selectPsycEXTRA();
      page.verifyOptions(["APA Full Text", "Peer-Reviewed", "Impact Statement", "Open Access"]);

      //clear selection
      page.selectPsycARTICLES();
      page.selectPsycCRITIQUES();
      page.selectPsycEXTRA();
    });

    it('should display options with some custom DB selection -- Select PI, PC, PE & PT alone', ()=>{

      page.selectPsycCRITIQUES();
      page.selectPsycINFO();
      page.selectPsycEXTRA();
      page.selectPsycTESTS();
      page.verifyOptions(["APA Full Text", "Peer-Reviewed", "Impact Statement", "Open Access", "Test Available"]);


      //clear selection
      page.selectPsycCRITIQUES();
      page.selectPsycINFO();
      page.selectPsycEXTRA();
      page.selectPsycTESTS();
    });
  });

  describe('Advanced Search form - Search Filters', function() {

    beforeEach(() => {
      browser.sleep(1000);
    });

    it('should display filter options ', () => {

      expect(page.getFilterComponent().isPresent()).toBe(true);

    });

    it('should add one more filter options ', () => {

      page.addFilterRow();

      expect(page.getFilterComponent().isPresent()).toBe(true);

      expect(page.getComponentById('deep_filters_2').isPresent()).toBe(true);

    });

    it('should open and select filter options ', () => {
      let fields = ["Select Filter", "Age Group", "Supplemental Material", "PsycINFO Classification", "Document Type", "Methodology", "Population Group"];
      page.checkFilterOptions('deep_filters_1', fields);

      page.hasFilterSubTypeComponentEnabled('filter_entries_1');
    });

    it('should open and select filter options - With PT alone', () => {
      browser.sleep(1000);

      let fields = ["Select Filter", "Instrument Type", "Fee", "Test Record Type", "Supporting Documentation", "Permissions", "Administration Method", "Age Group", "PsycTESTS Classification", "Population Group"];
      page.prepareOptionsVerification();

      page.selectPsycTESTS();

      page.checkFilterOptions('deep_filters_1', fields);

      page.hasFilterSubTypeComponentEnabled('filter_entries_1');

    });

    it('should open and select filter options - Clear Filters', () => {
      browser.sleep(1000);

      let fields = ["Select Filter", "Instrument Type", "Fee", "Test Record Type", "Supporting Documentation", "Permissions", "Administration Method", "Age Group", "PsycTESTS Classification", "Population Group"];
      page.prepareOptionsVerification();

      page.selectPsycTESTS();

      page.checkFilterOptions('deep_filters_1', fields);

      page.hasFilterSubTypeComponentEnabled('filter_entries_1');

      page.addFilterRow();

      expect(page.getComponentById('deep_filters_2').isPresent()).toBe(true);

      page.clearFilters();

      expect(page.getComponentById('filter_entries_1').isPresent()).toBe(false);

      expect(page.getComponentById('deep_filters_2').isPresent()).toBe(true);

    });
  });

});

describe('Advanced Search Form - Lookups & Recent Searches', () => {
  let page: LookupsAndRecentSearches;

  beforeAll(() => {
    browser.manage().timeouts().implicitlyWait(5000);
    page = new LookupsAndRecentSearches();
  });

  beforeEach(() => page.navigate());

  it('should interact with Author Lookup.', () => page.checkLookup());
  it('should interact with Journal Lookup.', () => page.checkLookup('journal'));
  it('should interact with Test Lookup.', () => page.checkLookup('test'));
  it('should list and interact with Recent Searches.', () => page.checkRecentSearches());
});
