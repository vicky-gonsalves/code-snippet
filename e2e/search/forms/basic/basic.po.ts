import { browser, element, by } from 'protractor';

export class BasicSearchForm {

  navigateTo() {
    return browser.get('/search/basic');
  }

  getPageHeading() {
    let heading = element(by.css('.searchHeading'));
    return heading.getText();
  }

  getSearchInputText() {
    let input = this.getSearchInput();
    return input.getAttribute('value');
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  executeSearch(){
    let searchBtn = element(by.id('searchForBtn'));
    return searchBtn.click();
  }

  private getSearchInput() {
    return element(by.id('searchForInput'));
  }

  openDatabaseSelectorAccordion() {
    let accordion = element(by.css('.db-open-link'));
    accordion.click();
  }

  getDBCheckBoxes(){
    return element.all(by.css('.dbOptions')).all(by.css('label.checkbox-inline'));
  }

  clickPsycTHERAPY(){
    let psycTheraphy = element(by.css('.PH-link'));
    psycTheraphy.click();
  }

  selectAPAFullTextOnly(){
    let ftOnly = element.all(by.css('.optionLeft')).get(1);
    ftOnly.click();
  }

  openTrendingMenu(){
    let trending = browser.findElement(by.id('trending-dropdown'));
    trending.click();
  }

  getTrendingMenu(){
    return element(by.className('trending-dropdown-menu'));
  }

  closeTrendingMenu(){
    let trending = this.getTrendingMenu().element(by.css('.close-action'));
    trending.click();
  }

  memoryTrending(){
    element(by.className('trending-dropdown-menu')).all(by.css('.dropdown-line')).filter( function(elem,index){
      return elem.getText().then(function(text) {
        return text === 'Memory';
      });
    }).click();
  }


  openSearchTips(){
    let trending = browser.findElement(by.id('searchTips'));
    trending.click();
  }

  getSearchTipModal(){
    return element(by.tagName('search-tips')).element(by.css('.modal'));
  }

  closeSearchTripModal(){
    element(by.tagName('search-tips')).element(by.css('.modal-footer')).element(by.tagName('button')).click();
  }

  getTypeaheadContainer(){
    return element(by.tagName('typeahead-container'));
  }

  getSearchOptionCheckBoxes(){
    return element.all(by.css('.optionLeft'));
  }

  selectPsycINFO(){
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === 'PsycINFO') {
          checkbox.click();
        }
      });
    });
  }

  selectPsycARTICLES(){
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === 'PsycARTICLES') {
          checkbox.click();
        }
      });
    });
  }

  selectPsycBOOKS(){
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === 'PsycBOOKS') {
          checkbox.click();
        }
      });
    });
  }

  selectPsycTESTS(){
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === 'PsycTESTS') {
          checkbox.click();
        }
      });
    });
  }

  selectPsycCRITIQUES(){
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === 'PsycCRITIQUES') {
          checkbox.click();
        }
      });
    });
  }

  selectPsycEXTRA(){
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === 'PsycEXTRA') {
          checkbox.click();
        }
      });
    });
  }

  verifyOptions(optionsArray){
    let options = this.getSearchOptionCheckBoxes();

    expect(options.count()).toBe(optionsArray.length);

    this.getSearchOptionCheckBoxes().each((check)=>{
      check.element(by.tagName('span')).getText().then((text) =>{
        expect(optionsArray.indexOf(text) != -1).toBe(true);
      });

    });
  }

  prepareOptionsVerification(){
    browser.sleep(1000);

    this.openDatabaseSelectorAccordion();

    //clear all goes here
    this.getDBCheckBoxes().then( function(checkboxes){
      checkboxes[6].click();
    });

    browser.sleep(500);

    let checkes = this.getSearchOptionCheckBoxes();

    expect(checkes.count()).toBe(0);
  }
}
