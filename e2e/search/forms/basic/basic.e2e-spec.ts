import { BasicSearchForm } from './basic.po';
import { browser } from 'protractor';

describe('Basic Search form', function() {
  let page: BasicSearchForm;

  beforeEach(() => {
    page = new BasicSearchForm();
    page.navigateTo();
  });

  it('should have Basic Search heading', () => {
    expect(page.getPageHeading()).toEqual('Basic Search');
  });

  it('should open and close database selector accordion', () => {
    expect(browser.getCurrentUrl()).toContain('/search/basic');

    page.openDatabaseSelectorAccordion();

    expect(page.getDBCheckBoxes().count()).toEqual(7);

  });

  it('should open PsycTHERAPY in new tab', () => {
    page.openDatabaseSelectorAccordion();

    page.clickPsycTHERAPY();

    browser.sleep(1000);

    browser.getAllWindowHandles().then(function (handles) {

      let newWindowHandle = handles[1]; // this is your new window

      browser.sleep(1000);
      browser.switchTo().window(newWindowHandle).then(function () {
        expect(browser.getCurrentUrl()).toContain('psyctherapy.apa.org');
      });

      browser.driver.close();
      browser.switchTo().window(handles[0]);

    });

  });

  it('should select clear all selected DATABASE', () => {

    page.openDatabaseSelectorAccordion();

    //clear all goes here
    page.getDBCheckBoxes().then( function(checkboxes){
      checkboxes[6].click();
    });

    browser.sleep(1000);

    page.getDBCheckBoxes().then( function(checkboxes){
      for(var check of checkboxes){
        expect(check.isSelected()).toBeFalsy();
      }
    });

  });

  it('should perform a search', () => {
    let searchFor = 'psychology';

    browser.sleep(1000);
    page.setSearchInputText(searchFor);

    // make sure text exists within input
    expect(page.getSearchInputText()).toEqual(searchFor);

    // execute the search and make sure it goes to the results page
    page.executeSearch();
    expect(browser.getCurrentUrl()).toContain('/search/results?');

  });

  it('should perform a search with APA Full-Text Only option', () => {

    page.selectAPAFullTextOnly();

    let searchFor = 'psychology';

    browser.sleep(1000);
    page.setSearchInputText(searchFor);

    // make sure text exists within input
    expect(page.getSearchInputText()).toEqual(searchFor);

    // execute the search and make sure it goes to the results page
    page.executeSearch();
    expect(browser.getCurrentUrl()).toContain('/search/results?');

  });

  it('should open and close trending menu', () => {

    page.openTrendingMenu();

    expect(page.getTrendingMenu().isDisplayed()).toBe(true);

    page.closeTrendingMenu();

    expect(page.getTrendingMenu().isDisplayed()).toBe(false);
  });

  it('should open and select Memory trending', () => {

    page.openTrendingMenu();

    expect(page.getTrendingMenu().isDisplayed()).toBe(true);

    page.memoryTrending();

    expect(browser.getCurrentUrl()).toContain('/search/results?');
  });

  it('should open and close Search Tips', () => {

    page.openSearchTips();

    browser.sleep(1000);

    expect(page.getSearchTipModal().isDisplayed()).toBe(true);

    page.closeSearchTripModal();

    browser.sleep(1000);

    expect(page.getSearchTipModal().isDisplayed()).toBe(false);
  });


  it('should show and hide Typeahead/ suggestion list', () => {
    let searchFor = 'psychology';

    browser.sleep(1000);

    page.setSearchInputText(searchFor);

    browser.sleep(3000);

    expect(page.getTypeaheadContainer().isDisplayed()).toBe(true);

    page.setSearchInputText('dfdffg');

    browser.sleep(1000);

    expect(page.getTypeaheadContainer().isPresent()).toBe(false);
  });

  it('should dont display search options with none DB selection', () => {

    page.prepareOptionsVerification();
  });

  it('should display options with some custom DB selection -- Select PI alone', () => {

    page.prepareOptionsVerification();

    page.selectPsycINFO();

    page.verifyOptions(["Peer-Reviewed", "Latest Update"]);

    //clear PI selection
    page.selectPsycINFO();

  });

  it('should display options with some custom DB selection -- Select PA alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycARTICLES();
    page.verifyOptions(["Latest Update"]);

    //clear selection
    page.selectPsycARTICLES();
  });

  it('should display options with some custom DB selection -- Select PB alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycBOOKS();

    page.verifyOptions(["Latest Update"]);

    //clear selection
    page.selectPsycBOOKS();
  });

  it('should display options with some custom DB selection -- Select PC alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycCRITIQUES();
    page.verifyOptions(["Latest Update"]);

    //clear selection
    page.selectPsycCRITIQUES();
  });

  it('should display options with some custom DB selection -- Select PE alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycEXTRA();

    page.verifyOptions(["APA Full Text", "Latest Update"]);

    //clear selection
    page.selectPsycEXTRA();
  });

  it('should display options with some custom DB selection -- Select PT alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycTESTS();

    page.verifyOptions(["Test Available", "Latest Update"]);

    //clear selection
    page.selectPsycTESTS();
  });

  it('should display options with some custom DB selection -- Select PA & PB alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycARTICLES();
    page.selectPsycBOOKS();
    page.verifyOptions(["Peer-Reviewed", "Latest Update"]);

    //clear selection
    page.selectPsycARTICLES();
    page.selectPsycBOOKS();
  });

  it('should display options with some custom DB selection -- Select PA & PC alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycARTICLES();
    page.selectPsycCRITIQUES();
    page.verifyOptions(["Peer-Reviewed", "Latest Update"]);

    //clear selection
    page.selectPsycARTICLES();
    page.selectPsycCRITIQUES();
  });

  it('should display options with some custom DB selection -- Select PB & PC alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycCRITIQUES();
    page.selectPsycBOOKS();
    page.verifyOptions(["Latest Update"]);

    //clear selection
    page.selectPsycCRITIQUES();
    page.selectPsycBOOKS();
  });

  it('should display options with some custom DB selection -- Select PI, PB & PC alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycINFO();
    page.selectPsycBOOKS();
    page.selectPsycCRITIQUES();

    page.verifyOptions(["Peer-Reviewed", "APA Full Text", "Latest Update"]);

    //clear selection
    page.selectPsycINFO();
    page.selectPsycBOOKS();
    page.selectPsycCRITIQUES();
  });

  it('should display options with some custom DB selection -- Select PI & PA alone', ()=>{

    page.prepareOptionsVerification();

    page.selectPsycINFO();
    page.selectPsycARTICLES();
    page.verifyOptions(["Peer-Reviewed", "APA Full Text", "Latest Update"]);

    //clear selection
    page.selectPsycINFO();
    page.selectPsycARTICLES();
  });

  it('should display options with some custom DB selection -- Select PI & PT alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycINFO();
    page.selectPsycTESTS();
    page.verifyOptions(["Peer-Reviewed", "Test Available", "Latest Update"]);

    //clear selection
    page.selectPsycINFO();
    page.selectPsycTESTS();
  });

  it('should display options with some custom DB selection -- Select PI & PE alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycINFO();
    page.selectPsycEXTRA();
    page.verifyOptions(["Peer-Reviewed", "APA Full Text", "Latest Update"]);

    //clear selection
    page.selectPsycINFO();
    page.selectPsycEXTRA();
  });



  it('should display options with some custom DB selection -- Select PT & PE alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycTESTS();
    page.selectPsycEXTRA();
    page.verifyOptions(["Test Available", "APA Full Text", "Latest Update"]);

    //clear selection
    page.selectPsycTESTS();
    page.selectPsycEXTRA();
  });

  it('should display options with some custom DB selection -- Select PB & PE alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycBOOKS();
    page.selectPsycEXTRA();
    page.verifyOptions(["APA Full Text", "Latest Update"]);

    //clear selection
    page.selectPsycBOOKS();
    page.selectPsycEXTRA();
  });

  it('should display options with some custom DB selection -- Select PA, PC & PE alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycARTICLES();
    page.selectPsycCRITIQUES();
    page.selectPsycEXTRA();
    page.verifyOptions(["Peer-Reviewed", "APA Full Text", "Latest Update"]);

    //clear selection
    page.selectPsycARTICLES();
    page.selectPsycCRITIQUES();
    page.selectPsycEXTRA();
  });

  it('should display options with some custom DB selection -- Select PI, PC, PE & PT alone', ()=>{
    page.prepareOptionsVerification();

    page.selectPsycCRITIQUES();
    page.selectPsycINFO();
    page.selectPsycEXTRA();
    page.selectPsycTESTS();
    page.verifyOptions(["Peer-Reviewed", "APA Full Text", "Test Available", "Latest Update"]);


    //clear selection
    page.selectPsycCRITIQUES();
    page.selectPsycINFO();
    page.selectPsycEXTRA();
    page.selectPsycTESTS();
  });



});
