import { browser, element, by } from 'protractor';

export class MyList {
  private itemsCount = 0;

  initialize() {
    this.setUser('56789');
    this.addRecords();
  }

  navigate() {
    browser.get('/');
    element(by.css('personalization li:nth-child(2) a')).click();
    browser.sleep(3000);
  }

  checkPage(cb) {
    element(by.css('my-list')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  checkResultsCount(cb) {
    element.all(by.css('.result-item')).count().then(count => cb(+count === this.itemsCount));
  }

  hideSidePaneWithNoTags(cb) {
    element(by.css('.facets')).isPresent().then(isPresent => cb(!isPresent));
  }

  AddTag(cb) {
    let tag = { tags: 'important;most-cited', note: 'This is an important item, since it is highly cited.' };
    this.editTag(tag);

    element.all(by.css('filter-box a')).count().then(sideCountBefore => {
      element.all(by.css('.result-item-tag')).count().then(itemTagsCountBefore => {

        element(by.css('.note-icon')).click();
        element(by.css('.note .caption')).getText().then(text => {
          let success = (sideCountBefore === 2 && itemTagsCountBefore === sideCountBefore) && (text === tag.note);

          this.editTag({ tags: 'most-cited' });
          element.all(by.css('filter-box a')).count().then(sideCountAfter => {
            element.all(by.css('.result-item-tag')).count().then(itemTagsCountAfter => {
              success = success && (sideCountAfter === 1 && itemTagsCountAfter === sideCountAfter);
              cb(success);
            });
          });
        });
      });
    });
  }

  filterByTag(cb) {
    element(by.css('filter-box a')).click()
    browser.sleep(1000);

    element.all(by.css('.result-item')).count().then(itemsCount => {
      element.all(by.css('.result-item .result-item-tag')).count().then(tagCount => {
        element(by.css('filter-box a')).click()
        browser.sleep(1000);
        cb(itemsCount === 1 && tagCount === 1)
      });
    });
  }

  toggleSidePane(cb) {
    element(by.css('.listIcon')).click();
    browser.sleep(500);
    element(by.css('filter-box')).isDisplayed().then(isDisplayed => {
      let switchedOff = !isDisplayed;

      element(by.css('.listIcon')).click();
      browser.sleep(500);
      element(by.css('filter-box')).isDisplayed().then(isDisplayed => {
        let switchedOn = isDisplayed;
        cb(switchedOff && switchedOn);
      });
    });
  }

  deleteAllItems(cb) {
    element(by.css('search-results-nav:first-child select-all input[type="checkbox"]')).click();
    element(by.css('search-results-nav:first-child .batch-delete')).click();
    browser.sleep(1000);

    element(by.css('search-results-nav:first-child delete-confirmation-modal .form-group button:first-child')).click();
    browser.sleep(3000);

    element.all(by.css('.result-item')).count().then(count => {
      element(by.css('personalization .badge')).isPresent().then(isPresent => cb(!isPresent && count === 0));
    });
  }

  checkEmptyResultsMessage(cb) {
    element(by.id('emptyMsg')).getText().then(text => cb(text.indexOf('There are no saved records in your list') === 0));
  }

  accessAsGuest(cb) {
    this.setUser(-1);
    this.navigate();
    element(by.css('my-list')).isPresent().then(isPresent => cb(isPresent));
  }

  conclude() {
    this.setUser('12345');
  }

  private setUser(userId) {
    browser.get('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys(userId);

    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }

  private addRecords() {
    // Add arbitrary records from the search results page.
    browser.get('/search/basic');
    element(by.id('searchForBtn')).click();
    browser.sleep(5000);

    element(by.css('personalization .badge')).isPresent().then(isPresent => {
      if (!isPresent) {
        element(by.css('search-results-nav:first-child select-all input[type="checkbox"]')).click();
        element(by.css('search-results-nav:first-child search-results-tools-bar .batch-add')).click();
        browser.sleep(2000);
      }
      element(by.css('personalization .badge')).getText().then(text => this.itemsCount = +text);
      browser.sleep(2000);
    });
  }

  private editTag(tag) {
    element(by.css('.result-item:first-child')).element(by.id('tagnote')).click();

    element(by.id('tags0')).clear();
    element(by.id('tags0')).sendKeys(tag.tags);

    element(by.id('notes0')).clear();
    if (tag.note && tag.note.length) {
      element(by.id('notes0')).sendKeys(tag.note);
    }

    element(by.css('addtagnote button[type="submit"]')).click();
    browser.sleep(5000);
  }

  sortBy(sortby){
    return element.all(by.css('sort-by')).first().element(by.cssContainingText('option', sortby)).click();
  }

  getSelectedSortOrder(){
    return element.all(by.css('sort-by')).first().element(by.css('option:checked')).getText();
  }
}
