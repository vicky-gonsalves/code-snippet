import {browser} from 'protractor';
import {MyList} from './myList.po';

// Initializing this test suite needs more time (about 25 - 35 seconds, based on the slow network connection).
// So this timeout should hold the test suite, in order to conduct all its checks successfully.
jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

describe('My List', () => {
  let page: MyList;

  beforeAll(() => {
    browser.manage().timeouts().implicitlyWait(5000);
    page = new MyList();
    page.initialize();
    page.navigate();
  });

  it('should display the correct page.', () => page.checkPage(success => expect(success).toBeTruthy()));
  it('should display records.', () => page.checkResultsCount(success => expect(success).toBeTruthy()));
  it('should hide the filters side-pane, when no tags exist.', () => page.hideSidePaneWithNoTags(success => expect(success).toBeTruthy()));
  it('should retain sort order when add/edit a (tag/note)', () => {
    let sortBy = 'Author';
    page.sortBy(sortBy);
    page.AddTag(success => {
      expect(success).toBeTruthy();
      expect(page.getSelectedSortOrder()).toEqual(sortBy);
    });
  });
  it('should add/edit a (tag/note), for some result item.', () => page.AddTag(success => expect(success).toBeTruthy()));
  it('should filter the results by tag.', () => page.filterByTag(success => expect(success).toBeTruthy()));
  it('should toggle the filters side-pane.', () => {
    page.AddTag(success => {
      page.toggleSidePane(success => expect(success).toBeTruthy())
    })
  });
  it('should delete all selected records, using (Delete From My List) within the top tools-bar.', () => page.deleteAllItems(success => expect(success).toBeTruthy()));
  it('should display a message, when no results are found.', () => page.checkEmptyResultsMessage(success => expect(success).toBeTruthy()));
  it('should not display this page for a guest user.', () => page.accessAsGuest(success => expect(success).toBeFalsy()));


  afterAll(() => page.conclude());
});
