import { browser } from 'protractor';
import { CitedRefsResults } from './citedRefsResults.po';

describe('Cited References Results', () => {
  let page: CitedRefsResults;

  beforeAll(() => {
    browser.manage().timeouts().implicitlyWait(5000);
    page = new CitedRefsResults();
    page.performSearch();
  });

  it('should display the correct page.', () => page.checkDisplayedPage(success => expect(success).toBeTruthy()));
  it('should display results.', () => page.checkResultsCount(success => expect(success).toBeTruthy()));
  it('should display references count and friendly query.', () => page.getResultsStatement(fq => expect(fq).toMatch(/[\d,]* References for [\w\s:\/\*\–]*/g)));
  it('should use pagination.', () => page.usePagination(success => expect(success).toBeTruthy()));
  it('should change sort criteria.', () => page.applySort(success => expect(success).toBeTruthy()));
  it('should change results number per page.', () => page.applyPaginationLimit(success => expect(success).toBeTruthy()));

  it('should perform search within results.', () => page.performSearchWithinResults('social', success => expect(success).toBeTruthy()));
  it('should not perform search within results, when search term is empty.', () => page.performSearchWithinResults('', success => expect(success).toBeFalsy()));

  it('should apply result filters.', () => page.applyFilter(success => expect(success).toBeTruthy()));
  it('should have working (Cited By) links.', () => page.navigateToCitedBy(success => expect(success).toBeTruthy()));

  it('should edit the search.', () => page.editSearch(success => expect(success).toBeTruthy()));
  it('should initiate a new search.', () => page.newSearch(success => expect(success).toBeTruthy()));
  it('should save the search.', () => page.saveSearch(success => expect(success).toBeTruthy()));
  it('should not save the search using a duplicate name.', () => page.saveSearch(success => expect(success).toBeFalsy()));
  it('should display a message when no results are found.', () => page.checkNoResultsCase(success => expect(success).toBeTruthy()));
});
