import { browser, element, by } from 'protractor';

export class CitedRefsResults {
  private data = {
    lastName: 'Baron'
  };

  performSearch(data?) {
    browser.get('/search/citedRefs');
    data = data || this.data;

    for (let fieldId in data) {
      element(by.id(fieldId)).sendKeys(this.data[fieldId]);
    }
    element(by.css('form button[type="submit"]')).click();
  }

  checkDisplayedPage(cb) {
    element(by.css('citedrefsresults')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  checkResultsCount(cb) {
    element.all(by.css('article')).count().then(count => cb(count > 0));
  }

  getResultsStatement(cb) {
    element(by.css('.navbar')).getText().then(text => cb(text));
  }

  usePagination(cb) {
    // When the results are sorted by (times-cited), a first page reference would have higher (or equal) citation-times value, than those coming in the next pages.
    element(by.css('article:first-child .citedby-count')).getText().then(page1FirstCount => {
      // Go to page 2
      element(by.css('search-results-pagination:first-child numberedpagination.hidden-xs li:nth-child(2) a')).click();

      element(by.css('article:first-child .citedby-count')).getText().then(page2FirstCount => cb(+page1FirstCount >= +page2FirstCount));
    });
  }

  applySort(cb) {
    this.performSearch();

    element(by.css('article:first-child .srhcSource span:last-child')).getText().then(firstYear => {
      // Apply sort by Year (descending)
      element(by.css('search-results-nav:first-child sort-by select')).click();
      element(by.css('search-results-nav:first-child sort-by select option:last-child')).click();
      browser.sleep(1000);

      element(by.css('article:first-child .srhcSource span:last-child')).getText().then(secondYear => cb(+secondYear >= +firstYear));
    });
  }

  applyPaginationLimit(cb) {
    element.all(by.css('article')).count().then(firstCount => {
      // Apply pagination limit = 50
      element(by.css('search-results-nav:first-child paginationlimit select')).click();
      element(by.css('search-results-nav:first-child paginationlimit select option:last-child')).click();
      browser.sleep(1000);

      element.all(by.css('article')).count().then(secondCount => cb(firstCount === 25 && firstCount < secondCount && secondCount === 50));
    });
  }

  performSearchWithinResults(term: string, cb) {
    element(by.css('.total-records')).getText().then(firstResultsCount => {
      firstResultsCount = firstResultsCount.replace(',', '');

      element(by.id('searchText')).sendKeys(term);
      element(by.css('search-within-results-box button[type="submit"]')).click();
      browser.sleep(1000);

      element(by.css('.total-records')).getText().then(secondResultsCount => {
        secondResultsCount = secondResultsCount.replace(/,/g, '');
        cb(+firstResultsCount > +secondResultsCount);
      });
    });
  }

  applyFilter(cb) {
    let filterLink = element(by.css('search-filter-box .filter-display:first-child li:first-child a'));
    filterLink.element(by.css('.filter-count')).getText().then(text => {
      let filterCount = text.replace(/[\(\),]/g, '');
      filterLink.click();
      browser.sleep(1000);

      element(by.css('.total-records')).getText().then(totalFiltered => {
        totalFiltered = totalFiltered.replace(/,/g, '');
        cb(+totalFiltered === +filterCount);
      });
    });
  }

  navigateToCitedBy(cb) {
    element(by.css('article:first-child a')).click();
    element(by.css('cited-by')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  editSearch(cb) {
    this.performSearch();
    element(by.css('button.edit')).click();
    element(by.css('searchcitedrefs')).isDisplayed().then(success => cb(success));
  }

  newSearch(cb) {
    this.performSearch();
    element(by.css('button.new')).click();
    element(by.css('searchcitedrefs')).isDisplayed().then(success => cb(success));
  }

  saveSearch(cb) {
    this.performSearch();
    element(by.css('button.save')).click();
    element(by.id('searchName')).sendKeys('Baron Citations');
    browser.sleep(500);
    element(by.css('save-search-modal button[type="submit"]')).click();
    browser.sleep(2000);

    element(by.css('mypsycnet')).isPresent().then(success => cb(success));
  }

  checkNoResultsCase(cb) {
    this.performSearch({ lastName: 'Baron', year: '1800' });
    element(by.css('.no-results')).getText().then(text => cb(text.indexOf('did not match any documents') > 0));
  }
}
