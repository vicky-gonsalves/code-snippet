import {SearchResultsPage} from './results.po';
import {browser} from 'protractor';

describe('Search Results Page - Individual User', function () {
  tests('individual');
});

describe('Search Results Page - Guest User', function () {
  tests('guest');
});

describe('Search Results Page - Organization User', function () {
  tests('org');
});

function tests(userType) {
  let page: SearchResultsPage;

  beforeEach(() => {
    browser.manage().timeouts().implicitlyWait(5000);
    page = new SearchResultsPage();
    if (userType == 'individual') {
      page.accessAsIndividualUser();
    } else if (userType == 'guest') {
      page.accessAsGuestUser();
    } else if (userType == 'org') {
      page.accessAsOrganizationUser();
    }
    page.navigate();
    page.search();
  });


  it('should have search action bar', () => {
    expect(page.getSearchActionBar().count()).toBe(1);
  });

  it('should show friendly query on search action bar', () => {
    expect(page.getFriendlyQuery().count()).toBe(1);
  });

  it('should redirect to search form page if Edit Search Button is clicked', () => {
    page.getEditSearchButtton().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/search\/basic/g).test(url);
        expect(regexTest).toBe(true);
      })
    })
  });
  it('should redirect to search form page if New Search Button is clicked', () => {
    page.getNewSearchButtton().click().then(() => {
      browser.getCurrentUrl().then(url => {
        let regexTest = (/search\/basic/g).test(url);
        expect(regexTest).toBe(true);
      })
    })
  });

  it('should Save Search', () => {
    page.getSaveSearchButtton().click().then(() => {
      page.saveSearch();
      browser.sleep(3000);
      browser.getCurrentUrl().then(url => {
        let regexTest = (/MyPsycNET\/savedSearches/g).test(url);
        expect(regexTest).toBe(true);
      })
    })
  });

  it('should Get Permalink', () => {
    page.getPermalinkButtton().click().then(() => {
      browser.sleep(3000);
      page.getPermalinkInput().getAttribute('value').then(text => {
        let regexTest = (/permalink\/[-a-zA-Z0-9]/g).test(text[0]);
        expect(regexTest).toBe(true);
      })
    })
  });
  it('should Set Email Alert', () => {
    page.getEmailAlertButtton().click().then(() => {
      browser.sleep(4000);
      browser.getCurrentUrl().then(url => {
        let regexTest = (/MyPsycNET\/alerts/g).test(url);
        expect(regexTest).toBe(true);
      })
    })
  });

  it('should Get RSS Feed', () => {
    page.getRssButtton().click().then(() => {
      browser.sleep(4000);
      browser.getAllWindowHandles().then(function (handles) {
        browser.ignoreSynchronization = true;
        let newWindowHandle = handles[1];
        browser.switchTo().window(newWindowHandle);
        expect(browser.getWindowHandle()).toEqual(newWindowHandle);
        browser.driver.close();
        browser.switchTo().window(handles[0]);
      });
    })
  });

  it('should have search tabs', () => {
    expect(page.getSearchTabsContainer().count()).toBe(1);
  });

  it('should search in each tab', () => {
    page.getSearchTabs().get(1).click().then(() => {
      browser.sleep(7000);
      browser.getCurrentUrl().then(url => {
        let regexTest = (/tab=PI/g).test(url);
        expect(regexTest).toBe(true);

        page.getSearchTabs().get(2).click().then(() => {
          browser.sleep(4000);
          browser.getCurrentUrl().then(url => {
            let regexTest = (/tab=PA/g).test(url);
            expect(regexTest).toBe(true);

            page.getSearchTabs().get(3).click().then(() => {
              browser.sleep(4000);
              browser.getCurrentUrl().then(url => {
                let regexTest = (/tab=PE/g).test(url);
                expect(regexTest).toBe(true);

                page.getSearchTabs().get(0).click().then(() => {
                  browser.sleep(4000);
                  browser.getCurrentUrl().then(url => {
                    let regexTest = (/tab=all/g).test(url);
                    expect(regexTest).toBe(true);
                  })
                })
              })
            })
          })
        })
      })
    })
  });

  it('should have search-within-results-box component', () => {
    browser.sleep(4000);
    expect(page.getSearchWithinResultComp().count()).toBe(1);
  });

  it('should Search and then Remove search from search-within-results-box component', () => {
    browser.sleep(7000);
    let searchFor = 'Deaf culture: Exploring deaf communities in the United States.';
    page.searchWithinSearchResults(searchFor);
    browser.sleep(7000);
    page.getArticles().first().getText().then(text => {
      expect(page.removeHighlights(text)).toEqual(searchFor);
      page.getSearchedItem().first().getText().then(text => {
        expect(searchFor.indexOf(text.replace('...', ''))).toBe(0);
        page.getSearchedItemLink().first().click().then(() => {
          page.getSearchedItem().count().then(count => {
            expect(count).toBe(0);
          });
        })
      })
    })
  });

  it('should have search-filter-box component', () => {
    browser.sleep(4000);
    expect(page.getSearchFilterComp().count()).toBe(1);
  });

  it('should have search query result component', () => {
    browser.sleep(4000);
    expect(page.searchQueryResult().count()).toBeTruthy();
  });

  it('should search based on filters and remove filter', () => {
    browser.sleep(4000);
    page.getFirstFilter().first().click().then(() => {
      browser.sleep(4000);
      page.getFirstFilterRemoveButton().count().then(count => {
        expect(count).toBe(1);
        page.getFirstFilter().first().click().then(() => {
          browser.sleep(4000);
          page.getFirstFilterRemoveButton().count().then(count => {
            expect(count).toBe(0);
          })
        })
      })
    })
  });

  it('should have search-results-nav component', () => {
    browser.sleep(4000);
    expect(page.searchResultNav().count()).toBeTruthy();
  });

  it('should have select-all component', () => {
    browser.sleep(4000);
    expect(page.getSelectAll().count()).toBeTruthy();
  });

  it('should have search-results-tools-bar component', () => {
    browser.sleep(4000);
    expect(page.searchResultToolBar().count()).toBeTruthy();
  });

  it('should have paginationLimit component', () => {
    browser.sleep(4000);
    expect(page.getPaginationLimit().count()).toBeGreaterThan(0);
  });

  it('should have sort-by component', () => {
    browser.sleep(4000);
    expect(page.getSortBy().count()).toBeGreaterThan(0);
  });

  it('should display correct number of records', () => {
    browser.sleep(4000);
    let limit: number;
    page.getPaginationLimit().first().getAttribute('ng-reflect-selected-limit').then(_l => {
      limit = parseInt(_l);
      expect(page.getArticleList().count()).toBe(limit);
    });
  });

  it('should export selected records', () => {
    browser.sleep(4000);
    page.selectTwoArticles();
    page.getExportButton().click().then(() => {
      browser.sleep(1000);
      expect(page.exportRecords()).toBe(null);
    })
  });

  it('should send Email', () => {
    browser.sleep(4000);
    page.selectTwoArticles();
    page.getEmailButton().click().then(() => {
      browser.sleep(1000);
      expect(page.sendEmail()).toBe(null);
    })
  });

  it('should Print', () => {
    browser.sleep(4000);
    page.selectTwoArticles();
    page.getPrintButton().click().then(() => {
      browser.sleep(2000);
      expect(page.printRecords()).toBe(null);
    })
  });

  it('should Add to My List', () => {
    browser.sleep(4000);
    page.selectTwoArticles();
    expect(page.getAddToMyListButton().click()).toBe(null);
  });

  it('should Add to My List', () => {
    browser.sleep(4000);
    page.selectTwoArticles();
    expect(page.getAddToMyListButton().click()).toBe(null);
  });

  it('should open record display page', () => {
    browser.sleep(4000);
    page.getFirstArticle().click().then(() => {
      browser.sleep(4000);
      browser.getCurrentUrl().then(url => {
        let regexTest = (/search\/display/g).test(url);
        expect(regexTest).toBe(true);
      })
    })
  });

  it('should open record display page', () => {
    browser.sleep(4000);
    page.getFirstArticle().click().then(() => {
      browser.sleep(4000);
      browser.getCurrentUrl().then(url => {
        let regexTest = (/search\/display/g).test(url);
        expect(regexTest).toBe(true);
      })
    })
  });

  it('should open abstract', () => {
    browser.sleep(4000);
    page.getFirstAbstract().click().then(() => {
      page.getOpenAbstract().isDisplayed().then(isDisplayed => {
        expect(isDisplayed).toBeTruthy();
      });
    })
  });

  it('should open references', () => {
    browser.sleep(4000);
    page.getFirstReferences().click().then(() => {
      browser.sleep(8000);
      page.getOpenReferences().isDisplayed().then(isDisplayed => {
        expect(isDisplayed).toBeTruthy();
      });
    })
  });

  it('should have My List button', () => {
    browser.sleep(4000);
    expect(page.getAddToMyList().count()).toBeTruthy();
  });
}
