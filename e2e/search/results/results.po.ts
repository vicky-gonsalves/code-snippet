import {browser, element, by} from 'protractor';

export class SearchResultsPage {
  navigate(path?: string) {
    return (path && path.length) ? browser.get(path) : browser.get('/search/basic');
  }

  getPageHeading() {
    let heading = element(by.css('browsepagesubheader h1'));
    return heading.getText();
  }

  search() {
    let searchFor = 'psychology';
    browser.sleep(1000);
    this.setSearchInputText(searchFor);
    this.executeSearch();
  }

  executeSearch() {
    let searchBtn = element(by.id('searchForBtn'));
    return searchBtn.click();
  }

  private getSearchInput() {
    return element(by.id('searchForInput'));
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }


  accessAsGuestUser() {
    this.setUserAsAnonymous();
    this.navigate();
  }

  accessAsIndividualUser() {
    this.setUserAsIndividual();
    this.navigate();
  }

  accessAsOrganizationUser() {
    // this.togglePCEntitlement();
    this.navigate();
  }

  private getBadgeValue() {
    return element(by.css('.navigation-menu .badge')).getText();
  }

  private setUserAsAnonymous(revert?: boolean) {
    this.navigate('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    if (revert) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');
      element(by.css('identitymanagement input[type=submit]')).click();

      browser.sleep(1000);

      element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
      for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
        element(by.id(thing)).click();
      }
      element(by.css('entitlements input[type=submit]')).click();

    } else {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');

      element(by.css('identitymanagement')).element(by.id('individualId')).clear();
      element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');
      element(by.css('identitymanagement input[type=submit]')).click();
    }

    browser.sleep(1000);
  }

  private setUserAsIndividual(revert?: boolean) {
    this.navigate('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    let value = revert ? '329008' : '-1';

    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys(value);
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
    this.navigate('/admin');
    browser.sleep(1000);
    // this.togglePCEntitlement();
  }

  // private togglePCEntitlement() {
  //   this.navigate('/admin');
  //
  //   element(by.id('PC')).click();
  //   element(by.css('entitlements input[type=submit]')).click();
  //   browser.sleep(1000);
  // }


  getSearchActionBar() {
    return element.all(by.css('search-action-bar'));
  }

  getFriendlyQuery() {
    return element.all(by.css('search-action-bar friendly-query'));
  }

  getEditSearchButtton() {
    return element.all(by.css('.searchoptions button')).first();
  }

  getNewSearchButtton() {
    return element.all(by.css('.searchoptions button')).get(1);
  }

  getSaveSearchButtton() {
    return element.all(by.css('search-action-bar .buttonsbar button')).first();
  }

  saveSearch() {
    let searchFor = 'psychology ' + Math.random();
    browser.sleep(1000);
    this.setSaveSearchInputText(searchFor);
    this.executeSaveSearch();
  }

  executeSaveSearch() {
    let searchBtn = element(by.css('.modal-body .row .form-group button[type="submit"]'));
    return searchBtn.click();
  }

  private getSaveSearchInput() {
    return element(by.id('searchName'));
  }

  setSaveSearchInputText(str) {
    let input = this.getSaveSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  getPermalinkInput() {
    return element.all(by.id('permalink'));
  }

  getPermalinkButtton() {
    return element.all(by.css('search-action-bar .buttonsbar button')).get(1);
  }

  getEmailAlertButtton() {
    return element.all(by.css('search-action-bar .buttonsbar button')).get(2);
  }

  getRssButtton() {
    return element.all(by.css('search-action-bar .buttonsbar button')).get(3);
  }

  getSearchTabsContainer() {
    return element.all(by.css('search-tabs'));
  }

  getSearchTabs() {
    return element.all(by.css('search-tabs .searchtabscontainer .tabsContainer .nav-tabs>li'));
  }

  getSearchWithinResultComp() {
    return element.all(by.css('search-within-results-box'));
  }

  searchWithinSearchResults(searchFor) {
    browser.sleep(1000);
    this.setSearchWithinInputText(searchFor);
    this.executeSearchWithin();
  }

  executeSearchWithin() {
    let searchBtn = element(by.css('.searchBtnEmb'));
    return searchBtn.click();
  }

  private getSearchWithinInput() {
    return element(by.id('searchText'));
  }

  setSearchWithinInputText(str) {
    let input = this.getSearchWithinInput();
    input.clear();
    return input.sendKeys(str);
  }

  getArticles() {
    return element.all(by.css('article a>span'));
  }

  getSearchedItem() {
    return element.all(by.css('search-within-results-box .list-unstyled li>a>span'));
  }

  getSearchedItemLink() {
    return element.all(by.css('search-within-results-box .list-unstyled li>a'));
  }

  removeHighlights(text) {
    var regExp = /(<span class=""hilite"">|<span class='hilite'>|<\/span>)/gi;
    var result = '';
    if (text && typeof text === 'string') {
      result = text.replace(regExp, '');
    }
    return result;
  }

  getSearchFilterComp() {
    return element.all(by.css('search-filter-box'));
  }

  getFirstFilter() {
    return this.getSearchFilterComp().all(by.css('.accordionContainer')).first().all(by.css('ul>li>a'));
  }

  getFirstFilterRemoveButton() {
    return this.getFirstFilter().first().all(by.css('.glyphicon-remove'));
  }

  searchQueryResult() {
    return element.all(by.css('.label.label-success'));
  }

  searchResultNav() {
    return element.all(by.css('search-results-nav'));
  }

  getSelectAll() {
    return element.all(by.css('select-all'));
  }

  searchResultToolBar() {
    return element.all(by.css('search-results-tools-bar'));
  }

  getPaginationLimit() {
    return element.all(by.css('paginationlimit'));
  }

  getSortBy() {
    return element.all(by.css('sort-by'));
  }

  getArticleList() {
    return element.all(by.css('article'));
  }

  selectTwoArticles() {
    this.getArticleList().get(0).all(by.css('input[type="checkbox"]')).first().click();
    this.getArticleList().get(1).all(by.css('input[type="checkbox"]')).first().click();
  }

  getExportButton() {
    return this.searchResultToolBar().all(by.css('button')).first();
  }

  exportRecords() {
    return element.all(by.css('export-ris-modal form button')).first().click();
  }

  getEmailButton() {
    return this.searchResultToolBar().all(by.css('button')).get(1);
  }

  getPrintButton() {
    return this.searchResultToolBar().all(by.css('button')).get(2);
  }

  printRecords() {
    return element.all(by.css('print-record-modal')).first().all(by.css('button[type="submit"]')).first().click();
  }

  getAddToMyListButton() {
    return this.searchResultToolBar().all(by.css('button')).get(3);
  }

  sendEmail() {
    browser.sleep(1000);
    let to = 'somebody@somewhere.com';
    let msg = 'Test Message';
    this.setEmailFields(to, msg);
    return this.executeSendEmail();
  }

  executeSendEmail() {
    let sendBtn = element.all(by.css('email-record-modal')).first().all(by.css('form button[type="submit"]')).first();
    return sendBtn.click();
  }

  private getTo() {
    return element(by.id('toTopNav'));
  }

  private getMessage() {
    return element(by.id('messageTopNav'));
  }

  setEmailFields(email_to, email_msg) {
    let to = this.getTo();
    let msg = this.getMessage();
    to.clear();
    msg.clear();
    to.sendKeys(email_to);
    return msg.sendKeys(email_msg);
  }

  getFirstAbstract() {
    return this.getArticleList().first().all(by.css('searchresults-buttonbar')).first().element(by.id('abstract'));
  }

  getOpenAbstract() {
    return this.getArticleList().first().all(by.css('searchresults-buttonbar')).first().element(by.id('absopen'));
  }

  getFirstReferences() {
    return this.getArticleList().first().all(by.css('searchresults-buttonbar')).first().element(by.id('references'));
  }

  getFirstArticle() {
    return this.getArticleList().first().all(by.css('a'));
  }

  getOpenReferences() {
    return this.getArticleList().first().all(by.css('searchresults-buttonbar')).first().element(by.id('refopen'));
  }

  getAddToMyList() {
    return this.getArticleList().first().all(by.css('searchresults-buttonbar')).first().all(by.css('add-to-my-list'));
  }

}
