import { browser } from 'protractor';

import { OptionsToBuy } from './optionsToBuy.po';

describe('Options to Buy', () => {
  let page: OptionsToBuy;

  beforeAll(() => {
    browser.manage().timeouts().implicitlyWait(5000);

    page = new OptionsToBuy();
    page.setUserAndEntitlements();
  });

  it('should display options for a journal article.', () => page.displayArticleOption(success => expect(success).toBeTruthy()));

  it('should display options for a book chapter.', () => page.displayChapterOption(success => expect(success).toBeTruthy()));

  it('should display options for a relevant database.', () => page.displayDbOptions(success => expect(success).toBeTruthy()));

  it('should add a UID item to cart.', () => page.AddUIDItem(success => expect(success).toBeTruthy()));

  it('should add a Database item to cart.', () => page.AddDbItem(success => expect(success).toBeTruthy()));

  it('should prevent the user from adding the same item twice.', () => page.AddDbItem(success => expect(success).toBeFalsy()));

  it('should look for more items, after adding some item.', () => page.AddThenLookForMore(success => expect(success).toBeTruthy()));

  xit('should proceed to checkout, after adding an item to cart.', () => { });

  it('should get out because of a non-relevant database.', () => page.tryNonEligibleDb(success => expect(success).toBeFalsy()));

  it('should get out because of a non-relevant record.', () => page.tryNonEligibleUID(success => expect(success).toBeFalsy()));

  it('should get out because the user belongs to an organization.', () => page.accessAsOrganization(success => expect(success).toBeFalsy()));

  afterAll(() => page.resetUserAndEntitlements());
});
