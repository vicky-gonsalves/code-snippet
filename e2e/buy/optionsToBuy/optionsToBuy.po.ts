import { browser, element, by } from 'protractor';

export class OptionsToBuy {
  navigate(path: string) {
    browser.get(path);
  }

  displayArticleOption(cb) {
    this.navigate('/buy/2015-55825-001');
    element(by.css('h2.item.header')).getText().then(text => cb(text === 'Article Selected'));
  }

  displayChapterOption(cb) {
    this.navigate('/buy/2016-13386-001');
    element(by.css('h2.item.header')).getText().then(text => cb(text === 'Book Chapter Selected'));
  }

  displayDbOptions(cb) {
    this.navigate('/buy/PI');
    element(by.css('.day-option:first-child h2')).getText().then(text => {
      element.all(by.css('.day-option')).count().then(count => {
        element(by.css('.day-option:first-child .pi.title')).isDisplayed().then(isDisplayed => {
          cb(text === 'Database Selected' && count === 2 && isDisplayed);
        });
      });
    });
  }

  AddUIDItem(cb) {
    this.navigate('/buy/2015-55825-001');
    browser.sleep(2000);

    element(by.css('identitybar .cart-count')).getText().then(cartCountBefore => {
      element(by.css('add-to-cart .btn')).click();
      browser.sleep(1000);

      element(by.css('add-to-cart .btn')).getText().then(text => {
        element(by.css('identitybar .cart-count')).getText().then(cartCountAfter => {
          cb((+cartCountAfter === (+cartCountBefore + 1)) && (text === 'ADDED TO CART'));
        });
      });
    });
  }

  AddDbItem(cb) {
    this.navigate('/buy/PC');
    browser.sleep(2000);

    element(by.css('identitybar .cart-count')).getText().then(cartCountBefore => {
      element(by.css('.day-option:first-child add-to-cart .btn')).click();
      browser.sleep(1000);

      element(by.css('.day-option:first-child add-to-cart .btn')).getText().then(text => {
        element(by.css('identitybar .cart-count')).getText().then(cartCountAfter => {
          cb((+cartCountAfter === (+cartCountBefore + 1)) && (text === 'ADDED TO CART'));
        });
      });
    });
  }

  AddThenLookForMore(cb) {
    this.navigate('/PsycARTICLES');
    browser.sleep(1000);
    this.navigate('/buy/PE');

    element(by.css('.day-option:last-child add-to-cart .btn')).click();
    browser.sleep(1000);

    element(by.css('after-add-modal .btn-info')).click();
    browser.sleep(1000);
    element(by.css('browsepa')).isDisplayed().then(isDisplayed => cb(isDisplayed));
  }

  tryNonEligibleDb(cb) {
    this.navigate('/buy/PA');
    element.all(by.css('.day-option')).count().then(count => cb(count > 0));
  }

  tryNonEligibleUID(cb) {
    // This record comes from PC.

    this.navigate('/buy/2016-36027-001');
    element.all(by.css('.uid-option')).count().then(count => cb(count > 0));
  }

  accessAsOrganization(cb) {
    this.navigate('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');

    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);

    this.navigate('/buy/2015-55825-001');
    element.all(by.css('.uid-option')).count().then(count => cb(count > 0));
  }

  setUserAndEntitlements() {
    this.setUserAsAnonymous();
  }

  resetUserAndEntitlements() {
    this.setUserAsAnonymous(true);
  }

  private setUserAsAnonymous(revert?: boolean) {
    this.navigate('/admin');
    browser.sleep(2000);

    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();

    if (revert) {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');

      element(by.css('identitymanagement input[type=submit]')).click();

      browser.sleep(1000);

      element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
      for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
        element(by.id(thing)).click();
      }
      element(by.css('entitlements input[type=submit]')).click();

    } else {
      element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
      element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');

      element(by.css('identitymanagement input[type=submit]')).click();

      browser.sleep(1000);

      element(by.css('admin-tabs > .nav-tabs > li:nth-child(1) > a')).click();
      for (let thing of ['PI', 'PA', 'PB', 'PC', 'PE', 'PT', 'PH']) {
        element(by.id(thing)).click();
      }
      element(by.css('entitlements input[type=submit]')).click();
    }

    browser.sleep(1000);
  }
}
