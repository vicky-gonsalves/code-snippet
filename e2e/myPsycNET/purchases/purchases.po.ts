import { browser, element, by } from 'protractor';

export class MyPsycNETPurchases {

  navigateTo() {
    browser.get('/MyPsycNET/purchases');
    browser.sleep(1000);
  }

  getSelectedTab() {
    return element(by.css('.mypntabscontainer ul>li:nth-child(3)'));
  }

  getPurchasesType() {
    return element(by.css('h1>span:first-child'));
  }

  getPurchasesCount(){
    return element(by.css('h1>span:last-child'));
  }

  getPurchaseRecord() {
    return element.all(by.css('tbody>tr'));
  }

  changePurchasesType(index) {
    let type = element.all(by.css('select option')).get(index);
    return type.click();
  }

  getIconLabel(index) {
    return element.all(by.css('.iconLabel')).get(index);
  }

  getEmptyState() {
    return element(by.css('.emptyState'));
  }

  getexpirationState(index) {
    return element.all(by.css('.expirationState')).get(index).getAttribute("textContent");
  }

  getRenewButton(index) {
    return element.all(by.css('td .link')).get(index);
  }

  getProductTitle(index) {
    return element.all(by.css('td .title')).get(index);
  }

  getProductUID(index) {
    return element.all(by.css('.productUID')).get(index).getAttribute("textContent");
  }

  getPDFLink(index) {
    return element.all(by.css('.fulltext > a:first-child')).get(index);
  }

  getHTMLLink(index) {
    return element.all(by.css('.fulltext > a:last-child')).get(index);
  }

   removeIdintity() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('-1');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
    this.navigateTo();
  }

  revertIdentity() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('organizationId')).clear();
    element(by.css('identitymanagement')).element(by.id('organizationId')).sendKeys('329008');
    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }
}
