import { MyPsycNETPurchases  } from './purchases.po';
import { browser } from 'protractor';

describe('MyPsycNET - Purchases Page', function() {
  let page: MyPsycNETPurchases ;

  beforeEach(() => {
    page = new MyPsycNETPurchases ();
    page.navigateTo();
    browser.manage().timeouts().implicitlyWait(9000);
  });

  it('should open purchases page', () => {
    expect(browser.getCurrentUrl()).toContain('/MyPsycNET/purchases');
  });

  it('should select "APA PsycNET Purchases" tab', () => {
    expect(page.getSelectedTab().getAttribute('class')).toBe("active");
  });

  it('should display "Full-text and databases" type',() => {
    expect(page.getPurchasesType().getText()).toBe("Full-text and databases");
  });

  it('should display correct number of purchases', () => {
    page.getPurchaseRecord().count().then(count=>{
      page.getPurchasesCount().getText().then(text=> {
        expect(count).toBe(parseInt(text));
      });
    });
  });

  it('should display correct number of purchases when type is changed', () => {
    page.changePurchasesType(1).then(() => {
      page.getPurchaseRecord().count().then(count=>{
        page.getPurchasesCount().getText().then(text=> {
          expect(count).toBe(parseInt(text));
        });
      });
    });
  });

  it('should display "Articles" heading when "Articles" type is selected', () => {
    page.changePurchasesType(1).then(() => {
      expect(page.getPurchasesType().getText()).toBe("Articles");
    });
  });

  it('should display articles and prints when "Articles" type is selected', () => {
    page.changePurchasesType(1).then(() => {
      page.getPurchaseRecord().then(list => {
        list.forEach((val,index) => {
        expect(page.getIconLabel(index).getText()).toMatch(/(Article|Print)/g);
        });
      });
    });
  });

  it('should display book chapters when "Book Chapters" type is selected', () => {
    page.changePurchasesType(2).then(() => {
      page.getPurchaseRecord().then(list => {
        list.forEach((val,index) => {
        expect(page.getIconLabel(index).getText()).toContain("Book Chapter");
        });
      });
    });
  });

  it('should display "No purchases" when "Reviews" type is selected', () => {
    page.changePurchasesType(3).then(() => {
      expect(page.getEmptyState().isDisplayed()).toBe(true);
    });
  });

  it('should display annual subscriptions and database passes when "Database subscriptions and day passes" type is selected', () => {
    page.changePurchasesType(4).then(() => {
      page.getPurchaseRecord().then(list => {
        list.forEach((val,index) => {
        expect(page.getIconLabel(index).getText()).toMatch(/(Annual Subscription|Database Pass)/g);
        });
      });
    });
  });

  it('should display RENEW button when access is expired', () => {
    page.getPurchaseRecord().then(list => {
      let renewIndex = 0;
      list.forEach((val,index) => {
        page.getexpirationState(index).then(text => {
          if(JSON.parse(text) == true) {
            expect(page.getRenewButton(renewIndex).isDisplayed()).toBe(true);
            renewIndex ++;
          }
        });
      });
    });
  });

  it('should redirect user to the option to buy page when the RENEW button is clicked', () => {
    page.getRenewButton(1).click();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/buy');
  });

  it('should open the Journal page when the title of print Subscription is clicked', () => {
    page.changePurchasesType(1).then(() => {
      browser.sleep(1000);
      page.getProductTitle(0).click().then(() => {
        browser.getAllWindowHandles().then(handles => {
        let newWindowHandle = handles[1]; // this is the journal window
        browser.switchTo().window(newWindowHandle).then(() => {
          browser.getCurrentUrl().then( url =>{
            expect(url).toContain('/PsycARTICLES/journal/');
          });
        });
        browser.driver.close();
        browser.switchTo().window(handles[0]);
      });
      });
    });
  });

  it('should open the article when the title of article is clicked', () => {
    page.changePurchasesType(1).then(() => {
      browser.sleep(1000);
      page.getProductUID(1).then(uid => {
        page.getProductTitle(1).click().then(() => {
          browser.getAllWindowHandles().then(handles => {
          let newWindowHandle = handles[1]; // this is the article window
          browser.switchTo().window(newWindowHandle).then(() => {
            browser.getCurrentUrl().then(url => {
              expect(url).toContain('/record/' + uid);
            });
          });
          browser.driver.close();
          browser.switchTo().window(handles[0]);
          });
        });
      });
    });
  });

  it('should open the chapter page when the title of book chapter is clicked', () => {
    page.changePurchasesType(2).then(() => {
      browser.sleep(1000);
      page.getProductUID(0).then(uid => {
        page.getProductTitle(0).click().then(() => {
          browser.getAllWindowHandles().then(handles => {
          let newWindowHandle = handles[1]; // this is the book window
          browser.switchTo().window(newWindowHandle).then(() => {
            browser.getCurrentUrl().then(url => {
              expect(url).toContain('/record/' + uid);
            });
          });
          browser.driver.close();
          browser.switchTo().window(handles[0]);
          });
        });
      });
    });
  });

  it('should open the correct pdf link', () => {
    page.changePurchasesType(1).then(() => {
      browser.sleep(1000);
      page.getProductUID(1).then(uid => {
        page.getPDFLink(0).click().then(() => {
          browser.getAllWindowHandles().then(handles => {
          let newWindowHandle = handles[1]; // this is the pdf window
          browser.switchTo().window(newWindowHandle).then(() => {
            browser.getCurrentUrl().then(url => {
              expect(url).toContain('/fulltext/' + uid+'.pdf');
            });
          });
          browser.driver.close();
          browser.switchTo().window(handles[0]);
          });
        });
      });
    });
  });

  it('should open the correct html link', () => {
    page.changePurchasesType(1).then(() => {
      browser.sleep(1000);
      page.getProductUID(1).then(uid => {
        page.getHTMLLink(0).click().then(() => {
          browser.getAllWindowHandles().then(handles => {
          let newWindowHandle = handles[1]; // this is the html window
          browser.switchTo().window(newWindowHandle).then(() => {
            browser.getCurrentUrl().then(url => {
              expect(url).toContain('/fulltext/' + uid+'.html');
            });
          });
          browser.driver.close();
          browser.switchTo().window(handles[0]);
          });
        });
      });
    });
  });

  it('should not allow any guest users to access this page.', () => {
    page.removeIdintity();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/apa/idm/login.seam');
  });

  afterAll(() =>  {
    page.revertIdentity();
  });
});
