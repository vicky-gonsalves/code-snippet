import { MyPsycNETSavedSearches  } from './savedSearches.po';
import { browser, by } from 'protractor';

describe('MyPsycNET - Saved Searches Page', function() {
  let page: MyPsycNETSavedSearches ;

  beforeEach(() => {
    page = new MyPsycNETSavedSearches ();
    page.navigateTo();
    browser.manage().timeouts().implicitlyWait(9000);
  });

  it('should open the correct component', () => {
    expect(browser.getCurrentUrl()).toContain('/savedSearches');
  });

  it('should select Saved Searches tab', () => {
    expect(page.getSelectedTab().getAttribute('class')).toBe("active");
  });

  it('should have \'Saved Searches\' heading', () => {
    expect(page.getPageHeading()).toEqual('Saved Searches');
  });

  it('should have Name, Query, Database, Alert and Action labels',() => {
    expect(page.getHeaderLabel(0).isDisplayed()).toBe(true);
    expect(page.getHeaderLabel(1).isDisplayed()).toBe(true);
    expect(page.getHeaderLabel(2).isDisplayed()).toBe(true);
    expect(page.getHeaderLabel(3).isDisplayed()).toBe(true);
  });

  it('should have Database and Action vlaues for all alerts', () => {
    page.getSavedSearchesList().then(list => {
      list.forEach((val,index) => {
      expect(page.getDatabaseValue(index).getText()).toBeTruthy();
      expect(page.getActionButtonsCount(index)).toBeGreaterThan(2);
      });
    });
  });

  it('should save search from recent search page where PT and PTH databases are selected', () => {
    let typeOfPage = 'recent searches'
    let name = 'Test search';
    page.search('play', 'PsycTESTS');
    browser.sleep(4000);
    let query;
    page.openRecentSearchesPage();
    page.getFriendlyQuery(typeOfPage).getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
      query = text;
    }).then(() =>{
      page.saveSearch(name, typeOfPage);
      browser.sleep(4000);
      page.getQueryValue(0).getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
        expect(text).toBe(query);
        expect(page.getNameValue(0).getText()).toBe(name);
        expect(page.getDatabaseValue(0).getText()).toBe('PsycTESTS, PsycTHERAPY');
        expect(page.getActionButtonsCount(0)).toBe(4);
      });
    });
  });

  it('should save search from search result page where PA and PTH databases are selected', () => {
    let typeOfPage = 'search result'
    let name = 'computer search';
    page.search('computer', 'PsycARTICLES');
    browser.sleep(4000);
    let query;
    page.getFriendlyQuery(typeOfPage).getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
      query = text;
    }).then(() =>{
      page.saveSearch(name, typeOfPage);
      browser.sleep(4000);
      page.getQueryValue(0).getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
        expect(text).toBe(query);
        expect(page.getNameValue(0).getText()).toBe(name);
        expect(page.getDatabaseValue(0).getText()).toBe('PsycARTICLES, PsycTHERAPY');
        expect(page.getActionButtonsCount(0)).toBe(4);
      });
    });
  });

  it('should run the correct search when the name of any saved search is clicked',() => {
    let query;
    let databases;
    page.getQueryValue(0).getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
        query = text;
    });
    page.getDatabaseValue(0).getText().then(dbs =>{
      databases = dbs;
    });
    page.getNameValue(0).click().then(() => {
      browser.sleep(2000);
       page.getFriendlyQuery('search result').getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
         expect(text).toBe(query);
       });
    }).then(() =>{
       expect(page.getSearchDBs().count()).toBe(3);
       expect(page.getSearchDBs().get(1).getText()).toContain('PsycARTICLES');
       expect(page.getSearchDBs().get(2).getText()).toContain('PsycTHERAPY');
    });
  });

  it('should display correct Permalink', () => {
    let permalink;
    let query;
    page.getQueryValue(0).getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
      query = text;
    }).then(() => {
      page.getPermalinkButton(0).then(() =>{
        page.getPermalink().getAttribute('ng-reflect-value').then(link => {
          permalink = link.replace('http://localhost:9001', '');
        }).then(() => {
          browser.get(permalink);
          browser.sleep(4000);
          page.getFriendlyQuery('search result').getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
            expect(text).toBe(query);
          });
        });
      });
    });
  });

  it('should open RSS Feed', () => {
    let searchId;
    page.getSearchId(0).then(id => {
      searchId = id;
      page.getRSSButton(0);
      browser.sleep(1000);
    }).then(() => {
      browser.getAllWindowHandles().then(function (handles) {
        let newWindowHandle = handles[1]; // this is the RSS window
        browser.switchTo().window(newWindowHandle).then(function () {
          browser.getCurrentUrl().then( url =>{
            expect(url).toContain(searchId);
          });
        });
        browser.driver.close();
        browser.switchTo().window(handles[0]);
      });
    });
  });

  it('should edit search',() => {
    let searchedTerm;
    let databases;
    page.getQueryValue(0).element(by.css('.fq-value')).getText().then(text =>{
      searchedTerm = text;
    });
    page.getDatabaseValue(0).getText().then(dbs =>{
      databases = dbs;
    });
    page.getEditButton(0).then(() => {
      browser.sleep(2000);
      page.getSearchField().getAttribute('ng-reflect-model').then(text => {
        expect(text).toBe(searchedTerm.toLowerCase());
      });
      page.checkSelectedDB(1).getAttribute('ng-reflect-model').then((value) =>{
        expect(value).toBe('true');
      });
      expect(browser.getCurrentUrl()).toContain('/search/advanced');
    });
  });

  it('should set email alert',() => {
    let query;
    let name;
   page.getQueryValue(0).getText().then(text =>{
      query = text;
    });
    page.getNameValue(0).getText().then(text =>{
      name = text;
    });
    page.setAlert(0).then(() => {
      browser.sleep(4000);
      page.getAlertNameValue(0).getText().then(text => {
        expect(text.trim()).toBe(name);
      });
      page.getAlertQueryValue(0).getText().then(text => {
        expect(text).toBe(query);
      });
      expect(browser.getCurrentUrl()).toContain('/alerts');
    }).then(() => {
      page.navigateTo();
    }).then(() => {
      expect(page.getAlertButton(0).isPresent()).toBe(false);
    });
  });

  it('should display comfirmation message for deletion', () => {
    page.deleteSearch(0).then(() =>{
      browser.sleep(1000);
      expect(page.getDeleteConfirmationModel().getAttribute('aria-hidden')).toBe('false');
    });
  });

  it('should remove specific record from saved searches as well as from topic alerts', () => {
    let name;
    page.getNameValue(0).getText().then(text =>{
      name = text;
    });
    page.deleteSearch(0).then(() =>{
      browser.sleep(1000);
      page.cofirmDeletion();
    }).then(() =>{
      expect(name === page.getNameValue(0).getText()).toBe(false);
    }).then(() => {
      page.openTopicAlertsComponent();
      browser.sleep(4000);
      expect(name === page.getAlertNameValue(0).getText()).toBe(false);
    });
  });

   it('should remove \'Test Search\' record from saved searches', () => {
    page.deleteSearch(0).then(() =>{
      browser.sleep(1000);
      page.cofirmDeletion();
    });
  });

  it('should display login page for guest or organization', () => {
    page.removeIndividual();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/apa/idm/login.seam');
  });

  afterAll(() =>  {
    page.revertIdentity();
    browser.sleep(1000);
  });
});
