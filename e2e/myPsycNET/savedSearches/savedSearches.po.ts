import { browser, element, by } from 'protractor';

export class MyPsycNETSavedSearches {

  navigateTo() {
    browser.get('/MyPsycNET/savedSearches');
    browser.sleep(1000);
  }

  getSelectedTab() {
    return element(by.css('.mypntabscontainer ul>li:nth-child(4)'));
  }

  getPageHeading() {
    let heading = element(by.tagName('h1'));
    return heading.getText();
  }

  getHeaderLabel(index) {
    return element.all(by.css('.header')).get(index);
  }

  getSavedSearchesList() {
    return element.all(by.css('.record-list'));
  }

  getNameValue(index) {
    return this.getSavedSearchesList().get(index).element(by.css('.run-search > span'));
  }

  getQueryValue(index) {
    return this.getSavedSearchesList().get(index).element(by.css('friendly-query > span'));
  }

  getDatabaseValue(index) {
    return this.getSavedSearchesList().get(index).element(by.css('.databases'));
  }

  getAlertValue(index) {
    return this.getSavedSearchesList().get(index).element(by.css('.alert'));
  }

  getActionButtonsCount(index) {
    return this.getSavedSearchesList().get(index).element(by.css('.saved-search-btn-grp')).all(by.css('button')).count();
  }

  getSavedSearchesCount () {
    return element.all(by.css('.record-list')).count();
  }

  search(searchFor, db) {
    browser.get('/search/basic');
    browser.sleep(1000);
    this.setSearchInputText(searchFor);
    this.selectDatabase(db);
    this.executeSearch();
  }

  executeSearch() {
    let searchBtn = element(by.id('searchForBtn'));
    return searchBtn.click();
  }

  private getSearchInput() {
    return element(by.id('searchForInput'));
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  getSaveButtton(type) {
    if (type == 'search result') {
      return element.all(by.css('search-action-bar .buttonsbar button')).get(0);
    }
    return element.all(by.css('article button:nth-child(2)')).get(0);
  }

  getFriendlyQuery(type) {
    if (type == 'search result') {
      return element(by.css('friendly-query > span'));
    }
    return element.all(by.css('friendly-query > span')).get(0);
  }

  openRecentSearchesPage() {
    element(by.css('personalization .top-menu li:nth-child(1)')).click();
  }

  saveSearch (name, type) {
    this.getSaveButtton(type).click().then(() =>{
      browser.sleep(2000);
      let input = element(by.css('save-search-modal input'));
      input.clear();
      input.sendKeys(name);
      return element(by.css('save-search-modal .btn-success')).click();
    });
  }

  openDatabaseSelectorAccordion() {
    let accordion = element(by.css('.db-open-link'));
    accordion.click();
  }

  getDBCheckBoxes(){
    return element.all(by.css('.dbOptions')).all(by.css('label.checkbox-inline'));
  }

  checkSelectedDB(dbOrder) {
    this.openDatabaseSelectorAccordion();
    return this.getDBCheckBoxes().get(dbOrder).element(by.tagName('input'));
  }

  selectDatabase(db){
    this.openDatabaseSelectorAccordion();
    this.getDBCheckBoxes().get(6).click(); //deselest all
    this.getDBCheckBoxes().each(function(checkbox){
      checkbox.element(by.tagName('span')).getText().then((text) =>{
        if (text === db) {
          checkbox.click();
        }
      });
    });
  }

  getSearchDBs() {
    return element.all(by.css('.tabsContainer ul > li'))
  }

  getPermalinkButton(index) {
    return this.getSavedSearchesList().get(index).element(by.css('.saved-search-btn-grp')).all(by.css('button')).get(1).click();
  }

  getPermalink() {
    return element(by.css('permalink-input input'));
  }

  getRSSButton(index) {
    return this.getSavedSearchesList().get(index).element(by.css('.saved-search-btn-grp')).element(by.css('.RSS')).click();
  }

  getSearchId (index) {
    return element.all(by.css('.searchId')).get(index).getAttribute("textContent");
  }

  setAlert(index) {
    return this.getSavedSearchesList().get(index).element(by.css('.saved-search-btn-grp')).element(by.id('alertBtn')).click();
  }

  getAlertButton(index) {
    return this.getSavedSearchesList().get(index).element(by.css('.saved-search-btn-grp')).element(by.id('alertBtn'));
  }

  getEditButton(index) {
    return this.getSavedSearchesList().get(index).element(by.css('.saved-search-btn-grp')).all(by.css('button')).get(0).click();
  }

  getSearchField() {
    return element(by.id('searchField'));
  }

  getAlertsList() {
    return element.all(by.css('.record-list'));
  }

  getAlertNameValue(index) {
    return this.getAlertsList().get(index).element(by.css('.search-name'));
  }

  getAlertQueryValue(index) {
    return this.getAlertsList().get(index).element(by.css('.search-query'));
  }

  deleteSearch(index) {
    return element.all(by.css('.delete-search')).get(index).click();
  }

  getDeleteConfirmationModel() {
    return element(by.css('delete-confirmation-modal > div'));
  }

  cofirmDeletion() {
    return element(by.css('delete-confirmation-modal .btn-success')).click();
  }

  openTopicAlertsComponent() {
    element(by.css('.mypntabscontainer ul>li:nth-child(2)')).click();
  }

  removeIndividual() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
    browser.get('/MyPsycNET/savedSearches');
  }

  revertIdentity() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }
}
