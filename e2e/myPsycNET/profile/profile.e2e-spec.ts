import { MyPsycNETProfile  } from './profile.po';
import { browser } from 'protractor';

describe('MyPsycNET - profile Page', function() {
  let page: MyPsycNETProfile ;

  beforeEach(() => {
    page = new MyPsycNETProfile ();
    page.navigateTo();
    browser.manage().timeouts().implicitlyWait(9000);
  });

  it('should select My Profile tab', () => {
    expect(page.getSelectedTab().getAttribute('class')).toBe("active");
  });

  it('should have \'My Profile\' heading', () => {
    expect(page.getPageHeading()).toEqual('My Profile');
  });

  it('should have Name and Email labels',() => {
    expect(page.getNameLabel().isDisplayed()).toBe(true);
    expect(page.getEmailLabel().isDisplayed()).toBe(true);
  });

  it('should have EDIT button',() => {
    expect(page.getEditButton().isDisplayed()).toBe(true);
  });

  it('should have correct account name',() => {
    expect(page.getAccountName().getText()).toBe(page.getUserName().getText());
  });

  it('should allow user to edit his profile', () => {
    page.getEditButton().click();
    browser.getAllWindowHandles().then(function (handles) {
      let newWindowHandle = handles[1]; // this is the edit profile window
      browser.switchTo().window(newWindowHandle).then(function () {
        browser.getCurrentUrl().then( url =>{
          expect(browser.getCurrentUrl()).toContain('/portal/editprofile');
        });
      });
      browser.driver.close();
      browser.switchTo().window(handles[0]);
    });
  });

  it('should display login page for guest or organization', () => {
    page.removeIndividual();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/apa/idm/login.seam');
  });

  afterAll(() =>  {
    page.revertIdentity();
    browser.sleep(1000);
  });
});
