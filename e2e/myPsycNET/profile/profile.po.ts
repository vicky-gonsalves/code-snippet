import { browser, element, by } from 'protractor';

export class MyPsycNETProfile {

  navigateTo() {
    browser.get('/MyPsycNET/profile');
    browser.sleep(1000);
  }

  getSelectedTab() {
    return element(by.css('.mypntabscontainer ul>li:first-child'));
  }

  getPageHeading() {
    let heading = element(by.tagName('h1'));
    return heading.getText();
  }

  getNameLabel() {
    return element.all(by.css('strong > span')).get(0)
  }

  getEmailLabel() {
    return element.all(by.css('strong > span')).get(1)
  }

  getEditButton() {
    return element(by.css('.myProfile a'));
  }

  getUserName() {
    return element.all(by.css('.myProfile p')).get(0);
  }

  getAccountName() {
    return element.all(by.css('.dropdown>a.dropdown-toggle')).get(1);
  }

  removeIndividual() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
    browser.get('/MyPsycNET/profile');
  }

  revertIdentity() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }
}
