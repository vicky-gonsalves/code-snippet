import { browser, element, by } from 'protractor';

export class MyPsycNETTopicAlters {

  navigateTo() {
    browser.get('/MyPsycNET/alerts');
    browser.sleep(1000);
  }

  getSelectedTab() {
    return element(by.css('mypsycnet-alerts ul>li:nth-child(1)'))
  }

  getPageHeading() {
    let heading = element.all(by.tagName('h1 > span')).get(0);
    return heading.getText();
  }

  getHeaderLabel(index) {
     return element.all(by.css('.header')).get(index);
  }

  getAlertsList() {
    return element.all(by.css('.record-list'));
  }

  getNameValue(index) {
    return this.getAlertsList().get(index).element(by.css('.search-name'));
  }

  getQueryValue(index) {
    return this.getAlertsList().get(index).element(by.css('.search-query'));
  }

  getExpiresValue(index) {
    return this.getAlertsList().get(index).element(by.css('.search-expires'));
  }

  getActionButtonsCount(index) {
    return this.getAlertsList().get(index).element(by.css('.search-action')).all(by.css('button')).count();
  }

  getSelectedAlertsCount () {
    return element.all(by.css('.record-list')).count();
  }

  getHeadingCount() {
    return element(by.css('.count')).getText();
  }

  search(searchFor) {
    browser.get('/search/basic');
    browser.sleep(1000);
    this.setSearchInputText(searchFor);
    this.executeSearch();
  }

  executeSearch() {
    let searchBtn = element(by.id('searchForBtn'));
    return searchBtn.click();
  }

  private getSearchInput() {
    return element(by.id('searchForInput'));
  }

  setSearchInputText(str) {
    let input = this.getSearchInput();
    input.clear();
    return input.sendKeys(str);
  }

  getEmailAlertButtton(type) {
    if (type == 'search result') {
      return element.all(by.css('search-action-bar .buttonsbar button')).get(2);
    } else if (type == 'recent searches') {
      return element.all(by.css('article button:nth-child(5)')).get(0);
    } else if(type == 'saved searches') {
      return element.all(by.css('td button:nth-child(3)')).get(0);
    }
  }

  getFriendlyQuery(type) {
    if (type == 'search result') {
      return element(by.css('friendly-query > span'));
    }
    return element.all(by.css('friendly-query > span')).get(0);
  }

  openRecentSearchesPage() {
    element(by.css('personalization .top-menu li:nth-child(1)')).click();
  }

  saveSearch (name) {
    element.all(by.css('search-action-bar .buttonsbar button')).get(0).click().then(() =>{
      browser.sleep(2000);
      let input = element(by.css('save-search-modal input'));
      input.clear();
      input.sendKeys(name);
      element(by.css('save-search-modal .btn-success')).click();
    });
  }

  deleteAlert(index) {
    return element.all(by.css('.delete-alert')).get(index).click();
  }

  getDeleteConfirmationModel() {
    return element(by.css('delete-confirmation-modal > div'));
  }

  cofirmDeletion() {
    return element(by.css('delete-confirmation-modal .btn-success')).click();
  }

  openEditAlertForm(index) {
    return this.getAlertsList().get(index).element(by.css('.search-action')).all(by.css('button')).get(0).click();
  }

  getEditingForm() {
    return element(by.css('.record-edit'));
  }

  getEditingComponent(index) {
    return element(by.css('.record-edit > form')).all(by.css('.form-group')).get(index);
  }

  getEditingButton() {
     return element(by.css('.record-edit > form button'));
  }

  editNameOfAlert (newName) {
    let input = this.getEditingComponent(0).element(by.css('input'));
    input.clear();
    input.sendKeys(newName);
    this.getEditingButton().click();
  }

  getAlertName(index) {
    return this.getNameValue(index).element(by.css('.alert-name'));
  }

  getPermalinkButton(index) {
    return this.getAlertsList().get(index).element(by.css('.search-action')).all(by.css('button')).get(1).click();
  }

  getPermalink() {
    return element(by.css('permalink-input input'));
  }

  getRSSButton(index) {
    return this.getAlertsList().get(index).element(by.css('.search-action')).all(by.css('button')).get(2).click();
  }

  getSearchId (index) {
    return element.all(by.css('.searchId')).get(index).getAttribute("textContent");
  }

  removeIndividual() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
    browser.get('/MyPsycNET/alerts');
  }

  revertIdentity() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }
}
