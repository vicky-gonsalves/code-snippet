import { MyPsycNETTopicAlters  } from './topicAlerts.po';
import { browser } from 'protractor';

describe('MyPsycNET - Topic Alerts Page', function() {
  let page: MyPsycNETTopicAlters ;

  beforeEach(() => {
    page = new MyPsycNETTopicAlters ();
    page.navigateTo();
    browser.manage().timeouts().implicitlyWait(9000);
  });

  it('should open the correct component', () => {
    expect(browser.getCurrentUrl()).toContain('/alerts');
  });

  it('should select Topic Alerts tab', () => {
    expect(page.getSelectedTab().getAttribute('class')).toBe("active");
  });

  it('should have \'Manage Topic Alerts\' heading', () => {
    expect(page.getPageHeading()).toEqual('Manage Topic Alerts');
  });

  it('should have Name, Query, Expires and Action labels',() => {
    expect(page.getHeaderLabel(0).isDisplayed()).toBe(true);
    expect(page.getHeaderLabel(1).isDisplayed()).toBe(true);
    expect(page.getHeaderLabel(2).isDisplayed()).toBe(true);
    expect(page.getHeaderLabel(3).isDisplayed()).toBe(true);
  });

  it('should have Expires and Action vlaues for all alerts', () => {
    page.getAlertsList().then(list => {
      list.forEach((val,index) => {
      expect(page.getExpiresValue(index).getText()).toBeTruthy();
      expect(page.getActionButtonsCount(index)).toBe(3);
      });
    });
  });

  it('should display correct count of Email Alerts', () => {
    page.getSelectedAlertsCount().then(count => {
      page.getHeadingCount().then(hCount => {
        expect(parseInt(hCount)).toBe(count);
      });
    });
  });

  it('should add Email Alert from search result page', () => {
    let typeOfPage = 'search result'
    page.search('happy');
    let query;
    page.getFriendlyQuery(typeOfPage).getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
      query = text.replace(/<(?:.|\n)*?>/g, '');
    }).then(() =>{
      page.getEmailAlertButtton(typeOfPage).click().then(() => {
        browser.sleep(4000);
        expect(page.getQueryValue(0).getText()).toBe(query);
      });
    });
  });

  it('should add Email Alert from recent searches page', () => {
    let typeOfPage = 'recent searches'
    page.search('test');
    browser.sleep(3000);
    let query;
    page.openRecentSearchesPage();
    page.getFriendlyQuery(typeOfPage).getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
      query = text.replace(/<(?:.|\n)*?>/g, '');
    }).then(() =>{
      page.getEmailAlertButtton(typeOfPage).click().then(() => {
        browser.sleep(4000);
        expect(page.getQueryValue(0).getText()).toBe(query);
      });
    });
  });

  it('should add Email Alert from saved searches page', () => {
    let typeOfPage = 'saved searches'
    page.search('computer');
    browser.sleep(4000);
    page.saveSearch('test saved search');
    let query;
    browser.sleep(4000);
    page.getFriendlyQuery(typeOfPage).getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
      query = text.replace(/<(?:.|\n)*?>/g, '');
    }).then(() =>{
      page.getEmailAlertButtton(typeOfPage).click().then(() => {
        browser.sleep(4000);
        expect(page.getQueryValue(0).getText()).toBe(query);
      });
    });
  });

  it('should display comfirmation message for deletion', () => {
    page.deleteAlert(0).then(() =>{
      browser.sleep(1000);
      expect(page.getDeleteConfirmationModel().getAttribute('aria-hidden')).toBe('false');
    });
  });

  it('should remove specific record from Topic Alerts', () => {
    page.deleteAlert(0).then(() =>{
      browser.sleep(1000);
      page.cofirmDeletion();
    }).then(() =>{
      page.getSelectedAlertsCount().then(count => {
        page.getHeadingCount().then(hCount => {
          expect(parseInt(hCount)).toBe(count);
        });
      });
    });
  });

  it('should display Editing form when Edit button clicked', () => {
    page.openEditAlertForm(0).then(() =>{
      expect(page.getEditingForm().isDisplayed()).toBe(true);
    });
  });

  it('should display Name, Frequency, Expires and updat button in the editing form', () => {
    page.openEditAlertForm(0).then(() =>{
      expect(page.getEditingComponent(0).isDisplayed()).toBe(true);
      expect(page.getEditingComponent(1).isDisplayed()).toBe(true);
      expect(page.getEditingComponent(2).isDisplayed()).toBe(true);
      expect(page.getEditingButton().isDisplayed()).toBe(true);
    });
  });

  it('should Edit the name of selected Alert', () => {
    let newName= 'Test Alert';
    page.openEditAlertForm(0).then(() =>{
      page.editNameOfAlert(newName);
      browser.sleep(1000);
    }).then(() => {
      expect(page.getAlertName(0).getText()).toBe(newName);
    });
  });

  it('should display correct Permalink', () => {
    let permalink;
    let query;
    page.getFriendlyQuery('Topic Alerts').getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
      query = text.replace(/<(?:.|\n)*?>/g, '');
    }).then(() => {
      page.getPermalinkButton(0).then(() =>{
        page.getPermalink().getAttribute('ng-reflect-value').then(link => {
          permalink = link.replace('http://localhost:9001', '');
        }).then(() => {
          browser.get(permalink);
          browser.sleep(4000);
          page.getFriendlyQuery('search result').getAttribute('ng-reflect-inner-h-t-m-l').then(text =>{
            text = text.replace(/<(?:.|\n)*?>/g, '');
            expect(text).toBe(query);
          });
        });
      });
    });
  });

  it('should open RSS Feed', () => {
    let searchId;
    page.getSearchId(0).then(id => {
      searchId = id;
      page.getRSSButton(0);
      browser.sleep(1000);
    }).then(() => {
      browser.getAllWindowHandles().then(function (handles) {
        let newWindowHandle = handles[1]; // this is the RSS window
        browser.switchTo().window(newWindowHandle).then(function () {
          browser.getCurrentUrl().then( url =>{
            expect(url).toContain(searchId);
          });
        });
        browser.driver.close();
        browser.switchTo().window(handles[0]);
      });
    });
  });

  it('should display login page for guest or organization', () => {
    page.removeIndividual();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/apa/idm/login.seam');
  });

  afterAll(() =>  {
    page.revertIdentity();
    browser.sleep(1000);
  });
});
