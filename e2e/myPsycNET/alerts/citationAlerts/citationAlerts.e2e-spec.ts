import { MyPsycNETCitationAlters  } from './citationAlerts.po';
import { browser } from 'protractor';

describe('MyPsycNET - Citation Alerts Page', function() {
  let page: MyPsycNETCitationAlters ;

  beforeEach(() => {
    page = new MyPsycNETCitationAlters ();
    page.navigateTo();
    page.openCitationAlertsComponent();
    browser.manage().timeouts().implicitlyWait(9000);
  });

  it('should open the correct component', () => {
    expect(browser.getCurrentUrl()).toContain('/alerts');
  });

  it('should select Citation Alerts tab', () => {
    expect(page.getSelectedTab().getAttribute('class')).toBe("active");
  });

  it('should have \'Manage Citation Alerts\' heading', () => {
    expect(page.getPageHeading()).toEqual('Manage Citation Alerts');
  });

  it('should add specific record as citated Alert from fullText page', () => {
    let selectedTitle = 'IQ at age 12 following a history of institutional care: Findings from the Bucharest Early Intervention Project';
    browser.get('/fulltext/2016-47857-001.html').then(() => {
      page.addToCitedAlerts();
    }).then(() =>{
      page.navigateTo();
      page.openCitationAlertsComponent();
    }).then(() =>{
      page.getCitationTitle().each((title,index) =>{
        title.getText().then((text) =>{
          if(text.indexOf(selectedTitle)> -1) {
            expect(page.getCitedCheckBox(index).getAttribute('ng-reflect-model')).toBe('true');
          }
        });
      });
    });
  });

  it('should add specific record as Corrected Alert from fullText page', () => {
    let selectedTitle = 'IQ at age 12 following a history of institutional care: Findings from the Bucharest Early Intervention Project';
    browser.get('/fulltext/2016-47857-001.html').then(() => {
      page.addToCorrectedAlerts();
    }).then(() =>{
      page.navigateTo();
      page.openCitationAlertsComponent();
    }).then(() =>{
      page.getCitationTitle().each((title,index) =>{
        title.getText().then((text) =>{
          if(text.indexOf(selectedTitle)> -1) {
            expect(page.getCorrectedCheckBox(index).getAttribute('ng-reflect-model')).toBe('true');
          }
        });
      });
    });
  });

  it('should add specific record as Comment Alert from fullText page', () => {
    let selectedTitle = 'IQ at age 12 following a history of institutional care: Findings from the Bucharest Early Intervention Project';
    browser.get('/fulltext/2016-47857-001.html').then(() => {
      page.addToCommentAlerts();
    }).then(() =>{
      page.navigateTo();
      page.openCitationAlertsComponent();
    }).then(() =>{
      page.getCitationTitle().each((title,index) =>{
        title.getText().then((text) =>{
          if(text.indexOf(selectedTitle)> -1) {
            expect(page.getCommentCheckBox(index).getAttribute('ng-reflect-model')).toBe('true');
          }
        });
      });
    });
  });

  it('should remove specific record from Comment, Cited and Correted Alerts', () => {
    let selectedTitle = 'IQ at age 12 following a history of institutional care: Findings from the Bucharest Early Intervention Project';
    let count;
     browser.sleep(2000);
     page.getHeadingCount().then(hCount =>{
        count = parseInt(hCount);
     }).then(() =>{
       page.getCitationTitle().each((title,index) =>{
        title.getText().then((text) =>{
          if(text.indexOf(selectedTitle)> -1) {
            page.getCommentCheckBox(index).click();
            page.getCorrectedCheckBox(index).click();
            page.getCitedCheckBox(index).click();
            count = count - 3;
            }
          });
        });
     }).then(() =>{
      browser.sleep(2000);
      page.getHeadingCount().then(hCount =>{
        expect(parseInt(hCount)).toBe(count);
      });
    });
  });

  it('should display correct count of citation Alerts', () => {
    page.getSelectedAlertsCount().then(count =>{
      page.getHeadingCount().then(hCount =>{
        expect(parseInt(hCount)).toBe(count);
      });
    });
  });

  it('should display login page for guest or organization', () => {
    page.removeIndividual();
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain('/apa/idm/login.seam');
  });

  afterAll(() =>  {
    page.revertIdentity();
    browser.sleep(1000);
  });
});
