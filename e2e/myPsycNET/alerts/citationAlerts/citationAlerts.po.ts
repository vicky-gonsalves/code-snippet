import { browser, element, by } from 'protractor';

export class MyPsycNETCitationAlters {

  navigateTo() {
    browser.get('/MyPsycNET/alerts');
    browser.sleep(1000);
  }

  openCitationAlertsComponent() {
    element(by.css('.mypntabscontainer ul>li:nth-child(2)')).click().then(() =>{
      element(by.css('mypsycnet-alerts ul>li:nth-child(2)')).click();
    });
  }

  getSelectedTab() {
    return element(by.css('mypsycnet-alerts ul>li:nth-child(2)'))
  }

  getPageHeading() {
    let heading = element.all(by.tagName('h1 > span')).get(0);
    return heading.getText();
  }

  addToCitedAlerts() {
    element(by.css('.alerts>a')).click().then(() =>{
      return element(by.css('.alerts>ul>li:nth-child(2)')).click();
    });
  }

  addToCorrectedAlerts() {
    element(by.css('.alerts>a')).click().then(() =>{
      return element(by.css('.alerts>ul>li:nth-child(3)')).click();
    });
  }

  addToCommentAlerts() {
    element(by.css('.alerts>a')).click().then(() =>{
      return element(by.css('.alerts>ul>li:nth-child(4)')).click();
    });
  }

  getCitationTitle() {
    return element.all(by.css('.record-list> div>div>div>div'));
  }

  getCitedCheckBox(index) {
    return element.all(by.css('.citation-action-cited >input')).get(index);
  }

  getCorrectedCheckBox(index) {
    return element.all(by.css('.citation-action-corrected >input')).get(index);
  }

  getCommentCheckBox(index) {
    return element.all(by.css('.citation-action-comments >input')).get(index);
  }

  getSelectedAlertsCount () {
    return element.all(by.css('.record-display input[ng-reflect-model = "true"]')).count();
  }

  getHeadingCount() {
    return element(by.css('.count')).getText();
  }

  removeIndividual() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('-1');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
    browser.get('/MyPsycNET/alerts');
  }

  revertIdentity() {
    browser.get('/admin');
    element(by.css('admin-tabs > .nav-tabs > li:nth-child(2) > a')).click();
    browser.sleep(1000);
    element(by.css('identitymanagement')).element(by.id('individualId')).clear();
    element(by.css('identitymanagement')).element(by.id('individualId')).sendKeys('12345');
    element(by.css('identitymanagement input[type=submit]')).click();
    browser.sleep(1000);
  }
}
