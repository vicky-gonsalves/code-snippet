# PsycNET 3.5

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.24.

## Development 
Development consists of a **Client** and **Server** side.  

For the **Client**, open a terminal and execute `npm run client:start`. 

For the **Server**, open a different terminal and execute `npm run server:start`.  Any time a change is made on the server-side, you will need to restart the server to bring in the new changes.

Once both the **Client** and **Server** are started, the navigate to `http://localhost:9000`.

## Building Code For Deployment
 
### Building for AWS DEV environment

When deploying to the AWS development servers, the code is automatically built/packaged when you use CodeDeploy.  You can see this in `scripts/install.sh`.


### Building for AWS QA environment

When deploying to the AWS test servers, you first need to build and package the code.  You may also need to merge code into the branch to be deployed into QA.  Here is an example of this process.

1.  Merge `build-3-lazy` into `staging-lazy` and commit the merge
2.  Run `npm run server:build` to build the server code
3.  Run `npm run client:build:test` to build the client code
4.  Commit the built code

### Building for AWS PROD environment

When deploying to the AWS test servers, you first need to build and package the code.  You may also need to merge code into the branch to be deployed into PROD.  The only difference in this process from QA above is Step 3, where we package the code for production, `npm run client:build:prod`. Here is an example of this process.

1.  Merge `build-3-lazy` into `staging-lazy` and commit the merge
2.  Run `npm run server:build` to build the server code
3.  Run `npm run client:build:prod` to build the client code
4.  Commit the built code


## Deploying

### Deploying to AWS DEV and QA

We use **CodeDeploy** to manage deployments. To deploy using **CodeDeploy**, go to [BitBucket](https://bitbucket.org/apapn/pn4angular2), select the branch that you want to deploy, click the `Deploy to AWS` link and choose your desired development group.

### Deploying to AWS Prod

We can deploy to DEV and QA, however, the network engineers have to deploy to production.  For them to deploy to production, they will need a deployment location, which is only assigned when you use CodeDeploy for deploying.  Here are the steps to prepare the code for deploying to production.

1.  Build the code as specified in the **Building for AWS PROD environment** above
2.  Go to [BitBucket](https://bitbucket.org/apapn/pn4angular2) and deploy the branch to the `psycnettest` group
3.  Click the deployment status indicator to open the current deployment progress
4.  Expand the **Deployment Details** section and copy the **Revision location** for the network engineers for deployment.  It will look similar to this: s3://awsdevpndeploy/bbaddon_{dfc7ee63-6c7c-4f01-9160-bf23c277ff44}7f644b34-a597-47a1-a23f-c4216f3c950e.zip
5.  After you have the **Revision location**, you will then need to make a QA deployment so that `awspntest.apa.org` works appropriately for testers. 



# The following is outdated and will be updated in the near future
## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to Github Pages

Run `ng github-pages:deploy` to deploy to Github Pages.

## Further help

To get more help on the `angular-cli` use `ng help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
