import {ProfileService} from "../../lib/personalization/profileService";

function markOpenUrls(req, res, next){

  if (req.result.results && req.result.results.result.doc) {
    ProfileService.markOpenUrls(req.result.results.result.doc, req.session.identity);
  } else if (req.result.response && req.result.response.result.doc) {
    ProfileService.markOpenUrls(req.result.response.result.doc, req.session.identity);
  }

  next();

}

exports.markOpenUrls = markOpenUrls;
