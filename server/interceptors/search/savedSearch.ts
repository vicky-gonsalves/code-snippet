var util = require('util');
var _= require("lodash");
import {AuroraSQL} from '../../lib/sql/awsSqlConnection';
import {Identity} from '../../lib/auth/identity/identity';
import {WddxService} from '../../lib/search/savedSearch/WddxService';
import {DateParser} from '../../lib/utility/dateParser';
import {SavedSearchTableSchema} from '../../lib/search/savedSearch/savedSearchTableSchema';
import {HighlightService} from '../../lib/utility/highlightService';
import {SavedSearch} from '../../lib/search/savedSearch/savedSearch';

function duplicateValidation (req, res, next) {
    var searchName = '';
    if (!SavedSearch.hasParam(req.body.params, 'searchId')) {
      if (SavedSearch.hasParam(req.body.params, 'searchName')) {
        searchName = req.body.params['searchName'];
      }
      else {
        searchName = HighlightService.removeHighlights(req.search.friendlyQuery);
      }
    }
    var saveSearch = new SavedSearchTableSchema(searchName, req.search, req.session.identity);
    saveSearch.WDDXSearchString = WddxService.queryToWddx(req.search);
    console.log('saveSearch'+saveSearch);
    var connection = AuroraSQL.getPNConnection();
    connection.connect();
    var existQuery = 'SELECT SEARCHID FROM SavedSearches WHERE SearchName = ? AND AsregID = ?';
    connection.query(existQuery, [saveSearch.SearchName, saveSearch.AsregID], function (err, result) {
      console.log("AuroraSQL service params: ", JSON.stringify(saveSearch.SearchName));
      console.log(result);
      if (result && result.length && result[0].hasOwnProperty('SEARCHID')) {
            connection.end();
            return res.send({isSaved: "false","EXIST":1, SearchId: result[0].SEARCHID});
          }
      else {
        next();
      }
    });
}

exports.duplicateValidation = duplicateValidation;
