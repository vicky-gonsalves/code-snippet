import {PermissionService} from '../../lib/auth/permissionService';
import {IdentityService} from "../../lib/auth/identity/identityService";
import {PsycNETQuery} from "../../lib/search/query/psycNETQuery";
import {IResponseParameters} from "../../lib/search/query/components/interfaces";
import {Search} from "../../lib/search/search";
import _ = require('lodash');
import {SearchDatabaseValidator} from "../../lib/search/searchDatabaseValidator";
var util = require('util');

function convertToSearchQuery(req, res, next){
  if (hasParam(req.body.params, 'searchId')) {
    next();
  } else {
    let id = req.body.params.id || null;
    let pnQuery = new PsycNETQuery(req.body.params.query);
    let responseParams: IResponseParameters = req.body.params.responseParameters;
    req.search = new Search(pnQuery, responseParams, id);

    // apply additional metadata
    req.search.metadata = req.body.params.metadata;

    // check if we are load testing
    req.search.metadata.bypassML = (req.get('bypassML') === 'true') ? true : false;

    // mark that it's a facet only search
    if (req.search.responseParameters.facet && !req.search.responseParameters.results) {
      req.search.type = "facet";
    }

    //console.log("Prepare: ");
    //console.log(util.inspect(req.search, false, null));

    // check if we need to exclude this action from the logs
    if(req.search.metadata.excludeFromLogs){
      req.logs.exclude = true;
      req.search.metadata.excludeFromLogs = false;
    }

    next();
  }
}

// validate databases being searched
function validateSearchDatabases(req, res, next){
  if(!req.hasOwnProperty('search')){
    return next();
  }

  // The My List page can show tabs for products that the user does not have access to.  Those records show the abbreviated record display
  if(req.search.query.source !== 'mylist'){
    req.search = SearchDatabaseValidator.validate(req.session.identity, req.search);
  }

  next();
}


// apply filter specific to identity
function applyProductFilterByIdentity(req, res, next){

  // If the user is accessing the record directly, such as /record/2016-22958-009, then don't apply a product filter.
  // When accessing directly, the user will see the abbreviated record display page.
  if(req.hasOwnProperty('search') && !req.search.metadata.directUID ){

    req.search.applyProductFilterByIdentity(req.session.identity);

    //console.log("Prepare: ");
    //console.log(util.inspect(req.search, false, null));
  }
  next();
}

function markAsRecordDisplaySearch(req, res, next){
  if(req.hasOwnProperty('search')){
    req.search.type = 'record';
    req.search.responseParameters.facet = false;
  }
  next();
}

function markAsCitedInSearch(req, res, next){
  if(req.hasOwnProperty('search')){
    req.search.type = 'citedIn';
    req.search.responseParameters.facet = false;
    req.search.queryParameters.fq = '';
  }
  next();
}

function applyAuthorFacetParams(req, res, next){
  if(req.hasOwnProperty('search')){
    let fq = '';
    let queryParams = req.search.queryParameters;
    let separator = queryParams.fq.length ? ' AND ' : '';

    if(queryParams.productFilter){
      fq = queryParams.fq + separator + queryParams.productFilter;
    }

    let q = queryParams.q;
    //console.log("\n q: "+ JSON.stringify( req.body.params));
    delete req.body.params;

    req.body.params = {
      "fq": fq,
      "q": q,
      "facets":true,
      "results":false,
      "fc":"Author"
    };

    //console.log("\n q: "+ JSON.stringify( req.body.params));
    //console.log("applyAuthorFacetParams: ");
  }
  next();
}

function hasParam(params, field) {
  return (params.hasOwnProperty(field) && params[field].length);
}

exports.applyProductFilterByIdentity = applyProductFilterByIdentity;
exports.convertToSearchQuery = convertToSearchQuery;
exports.markAsRecordDisplaySearch = markAsRecordDisplaySearch;
exports.markAsCitedInSearch = markAsCitedInSearch;
exports.validateSearchDatabases = validateSearchDatabases;
exports.applyAuthorFacetParams = applyAuthorFacetParams;
