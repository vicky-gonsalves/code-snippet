var util = require('util');
var _= require("lodash");
import {RecordAccess} from '../../lib/record/recordAccess';
import {PermissionService} from '../../lib/auth/permissionService';

function checkPTRecordAccess(req, res, next) {
  // Check for PT records access
  if (req.search.metadata.uid.indexOf('9999') === 0) {
    // Should prevent access of the following user categories (except after using the DOI link, where a limited record is displayed):
    // - Anonymous users,
    // - Individual users, and
    // - Non-entitled organization users.
    if (!req.search.metadata.fromDoi && !PermissionService.mayAccess(req.session.identity.entitlements, 'PT') ) {
      return res.status(403).json({forbidden: true});
    }
  }
  next();
}

function markRecordAccess(req, res, next){

  if(req.result.results.result.doc) {
    RecordAccess.markRecordAccess(req.result.results.result.doc, req.session.identity);
  }

  next();

}

function filterRecordFields(req, res, next){
  RecordAccess.filterRecordFields(req.result.results.result.doc);
  next();
}

exports.checkPTRecordAccess = checkPTRecordAccess;
exports.markRecordAccess = markRecordAccess;
exports.filterRecordFields = filterRecordFields;
