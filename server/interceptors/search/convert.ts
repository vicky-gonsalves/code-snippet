import {SearchService} from "../../lib/search/searchService";
import {Search} from "../../lib/search/search";
import {SearchResult} from "../../lib/search/searchResult";
var highlights = require('./highlightService');
var _ = require('lodash');
var config = require('../../config/settings');

// uses the current search criteria to get the UID of a record and then applies a UID filter on the original search
function getUID(req, res, next){

  // user is already filtering by a specific UID
  /*
  if(req.search.queryParameters.q.indexOf('UID:') >= 0 || req.search.queryParameters.fq.indexOf('UID:') >= 0){
    return next();
  }
  */

  //console.log('Metadata: ', req.search.metadata);
  if(req.search.metadata.uid.length){
    applyUIDFilterToSearch(req.search, req.search.metadata.uid);
    return next();
  }

  let uidSearch:Search = _.cloneDeep(req.search);
  uidSearch.type = "uidLookup";

  SearchService.execute(uidSearch, (err, response:SearchResult) => {
    if(err){
      return next(err);
    }

    if(response.results.result.doc.length){
      applyUIDFilterToSearch(req.search, response.results.result.doc[0].UID);
    }
    return next();
  })
}

function applyUIDFilterToSearch(search:Search, uid:string){
  let filterCategory = 'UID';
  uid = highlights.removeHighlights(uid);
  search.responseParameters.start = 0;
  search.responseParameters.rows = 1;
  search.queryParameters.sort = '';
  search.metadata.uid = uid;

  //console.log('METADATA UID: ', uid);
  // mark that it's a PsycTESTS record search.  If the UID ends in '-000', then we need to grab the parent with children, otherwise just the child record
  if(uid && uid.indexOf('9999') === 0) {
    search.type = (search.metadata.uid.indexOf('-000') > 0) ? 'ptRecord' : 'ptChildRecord';

    if(search.type === 'ptChildRecord'){
      filterCategory = 'ChildUID';
    }
  }

  search.queryParameters.fq = filterCategory + ":(" + uid + ")";

  return search;
}


function getRssLink(req, res, next) {
  let result = prepareRssLink(req);
  req.result = result;
  next();
}


function prepareRssLink(req) {
  let response = {};
  if (req.result) {
    let result = req.result;
    result.rssLink = 'http://' + config.web.hostname + (config.isDeveloping ? ':' + config.web.port : '') + '/search/feed/' + result.SearchId;
    response = result;
  }
  return response;
}

function reformatResponse(req, res, next) {
  let trendingList = [];
  if (req.result) {
    let response = JSON.parse(req.result);
    if (response.response && response.response.result && response.response.result.doc) {
      response.response.result.doc.forEach(pairCombo=> {
        if (pairCombo.str.length) {
          trendingList.push(pairCombo.str[0].value);
        }
      });
      req.result = trendingList;
    }
  }
  next();
}

exports.getUID = getUID;
exports.getRssLink = getRssLink;
exports.reformatResponse = reformatResponse;
