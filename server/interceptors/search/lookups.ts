/**
 * Creates filter (fq)
 */
function applyFilter(req, res, next) {
  let filter = getFilterString(req.body.params);
  req.body.params.fq = filter;
  next();
}

function getProductNameFacet(productName) {
  let productNameFacet = 'ProductNameFacet:';
  switch (productName) {
    case 'PI':
      productNameFacet += '"PsycINFO"';
      break;
    case 'PA':
      productNameFacet += '"PsycARTICLES"';
      break;
    case 'PB':
      productNameFacet += '"PsycBOOKS"';
      break;
    case 'PE':
      productNameFacet += '"PsycEXTRA"';
      break;
    case 'PT':
      productNameFacet += '"PsycTESTS"';
      break;
    case 'PC':
    case 'PQ':
      productNameFacet += '"PsycCRITIQUES"';
      break;
    case 'PH':
      productNameFacet += '"PsycTHERAPY"';
      break;
  }
  return productNameFacet;
}

/**
 * Creates filter string based on productCodes
 */
function getFilterString(params) {
  let filters = [];
  let filterStr = '';

  if (has(params, 'hasPiJournalTitle')) {
    filters.push('HasPIJournalTitle:' + params.hasPiJournalTitle);
    delete params.hasPiJournalTitle;
  }

  if (has(params, 'productCodes')) {
    let productNames = [];
    params.productCodes.forEach(pc=> {
      productNames.push(getProductNameFacet(pc));
    });
    filters.push(productNames.join(' OR '));
    delete params.productCodes;
  }

  if (filters.length) {
    filterStr = filters.join(' AND ');
  }

  return filterStr;
}

// helper function to determine if user has entered data for a given field
function has(params, field) {
  return (params.hasOwnProperty(field) && params[field].length);
}

exports.applyFilter = applyFilter;
