function removeHighlights(text: string): string {
  let regExp = /(<span class=""hilite"">|<span class='hilite'>|<\/span>)/gi;
  let result = '';

  if(text && typeof text === 'string') {
    result = text.replace(regExp, '');
  }
  console.log(result);
  return result;
}

exports.removeHighlights = removeHighlights;
