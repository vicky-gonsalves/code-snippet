let redisClient = require('../../lib/redis/redisClient');
let trendingCacheKey = 'cache:trending';
let trendingCacheExpiry = 28800;

function storeTrendingInCache(req, res, next) {

  /*
  Hard coded for Beta until the Trending Admin app is available


  req.result = [
    "Trauma",
    "Schizophrenia",
    "Music",
    "Identity",
    "Mindfulness"
  ];
   */

  if (req.result && req.result.length) {
    redisClient.setex(trendingCacheKey, trendingCacheExpiry, JSON.stringify(req.result));
  }
  next();
}

function getTrendingCache(req, res, next) {
  redisClient.get(trendingCacheKey, function (err, trendingData) {
    if (err) {
      return res.send({status: "error", message: "unable to get Trending from cache"});
    } else if (trendingData) {
      let parsedTendingData = JSON.parse(trendingData);
      if (parsedTendingData && parsedTendingData.length) {
        return res.status(200).json(parsedTendingData);
      }
    }
    next();
  });
}

exports.storeTrendingInCache = storeTrendingInCache;
exports.getTrendingCache = getTrendingCache;
