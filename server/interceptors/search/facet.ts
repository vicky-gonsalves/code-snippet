let _ = require('lodash');

function sortPublicationYearInFacet(req, res, next) {
    if (req.body.params.responseParameters.facet) {
        let facets = req.result.results.facets;
        let PublicationYearObj = facets.filter(function (obj) { return obj.name == 'PublicationYear'; });
        if (PublicationYearObj.length && PublicationYearObj[0].hasOwnProperty('values')) {
            PublicationYearObj[0].values.sort(sortItems("value", -1));
        }
    }
    return next();
}

function sortItems(value, direction) {
    return function (first, second) {
        let SortedResult = (parseInt(first[value]) < parseInt(second[value])) ? -1 : 1;
        return SortedResult * direction;
    }
}

function updateCitedRefFacets(req, res, next) {
    if(req.search.type === 'citedRefs' && req.result && req.result.results && req.result.results.facets && req.result.results.facets.length) {
        let facet = _.filter(req.result.results.facets, {name: 'PublicationType'});
        facet[0].values = _.reject(facet[0].values, {name: 'other'});
    }
    return next();
}

function updateAuthorFacet(req, res, next) {
  if(req.search && req.search.query && req.search.query.filter && req.search.query.filter.facets && req.search.query.filter.facets.length) {
    let facets = req.search.query.filter.facets;
    if(req.result) {
      req.result = JSON.parse(req.result);
      if(req.result.response && req.result.response.facet && req.result.response.facet.values && req.result.response.facet.values.length) {
        let results = req.result.response.facet.values;
        let authorFacet = facets.filter(obj =>  obj.field == 'Author');
        if(authorFacet) {
          let authors =[];
          authorFacet.forEach(facet => {
            let filtered = _.filter(results, obj => obj.name == facet.value);
            if(filtered) {
              let index = results.indexOf(filtered[0]);
              req.result.response.facet.values[index]['selected'] = true;
            }
          });
        }
      }
    }
  }
  next();
}

exports.sortPublicationYearInFacet = sortPublicationYearInFacet;
exports.updateCitedRefFacets = updateCitedRefFacets;
exports.updateAuthorFacet = updateAuthorFacet;
