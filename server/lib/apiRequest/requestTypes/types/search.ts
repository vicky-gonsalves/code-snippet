import _ = require('lodash');
import {BaseRequestType} from "./baseRequestType";
import config = require('../../../../config/settings');
import {SearchService} from '../../../search/searchService';
import {IdentityService} from "../../../auth/identity/identityService";
import async = require("async");
var util = require('util');

export class Search extends BaseRequestType {

  constructor() {
    super();
  }


  /*
  execute(req, res, next, cb) {

    req.search.responseParameters = _.defaultsDeep(req.search.responseParameters, req.apiRequest.defaultParams);

    SearchService.execute(req.search, (err, data) => {

      if (err) {
        return cb(err);
      }

      return cb(null, data);
    });

  }
  */


  execute(req, res, next, cb) {

    async.parallel([
        // Get the latest product counts if we need to
        function (callback) {
          let shouldRefresh = (req.search.type === 'facet' && req.search.query.tab !== 'all');
          //console.log('Should refresh tabs: ', shouldRefresh);

          if (shouldRefresh) {
            SearchService.getProductTabCounts(_.cloneDeep(req.search), req.session.identity, (err, result) => {
              callback(null, result);
            });
          } else {
            callback(null, {});
          }
        },

        // execute the actual search and get the record results
        function (callback) {

          req.search.responseParameters = _.defaultsDeep(req.search.responseParameters, req.apiRequest.defaultParams);

          SearchService.execute(req.search, (err, result) => {
            callback(null, result);
          });
        }
      ],

      // handle the response from both asynchronous calls
      function (err, asyncResults) {
        if (err) {
          return cb(err);
        }

        // merge tab counts into search request
        let facetResults = asyncResults[0];
        let searchResults = asyncResults[1];
        let facetCounts = (facetResults['results'] && facetResults['results']['facets']) ? facetResults['results']['facets'] : [];

        // replace facets from hitcount query into the search query so we only return one response
        for (let i = 0; i < facetCounts.length; i++) {
          for (let j = 0; j < searchResults['results']['facets'].length; j++) {
            if (searchResults['results']['facets'][j]['name'] === facetCounts[i]['name']) {
              searchResults['results']['facets'][j] = facetCounts[i];
              break;
            }
          }
        }

        return cb(null, searchResults);
      });

  }


  getOptions() {

    let result = {
      defaultParams:{
        // pagination controls
        start: 0,
        rows: 25,

        // desired fields returned (comma delimited list)
        fl: 'UID',

        // facets
        facet: false,
        fc: '', // comma delimited list of facets)

        results: false,

        // highlight
        hl: false,

        format: 'json'
      },

      // Define the list of fields that are returned if the user does not have access to the record
      noAccessFields: ['UID']
    };

    return result;
  }

}
