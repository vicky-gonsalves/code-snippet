import _ = require("lodash");

/**
 * Base request class that must be inherited by request type classes
 */
export class BaseRequestType {
  common:{};
  options:{};

  constructor() {
    
  }

  /**
   * Base 'execute' function that must be overriden in child class
   */
  execute(req, res, next, cb){ //TODO: what are the types for these params?
    console.log("You must override the 'execute' method within your Request Type.");
  }

  /**
   * Base 'getOptions' function that must be overriden in child class
   */
  getOptions() {
    console.log("You must override the 'getOptions' method within your Request Type.");
  }

  getDefaultOptions() {
    this.common = this.getCommonOptions();
    this.options = _.merge({}, this.common, this.getOptions());

    return this.options;
  }

  /**
   * Default configuration options that are applied to ALL request definitions
   * @returns {}
   */
  getCommonOptions() {
    return {

      // does the user have to be logged in to access this API
      restricted: true,

      // these are the default interceptors. The default interceptors do not do anything.
      interceptors:{
        before:   null,
        after:    null
      }
    }
  }
}
