import {BaseRequestType} from "./baseRequestType";

export class Command extends BaseRequestType {

  constructor() {
    super();
  }

  execute(req, res, next, cb){
    let bypassML = (req.get('bypassML') === 'true') ? true : false;

    if(bypassML){
      return cb(null, JSON.stringify({result:false}));
    }

    req.apiRequest.command(req, res, next, function(err, data){
      if (err) {
        return cb(err);
      }

      cb(null, data);
    });
  }

  getOptions() {
    return {
      command: null
    }
  }
}
