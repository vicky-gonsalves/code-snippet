import {HttpHandler} from "../../util/httpHandler";
import {BaseRequestType} from "./baseRequestType";
import {CacheService} from '../../../cache/cacheService';

var async = require('async');
var _ = require('lodash');
var qs = require('querystring');
var config = require('../../../../config/settings');
var cacheKeyPrefix = "cache:browse:";

var statsClient = require('../../../logs/statsClient');

export interface IWebServiceConnection {
  dataType: string,   // 'string' or 'binary'.  Used to determine which http call to use to get content
  hostname: string,
  port: any,
  path: string,
  method: string, // POST, PUT, GET, DELETE
  headers: any
}


export class WebService extends BaseRequestType {

  constructor() {
    super();
  }

  execute(req, res, next, callback) {


    let request = this.assembleServiceOptions(req.apiRequest.server, req.body.params);
    let stringParams = JSON.stringify(request.params);
    let serviceCall = "http://" + request.options.hostname + ":" + request.options.port + request.options.path;
    let bypassML = (req.get('bypassML') === 'true') ? true : false;
    let cachedData = null;
    let mlData = null;

    if(bypassML){
      //console.log('By passing ML webservice call for ', req.body.api);
      return callback(null, JSON.stringify({result:false}));
    }

    async.series({

      // check cache first
      checkCache: function(cb){

        if(!config.cache.browse || !req.apiRequest.cacheable){
          return cb();
        }

        WebService.getCachedData(serviceCall, (err, data) => {
          if(err){ return cb(err); }

          // save the data for the next call to inspect
          cachedData = JSON.parse(data);

          if(data){
            console.log('\nUsing cache for: ', serviceCall);

            statsClient.increment('browse.cachehit.count');
            // we could cache this again to start the timer all over, but for now we'll be safe for memory purposes
            //WebService.cacheResults(serviceCall, cachedData);
          }

          return cb();
        });

      },

      // get data from MarkLogic if there is no cache
      getDataFromML: function(cb){
        if(cachedData){
          return cb();
        }

        console.log("webservice: ", serviceCall);
        statsClient.increment('browse.api.count');

        let startTime = new Date().getTime();

       let handler = new HttpHandler(req.apiRequest.type);
       // set the options api name to the req.body.api;
       request.options.api = req.body.api;

       handler.execute(request.options, stringParams, function (err, data) {
          if (err) {
            return cb(err);
          }

          let endTime = new Date().getTime();
          let executionTime = endTime - startTime;

          mlData = data;
          statsClient.histogram('browse.api.time', executionTime);
          // save to cache if enabled
          if(config.cache.browse && req.apiRequest.cacheable){
            WebService.cacheResults(serviceCall, data);
          }

          return cb();
        });

      },

    },

    // when all the steps are finished, return the result to the user
    function(err) {
      if(err) {
        return callback(err);
      }

      let dataForUser = cachedData || mlData;

      // return the results
      callback(null,dataForUser);
    });

  }

  getOptions() {
    let connectionOptions: IWebServiceConnection = {
      dataType: "string",
      hostname: config.marklogic.hostname,
      port: config.marklogic.port,
      path: "",
      method: "GET",
      headers: {}
    };

    return {
      // webservice connection information
      server: connectionOptions,

      // key-value default parameters that get applied to a request.  If the key-value combination already exists
      // in the parameters being passed in from the front-end, it does NOT override them.
      defaultParams: {},

      // by defaul all web services are cached
      cacheable: true
    }
  }

  assembleServiceOptions(options, params) {
    let paramPattern = "";
    let queryStringSeperator = "?";


    for (let param in params) {
      if (params.hasOwnProperty(param)) {
        paramPattern = "{{" + param + "}}";

        // check to see if the parameter is found within the URL
        if (options.path.indexOf(paramPattern) !== -1) {

          // inject path with parameter value
          options.path = options.path.replace(new RegExp(paramPattern, 'g'), encodeURIComponent(params[param]));

          // remove parameter from arguments since it's being used in URL
          delete params[param];
        }
      }
    }


    // With the remaining params, check if this is a GET and create a query string based on parameters being passed in
    if (options.method === "GET") {

      if (options.path.indexOf("?") !== -1) {
        queryStringSeperator = "&";
      }

      //check for params and append if available
      if (_.keys(params).length) {
        options.path = options.path + queryStringSeperator + qs.stringify(params);
      }

      // reset params
      params = {};
    }

    return {
      options: options,
      params: params
    }
  }

  private static cacheResults(serviceUrl, data){
    CacheService.cacheResults(cacheKeyPrefix, serviceUrl, data);
  }

  private static getCachedData(serviceUrl, cb){
    CacheService.getCachedData(cacheKeyPrefix, serviceUrl, (err, cachedData) => {
      if(err){
        return cb(err);
      }

      return cb(null, cachedData);
    });
  }
}
