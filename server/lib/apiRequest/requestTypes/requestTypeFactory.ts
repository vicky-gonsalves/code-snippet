import {Command} from "./types/command";
import {WebService} from "./types/webservice";
import {BaseRequestType} from "./types/baseRequestType";
import {Search} from "./types/search";

/**
 * Gets the desired request type after instantiating the base request extended class
 * @param type
 * @returns {RequestType}
 */
export function getRequestType(type): BaseRequestType {
  var requestType;

  switch (type) {
    case 'command':
      requestType = new Command();
      break;
    case 'webservice':
      requestType = new WebService();
      break;
    case 'search':
      requestType = new Search();
      break;
    default:
  }

  return requestType;
}
