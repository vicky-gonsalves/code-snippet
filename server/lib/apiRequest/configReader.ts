import _ = require("lodash");
import recursive = require('recursive-readdir');
import requestTypeFactory = require('./requestTypes/requestTypeFactory');
import {BaseRequestType} from "./requestTypes/types/baseRequestType";

export class ConfigReader  {

  constructor() {

  }

  read(pathToDefinitions, appOverride, cb) {
    var apiRequests = {};
    var self = this;

    recursive(pathToDefinitions, function (err, files) {

      _.forEach(files, function (value) {
        _.merge(apiRequests, self.applyNamespace(value));
      });

      // apply default options to all APIs
      apiRequests = self.applyDefaultOptions(apiRequests, appOverride);

      cb(apiRequests);

    });
  }

  applyNamespace(filePath) {
    var fileName = this.getFileName(filePath);
    var fileAPIs = require(filePath);

    var mergedKeys = _.mapKeys(fileAPIs, function (value, key) {
      return fileName + '.' + key;
    });

    return mergedKeys;
  }

  getFileName(filePath) {
    var fileName = filePath.replace(/^.*[\\\/]/, '');
    fileName = fileName.split('.')[0];

    return fileName;
  }

  applyDefaultOptions(apis, appOverride){
    var requestType: BaseRequestType = null;

    _.forOwn(apis, function(value, key) {

      // apply default options for each given type
      requestType = requestTypeFactory.getRequestType(value.type);
      //apis[key] = _.merge({}, requestType.getDefaultOptions(), value);
      apis[key] = _.defaultsDeep(value, requestType.getDefaultOptions());

      // apply application option overrides
      if(appOverride.hasOwnProperty(value.type)){
        //apis[key] = _.merge({}, apis[key], appOverride[value.type]);
        apis[key] = _.defaultsDeep(value, appOverride[value.type]);
      }
    });

    return apis;
  }
}
