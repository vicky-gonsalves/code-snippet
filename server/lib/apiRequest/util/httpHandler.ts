import http = require("http");
import https = require("https");

var statsClient = require('../../logs/statsClient');
var config = require('../../../config/settings');

export class HttpHandler {
  requestTimeout:number = 30 * 1000;  // 30 seconds
  warnTime:number = 20 * 1000;   //20 seconds

  // execution method
  execute: Function;

  constructor(dataType: string = 'string') {
    this.execute = this.getExecutionMethod(dataType);
  }

  request(options, data, onResult){
    var timeout = this.requestTimeout;
    var startTime = new Date().getTime();
    var port:any = options.port === 443 ? https : http;
    var apiname = options.api || 'unknown';

    // console.log('apiname: ' + apiname);
    var req = port.request(options, function(res) {
      var result = "";

      res.on('data', function (chunk) {
        result += chunk;
      });

      // send the response back to the client
      res.on('end', function() {

        let endTime = new Date().getTime();
        let executionTime = endTime - startTime;

        if (executionTime > this.warnTime) {
          statsClient.event('Slow API response', 'API Response > 20 seconds: ' + apiname + ' ' + executionTime, {alert_type: 'warning', hostname: config.web.hostname});
        }

        onResult(null, result, res);
      });

      req.on('error', function(err) {
        onResult(err, null);
      });
    });

    // Handle errors which comes from the request (This will catch errors with the request connection itself, such as Socket hang up errors and reset connections)
    req.on('error', function(err) {
      statsClient.event('API Error', 'Unknown API error: ' + apiname, {alert_type: 'error', hostname: config.web.hostname});

      onResult(err, null);
    });

    // handle timeout errors
    req.setTimeout(timeout, function() {
      statsClient.event('API Timeout Error', 'API Response > 30 seconds: ' + apiname, {alert_type: 'error', hostname: config.web.hostname});
      onResult("ETIMEDOUT", null);
    });

    // post the data
    req.write(data);
    req.end();
  }

  binary(options, data, onResult){
    var timeout = this.requestTimeout;

    var req = http.request(options, function(res) {
      var data = [];

      res.on('data', function (chunk) {
        data.push(chunk);
      });

      // send the response back to the client
      res.on('end', function() {
        onResult(null, Buffer.concat(data), res);
      });

      req.on('error', function(err) {
        onResult(err, null);
      });
    });

    // Handle errors which comes from the request (This will catch errors with the request connection itself, such as Socket hang up errors and reset connections)
    req.on('error', function(err) {
      onResult(err, null);
    });

    // handle timeout errors
    req.setTimeout(timeout, function() {
      onResult("ETIMEDOUT", null);
    });

    // post the data
    req.write(data);
    req.end();
  }

  getExecutionMethod(dataType: string): Function {
    if (dataType === "binary") {
      return this.binary;
    } else {
      return this.request;
    }
  }
}
