import {Criteria} from '../query/components/criteria';
import {PsycNETQuery} from '../query/psycNETQuery';
import {SearchData} from "../../data/search/searchData";
import {Limit} from "../query/components/limit";
import * as moment from 'moment';
import _ = require("lodash");
import {CommandLineParser, ICommandlineResult} from "./commandline/commandLineParser";
import {ProductDefinitionService} from "../../product/productDefinitionService";
import {CommandLineFieldService} from "./commandline/commandLineFieldService";

export class FriendlyQuery {

  static getFriendlyQuery(query: PsycNETQuery): string {
    let result = '';
    let tokens = [];

    // use a copy of the query
    query = _.cloneDeep(query);

    // calculate the criteria string
    let criteriaString = FriendlyQuery.getCriteriaString(query);

    if (criteriaString.length) {
      tokens.push(criteriaString);
    }

    let filterString = FriendlyQuery.getFilterString(query);

    if (filterString.length) {
      tokens.push(filterString);
    }

    if (tokens.length) {
      result = tokens.join(FriendlyQuery.wrapInOperatorTag('AND'));
    }

    if(!result.length) {
       result =  FriendlyQuery.wrapInFieldTag('Any Field') + ': *';
    }

    return result;
  }

  static getCriteriaString(query: PsycNETQuery): string {
    let result = '';
    let tokens = [];
    let criteria: Criteria;
    let commandLineResult: ICommandlineResult = null;
    let token ={};
    let thisCriteriaValue ='';
    let queryField = '';

    // if the user hasn't entered any criteria, then stop processing
    if(!query.criteria.length || (query.criteria.length === 1 && query.criteria[0].value.length === 0)){
      return result;
    }

    for (let i = 0; i < query.criteria.length; i++) {
      criteria = query.criteria[i];

      // make sure the journal title is displayed instead of the journal code
      criteria = FriendlyQuery.getJournalTitle(criteria);

      if(criteria.operator.indexOf('NEAR')>=0){
        criteria.operator = 'AND';
      }

      if (i !== 0) {
        tokens.push(FriendlyQuery.wrapInOperatorTag(criteria.operator));
      }

      commandLineResult = CommandLineParser.parse(criteria.value, criteria.field);

      // if the command line parser was able to tokenize the user input, then add it
      if(commandLineResult.tokens.length){

        for (let j = 0; j < commandLineResult.tokens.length; j++) {
          token = commandLineResult.tokens[j];
          thisCriteriaValue = '';
          queryField = '';

          switch (token['type']) {
            case 'field':
              // get proper casing
              queryField = CommandLineFieldService.getField(token['value']);

              // map to proper term for display
              queryField = SearchData.getCriteriaLookupValue(queryField, query.databases);

              thisCriteriaValue = FriendlyQuery.wrapInFieldTag(queryField) + ':';
              break;
            case 'term':
              //queryField = CommandLineParser.getFieldForTerm(commandLineResult.tokens, j) || criteria.field;
              //queryField = SearchData.getCriteriaLookupValue(queryField, query.databases);

              queryField = CommandLineParser.getFieldForTerm(commandLineResult.tokens, j);

              // if the user explicitly entered a 'field', then it will be captured above in the 'field' case.  Otherwise we'll need to include the criteria.field value from the drop down
              if(queryField){
                thisCriteriaValue = FriendlyQuery.wrapInValueTag(token['value']);
              } else {
                // add criteria.field selection from dropdown
                queryField = SearchData.getCriteriaLookupValue(criteria.field, query.databases);
                tokens.push(FriendlyQuery.wrapInFieldTag(queryField) + ':');

                thisCriteriaValue = FriendlyQuery.wrapInValueTag(token['value']);
              }

              //thisCriteriaValue = thisCriteriaValue + FriendlyQuery.wrapInFieldTag(queryField) + ': ' + FriendlyQuery.wrapInValueTag(token['value']) + ' ';
              break;
            case 'op':
            case 'not-op':
            case 'near':
              thisCriteriaValue = FriendlyQuery.wrapInOperatorTag(token['value']);
              break;
            case 'parenstart':
            case 'parenend':
              thisCriteriaValue = token['value'];
              break;
            default:
          }

          if(thisCriteriaValue.length){
            tokens.push(thisCriteriaValue);
          }
        }

      } else {
        // if the user enters something strange, we may not be able to tokenize it correctly.  As a fail safe, just add the field-value pair
        tokens.push(FriendlyQuery.getFieldValueString(criteria, '|', true, query.databases))
      }

    }

    if (tokens.length) {
      result = tokens.join(' ');

      // remove spaces in front of parenthesis
      result = result.replace(/\( /g, '(');
      result = result.replace(/ \)/g, ')');
    }

    // trim whitespace
    result = result.trim();

    return result;
  }

  static getFilterString(query: PsycNETQuery): string {
    let result = '';
    let tokens = [];

    // process limits
    let limitString = FriendlyQuery.getLimitString(query);

    if (limitString.length) {
      tokens.push(limitString);
    }

    // process facets
    let facetString = FriendlyQuery.getFacetString(query);

    if (facetString.length) {
      tokens.push(facetString);
    }

    // process onlyShow
    let onlyShowString = FriendlyQuery.getOnlyShowWhereString(query);

    if (onlyShowString.length) {
      tokens.push(onlyShowString);
    }

    //process native options
    let nativeOptionsString = FriendlyQuery.getNativeOptionsString(query);

    if (nativeOptionsString.length) {
      tokens.push(nativeOptionsString);
    }

    // process dates
    let dateString = FriendlyQuery.getDateString(query);

    if (dateString.length) {
      tokens.push(dateString);
    }


    if (tokens.length) {
      result = tokens.join(FriendlyQuery.wrapInOperatorTag('AND'));
    }

    return result;
  }

  static getLimitString(query: PsycNETQuery): string {
    let result = '';
    let limits = query.limits;
    let aLimit: Limit;
    let limitString: string;
    let limitOperator: string;
    let limitField: string;
    let tokens = [];
    let isNegation: boolean = false;
    let operator = {
      or: FriendlyQuery.wrapInOperatorTag('OR'),
      and: FriendlyQuery.wrapInOperatorTag('AND'),
      not: FriendlyQuery.wrapInOperatorTag('NOT')
    };

    let isPTOnly = FriendlyQuery.isPTOnly(query);

    for (let i = 0; i < limits.length; i++) {
      aLimit = limits[i];

      // show the full description for classification codes
      aLimit = FriendlyQuery.getClassificationValues(aLimit);

      isNegation = (aLimit.operator === 'is not');
      limitOperator = isNegation ? operator.not : operator.and;

      if (i == 0 && limitOperator == operator.and) {
        limitOperator = '';
      }

      // remove any quotes from the values, particularly the AgeGroupFilt filters
      aLimit.values = aLimit.values.map(value => {
        return value.replace(/"/g, '');
      });


      limitField = FriendlyQuery.getLookupValue(aLimit.field);

      limitString = limitOperator + ' ' + FriendlyQuery.wrapInFieldTag(limitField) + ': ' + FriendlyQuery.wrapInValueTag(aLimit.values.join(operator.or));
      limitString = limitString.trim();

      if (limitString.length) {
        tokens.push(limitString);
      }

    }

    if (tokens.length) {
      result = tokens.join(' ');
      result = result.trim();
    }

    // if the user is searching PsycTESTS, then change the Classification field
    if (isPTOnly) {
      result = result.replace(/PsycINFO Classification/g, 'PsycTESTS Classification');
    }

    return result;
  }

  static getClassificationValues(limit:Limit){

    if(limit.field === 'Classification' || limit.field === 'PTClassificationCode'){
      for(let i=0; i<limit.values.length; i++){
        if( SearchData.classificationCodeLookup.hasOwnProperty(limit.values[i]) ){
          limit.values[i] = SearchData.classificationCodeLookup[limit.values[i]];
        }
      }
    }

    return limit;
  }

  static getJournalTitle(criteria:Criteria){

    if(criteria.field === 'PAJournalCode'){
      let journals = ProductDefinitionService.getProductsByPNCodes([criteria.value]);

      if(journals.length === 1){
        criteria.field = 'Journal Title';
        criteria.value = journals[0].label;
      }
    }

    return criteria;
  }

  static isPTOnly(query: PsycNETQuery){
    return (query.tab === "PT" || (query.databases.indexOf('PT') === 0 && query.databases.length === 1));
  }

  static getFacetString(query: PsycNETQuery): string {
    let result = '';
    let facets = query.filter.facets;
    let tokens = [];
    let field = '';

    // some of these mappings clash with the lookup data, so we are going to define them here
    let lookup = {
      'PublicationType': ' Publication Type',
      'IndexTerms': 'Index Term',
      'PublicationYear': 'Year',
      'AgeGroup': 'Age Group',
      'PopulationGroup': 'Population Group',
      'Instrumentation': 'Tests and Measures',
      'Classification': 'PsycINFO Classification',
      'LanguageFacet': 'Language'
    };

    for (let i = 0; i < facets.length; i++) {
      field = lookup.hasOwnProperty(facets[i].field) ? lookup[facets[i].field] : facets[i].field;
      field = FriendlyQuery.wrapInFieldTag(field);

      tokens.push(field + ': ' + FriendlyQuery.wrapInValueTag(facets[i].value));
    }

    if (tokens.length) {
      result = tokens.join(FriendlyQuery.wrapInOperatorTag('AND'));
    }

    return result;
  }

  static getOnlyShowWhereString(query: PsycNETQuery): string {
    let result = '';
    let onlyShow = query.filter.onlyShow;
    let tokens = [];

    // iterate over the properties of each category
    for (let prop in onlyShow) {
      if (onlyShow.hasOwnProperty(prop) && onlyShow[prop]) {
        tokens.push(FriendlyQuery.wrapInFieldTag(FriendlyQuery.getLookupValue(prop)));
      }
    }

    if (tokens.length) {
      result = tokens.join(FriendlyQuery.wrapInOperatorTag('AND'));
    }

    return result;
  }

  static getDateString(query: PsycNETQuery): string {
    let result = '';
    let dateRange = query.filter.dateRange;
    let d1 = '';
    let d2 = '';

    let field = FriendlyQuery.getLookupValue(dateRange.field);
    field = FriendlyQuery.wrapInFieldTag(field);

    // if there is no date filter, return the empty string
    if (dateRange.begin === 'eternity' && dateRange.end === 'eternity') {
      return result;
    }

    if (dateRange.field === 'releaseDate') {
      let from = moment(dateRange.begin, 'YYYYMMDD');
      let to = moment(dateRange.end, 'YYYYMMDD');
      let diff = to.diff(from, 'days');

      result = field + ': ' + FriendlyQuery.wrapInValueTag('last ' + diff + ' days');

    } else {
      d1 = (dateRange.begin === 'eternity') ? '0' : dateRange.begin;
      d2 = (dateRange.end === 'eternity') ? moment().format('YYYY') : dateRange.end;

      result = field + ': ' + FriendlyQuery.wrapInValueTag(d1);

      if (d1 !== d2) {
        result = result + FriendlyQuery.wrapInOperatorTag('To') + FriendlyQuery.wrapInValueTag(d2);
      }
    }

    return result;
  }

  static getNativeOptionsString(query: PsycNETQuery): string {
    let result = '';
    let pb = query.nativeOptions.pb;
    let tokens = [];
    let categoryTokens = [];
    let categoryLabel = '';
    let categoryValue = '';
    let categoryString = '';

    // iterate over each major category
    for (let category in pb) {
      if (pb.hasOwnProperty(category)) {
        categoryLabel = FriendlyQuery.wrapInFieldTag(FriendlyQuery.getLookupValue(category));
        categoryTokens = [];

        // iterate over the properties of each category
        for (let prop in pb[category]) {
          if (pb[category].hasOwnProperty(prop) && pb[category][prop]) {
            categoryValue = FriendlyQuery.getLookupValue(prop);
            categoryTokens.push(categoryValue);
          }
        }

        // add all category selections
        if (categoryTokens.length) {
          categoryString = categoryLabel + ': ' + FriendlyQuery.wrapInValueTag(categoryTokens.join(FriendlyQuery.wrapInOperatorTag('OR')));
          tokens.push(categoryString);
        }

      }
    }

    // combine all native options
    if (tokens.length) {
      result = tokens.join(FriendlyQuery.wrapInOperatorTag('AND'));
    }

    return result;
  }


  static getLookupValue(term) {
    return SearchData.fieldLookup.hasOwnProperty(term) ? SearchData.fieldLookup[term] : term;
  }

  static getFieldValueString(criteria: Criteria, delimiter: string = ',', isNegation: boolean = false, selectedDBList: string[] = []) {
    let result = '';
    let negateOperator = FriendlyQuery.wrapInOperatorTag('NOT');
    let negateString = isNegation ? negateOperator : '';
    let fieldValue = SearchData.getCriteriaLookupValue(criteria.field, selectedDBList);
    let arrValues = criteria.value.split(delimiter);
    let tokens = [];
    let pair = '';

    // boldface the field
    fieldValue = FriendlyQuery.wrapInFieldTag(fieldValue);

    for (let i = 0; i < arrValues.length; i++) {

      if (i > 0 && criteria.operator === "NOT") {
        criteria.operator = "AND";
        negateString = negateOperator;
      }
      if (criteria.field == 'AnyField' && arrValues[i].indexOf(':(') >= 0) {
        pair = (arrValues[i]);
      }
      else {
        pair = fieldValue + ': ' + FriendlyQuery.wrapInValueTag(arrValues[i]);
      }
      tokens.push(pair);
    }

    result = tokens.join(FriendlyQuery.wrapInOperatorTag(criteria.operator));

    return result;
  }

  // operator tags are italic
  static wrapInOperatorTag(term) {
    return ' <span class="fq-op">' + term + '</span> ';
  }

  // field tags are bold
  static wrapInFieldTag(term) {
    return '<span class="fq-field">' + term + '</span>';
  }

  // field tags are bold
  static wrapInValueTag(term) {
    return '<span class="fq-value">' + term + '</span>';
  }
}
