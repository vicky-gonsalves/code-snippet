
const assert = require("assert");

import {PsycNETQuery} from "../query/psycNETQuery";
import {Criteria} from "../query/components/criteria";
import {FriendlyQuery} from "./friendlyQuery";
import {HighlightService} from "../../utility/highlightService";


let testCases = {
  criteriaOnly: [
    {
      criteria: [
        {
          field: 'Title',
          op: 'OR',
          value: 'red or blue or green or pink'
        }
      ],
      correct: 'Title: red OR Title: blue OR Title: green OR Title: pink'
    },
    {
      criteria: [
        {
          field: 'Title',
          op: 'OR',
          value: 'this is a test'
        }
      ],
      correct: 'Title: this is a test'
    },
    {
      criteria: [
        {
          field: 'Title',
          op: 'OR',
          value: 'this is a test'
        },
        {
          field: 'Title',
          op: 'AND',
          value: 'red or blue or green or pink'
        }
      ],
      correct: 'Title: this is a test AND Title: red OR Title: blue OR Title: green OR Title: pink'
    },
    {
      criteria: [
        {
          field: 'Title',
          op: 'OR',
          value: 'this is a test'
        },
        {
          field: 'Title',
          op: 'NOT',
          value: 'red or blue or green or pink'
        }
      ],
      correct: 'Title: this is a test NOT Title: red OR Title: blue OR Title: green OR Title: pink'
    },

    {
      criteria: [
        {
          field: 'Abstract',
          op: 'OR',
          value: 'therapy'
        },
        {
          field: 'AnyField',
          op: 'AND',
          value: 'sleep'
        }
      ],
      correct: 'Abstract: therapy AND Any Field: sleep'
    },

    {
      criteria: [
        {
          field: 'AnyField',
          op: 'OR',
          value: 'ti=(black sheep or white sheep)'
        }
      ],
      correct: 'ti: (black sheep OR white sheep)'
    },
    {
      criteria: [
        {
          field: 'AnyField',
          op: 'OR',
          value: 'ti:((black sheep OR white sheep) NOT baby)'
        }
      ],
      correct: 'ti: ((black sheep OR white sheep) NOT baby)'
    },

    // does the friendly query field get reformatted to proper case
    {
      criteria: [
        {
          field: 'AnyField',
          op: 'OR',
          value: 'Ti=(black sheep or white sheep)'
        }
      ],
      correct: 'ti: (black sheep OR white sheep)'
    }

  ]
};

function getQueryCriteria(criteria){
  let pnQuery = new PsycNETQuery();
  criteria.forEach(function (criteria) {
    pnQuery.criteria.push( new Criteria(criteria.field, criteria.op, criteria.value, 'user') );

  });
  return pnQuery.criteria;
}

function cleanupStringForComparison(friendlyQuery){
  friendlyQuery = HighlightService.removeHighlights(friendlyQuery);

  // remove brackets which are used for localization
  friendlyQuery = friendlyQuery.replace(/(\[|\])+/g,'');

  // remove multiple spaces
  friendlyQuery = friendlyQuery.replace(/ +(?= )/g,'');
  return friendlyQuery;
}

describe('Friendly Query parser', function() {
  describe('Basic queries', function () {
    testCases.criteriaOnly.forEach(function (test) {
      let pnQuery = new PsycNETQuery();
      pnQuery.criteria = getQueryCriteria(test.criteria);

      it(test.correct, function () {
        let friendlyQuery = FriendlyQuery.getFriendlyQuery(pnQuery);

        // remove markup for comparison reasons
        friendlyQuery = cleanupStringForComparison(friendlyQuery);

        assert.equal(friendlyQuery, test.correct);
      })
    });
  });

});
