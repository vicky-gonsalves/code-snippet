var Lexer = require("lex");

import {CommandLineFieldService} from './commandLineFieldService';


export interface ICommandlineResult {

  // this is more of a friendly query display
  query: string;

  // applies query formatting, such as 'NOT' to 'AND -' conversion
  queryFormatted: string;

  // holds the tokenization of fields, terms, and operators
  tokens: any[]
}


/**
 * Uses the lexer library to tokenize the input string and formulate a command line query
 * @param str - the text to parse for a command line
 * @param defaultFieldName - the default value for the field to use
 * @returns {{query: string, isCommandLine: boolean}}
 */
export class CommandLineParser {

  private static fieldListRegExp = null;

  public static parse(str, defaultFieldName?): ICommandlineResult {
    let lexer = new Lexer;
    let tokens = [];
    let result: ICommandlineResult = {
      query: '',
      queryFormatted: '',
      tokens:[]
    };


    //build a regular expression for field identification.  Matches things such as "ab:" or "ab   :" or with equal signs
    if(!CommandLineParser.fieldListRegExp){
      let fieldList = CommandLineFieldService.fields.join('|');
      CommandLineParser.fieldListRegExp = new RegExp('\s*('+fieldList+'| )+\s*(:|=)( )*' ,'gi');
    }

    //console.log('CommandLineParser.fieldListRegExp: ', CommandLineParser.fieldListRegExp);

    // if no defaultFieldName is provided, then use AnyField as the value
    defaultFieldName = defaultFieldName || 'AnyField';

    // Identify the terms with double quotes
    lexer.addRule(/\s*"(.*?)"/g, function (value) {
      tokens.push({type: 'term', value: value.trim()});
    });

    // Identify the terms with single quotes
    lexer.addRule(/\s*'(.*?)'/g, function (value) {
      tokens.push({type: 'term', value: value.trim()});
    });

    // Identify NEAR/3 which is case-insensitive and requires a number
    lexer.addRule(/\s* NEAR\/[0-9]+ \s*/gi, function (value) {
      let nearOp = value.toUpperCase();
      tokens.push({type: 'near', value: nearOp.trim()});
    });

    // Identify the field name before colon
   // lexer.addRule(/\s*\w+\s*:\s*/g, function (value) {
    //  let field = value.replace(':', '').trim();
    //  tokens.push({type: 'field', value: field});
    //});

    lexer.addRule(CommandLineParser.fieldListRegExp, function (value) {
      let field = value.replace(/(:|=)/g, '');
      field = field.trim();
      tokens.push({type: 'field', value: field});
    });


    // Identify the NOT operator (it must be in all caps)
    lexer.addRule(/\s* NOT \s*/g, function (value) {
      let op = value.toUpperCase();
      tokens.push({type: 'not-op', value: op.trim()});
    });

    // Identify the AND/OR operators
    lexer.addRule(/\s*( OR | AND )\s*/gi, function (value) {
      let op = value.toUpperCase();
      tokens.push({type: 'op', value: op.trim()});
    });

    // Identify the opening parenthesis
    lexer.addRule(/\(\s*/g, function (value) {
      tokens.push({type: 'parenstart', value: value.trim()});
    });

    // Identify the closing parenthesis
    lexer.addRule(/\s*\)/g, function (value) {
      tokens.push({type: 'parenend', value: value.trim()});
    });


    // Identify the term after the colon with support for apostrophes and colons
    lexer.addRule(/\s*:*(\w|'|:|;|%|\^|&|@|,|\.|\-|!|#|\$|\*|\+|=|~|`|_|<|>|\?|\/|\\|\[|\])+\*?/g, function (value) {
      tokens.push({type: 'term', value: value.trim()});
    });

    //trim outside whitespace before parsing
    str = str.trim();

    try {

      // set the string to parse and process them into tokens
      lexer.setInput(str);
      lexer.lex();

      //console.log(str);
      //console.log(tokens);

      tokens = CommandLineParser.mergeImmediateTerms(tokens);

      //console.log(tokens);
      result.query = CommandLineParser.convertToCommandLine(tokens, defaultFieldName);
      result.queryFormatted = CommandLineParser.formatQuery(result.query);
      result.tokens = tokens;
    }
    catch(err) {
      // do nothing and just return the initial result
      //console.log('Could not parse criteria input: ', str);
    }

    //console.log(result);
    //console.log(tokens);

    return result;
  }

  // concatenates consecutive 'term', 'near', and 'not-op'
  public static mergeImmediateTerms(tokens) {
    let previousToken = {};
    let nextToken = {};
    let thisToken = {};
    for (let i = 0; i < tokens.length; i++) {
      previousToken = tokens[i-1];
      nextToken = (i < tokens.length-1) ? tokens[i+1] : null;
      thisToken = tokens[i];

      if(i > 0 && (
                  (previousToken['type'] === 'term' && thisToken['type'] === 'term')
                  || ( previousToken['type'] === 'term' && thisToken['type'] === 'near')
                  || ( nextToken && previousToken['type'] === 'term' && nextToken['type'] === 'term' && thisToken['type'] === 'not-op')
                )){

        tokens[i-1].value = previousToken['value']  + ' ' + thisToken['value'];
        tokens.splice(i, 1);
        i=0;
      }

    }
    return tokens;

  }

  public static mergeNotOperatorTerms(tokens) {
    let previousToken = {};
    let thisToken = {};
    for (let i = 0; i < tokens.length; i++) {
      previousToken = tokens[i-1];
      thisToken = tokens[i];

      if(i > 0 && ( (previousToken['type'] === 'term' && thisToken['type'] === 'term') || ( previousToken['type'] === 'term' && thisToken['type'] === 'near') )  ){
        tokens[i-1].value = previousToken['value']  + ' ' + thisToken['value'];
        tokens.splice(i, 1);
        i=0;
      }

    }
    return tokens;

  }


  /**
   * Takes the tokens produced from the lexer and builds the command line query string
   * @param tokens
   * @param defaultField
   * @returns {string}
   */
  public static convertToCommandLine(tokens, defaultField) {
    let queryField = "";
    let token = {};
    let mergedTokens = [];
    let result = "";

    for (let i = 0; i < tokens.length; i++) {
      token = tokens[i];

      switch (token['type']) {
        case 'term':
          queryField = CommandLineParser.getFieldForTerm(tokens, i) || defaultField;
          mergedTokens.push(CommandLineParser.wrapFieldValueInParenthesis(queryField, token['value']));
          break;

        case 'op':
        case 'not-op':
        case 'near':
        case 'parenstart':
        case 'parenend':
          mergedTokens.push(token['value']);
          break;
        default:
        // do nothing.  You'll notice that we do not look for the 'field' either here because that is calculated within the 'term' case
      }
    }

    // join the token array together in a space delimited string, then remove the spaces around the parenthesis
    if (mergedTokens.length) {
      result = mergedTokens.join(" ");

      // remove spaces in front of parenthesis
      result = result.replace(/\( /g, '(');
      result = result.replace(/ \)/g, ')');

      // wrap in parenthesis if not already done and if there is no field associated with the text
      if(result.length > 0 && result.charAt(0) !== '(' && !CommandLineParser.hasOneField(tokens)){
        result = '(' + result + ')';
      }
    }

    return result;
  }

  /**
   * Helper method to formulate the field-value pair
   * @param field
   * @param value
   * @returns {string}
   */
  public static wrapFieldValueInParenthesis(field, value) {
    let criteria = field + ':(' + value + ')';
    return criteria
  }

  /**
   * Walks upwards through the token array to identify which field ancestor the term value should inherit its field value from
   * @param tokens
   * @param position
   * @returns {*}
   */
  public static getFieldForTerm(tokens, position) {
    let field = null;
    let withinParen = false;
    let startPos = position - 1;
    let parenBalance = 0;

    for (let i = position - 1; i >= 0; i--) {

      // if the previous token is a field use it OR use the first field before the opening parenthesis
      if ((tokens[i].type === 'field' && i === startPos) || (withinParen && tokens[i].type === 'field' && parenBalance >= 1)) {
        field = tokens[i].value;
        break;
      }

      if (tokens[i].type === 'parenstart') {
        withinParen = true;
        parenBalance++;
      }

      if (tokens[i].type === 'parenend') {
        parenBalance--;
      }

      if (tokens[i].type === 'term') {
        withinParen = false;
      }
    }

    // if a field is found, then get the correct casing
    if (field) {
      field = CommandLineFieldService.getField(field);
    }

    return field;
  }

  // applies specific formatting for ML query
  private static formatQuery(query) {
    let result = query;

    result = result.replace(/ NOT /g, ' AND -');

    return result;

  }

  private static hasOneField(tokens){
    for(let i=0; i<tokens.length; i++){
      if(tokens[i].type === 'field'){
        return true;
      }
    }

    return false;
  }
}
