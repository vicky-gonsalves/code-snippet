
export class CommandLineFieldService {
// supported command line fields

  public static fields = [
    'AnyField',
    'ab',
    'abstract',
    'aff',
    'ag',
    'au',
    'author',
    'aur',
    'aux',
    'bt',
    'booktitle',
    'bty',
    'booktype',
    'cc',
    'class',
    'conf',
    'conference',
    'co',
    'cot',
    'CopyrightHolder',
    'CopyrightStatement',
    'CopyrightYear',
    'dt',
    'doc',
    'doi',
    'ft',
    'it',
    'indexterm',
    'isbn',
    'issn',
    'issue',
    'IssuePublicationDate',
    'jt',
    'journaltitle',
    'kw',
    'keyword',
    'KEYWORDS',
    'lang',
    'loc',
    'me',
    'methodology',
    'pg',
    'pop',
    'PublicationDate',
    'PublicationHistory',
    'PublicationStatus',
    'py',
    'pubyr',
    'pub',
    'publisher',
    'rd',
    'rev',
    'Subject',
    'tm',
    'ti',
    'title',
    'tr',
    'yrrev',
    'uid',

    // add the search within journal filter on journal TOC page
    'PAJournalCode',

    // add facet fields for combining recent searches
    'AuthorFilt',
    'JournalTitleFilt',
    'IndexTermsFilt',
    'PopulationGroupFilt',
    'PublicationType',
    'PublicationYear',
    'MethodologyFilt',
    'AffiliationFilt',
    'ConstructFilt',
    'AgeGroupFilt',
    'SerialTitleFilt',
    'ClassificationFilt',
    'InstrumentationFilt'

  ];


  /**
   * If there is an operator, NEAR term, or a command line field, then it is a command line query
   * @param tokens
   * @returns {boolean}
   */


  public static isCommandLineQuery(tokens) {
    let result = false;
    let token = {};

    for (let i = 0; i < tokens.length; i++) {
      token = tokens[i];

      if (token['type'] === 'op' || token['type'] === 'near') {
        result = true;
        break;
      }

      if (token['type'] === 'field' && CommandLineFieldService.hasField(token['value'])) {
        result = true;
        break;
      }
    }

    return result;
  }

  /**
   * Gets the field name from the list to preserve casing
   * @param fieldName
   * @returns {string}
   */


  public static getField(fieldName) {
    let lcField = '';
    let lcQueryField = fieldName.toLowerCase();
    let result = fieldName;

    for (let i = 0; i < CommandLineFieldService.fields.length; i++) {
      lcField = CommandLineFieldService.fields[i].toLowerCase();

      if (lcField === lcQueryField) {
        result = CommandLineFieldService.fields[i];
        break;
      }

    }
    return result;
  }

  /**
   * Determines if a field name exists within the list
   * @param fieldName
   * @returns {boolean}
   */


  public static hasField(fieldName) {
    let lcField = '';
    let lcQueryField = fieldName.toLowerCase();
    let result = false;

    for (let i = 0; i < CommandLineFieldService.fields.length; i++) {
      lcField = CommandLineFieldService.fields[i].toLowerCase();

      if (lcField === lcQueryField) {
        result = true;
        break;
      }

    }
    return result;
  }
}



