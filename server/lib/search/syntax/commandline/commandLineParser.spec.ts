const assert = require("assert");

import {CommandLineParser as parser} from "./commandLineParser";

const testCases = {
  basic: [
    {
      command: 'sleep disorder',
      correct: '(AnyField:(sleep disorder))'
    },
    {
      command: 'ab: a',
      correct: 'ab:(a)'
    },
    {
      command: 'booktitle:a',
      correct: 'booktitle:(a)'
    },
    {
      command: 'AnyField:intellectual AND PAJournalCode:ort',
      correct: 'AnyField:(intellectual) AND PAJournalCode:(ort)'
    }
  ],

  assignment: [
    {
      command: 'ab: a',
      correct: 'ab:(a)'
    },
    {
      command: 'ab= a',
      correct: 'ab:(a)'
    }
  ],

  fieldCase: [
    {
      command: 'AB: a',
      correct: 'ab:(a)'
    },
    {
      command: 'BookTitle:a',
      correct: 'booktitle:(a)'
    },
    {
      command: 'copyrightholder:a',
      correct: 'CopyrightHolder:(a)'
    },
    {
      command: 'ISSUEPublicationDate:a',
      correct: 'IssuePublicationDate:(a)'
    },
    {
      command: 'keywords:a',
      correct: 'KEYWORDS:(a)'
    }
  ],

  booleanOp: [
    {
      command: 'a OR b',
      correct: '(AnyField:(a) OR AnyField:(b))'
    },
    {
      command: 'a Or b',
      correct: '(AnyField:(a) OR AnyField:(b))'
    },
    {
      command: 'a oR b',
      correct: '(AnyField:(a) OR AnyField:(b))'
    },
    {
      command: 'a or b',
      correct: '(AnyField:(a) OR AnyField:(b))'
    },
    {
      command: 'a AND b',
      correct: '(AnyField:(a) AND AnyField:(b))'
    },
    {
      command: 'a AnD b',
      correct: '(AnyField:(a) AND AnyField:(b))'
    }
  ],

  notOperator: [
    {
      command: 'ab: a not b',
      correct: 'ab:(a not b)'
    },
    {
      command: 'ab: a Not b',
      correct: 'ab:(a Not b)'
    },
    {
      command: 'ab: a NoT b',
      correct: 'ab:(a NoT b)'
    },
    {
      command: 'ab: a NOT b',
      correct: 'ab:(a NOT b)'
    },
    {
      command: 'ab: a NOT (ti:b OR c)',
      correct: 'ab:(a) NOT (ti:(b) OR AnyField:(c))'
    },

    {
      command: 'depression NOT psychotic',
      correct: '(AnyField:(depression NOT psychotic))'
    }


  ],

  nearOperator: [

    {
      command: 'ab: a near b',
      correct: 'ab:(a near b)'
    },
    {
      command: 'ab: a Near b',
      correct: 'ab:(a Near b)'
    },
    {
      command: 'ab: a Near/2 b',
      correct: 'ab:(a NEAR/2 b)'
    },

    {
      command: 'ab: a NEAR/2 b',
      correct: 'ab:(a NEAR/2 b)'
    },

    {
      command: 'a NEAR/2 b',
      correct: '(AnyField:(a NEAR/2 b))'
    }
  ],

  parenthesis: [
    {
      command: 'aff:( a or b )',
      correct: '(aff:(a) OR aff:(b))'
    },
    {
      command: 'aff:( a or b )',
      correct: '(aff:(a) OR aff:(b))'
    },
    {
      command: 'performance appraisal OR program OR incompetence',
      correct: '(AnyField:(performance appraisal) OR AnyField:(program) OR AnyField:(incompetence))'
    },
    {
      command: 'ti=( a or b AND c) or ab:( d and f)',
      correct: '(ti:(a) OR ti:(b) AND ti:(c)) OR (ab:(d) AND ab:(f))'
    },
    {
      command: 'ab=( a or (b AND c))',
      correct: '(ab:(a) OR (ab:(b) AND ab:(c)))'
    },
    {
      command: 'ab=( a or (ti:b AND c))',
      correct: '(ab:(a) OR (ti:(b) AND ab:(c)))'
    },
    {
      command: 'ab=( a or title:(b AND c))',
      correct: '(ab:(a) OR (title:(b) AND title:(c)))'
    },
    {
      command: 'ab:(a or b NEAR/5 c)',
      correct: '(ab:(a) OR ab:(b NEAR/5 c))'
    },
    {
      command: 'ab=( a or ti:(b AND c and (d and e and (f OR doi=g) and h)) NOT i)',
      correct: '(ab:(a) OR (ti:(b) AND ti:(c) AND (ti:(d) AND ti:(e) AND (ti:(f) OR doi:(g)) AND ti:(h))) NOT ab:(i))'
    },
  ],

  fieldInheritance: [
    {
      command: 'ab:a or b',
      correct: 'ab:(a) OR title:(b)'
    },
    {
      command: 'ab = a or b',
      correct: 'ab:(a) OR title:(b)'
    },
    {
      command: 'a or ab:b and c',
      correct: 'title:(a) OR ab:(b) AND title:(c)'
    },
    {
      command: 'ab:(a or b) NEAR/5 c',
      correct: '(ab:(a) OR ab:(b)) NEAR/5 title:(c)'
    },
    {
      command: 'ti=( depression or anger AND psychosis)',
      correct: '(ti:(depression) OR ti:(anger) AND ti:(psychosis))'
    },
    {
      command: 'ti=( depression or anger AND psychosis) or ab:( child and therapy)',
      correct: '(ti:(depression) OR ti:(anger) AND ti:(psychosis)) OR (ab:(child) AND ab:(therapy))'
    },
    {
      command: '(ab=depression or title:(child AND anger))',
      correct: '(ab:(depression) OR (title:(child) AND title:(anger)))'
    },
    {
      command: 'ab:(depression or child NEAR/5 anger)',
      correct: '(ab:(depression) OR ab:(child NEAR/5 anger))'
    }
  ],

  truncation: [
    {
      command: 'ti:psyc*',
      correct: 'ti:(psyc*)'
    },
    {
      command: 'ti:(psyc* NOT dis*)',
      correct: '(ti:(psyc* NOT dis*))'
    },
    {
      command: 'ab:(a or b NEAR/5 psyc*)',
      correct: '(ab:(a) OR ab:(b NEAR/5 psyc*))'
    }
  ],


  exact: [
    {
      command: 'ti:"going near and far but not to far or long"',
      correct: 'ti:("going near and far but not to far or long")'
    },
    {
      command: "ti:'going near and far but not to far or long'",
      correct: "ti:('going near and far but not to far or long')"
    },
    {
      command: 'ti:"it\'s sunny outside"',
      correct: 'ti:("it\'s sunny outside")'
    },
    {
      command: "ti:(it's sunny outside)",
      correct: "(ti:(it's sunny outside))"
    },
    {
      command: 'ti:"The neurotic wandering mind: An individual differences investigation"',
      correct: 'ti:("The neurotic wandering mind: An individual differences investigation")'
    },
    {
      command: 'ti:"ab:a or b"',
      correct: 'ti:("ab:a or b")'
    }
  ],

  spacing: [
    {
      command: 'aff : ( a or b )',
      correct: '(aff:(a) OR aff:(b))'
    },
    {
      command: '     aff     : ( c     or d )',
      correct: '(aff:(c) OR aff:(d))'
    },
    {
      command: 'ab: a or ti=b',
      correct: 'ab:(a) OR ti:(b)'
    },
    {
      command: 'ab      :       a  or       ti=            b ',
      correct: 'ab:(a) OR ti:(b)'
    }
  ],

  punctuation: [
    {
      command: 'ISSUEPublicationDate:December 12, 2014',
      correct: 'IssuePublicationDate:(December 12, 2014)'
    },
    {
      command: 'ab: (giving 100% effort)',
      correct: '(ab:(giving 100% effort))'
    },
    {
      command: 'ab: (red & blue)',
      correct: '(ab:(red & blue))'
    },
    {
      command: 'ab: (red ^ blue)',
      correct: '(ab:(red ^ blue))'
    },
    {
      command: 'aff: (test@apa.org)',
      correct: '(aff:(test@apa.org))'
    },
    {
      command: 'ab: home-run',
      correct: 'ab:(home-run)'
    },
    {
      command: 'ab: hello world!!!',
      correct: 'ab:(hello world!!!)'
    },
    {
      command: 'ab: $200 is a lot of money',
      correct: 'ab:($200 is a lot of money)'
    },
    {
      command: 'ab: 5!=6',
      correct: 'ab:(5!=6)'
    },
    {
      command: 'ab: 2+3=5',
      correct: 'ab:(2+3=5)'
    },
    {
      command: 'ab: 3-1=2',
      correct: 'ab:(3-1=2)'
    },

    // this should gracefully fail and use our backup plan of just using what ever is in  criteria.value
    {
      command: 'ab: (does ßthis failß gracefully)',
      correct: ''
    }
  ]



};


describe('Command line parser', function() {

  describe('Basic queries', function(){
    testCases.basic.forEach(function(test){
      it(test.command + ' => ' + test.correct, function() {
        let parsed = parser.parse(test.command);
        assert.equal(parsed.query, test.correct);
      })
    });
  });


  describe('Equal signs are normalized to colons', function(){
    testCases.assignment.forEach(function(test){
      it(test.command + ' => ' + test.correct, function() {
        let parsed = parser.parse(test.command);
        assert.equal(parsed.query, test.correct);
      })
    });
  });


  describe('Uses correct field name case', function(){
    testCases.fieldCase.forEach(function(test){
      it(test.command + ' => ' + test.correct, function() {
        let parsed = parser.parse(test.command);
        assert.equal(parsed.query, test.correct);
      })
    });
  });

  describe('Boolean and proximity operators are case insensitive', function(){
    testCases.booleanOp.forEach(function(test){
      it(test.command + ' => ' + test.correct, function() {
        let parsed = parser.parse(test.command);
        assert.equal(parsed.query, test.correct);
      })
    });
  });

  describe('Operator "NOT" is case sensitive', function(){
    testCases.notOperator.forEach(function(test){
      it(test.command + ' => ' + test.correct, function() {
        let parsed = parser.parse(test.command);
        assert.equal(parsed.query, test.correct);
      })
    });
  });


  describe('Operator "NEAR/2" is case insensitive and requires a slash and number', function(){
    testCases.nearOperator.forEach(function(test){
      it(test.command + ' => ' + test.correct, function() {
        let parsed = parser.parse(test.command);
        assert.equal(parsed.query, test.correct);
      })
    });
  });


  describe('Grouped child terms will inherit the field name of their closest ancestor unless explicitly defined', function(){
    testCases.parenthesis.forEach(function(test){
      it(test.command + ' => ' + test.correct, function() {
        let parsed = parser.parse(test.command);
        assert.equal(parsed.query, test.correct);
      })
    });
  });

  describe('Terms without a field name will be assigned a default field name, in this case "title"', function(){
    testCases.fieldInheritance.forEach(function(test){
      it(test.command + ' => ' + test.correct, function() {
        let parsed = parser.parse(test.command, 'title');
        assert.equal(parsed.query, test.correct);
      })
    });
  });

  describe('Truncation is preserved', function(){
    testCases.truncation.forEach(function(test){
      it(test.command + ' => ' + test.correct, function() {
        let parsed = parser.parse(test.command, 'title');
        assert.equal(parsed.query, test.correct);
      })
    });
  });

  describe('Exact phrases are preserved and have the highest precedence for both single and double quotes', function(){
    testCases.exact.forEach(function(test){
      it(test.command + ' => ' + test.correct, function() {
        let parsed = parser.parse(test.command, 'title');
        assert.equal(parsed.query, test.correct);
      })
    });
  });

  describe('White space does not matter', function(){
    testCases.spacing.forEach(function(test){
      it(test.command + ' => ' + test.correct, function() {
        let parsed = parser.parse(test.command);
        assert.equal(parsed.query, test.correct);
      })
    });
  });

  describe('Punctuation is handled properly', function(){
    testCases.punctuation.forEach(function(test){
      it(test.command + ' => ' + test.correct, function() {
        let parsed = parser.parse(test.command);
        assert.equal(parsed.query, test.correct);
      })
    });
  });

});


/** Uncomment the following for a simple benchmark


let loopCount = 100;
let totalTestsExecuted = 0;
let startTime = new Date().getTime();

console.log('Test started: iterating over each test ' + loopCount + ' times');

for(let i=0; i<loopCount; i++){

  for( let testCase in testCases ) {
    testCases[testCase].forEach(function(test){
      let parsed = parser.parse(test.command);
      totalTestsExecuted++;
    });
  }

}


let endTime = new Date().getTime();
let executionTime = endTime - startTime;

console.log('Tests completed: ', totalTestsExecuted);
console.log('Time taken: ' + executionTime + 'ms');

 */
