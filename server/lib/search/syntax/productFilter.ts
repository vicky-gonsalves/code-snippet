import _ = require("lodash");
import {Identity} from "../../auth/identity/identity";
import {PsycNETQuery} from "../query/psycNETQuery";
import {PermissionService} from "../../auth/permissionService";
import {EntitlementService} from "../../auth/entitlementService";
import {ProductDefinitionService, IProductDefinition} from "../../product/productDefinitionService";

export class ProductFilter {
  query: PsycNETQuery;
  userIdentity: Identity;

  constructor(query: PsycNETQuery, userIdentity: Identity) {
    this.query = query;
    this.userIdentity = userIdentity;
  }

  getProductFilterString(): string{
    let productFilter = '';
    let databasesToFilterOn = [this.query.tab];

    if(this.query.tab === 'all'){
      databasesToFilterOn = _.cloneDeep(this.query.databases);
    }

    // PsycBOOKS and Book Collections should be rolled into one if they have a subscription to both, so append collections
    databasesToFilterOn  = this.foldCollectionsIntoPB(databasesToFilterOn);

    // determine if we need to filter on book collections instead of PsycBOOKS
    databasesToFilterOn  = this.applyBookCollectionsOnly(databasesToFilterOn);

    // determine if we need to filter on print subscriptions instead of PsycARTICLES
    databasesToFilterOn  = this.applyPrintSubscriptions(databasesToFilterOn);

    // get product filter
    productFilter = this.buildFilter(databasesToFilterOn);

    // If PsycINFO is part of the results, explicitly omit any chosen PI subsets
    if(databasesToFilterOn.length && this.query.tab === "PI"){
      let exclusionFilter = this.negatePsycINFOProducts();

      if(exclusionFilter.length){
        productFilter = productFilter + ' AND ' + exclusionFilter;
      }

    }

    return productFilter;
  }

  // If user has BookCollections and the query includes PB, then add BookCollections to the filter
  private foldCollectionsIntoPB(databasesToFilterOn: string[]): string[]{

    if(this.userIdentity.organization.id !== "-1" && this.shouldRollBookCollectionsIntoPB(databasesToFilterOn)){
      databasesToFilterOn = this.applyBookCollections(databasesToFilterOn);
    }

    return databasesToFilterOn;
  }

  // If user has print subscriptions, then we need to remove "PA" from the databases and add only the print subscriptions
  private applyPrintSubscriptions(databasesToFilterOn: string[]): string[]{
    let databases = [];

    if(this.userIdentity.organization.id !== "-1" && !PermissionService.mayAccess(this.userIdentity.entitlements,'PA') && this.shouldApplyPrintSubscriptions(databasesToFilterOn)){
      let printSubscriptions = this.getUserPrintSubscriptionCodes();
      let dbsWithoutPA = _.pull(databasesToFilterOn,"PA");
      databases = [...dbsWithoutPA, ...printSubscriptions];
    } else{
      databases = databasesToFilterOn;
    }

    return databases;
  }

  // If user has book collections and not "PB", then we need to remove "PB" from the databases and add only the book collections
  private applyBookCollectionsOnly(databasesToFilterOn: string[]): string[]{
    let databases = [];

    if(this.userIdentity.organization.id !== "-1" && !PermissionService.mayAccess(this.userIdentity.entitlements,'PB') && this.shouldRollBookCollectionsIntoPB(databasesToFilterOn)){
      let bookCollections = this.getUserBookCollectionCodes();
      let dbsWithoutPB = _.pull(databasesToFilterOn,"PB");
      databases = [...dbsWithoutPB, ...bookCollections];
    } else{
      databases = databasesToFilterOn;
    }

    return databases;
  }


  private buildFilter(databasesToFilterOn: string[], operator: string = 'OR', isNegation: boolean = false): string{
    let filter = '';
    let tokens = [];
    let pair = {};
    let negateString = isNegation ? '-' : '';
    let products = ProductDefinitionService.getProductsByPNCodes(databasesToFilterOn);
    let product:IProductDefinition;

    for( let i =0; i<products.length; i++){
      product = products[i];

      product.filter.forEach( item => {
        pair = negateString + item.field + ':' + item.value + '';
        tokens.push(pair);
      });
    }

    if(tokens.length){
      filter = tokens.join(' ' + operator + ' ');

      // add outer parenthesis
      filter = '(' + filter + ')';
    }

    return filter;
  }

  private negatePsycINFOProducts(): string{
    let databases = this.query.databases;
    let databasesDuplicatedInPI = ['PA', 'PB', 'PC'];
    let mayAccess = {
      pa: PermissionService.mayAccess(this.userIdentity.entitlements,'PB'),
      pb: PermissionService.mayAccess(this.userIdentity.entitlements,'PB'),
      pc: PermissionService.mayAccess(this.userIdentity.entitlements,'PC')
    };

    // only include products that exist in both lists
    let products = _.intersection(databasesDuplicatedInPI, databases);

    // if the user has book collections but not PB, then remove PB from the products filter
    if( PermissionService.hasGroupAccess(this.userIdentity.entitlements, 'bookCollection') && !mayAccess.pb ) {
      _.pull(products, 'PB');
    }

    // if the user has a print subscription but not PA, then remove PA from the products filter
    if( PermissionService.hasGroupAccess(this.userIdentity.entitlements, 'printSubscription') && !mayAccess.pa ) {
      _.pull(products, 'PA');
    }

    if(this.query.source === 'mylist'){

      // if the user does not have PA, then remove PA from the products filter
      if (!mayAccess.pa) {
        _.pull(products, 'PA');
      }

      // if the user does not have PB, then remove PB from the products filter
      if (!mayAccess.pb && this.userIdentity.organization.id === "-1") {
        _.pull(products, 'PB');
      }

      // if the user does not have PC, then remove PC from the products filter
      if (!mayAccess.pc) {
        _.pull(products, 'PC');
      }
    }

    if( this.shouldRollBookCollectionsIntoPB(databases) ){
      let bookCollectionCodes = this.getUserBookCollectionCodes();
      products = [...products, ...bookCollectionCodes];
    }

    if( this.shouldApplyPrintSubscriptions(databases) ){
      let printSubscriptionCodes = this.getUserPrintSubscriptionCodes();
      products = [...products, ...printSubscriptionCodes];
    }

    let filter = this.buildFilter(products, 'AND', true);

    return filter;
  }

  private shouldRollBookCollectionsIntoPB(databases: string[]): boolean{
    return ( databases.indexOf("PB") >= 0 && PermissionService.hasGroupAccess(this.userIdentity.entitlements, 'bookCollection') );
  }

  private shouldApplyPrintSubscriptions(databases: string[]): boolean{
    return ( databases.indexOf("PA") >= 0 && PermissionService.hasGroupAccess(this.userIdentity.entitlements, 'printSubscription') );
  }

  // get user book collections and append them to the database filter lis
  private applyBookCollections(databases: string[]): string[] {
    let bookCollectionCodes = this.getUserBookCollectionCodes();
    let productsWithBookCollections = [...databases, ...bookCollectionCodes];

    return productsWithBookCollections;
  }

  private getUserBookCollectionCodes(): string[]{
    let entitlements = EntitlementService.getEntitlementsByGroup(this.userIdentity.entitlements, 'bookCollection');
    return this.extractCodesFromEntitlements(entitlements);
  }

  private getUserPrintSubscriptionCodes(): string[]{
    let entitlements = EntitlementService.getEntitlementsByGroup(this.userIdentity.entitlements, 'printSubscription');
    return this.extractCodesFromEntitlements(entitlements);
  }

  private extractCodesFromEntitlements(entitlements): string[]{
    let codes: string[] = [];

    entitlements.forEach( entitlement => {
      codes.push(entitlement.code);
    });

    return codes;
  }



}
