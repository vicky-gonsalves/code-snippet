import {Criteria} from '../query/components/criteria';
import {PsycNETQuery} from '../query/psycNETQuery';
import {ISortDescription} from "../query/components/interfaces";
import {Limit} from "../query/components/limit";
import {CommandLineParser, ICommandlineResult} from "./commandline/commandLineParser";
import _ = require("lodash");
import * as moment from 'moment';

export interface IQueryParameters {
  q: string,
  fq: string,
  productFilter: string,
  sort?: string,
  boosting?: boolean
}

export class QueryFormulator {
  private static negateOperator: string = "-";

  public static getQueryParameters(query: PsycNETQuery): IQueryParameters {
    // use a copy so that manipulations doesn't effect form behavior
    let queryCopy = _.cloneDeep(query);

    let params: IQueryParameters = {
      q: QueryFormulator.getQueryString(queryCopy),
      fq: QueryFormulator.getFilterString(queryCopy),

      // this is a relevancy sort parameter
      boosting: QueryFormulator.isRelevancySort(queryCopy),

      // this field is populated on demand within Search.applyProductFilterByIdentity().  This allows use to generate a search and apply
      // a product filter for specific individuals/organizations without the need to create a new search
      productFilter: ''
    };

    return params;
  }

  private static getQueryString(query: PsycNETQuery): string {
    let result = '';
    let tokens = [];
    let criteria:Criteria;
    let op = '';
    let usesEmbeddedBooleans = false;
    let commandLineResult: ICommandlineResult = null;

    for(let i=0; i < query.criteria.length; i++){
      criteria = query.criteria[i];
      op = criteria.operator;
      usesEmbeddedBooleans = false;

      if(i !== 0){
        //op = (op ==='NOT') ? 'AND' : op;
        tokens.push(op);
      }

      // convert brackets to quotes for Author, Journal, and Index Terms queries
      criteria = QueryFormulator.convertExactMatchToQuotes(criteria);

      // normalize curly quotes to straight quotes
      criteria.value = QueryFormulator.normalizeQuotes(criteria.value);

      //apply wildcard to Cited Refs FirstName field
      if(query.source === 'citedRefs'){
        criteria = QueryFormulator.applyWildcardToFirstName(criteria);
      }

      commandLineResult = CommandLineParser.parse(criteria.value, criteria.field);

      // if the command line parser was able to tokenize the user input, then add it
      if(commandLineResult.queryFormatted.length){
        tokens.push(commandLineResult.queryFormatted);
      } else {
        // if the user enters something strange, we may not be able to tokenize it correctly.  As a fail safe, just add the field-value pair
        tokens.push( QueryFormulator.getFieldValueString(criteria, '|') );
      }

    }

    if(tokens.length){
      result = tokens.join(' ');
    }

    result = result.replace(/ NOT /g, ' AND -');

    return result;
  }

  private static getFilterString(query: PsycNETQuery): string {
    let result = '';
    let tokens = [];

    // process dates
    let dateString = QueryFormulator.getDateString(query);

    if(dateString.length){
      tokens.push( dateString );
    }

    // process onlyShow
    let onlyShowString = QueryFormulator.getOnlyShowWhereString(query);

    if(onlyShowString.length){
      tokens.push( onlyShowString );
    }

    // process facets
    let facetString = QueryFormulator.getFacetString(query);

    if(facetString.length){
      tokens.push( facetString );
    }


    // process limits
    let limitString = QueryFormulator.getLimitString(query);

    if(limitString.length){
      tokens.push( limitString );
    }

    //process native options
    let nativeOptionsString = QueryFormulator.getNativeOptionsString(query);

    if(nativeOptionsString.length){
      tokens.push( nativeOptionsString );
    }



    if(tokens.length){
      result = tokens.join(' AND ');
    }

    return result;
  }

  private static getLimitString(query: PsycNETQuery): string {
    let result = '';
    let limits = query.limits;
    let aLimit:Limit;
    let limitCriteria: Criteria;
    let limitStr = '';
    let tokens = [];
    let isNegation: boolean = false;
    let fieldMap = {
      DocumentTypePE: 'DocumentTypeFilt',
      ContentProviderType: 'ContentProviderType',
      AuxiliaryMaterial: 'AuxiliaryMaterialFilt',
      PopulationGroup: 'PopulationGroupFilt',
      Methodology: 'MethodologyFilt'
    };

    for(let i=0; i<limits.length; i++){
      aLimit = limits[i];

      isNegation = (aLimit.operator === 'is not');

      // remove any quotes from the values, particularly the AgeGroupFilt filters
      aLimit.values = aLimit.values.map( value => {
        // normalize quotes.  Remove them, then add them back in to ensure every request is consistent
        value = value.replace(/"/g, '');
        value = '"' + value + '"';

        return value;
      });

      // update field value
      if(fieldMap.hasOwnProperty(aLimit.field)){
        aLimit.field = fieldMap[aLimit.field];
      }

      limitCriteria = new Criteria(aLimit.field, 'OR', aLimit.values.join(','));

      limitStr = QueryFormulator.getFieldValueString(limitCriteria, ',', isNegation);

      // add parenthesis around limits
      if(limitStr.length){
        limitStr = '('+limitStr+')';
      }

      tokens.push( limitStr );

    }

    if(tokens.length){
      result = tokens.join(' AND ');
    }

    return result;
  }

  private static getOnlyShowWhereString(query: PsycNETQuery): string {
    let result = '';
    let onlyShow = query.filter.onlyShow;
    let tokens = [];

    if(onlyShow.peerReviewed) {
      tokens.push('PublicationTypeFilt:"Peer Reviewed Journal"');
    }

    if(onlyShow.ftOnly){
      tokens.push('HasFullText:true');
    }

    if(onlyShow.testAvailable){
      tokens.push('(TestItemsAvailable:"Yes")');
    }

    if(onlyShow.openAccess){
      tokens.push('(OpenAccess:(Yes))');
    }

    if(onlyShow.impactStatement){
      tokens.push('(AbstractType:("Impact Statement"))');
    }

    /*
    if(onlyShow.latestUpdate){
      let to = moment().format('YYYYMMDD');
      let from = moment().subtract(7,'d').format('YYYYMMDD');
      tokens.push(`ReleaseDateUTC:[${from} TO ${to}]`);
    }
    */

    if(tokens.length){
      result = tokens.join(' AND ');
    }

    return result;
  }

  private static getFacetString(query: PsycNETQuery): string {
    let result = '';
    let facets = query.filter.facets;
    let tokens = [];
    let facetValue = '';
    let fieldKey = '';
    let fieldMap = {
      "PublicationType" : "PublicationTypeFilt",
      "IndexTerms" : "IndexTermsFilt",
      "SerialTitle" : "SerialTitleFilt",
      "Affiliation" :"AffiliationFilt",
      "Construct": "ConstructFilt",
      "Author" : "AuthorFilt",
      "AgeGroup": "AgeGroupFilt",
      "PopulationGroup" : "PopulationGroupFilt",
      "InstrumentType": "InstrumentTypeFilt",
      "Methodology" : "MethodologyFilt",
      "TestRecordType" : "TestRecordTypeFilt",
      "Permissions" : "PermissionsFilt",
      "Instrumentation" : "InstrumentationFilt",
      "Classification" : "ClassificationFilt"
    };

    for(let i=0; i<facets.length; i++){

      // wrap in quotes
      facetValue = '"' + facets[i].value + '"';
      fieldKey = facets[i].field;

      // wrap in parenthesis
      if(facets[i].field.indexOf('PublicationType') >=0 || facets[i].field.indexOf('PubYear') >=0 || facets[i].field.indexOf('PublicationYear') >=0){
        facetValue = '(' + facetValue + ')';
      }else if(fieldMap.hasOwnProperty(fieldKey)){
        fieldKey = fieldMap[fieldKey];
      }

      let facetItem = fieldKey + ':' + facetValue;

      // Index terms (key and value) are surrounded by parenthesis.
      if (facetItem.length) {
        facetItem = '(' + facetItem + ')'
      }

      tokens.push(facetItem);
    }

    if(tokens.length){
      result = tokens.join(' AND ');
    }

    return result;
  }

  private static getFieldValueString(criteria: Criteria, delimiter: string=',', isNegation:boolean=false){
    let result = '';
    let negateString = isNegation ? QueryFormulator.negateOperator: '';
    let arrValues = criteria.value.split(delimiter);
    let tokens = [];
    let pair = '';

    for(let i=0; i<arrValues.length; i++){

      if(criteria.operator === "NOT"){
        criteria.operator = "AND";
      }

      if(criteria.field == 'AnyField' && arrValues[i].indexOf(':(') >= 0){
          pair =  '(' + negateString + arrValues[i] + ')';

      }
      else{
        pair = negateString + criteria.field + ':(' + arrValues[i] + ')';
      }


      tokens.push(pair);
    }

    result = tokens.join(' ' + criteria.operator + ' ');

    // wrap exact match criteria in parentheses
    if(QueryFormulator.isExactMatchField(criteria.field)){
      result = '(' +result + ')';
    }

    return result;
  }


  /**
   * Applies syntax for Author, Journal, and Index Terms
   * @param value
     */
  private static convertExactMatchToQuotes(criteria: Criteria): Criteria{
    let result: Criteria = criteria;

    if(criteria.value.indexOf('{') >=0 && criteria.value.indexOf('}') >=0 && criteria.value.indexOf(' TO ') === -1){

      if(criteria.field === 'TestNames'){
        result.value = criteria.value.replace(/(\{|})/g, '');
        result.field = criteria.field ;
      }
      else{
      // replace brackets with quotes
      result.value = criteria.value.replace(/(\{|})/g, '"');

      // the field should have the 'Filt' suffix for exact matches
      if(!QueryFormulator.isExactMatchField(criteria.field)){
        result.field = criteria.field + 'Filt';
      }
      }
      // each exact term gets its own field:value pair so we need to mark them as seperate list items
      if(criteria.value.indexOf(' AND ') >= 0){
        result.value = criteria.value.replace(/ AND /g, '|');
        result.operator = 'AND';
      }else if(criteria.value.indexOf(' OR ') >= 0){
        result.value = criteria.value.replace(/ OR /g, '|');
        result.operator = 'OR';
      }

    }

    return result;
  }

  /**
   * Applies a wildcard to the cited refs firstName field
   * @param value
   */
  private static applyWildcardToFirstName(criteria: Criteria): Criteria{
    let result: Criteria = criteria;

    if(criteria.field === 'FirstName' && criteria.value.length && criteria.value.indexOf('*') === -1){
      result.value = result.value + '*';
    }

    return result;
  }

  private static getDateString(query: PsycNETQuery): string{
    let result = '';
    let dateRange = query.filter.dateRange;

    // if there is no date restriction exit out of function
    if(dateRange.begin === 'eternity' && dateRange.end === 'eternity'){
      return result;
    }

    let now = new Date();
    let d1 = (dateRange.begin === 'eternity') ? '0' : dateRange.begin;
    let d2 = (dateRange.end === 'eternity') ? String(now.getFullYear()) : dateRange.end;

    if(d1.indexOf('?*') >= 0 || d1 === d2){
      result = dateRange.field + ':' + d1;
    } else if (dateRange.field === 'releaseDate'){
      result = `((PIReleaseDate:[${d1} TO ${d2}] OR PEReleaseDate:[${d1} TO ${d2}]) OR (FTReleaseDate:[${d1} TO ${d2}] OR PTReleaseDate:[${d1} TO ${d2}]))`;
    } else if (query.databases.indexOf('PT') >= 0 && dateRange.field !== 'TestYear'){
      result = `(${dateRange.field}:[${d1} TO ${d2}] OR TestYear:[${d1} TO ${d2}])`;
    } else {
      result = `(${dateRange.field}:[${d1} TO ${d2}])`;
    }

    return result;
  }

  private static getNativeOptionsString(query: PsycNETQuery): string{
    let result = '';
    let pb = query.nativeOptions.pb;
    let optionValue = '';
    let tokens = [];
    let bookTypeCriteria;
    let bookPubTypeCriteria;
    let bookAudienceCriteria;
    let bookTypeString = '';
    let bookPubTypeString ='';
    let bookAudienceString ='';
    let bookTypeTokens = [];
    let bookPubTypeTokens = [];
    let bookAudienceTokens = [];
    let bookTypeMap  = {
      classicBook: '"Classic Book"',
      conferenceProceedings: '"Conference Proceedings"',
      handbookManual: '"Handbook/Manual"',
      referenceBook: '"Reference Book"',
      textbookStudyGuide: '"Textbook/Study Guide"'
    };

    // build book publication type filters
    if(pb.publicationType.authored) {
      bookPubTypeTokens.push('"Authored"');
    }
    if(pb.publicationType.edited) {
      bookPubTypeTokens.push('"Edited"');
    }
    if(bookPubTypeTokens.length){
      bookPubTypeCriteria = new Criteria('PublicationTypeFilt', 'OR', bookPubTypeTokens.join(','));
      bookPubTypeString = QueryFormulator.getFieldValueString(bookPubTypeCriteria);
      tokens.push(bookPubTypeString);
    }

    // build book audience filters
    if(pb.audience.generalPublic) {
      bookAudienceTokens.push('"General"');
    }
    if(pb.audience.professional) {
      bookAudienceTokens.push('"Professional"');
    }
    if(bookAudienceTokens.length){
      bookAudienceCriteria = new Criteria('AudienceTypeFilt', 'OR', bookAudienceTokens.join(','));
      bookAudienceString = QueryFormulator.getFieldValueString(bookAudienceCriteria);
      tokens.push(bookAudienceString);
    }

    // build book type filters
    for(let prop in pb.type){
      if(pb.type.hasOwnProperty(prop) && pb.type[prop] && bookTypeMap.hasOwnProperty(prop)){
        bookTypeTokens.push(bookTypeMap[prop]);
      }
    }

    if(bookTypeTokens.length){
      bookTypeCriteria = new Criteria('BookTypeFilt','OR', bookTypeTokens.join(','));
      bookTypeString = QueryFormulator.getFieldValueString(bookTypeCriteria);
      tokens.push(bookTypeString);
    }

    if(tokens.length){
      result = tokens.join(' AND ');
    }

    return result;
  }

  public static getSortString(query: PsycNETQuery): string{
    let result = '';
    let sort:ISortDescription[] = query.displayOptions.sort;
    let tokens = [];

    for(let i=0; i<sort.length; i++){
      tokens.push(sort[i].field + ' ' + sort[i].direction);
    }

    if(tokens.length){
      result = tokens.join(',');
    }

    return result;
  }

  private static isRelevancySort(query: PsycNETQuery): boolean{
    let result = false;
    let sort:ISortDescription[] = query.displayOptions.sort;

    if(sort.length && sort[0].field === 'score'){

      // if there are any criteria with 'AnyField' fields, then apply a boosting param.

      for(let i=0; i < query.criteria.length; i++){
        if(query.criteria[i].field === 'AnyField'){
          result = true;
          break;
        }
      }
    }

    return result;
  }


  // determine if 'Filt' fields are used for exact matching
  private static isExactMatchField(field: string): boolean{
    return (field.indexOf('Filt') >= 0);
  }

  // changes all = to : for commandline compatibility
  private static convertEqualsToColon(value: string): string{
    return value.replace(/=/g, ':');
  }

  private static isNegateQuery(criteria: Criteria): boolean{
    return criteria.value.indexOf(' NOT ') >= 0;
  }

  private static normalizeQuotes(value: string): string{
    return value.replace(/(\"|'|‘|“|’|”)/g, "\"");
  }
}
