import {Search} from './search';
import {Identity} from "../auth/identity/identity";
import {PermissionService} from "../auth/permissionService";
import * as _ from "lodash";

export class SearchDatabaseValidator {

  static validate(identity: Identity, search: Search): Search{
    let activeTab = null;
    let entitlements = identity.entitlements;

    // individual users search the 5 main databases and then mark the first database they have access to as the active tab
    let guestDbs = ["PI","PA","PB","PC","PE"];
    let allDB= ["PI","PA","PB","PC","PE","PT","PH"];
    let accessAvailableDBs = PermissionService.filterValidEntitlements(entitlements, allDB);

    // if nothing is selected, then select all of them
    if(search.query.databases.length === 0){
      search.query.databases = accessAvailableDBs;
    }

    if( identity.organization.id !== "-1" ) {

      // organizations only search the products they have access to
      search.query.databases = PermissionService.filterValidEntitlements(entitlements, search.query.databases);

      // make sure that the selected tab is a valid database
      if (search.query.tab !== 'all' && search.query.databases.indexOf(search.query.tab) === -1) {
        search.query.tab = search.query.databases[0];
      }

      if (search.query.databases.length === 1) {
        search.query.tab = search.query.databases[0];
      }

    } else {


      let mayAccessAllDefaultDBs = PermissionService.mayAccessAll(entitlements, guestDbs);
      let individualDefaultDBs = ["PA","PB"];

      // if the user has access to the 5 main databases, then they can select/deselect databases for a search.  Otherwise,
      // they are required to search the 5 min databases plus anything else they might have access to
      if(mayAccessAllDefaultDBs){
        search.query.databases = PermissionService.filterValidEntitlements(entitlements, search.query.databases);
      } else {
        search.query.databases = _.union(guestDbs, accessAvailableDBs);
      }

      // calculate which tab to be selected by default
      if(mayAccessAllDefaultDBs && search.query.tab === 'all' && search.query.databases.length > 1){ // all tab
        activeTab = search.query.tab;
      } else if ((mayAccessAllDefaultDBs && search.query.tab !== 'all') || individualDefaultDBs.indexOf(search.query.tab) > -1 || accessAvailableDBs.indexOf(search.query.tab) > -1) { // any tab as long as the user has access to all products
        activeTab = search.query.tab;
      } else if(search.query.tab === 'all' && search.query.databases.length === 1 && accessAvailableDBs.indexOf(search.query.databases[0]) > -1) { // if only one is selected, then choose it
        activeTab = search.query.databases[0];
      } else { // default to PA
        activeTab = "PA";
      }

      search.query.tab = activeTab;

    }

    return search;
  }

}
