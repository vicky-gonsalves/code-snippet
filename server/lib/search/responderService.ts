import {Search} from "./search";
import config = require('../../config/settings');

interface IResponder {
  path: string,
  parameters: {}
}

export class ResponderService {

  latestUpdatePath = '/psycinfo/query/LatestReleaseDate?';

  responder = {
    piRecord: {
      path: '/psycinfo/query/facet?',
      parameters: {
        fl: 'GivenBookSeriesTitle,InstitutionalAuthor,Abstract,UID,AuthorName,GivenDocumentTitle,TransDocumentTitle,PublicationYear,SourcePI,SourcePE,ISSN,ISBN,PMID,SetISBN,Language,KeyConcepts,Classification,PopulationGroup,AudienceType,PublicationStatus,PublicationHistory,PublicationName,PublicationType,DocumentType,Methodology,DOI,IndexTerms,SerialTitle,Correspondence,Affiliation,AwardsAndRecognition,KeyParticipants,Narrator,Publisher,BookType,BookTitle,AgeGroup,Location,SupplementalMaterial,Instrumentation,CopyrightStatement,CopyrightHolder,CopyrightYear,ReviewedItemOrig,ReviewedAuthorOrig,ReviewedISBN,SponsorList,ConferenceInfo,ConferenceNote,PIReleaseDate,FTReleaseDate,PEReleaseDate,PICorrectionDate,FTCorrectionDate,PECorrectionDate,XMLLink,PDFLink,HasFullText,HasCitations,ExternalLink,OtherSerialTitle,OtherPublisher,CitedByCount,ProductCode,AuthorAPA,FormatAvailable,SourceAPA,FormatCovered,EditorOrig,EmailAddress,SpecialIssueTitle,SpecialSectionTitle,UMI,Pagination,PageCount,ContentOwner,PublicationDate,Distributor,Institutions,BookCollection,MonthSeason,PAJournalCode,PAVolume,PAIssueCode,PAFirstPage,SupplementalDOI,OpenURL,SFXOpenURL,ArtID,IsOFP,PBID,Construct,AdministrationMethods,TestTitle,TestPublisher,TestAuthorName,TestRecordType,TestYear,NumberOfTestItems,SupportingDocumentation,SupportingDocumentationLink,Purpose,AlternateTitle,Acronyms,Websites,Format,Accommodations,Permissions,Fee,Commercial,PTReleaseDate,AdministrationTime,Validity,Setting,Reliability,WorkUID,TestUID,MediaType,PDFLinkCategory,MediaFormat,RunningTime,MediaLink,OtherPopulationDetails,TestNameSource,TOC,GrantSponsorship,HasOpenAccess,DocumentTypeID,InstrumentType,FactorAnalysis,TestLocation,OtherVersions,AuthorID,DataSetDescriptionList,DataSetAccessList,SerialTitleTranslation,DissertationDetails,ClinicalTrialNumber,NLMSerialTitleAbbreviation,MeSH,ImpactStatement,HasImpactStatement,HasAbstract,AbstractLanguageList,ImpactStatementLanguageList,SourcePIWithAuthorIDs,Volume,Issue',
        facet: false,
        fc: ''
      }
    },
    piSearch: {
      path: '/psycinfo/query/facet?',
      parameters: {
        fl: "Acronyms,GivenBookSeriesTitle,AwardsAndRecognition,KeyParticipants,Narrator,UID,PublicationType,DocumentType,BookTitle,BookType,GivenDocumentTitle,TransDocumentTitle,AuthorName,AuthorOrig,SourcePI,SourcePE,HasFullText,XMLLink,PDFLink,HasCitations,CitedByCount,ProductName,ProductCode,ProductNameFacet,PIReleaseDate,PEReleaseDate,DOI,ContentOwner,ExternalLink,PublicationYear,FTReleaseDate,BookCollection,MonthSeason,PublicationDate,ArtID,IsOFP,Construct,WorkUID,AdministrationMethods,TestTitle,TestPublisher,TestAuthorName,TestRecordType,PDFLinkCategory,MediaLink,MediaLinkCategory,TestYear,TestNameSource,HasAbstract,HasOtherVersions,OpenURL,SFXOpenURL,InstitutionalAuthor,SpecialSectionTitle,PublicationName,HasOpenAccess,AuthorID,DataSetAccessList,ClinicalTrialNumber,NLMSerialTitleAbbreviation,MeSH,HasImpactStatement,PAJournalCode,PBID",
        facet: false,
        results: true
      }
    },

    // this responder definition will inherit all other properties from 'piSearch' above
    mylist: {
      path: '/mylist/query/facet?',
      parameters: {}
    },

    mylistFacet: {
      path: '/psycinfo/query/facet?',
      parameters: {
        fc: "ProductName,PAJournalCode,BookCollection",
        facet: true,
        start: 0,
        rows: 0,
        hl: false
      }
    },

    piSearchTabCounts: {
      path: '/psycinfo/query/facet?',
      parameters: {
        fc: "ProductName,BookCollection",
        facet: true,
        results: false
      }
    },

    piFacet: {
      path: '/psycinfo/query/facet?',
      parameters: {
        //fc: "PublicationType,PublicationYear,IndexTerms,PublicationType,Author,AgeGroup,PopulationGroup,Methodology,Instrumentation,Classification,ProductName,BookCollection",
        // for performance test, Author is removed below
        fc: "PublicationYear,Affiliation,IndexTerms,PublicationType,AgeGroup,PopulationGroup,Methodology,Instrumentation,Classification,ProductName,BookCollection,SerialTitle,Acronyms,Construct",
       // fc: "PublicationType,ProductName,BookCollection",
        facet: true,
        start: 0,
        rows: 0,
        hl: false
      }
    },

    ptRecord: {
      path: '/psycinfo/PT/query/facet?',
      parameters: {
        fl: 'Acronyms,Abstract,AdministrationMethods,Affiliation,AlternateTitle,Commercial,DOI,UID,TestUID,TestYear,TestTitle,TestPublisher,TestAuthorName,TestRecordType,Purpose,Format,Fee,PublicationType,DocumentType,GivenDocumentTitle,TransDocumentTitle,AuthorName,SourcePI,SourcePE,HasFullText,XMLLink,PDFLink,ProductName,ProductCode,ProductNameFacet,PIReleaseDate,PTReleaseDate,ContentOwner,PublicationYear,FTReleaseDate,MonthSeason,PublicationDate,Construct,WorkUID,Language,SupportingDocumentation,SupportingDocumentationLink,NumberOfTestItems,Websites,Accommodations,Permissions,EmailAddress,Correspondence,PDFLinkCategory,MediaLink,MediaLinkCategory,TestNameSource,InstrumentType,OtherVersions,AuthorID,SourceAPA',
        child: true,
        facet: false
      }
    },
    ptChildRecord: {
      path: '/psycinfo/PT/query/facet?',
      parameters: {
        fl: 'GivenBookSeriesTitle,InstitutionalAuthor,Abstract,UID,AuthorName,AuthorOrig,GivenDocumentTitle,TransDocumentTitle,PublicationYear,SourcePI,SourcePE,ISSN,ISBN,PMID,SetISBN,Language,KeyConcepts,Classification,PopulationGroup,AudienceType,PublicationStatus,PublicationHistory,PublicationName,PublicationType,DocumentType,Methodology,DOI,IndexTerms,SerialTitle,Correspondence,Affiliation,AwardsAndRecognition,KeyParticipants,Narrator,Publisher,BookType,BookTitle,AgeGroup,Location,SupplementalMaterial,Instrumentation,CopyrightStatement,CopyrightHolder,CopyrightYear,ReviewedItemOrig,ReviewedAuthorOrig,ReviewedISBN,SponsorList,ConferenceInfo,ConferenceNote,PIReleaseDate,FTReleaseDate,PEReleaseDate,PICorrectionDate,FTCorrectionDate,PECorrectionDate,XMLLink,PDFLink,HasFullText,ExternalLink,OtherSerialTitle,OtherPublisher,CitedByCount,ProductCode,AuthorAPA,FormatAvailable,SourceAPA,FormatCovered,EditorOrig,EmailAddress,SpecialIssueTitle,SpecialSectionTitle,UMI,Pagination,PageCount,ContentOwner,PublicationDate,Distributor,Institutions,BookCollection,MonthSeason,PAJournalCode,PAVolume,PAIssueCode,PAFirstPage,SupplementalDOI,OpenURL,SFXOpenURL,ArtID,IsOFP,PBID,Construct,AdministrationMethods,TestTitle,TestPublisher,TestAuthorName,TestRecordType,TestYear,NumberOfTestItems,SupportingDocumentation,SupportingDocumentationLink,Purpose,AlternateTitle,Acronyms,Websites,Format,Accommodations,Permissions,Fee,Commercial,PTReleaseDate,AdministrationTime,Validity,Setting,Reliability,WorkUID,TestUID,MediaType,PDFLinkCategory,MediaFormat,RunningTime,MediaLink,OtherPopulationDetails,TestNameSource,TOC,GrantSponsorship,HasOpenAccess,DocumentTypeID,InstrumentType,FactorAnalysis,TestLocation,OtherVersions,AuthorID,DataSetDescriptionList,DataSetAccessList,SerialTitleTranslation,DissertationDetails,ClinicalTrialNumber,NLMSerialTitleAbbreviation,MeSH,HasImpactStatement,HasAbstract,AbstractLanguageList,ImpactStatementLanguageList,SourcePIWithAuthorIDs',
        child: false,
        facet: false,
        hl: false,
        results: true
      }
    },
    citedRefs: {
      path: '/citedref/query/facet?',
      parameters: {
        fl: 'DOI,Author,DocumentTitle,PubYear,ReleaseDate,Seq,SourceTitle,TextRef,UID,DocumentType,PublicationType,BookType,CitedByCount',
        fc: 'Author,PubYear,PublicationType,SourceTitle',
        facet: true
      }
    },
    uidLookup: {
      path: '/psycinfo/query/facet?',
      parameters: {
        fl: 'UID',
        facet: false,
        fc: ''
      }
    },
    PBIDLookup: {
      path: '/psycinfo/query/facet?',
      parameters: {
        fl: 'PBID,UID,DOI',
        facet: false,
        fc: ''
      }
    },
    PCIssueLookup: {
      path: '/psycinfo/query/facet?',
      parameters: {
        fl: 'PCReleaseDate,UID,ExternalLink',
        facet: false,
        fc: ''
      }
    },

    citationLookUp:{
      path: '/psycinfo/query/facet?',
      parameters: {
        fl: 'UID,SourceAPA',
        facet: false,
        hl: false,
        fc: ''
      }
    },

    // used for HTML data loading from API Request module
    fulltextDataLookup: {
      path: '/psycinfo/query/facet?',
      parameters: {
        fl: 'UID,DOI,ProductCode,PAJournalCode,XMLLink,FirstAuthorFullName,HasCitations,CopyrightHolder,CopyrightYear,CitedByCount,SupplementalDOI,PDFLink,ExternalLink,SupplementalMaterial,EditorOrig,SpecialIssueTitle,SpecialSectionTitle,SpecialIssueEditors,Volume,Issue,PublicationYear,MonthSeason,HasOpenAccess,ClassificationCode,GivenDocumentTitle',
        facet: false,
        fc: '',
        results: true,
        start:0,
        rows:1,
      }
    },

    // used for fulltext access checking middleware
    fulltextAccess: {
      path: '/psycinfo/query/facet?',
      parameters: {
        fl: 'UID,ProductCode,PAJournalCode,BookCollection,PDFLink,MediaLink,HasOpenAccess,IsOFP',
        facet: false,
        fc: '',
        results: true,
        start:0,
        rows:1,
      }
    },

    // used for DOI redirect logic
    doiResolver: {
      path: '/psycinfo/query/facet?',
      parameters: {
        fl: 'UID,DOI,ProductCode,PAJournalCode,PDFLink,XMLLink,HasOpenAccess',
        facet: false,
        fc: '',
        results: true,
        start:0,
        rows:1,
      }
    },

    citedIn: {
      path:'/cited/query/facet?',
      parameters: {
        fl: 'ArtID,Author,AuthorOrig,BookCollection,CitedByCount,DocTitle,DocumentType,DOI,ExternalLink,GivenDocumentTitle,HasCitations,HasFullText,HasOpenAccess,HTMLLink,JournalCode,PDFLink,ProductCode,PublicationType,PublicationYear,PubYear,RecordPILink,Seq,SourcePI,SourceAPA,SrcTitle,TextRef,TransDocumentTitle,UID,XMLLink,HasAbstract,HasImpactStatement,DataSetAccessList',
        facet: false,
        results: true,
        format : 'json'
      }
    },

    latestRelease: {
      path:'/psycinfo/query/LatestReleaseDate?',
      parameters: {
        fl: 'ArtID,Author,AuthorOrig,BookCollection,CitedByCount,DocTitle,DocumentType,DOI,ExternalLink,GivenDocumentTitle,HasCitations,HasFullText,HasOpenAccess,HTMLLink,JournalCode,PDFLink,ProductCode,PublicationType,PublicationYear,PubYear,RecordPILink,Seq,SourcePI,SrcTitle,TextRef,TransDocumentTitle,UID,XMLLink,HasAbstract,HasImpactStatement,DataSetAccessList',
        facet: false,
        results: true,
        format : 'json'
      }
    },

    purchaseHistory: {
      path:'/psycinfo/query/facet?',
      parameters: {
        fl: 'UID,DOI,ProductCode,PAJournalCode,JournalCode,PDFLink,XMLLink,HasFullText,AuthorOrig,GivenDocumentTitle,SourcePI,ItemNumber',
        facet: false,
        results: true,
        format : 'json'
      }
    }
  };

  constructor(){

    // make mylist inherit properties from piSearch
    this.responder.mylist.parameters = this.responder.piSearch.parameters

  }


  getResponder(search: Search):IResponder{
    let result:IResponder = null;

    switch(search.type){
      case 'search':
        result = this.responder.piSearch;

        // for proper sequencing of records, we need to use a special my list responder
        if(search.query.source === 'mylist'){
          result = this.responder.mylist;
        }

        break;

      case 'facet':
        result = this.responder.piFacet;

        // My list searches use their special facet
        if(search.query.source === 'mylistFacet') {
          result = this.responder.mylistFacet;
        }

        // PsycTESTS searches use their own facets
        if(search.query.tab === 'PT'){
          result.parameters['fc'] = this.getPsycTESTSOnlyFacets();
        }

        break;

      case 'tabCounts':
        result = this.responder.piSearchTabCounts;
        break;
      case 'record':
        result = this.responder.piRecord;
        break;
      case 'citedRefs':
        result = this.responder.citedRefs;
        break;
      case 'ptRecord':
        result = this.responder.ptRecord;
        break;
      case 'ptChildRecord':
        result = this.responder.ptChildRecord;
        break;
      case 'uidLookup':
        result = this.responder.uidLookup;
        break;
      case 'PBIDLookup':
        result = this.responder.PBIDLookup;
        break;
      case 'PCIssueLookup':
        result = this.responder.PCIssueLookup;
        break;
      case 'citationLookUp':
        result = this.responder.citationLookUp;
        break;
      case 'fulltextDataLookup':
        result = this.responder.fulltextDataLookup;
        break;
      case 'fulltextAccess':
        result = this.responder.fulltextAccess;
        break;
      case 'doiResolver':
        result = this.responder.doiResolver;
        break;
      case 'citedIn':
        result = this.responder.citedIn;
        break;
      case 'purchaseHistory':
        result = this.responder.purchaseHistory;
        break;
      default:
        result = this.responder.piSearch;
    }

    if(search.query.filter.onlyShow.latestUpdate){
      result.path = this.latestUpdatePath;
    }

    return result;
  }

  private getPsycTESTSOnlyFacets(){
    // for performance test, Author is removed below
    //return 'IndexTerms,Author,Affiliation,AgeGroup,PopulationGroup,Classification,TestRecordType,TestYear,Permissions,Construct,InstrumentType';
    return 'IndexTerms,Affiliation,AgeGroup,PopulationGroup,ProductName,Classification,TestRecordType,TestYear,Permissions,Construct,InstrumentType';
  }
}


