import {PsycNETQuery} from './query/psycNETQuery';
import {QueryFormulator, IQueryParameters} from './syntax/queryFormulator';
import {IResponseParameters} from "./query/components/interfaces";
import {ProductFilter} from "./syntax/productFilter";
import {Identity} from "../auth/identity/identity";
import {FriendlyQuery} from "./syntax/friendlyQuery";
var uuid = require('uuid/v1');

export class Search {
  id: string = '';
  type: string = 'search';  // possible values 'search', 'record', 'citedRefs', 'ptRecord'
  query: PsycNETQuery;
  timeCreated: Date;
  queryParameters: IQueryParameters;
  productFilter: string = '';
  responseParameters: IResponseParameters;
  friendlyQuery: string = '';
  totalRecords: string = '0';


  // the 'metadata' property is a stash for items such as UID (record display lookup), ProduceCode, or any other data which might be helpful to identify the search
  metadata = {
    uid:  '',
    excludeFromLogs: false,

    // indicator to make sure we do not log tab or page changes
    hasBeenLogged: false,

    // directUID occurs when a user directly goes to the record display page /record/2016-22958-009
    directUID: false,

    // used for load testing to bypass ML and only hit Node.js
    bypassML: false
  };

  constructor(query: PsycNETQuery, responseParameters: IResponseParameters, id?:string){

    if(!id){
      id = uuid();
    }

    this.id = id;
    this.query = query;
    this.responseParameters = responseParameters;
    this.timeCreated = new Date();
    this.queryParameters = QueryFormulator.getQueryParameters(query);

    // only calculate certain things if there are results shown
    if(this.responseParameters && this.responseParameters.results){
      this.queryParameters.sort = QueryFormulator.getSortString(query);

      // generate friendly query
      this.friendlyQuery = FriendlyQuery.getFriendlyQuery(this.query);
    }

    // set the search type
    if(this.query.source === "citedRefs"){
      this.type = "citedRefs";
    }

  }

  applyProductFilterByIdentity(userIdentity:Identity):void{
    // using product filters on cited refs breaks the responder
    if(this.type !== 'citedRefs' && this.query.source !== 'mylistFacet'){
      let productFilter = new ProductFilter(this.query, userIdentity);
      this.queryParameters.productFilter = productFilter.getProductFilterString();
    }
  }



}
