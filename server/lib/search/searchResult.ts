import {Search} from "./search";

export class SearchResult {
  results: any;
  search: Search;
  executionTime: number  = 0;

  constructor(results: any, search: Search, executionTime?: number){
    this.results = results;
    this.search = search;

    if(executionTime){
      this.executionTime = executionTime;
    }

  }


}

