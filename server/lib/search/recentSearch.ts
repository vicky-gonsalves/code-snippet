import _ = require('lodash');
import {Search} from './search'
import {TopicAlerts} from '../myPsycNet/topicAlerts';
import {Identity} from '../auth/identity/identity';
import {SavedSearch} from './savedSearch/savedSearch';
var async = require('async');

export class RecentSearch {

  // types of queries which are valid for saving as a recent search
  static validQueryTypes = ['easy','advanced', 'citedRefs'];

  // adds a search to the user's recent searches list
  static add(req, res, next, cb){

    // only save searches that produce results (not facet only) or make sure it's a valid query type before saving
    if( !RecentSearch.isValidRecentSearch(req.result.search) ){
      return next();
    }

    // this is a valid search to save as a 'Recent search' so add it to the collection
    req.session.recentSearch.push(req.result.search);
    req.session.save();

    return next();
  }


  static isValidRecentSearch(search: Search){
     if (!search.metadata.hasBeenLogged && search.responseParameters.results && RecentSearch.validQueryTypes.indexOf(search.query.source) >= 0 && (typeof search.friendlyQuery === 'string') && search.responseParameters.start === 0)
       return true;
     else
       return false;
  }


  // returns a list of recent searches
  static list(req, res, next, callback){
    let limit = req.body.params.limit || 0;
    let allSearches = _.cloneDeep(req.session.recentSearch);
    // show recent first
    let searches = allSearches.reverse();

    // get the top 'n'
    if(limit){
      searches = searches.slice(0, limit);
    }

    let searchIds: Array<any>= [], userId: number, topicAlertsList: Array<any>= [] , savedSearchList: Array<any>= [];
    async.series({

      prepareParams: function (cb) {
        let identity:Identity = req.session.identity;
        userId = parseInt(identity.individual.id);
        searchIds = _.map(searches, 'id');
        return cb();
      },
      saveSearches: function (cb) {
        if (searchIds && searchIds.length) {
          SavedSearch.getSavedSearchBasedSearchIDs(userId, searchIds, (err, savedSearches) =>{

            if (err) {
              return cb();
            }

            savedSearchList = savedSearches;
            return cb();
          });
        }else{
          return cb();
        }
      },
      topicAlerts: function(cb){
        if (searchIds && searchIds.length) {
          TopicAlerts.getTopicAlertBasedSearchIDs(userId, searchIds, (err, topicAlerts) => {

            if (err) {
              return callback(null, searches);
            }

            topicAlertsList = topicAlerts;
            return cb();
          });
        }else{
          return cb();
        }
      }
    },function (err) {
      for (let search of searches) {
        search['hasEmailAlert'] = topicAlertsList && topicAlertsList.length && _.findIndex(topicAlertsList, ["SEARCHID", search.id]) > -1;
        search['isSaved'] = savedSearchList && savedSearchList.length && _.findIndex(savedSearchList, ["SEARCHID",  search.id]) > -1;
      }

      callback(null, searches);
    });
  }

  // returns a list of recent searches for the advanced search form page
  static getLatest(req, res, next, callback){
    let limit = req.body.params.limit || 0;
    let allSearches = _.cloneDeep(req.session.recentSearch);
    // show recent first
    let searches = allSearches.reverse();

    // get the top 'n'
    if(limit){
      searches = searches.slice(0, limit);
    }

    callback(null, searches);
  }

  // gets a search from the recent searches list
  static get(req, res, next, cb){
    let id = req.body.params.id || '';
    RecentSearch.getById(req.session.recentSearch, id, (err, search) => {
      cb(null, search);
    });
  }

  static getById(collection, id, cb){
    let searches = collection;
    let search = {};

    // exit out early if no 'id' present
    if(id.length === 0){
      return cb(null, search);
    }

    // Find the most recent search instance that has the specified ID.
    let index = searches.length - 1;
    let proceed = true;
    while (index >= 0 && proceed) {
      if (id === searches[index].id) {
        search = searches[index];
        proceed = false;
      }
      index--;
    }

    cb(null, search);
  }

  static delete(req, res, next, cb){
    let id = req.body.params.id || '';
    let searches = _.cloneDeep(req.session.recentSearch);

    // exit out early if no 'id' present
    if(id.length === 0){
      return cb(null, {removed:""});
    }

    var index = 0;
    for(; index < searches.length; index++){
      if(id === searches[index].id){
        break;
      }
    }
    searches.splice(index,1);
    req.session.recentSearch = _.cloneDeep(searches);

    cb(null, {removed:id, searches : searches.reverse()});
  }
}
