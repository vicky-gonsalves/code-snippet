import {Search} from "./search";
import {HttpHandler} from "../apiRequest/util/httpHandler";
import {ResponderService} from "./responderService";
import {CacheService} from '../cache/cacheService';

var config = require('../../config/settings');
var qs = require('querystring');
var _ = require('lodash');
var async = require('async');

var cacheKeyPrefix = "cache:results:";

export class RequestHandler {

  static performRequest(search: Search, callback: Function) {
    let cachedData = null;
    let mlData = null;
    let requestOptions = {};
    let serviceCall = '';

    async.series({

        // prepare the service call
        prepareQuery: function(cb){

          requestOptions = {
            hostname: config.marklogic.hostname,
            api: search.type,
            port: config.marklogic.port,
            path: RequestHandler.formatPathWithParameters(search),
            method: "GET",
            headers: {}
          };

          serviceCall = 'http://' + requestOptions['hostname'] + ':' + requestOptions['port'] + requestOptions['path'];

          return cb();
        },

        // check cache first
        checkCache: function(cb){

          if(!config.cache.searchResults){
            return cb();
          }

          RequestHandler.getCachedData(serviceCall, (err, data) => {
            if(err){ return cb(err); }

            // save the data for the next call to inspect
            cachedData = JSON.parse(data);

            if(data){
              console.log('\nUsing cache for: ', serviceCall);
              // we could cache this again to start the timer all over, but for now we'll be safe for memory purposes
              //RequestHandler.cacheResults(serviceCall, cachedData);
            }

            return cb();
          });

        },

        // get data from MarkLogic if there is no cache
        getDataFromML: function(cb){
          if(cachedData){
            return cb();
          }

          console.log("\nCalling ML for: ", serviceCall);

          let httpHandler:HttpHandler = new HttpHandler();

          httpHandler.execute(requestOptions, JSON.stringify({}), (err, data) => {
            if(err){ return cb(err); }
            let parsedData = {};

            try {
              parsedData = JSON.parse(data);
            } catch (exception) {
              //return cb(exception);
            }

            // normalize response
            mlData = RequestHandler.getMLResponse();

            if(parsedData.hasOwnProperty('response') && parsedData['response'].hasOwnProperty('result') && parsedData['response']['result'].hasOwnProperty('doc')) {
              mlData.result = parsedData['response']['result'];
            }

            if(parsedData.hasOwnProperty('response') && parsedData['response'].hasOwnProperty('facets')) {
              mlData.facets = parsedData['response']['facets'];
            }

            // save to cache if enabled
            if(config.cache.searchResults){
              RequestHandler.cacheResults(serviceCall, mlData);
            }

            return cb();
          });

        },

      },

      // when all the steps are finished, return the result to the user
      function(err) {
        if(err) {
          return callback(err);
        }

        let dataForUser = cachedData || mlData;

        // return the results
        callback(null,dataForUser);
      });

  }

  public static getMLResponse(){
    return {
      result: {
        doc:[],
        numFound: 0
      },
      facets: []
    };
  }

  private static formatPathWithParameters(search: Search): string{
    let responderService = new ResponderService();
    let responder = responderService.getResponder(search);
    let result = responder.path;

    // everything is json
    search.responseParameters.format = 'json';

    let queryParamsWithProductFilter = this.getQueryParamsWithMergedProductFilter(search);

    // merge the request parameters into one, in the order of precedence (least to highest) over overriding
    let params = _.merge({}, queryParamsWithProductFilter, search.responseParameters, responder.parameters);
    result = result + qs.stringify(params);

    return result;
  }

  // merges the filter (fq) and product filter together for the request
  private static getQueryParamsWithMergedProductFilter(search: Search): any{
    let queryParams = {
      q: search.queryParameters.q,
      fq: search.queryParameters.fq,
      sort: search.queryParameters.sort
    };

    let separator = queryParams.fq.length ? ' AND ' : '';

    if(search.queryParameters.productFilter){
      queryParams.fq = queryParams.fq + separator + search.queryParameters.productFilter;
    }

    // only apply the boosting param if we are doing relevancy sort
    if(search.queryParameters.boosting){
      queryParams['boosting'] = search.queryParameters.boosting;
    }

    return queryParams;
  }

  private static cacheResults(serviceUrl, data){
    CacheService.cacheResults(cacheKeyPrefix, serviceUrl, data);
  }

  private static getCachedData(serviceUrl, cb){
    CacheService.getCachedData(cacheKeyPrefix, serviceUrl, (err, cachedData) => {
      if(err){
        return cb(err);
      }

      return cb(null, cachedData);
    });
  }

}


