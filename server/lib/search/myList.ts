import {AuroraSQL} from '../sql/awsSqlConnection';
import * as _ from "lodash";
var async = require('async');

var statsClient = require('../logs/statsClient');

export class MyList {
  static getMyList(req, res, next, cb) {
    console.log(req.body);
    let connection = AuroraSQL.getPNConnection();
    let UserID = req.session.identity.individual.id;
    let TagID = req.body.params.TagID;

    let placeHolders = [UserID];
    let listQuery = "SELECT article_id, notes FROM SavedArticle WHERE user_id= ?";

    if (TagID && TagID > 0) {
      placeHolders = [UserID, TagID];
      listQuery += " AND id IN (SELECT ID FROM SavedArticleTags WHERE TAG_ID = ? )";
    }
    listQuery += " ORDER by id desc";

    let tagsQuery = "SELECT SAT.TAG_ID, TL.TAGNAME, COUNT(SAT.TAG_ID) as tagsCount FROM SavedArticle SA INNER JOIN SavedArticleTags SAT ON SAT.ID = SA.ID AND SA.user_id = ? INNER JOIN TagList TL ON TL.USER_ID = SA.user_id AND TL.TAG_ID = SAT.TAG_ID GROUP BY SAT.TAG_ID, TL.TAGNAME ORDER BY tagsCount DESC";
    let articleTagsQuery = "SELECT  TL.TAG_ID, SA.ARTICLE_ID, TL.TAGNAME FROM SavedArticle SA INNER JOIN SavedArticleTags SAT ON SAT.ID = SA.ID  AND SA.USER_ID = ? INNER JOIN TagList TL ON TL.USER_ID = SA.user_id  AND TL.TAG_ID = SAT.TAG_ID ORDER BY SA.ARTICLE_ID, TL.TAGNAME ASC";

    async.parallel({
      list: function (parallel_done) {
        connection.query(listQuery, placeHolders, function (err, results) {
          if (err) return parallel_done(err);
          parallel_done(null, results);
        });
      },
      tags: function (parallel_done) {
        connection.query(tagsQuery, [UserID], function (err, results) {
          if (err) return parallel_done(err);
          parallel_done(null, results);
        });
      },
      articleTags: function (parallel_done) {
        connection.query(articleTagsQuery, [UserID], function (err, results) {
          if (err) return parallel_done(err);
          parallel_done(null, results);
        });
      }
    }, function (err, finalResult) {
      connection.end();
      if (err) {
        console.log(err);
        return cb(err);
      }
      MyList.reFormatData(finalResult, function (data) {
        cb(null, JSON.stringify(data));
      });
    });
  }

  static reFormatData(data, callback) {
    let simplifiedData = {tags: [], list: []};
    if (data && data.tags && data.list && data.articleTags) {
      simplifiedData.tags = data.tags;
      data.list.forEach(function (_listItem) {
        if (_listItem && _listItem.article_id) {
          let listItem = {articleId: null, tags: [], notes: null};
          let _articleTags = _.filter(data.articleTags, function (_arTag) {
            return _arTag['ARTICLE_ID'] == _listItem.article_id;
          });
          listItem.tags = _.map(_articleTags, function (a) {
            return {tagId: a['TAG_ID'], tagName: a['TAGNAME']};
          });
          listItem.articleId = _listItem.article_id;
          listItem.notes = _listItem.notes;
          simplifiedData.list.push(listItem);
        }
      })
    }
    callback(simplifiedData);
  }

  static delete(req, res, next, cb) {
    let connection = AuroraSQL.getPNConnection();
    let UserID = req.session.identity.individual.id;
    let data = {};
    let ArticleList = req.body.params.ArticleList;
    //ArticleList = "'" + ArticleList.join("','") + "'";

    statsClient.increment('mylist.delete');

    let deleteArticleQuery = "DELETE FROM SavedArticleTags WHERE ID IN (SELECT ID FROM SavedArticle WHERE ARTICLE_ID IN (?) AND user_id = ? )";
    let deleteTagsQuery = "DELETE FROM SavedArticle WHERE ARTICLE_ID IN (?) AND user_id =?";
    async.waterfall([
      function (callback) {
        connection.query(deleteTagsQuery, [ArticleList, UserID], function (err, results) {
          if (err) return callback(err);
          data = {"isDeleted": true};
          callback();
        });
      },
      function (callback) {
        connection.query(deleteArticleQuery, [ArticleList, UserID], function (err, results) {
          if (err) return callback(err);
          data = {"isDeleted": true};
          callback(null);
        });
      }
    ], function (err) {
      if (err) {
        console.log(err);
        data = err;
      }
      connection.end();
      cb(null, JSON.stringify(data));
    });
  }

  static insert(ArticleList, UserID, cb) {
    let connection = AuroraSQL.getPNConnection();
    let data = {};
    let articleData = [];

    statsClient.increment('mylist.insert');
    for (let a = 0; a < ArticleList.length; a++) {
      articleData.push([UserID.toString(), "myList1", a, ArticleList[a]])
    }
    let insertArticleQuery = "INSERT INTO SavedArticle (user_id , list, `order`, article_id) VALUES ?";

    connection.query(insertArticleQuery, [articleData], function (err, results) {
      if (err) {
        console.log(err);
        console.log(insertArticleQuery);
        console.log(articleData);
        data = err;
      } else {
        data = {"isAdded": true};
      }
      cb(null, JSON.stringify(data));
      connection.end();
      return data;
    });
  }

  static addTagNotes(req, res, next, cb) {
    let connection = AuroraSQL.getPNConnection();
    let UID = req.body.params.UID;
    let Notes = req.body.params.Notes;
    let Tags = req.body.params.Tags;
    let UserID = req.session.identity.individual.id.toString();
    let articleID = 0;
    let TagsListTemp = Tags.split(';');
    let TagsList = [];
    let data = {};

    statsClient.increment('mylist.addTagNotes');

    let getArticleQuery = "SELECT ID FROM SavedArticle WHERE user_id =? AND article_id =?";
    let updateArticleNotesQuery = "UPDATE SavedArticle SET NOTES = ? WHERE ID =?";
    let deleteArticleQuery = "DELETE FROM SavedArticleTags WHERE ID IN (SELECT ID FROM SavedArticle WHERE ARTICLE_ID = ? AND user_id = ? )";

    async.waterfall([
      function (callback) {
        connection.query(deleteArticleQuery, [UID, UserID], function (err, results) {
          if (err) return callback(err);
          data = {"isDeleted": true};
          callback(null);
        });
      },
      function (callback) {
        connection.query(getArticleQuery, [UserID, UID], function (err, results) {
          if (err) return callback(err);
          articleID = results[0].ID;
          callback();
        });
      },
      function (callback) {
        connection.query(updateArticleNotesQuery, [Notes, articleID], function (err, results) {
          if (err) return callback(err);
          callback();
        });
      },
      function (callback) {
        async.each(TagsListTemp, function (tag, eachCallBack) {
          if (tag.trim().length) {
            MyList.addTags(UserID, tag.trim(), articleID, (err, data)=> {
              eachCallBack();
            });
          }
          else
            eachCallBack();
        }, function (err) {
          callback(null);
        });
      }
    ], function (err) {
      if (err) {
        console.log(err);
        console.log(getArticleQuery);
        data = err;
      }
      connection.end();
      cb(null, JSON.stringify(data));
    });
  }


  //Todo : cb impl
  static addTags(UserID, Tag, articleID, cb) {
    statsClient.increment('mylist.addTags');
    let connection = AuroraSQL.getPNConnection();
    let addTagsQuery = "INSERT INTO TagList(USER_ID, TAGNAME) VALUES (?,?)";
    let checkTagsQuery = "SELECT TAG_ID FROM TagList WHERE USER_ID = ? AND TAGNAME = ?";
    let addArticleTagsQuery = "INSERT INTO SavedArticleTags(ID, Tag_ID) VALUES (?,?)";
    connection.query(addTagsQuery, [UserID, Tag], function (err, Tagresults) {
      if (err)
        console.log(err);
      let UID = articleID;
      let TagID = 0;
      connection.query(checkTagsQuery, [UserID, Tag], function (err, Tagresults) {
        if (Tagresults && Tagresults.length) {
          TagID = Tagresults[0].TAG_ID;
        }
        else {
          connection.query(addTagsQuery, [UserID, Tag], function (err, Tagresults) {
            if (err) {
              console.log(err);
            }
            TagID = Tagresults.insertId;
          });
        }
        connection.query(addArticleTagsQuery, [articleID, TagID], function (err, results) {
          if (err) {
            console.log(err);
          }
          cb(null);

        });
      });
    });
  }


  static add(req, res, next, cb) {
    console.log(req.body);
    let connection = AuroraSQL.getPNConnection();
    let UserID = req.session.identity.individual.id;
    let data = {};
    let ArticleList = req.body.params.ArticleList;
    let articleData = [];

    statsClient.increment('mylist.add');

    let checkArticleQuery = "Select article_id FROM SavedArticle WHERE user_id = ? AND article_id IN (?)";

    connection.query(checkArticleQuery, [UserID, ArticleList], function (err, results) {
      if (err) {
        console.log(err);
        data = err;
        connection.end();
        cb(null, JSON.stringify(data));
      } else {
        for (let r = 0; r < results.length; r++) {
          console.log(results[r]);
          if (ArticleList.indexOf(results[r].article_id) > -1) {
            ArticleList.splice(ArticleList.indexOf(results[r].article_id), 1);
          }
        }
        if (ArticleList.length) {
          MyList.insert(ArticleList, UserID, cb)
        }
        else {
          data = {"isAdded": false};
          cb(null, JSON.stringify(data));
        }
        connection.end();
      }

    });
  }


  static getUIDs(req, res, next, cb) {
    let userId = req.session.identity.individual.id;
    let connection = AuroraSQL.getPNConnection();
    let qryGetUIDs = "Select article_id FROM SavedArticle WHERE user_id = ?";
    let uids = [];

    connection.query(qryGetUIDs, [userId], function (err, results) {
      connection.end();

      if (err) {
        return cb(err);
      }

      for (let i = 0; i < results.length; i++) {
        uids.push(results[i].article_id);
      }

      return cb(null, uids);
    });
  }


}
