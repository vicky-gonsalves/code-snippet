import _ = require('lodash');
import {Search} from "../search";
import {PsycNETQuery} from "../query/psycNETQuery";
var wddxReader = require("./WddxReader");
import {WddxCreator} from './WddxCreator';

export class WddxService{

  public static queryToWddx(search : Search) : string{

    var WDDXSearchString = '';

    WDDXSearchString = WddxCreator.convertWDDXString(search.query);

    return WDDXSearchString;
  }

  public static wddxToQuery(savedSearchStr, cb){

    wddxReader.convertQuery(savedSearchStr,(err, result) => {

      if(err){
        cb(err);
      }

      cb(null, result);
    });
  }
}
