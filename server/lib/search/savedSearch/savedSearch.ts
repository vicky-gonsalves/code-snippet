import {PsycNETQuery} from "../query/psycNETQuery";
var _ = require("lodash");
import {AuroraSQL} from '../../sql/awsSqlConnection';
import {WddxService} from './WddxService';
import {SavedSearchTableSchema} from './savedSearchTableSchema';
import {Identity} from '../../auth/identity/identity';
import {DateParser} from '../../utility/dateParser';
import {Search} from '../search';
import {RecentSearch} from "../recentSearch";
import {HighlightService} from "../../utility/highlightService";
var config = require('../../../config/settings');

var statsClient = require('../../logs/statsClient');

export class SavedSearch {

  static getSavedSearchList(req, res, next, cb) {

    let identity: Identity = req.session.identity;
    let id: number = parseInt(identity.individual.id);

    statsClient.increment('savedsearch.recall');

    let query = 'SELECT ss.SEARCHID, ss.ASREGID, ss.ORGASREGID, ss.WDDXSEARCHSTRING, ss.SEARCHNAME, ss.FRIENDLYQUERY, ss.DATABASES, ss.TIMESAVED, ss.NOTES, ss.QUERYHASH, ' +
      'sn.ID, sn.FREQUENCY, sn.CREATE_DATE,  sn.LAST_SEND_DATE, sn.EXPIRESAFTER FROM ' +
      'SavedSearches ss LEFT JOIN ' +
      'SearchNotifier sn ON ss.SEARCHID = sn.SAVEDSEARCH_ID ' +
      'WHERE ' +
      'ss.ASREGID = ? ' +
      'ORDER BY ss.TIMESAVED DESC LIMIT 10000';

    var connection = AuroraSQL.getPNConnection();
    connection.connect();

    connection.query(query, [id], function (err, resultSet) {

      console.log("AuroraSQL service query: " + query);
      console.log("AuroraSQL service params: ", JSON.stringify(id));

      var data = [];

      if (err) {
        return cb(err);
      }

      if (resultSet && resultSet.length) {
        for (let rowSet of resultSet) {

          WddxService.wddxToQuery(rowSet.WDDXSEARCHSTRING, (err, result) => {
            let newSearch = {
              friendlyQuery: rowSet.FRIENDLYQUERY,
              databases: rowSet.DATABASES.toUpperCase().replace(/pq|PQ/g, "PC"),
              query: result,
              searchName: rowSet.SEARCHNAME,
              id: rowSet.SEARCHID,
              hasEmailAlert: (rowSet.ID && rowSet.ID != null && rowSet.ID != ''),
              frequency: rowSet.FREQUENCY,
              expiresAfter: rowSet.EXPIRESAFTER,
              exipres: DateParser.calculateExpireDate(rowSet.CREATE_DATE, rowSet.LAST_SEND_DATE)
            };
            data.push(newSearch);
          });
        }
      }

      cb(null, data);
    });

    connection.end();
  }

  static get(id: string, cb) {
    let query = 'SELECT ss.SEARCHID, ss.ASREGID, ss.ORGASREGID, ss.WDDXSEARCHSTRING, ss.SEARCHNAME, ss.FRIENDLYQUERY, ss.DATABASES, ss.TIMESAVED, ss.NOTES, ss.QUERYHASH, ' +
      'sn.ID, sn.FREQUENCY, sn.CREATE_DATE,  sn.LAST_SEND_DATE, sn.EXPIRESAFTER FROM ' +
      'SavedSearches ss LEFT JOIN ' +
      'SearchNotifier sn ON ss.SEARCHID = sn.SAVEDSEARCH_ID ' +
      'WHERE ' +
      'ss.SEARCHID = ? ';

    let connection = AuroraSQL.getPNConnection();
    connection.connect();
    connection.query(query, [id], function (err, resultSet) {
      if (err) {
        return cb(err);
      }
      if (resultSet && resultSet.length) {
        return cb(null, resultSet.shift());
      } else {
        return cb(true);
      }
    });
    connection.end();
  }

  static saveSearch(req, res, next, cb) {
    let searchName = '';

    statsClient.increment('savedsearch.save');

    if (!SavedSearch.hasParam(req.body.params, 'searchId')) {
      if (SavedSearch.hasParam(req.body.params, 'searchName')) {
        searchName = req.body.params['searchName'];
      } else {
        searchName = HighlightService.removeHighlights(req.search.friendlyQuery);
      }
    }
    let saveSearch: SavedSearchTableSchema = new SavedSearchTableSchema(searchName, req.search, req.session.identity);
    saveSearch.WDDXSearchString = WddxService.queryToWddx(req.search);

    var connection = AuroraSQL.getPNConnection();
    connection.connect();

    let insertQuery = 'INSERT INTO SavedSearches SET ? ';
        connection.query(insertQuery, saveSearch, function (err, resultSet) {
          if (err) {
            connection.end();
            return cb(err);
          }
          cb(null, {isSaved: "true", SearchId: saveSearch.SearchId});
          connection.end();
        });
  }

  static deleteSearch(req, res, next, cb) {

    let savedSearchId = req.body.params.searchId;
    statsClient.increment('savedsearch.delete');
    var connection = AuroraSQL.getPNConnection();
    connection.connect();
    connection.query('DELETE FROM SavedSearches WHERE SearchId = ?', [savedSearchId], function (err, resultSet) {
      if (err) {
        cb(err);
      }

      console.log('Record Updated ' + resultSet.affectedRows + ' rows');
      cb(null, {isDelete: "true", deletedID: savedSearchId});
    });

    connection.end();
  }

  static getExistingPermalink(req, res, next, cb){
    let params = req.body.params;
    let result = {
      permalink: ''
    };

    // user already has a saved search, so just return the link
    if(SavedSearch.hasParam(params, 'savedSearchId')){
      result.permalink = SavedSearch.preparePermalink(params.savedSearchId);
    }

    cb(null, result);
  }

  static getNewPermalink(req, res, next, cb){
    let result = {
      permalink: ''
    };

    // we need to save the search first before processing it
    SavedSearch.saveSearch(req, res, next, function(err, saveResult){
      result.permalink = SavedSearch.preparePermalink(saveResult.SearchId);
      cb(null, result);
    });
  }

  static preparePermalink(savedSearchId) {
    let url  = 'http://' + config.web.hostname + (config.isDeveloping ? ':' + config.web.port : '') + '/permalink/' + savedSearchId;
    return url;
  }

  static getExistingRssLink(req, res, next, cb){
    let params = req.body.params;
    let result = {
      SearchId: ''
    };

    // user already has a saved search, so just return the link
    if(SavedSearch.hasParam(params, 'searchId')){
      result.SearchId = params.searchId;
    }

    cb(null, result);
  }

  static hasParam(params, field) {
    return (params.hasOwnProperty(field) && params[field].length);
  }

  static getSavedSearchBasedSearchIDs(userId:number, searchIdList:Array<any>, callback) {

    if (searchIdList && searchIdList.length) {

      let query = 'SELECT ss.SEARCHID from ' +
        'SavedSearches ss WHERE ' +
        'ss.ASREGID = ? AND ss.SEARCHID IN ( ? ) ' +
        'ORDER BY ss.TIMESAVED DESC';

      var connection = AuroraSQL.getPNConnection();
      connection.connect();
      connection.query(query, [userId, searchIdList], function (err, resultSet) {

        console.log("AuroraSQL service query: " + query);
        console.log("AuroraSQL service params: ", userId, JSON.stringify(searchIdList));

        var data = [];

        if (err) {
          callback(err);
        }

        if (resultSet && resultSet.length) {
          data = resultSet;
        }

        callback(null, data);
      });

      connection.end();
    } else {
      callback(new Error("No params found"));
    }
  }
}
