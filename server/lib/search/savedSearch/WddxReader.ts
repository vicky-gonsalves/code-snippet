var xml2js = require('xml2js');
var parser = new xml2js.Parser({explicitRoot:true, cdata:false, explicitArray:false, mergeAttrs: true});
import _ = require('lodash');
import {Search} from "../search";
import {ICriteria, ISortDescription, IDisplayOptions, INativeOptions, IFacetFilter, IDateRange, IOnlyShow, IFilter} from "../query/components/interfaces";
import {DisplayOptions} from "../query/components/displayOptions";
import {NativeOptions} from "../query/components/nativeOptions";
import {Filter} from "../query/components/filter";
import {PsycNETQuery} from "../query/psycNETQuery";


var labelMap = {
  "ARRCRITERIA" : "criteria",
  "ARRLIMITS" :"limits",
  "FILTER" : "filter",
  "DISPLAYOPTIONS" : "displayOptions" ,
  "NATIVEOPTIONS": "nativeOptions" ,
  "DATABASES" : "databases",
  "VIEWMODE" :"viewMode",
  "SOLRINDEX": "index",
  "SOURCE": "source"
};

var onlyShowLabelMap = {
  "PEERREVIEWED": "peerReviewed",
  "FTONLY" : "ftOnly" ,
  "TESTAVAILABLE" : "testAvailable" ,
  "LATESTUPDATE": "latestUpdate",
  "OPENACCESS": "openAccess",
  "IMPACTSTATEMENT": "impactStatement",
};

var dateRangeLabelMap = {
  "BEGIN": "begin",
  "END": "end",
  "FIELD": "field",
  "PUBY1": "puby1",
  "YEARRANGE": "yearRange"
};

function convertQuery(wddxString :string, callback : Function) {

  parser.parseString(wddxString, function (err, result){
    if(err){
      console.log(err);
      callback(err);
    }

    var queryData = result.wddxPacket.data.struct.var;

    var pnQuery : PsycNETQuery  = new PsycNETQuery();
    pnQuery.source = 'advanced';

    for(let element of queryData){
      let key = labelMap[element.name];

      switch(element.name){
        case 'SOLRINDEX' :
        case 'VIEWMODE':
        case 'SOURCE':
            pnQuery[key] = element.string;
              break;

        case 'ARRCRITERIA' :
            pnQuery[key] = parseCriteria(element);
              break;

        case 'ARRLIMITS' :
            pnQuery[key] = parseLimits(element);
              break;

        case 'DATABASES' :
            pnQuery[key] = element.string.split(",");
              break;

        case 'FILTER' :
            pnQuery[key] = parseFilters(element);
              break;

        case 'NATIVEOPTIONS' :
            pnQuery[key] = parseNativeOption(element);
              break;

        case 'DISPLAYOPTIONS' :
          pnQuery[key] = parseDisplayOption(element);
              break;
      }
    }

    if(pnQuery.source === 'citedRefs'){
      pnQuery.displayOptions.sort = [{field: "CitedInCountSort1", direction: "desc"}];
    }

    callback(null, pnQuery);
  });
}

function parseCriteria(filterElem){
  var criteriaArray: ICriteria[] = [];
  var labelMap = {
   'F': 'field',
   'O': 'operator',
   'SOURCE': 'source',
   'V':'value'
  };

  if(!filterElem.array || !filterElem.array.struct){
    return criteriaArray;
  }

  if(filterElem.array.length == '1'){
    filterElem.array.struct = new Array(filterElem.array.struct);
  }

  for(let criteriaList of filterElem.array.struct){
    if(criteriaList.var && criteriaList.var.length){
      let criteria  = {
        field: "",
        operator: "",
        value: "",
        source: ""
      };

      for(var prop of criteriaList.var){
        if(prop.name != "ORIGIN"){
          criteria[labelMap[prop.name]] = prop.string;
        }
      }

      // validate the value in case garbled text is identified and value is no longer a string.
      if(!criteria.value.indexOf){
        criteria.value = '';
      }

      criteriaArray.push(criteria);
    }
  }

  return criteriaArray;
}

/*
 * Parsing limits based on the JSON object
 * */
function parseLimits(data){
  let limits = [];
  var labelMap = {
    'LV': 'values',
    'LO': 'operator',
    'L': 'field'
  };

  if(!data.array || !data.array.struct){
    return limits;
  }

  if(data.array.length == '1'){
    data.array.struct = new Array(data.array.struct);
  }

  for(let limitList of data.array.struct){

    if(limitList.var && limitList.var.length){
      let limit = {
        field: "",
        operator: "",
        values: []
      };

      for(var prop of limitList.var){
        if(prop.name == "LV"){
          let tempArray = prop.string.split(",");
          limit[labelMap[prop.name]] = tempArray;
        }else{
          limit[labelMap[prop.name]] = prop.string;
        }
      }
      limits.push(limit);
    }
  }

  return limits;
}

/*
 * Parsing Display option based on the JSON object
 * */

function parseDisplayOption(filterElem){
  var displayOptions: IDisplayOptions = new DisplayOptions();

  if(!filterElem.struct || !filterElem.struct.var || !filterElem.struct.var.length){
    return displayOptions;
  }

  for(var options of filterElem.struct.var){
    if(options.name === "SORT"){
      displayOptions.sort = getSortOption(options, displayOptions.sort);
    }else{
      displayOptions.pageSize = options.string;
    }
  }

  function getSortOption(options, sortOrder){
    let label = {
      'FIELD' : 'field',
      'DIRECTION': 'direction'
    };

    if(options.array && options.array.struct && options.array.struct.length) {

      for (var options of options.array.struct) {
        if(options.var && options.var.length){

          let sort = {};

          for(var optn of options.var){
            sort[label[optn.name]] = optn.string;
          }

          sortOrder.push(sort);
        }
      }
    }
    return sortOrder;
  }

  return displayOptions;
}

/*
* Parsing Native option based on the JSON object
* */
function parseNativeOption(filterElem):INativeOptions{

  var nativeOptions : INativeOptions = new NativeOptions();

  if(!filterElem.struct || !filterElem.struct.var || !filterElem.struct.var || !filterElem.struct.var.struct ||
      !filterElem.struct.var.struct.var || !filterElem.struct.var.struct.var.length){

    return nativeOptions;
  }

  for(let options of filterElem.struct.var.struct.var){
    switch (options.name){
      case 'TYPE':
          nativeOptions.pb.type = getNativeType(options, nativeOptions.pb.type);
            break;
      case 'PUBLICATIONTYPE':
          nativeOptions.pb.publicationType = getPublicationType(options, nativeOptions.pb.publicationType);
            break;
      case 'AUDIENCE':
          nativeOptions.pb.audience = getAudience(options, nativeOptions.pb.audience);
            break;
    }
  }

  function getAudience(options, audience){
    let audienceLabelMap= {
      "GENERALPUBLIC" : 'generalPublic',
      "PROFESSIONAL" : 'professional'
    };

    if(options.struct.var && options.struct.var.length){
      for(let option of options.struct.var){
        audience[audienceLabelMap[option.name]] = option.string === "true" || option.string === true;
      }
    }

    return audience;
  }

  function getPublicationType(options, pubType){
    let pubTypeLabelMap= {
      "EDITED" : 'edited',
      "AUTHORED" : 'authored'
    };

    if(options.struct.var && options.struct.var.length){
      for(let option of options.struct.var){
        pubType[pubTypeLabelMap[option.name]] = option.string === "true" || option.string === true;
      }
    }

    return pubType;
  }

  function getNativeType(options, type){
    let labelMap = {
      'CONFERENCEPROCEEDINGS' :'conferenceProceedings',
      'HANDBOOKMANUAL' : 'handbookManual',
      'REFERENCEBOOK': 'referenceBook',
      'CLASSICBOOK' :'classicBook',
      'TEXTBOOKSTUDYGUIDE' : 'textbookStudyGuide'
    };

    if(options.struct.var && options.struct.var.length){
      for(let option of options.struct.var){
        type[labelMap[option.name]] = option.string === "true" || option.string === true;
      }
    }

    return type;
  }

  return nativeOptions;
}

/*
 * Parsing filter option based on the JSON object
 * */
function parseFilters(data){
  let filter: IFilter = new Filter();
  if(!data.struct || !data.struct.var || !data.struct.var.length){
    return filter;
  }

  for(let filterElem of data.struct.var){
    switch(filterElem.name){
      case 'DATERANGE':
          filter.dateRange = getDateRange(filterElem);
            break;
      case 'ONLYSHOW':
          filter.onlyShow = getOnlyShow(filterElem);
            break;
      case 'ARRFACETS':
          filter.facets = getFacets(filterElem);
            break;
    }
  }

  function getFacets(filterElem): IFacetFilter[]{
    var facets: IFacetFilter[] = [];
    var facetMap = {
      'F':"field",
      'V':"value"
    };

    if(!filterElem.array || !filterElem.array.struct){
      return facets;
    }

    if(filterElem.array.length == '1'){
      filterElem.array.struct = new Array(filterElem.array.struct);
    }

    for(let facetList of filterElem.array.struct){
      if (facetList && facetList.var && facetList.var.length) {
        let newFacet:IFacetFilter = {
          field: "",
          value: ""
        };

        for (let temp of facetList.var) {
          let key = facetMap[temp.name];
          newFacet[key] = temp.string;
        }

        facets.push(newFacet);
      }
    }

    return facets;
  }

  function getDateRange(filterElem) : IDateRange{
    var dateRange: IDateRange = {
      begin: 'eternity',
      end: 'eternity',
      field: 'PublicationYear',
      puby1: '*',
      yearRange: ''
    };

   // console.log("filterElem.struct:" + filterElem.struct);

    if (!filterElem.struct || !filterElem.struct.var || !filterElem.struct.var.length) {
      return dateRange;
    }

    for (let prop of filterElem.struct.var){
      let key = dateRangeLabelMap[prop.name];
      dateRange[key] = prop.string;
    }

    if (dateRange.field === 'ReleaseDate') {
       dateRange = fixReleaseDate(dateRange);
      }

    return dateRange;
  }

  function fixReleaseDate(dateRange): IDateRange {

    dateRange.field = 'PIReleaseDate';
    dateRange.begin = dateRange.begin.replace(/-/g,'');
    dateRange.end = dateRange.end.replace(/-/g,'');
    dateRange.puby1 = dateRange.puby1;
    dateRange.yearRange = dateRange.yearRange;

    return dateRange;
  }

  function getOnlyShow(filterElem): IOnlyShow{
    var onlyShow: IOnlyShow ={
      ftOnly: false,
      testAvailable: false,
      peerReviewed: false,
      latestUpdate: false,
      impactStatement: false,
      openAccess: false
    };

    if(!filterElem.struct || !filterElem.struct.var || !filterElem.struct.var.length){
      return onlyShow;
    }

    for (let prop of filterElem.struct.var){
      let key = onlyShowLabelMap[prop.name];
      onlyShow[key] = prop.string == '0' || prop.string == 'false' ? false : true ;
    }

    return onlyShow;
  }

  return filter;
}

exports.convertQuery = convertQuery;
