
const keyLabelMap_CR = {
  "criteria": "ARRCRITERIA",
  "limits": "ARRLIMITS",
  "filter": "FILTER",
  "displayOptions": "DISPLAYOPTIONS",
  "nativeOptions": "NATIVEOPTIONS",
  "databases": "DATABASES",
  "viewMode": "VIEWMODE",
  "index": "SOLRINDEX",
  "source": "SOURCE",
};

const criteriaKeyLabelMap_CR = {
  "field": "F",
  "operator": "O",
  "value": "V",
  "source": "SOURCE",
};

const limitKeyMapLabel_CR = {
  "field":"L",
  "operator":"LO",
  "values":"LV"
};

const sortKeyMap_CR = {
  field: "FIELD",
  direction: "DIRECTION"
};

export class KeyMapService{

  public static get_CR_Key( key: string ): string{
    return keyLabelMap_CR[key];
  }

  public static get_CR_CriteriaKey( key : string) : string {
    return criteriaKeyLabelMap_CR[key];
  }

  public static get_CR_LimitKey( key : string) : string {
    return limitKeyMapLabel_CR[key];
  }

  public static get_CR_SortKey( key : string) : string {
    return sortKeyMap_CR[key];
  }

  public static getNewOnlyShowObject(){
    return {
      $: { name: 'ONLYSHOW' },
      struct: {
        var: [
          {
            $: { name: 'TESTAVAILABLE' },
            string: 'false'
          },
          {
            $: { name: 'LATESTUPDATE' },
            string: 'false'
          },
          {
            $: { name: 'IMPACTSTATEMENT' },
            string: 'false'
          },
          {
            $: { name: 'FTONLY' },
            string: 'false'
          },
          {
            $: { name: 'OPENACCESS' },
            string: 'false'
          },
          {
            $: { name: 'PEERREVIEWED' },
            string: 'false'
          }
        ]
      }
    };
  }

  public static getNewDataRangeObject(){
    return {
      $: { name: 'DATERANGE' },
      struct: {
        var: [
          {
            $: { name: 'FIELD' },
            string: 'PublicationYear'
          },
          {
            $: { name: 'BEGIN' },
            string: 'eternity'
          },
          {
            $: { name: 'END' },
            string: 'eternity'
          },
          {
            $: { name: 'PUBY1' },
            string: '*'
          },
          {
            $: { name: 'YEARRANGE' },
            string: ''
          }
        ]
      }
    };
  }

  public static get_CR_OnlyShowKeyMap(){
    return {
      "peerReviewed": "PEERREVIEWED",
      "ftOnly": "FTONLY",
      "testAvailable": "TESTAVAILABLE",
      "latestUpdate": "LATESTUPDATE",
      "openAccess": "OPENACCESS",
      "impactStatement": "IMPACTSTATEMENT"
    };
  }

  public static get_CR_DateRangKeyMap(){
    return {
      "begin": "BEGIN",
      "end": "END",
      "field": "FIELD",
      "puby1": "PUBY1",
      "yearRange": "YEARRANGE"
    };
  }

  public static get_CR_NewNativeTypeObject(){
    return {
      $: { name: 'TYPE' },
      struct: {
        var: [
          {
            $: { name: 'HANDBOOKMANUAL' },
            string: 'false'
          },
          {
            $: { name: 'CLASSICBOOK' },
            string: 'false'
          },
          {
            $: { name: 'TEXTBOOKSTUDYGUIDE' },
            string: 'false'
          },
          {
            $: { name: 'REFERENCEBOOK' },
            string: 'false'
          },
          {
            $: { name: 'CONFERENCEPROCEEDINGS' },
            string: 'false'
          }
        ]
      }
    };
  }

  public static get_CR_NewNativePublicationTypeObject(){
    return {
      $: { name: 'PUBLICATIONTYPE' },
      struct: {
        var: [
          {
            $: { name: 'EDITED' },
            string: 'false'
          },
          {
            $: { name: 'AUTHORED' },
            string: 'false'
          }
        ]
      }
    };
  }

  public static get_CR_NewNativeOptionAudienceObject(){
    return {
      $: { name: 'AUDIENCE' },
      struct: {
        var: [
          {
            $: { name: 'PROFESSIONAL' },
            string: 'false'
          },
          {
            $: { name: 'GENERALPUBLIC' },
            string: 'false'
          }
        ]
      }
    };
  }

  public static get_CR_NativeTypeKeyMap(){
    return {
      'conferenceProceedings' : 'CONFERENCEPROCEEDINGS',
      'handbookManual' : 'HANDBOOKMANUAL',
      'referenceBook' : 'REFERENCEBOOK',
      'classicBook' : 'CLASSICBOOK',
      'textbookStudyGuide' : 'TEXTBOOKSTUDYGUIDE'
    };
  }

  public static get_CR_NativePublicationTypeKeymap (){
    return {
      'edited' : "EDITED" ,
      'authored' : "AUTHORED"
    };
  }

  public static get_CR_NativeAudienceKeyMap(){
    return {
      'generalPublic' : "GENERALPUBLIC",
      'professional' : "PROFESSIONAL"
    };
  }
}
