import _ = require('lodash');
var xml2js = require('xml2js');
var builder = new xml2js.Builder({rootName :'wddxPacket',xmldec :{'version': '1.0'},renderOpts : {pretty: false}});
//var inspect = require('eyes').inspector({maxLength: false});
import {Search} from "../search";
import {ICriteria, ILimit, ISortDescription, IDisplayOptions, INativeOptions, IFacetFilter, IDateRange, IOnlyShow, IFilter} from "../query/components/interfaces";
import {DisplayOptions} from "../query/components/displayOptions";
import {NativeOptions} from "../query/components/nativeOptions";
import {Filter} from "../query/components/filter";
import {PsycNETQuery} from "../query/psycNETQuery";
import {KeyMapService} from "./KeyMapService";

export class WddxCreator{

  private static _queryElements = ["source","index", "criteria", "limits", "databases", "filter", "nativeOptions", "viewMode", "displayOptions"];

  public static convertWDDXString(pnQuery: PsycNETQuery): string{
    var wddxString ='';

    var formattedObj = this._parsePsycNETQuery(pnQuery);
    var wddx = {
      header: '',
      data : {
        struct : formattedObj['struct']
      }
    };

    var xml = builder.buildObject(wddx);
    wddxString = xml.replace("<?xml version=\"1.0\"?>","");
    wddxString = wddxString.replace("<wddxPacket>","<wddxPacket version=\"1.0\">");

    return wddxString;
  }

  private static _parsePsycNETQuery(pnQuery: PsycNETQuery) : Object{
    var xml = {struct: {var: []}};

    for (let queryElem of this._queryElements) {
      if (pnQuery.hasOwnProperty(queryElem)) {
        let element = pnQuery[queryElem];
        let key = KeyMapService.get_CR_Key(queryElem);

        switch (key) {
          case 'SOLRINDEX' :
          case 'VIEWMODE':
          case 'SOURCE':
            xml.struct.var.push(this._formatStringElement(key, element));
            break;

          case 'DATABASES' :
            xml.struct.var.push(this._formatStringElement(key, element.toString()));
            break;

          case 'ARRCRITERIA' :
            xml.struct.var.push(this._formatCriteria(element));
            break;

          case 'ARRLIMITS' :
            xml.struct.var.push(this._formatLimits(element));
            break;

          case 'FILTER' :
            xml.struct.var.push(this._formatFilters(element));
            break;

          case 'NATIVEOPTIONS' :
            xml.struct.var.push(this._formatNativeOption(element));
            break;

          case 'DISPLAYOPTIONS' :
            xml.struct.var.push(this._formatDisplayOptions(element));
            break;
        }
      }
    }

    return xml;
  }


  private static _formatStringElement( key, value ){
    let stringObj = {
      $: {name: key},
      string: value
    };
    return stringObj;
  }

  private static _formatCriteria(allCriteria :ICriteria[]){
    var criteria = {
      $: { name: 'ARRCRITERIA' },
      array: {
        $: {length: allCriteria.length}
      }
    };

    if(allCriteria.length == 0){
      return criteria;
    }

    allCriteria.length == 1 ?  criteria.array['struct'] = { var : [] } : criteria.array['struct'] = [];

    for(let crit of allCriteria ){

      let newCriteria = {var :[]};
      for (let prop in crit){
        newCriteria.var.push(this._formatStringElement(KeyMapService.get_CR_CriteriaKey(prop), crit[prop]));
      }
      newCriteria.var.push(this._formatStringElement("ORIGIN", "true"));

      if(allCriteria.length == 1 ){
        criteria.array['struct']['var'] = newCriteria.var;
      }  else{
        criteria.array['struct'].push(newCriteria);
      }
    }

    return criteria;
  }

  private static _formatLimits(allLimits : ILimit[]){
    var limits = {
      $: { name: 'ARRLIMITS' },
      array: {
        $: {length: allLimits.length}
      }
    };

    if(allLimits.length == 0){
      return limits;
    }

    allLimits.length == 1 ?  limits.array['struct'] = { var : [] } : limits.array['struct'] = [];

    for(let limit of allLimits ){

      let newLimit = {var :[]};
      for (let prop in limit){
        let values = limit[prop];
        if(KeyMapService.get_CR_LimitKey(prop) == "LV"){
          values = limit[prop].toString();
        }
        newLimit.var.push(this._formatStringElement(KeyMapService.get_CR_LimitKey(prop), values));
      }

      if(allLimits.length == 1 ){
        limits.array['struct']['var'] = newLimit.var;
      } else {
        limits.array['struct'].push(newLimit);
      }
    }

    return limits;
  }

  private static _formatFilters(filterCondition : IFilter){
    var filters = {
      $: { name: 'FILTER' },
      struct: {
        var: []
      }
    };

    var dataRange = KeyMapService.getNewDataRangeObject();

    var onlyShow = KeyMapService.getNewOnlyShowObject();

    var facets = {
      $: { name: 'ARRFACETS' },
      array: {
        $: { length: filterCondition.facets.length }
      }
    };

    let newFilter = new Filter();

    for(var prop in filterCondition){
      if( JSON.stringify(newFilter[prop]) != JSON.stringify(filterCondition[prop])){

        switch(prop){
          case 'onlyShow' :
            let tempOnlyshow = this._updateFilters(filterCondition[prop], KeyMapService.get_CR_OnlyShowKeyMap());
            onlyShow.struct.var = tempOnlyshow.var;
            break;
          case 'dateRange' :
            let tempDataRange = this._updateFilters(filterCondition[prop], KeyMapService.get_CR_DateRangKeyMap());
            dataRange.struct.var = tempDataRange.var;
            break;
          case 'facets' :
            let tempFacts = this._updateFacetFilters(filterCondition[prop]);
            facets.array['struct'] = tempFacts['struct'];
            break;
        }
      }
    }

    filters.struct.var = [dataRange, onlyShow, facets];

    return filters;
  }

  private static _updateFilters(filter, keyMap){
    let newFilter = {var :[]};
    for(let property in filter){
      newFilter.var.push(this._formatStringElement(keyMap[property], filter[property]));
    }

    return newFilter;
  }

  private static _updateFacetFilters(allFacets : IFacetFilter[]){
    var facetsList = {};

    var facetMap = {
      "field":'F',
      "value":'V'
    };

    allFacets.length == 1 ?  facetsList['struct'] = { var : [] } : facetsList['struct'] = [];

    for(let facet of allFacets ){

      let newFacet = { var :[] };
      for (let prop in facet){
        newFacet.var.push(this._formatStringElement(facetMap[prop], facet[prop]));
      }

      if(allFacets.length == 1 ){
        facetsList['struct'].var = newFacet.var;
      }  else{
        facetsList['struct'].push(newFacet);
      }
    }

    return facetsList;
  }

  private static _formatDisplayOptions(displyCondn : IDisplayOptions){
    var displayOption = {
      $: {name: 'DISPLAYOPTIONS'},
      struct: {
        var: []
      }
    };

    var sortOption = {
      $: {name: 'SORT'},
      array: {
        $: {length: displyCondn.sort.length},
      }
    };

    var page = this._formatStringElement("PAGESIZE", displyCondn.pageSize);

    if(displyCondn.sort.length > 1){


      displyCondn.sort.length == 1  ?  sortOption.array['struct'] = { var : [] } : sortOption.array['struct'] = [];

      for(var sort of displyCondn.sort){
        let newSort = {var :[]};
        for (let prop in sort){
          newSort.var.push(this._formatStringElement(KeyMapService.get_CR_SortKey(prop), sort[prop]));
        }

        if(displyCondn.sort.length == 1 ){
          sortOption.array['struct']['var'] = newSort.var;
        }  else{
          sortOption.array['struct'].push(newSort);
        }
      }
    }

    displayOption.struct.var= [sortOption , page];

    return displayOption;
  }

  private static _formatNativeOption(nativeOptions : INativeOptions){
    var nativeOption = {
      $: {name: 'NATIVEOPTIONS'},
      struct: {
        var: {
          $: {name: 'PSYCBOOKS'},
          struct: {
            var: []
          }
        }
      }
    };

    var type = KeyMapService.get_CR_NewNativeTypeObject();
    var publicationType = KeyMapService.get_CR_NewNativePublicationTypeObject();
    var audience = KeyMapService.get_CR_NewNativeOptionAudienceObject();

    let newNativeOptions : INativeOptions = new NativeOptions();

    for(var prop in nativeOptions.pb){
      if( JSON.stringify(newNativeOptions.pb[prop]) != JSON.stringify(nativeOptions.pb[prop])){

        switch(prop){
          case 'type' :
            let tempType = this._updateNativeOptions(nativeOptions.pb[prop], KeyMapService.get_CR_NativeTypeKeyMap());
            type.struct.var = tempType.var;
            break;
          case 'publicationType' :
            let tempPublicationType = this._updateNativeOptions(nativeOptions.pb[prop], KeyMapService.get_CR_NativePublicationTypeKeymap());
            publicationType.struct.var = tempPublicationType.var;
            break;
          case 'audience' :
            let tempAudience = this._updateNativeOptions(nativeOptions.pb[prop], KeyMapService.get_CR_NativeAudienceKeyMap());
            audience.struct.var = tempAudience.var;
            break;
        }
      }
    }

    nativeOption.struct.var.struct.var = [type, publicationType, audience];

    return nativeOption;
  }

  private static _updateNativeOptions(nativeCondn, keyMap){
    let newNationOption = {var :[]};
    for(let property in nativeCondn){
      newNationOption.var.push(this._formatStringElement(keyMap[property], nativeCondn[property]));
    }

    return newNationOption;
  }
}
