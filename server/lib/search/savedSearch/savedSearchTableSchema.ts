var crypto = require('crypto');
import {Search} from "../search";
import {Identity} from '../../auth/identity/identity';
import {WddxService} from './WddxService';

export interface ISavedSearchTableSchema{
  SearchId : string;
  AsregID : number;
  OrgAsregID : number;
  WDDXSearchString : string;
  SearchName: string;
  FriendlyQuery : string;
  Databases : string;
  Frequency : string;
  TimeSaved : string;
  LastSent : string;
  notes : string;
  QueryHash : string;
  Notes2: string;
  QueryHash2: string;
}


export class SavedSearchTableSchema implements ISavedSearchTableSchema{

  SearchId : string;
  AsregID : number;
  OrgAsregID : number;
  WDDXSearchString : string;
  SearchName: string;
  FriendlyQuery : string;
  Databases : string;
  Frequency : string;
  TimeSaved : string;
  LastSent : string;
  notes : string;
  QueryHash : string;
  Notes2: string;
  QueryHash2: string;

  constructor( searchName : string, search : Search, identity:Identity){

    this.SearchId = search.id;
    this.AsregID = parseInt(identity.individual.id);
    this.OrgAsregID =  parseInt(identity.organization.id);
    this.WDDXSearchString = '';
    this.SearchName = searchName;
    this.FriendlyQuery = search.friendlyQuery;
    this.Databases = search.query.databases.toString();
    this.Frequency  = null;
    this.TimeSaved = new Date().toISOString();
    this.LastSent = null;
    this.notes = '';
    this.QueryHash = crypto.createHash('md5').update(JSON.stringify(search.query)).digest("hex");
    this.Notes2 = null;
    this.QueryHash2 = null;
  }
}
