import {IDisplayOptions, ISortDescription} from './interfaces';


export class DisplayOptions implements IDisplayOptions {
  pageSize: number = 25;
  sort: ISortDescription[] = [];

  constructor(){

  }

}
