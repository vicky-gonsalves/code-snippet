import {ICriteria} from './interfaces';

export class Criteria implements ICriteria {
  field: string = '';
  operator: string = '';
  value: string = '';
  source: string = 'user';

  constructor(field: string='AnyField', operator: string='AND', value: string='', source?: string){
    this.field = field;
    this.operator = operator;
    this.value = value;

    if(source){
      this.source = source;
    }

  }
}
