import {ILimit} from './interfaces';


export class Limit implements ILimit {
  field: string = '';
  operator: string = '';
  values: string[];

  constructor(field: string='', operator: string='IS', values: string[]=[]){
    this.field = field;
    this.operator = operator;
    this.values = values;
  }
}
