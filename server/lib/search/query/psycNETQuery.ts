import {IPsycNETQuery, ICriteria, ILimit, IFilter, IDisplayOptions, INativeOptions} from './components/interfaces';
import {Filter} from './components/filter';
import {DisplayOptions} from './components/displayOptions';
import {NativeOptions} from './components/nativeOptions';
import {SortDescriptionService} from './sortDescriptionService';

export class PsycNETQuery implements IPsycNETQuery {
  criteria: ICriteria[] = [];
  limits: ILimit[] = [];
  filter: IFilter = new Filter();
  displayOptions: IDisplayOptions = new DisplayOptions();
  nativeOptions: INativeOptions = new NativeOptions();
  databases: string[] = [];
  tab: string = 'all';
  source: string = '';
  viewMode: string = 'psycnet';
  index: string = 'psycnet';

  constructor(data?: IPsycNETQuery){
    if(data){
      this.criteria = data.criteria;
      this.limits = data.limits;
      this.filter = data.filter;
      this.displayOptions = data.displayOptions;
      this.nativeOptions = data.nativeOptions;
      this.databases = data.databases;
      this.tab = data.tab;
      this.source = data.source;
      this.viewMode = data.viewMode;
      this.index = data.viewMode;
    }
  }

}
