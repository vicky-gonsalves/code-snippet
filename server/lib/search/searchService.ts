import _ = require('lodash');
import {RequestHandler} from './requestHandler';
import {Search} from './search';
import {SearchResult} from "./searchResult";
import {PsycNETQuery} from "./query/psycNETQuery";
import {Identity} from "../auth/identity/identity";

var statsClient = require('../logs/statsClient');
//import StatsD = require('hot-shots');

//var StatsD = require('hot-shots'),
//    statsClient = new StatsD();



// Catch socket errors so they don't go unhandled, as explained
// in the Errors section below
//statsClient.socket.on('error', function(error) {
//  console.error("Error in socket: ", error);
//});



export class SearchService {

  static commandExecute(req, res, next, cb){
    SearchService.execute(req.search, (err, result) => {
      cb(null, result);
    });
  }

  static execute(search: Search, cb: Function){
    let startTime = new Date().getTime();

    // if load testing Node.js only, then skip the ML call
    if(search.metadata.bypassML === true){
      let fakedata = RequestHandler.getMLResponse();
      let searchResult: SearchResult = new SearchResult(fakedata, search, 0);

      return cb(null, searchResult);
    }

  //  statsClient.increment('apirequest.search.count');
    //statsClient.status();
    statsClient.increment('apirequest.search.count');

    RequestHandler.performRequest(search, (err, data) => {

      if (err) {
        return cb(err);
      }

      // calculate execution time
      let endTime = new Date().getTime();
      let executionTime = endTime - startTime;

      // set the total records found
      if(data.result && data.result.numFound){
        search.totalRecords = data.result.numFound;
      }

      let searchResult: SearchResult = new SearchResult(data, search, executionTime);

      statsClient.histogram('apirequest.search.time', executionTime);
     
      return cb(null, searchResult);
    });

  }

  static getProductTabCounts(search: Search, identity: Identity, cb: Function){
    let pnQuery = new PsycNETQuery(search.query);
    //pnQuery.databases = ["PI","PA","PB","PC","PE"]; // use databases from "search.query.databases"?
    pnQuery.databases = search.query.databases;
    pnQuery.tab = 'all';

    let hitcountSearch = new Search(pnQuery, {});
    hitcountSearch.type = 'tabCounts';

    // refresh product filter
    hitcountSearch.applyProductFilterByIdentity(identity);

    let startTime = new Date().getTime();

    RequestHandler.performRequest(hitcountSearch, (err, data) => {

      if (err) {
        return cb(err);
      }

      // calculate execution time
      let endTime = new Date().getTime();
      let executionTime = endTime - startTime;
      let searchResult: SearchResult = new SearchResult(data, hitcountSearch, executionTime);

      return cb(null, searchResult)
    });

  }

}
