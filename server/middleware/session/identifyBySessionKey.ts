
import {IdentityService} from "../../lib/auth/identity/identityService";
import {IdentityMockService} from "../../lib/auth/identity/identityMockService";
import {SessionManagement} from "../../lib/auth/sessionManagement";
import {CookieManager} from "../../lib/auth/cookieManager";
import {PublicURL} from "../../lib/utility/publicURL";
var config = require('../../config/settings');

/**
 * Middleware to identify the user by session cookie.
 * Note: This will only execute if the user is developing locally
 */

exports.mw = function(req, res, next) {
  return function(req, res, next) {
    req.cookies = req.cookies || {};

    // if the user is load testing, then use a mock identity
    if(req.get('bypassML')){
      let identityMockService = new IdentityMockService();
      let identity = identityMockService.createIdentity('329008', '12345',['PI','PB','PA','PC','PE','PT','PH','ASC'], []);
      identity.recordClientIP(req.clientIp);

      // save to session
      req.session.identity = identity;
      // save session starting time, to get confirmation on working for long time
      req.session.sessionConfirmationTime = new Date().getTime();
      req.session.recentSearch = [];

      // use a mock purchase cart
      req.session.cartItems = [];

      // apply the host name for ecommerce for this environment
      CookieManager.setCookies(req, res);

      req.session.save( err => { return next() });

    } else {
      // Identity by Erights cookie

      // if user is developing locally, then skip this middleware check
      /*
      if( req.isDevelopingLocally || req.xhr || req.url !== '/'){
        return next();
      }
      */

      if( req.isDevelopingLocally ){
        return next();
      }

      let sessionKey = req.cookies.ERIGHTS;
      //  let sessionKey = "KN5ix2FJ0a0KWGIjzjABobvjsONsdZUZm8-18x2dzdx2B6SkYRRMVTPTpUU0DGKQx3Dx3DrKndHREBkIuF3j4iWx2FExxLQx3Dx3D-8GAwmzaO9mbx2Fi0l76LNOwQx3Dx3D-TYNEYWLT7OCKvCXA5VQx2BTgx3Dx3D";
      let refreshSession = (req.cookies.REFRESH_SESSION && req.cookies.REFRESH_SESSION == 'true');

      //console.log('req.cookies: ', req.cookies);
      //console.log('refreshSession: ', refreshSession);

      // session is established and there is no need to refresh it, so exit out of function
      if( req.session.hasOwnProperty('identity') && !refreshSession ) {
        //console.log('User has existing session');
        return next();
      } else if(sessionKey){
        // user needs to have their session created/updated
        //console.log('User has session key to login');
        createIdentityFromSessionKey(req, res, next);
      } else {
        handleNoSession(req, res, next);
        //console.log('No session key defined, continuing onto next process.');
        //return next();
      }

    }

  };
};

// creates an identity based on session key
function createIdentityFromSessionKey(req, res, next){
  let sessionKey = req.cookies.ERIGHTS;
  //let sessionKey = "KN5ix2FJ0a0KWGIjzjABobvjsONsdZUZm8-18x2dzdx2B6SkYRRMVTPTpUU0DGKQx3Dx3DrKndHREBkIuF3j4iWx2FExxLQx3Dx3D-8GAwmzaO9mbx2Fi0l76LNOwQx3Dx3D-TYNEYWLT7OCKvCXA5VQx2BTgx3Dx3D";


  IdentityService.createIdentityFromSessionKey(sessionKey, function(err, identity){
    if(err) {
      console.log('Identity error occurred');
      return next(err);
    }

    // save user's identity to session
    if( identity ){

      //save user's IP
      identity.recordClientIP(req.clientIp);

      console.log("User has valid session, so I'm saving it");

      // mark REFRESH_SESSION cookie as false so that subsequent requests don't establish a new session
      res.cookie('REFRESH_SESSION',"false",{ domain: '.apa.org'});

      // apply the host name for ecommerce for this environment
      CookieManager.setCookies(req, res);

      // save identity to session
      req.session.identity = identity;

      // save session starting time, to get confirmation on working for long time
      req.session.sessionConfirmationTime = new Date().getTime();

      // set initial recent searches to an empty array
      if(!req.session.hasOwnProperty('recentSearch')){
        req.session.recentSearch = [];
      }

      req.session.save(function(){
        console.log('Session saved, continuing onto next process');
        return next();
      });

    } else {
      /*
       sometimes a user's might have a session established, but it might be stale of invalid.
       this can occur for APA users who login to production and get a cookie, but then try to
       access QA.  Since they have a cookie, an attempt is made to get their identity, but since
       it's different environments, the session does not come back valid.  For these type of events
       we want to clear the existing cookies and restart the login process.
       */
      console.log("Invalid session, so we must redirect");
      SessionManagement.expireAndRedirect(req, res);
    }

  });
}

function handleNoSession(req, res, next){

  // let the server status check and logout go through
  if(PublicURL.isPublic(req.url)){
    return next();
  }

  let redirectUrl = '/user/login';

  // don't redirect for API calls
  if(req.url && req.url.indexOf('/api/') === -1) {
    redirectUrl = redirectUrl + '?returnUrl='+req.url;
  }

  console.log('User does not have a session: ', req.url, redirectUrl);

  return res.redirect(redirectUrl);
}

