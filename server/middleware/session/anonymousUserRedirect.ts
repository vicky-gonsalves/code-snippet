
import {IdentityService} from "../../lib/auth/identity/identityService";
import {IDEMService} from "../../lib/auth/IDEMService";
import {PublicURL} from "../../lib/utility/publicURL";

/**
 * Middleware to identify if the user is an anonymous user and determine if they should be redirected to the login page
 * for the initial request
 */

exports.mw = function(req, res, next) {
  return function(req, res, next) {

    if(PublicURL.isPublic(req.url)){
      return next();
    }

    // if this is a scheduled task, then don't redirect for login
    if(req.url.indexOf('/jobs/') >= 0){
      req.session.identity.shouldShowLogin = false;
    }

    // redirect to the login page if the user has not been shown the login page
    if(req.session.hasOwnProperty('identity') && req.session.identity.shouldShowLogin && IdentityService.hasGuestIdentity(req)){
      req.session.identity.shouldShowLogin = false;

      req.session.save(function(){
        console.log('Redirecting for anonymous user login');
        let returnUrl = req.url || '';
        let redirectTo = IDEMService.getIndividualLoginURL(returnUrl);
        res.redirect(redirectTo);
      });

    } else {
      return next();
    }

  };
};
