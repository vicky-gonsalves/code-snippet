/**
 * Middleware to identify the user by asregId.
 * Note: This will only execute if the user is developing locally
 */
import {IdentityMockService} from '../../lib/auth/identity/identityMockService';
import {CookieManager} from '../../lib/auth/cookieManager';
import {MockProfileService} from "../../lib/personalization/mockProfileService";
import {ProfileService} from "../../lib/personalization/profileService";
var config = require('../../config/settings');

exports.mw = function(req, res, next) {
  return function(req, res, next) {

    // Apply identity if accessing the root URL to save cycles
    /*
    if(req.originalUrl !== '/' || req.session.hasOwnProperty('identity')){
      return next();
    }

    */

    if(req.session.hasOwnProperty('identity')){
      return next();
    }

    // create mock user
    let identityMockService = new IdentityMockService();
    //let identity = identityMockService.createIdentity('329008', '12345',['PI','PB','PA','PC','PE','PT','PH','ASC'], []);

    // APA LAN for testing PNPROD-21
    //let identity = identityMockService.createIdentity('329008', '328833',["ASC", "PI", "PA", "PB", "PC", "PE", "PT", "PH", "PBC2010", "PBC2011", "PBC2012", "PBC2015", "NCHBELEC", "JJHBELEC", "MNHBELEC", "HMHBELEC", "CGHBELEC", "FRHBELEC", "CRHBELEC", "PSHBELEC", "MCHBELEC", "SXHBELEC", "IOHBELEC", "ETHBELEC", "EDHBELEC", "RMHBELEC", "ADHBELEC", "COHBELEC", "BAHBELEC", "RSHBELEC", "TAHBELEC"], []);
    let identity = identityMockService.createIdentity('329008', '328833',["PI","PA","PB","PC","PE","PT","PH","CRD","PSB","MON", "PBC2010", "PBC2011", "PBC2012", "PBC2013", "PBC2014", "PBC2015","NCHBELEC","JJHBELEC", "MNHBELEC","HMHBELEC","CGHBELEC","FRHBELEC","CRHBELEC","PSHBELEC","MCHBELEC","SXHBELEC","IOHBELEC","ETHBELEC","EDHBELEC","RMHBELEC","ADHBELEC","COHBELEC","BAHBELEC","RSHBELEC","TAHBELEC","CTVINTRO"], []);

    //let identity = identityMockService.createIdentity('-1', '12345',['PI'], ['2016-36829-001','2016-27724-001','2016-39792-002']);

    // Test user for PNTHREEX-1908
    //let identity = identityMockService.createIdentity('329008', '12345',["PI","PA","PB","PC","PE",'PT','PSB',"PBC2010",'PBC2011','PBC2012','PBC2015','NCHBELEC','JJHBELEC','MNHBELEC','HMHBELEC','CGHBELEC','FRHBELEC','CRHBELEC','PSHBELEC','MCHBELEC','SXHBELEC','IOHBELEC','ETHBELEC','EDHBELEC','RMHBELEC','ADHBELEC','COHBELEC','BAHBELEC','RSHBELEC','TAHBELEC'], []);

    // NOTRE DAME mock user
    //let identity = identityMockService.createIdentity('329008', '12345',['ADHBELEC', 'BAHBELEC', 'ETHBELEC', 'PAC', 'PBC2008', 'PBC2009', 'PBC2010', 'PBC2011', 'PBC2012', 'PBC2013', 'PBC2014', 'PBC2015', 'PBC2016', 'PBC2017', 'PT', 'RMHBELEC', 'TAHBELEC'], []);

    // Test user for PNSUP-27
    //let identity = identityMockService.createIdentity('697723', '9201636',['PI','PA'], []);

    // set a mock profile
    //identity.profile = MockProfileService.getProfile2();


    identity.recordClientIP(req.clientIp);

    //set athens
    //identity.athens = true;

    //console.log('*** MOCK IDENTITY CREATED ***', identity);

    // save to session
    req.session.identity = identity;
    // save session starting time, to get confirmation on working for long time
    req.session.sessionConfirmationTime = new Date().getTime();
    req.session.recentSearch = [];

    // use a mock purchase cart
    req.session.cartItems = [];

    // apply the host name for ecommerce for this environment
    CookieManager.setCookies(req, res);

    req.session.save( err => { return next() });
  };
};
