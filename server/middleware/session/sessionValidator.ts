const allowedSessionTime = 180; // 180 mins

const millisecond = 60000;

export class SessionValidator{

  public static validateUserSessionTime(req, res, next){

    //disables this for now until we look at the security.  This was incorrectly killing sessions when multiple tabs were open
    next();
    /*

    if(req.hasOwnProperty("session") && req.session.hasOwnProperty('sessionConfirmationTime')){
      let currentTime = new Date().getTime();
      let startTime = req.session['sessionConfirmationTime'];

      let diff = (currentTime - startTime) / millisecond;
      //console.log("\ndiff", diff);
      if(allowedSessionTime > diff){
        next();
      } else {
        res.status(401).send({confirmSession: true });
      }

    } else {
      next(new Error("Session not established!"));
    }

    */
  }
}
