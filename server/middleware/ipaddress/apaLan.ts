/**
 * Middleware to mark in req scope if user is developing locally
 */

var ipUtility = require('../../lib/utility/ipUtility');

exports.mw = function(req, res, next) {
  return function(req, res, next) {
    req.isOnAPALan = ipUtility.isOnAPALan(req);

    return next();
  };
};
