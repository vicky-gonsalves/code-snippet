/**
 * Middleware to mark in req scope if user is developing locally
 */

var ipUtility = require('../../lib/utility/ipUtility');

exports.mw = function(req, res, next) {
  return function(req, res, next) {
    req.isDevelopingLocally = (process.env.NODE_ENV !== 'production' && ipUtility.isLocalHost(req));

    return next();
  };
};
