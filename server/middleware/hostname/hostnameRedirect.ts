var config = require('../../config/settings');

/**
 * Identifies if the request hostname is content.apa.org or doi.apa.org and then
 * it redirects the user to psycnet.apa.org but preserves the path.
 */

exports.mw = function(req, res, next) {
  return function(req, res, next) {
    let hostname = req.headers['x-forwarded-host'];

    if(config.isDeveloping){
      return next();
    }

    //Check if the request is coming from something other than psycnet.apa.org and redirect accordingly
    if(req.method === "GET" && isUsingAlternatePsycNETHostname(hostname)){
      let originalPath = hostname + req.originalUrl;
      let redirectTo = getRedirectUrl(req);

      console.log('Redirecting from: ', originalPath);
      console.log('Redirecting to: ', redirectTo);

      return res.redirect(redirectTo);
    }

    return next();
  };
};

function isUsingAlternatePsycNETHostname(hostname){
  // regular expression that looks for psycnet.apa.org or a variation there of due to proxy servers
  let regExHostname = '(contentdev[-.]apa[-.]org|content[-.]apa[-.]org|contenttest[-.]apa[-.]org|doidev[-.]apa[-.]org|doitest[-.]apa[-.]org|doi[-.]apa[-.]org)';
  let match = hostname.match(new RegExp(regExHostname, 'gi')) || [];

  return (match.length > 0);
}

function getRedirectUrl(req){
  let fullUrl = 'http://' + config.web.hostname + req.originalUrl;
  return fullUrl;
}
