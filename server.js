
var path = require('path'),
  express = require('express'),
  environment = require('./server/config/environment'),
  selfCheckService = require('./server/lib/utility/selfCheckService'),
  config = require('./server/config/settings');


const app = express();

//app.use(morgan('combined'));

// initialize apiRequest service
var apiRequestService = require('./server/lib/apiRequest/apiRequestService');
apiRequestService.init({
  // define path to request definitions
  pathToDefinitions: path.resolve(__dirname, 'server', 'config', 'requests'),

  // apply application specific connection information
  defaults:{
    webservice: {
      server: config.marklogic
    }
  }
});

// initialize connection to datadog via hot-shots/statsClient
var StatsService = require('./server/lib/logs/statsClient');

StatsService.init();

//StatsService.check('service.up');






// Application middleware and environment configuration
environment(app, express, __dirname);


// start node server and listen for requests
var server = app.listen(config.web.port, function onStart(err) {
  if (err) {
    console.log(err);
  }

  console.info('==> 🌎 Listening on port %s. \n\n', config.web.port);

  console.log('-------------------------------------------');
  console.log('Connected to the following resources:');
  console.log('Erights: %s', config.erights.host);
  console.log('Redis: %s:%s', config.redis.host, config.redis.port);
  console.log('MarkLogic: %s:%s', config.marklogic.hostname, config.marklogic.port);
  console.log('Static Assets: %s',  config.web.staticAssets);
  console.log('-------------------------------------------\n\n');

  // Perform services' self-check.
  selfCheckService.performCheck();
});
