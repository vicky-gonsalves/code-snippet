/*
 * Angular 2 decorators and services
 */
import {Component, ViewContainerRef, ViewChild, Renderer} from '@angular/core';

// identity management
import {CookieService} from 'angular2-cookie/core';
import {Router, NavigationEnd} from '@angular/router';
import {IdentityService} from './core/services/identity/identityService';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import {Observable} from 'rxjs/Observable';

import {environment} from '../environments/environment';

import {ApiRequestService} from './core/services/apiRequestService';
import {SessionWarningDialog} from './core/session/sessionWarning/sessionWarningDialog';
import {SessionExpiredDialog} from './core/session/sessionExpire/sessionExpiredDialog';
import {IPAddressChangeDialog} from './core/session/ipAddressChangeDialog/ipAddressChangeDialog';
import {DownloadBlockMessage} from './core/session/downloadBlockMessage/downloadBlockMessage';
import {MenuToggleNotifier} from './core/services/menuToggleNotifier';

import { ComingSoonModal } from './volatile/comingSoonModal/comingSoonModal';

declare const gtm: Function;

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  host: {
    // register lister events to manage session timeout when multiple tabs are open
    '(document:mousedown)': 'updateAccessTime($event)',
    '(document:touchstart)': 'updateAccessTime($event)'
  }
})

export class AppComponent {

  accessTime = null;
  viewContainerRef;
  routeEventSubcription: any;
  apiErrorSubscription: any;
  menuToggleSubscription: any;
  isWarningDialogClosed;

  comingSoonSubject: string;
  comingSoonSource: string;

  @ViewChild('sessionWarningDialog') sessionWarningDlg: SessionWarningDialog;
  @ViewChild('sessionExpiredDialog') sessionExpireDlg: SessionExpiredDialog;
  @ViewChild('ipAddressChangeDialog') ipAddressChangeDlg: IPAddressChangeDialog;
  @ViewChild('downloadBlockMessage') downloadBlockMessage: DownloadBlockMessage;

  @ViewChild('comingSoonModal') comingSoonModal: ComingSoonModal;

  constructor (
               viewContainerRef:ViewContainerRef,
               private _router: Router,
               private cookieService:CookieService,
               private identityService: IdentityService,
               private idle: Idle,
               private keepalive: Keepalive,
               private _renderer: Renderer,
               private _menuToggle: MenuToggleNotifier,
               private apiService:ApiRequestService
          ) {
    // Setup (Google Tag Manager) using the environment's (gtm) setting.
    gtm(environment.gtm);

    // You need this small hack in order to catch application root view container ref
    this.viewContainerRef = viewContainerRef;

    // manage user session
    this.startIdleWatch();
  }


  ngOnInit() {
      this.identityService.populateIdentity();

      this.menuToggleSubscription = this._menuToggle.toggle$.subscribe(toggleOn => this.toggleMobileMenu(toggleOn));

      this.apiErrorSubscription = this.apiService.error.subscribe(err => {
        if(err === 'IP Address Error') {
          if(!this.ipAddressChangeDlg.isOpen()) {
            this.closeModals();
            this.ipAddressChangeDlg.openModal();
          }
        } else if (err === 'Exceeded the number of downloading') {
          if(!this.downloadBlockMessage.isOpen()) {
            this.closeModals();
            this.downloadBlockMessage.openModal();
          }
        }
      });

      // when the user's session is expired and they return, force them to do a page refresh
      this.sessionExpireDlg.sessionExpireModal.onHide.subscribe(() => window.location.href = "/user/logout");

      // When the (IP Address) changes, go to the home page upon closing the dialog.
      this.ipAddressChangeDlg.ipAddressChangedModal.onHide.subscribe(() => window.location.href = "/");

      this.routeEventSubcription = this._router.events.filter(evt => evt instanceof NavigationEnd).subscribe((navEvent) => {

        // if the new URL is not a page anchor, then scroll to the top
        if(navEvent.url.indexOf('#') === -1){
          document.body.scrollTop = 0;
        }
      });
  }

  showComingSoonMessage($event) {
    this.comingSoonSubject = $event.title || '';
    this.comingSoonSource = $event.source || '';
    this.comingSoonModal.openModal();
  }

  closeModals(){
    if(this.sessionWarningDlg.isOpen()){
      this.sessionWarningDlg.closeModal();
    }

    if(this.sessionExpireDlg.isOpen()){
      this.sessionExpireDlg.closeModal();
    }

    if(this.ipAddressChangeDlg.isOpen()){
      this.ipAddressChangeDlg.closeModal();
    }

    if(this.comingSoonModal.isOpen()) {
      this.comingSoonModal.closeModal();
    }
  }

  checkWarningDialogStatus(isClosed) {
    this.isWarningDialogClosed = isClosed;
  }

  startIdleWatch(){
    this.updateAccessTime();
    this.registerSessionKeepalive();
    this.registerIdleTime();
    this.listenToUserEvents();

    // start watching for idleness right away.
    this.idle.watch();
  }

  private registerSessionKeepalive(){
    this.keepalive.request('/user/keepalive');
    this.keepalive.interval(60);
    this.keepalive.start();
  }

  private registerIdleTime(){
    //sets an idle timeout of 25 minutes
    this.idle.setIdle(1500);

    // sets a timeout period of 5 minutes. after 25 minutes of inactivity, the user will be considered timed out.
    this.idle.setTimeout(300);
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
  }

  private listenToUserEvents(){

    // warn the user after 25 minutes of inactivity
    this.idle.onIdleStart.subscribe(() => {
      this.closeModals();
      this.sessionWarningDlg.openModal();
    });

    // when the user resumes, close the warning and start watching for idleness again
    if(this.isWarningDialogClosed) {
       this.closeModals();
    }

    // when the user has been inactive for 30 minutes, expire their session
    this.idle.onTimeout.subscribe(() => {
      this.closeModals();

      if(this.isActiveInstance()){
        this.keepalive.stop();
        this.sessionExpireDlg.openModal();

        let qry = this.apiService.createQuery('session.expire');
        this.apiService.execute(qry).subscribe(() => {});
      }

    });
  }

  // sets a cookie value for access time that is shared between browser instances.
  // also sets a local copy for comparison and determining which is the active tab.
   private updateAccessTime(){
    let now = new Date();
    let time = now.getTime().toString();
    this.cookieService.put('PN_ACCESSTIME', time);
    this.accessTime = time;
  }

  // does a comparision of the local time vs cookie time to determine if this is the active tab/browser
  isActiveInstance(){
    return parseInt(this.accessTime) >= parseInt(this.cookieService.get('PN_ACCESSTIME'));
  }

  toggleMobileMenu(turnOn: boolean) {
    let rootElement = this.viewContainerRef.element.nativeElement;

    // Full-screen menus are used with 'xs', 'sm' and 'md' Bootstrap layouts (Maximum width = 991px).
    // So the 'lockMainScroll' class should work only for these displays.

    /*
    This version uses (Renderer) class, which in turn is deprecated as of Angular 4 API Documentation.
    The suggested alternative is to use the (Renderer2) class, which is, at the time of writing these lines, experimental and not part of Angular 2.
    If there is any intention to upgrade Angular from v2 in the future, the recipe should be something like the following:

      if(turnOn) {
        this._renderer2.addClass(rootElement, 'lockMainScroll');
      } else {
        this._renderer2.removeClass(rootElement, 'lockMainScroll');
      }

    */
    this._renderer.setElementClass(rootElement, 'lockMainScroll', turnOn);
  }

  ngOnDestroy() {
    this.menuToggleSubscription.unsubscribe();
    this.routeEventSubcription.unsubscribe();
    this.apiErrorSubscription.unsubscribe();
  }

}
