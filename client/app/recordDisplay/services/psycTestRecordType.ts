import {IRecord} from "./format/IRecord";

export class PsycTestRecordType{

  static childSearch = {};

  constructor(){

  }

  static addChildRecord(record: IRecord): void{
    this.childSearch[record.UID] = record;
  }

  static isRecordAvailable(uid) : boolean{
    return !!this.childSearch[uid];
  }

  static getChildRecord(uid) : IRecord{
    return this.childSearch[uid];
  }

}
