import {Injectable} from '@angular/core';

@Injectable()
export class ReviewedItemFormatter {

  public static format(items: string[]) {
    let result = [];
    let item = {};

    for(let i = 0; i < items.length; i++) {
      item = this.applyReviewedItemPairs(items[i]);
      result.push(item);
    }

    return result;
  }

  // A reviewed items list in the format of [1]value[2]value[3]value, etc.
  private static splitReviewedItems(str):string[] {
    let items = str.replace(/(\[[0-9]{1,2}\])/g,'~');
    let result = items.split('~');
    return result;
  }

  // converts a string of "A string of reviewed item pairs. e.g., 'Title: The Cunning of Unreason: Making Sense of Politics.|Other Info: London: HarperCollins.|Year: 2000" into a structure
  private static applyReviewedItemPairs(str) {
    let reviewedItem = {};
    let reviewedList = this.splitReviewedItems(str).filter(String);
    for(let item of reviewedList) {
      let pairs = item.split('|');
      let pair = [];
      let field = '';
      let value = '';

      for(let i=0; i< pairs.length; i++) {
        pair = pairs[i].split(/:(.+)/).filter(String);

        if(pair.length === 2) {
          field = pair[0].trim();
          if (field.match(/\[\d\]/g)) {
            field = field.replace(/\[\d\]/g,'');
          }
          if (field === 'Author') {
              field = 'Author(s)';
          }

          value = pair[1].trim();
          if(!value.length) {
            continue;
          }
          
          reviewedItem[field] = value;
        }
      }
    }
    return reviewedItem;
  }
}
