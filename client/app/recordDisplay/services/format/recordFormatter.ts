import {Injectable} from '@angular/core';
import {IRecord} from './IRecord';
import {HighlightService} from '../../../shared/highlight/highlightService';
import {ProductLabels} from '../../../shared/productLabels/productLabels';
import {ReviewedItemFormatter} from "./ReviewedItemFormatter";
import {RelatedUIDFormatter} from '../../../shared/relatedUIDFormatter/relatedUIDFormatter'
import {DOIExtractor} from '../../../shared/doiExtractor/doiExtractor'

@Injectable()
export class RecordFormatter {

  static convertToPNRecord(data: any): IRecord {
    let record: IRecord = {};
    let noHighlightFields = ['BookCollection','IsOFP','HasOpenAccess'];
    let fieldsWithSemiColonDelimiter = ['ISSN','ISBN','SetISBN','KeyConcepts','PopulationGroup','Methodology','AgeGroup','Location','FormatAvailable','FormatCovered'];

    if(this.has(data, 'UID')){

      record.UID = data.UID;

      // remove the highlight so we can use this property for link construction
      record.UIDNoHighlight = HighlightService.removeHighlights(data.UID);
    }

    if(this.has(data, 'ProductCode')) {
      record.ProductLabel = ProductLabels.getLabel(data.ProductCode);
    }


    if(data.Abstract){
      record.Abstract = this.normalizeLanguageOptions(data.Abstract);
    }

    if(this.has(data, 'ImpactStatement')){
      record.ImpactStatement = this.normalizeLanguageOptions(data.ImpactStatement);
    }

    if(this.has(data, 'PDFLink')){

      console.log(data.PDFLink);
      console.log(data.ProductCode);
      switch(data.ProductCode) {
        case 'PA':
          record.PDFLink = '/journal/'+ data.PDFLink;
          break;

        case 'PB':
          record.PDFLink = '/book/'+ data.PDFLink;
          break;

        default:
          record.PDFLink = data.PDFLink;
      }
      record.PDFLink = HighlightService.removeHighlights(record.PDFLink);
      console.log(record.PDFLink);
    }

    if(this.has(data, 'XMLLink')){
       console.log(data.XMLLink);
      console.log(data.ProductCode);
      switch(data.ProductCode) {
        case 'PA':
          record.XMLLink = '/journal/'+ data.XMLLink;
          break;

        case 'PC':
          record.XMLLink = '/critique/'+ data.XMLLink;
          break;

        default:
          record.XMLLink = data.XMLLink;
      }
      record.XMLLink = HighlightService.removeHighlights(record.XMLLink);
      console.log(record.XMLLink);
    }


    // format authors
    if(this.has(data, 'AuthorName')){
      record.Authors = data.AuthorName;
      record.AuthorsList = data.AuthorName.join('; ');
    }

    // format title
    if(this.has(data, 'GivenDocumentTitle')){
      record.Title = HighlightService.removeHighlights(data.GivenDocumentTitle);
    }

    // apply Book Title
    if(this.has(data, 'BookTitle')
      && this.has(data, 'DocumentType')
      && this.has(data, 'ProductCode')
      && data.DocumentType !== 'Chapter'
      && ['BC','PB'].indexOf(data.ProductCode) >= 0){

      record.Title = data.BookTitle;
    }

    if(this.has(data, 'SourcePE')) {
      record.SourcePE = data.SourcePE.replace(/\|/g, '; ') + '.';
    }

    // format the record source
    if(this.has(data, 'SourcePI')){
      record.Source = this.formatSource(data.SourcePI, data.SourcePE);
    }

    if(this.has(data, 'SourcePIWithAuthorIDs')){
      record.SourcePIWithAuthorIDs = this.formatSource(data.SourcePIWithAuthorIDs, data.SourcePE);
    }

    if(this.has(data, 'PublicationHistory')){
      record.PublicationHistory = this.formatPublicationHistory(data.PublicationHistory[0]);
    }

    if(this.has(data, 'TestNameSource')){
      record.TestNameSource = 'Test name created by PsycTESTS.';
    }

    if(this.has(data, 'TestYear') && data.TestYear === 'No year specified'){
      record.TestYear = '(N.D.)';
    }

    // change the no year value within the APA Source.  This data will automatically be copied to the record at the end.
    if(this.has(data, 'SourceAPA') && data.SourceAPA.indexOf('No year specified') >= 0){
      data.SourceAPA = data.SourceAPA.replace('(No year specified)', '(N.D.)');
    }

    //Remove DOI and trim whitespaces
    if (this.has(data, 'SourceAPA')) {
      data.SourceAPAWithoutDOI = data.SourceAPA.replace(/doi:*.*?(?=<)/g, '').trim();
    }

    // format classification codes
    if (this.has(data, 'Classification')) {
      record.Classification = this.formatClassifications(data.Classification);
    }

    // format conference data
    if (this.has(data, 'ConferenceInfo') || this.has(data, 'ConferenceNote')) {
      record.Conference = this.formatConferenceData(data.ConferenceInfo, data.ConferenceNote);
    }

    // format reviewed item
    if (this.has(data, 'ReviewedItemOrig') && data.ReviewedItemOrig.length && data.ReviewedItemOrig[0].length) {
      record.ReviewedItems = ReviewedItemFormatter.format(data.ReviewedItemOrig);
    }

    if (this.has(data, 'IndexTerms')) {
      record.FormattedIndexTerms = this.formatIndexTerms(data.IndexTerms[0]);
    }

    // TODO: format grant sponsorship
    if (this.has(data, 'SponsorList') && data.SponsorList.length) {
      record.SponsorList = this.formatGrantSponsorship(data.SponsorList);
    }

    // TODO: format clinical trial number
    if (this.has(data, 'ClinicalTrialNumber')) {
      record.ClinicalTrialNumber = this.formatClinicalTrialNumber(data.ClinicalTrialNumber);
    }

    if (this.has(data, 'DissertationDetails')) {
      record.DissertationDetails = this.formatDissertationDetails(data.DissertationDetails);
    }

    if (this.has(data, 'TOC')) {
      record.TOC = this.formatTOC(data.TOC);
    }

    // iterate over the non highlighted fields and save to the record
    noHighlightFields.forEach(field => record[field] = HighlightService.removeHighlights(data[field]));

    // iterate over fields that have a semi-colon delimiter
    fieldsWithSemiColonDelimiter.forEach(field => {
      if(data.hasOwnProperty(field)){
        let str = data[field];
        record[field] = str.join('; ')
      }
    });

    // iterate over the result set one more time and save the data 'as-is' if not already saved/manipulated above
    for(let prop in data){
      if(data.hasOwnProperty(prop) && !record.hasOwnProperty(prop)){
        record[prop] = data[prop];
      }
    }

    //separate doi and text
    if(this.has(data, 'Instrumentation')) {
      record.Instrumentation = data.Instrumentation.map(str=> DOIExtractor.extract(str));
    }

    if(this.has(data, 'OtherPopulationDetails')) {
      let detailsList = [];
      for(let detail of data.OtherPopulationDetails) {
        detailsList.push(detail.split(': '));
      }
      record.OtherPopulationDetails = detailsList;
    }

    // indicate if the record is a PsycTESTS record.  This is a helper variable because the view will change depending if the record is a PsycTEST
    record.isPT = (record.ProductCode === "PT");

    // format the test title that is displayed at the top
    if(record.isPT){
      record.TestTitleForDisplay = this.formatTestTitle(record.TestTitle, record.Acronyms, record.TestYear);
    }

    return record;
  }

  private static has(data: any, prop: string) {
    return data.hasOwnProperty(prop) && data[prop] && data[prop].length;
  }

  // Some records have multiple languages for abstracts and some do not.  This normalizes all records to use a standard format no matter what type
  // of record is requested.
  private static normalizeLanguageOptions(data: any):{} {
    let languageOptions = {};

    // implemented linkify UIDs: RelatedUIDFormatter.linkRelatedUIDs

    if(typeof data === 'object'){
      for(let prop in data){
        if(data.hasOwnProperty(prop)){
          languageOptions[prop] = RelatedUIDFormatter.linkRelatedUIDs(data[prop]);
        }
      }
    }else{
      languageOptions['English'] = RelatedUIDFormatter.linkRelatedUIDs(data);
    }

    return languageOptions;
  }

  private static formatSource(sourcePI: string, sourcePE: string):string {
    let source = sourcePI;

    if(sourcePE && sourcePE.length){
      source = source + sourcePE.replace(/\|/g, '; ');
    }

    // add period at end
    if(source.length && source.charAt(source.length-1) !== '.'){
      source = source + '.';
    }

    return source;
  }

  private static formatPublicationHistory(publicationHistory: string) {
    let result = [];
    let arrHistory = publicationHistory.split('|');

    arrHistory.forEach(history => {
      let historyItems = history.split(':');

      if(historyItems.length === 2){
        result.push({
          description: historyItems[0].trim(),
          date: historyItems[1].replace(/[ ]{2},/g, ',').trim()
        })
      }
    });

    return result;
  }

  private static formatClassifications(classifications: string[]) {
    let result = [];

    classifications.forEach(item => {
      let splitClassifications = item.split('; ');
      splitClassifications.forEach(classification => {
        result.push({
          code: classification.substring(0, 4),
          label: classification
        });
      });
    });

    return result;
  }

  private static formatConferenceData(info:string, note: string):string {
    let tokens = [];
    let result = '';

    if(info){
      tokens.push(removePipe(info));
    }

    if(note){
      tokens.push(removePipe(note));
    }

    if(tokens.length){
      result = tokens.join('; ');
    }

    function removePipe(str){
      str = str.trim();
      return str.replace(/\|/g,'');
    }

    return result;
  }


  private static formatGrantSponsorship(sponsorArray: string[]) {
    let _sponsors = sponsorArray.map(sponsorList=> {
      // change the sponsor delimiter to use '~' because 'Other Details' has embedded ';'
      let sponsorData = sponsorList.replace(';Sponsor:', '~Sponsor:');
      let sponsors = sponsorData.split('~');
      let sponsorLine = [];
      let pairs = [];
      let field = '';
      let value = '';
      let sponsor = {};
      let result = [];

      for (let i = 0; i < sponsors.length; i++) {
        sponsorLine = sponsors[i].split('|');
        sponsor = {};

        for (let j = 0; j < sponsorLine.length; j++) {
          pairs = sponsorLine[j].split(':');

          if (pairs.length === 2) {
            field = pairs[0];
            value = pairs[1];
            sponsor[field] = value;
          }
        }

        result.push(sponsor);
      }

      return result;
    });
    console.log(_sponsors);
    return _sponsors;
  }

  private static formatClinicalTrialNumber(str: string){
    return str;
  }

  private static formatDissertationDetails(str: string) {
    let _details = str.split('|');
    let details = _details.map(_d=> {
      let labelize = _d.split(': ');
      let label = labelize[0];
      let href = null;
      let text = null;
      if (labelize.length > 1) {
        if (labelize[0] == 'Open URL') {
          href = labelize[1];
        } else {
          text = labelize[1];
        }
      }
      return {label: label, text: text, href: href}
    });
    return details;
  }


  private static formatIndexTerms(indexTerms: string) {
    let splitTerms = indexTerms.split(';');
    return splitTerms.map(term=> {
      term = term.trim();
      let isMajor = (term.indexOf('*') === 0) ? true : false;
      term = term.replace('*', '');
      return {isMajor: isMajor, term: term};
    });
  }

  private static formatTestTitle(title: string, acronyms: string, year:string): string {
    let result = '';

    if(title){
      result = title;
    }

    if(acronyms){
      result = result + ' (' + acronyms + ')';
    }

    if(year){
      result = result + ', ' + year;
    }

    return result;
  }
  private static formatTOC(TOCContent: string){
    let HeadingregExp = /(<Heading>|<\/Heading>)/g;
    let TitleregExp = /(<Title>|<\/Title>)/g;

    TOCContent = TOCContent.replace(HeadingregExp, '');
    TOCContent = TOCContent.replace(TitleregExp, '');
    TOCContent = TOCContent.replace('<TOC', '<ul');
    TOCContent = TOCContent.replace('</TOC>', '</ul>');
    TOCContent = TOCContent.replace(/(Entry)/g, 'li');
    TOCContent = TOCContent.replace(/\<AuthorName>([^)]+)\<\/AuthorName>/g, '');  ///\<AuthorName>([^)]+)\<\/AuthorName>/g
    TOCContent = TOCContent.replace(/\<Pages>([^)]+)\<\/Pages>/g, '');
    return TOCContent ;
  }
  /*
  static convertToPNRecord(data: any): IRecord {
    let record: IRecord = {};
    let conversionMap = ['IndexTerms', 'HasAbstract', 'HasFullText', 'HasAccess', 'PAJournalCode', 'ProductCode', 'PublicationHistory', 'Language', 'AuthorName', 'Affiliation', 'SourcePI', 'ISSN', 'PMID', 'Publisher', 'DocumentType', 'DOI', 'Classification', 'CopyrightHolder', 'CopyrightYear', 'PIReleaseDate', 'PEReleaseDate', 'FTReleaseDate', 'PICorrectionDate', 'FTCorrectionDate', 'DataSetDescriptionList', 'DataSetAccessList'];
    let delimiterConversionMap = ['OtherPublisher', 'PublicationType', 'EmailAddress', 'PopulationGroup', 'AgeGroup', 'Location', 'AdministrationMethods', 'Methodology'];
    let commaDelimiterConversionMap = ['FormatCovered'];
    let hiliteSanitizerMap = ['UID', 'PublicationName', 'TestLocation', 'Permissions', 'Fee', 'Commercial', 'Reliability', 'Validity', 'FactorAnalysis', 'AdministrationTime', 'NumberOfTestItems', 'Acronyms', 'TestRecordType', 'GivenDocumentTitle', 'Correspondence', 'PublicationDate', 'PublicationDate', 'InstrumentType', 'Purpose', 'OtherVersions', 'PDFLink'];

    for (let prop in data) {
      if (data.hasOwnProperty(prop) && data[prop] && (conversionMap.indexOf(prop) > -1)) {
        record[prop] = data[prop];
      }
      else if (data.hasOwnProperty(prop) && data[prop] && (delimiterConversionMap.indexOf(prop) > -1)) {
        record[prop] = this.applyDelimiter(data[prop], '; ');
      }
      else if (data.hasOwnProperty(prop) && data[prop] && (commaDelimiterConversionMap.indexOf(prop) > -1)) {
        record[prop] = this.applyDelimiter(data[prop], ', ');
      }
      else if (data.hasOwnProperty(prop) && data[prop] && (hiliteSanitizerMap.indexOf(prop) > -1)) {
        record[prop] = this.removeHighlights(data[prop]);
      }
    }

    if (this.has('Instrumentation', data)) {
      record.Instrumentation = data.Instrumentation.map(test=> this.removeHighlights(test));
    }
    if (this.has('KeyConcepts', data)) {
      record.KeyConcepts = data.KeyConcepts.map(keyword=> this.removeHighlights(keyword)).join('; ');
    }
    if (this.has('AuthorOrig', data)) {
      record.AuthorOrig = data.AuthorOrig.map(AuthorOrig=>this.removeHighlights(AuthorOrig)).join('; ');
    }
    if (this.has('Construct', data)) {
      record.Construct = data.Construct.map(construct=> this.removeHighlights(construct));
    }
    if (data.hasOwnProperty('Abstract') && data.Abstract && this.has('Language', data) && data.Abstract[data.Language[0]]) {
      record.Abstract = this.formatAbstracts(data.Abstract[data.Language[0]]);  //Forcing English for now
    }
    if (data.hasOwnProperty('Abstract') && data.Abstract && this.has('Language', data) && !data.Abstract[data.Language[0]]) {
      record.Abstract = this.formatAbstracts(data.Abstract);
    }
    if (this.has('ProductCode', data)) {
      record.ProductLabel = ProductLabels.getLabel(data.ProductCode);
    }
    if (this.has('SourcePI', data)) {
      record.SourcePI = this.removeHighlights(this.formatSource(data.SourcePI));
    }
    if (this.has('SourcePE', data)) {
      record.SourcePE = this.formatSource(data.SourcePE);
    }
    if (this.has('IndexTerms', data)) {
      record.FormattedIndexTerms = this.formatIndexTerms(data.IndexTerms[0]);
    }
    if (this.has('Affiliation', data)) {
      record.Affiliation = this.formatAffiliations(data.Affiliation);
    }
    if (this.has('Classification', data)) {
      record.Classification = this.formatClassifications(data.Classification);
    }

    // indicate if the record is a PsycTESTS record.  This is a helper variable because the view will change depending if the record is a PsycTEST
    record.isPT = (record.ProductCode === "PT");

    return record;
  }

  public static applyDelimiter(items: string[], delimiter: string) {
    return items.join(delimiter);
  }

  public static removeHighlights(text: string) {
    return text.replace(/<span class=""hilite"">/gi, '').replace(/<span class='hilite'>/gi, "").replace(/<\/span>/gi, '');
  }

  public static formatAbstracts(text: string) {
    return this.removeHighlights(text.replace(/\t+/g, ''));
  }

  public static has(prop: string, data: any) {
    return data.hasOwnProperty(prop) && data[prop] && data[prop].length;
  }

  public static formatSource(source: string) {
    return source.replace(/\|/g, '; ');
  }

  public static formatIndexTerms(indexTerms: string) {
    let splitTerms = this.removeHighlights(indexTerms).split('; ');
    return splitTerms.map(term=> {
      let isMajor = false;
      if (term.indexOf('*') === 0) {
        isMajor = true;
      }
      term = term.replace('*', '');
      return {isMajor: isMajor, term: term.trim()};
    });
  }

  public static formatAffiliations(affiliations: string[]) {
    return affiliations.map(affiliation=> this.removeHighlights(affiliation.replace(/\^/g, '')));
  }

  public static formatClassifications(classifications: string[]) {
    let result = [];

    classifications.forEach(classification => {
      result.push({
        code: classification.substring(0, 4),
        label: classification
      });
    });

    return result;
  }
  */

}
