import {Injectable} from '@angular/core';
import {HighlightService} from '../../../shared/highlight/highlightService';

@Injectable()
export class DocumentTypeFormatter {

  public static getDocumentTypeLabel(record: any) {
    let descriptor = 'Other';

    if (record["IsOFP"] && record["IsOFP"] === 'true')
      descriptor = 'First Posting';
    else if (record["PublicationStatus"] && record["PublicationStatus"] === 'First Posting')
      descriptor = 'First Posting';
    else if (record["BookType"] && record["BookType"] === "0600")
      record["descriptor"] = 'Conference Proceedings';
    else if (record["DocumentType"] && record["DocumentType"].length)
      descriptor = HighlightService.removeHighlights(record["DocumentType"]);
    else if (record["PublicationType"] && record["PublicationType"][0] && record["PublicationType"][0].length) {
      record["PublicationType"][0] = HighlightService.removeHighlights(record["PublicationType"][0]);

      if (record["PublicationType"][0] == '0280')
        descriptor = 'Edited Book';
      else if (record["PublicationType"][0] == '0240')
        descriptor = 'Authored Book';
      else if (record["PublicationType"][0] == '0300')
        descriptor = 'Encyclopedia';
      else
        descriptor = record["PublicationType"][0];
    }

    switch (descriptor) {
      case 'Erratum/Correction':
        descriptor = 'Erratum / Correction';
        break;
      case 'Comment/Reply':
        descriptor = 'Comment / Reply';
        break;
      case 'Brochure/Pamphlet':
        descriptor = 'Brochure / Pamphlet';
        break;
      case 'Conference Presentat':
        descriptor = 'Conference Presentation';
        break;
    }

    return descriptor;
  }

}
