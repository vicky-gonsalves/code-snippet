import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {RecordDisplay} from "./recordDisplay";
import { UIDGuardService } from "../shared/id/uidGuardService";

const routes: Routes = [
  { path: '', component: RecordDisplay },
  { path: ':uid', component: RecordDisplay, canActivate: [UIDGuardService] },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
