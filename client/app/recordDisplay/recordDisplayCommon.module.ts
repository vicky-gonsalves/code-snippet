import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import {Abstract} from "./components/abstract/abstract";
import {Citation} from "./components/citation/citation";
import {CitedReferences} from "./components/citedReferences/citedReferences";
import {CitedReferencesAccordion} from "./components/citedReferencesAccordion/citedReferencesAccordion";
import {FullRecordDisplay} from "./components/fullRecordDisplay/fullRecordDisplay";
import {FullRecordDisplayAccordion} from "./components/fullRecordDisplayAccordion/fullRecordDisplayAccordion";

const declarations = [
  Abstract,
  Citation,
  CitedReferences,
  CitedReferencesAccordion,
  FullRecordDisplay,
  FullRecordDisplayAccordion,
];

@NgModule({
  imports: [SharedModule],
  declarations: [ ...declarations ],
  providers: [],
  exports:[ ...declarations ]
})
export class RecordDisplayCommonModule {}
