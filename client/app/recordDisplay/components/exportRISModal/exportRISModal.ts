import {Component, Input, ViewChild, Optional} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap';
import {SessionStorageService} from 'ng2-webstorage';
import {TranslatePipe, LocaleService, LocalizationService} from 'angular2localization';
import {TranslationAlias} from '../../../shared/pipes/translationAlias';
import {ApiRequestService, Query} from "../../../core/services/apiRequestService";
import {PermissionService} from "../../../core/services/identity/permissionService";
import * as _ from "lodash"


@Component({
  selector : 'export-ris-modal',
  templateUrl: './exportRISModal.html',
  styleUrls: ['./exportRISModal.less']
})
export class ExportRISModal {
  @Input() isMyList: boolean;
  @Input() parentId : string;
  @Input() recordsPerPage:number;
  @Input() allRecordsOnPageUIDList:Array<any>;
  @ViewChild('exportRISModal') public exportRISModal:ModalDirective;

  export:any;
  exportTypeList:Array<any> = [];
  selectedRecordUIDList = [];
  currentPageSelectedRecordUIDList = [];
  submitted: boolean;
  showErrorMassage:boolean;
  loading: boolean;

  selectedExportOption: string;
  translator: TranslatePipe;

  constructor(private _sessionStorageService : SessionStorageService,
              private _locale: LocaleService,
              private _localization: LocalizationService,
              private _permission: PermissionService,
              private _apiService : ApiRequestService){
    this.translator = new TranslatePipe(this._localization, this._locale);

    this.exportTypeList =  [
      {name : "Reference Software", value : "referenceSoftware"},
      {name : "APA Style CENTRAL", value : "apaStyleCentral"},
      {name : "RefWorks" , value: "refworks"},
      {name : "EndNote Web", value : "endnote"},
      {name : "EndNote Desktop", value : "endnoteDesktop"},
      {name : "Mendeley", value : "mendeley"},
      {name : "Zotero", value : "zotero"}
    ];

    if(!_permission.mayAccess('ASC')) {
      this.exportTypeList.splice(1, 1);
    }

    this._resetForm();
  }

  ngOnInit(){
  }

  openModal(): void {
    this._resetForm();


    this.selectedRecordUIDList = this._sessionStorageService.retrieve('selectedSearchRecords') || [];
    this.currentPageSelectedRecordUIDList = this._sessionStorageService.retrieve('currentPageSearchRecords') || [];


    this.exportRISModal.show();
  }

  private _resetForm(){
    this.showErrorMassage = false;
    this.submitted = false;
    this.loading = false;
    this.export = {
      exportTo: this.exportTypeList[0].value,
      selectedType : 'Selected Records - All Pages'
    };

    this.selectedExportOption = this.translate(this.exportTypeList[0].name);
  }

  closeModal():void {
    this.submitted = false;
    this.exportRISModal.hide();
  }

  isOpen(): boolean{
    return this.exportRISModal.isShown;
  }

  setExportType(index) {
    this.selectedExportOption = this.translate(this.exportTypeList[index].name);
    this.export.exportTo = this.exportTypeList[index].value;
  }

  exportRISFile() {
    this.submitted = true;
    let importThreshold = 250;
    let uidList = this._getUIDList();

    if(uidList && uidList.length && uidList.length <= importThreshold){

      let qry, params ;
      if (this.export.exportTo === "refworks" || this.export.exportTo === "endnote") {
        qry = this._apiService.createQuery('record.exportRefWorkAndEndNoteRIS');
        params = {
          UIDList: uidList,
          exportType: this.export.exportTo
        };
        this.loading = true;

      } else {
        qry = this._apiService.createQuery('record.exportRISFile');
        params = {
          UIDList: uidList,
          exportType: this.export.exportTo
        };
      }

      qry.params(params);
      this._apiService.execute(qry).subscribe(data => {
        if( data.isRisExportCreated && data.accessLink.length){
          window.open(data.accessLink);
        } else {
          window.open('/ris/download');
        }
        this.closeModal();
      });

    }else{
      this.showErrorMassage = true;
    }

  }

  private verifySelection() {
    let validSelection;

    switch(this.export.selectedType){
      case 'Selected Records':
        validSelection = this.currentPageSelectedRecordUIDList.length > 0;
        break;
      case 'All Records on Page':
        validSelection = this.recordsPerPage > 0;
        break;
      case 'Selected Records - All Pages':
        validSelection = this.selectedRecordUIDList.length > 0;
        break;
    }

    return validSelection;
  }

  private _getUIDList(){
    let uidList = [];

    switch(this.export.selectedType){
      case 'Selected Records':
        uidList = _.cloneDeep(this.currentPageSelectedRecordUIDList);
        break;
      case 'All Records on Page':
        uidList = _.cloneDeep(this.allRecordsOnPageUIDList);
        break;
      case 'Selected Records - All Pages':
        uidList = _.cloneDeep(this.selectedRecordUIDList);
        break;
    }
    uidList = this._reomvePsycTestAndPsycEXTRAUID(uidList);
    return uidList;
  }

  private _reomvePsycTestAndPsycEXTRAUID(uidList){
    let newUID = [];
    for(let uid of uidList){


      if(uid['UID'].indexOf("-") != 9 && uid['UID'].indexOf('9999') != 0){
        newUID.push(uid);
      }
    }
    return newUID;
  }

  translate(term: string): string {
    let alias = new TranslationAlias().transform(term, 'RECORD_DISPLAY');
    return this.translator.transform(alias, this._localization.languageCode);
  }
}
