import {Component, Input} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";
import {ApiRequestService} from '../../../core/services/apiRequestService';
import {IdentityService} from '../../../core/services/identity/identityService';
import {PurchaseService} from '../../../core/services/purchase/purchaseService';

@Component({
  selector: 'button-box',
  templateUrl: './buttonBox.html',
  styleUrls: ['./buttonBox.less']
})


export class ButtonBox {
  @Input() record: IRecord;
  @Input() showComponents: string[];

  isOrganization: boolean;

  constructor(private _apiService:ApiRequestService ,private _purchase: PurchaseService) {

  }

  ngOnInit() {
    IdentityService.identity.subscribe(identity => this.isOrganization = (identity && identity.isOrganization));
  }

  toShow(item: string) {
    return this.showComponents && this.showComponents.length && this.showComponents.indexOf(item) > -1;
  }

  hasCitedByCount(): boolean {
    return this.record.CitedByCount && parseInt(this.record.CitedByCount) > 0;
  }

  PDFAvailable(){
    if (this.record.PDFLink && this.record.PDFLink.length) {
      return true;
    }
    else {
      return false;
    }
  }

 MediaAvailable(){
    if (this.record.MediaLink && this.record.MediaLink.length) {
      return true;
    }
    else {
      return false;
    }
  }

  HTMLAvailable(){
    if (this.record.XMLLink && this.record.XMLLink.length) {
      return true;
    }
    else {
      return false;
    }
  }

  SupportDocAvailable(){
    return (this.record.SupportingDocumentationLink && this.record.SupportingDocumentationLink.length);
  }

  logSupportingDoc(){
    let query = this._apiService.createQuery('log.logEntry');
    query.logs({
      pageId: 'Test_supporting_doc',
      eventType: 'PsycTESTS Supporting Docs'
    });
    this._apiService.execute(query).subscribe();
  }

  shouldPurchase(): boolean {
    return !this.record.HasAccess && (['PA', 'PB'].indexOf(this.record.ProductCode) >= 0) && !this.isOrganization;
  }

  isInCart(): boolean {
    return this._purchase.has(this.record.UID);
  }

}
