import {Component, Input} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";
import {HighlightService} from "../../../shared/highlight/highlightService";

@Component({
  selector: 'abstract',
  templateUrl: './abstract.html',
  styleUrls: ['./abstract.less']
})


export class Abstract {
  @Input() record: IRecord;
  @Input() hideAbstractHeader : boolean;
  @Input() isTestRecord : boolean;
  extraLanguages = [];

  language: string = 'English';
  englishAbstract: string;

  constructor() {
  }

  ngOnInit() {
    if(this.record.isPT && typeof this.record.Abstract == 'string') {
       this.englishAbstract = this.record.Abstract;
    } else {
      this.englishAbstract = this.record.Abstract[this.language];
      if(this.record.Language) {
        for(let abstractLanguage of this.record.Language) {
          this.getAbstracts(abstractLanguage);
        }
      } else if(this.record.AbstractLanguageList) {
        this.getAbstracts(this.record.AbstractLanguageList);
      }
    }
  }

  getAbstracts(languageList) {
    let splitLanguages = languageList.split(';');
    for(let lang of splitLanguages) {
      lang = HighlightService.removeHighlights(lang);
      if(lang != 'English' && this.record.Abstract[lang]) {
        this.extraLanguages.push(lang);
      }
    }
  }

}
