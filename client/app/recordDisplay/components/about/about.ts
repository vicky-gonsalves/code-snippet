import {Component, Input} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";
import {ApiRequestService, Query} from "../../../core/services/apiRequestService";
import {PermissionService} from "../../../core/services/identity/permissionService";
import {IdentityService} from "../../../core/services/identity/identityService";
import {Identity} from "../../../core/services/identity/identity";

@Component({
  selector: 'about',
  templateUrl: './about.html',
  styleUrls: ['./about.less']
})


export class About {
  @Input() record: IRecord;
  loadingEditor: boolean = true;
  query: Query;
  journalEditors: string;
  showContainer = true;
  showJournalTOC: boolean = false;
  identity: Identity;

  constructor(private _apiService: ApiRequestService, private _permissionService: PermissionService) {
  }

  ngOnInit() {
    IdentityService.identity.subscribe(data => {
      // ensure identity is loaded
      if(!data){
        return false;
      }

      this.identity = data;
      this.showJournalTOC = this.allowTOCLinkDisplay();
      this.loadEditors();
    });
  }

  allowTOCLinkDisplay() {
    let result = true;

    if(this.identity.isOrganization && !this.record.HasAccess) {
      result = false;
    }

    return result;
  }

  loadEditors() {

    this.query = this._apiService.createQuery('browsePA.getJournals');
    this.query.params({code: this.record.PAJournalCode});

    this._apiService.execute(this.query).subscribe(
      data => {
        let result = data.response.result.doc || [];

        // could be a data issue if journals are not available
        if(!result.length){
          this.showContainer = false;
        }

        for (let i = 0; i < result.length; i++) {
          if (result[i].PAJournalCode == this.record.PAJournalCode) {
            this.journalEditors = result[i].Editor;
            break;
          }
        }

        this.loadingEditor = false;
      },
      error => {
        console.log(error);
        this.loadingEditor = false;
      }
    );
  }
}
