import {Component, Input} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";

@Component({
  selector: 'impact-statement',
  templateUrl: './impactStatement.html',
  styleUrls: ['./impactStatement.less']
})


export class ImpactStatement {
  @Input() record: IRecord;

  heading: string;
  statement: string;

  ngOnInit() {
    if(this.record.ImpactStatement && this.record.ImpactStatement.English && this.record.ImpactStatement.English.length) {
      let splitter = this.record.ImpactStatement.English.split('—');
      if(splitter.length > 1) {
        this.heading = splitter.splice(0, 1)[0];
      }
      this.statement = '—' + splitter.join();
    }
  }
}
