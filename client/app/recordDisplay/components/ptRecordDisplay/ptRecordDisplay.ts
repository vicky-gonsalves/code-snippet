import {Component, Input} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";

@Component({
  selector: 'pt-record-display',
  templateUrl: './ptRecordDisplay.html',
  styleUrls: ['./ptRecordDisplay.less']
})


export class PTRecordDisplay {
  @Input() record: IRecord;
  @Input() tabRecords: Array<IRecord>;
  @Input() testPrimaryRecord: IRecord;

  constructor() {

  }

  ngOnInit() {

  }
}


