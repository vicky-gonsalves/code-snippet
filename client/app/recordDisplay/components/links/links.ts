import {Component, Input} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";
import {PermissionService} from "../../../core/services/identity/permissionService";
import {ApiRequestService, Query} from "../../../core/services/apiRequestService";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'links',
  templateUrl: './links.html',
  styleUrls: ['./links.less']
})


export class Links {
  @Input() record: IRecord;
  private scrollExecuted: boolean = false;

  constructor(private _permissionService: PermissionService,
              private _apiService: ApiRequestService,
              private route: ActivatedRoute,
              private _router:Router) {
  }

  ngAfterViewChecked() {
    if (!this.scrollExecuted) {
      let routeFragmentSubscription: Subscription;
      routeFragmentSubscription = this.route.fragment.subscribe(fragment => {
        if (fragment) {
          let element = document.getElementById(fragment);
          if (element) {
            element.scrollIntoView();
            this.scrollExecuted = true;
            // Free resources
            setTimeout(
              () => {
                console.log('routeFragmentSubscription unsubscribe');
                routeFragmentSubscription.unsubscribe();
              }, 0);
          }
        }
      });
    }
  }

  gotoHashtag(fragment: string) {
    let url = '';
    let urlWithSegments = this._router.url.split('#');

    if(urlWithSegments.length){
      url = urlWithSegments[0];
    }

    window.location.hash = fragment;
    const element = document.querySelector("#" + fragment);
    if (element) element.scrollIntoView(element);
  }

  ngOnInit() {
  }

  public shdDisplayFullText(): boolean {
    return this.record && this.record.ProductCode && this.record.ProductCode === 'PI' && this.record.DOI && this.record.DOI.length && !this.record.BookCollection && this.record.ProductCode !== 'BC';
  }

  public shdDisplayBookTocChapterPDF(): boolean {
    return this.record && ((this.record.ProductCode && this.record.ProductCode === 'PB') || (this.record.BookCollection && this._permissionService.mayAccessRecord(this.record)));
  }

  public shdDisplayBookToc(): boolean {
    return this.record && this.record.ProductCode && this.record.ProductCode === 'PI' && this.record.TOC && this.record.TOC.length && !this.record.BookCollection;
  }

  public logAction(){
    let query = this._apiService.createQuery('log.logEntry');

    query.logs({
      pageId: 'PI_DOI_link',
      eventType: 'DOI referral from PI',
      uid: this.record.UID,
      abstractsViewedUIDs: [{
        UID: this.record.UID,
        ProductCode: this.record.ProductCode
      }]
    });

    this._apiService.execute(query).subscribe();
  }

}
