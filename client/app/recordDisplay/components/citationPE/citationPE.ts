import { Component, Input } from '@angular/core';
import { IRecord } from "../../services/format/IRecord";

@Component({
  selector: 'citation-pe',
  templateUrl: './citationPE.html',
  styleUrls: ['./citationPE.less']
})
export class CitationPE {
    @Input() record: IRecord;
    @Input() showHeading: boolean;
    pePages: any;

    constructor() { }

    ngOnInit() {
        if (this.record.hasOwnProperty('SourcePE') && this.record.SourcePE.trim().length) {
            this.setPEPages();
        }
        if(this.record.PublicationDate) {
          this.record.PublicationDate = this.record.PublicationDate.trim();
        }
    }

    setPEPages() {
        if (this.record.SourcePE.match(/\d+-\d+/g)) {
            this.pePages = /\d+-\d+/g.exec(this.record.SourcePE);
        }
        else if (this.record.SourcePE.match(/\d+ p+/g)) {
            this.pePages = /\d+ p+/g.exec(this.record.SourcePE);
        }
        else {
            this.pePages = "";
        }
    }
}
