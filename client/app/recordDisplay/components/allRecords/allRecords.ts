import {Component, Input, Output, EventEmitter} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";

@Component({
  selector: 'all-records',
  templateUrl: './allRecords.html',
  styleUrls: ['./allRecords.less']
})


export class AllRecords {
  @Input() record: IRecord;
  @Output() recordSelect = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  toggleSelection(record:IRecord) {
    this.recordSelect.emit(record);
    console.log(record);
  }
}


