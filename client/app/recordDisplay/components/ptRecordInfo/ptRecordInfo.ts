import {Component, Input} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";
import {Search} from "../../../search/services/search/search";
import {ApiRequestService, Query} from "../../../core/services/apiRequestService";
import {PsycNETQuery} from "../../../search/services/search/query/psycNETQuery";
import {Criteria} from "../../../search/services/search/query/components/criteria";
import {IResponseParameters} from "../../../search/services/search/query/components/interfaces";
import {RecordFormatter} from "../../services/format/recordFormatter";


@Component({
  selector: 'pt-record-info',
  templateUrl: './ptRecordInfo.html',
  styleUrls: ['./ptRecordInfo.less']
})
export class RecordInfo {
  @Input() record: IRecord;
  @Input() testPrimaryData : IRecord;
  otherVersionsArray = [];
  recordSearch: Search;
  loading: boolean;
  newRecord : IRecord;
  showDetails = false;
  suppressedFields: string[];

  constructor(private _apiService: ApiRequestService) {
  }

  ngOnInit() {
    this.suppressedFields = ['ReportedIn', 'DOI', 'Title', 'PublicationDate'];
    if(this.record.OtherVersions) {
      this.generateVersionItems();
    }
  }

  generateVersionItems() {
    for (let i = 0; i < this.record.OtherVersions.length; i++) {
      let otherVersion = this.record.OtherVersions[i].toString().split(',');
      this.otherVersionsArray[i] = {
        'UID': otherVersion[0],
        'Title': otherVersion[1],
        'Type': otherVersion[2]
      };
    }
  }

  private createUIDSearch(uid: string): Search {
    let pnQuery = new PsycNETQuery();
    pnQuery.criteria.push(new Criteria('UIDs', 'OR', uid, 'user'));
    pnQuery.filter.facets.push({field: 'ChildUID', value: uid});

    let responseParams: IResponseParameters = {
      start: 0,
      rows: 1
    };

    let search = new Search(pnQuery, responseParams);
    search.metadata.uid = uid;

    // disable highlighting for UID searches
    search.responseParameters.hl = false;

    return search;
  }

  public performSearch() {
    if (!this.recordSearch) {
      return false;
    }

    this.loading = true;

    let qry: Query = this._apiService.createQuery('search.record');
    qry.params(this.recordSearch);

    this._apiService.execute(qry).subscribe(
      data => {
        if (data.results && data.results.result && data.results.result.doc && data.results.result.doc.length) {
          let record = data.results.result.doc.filter(doc=>doc.UID == this.testPrimaryData.UID);
          if (record.length) {
            this.newRecord = RecordFormatter.convertToPNRecord(record[0]);
            this.newRecord.Fee=this.record.Fee;
            this.newRecord.Format=this.record.Format;
            this.newRecord.Purpose=this.record.Purpose;
            this.newRecord.Language=this.record.Language;
            this.newRecord.Construct=this.record.Construct;
            this.newRecord.AuthorName=this.record.AuthorName;
            this.newRecord.Commercial=this.record.Commercial;
            this.newRecord.Permissions=this.record.Permissions;
            this.newRecord.PTReleaseDate=this.record.PTReleaseDate;
            this.newRecord.Correspondence=this.record.Correspondence;
          } else {
            this.newRecord = null;
          }
        }
        this.loading = false;
      },
      error => {
        this.loading = false;
        console.log('Record Error: ', error);
      }
    );
  }

  /*Toggle Accordion*/
  toggleFullRecord() {
     this.showDetails = !this.showDetails;
     if(this.showDetails && !this.newRecord) {
       this.recordSearch = this.createUIDSearch(this.testPrimaryData.UID);
       this.performSearch();
    }
  }
}
