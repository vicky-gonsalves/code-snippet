import {Component, Input} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";
import {ITabsData} from "./ITabsData";
import * as _ from "lodash";
import {SessionStorageService} from 'ng2-webstorage';

@Component({
  selector: 'pt-tabs-component',
  templateUrl: './ptTabsComponent.html',
  styleUrls: ['./ptTabsComponent.less']
})


export class PTTabsComponent {
  @Input() tabRecords: Array<IRecord>;
  @Input() record: IRecord;
  selectedTab: string;
  selectedRecord: IRecord;
  selectedRecordList : IRecord[] = [];
  allSelection: IRecord;
  isAllowed: boolean;
  isSelectAll : boolean = false;
  selectedCount = 0;

  tabsData: ITabsData;
  currentTabRecords: Array<IRecord>;

  constructor(private _sessionStorageService : SessionStorageService) {

  }

  ngOnInit() {
    if (this.tabRecords && this.tabRecords.length) {
      this.tabsData = {testDevelopment: [], testReview: [], testUse: [], all: []};
      this.separateTabs();
      this.selectTab(this.selectedTab);
      this.getSelectedRecord();
    }
  }

  separateTabs() {
    this.tabsData.testDevelopment = this.tabRecords.filter(record=>record.TestRecordType == 'Test Development');
    this.tabsData.testReview = this.tabRecords.filter(record=>record.TestRecordType == 'Test Review');
    this.tabsData.testUse = this.tabRecords.filter(record=>record.TestRecordType == 'Test Use');
    this.tabsData.all = this.tabRecords;
    if (this.showAllRecords()) {
      this.selectedTab = "All";
    } else if (this.tabsData.testDevelopment.length) {
      this.selectedTab = "Test Development";
    } else if (this.tabsData.testReview.length) {
      this.selectedTab = "Test Review";
    } else if (this.tabsData.testUse.length) {
      this.selectedTab = "Test Use";
    }
  }

  showAllRecords() {
    return this.tabsData && this.tabsData.all && this.tabsData.all.length > 1;
  }

  getRecords() {
    let recordMap = {
      'Test Development': 'testDevelopment',
      'Test Review': 'testReview',
      'Test Use': 'testUse',
      'All': 'all'
    };
    return this.tabsData[recordMap[this.selectedTab]];
  }

  selectTab(tab: string) {
    this.selectedTab = tab;
    this.currentTabRecords = this.getRecords();
    for (let record of this.currentTabRecords){
      record.isFullRecordOpen = false;
    }
    this.selectedRecordList= [];
    this.getSelectedRecord();
  }

  toggleAllSelection(selectAll: boolean) {
    this.selectedRecordList= [];
    for(let record of this.tabsData.all){
      record.isSelected = selectAll;
      if(selectAll){
        this.selectedRecordList.push(record);
      }
    }

    this.isSelectAll = this.selectedRecordList.length == this.tabsData.all.length;
    this.getSelectedRecord();
  }

  setAllSelection (record) {
    this.selectedRecordList = [];
    for(let record of this.tabsData.all){
      if(record.isSelected){
        this.selectedRecordList.push(record);
      }
    }

    this.isSelectAll = this.selectedRecordList.length == this.tabsData.all.length;
    this.getSelectedRecord();
  }

  getSelectedRecord() {
    if(this.selectedTab == 'All' && this.selectedRecordList.length > 0) {
      this._sessionStorageService.clear("currentPageSearchRecords");
      this.selectedRecord = {};
      this.isAllowed = true;
    } else if(this.selectedTab == 'All' && this.selectedRecordList.length == 0){
        this.isAllowed= false;
    } else if(this.selectedTab != 'All') {
      this.selectedRecordList = [];
      this.selectedRecord = this.getRecords()[0];
      this._sessionStorageService.store("currentPageSearchRecords", [ _.pick(this.selectedRecord, ['UID','ProductCode']) ]);
      this.isAllowed= true;
    }
  }
}


