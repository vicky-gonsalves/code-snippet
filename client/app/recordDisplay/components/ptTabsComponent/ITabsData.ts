import {IRecord} from "../../services/format/IRecord";

export interface ITabsData {
  testDevelopment: IRecord[],
  testReview: IRecord[],
  testUse: IRecord[],
  all: IRecord[]
}
