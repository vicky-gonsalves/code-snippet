import {Component, Input} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";

@Component({
  selector: 'index-terms',
  templateUrl: './indexTerms.html',
  styleUrls: ['./indexTerms.less']
})


export class IndexTerms {
  @Input() record: IRecord;

  constructor() {

  }

  ngOnInit() {
  }

  stripTags(term){
    return  term ? String(term).replace(/<[^>]+>/gm, '') : '';
  }

}
