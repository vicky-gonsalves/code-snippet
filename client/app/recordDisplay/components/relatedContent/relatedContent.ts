import {Component, Input, Optional} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";
import {ApiRequestService, Query} from "../../../core/services/apiRequestService";

import {Search} from "../../../search/services/search/search";
import {SearchCollection} from "../../../search/services/search/searchCollection";
import {PsycNETQuery} from "../../../search/services/search/query/psycNETQuery";
import {IResponseParameters} from "../../../search/services/search/query/components/interfaces";
import {Criteria} from "../../../search/services/search/query/components/criteria";
import {SearchNavigation} from "../../../search/services/searchNavigation";

import {HighlightService} from "../../../shared/highlight/highlightService";

@Component({
  selector: 'related-content',
  templateUrl: './relatedContent.html',
  styleUrls: ['./relatedContent.less']
})


export class RelatedContent {
  @Input() record: IRecord;
  @Input('no-more') hideMoreLink: boolean;
  recommended = [];
  loading = false;

  constructor(private _apiService: ApiRequestService,
              private _searchNavigation: SearchNavigation) {

  }

  ngOnInit() {
    console.log("Related: ", this.record);
    this.getRelatedRecords(this.record.UID, function(){});
  }

  public getRelatedRecords(uid:string, cb: Function, @Optional() limit?:number){
    if(!limit){ this.loading = true; }

    let qry: Query = this._apiService.createQuery('record.getRecommendations');

    // Remove the highlight from UID, if it exists
    uid = HighlightService.removeHighlights(uid);

    qry.params({
      uid: uid,
      limit: limit || 5
    });

    this._apiService.execute(qry).subscribe(
      data => {
        if(!limit){
          this.recommended = data;
          this.loading = false;
        }
        cb(null, data);
      },
      error => {
        console.error('Recommended Error: ', error);
        cb(error , null);
      }
    );
  }

  getMoreRecords(){
    var self = this;
    this.getRelatedRecords(this.record.UID, function(err, data){
      if(err) {
        return false;
      }

      self._redirectSearch(data.articles);
    }, 25);
  }

  // perform Basic Search
  private _redirectSearch(relatedArticles : any){
    let pnQuery = this._relatedQueryFormation(relatedArticles);

    let responseParams: IResponseParameters = {
      start: 0,
      rows: 25,
      facet: true,
      results: true
    };

    // add search to the collection
    let search = new Search(pnQuery, responseParams);
    search.query.databases = ['PI','PA','PB'];  // explicitly specified for Counter4 logging

    console.log('Search: ', search);
    SearchCollection.add(search);

    // setting queryParams to an empty object will remove existing query params from URL
    this._searchNavigation.gotoResults(search, {queryParams: {id: search.id}});
  }

  private _relatedQueryFormation(relatedArticles):PsycNETQuery {
    let pnQuery = new PsycNETQuery();
    for(let article of relatedArticles){
      pnQuery.criteria.push( new Criteria('UIDs', 'OR', article.UID, 'user') );
    }
    pnQuery.source = 'recommended';

    return pnQuery;
  }
}
