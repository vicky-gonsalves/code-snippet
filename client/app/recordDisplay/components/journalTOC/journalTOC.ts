import {Component, Input} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";

@Component({
  selector: 'journal-toc',
  templateUrl: './journalTOC.html',
  styleUrls: ['./journalTOC.less']
})


export class JournalTOC {
  @Input() record: IRecord;
  active: boolean;

  constructor() {
    if (this.record) {
      this.toggleTOC();
    }
  }

  ngOnInit() {
  }

  /*Toggle TOC*/
  toggleTOC() {
    this.active = !this.active;
  }

}
