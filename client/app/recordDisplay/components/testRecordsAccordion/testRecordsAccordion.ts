import { Component, Input, Output, EventEmitter } from "@angular/core";
import { IRecord } from "../../services/format/IRecord";
import { RecordFormatter } from "../../services/format/recordFormatter";
import { Search } from "../../../search/services/search/search";
import { PsycNETQuery } from "../../../search/services/search/query/psycNETQuery";
import { Criteria } from "../../../search/services/search/query/components/criteria";
import { IResponseParameters } from "../../../search/services/search/query/components/interfaces";
import { ApiRequestService, Query } from "../../../core/services/apiRequestService";
import { PsycTestRecordType } from "../../services/psycTestRecordType";
import * as _ from "lodash";

@Component({
  selector: 'test-records-accordion',
  templateUrl: './testRecordsAccordion.html',
  styleUrls: ['./testRecordsAccordion.less']
})


export class TestRecordsAccordion {
  @Input() record: IRecord;

  newRecord: IRecord;
  newRecordRef: IRecord;
  recordSearch: Search;
  loading: boolean;
  breadcrumblist: Array<any>;
  browseMode: string = '';
  currentPage: number;
  suppressedFields: string[];

  constructor(private _apiService: ApiRequestService) {
  }

  ngOnInit() {
    this.suppressedFields = ['ReportedIn', 'DOI', 'Title', 'PublicationDate'];
  }

  private getRecordData(search: Search) {
    this.recordSearch = search;

    // get the record data
    this.performSearch();
  }

  private createUIDSearch(uid: string): Search {
    let pnQuery = new PsycNETQuery();
    pnQuery.criteria.push(new Criteria('UIDs', 'OR', uid, 'user'));
    pnQuery.filter.facets.push({ field: 'ChildUID', value: uid });

    let responseParams: IResponseParameters = {
      start: 0,
      rows: 1
    };

    let search = new Search(pnQuery, responseParams);
    search.metadata.uid = uid;

    // disable highlighting for UID searches
    search.responseParameters.hl = false;

    return search;
  }

  public performSearch() {
    if (!this.recordSearch) {
      return false;
    }

    this.loading = true;

    let qry: Query = this._apiService.createQuery('search.record');
    qry.params(this.recordSearch);

    this._apiService.execute(qry).subscribe(
      data => {
        if (data.results && data.results.result && data.results.result.doc && data.results.result.doc.length) {
          let record = data.results.result.doc.filter(doc => doc.UID == this.record.UID);
          if (record.length) {
            let parentRecord = data.results.result.doc[0];
            this.newRecord = RecordFormatter.convertToPNRecord(record[0]);
            PsycTestRecordType.addChildRecord(_.cloneDeep(this.newRecord));
            console.log('Parent Record: ', parentRecord);
            this.newRecord.Fee = parentRecord.Fee;
            this.newRecord.Format = parentRecord.Format;
            this.newRecord.Purpose = parentRecord.Purpose;
            this.newRecord.AlternateTitle = parentRecord.AlternateTitle;
            this.newRecord.Acronyms = parentRecord.Acronyms;
            this.newRecord.Language = parentRecord.Language;
            this.newRecord.Construct = parentRecord.Construct;
            this.newRecord.Commercial = parentRecord.Commercial;
            this.newRecord.Permissions = parentRecord.Permissions;
            this.newRecord.PTReleaseDate = parentRecord.PTReleaseDate;
            this.newRecord.Correspondence = parentRecord.Correspondence;
            console.log('New Record: ', this.newRecord);
            this.newRecordRef = _.pick(this.newRecord, ['isPT', 'UID', 'CitedReferences', 'TestRecordType', 'HasAccess']);
            this.newRecordRef.UID = this.newRecord['WorkUID'];
          } else {
            this.newRecord = null;
            this.newRecordRef = null;
          }
        }
        this.loading = false;
      },
      error => {
        this.loading = false;
        console.log('Record Error: ', error);
      }
    );
  }


  /*Toggle Accordion*/
  toggleFullRecord() {
    this.record.isFullRecordOpen = !this.record.isFullRecordOpen;
    if (!this.newRecord && !PsycTestRecordType.isRecordAvailable(this.record.UID)) {
      this.recordSearch = this.createUIDSearch(this.record.UID);
      this.performSearch();
    } else {
      this.newRecord = PsycTestRecordType.getChildRecord(this.record.UID);
      this.newRecordRef = _.pick(this.newRecord, ['isPT', 'UID', 'CitedReferences', 'TestRecordType', 'HasAccess']);
      this.newRecordRef.UID = this.newRecord['WorkUID'];
    }
  }
}


