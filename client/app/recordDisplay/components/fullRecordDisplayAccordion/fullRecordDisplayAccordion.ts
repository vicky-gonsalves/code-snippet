import {Component, Input} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";

@Component({
  selector: 'full-record-display-accordion',
  templateUrl: './fullRecordDisplayAccordion.html',
  styleUrls: ['./fullRecordDisplayAccordion.less']
})

export class FullRecordDisplayAccordion {
  @Input() record: IRecord;
  @Input() forPrint: string;
  @Input() hideFullRecordHeader: boolean;

  constructor() {

  }

  ngOnInit() {
    if (this.record) {
      this.record.isFullRecordOpen = true;
    }
  }

  /*Toggle Accordion*/
  toggleFullRecord() {
    this.record.isFullRecordOpen = !this.record.isFullRecordOpen;
  }

}
