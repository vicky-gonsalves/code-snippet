import {Component, Input, ViewChild} from "@angular/core";
import {EmailRecordModal} from '../../../shared/emailRecordModal/emailRecordModal';
import {ExportRISModal} from '../exportRISModal/exportRISModal';
import {IRecord} from "../../services/format/IRecord";
import {Router, ActivatedRoute} from "@angular/router";
import {SearchCollection} from "../../../search/services/search/searchCollection";
import {Search} from "../../../search/services/search/search";
import {PrintRecordModal} from '../../../search/print/components/printRecordModal/printRecordModal';
import {ButtonBox} from '../buttonBox/buttonBox';
import { Locale, LocaleService, LocalizationService } from 'angular2localization';
import * as _ from "lodash";
import {SessionStorageService} from 'ng2-webstorage';

@Component({
  selector: 'record-action-bar',
  templateUrl: './recordActionBar.html',
  styleUrls: ['./recordActionBar.less']
})


export class RecordActionBar extends Locale {
  @Input() showComponents: string[];
  @Input() record : IRecord;
  @Input() recordList : IRecord[];
  @Input() isAllowed: boolean;
  @ViewChild('emailRecordDialog') emailRecordDialog : EmailRecordModal;
  @ViewChild('exportRISDialog') exportRISDialog : ExportRISModal;
  @ViewChild('printRecordDialog') printRecordDialog : PrintRecordModal;

  searchId : string;
  urlInfo: string;
  showAlert = false;
  alertMessage = {
    type: 'danger',
    timeout: 3000
  };

  constructor( private _route: ActivatedRoute,
               private _sessionStorageService : SessionStorageService,
               public locale: LocaleService,
               public localization: LocalizationService) {
    super(locale, localization);
  }

  ngOnInit() {
    this.urlInfo = window.location.href;
    this._route.queryParams.subscribe(urlParams => {
      if (urlParams.hasOwnProperty('id') && urlParams['id']) {
        this.searchId = urlParams['id'];
      }
    });

    if(this.record && !this.record.hasOwnProperty('DocumentType') && this.record.hasOwnProperty('DocumentTypeLabel')) {
      this.record['DocumentType'] = this.record['DocumentTypeLabel'];
    }

  }

  toShow(item: string) {
    return this.showComponents && this.showComponents.length && this.showComponents.indexOf(item) > -1;
  }

  openEmailRecordModal(){
    if(this.isAllowed) {
      if(this.recordList && this.recordList.length){
        let selectedItems = [];
        for(var search of this.recordList){
          let newSelectedUID = _.pick(search, ['UID','ProductCode']);
          selectedItems.push(newSelectedUID);
        }
        this._sessionStorageService.store("currentPageSearchRecords", selectedItems);
      }
      this.showAlert = false;
      let newSearch = this._getNewAlteredSearchObject();
      this.emailRecordDialog.openModal(newSearch);
    } else {
      this.showAlert = true;
      setTimeout(() => this.showAlert = false, this.alertMessage.timeout);
    }
  }

  openExportRISModalPopup(){
    this.exportRISDialog.openModal();
  }

  openPrintModalPopup(){
    if(this.isAllowed) {
      if(this.recordList && this.recordList.length){
        let selectedItems = [];
        for(var search of this.recordList){
          let newSelectedUID = _.pick(search, ['UID','ProductCode']);
          selectedItems.push(newSelectedUID);
        }
        this._sessionStorageService.store("currentPageSearchRecords", selectedItems);
      }
      this.showAlert = false;
      let newSearch = this._getNewAlteredSearchObject();
      this.printRecordDialog.openModal(newSearch);
    } else {
      this.showAlert = true;
      setTimeout(() => this.showAlert = false, this.alertMessage.timeout);
    }
  }

  private getButtonBoxComponentsRAB() {
    let components = [];
    if (this.record) {
      components = ['pdf', 'full text', 'cited by'];
      if (this.record.hasOwnProperty('isPT') && this.record.isPT) {
        components = ['pdf', 'support-doc'];
      }
    }
    return components;
  }

  private _getNewAlteredSearchObject(){
    if(this.searchId){
      let search:Search = SearchCollection.get(this.searchId);
      let newSearch = _.pick(search, ['id', 'friendlyQuery']);
      newSearch['databases'] = search.query.databases;
      newSearch['numFound'] = search.totalRecords;

      return newSearch;
    }
    return {};
  }

}


