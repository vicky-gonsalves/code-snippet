import {Component, Input, Output, EventEmitter} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";

@Component({
  selector: 'cited-references-accordion',
  templateUrl: './citedReferencesAccordion.html',
  styleUrls: ['./citedReferencesAccordion.less']
})

export class CitedReferencesAccordion {
  @Input() record: IRecord;
  @Input() forPrint: string;
  @Input() hideCitedRefHeader:boolean;
  @Output() intimateComponentInitialized = new EventEmitter();

  constructor() {

  }

  ngOnInit() {
  }

  intimateParent(){
    if(this.intimateComponentInitialized){
      this.intimateComponentInitialized.emit();
    }
  }


  /*Toggle Accordion*/
  toggleCitedRefs() {
    this.record.isCiteOpen = !this.record.isCiteOpen;
  }
}
