import {Component, Input, ViewEncapsulation, Optional} from "@angular/core";
import {DomSanitizer} from '@angular/platform-browser';
import {IRecord} from "../../services/format/IRecord";
import {OpenURLNewWindow}  from "../../../shared/pipes/openURLNewWindow";
import {Query,ApiRequestService} from '../../../core/services/apiRequestService';
import {AuthService} from '../../../core/services/authService';

@Component({
  selector: 'full-record-display',
  templateUrl: './fullRecordDisplay.html',
  styleUrls: ['./fullRecordDisplay.less'],
  encapsulation: ViewEncapsulation.None
})

export class FullRecordDisplay {
  @Input() record: IRecord;
  @Input() forPrint: string;
  @Input() suppressedFields: string[];
  reviewedItemOrder = ['Author(s)', 'Title', 'Translated Title', 'Year', 'ISBN', 'ISBN10', 'ISBN13', 'Other Info'];
  windowObjectReference = null;
  contentOwnerList: any[];
  releaseDate: string;
  affiliations = [];
  emails = [];
  otherPublishers = [];

  constructor(private sanitizer: DomSanitizer, private _apiService: ApiRequestService, private _authService: AuthService) {
  }

  toShow(item: string) {
    return !(this.suppressedFields && this.suppressedFields.length && this.suppressedFields.indexOf(item) > -1);
  }

  getPMIDHref(term) {
    return this.sanitizer.bypassSecurityTrustUrl(this._getURL(term, false));
  }

  private _getURL(term: string, isClinical: boolean, @Optional() type?: String):string {
    let formattedURL = '';
    if (isClinical) {
      formattedURL = new OpenURLNewWindow().transformClinicalTrialNumber(term);
    } else {
      formattedURL = new OpenURLNewWindow().transform(term, type);
    }
    return formattedURL;
  }

  getMeSHHref(term) {
    return this.sanitizer.bypassSecurityTrustUrl(this._getURL(term, false, "MeSH"));
  }

  getClinicalTrialNumberHref(clinicalTrialNumber) {
    return this.sanitizer.bypassSecurityTrustUrl(this._getURL(clinicalTrialNumber, true));
  }
  getAuthorName(AuthID){
    var AuthIDdetails = AuthID.split('|');
    if(AuthIDdetails.length === 2){
      AuthIDdetails[1] = 'http://orcid.org/'+ AuthIDdetails[1].trim();
    }
    return AuthIDdetails;

  }
  stripTags(term){
    return  term ? String(term).replace(/<[^>]+>/gm, '') : '';
  }

  openPopup(popupName: string, term: string, isClinical: boolean, @Optional() type?: String) {
    let url = this._getURL(term, isClinical, type);
    /*https://developer.mozilla.org/en-US/docs/Web/API/Window/open*/
    if (this.windowObjectReference == null || this.windowObjectReference.closed) {
      this.windowObjectReference = window.open(url, popupName, "width=800,height=620,resizable=yes,scrollbars=yes,toolbar=yes");
    } else {
      this.windowObjectReference.focus();
    }
    return false;
  }

  setOwnerData() {
    let result = [];

    for(let item of this.record.ContentOwner) {
      let splittedItem = item.split('| ');
      result.push({name: splittedItem[0], url: splittedItem[1]});
    }

    this.contentOwnerList = result;
  }

  updateReleaseDate(dateField: string, showLabel?: boolean, label?:string): void {
    if(dateField.trim().length) {
      this.releaseDate += this.releaseDate.length ? '; ' : '';

      if (showLabel && label && label.trim().length) {
        this.releaseDate += dateField + ' (' + label + ')';
      }
      else if(showLabel && (this.record.hasOwnProperty('ProductLabel') && this.record.ProductLabel && this.record.ProductLabel.trim().length > 0)) {
        this.releaseDate += dateField + ' (' + this.record.ProductLabel + ')';
      }
      else {
        this.releaseDate += dateField;
      }
    }
  }

  private seperatePairedDetails(source: any[], target: any[]) {
    if(source && source.length) {
      // Empty the target before filling it with fresh values.
      target.length = 0;

      for(let item of source) {
        let details = item.split(': ');
        target.push({ name: details[0], value: details[1] });
      }
    }
  }

  private initializeOtherPublishersList() {
    if(this.record.OtherPublisher && this.record.OtherPublisher.length) {
      for(let item of this.record.OtherPublisher) {
        let itemAsList = item.split(';');
        for(let publisher of itemAsList) {
          if(publisher.trim().length) {
            this.otherPublishers.push(publisher.trim());
          }
        }
      }
    }
  }

  private initializeReleaseDate() {
    this.releaseDate = '';

    if(this.record.PIReleaseDate) {
      this.updateReleaseDate(this.record.PIReleaseDate, true, 'PsycINFO');
    }
    if(this.record.PEReleaseDate) {
      this.updateReleaseDate(this.record.PEReleaseDate, true);
    }
    if(this.record.FTReleaseDate) {
      this.updateReleaseDate(this.record.FTReleaseDate, true);
    }
    if(this.record.PTReleaseDate) {
      this.updateReleaseDate(this.record.PTReleaseDate);
    }
  }

  private initializeInstrumentation() {
    if(this.record.Instrumentation && this.record.Instrumentation.length) {
      for(let item of this.record.Instrumentation) {
        item.text = item.text.replace(/\[/g,' [');
      }
    }
  }

  ngOnInit() {
    if(this.record.ContentOwner) {
      this.setOwnerData();
    }

    // Add a space between the ISSN number and the qualifier
    if(this.record.ISSN) {
      this.record.ISSN = this.record.ISSN.replace(/\(/g,' (');
    }

    // Add a space between the test name and the qualifier
    this.initializeInstrumentation();

    // Seperate the name and affiliation values.
    this.seperatePairedDetails(this.record.Affiliation, this.affiliations);

    // Seperate the name and email address values.
    this.seperatePairedDetails(this.record.EmailAddress, this.emails);

    // Seperate (other publishers) value into a list of publishers.
    this.initializeOtherPublishersList();

    // Calculate the release date for the record
    this.initializeReleaseDate();
  }


}
