import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ApiRequestService, Query } from "../../../core/services/apiRequestService";
import { IRecord } from "../../services/format/IRecord";
import { HighlightService } from '../../../shared/highlight/highlightService';
import { PsycTestRecordType } from "../../services/psycTestRecordType";

import * as _ from "lodash";

@Component({
  selector: 'cited-references',
  templateUrl: './citedReferences.html',
  styleUrls: ['./citedReferences.less']
})

export class CitedReferences {
  @Input() record: IRecord;
  @Input() forPrint: string;
  @Output() intimateParent = new EventEmitter();
  loading: boolean;
  isEmit: boolean;

  // used for appending query string 'ptDisplay=1' to doi links if we are coming from PT Display for logging purposes
  doiLinkPage: string = '';

  constructor(private _apiService: ApiRequestService) {
  }

  ngOnInit() {

    if(this.record.isPT){
      this.doiLinkPage = 'ptDisplay';
    }


    if (this.record.isPT && PsycTestRecordType.isRecordAvailable(this.record.UID)) {
      this.record = PsycTestRecordType.getChildRecord(this.record.UID);
    }
    if (this.record && this.record.UID && !this.record.CitedReferences) {
      this.isEmit = false;
      // Get the Cited References
      this.getCitedrefrences();
    }
  }

  ngAfterViewChecked() {
    if (this.record.CitedReferences && !this.loading && this.intimateParent && !this.isEmit) {
      this.isEmit = true;
      this.intimateParent.emit();
    }
  }

  public updateLoadingStatus(flag: boolean) {
    this.loading = flag;
  }

  /*Returns query params*/
  public getQueryParams() {
    return {
      start: 0, //offset starts at 0th index
      rows: 1000,
      workId: this.record.UID
    };
  }

  public getCitedrefrences() {
    let queryParams = this.getQueryParams();
    queryParams.workId = HighlightService.removeHighlights(queryParams.workId);
    this.fetchCitedReferences(queryParams);
  }

  public fetchCitedReferences(queryParams) {
    this.updateLoadingStatus(true);
    let qry: Query = this._apiService.createQuery('record.getCitedReferences');
    qry.params(queryParams);
    this._apiService.execute(qry).subscribe(
      data => {
        this.record.CitedReferences = data.response.result;
        if (this.record.isPT && this.record.TestRecordType && this.record.TestRecordType.trim() != "" && !PsycTestRecordType.isRecordAvailable(this.record.UID)) {
          PsycTestRecordType.addChildRecord(_.cloneDeep(this.record));
        }
        this.updateLoadingStatus(false);
      },
      error => {
        this.updateLoadingStatus(false);
        console.log('Record Error: ', error);
      }
    );
  }

}
