export interface ICitedRefDocs {
  Author?: string,
  CitedByCount?: string,
  DOI?: string,
  PubYear?: string,
  RecordPILink?: string,
  TextRef?: string,
  UID?: string,
  WorkID?: string[],
  value?: string,
}

export interface ICitedRef {
  doc?: ICitedRefDocs[],
  numFound?: string,
  rows?: string,
  start?: string
  selectedPage?: number
}
