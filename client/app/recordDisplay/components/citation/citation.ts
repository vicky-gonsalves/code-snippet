import {Component, Input} from "@angular/core";
import {IRecord} from "../../services/format/IRecord";

@Component({
  selector: 'citation',
  templateUrl: './citation.html',
  styleUrls: ['./citation.less']
})


export class Citation {
  @Input() record: IRecord;
  @Input() forPrint: string;
  @Input() hideCitationHeader : boolean;
  @Input() displayTitle: boolean;
  constructor() {

  }

  stripTags(term){
    return  term ? String(term).replace(/<[^>]+>/gm, '') : '';
  }

  getAuthorName(term) {
    return this.stripTags(term).replace(' (Ed)', '');
  }

}
