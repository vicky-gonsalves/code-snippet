import {Component} from "@angular/core";
import {SessionStorageService} from 'ng2-webstorage';
import {SearchCollection} from "../search/services/search/searchCollection";
import {Search} from "../search/services/search/search";
import {ApiRequestService, Query} from "../core/services/apiRequestService";
import {LocationService} from "../core/services/locationService";
import {RecordFormatter} from "./services/format/recordFormatter";
import {DocumentTypeFormatter} from "./services/format/DocumentTypeFormatter";
import {IRecord} from "./services/format/IRecord";
import {Router, ActivatedRoute} from "@angular/router";
import {PsycNETQuery} from "../search/services/search/query/psycNETQuery";
import {Criteria} from "../search/services/search/query/components/criteria";
import {IResponseParameters} from "../search/services/search/query/components/interfaces";
import {IdentityService} from '../core/services/identity/identityService';
import {PermissionService} from '../core/services/identity/permissionService';
import * as _ from "lodash"
import {AuthService} from "../core/services/authService";
import {HighlightService} from '../shared/highlight/highlightService';


@Component({
  selector: 'recordDisplay',
  templateUrl: './recordDisplay.html',
  styleUrls: ['./recordDisplay.less']
})

export class RecordDisplay {

  recordSearch: Search;
  loading: boolean;
  breadcrumblist: Array<any>;
  record: IRecord;
  tabRecords: Array<IRecord>;
  testPrimaryRecord: IRecord;
  browseMode: string = '';
  totalCount: number;
  currentPage: number;
  searchId: string;
  isFromSearch: boolean = false;
  pageParams: any = {};
  isTransTitle: boolean = true;
  isNotInitialized = true;
  originIsDoi = false;
  isMissing = false;
  hasSFX: boolean = false;
  hasILL: boolean = false;
  sfxDisplayIcon: string;
  sfxDisplayText: string;
  illDisplayIcon: string;
  illDisplayText: string;


  constructor(private _apiService: ApiRequestService, private router: Router, private _route: ActivatedRoute, private _sessionStorageService: SessionStorageService, private _locationService: LocationService, private _authService: AuthService, private _permission: PermissionService) {
    //reset current page selected records
    this._sessionStorageService.clear('currentPageSearchRecords');
  }

  ngOnInit() {


    // TODO: remove this nesting when we upgrade Angular and use the different approaches available
    IdentityService.identity.subscribe(data => {
      // ensure identity is loaded
      if (!data) {
        return false;
      }

      this._route.params.subscribe(routeParams => {
        this._route.queryParams.subscribe(urlParams => {

          // centralize all page parameters
          this.pageParams = _.extend({}, routeParams, urlParams);

          // remove the &sr=1 param.  We only want to log searches with -RC when they come directly from the results
          delete this.pageParams.sr;

          this.pageParams.return = true;

          if (this.pageParams.hasOwnProperty('id') && this.pageParams['id']) {
            this.searchId = this.pageParams['id'];
            this.isFromSearch = true;
          }

          // determine if the user is requesting a record directly (via UID)
          if (this.pageParams.hasOwnProperty('uid') && this.pageParams['uid']) {

            // For PT records: check if the user has reached here via DOI link, to display the limited view.
            if ((this.pageParams['uid'].indexOf('9999-') === 0) && this.pageParams.hasOwnProperty('doi') && (+this.pageParams['doi'] === 1)) {
              this.originIsDoi = true;
            }

            this.searchId = this.getUIDSearch(this.pageParams['uid']);
          }

          if (!this.searchId) {
            this.router.navigate(['/search']);
          } else {
            let search = SearchCollection.get(this.searchId);
            if (search) {
              this.getRecordData(search);
            } else {
              this.getSearchById();
            }
          }

          this.hasSFX = this.hasProfileProp(data, 'sfx');
          this.hasILL = this.hasProfileProp(data, 'ill');

          if (this.hasSFXProp(data, 'displayIcon')) {
            this.sfxDisplayIcon = data.profile['sfx'].displayIcon;
          } else if (this.hasSFXProp(data, 'displayText')) {
            this.sfxDisplayText = data.profile['sfx'].displayText;
          }

          if (this.hasILLProp(data, 'displayIcon')) {
            this.illDisplayIcon = data.profile['ill'].displayIcon;
          } else if (this.hasILLProp(data, 'displayText')) {
            this.illDisplayText = data.profile['ill'].displayText;
          }

        });

      });

    });

  }

  private hasProfileProp(identity, prop: string): boolean {
    return identity && identity.hasOwnProperty('profile') && identity.profile && identity.profile.hasOwnProperty(prop) && identity.profile[prop];
  }

  private hasSFXProp(identity, prop: string): boolean {
    return this.hasSFX && identity.profile.sfx.hasOwnProperty(prop) && identity.profile.sfx[prop] && identity.profile.sfx[prop].length;
  }

  private hasILLProp(identity, prop: string): boolean {
    return this.hasILL && identity.profile.ill.hasOwnProperty(prop) && identity.profile.ill[prop] && identity.profile.ill[prop].length;
  }

  openLink(popupName: string, url: string) {
    window.open(url, popupName, "location=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=460,height=420");
  }

  private getRecordData(search: Search) {
    this.recordSearch = search;
    this.applyDisplayParams();
    this.setTotalCount();

    // get the record data
    this.performSearch();
  }

  private getUIDSearch(uid: string): string {
    let pnQuery = new PsycNETQuery();
    pnQuery.criteria.push(new Criteria('UID', 'OR', uid, 'user'));

    let responseParams: IResponseParameters = {
      start: 0,
      rows: 1,
      facet: false,
      results: true
    };

    let search = new Search(pnQuery, responseParams);

    // disable highlighting for UID searches
    search.responseParameters.hl = false;

    // apply metadata for reduced query calls
    search.metadata.uid = uid;
    search.metadata.directUID = true;

    search.metadata.fromDoi = this.originIsDoi;

    // add search to the collection
    SearchCollection.add(search);

    return search.id;
  }

  private getSearchById() {

    // if there is no 'id' url parameter, then this is a new search
    if (!this.searchId) {
      return false;
    }

    // get the recent search data and populate form
    let qry = this._apiService.createQuery('recentSearch.get');
    qry.params({id: this.searchId});

    this._apiService.execute(qry).subscribe(
      data => {
        if (data && data.query) {
          data.query.tab = this.pageParams.tab;
          SearchCollection.add(data);

          let searchCopy = SearchCollection.get(data.id);
          this.getRecordData(searchCopy);
        } else {
          this.isMissing = true;
        }
      });
  }

  private setTotalCount() {
    if (this.recordSearch && this.recordSearch.totalRecords) {
      this.totalCount = +this.recordSearch.totalRecords;
    }
  }

  public applyDisplayParams() {

    if (!this.recordSearch) {
      return false;
    }

    // indicate the record to return make sure only one result is returned
    this.recordSearch.responseParameters.rows = 1;
    if (this.pageParams['recordId']) {
      this.recordSearch.responseParameters.start = parseInt(this.pageParams['recordId']) - 1;
      this.pageParams.recordId = this.currentPage = parseInt(this.pageParams['recordId']);
    }
    if (this.pageParams['tab']) {
      this.recordSearch.query.tab = this.pageParams['tab'];
    }
    if (this.pageParams['display']) {
      this.pageParams.page = 1 + Math.floor((this.pageParams.recordId - 1) / this.pageParams.display);
    }
    if (this.pageParams['sort']) {
      this.recordSearch.query.displayOptions.sort = this._locationService.parseSortParam(this.pageParams['sort']);
    }
    SearchCollection.update(this.recordSearch);
  }

  public performSearch() {
    if (!this.recordSearch) {
      return false;
    }

    this.loading = true;

    let qry: Query = this._apiService.createQuery('search.record');
    qry.params(this.recordSearch);

    this._apiService.execute(qry).subscribe(
      data => {

        if (data.results && data.results.result && data.results.result.doc && data.results.result.doc.length) {
          let mainRecord = data.results.result.doc.slice(0, 1).shift();  //Get The First Doc
          console.log('Main record: ', mainRecord);

          this.record = RecordFormatter.convertToPNRecord(mainRecord);

          if (!(this.record.hasOwnProperty('DocumentType') && this.record.DocumentType.length)) {
            let documentTypeLabel = DocumentTypeFormatter.getDocumentTypeLabel(this.record);
            if (documentTypeLabel.trim().length) {
              documentTypeLabel = documentTypeLabel.replace(/(\/)/g, '/ ');
              this.record['DocumentTypeLabel'] = (documentTypeLabel.indexOf(';') > -1) ? documentTypeLabel.split(';')[0] : documentTypeLabel;
            }
          }

          console.log('Converted record: ', this.record);

          this._sessionStorageService.store("currentPageSearchRecords", [_.pick(this.record, ['UID', 'ProductCode'])]);

          if (this.record.hasOwnProperty('isPT') && this.record.isPT) {
            let tabRecords = data.results.result.doc.slice(1); //Skip First Object
            this.tabRecords = [];
            for (var doc of tabRecords) {
              var convertedDocument = RecordFormatter.convertToPNRecord(doc);
              if (doc.hasOwnProperty('TestRecordType') && doc['TestRecordType'] !== "Test Primary Data") {
                this.tabRecords.push(convertedDocument);
              } else {
                this.testPrimaryRecord = convertedDocument;
              }
              this.record.AdministrationMethods = convertedDocument.AdministrationMethods;
            }

            console.log('Tab Records: ', this.tabRecords);
            console.log('Test Primary Data: ', this.testPrimaryRecord);
          }

          if (this.record.hasOwnProperty('TransDocumentTitle') && this.record.TransDocumentTitle.length > 1) {
            this.compareTitleWithTransTitle();
          }
        }
        this.loading = false;
        this.isNotInitialized = false;
      },
      error => {
        this.loading = false;
        this.isNotInitialized = false;
        if (error.status === 403) {
          this.router.navigate(['/no-access'], {skipLocationChange: true});
        }
        console.log('Record Error: ', error);
      }
    );
  }

  /*Pagination method*/
  public getNewPagination(pagination) {
    if (pagination.hasOwnProperty('currentPage') && pagination.currentPage >= 0) {
      this.pageParams.recordId = pagination.currentPage;
      this.pageParams.page = 1 + Math.floor((this.pageParams.recordId - 1) / this.pageParams.display);
      this.pageParams.sort = this._locationService.generateSortParamString(this.recordSearch.query.displayOptions.sort);
      this.router.navigate(['/search/display'], {queryParams: this.pageParams});
    }
  }

  public getButtonBoxComponents() {
    let components = [];
    if (this.record) {
      components = ['pdf', 'full text', 'cited by'];
      if (this.record.hasOwnProperty('isPT') && this.record.isPT) {
        components = ['pdf', 'support-doc', 'media'];
      }
    }
    return components;
  }

  public getRecordActionBarComponents() {
    let components = [];
    if (this.record) {
      components = ['export', 'marked list', 'mail', 'print', 'share', 'labels'];
      if (this.record.hasOwnProperty('isPT') && this.record.isPT) {
        components = ['mail', 'print', 'share', 'labels'];
      }
    }
    return components;
  }

  compareTitleWithTransTitle() {
    let transTtile = HighlightService.removeHighlights(this.record.TransDocumentTitle);
    let title = HighlightService.removeHighlights(this.record.Title);
    this.isTransTitle = title == transTtile ? false : true;
  }

  ngOnDestroy() {

  }
}


