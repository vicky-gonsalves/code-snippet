import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { SearchCommonModule } from '../search/searchcommon.module';
import { RecordDisplayCommonModule } from './recordDisplayCommon.module';
import { FulltextCommonModule } from '../fulltext/fulltextCommon.module';


import {About} from "./components/about/about";
import {AllRecords} from "./components/allRecords/allRecords";
import {ButtonBox} from "./components/buttonBox/buttonBox";
import {IndexTerms} from "./components/indexTerms/indexTerms";
import {JournalTOC} from "./components/journalTOC/journalTOC";
import {PTRecordDisplay} from "./components/ptRecordDisplay/ptRecordDisplay";
import {PTTabsComponent} from "./components/ptTabsComponent/ptTabsComponent";
import {RecordActionBar} from "./components/recordActionBar/recordActionBar";
import {RecordDisplay} from "./recordDisplay";
import {RecordInfo} from "./components/ptRecordInfo/ptRecordInfo";
//import {RelatedContent} from "./components/relatedContent/relatedContent";
import {TestRecordsAccordion} from "./components/testRecordsAccordion/testRecordsAccordion";
import {ImpactStatement} from "./components/impactStatement/impactStatement";
import {Links} from "./components/links/links";


// Services
import {RecordFormatter} from './services/format/recordFormatter';
import {DocumentTypeFormatter} from './services/format/DocumentTypeFormatter';
import {SearchNavigation} from '../search/services/searchNavigation';


const declarations = [
  About,
  AllRecords,
  ButtonBox,
  IndexTerms,
  JournalTOC,
  PTRecordDisplay,
  PTTabsComponent,
  RecordActionBar,
  RecordDisplay,
  RecordInfo,
 // RelatedContent,
  TestRecordsAccordion,
  ImpactStatement,
  Links
];


import { routing } from './recordDisplay.routes';


@NgModule({
  imports: [SharedModule, SearchCommonModule, RecordDisplayCommonModule, FulltextCommonModule, routing],
  declarations: [
    ...declarations
  ],
  providers: [
    RecordFormatter,
    DocumentTypeFormatter,
    SearchNavigation
  ]
})
export class RecordDisplayModule {}


