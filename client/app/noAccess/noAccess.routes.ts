import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoAccess } from './noAccess';

export const routes: Routes = [
  {
    path: '',
    component: NoAccess
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

