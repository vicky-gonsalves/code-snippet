import { NgModule } from '@angular/core';
import { routing } from './noAccess.routes';

import { NoAccess } from './noAccess';

@NgModule({
  imports: [routing],
  declarations: [NoAccess]
})
export class NoAccessModule { }
