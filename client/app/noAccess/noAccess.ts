import { Component } from '@angular/core';

@Component({
  selector: 'no-access',
  templateUrl: './noAccess.html',
  styleUrls: ['./noAccess.less']
})
export class NoAccess {

  constructor() { }

  ngOnInit() { }

}
