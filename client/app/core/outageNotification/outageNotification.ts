import {Component} from '@angular/core';
import {ApiRequestService} from '../../core/services/apiRequestService';
import {SessionStorageService} from 'ng2-webstorage';

@Component({
  selector: 'outage-notification',
  templateUrl: './outageNotification.html',
  styleUrls: ['./outageNotification.less']
})
export class OutageNotification {

  outageData ={};
  isHidden: boolean;

  constructor(private _apiService: ApiRequestService, private _sessionStorageService : SessionStorageService) {
    this.isHidden = this._sessionStorageService.retrieve('hideOutage');
    this.addOutageNotification();
  }

  public addOutageNotification() {
    let query = this._apiService.createQuery('outage.getOutageData');
    this._apiService.execute(query).subscribe(
      data => {
        if(!data.msg) {
          this._sessionStorageService.clear('hideOutage');
        }
        this.outageData = data;
      },
      error => console.log(error)
    );
  }

  public hideFutureNotification($event) {
    this.isHidden = true;
    this._sessionStorageService.store('hideOutage', true);
  }

}
