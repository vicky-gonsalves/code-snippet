import {Component, Output, EventEmitter} from '@angular/core';
import { Locale, LocaleService, LocalizationService } from 'angular2localization';

import {MenuToggleNotifier} from '../services/menuToggleNotifier';
import {IdentityService} from '../services/identity/identityService';
import {DOILandingGuardService} from '../services/resourceGuardService';
import {PurchaseService} from '../services/purchase/purchaseService';
import {IIdentity} from '../services/identity/identity';
import {ApiRequestService, Query} from "../services/apiRequestService";
import {DatabaseFilterSubscription} from '../services/databaseSubscription/databaseFilterSubscription';
import {IDatabase} from '../services/databaseSubscription/database';
import {CookieService} from 'angular2-cookie/core';
import {Router} from '@angular/router'

@Component({
  selector: 'pnheader',
  templateUrl: './pnheader.html',
  styleUrls: ['./pnheader.less']
})
export class PNHeader extends Locale {
  @Output() comeSoon = new EventEmitter();

  private _identity: IIdentity;
  private cartCount: number;
  isOrganization: boolean;
  isViewable: boolean;
  browseList: Array<IDatabase>;
  mainNav: boolean;
  returnUrl: string;

  constructor(public _router: Router, public locale: LocaleService, public localization: LocalizationService, private _apiService: ApiRequestService, private _databaseFilterSubscription : DatabaseFilterSubscription, private _menuToggle: MenuToggleNotifier, private _cookieService:CookieService) {
    super(locale, localization);
    _databaseFilterSubscription.getSubscribedDatabase();
    this.returnUrl = encodeURIComponent(window.location.href);
  }

  ngOnInit() {
    IdentityService.identity.subscribe(data => {
      // ensure identity is loaded
      if(!data) {
        return false;
      }

      this._router.events.subscribe(() => this.returnUrl = encodeURIComponent(window.location.href));

      this._identity = data;
      this.isOrganization = this._identity.isOrganization;
      PurchaseService.count.subscribe(count => this.cartCount = count);
      DatabaseFilterSubscription.subscribedDatabase.subscribe(data => this.browseList = data.database);
      this.isViewable = (window.location.pathname !== '/doiLanding' && window.location.pathname !== '/doiLanding/removed' || !DOILandingGuardService.isAllowed(this._identity));
    });
  }

  triggerComingSoon() {
    this.comeSoon.emit({title: 'APA PsycNET Help', source: 'help'});
    return false;
  }

  // the other API Requests for this page will be cached in localStorage, so we don't want to make this log request on its own so that it's not missed
  public logPageView(pageId: string) {
    let query: Query = this._apiService.createQuery('log.logEntry');
    query.logs({pageId: pageId});
    this._apiService.execute(query).subscribe();
  }

  logPageViewAndToggleMobileMenu(pageId) {
    this.logPageView(pageId);
    this.toggleMobileMenu();
  }

  // Sets a new locale & currency.
  selectLocale(language: string, country: string, currency: string): void {
    this.locale.setCurrentLocale(language, country);
    this.locale.setCurrentCurrency(currency);
    if (language != 'en') {
      this.logPageView('Languages');
    }
  }

  setLocaleAndToggleMobileMenu(lang, country, currency) {
    this.selectLocale(lang, country, currency);
    this.toggleMobileMenu();
  }

  isAllowedToDisplay(code) {
    let result = false;
    if(this.browseList && this.browseList.length) {
      let menuOption = this.browseList.filter(option => { return option.code == code; });
      if(menuOption && menuOption.length) {
        result = true;
      }
    }
    return result;
  }

  isAllowedToDisplayThesaurus() {
    let result = false;
    if(this._identity && this._identity.entitlements.length) {
      result = true;
    }
    return result;
  }

  toggleMobileMenu(calledByMenuButton?: boolean) {
    this.mainNav = calledByMenuButton ? !this.mainNav : false;
    this._menuToggle.toggle(this.mainNav);
  }

  public getMyAPALink(){
    return this._cookieService.get('PN_MY') + '/portal/home';
  }
}
