import {APP_INITIALIZER, ModuleWithProviders, NgModule, Optional, SkipSelf, Injectable} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CookieService } from 'angular2-cookie/services/cookies.service';
import {AccordionModule, AlertModule, CollapseModule, DropdownModule, ModalModule, TooltipModule, TypeaheadModule} from 'ng2-bootstrap';
import {Ng2Webstorage} from 'ng2-webstorage';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { LocaleModule, LocalizationModule, LocaleService, LocalizationService  } from 'angular2localization';

import {SharedModule} from '../shared/shared.module';

import {environment} from '../../environments/environment';

// components
import { APALogo } from './apaLogo/apaLogo';
import { PNLogo } from './pnLogo/pnLogo';
import { IdentityBar } from './identitybar/identitybar';
import { Personalization } from './menu/personalization';
import { SearchMenu } from './menu/searchmenu';
import { BrowseMenu } from './menu/browsemenu';
import { ThesaurusMenu } from './menu/thesaurusmenu';
import { PNHeader } from './pnheader/pnheader';
import {OutageNotification} from './outageNotification/outageNotification';
import { PNFooter } from './pnfooter/pnfooter';
import { SessionRefresh } from './session/sessionRefresh/sessionRefresh';
import { SessionWarningDialog } from './session/sessionWarning/sessionWarningDialog';
import { SessionExpiredDialog } from './session/sessionExpire/sessionExpiredDialog';
import { IPAddressChangeDialog } from './session/ipAddressChangeDialog/ipAddressChangeDialog';
import { DownloadBlockMessage } from './session/downloadBlockMessage/downloadBlockMessage'

//services
import { ApiRequestService } from './services/apiRequestService';
import { PurchaseService } from './services/purchase/purchaseService';
import { MylistStorageService } from './services/mylistStorageService';
import { LocationService } from './services/locationService';
import { IdentityService } from './services/identity/identityService';
import { PermissionService } from './services/identity/permissionService';
import { DatabaseFilterSubscription } from './services/databaseSubscription/databaseFilterSubscription';
import { AuthService } from './services/authService';
import { AuthGuardService } from './services/authGuardService';
import { CitedRefGuardService, PAGuardService, PBGuardService, PCGuardService, PEGuardService, PTGuardService, PHGuardService, ThesaurusGuardService, DOILandingGuardService } from './services/resourceGuardService';
import { DefaultSearchPageService } from './services/defaultSearchPageService';
import { NavigationEvent } from './services/navigationEvent';
import { MenuToggleNotifier } from './services/menuToggleNotifier';

/**
 * Load translation file before loading the app
 */
@Injectable()
export class LocalizationConfig {

  constructor(public locale: LocaleService, public localization: LocalizationService) { }

  load(): Promise<any> {

    // Adds the languages (ISO 639 two-letter or three-letter code).
    this.locale.addLanguages(['de','en', 'es', 'fr', 'pt']);

    // Required: default language, country (ISO 3166 two-letter, uppercase code) and expiry (No days). If the expiry is omitted, the cookie becomes a session cookie.
    // Selects the default language and country, regardless of the browser language, to avoid inconsistencies between the language and country.
    this.locale.definePreferredLocale('en', 'US');

    // Optional: default currency (ISO 4217 three-letter code).
    this.locale.definePreferredCurrency('USD');

    // Initializes LocalizationService: asynchronous loading.
    this.localization.translationProvider('./assets/localization/core/locale-'); // Required: initializes the translation provider with the given path prefix.

    let promise: Promise<any> = new Promise((resolve: any) => {
      this.localization.translationChanged.subscribe(() => {
        resolve(true);
      });
    });

    this.localization.updateTranslation(); // Need to update the translation.

    return promise;
  }
}

/**
 * Aot compilation requires a reference to an exported function.
 */
export function initLocalization(localizationConfig: LocalizationConfig): Function {
  return () => localizationConfig.load();
}


@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    // ng2-bootstrap
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    CollapseModule.forRoot(),
    DropdownModule.forRoot(),
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot(),

    Ng2Webstorage,

    NgIdleKeepaliveModule.forRoot(),

    // localization
    LocaleModule.forRoot(),
    LocalizationModule.forRoot(),
    SharedModule
  ],
  declarations: [
    APALogo,
    PNLogo,
    IdentityBar,
    Personalization,
    SearchMenu,
    BrowseMenu,
    ThesaurusMenu,
    PNHeader,
    PNFooter,
    OutageNotification,
    SessionRefresh,
    SessionWarningDialog,
    SessionExpiredDialog,
    IPAddressChangeDialog,
    DownloadBlockMessage
  ],
  exports: [
    PNHeader,
    PNFooter,
    OutageNotification,
    SessionRefresh,
    SessionWarningDialog,
    SessionExpiredDialog,
    IPAddressChangeDialog,
    DownloadBlockMessage
  ],
  providers: [
    CookieService,
    ApiRequestService,
    PurchaseService,
    MylistStorageService,
    LocationService,
    IdentityService,
    PermissionService,
    DatabaseFilterSubscription,
    AuthService,
    AuthGuardService,
    CitedRefGuardService,
    PAGuardService,
    PBGuardService,
    PCGuardService,
    PEGuardService,
    PTGuardService,
    PHGuardService,
    ThesaurusGuardService,
    DOILandingGuardService,
    DefaultSearchPageService,
    NavigationEvent,
    MenuToggleNotifier,

    LocalizationConfig,
    {
      provide: APP_INITIALIZER, // APP_INITIALIZER will execute the function when the app is initialized and delay what it provides.
      useFactory: initLocalization,
      deps: [LocalizationConfig],
      multi: true
    }
  ]
})
export class CoreModule {
}
