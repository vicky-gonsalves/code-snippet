import {Component} from '@angular/core';
import {ApiRequestService, Query} from '../../services/apiRequestService';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector : 'session-refresh',
  templateUrl: './sessionRefresh.html',
  styleUrls: ['./sessionRefresh.less']
})
export class SessionRefresh{

  refreshURL :SafeResourceUrl;

  constructor(private _apiRequest : ApiRequestService, private _sanitizer: DomSanitizer){
    this.refreshURL = this._sanitizer.bypassSecurityTrustResourceUrl("");
  }

  ngOnInit() {
    let qry:Query = this._apiRequest.createQuery("session.refresh");
    this._apiRequest.execute(qry).subscribe(response => {
      this.refreshURL = this._sanitizer.bypassSecurityTrustResourceUrl(response.url);
    });
  }
}
