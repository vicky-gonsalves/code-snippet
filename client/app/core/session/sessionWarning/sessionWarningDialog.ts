import {Component, ViewChild, Output, EventEmitter} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap';

@Component({
  selector : 'session-warning',
  templateUrl: './sessionWarningDialog.html',
  styleUrls: ['./sessionWarningDialog.less']
})
export class SessionWarningDialog{
  @Output() closeDialog = new EventEmitter();
  @ViewChild('sessionWarningModal') public sessionWarningModal: ModalDirective;

  openModal():void {
    this.sessionWarningModal.show();
  }

  closeModal():void {
    this.sessionWarningModal.hide();
    this.closeDialog.emit(true);
  }

  isOpen(): boolean{
    return this.sessionWarningModal.isShown;
  }
}
