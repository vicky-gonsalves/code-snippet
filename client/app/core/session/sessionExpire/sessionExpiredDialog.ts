import {Component, ViewChild} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap';

@Component({
  selector : 'session-expired',
  templateUrl: './sessionExpiredDialog.html',
  styleUrls: ['./sessionExpiredDialog.less']
})
export class SessionExpiredDialog{
  @ViewChild('sessionExpireModal') public sessionExpireModal: ModalDirective;

  openModal():void {
    this.sessionExpireModal.show();
  }

  closeModal():void {
    this.sessionExpireModal.hide();
  }

  isOpen(): boolean{
    return this.sessionExpireModal.isShown;
  }
}
