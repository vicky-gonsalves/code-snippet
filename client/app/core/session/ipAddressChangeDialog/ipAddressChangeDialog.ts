import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';

@Component({
  selector: 'ip-address-changed',
  templateUrl: './ipAddressChangeDialog.html',
  styleUrls: ['./ipAddressChangeDialog.less']
})
export class IPAddressChangeDialog {
  @ViewChild('ipAddressChangedModal') public ipAddressChangedModal: ModalDirective;

  constructor() { }

  ngOnInit() { }

  openModal(): void {
    this.ipAddressChangedModal.show();
  }

  closeModal(): void {
    this.ipAddressChangedModal.hide();
  }

  isOpen(): boolean {
    return this.ipAddressChangedModal.isShown;
  }
}
