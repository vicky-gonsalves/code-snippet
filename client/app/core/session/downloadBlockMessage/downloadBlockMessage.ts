import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';

@Component({
  selector: 'download-block-message',
  templateUrl: './downloadBlockMessage.html',
  styleUrls: ['./downloadBlockMessage.less']
})
export class DownloadBlockMessage {
  @ViewChild('downloadBlockMessage') public downloadBlockMessageModal: ModalDirective;

  constructor() { }

  ngOnInit() { }

  openModal(): void {
    this.downloadBlockMessageModal.show();
  }

  closeModal(): void {
    this.downloadBlockMessageModal.hide();
  }

  isOpen(): boolean {
    return this.downloadBlockMessageModal.isShown;
  }
}
