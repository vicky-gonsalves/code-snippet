import { Component, ElementRef } from '@angular/core';

import * as _ from 'lodash';

import { PermissionService } from '../services/identity/permissionService';

@Component({
  selector: 'searchmenu',
  templateUrl: './searchmenu.html',
  styleUrls: ['./commonmenu.less', './searchmenu.less']
})
export class SearchMenu {
  searchToggle = false;

  constructor(private _eref: ElementRef, private _permission: PermissionService) {
    document.addEventListener('click', this.onClick.bind(this));
  }

  onClick(event) {
    let clickedComponent = event.target;
    let inside = false;
    do {
      if (clickedComponent === this._eref.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);

    if (inside) {
      this.searchToggle = !this.searchToggle;
    } else {
      this.searchToggle = false;
    }
  }

  showCitedRefsItem(): boolean {
    return this._permission.mayAccessCitedRefSearch();
  }
}
