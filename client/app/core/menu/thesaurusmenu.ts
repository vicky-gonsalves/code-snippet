import {Component} from '@angular/core';
import {Identity} from '../services/identity/identity';
import {IdentityService} from '../services/identity/identityService';
import {Locale, LocalizationService} from "angular2localization";

@Component({
  selector: 'thesaurusmenu',
  templateUrl: './thesaurusmenu.html',
  styleUrls: ['./thesaurusmenu.less']
})
export class ThesaurusMenu extends Locale{

  private identity: Identity;
  isAllowed: boolean = false;

  constructor(public localization: LocalizationService) {
    super(null, localization);
  }

  ngOnInit() {
    IdentityService.identity.subscribe(data => {
      this.identity = data;
      this.checkAuthorization();
    });
  }

  checkAuthorization() {
    if(this.identity && this.identity.entitlements.length) {
      this.isAllowed = true;
    }
  }
}
