import {Component} from '@angular/core';
import {IdentityService} from '../services/identity/identityService';
import {MylistStorageService} from "../services/mylistStorageService";
import {SessionStorageService} from 'ng2-webstorage';

@Component({
  selector: 'personalization',
  templateUrl: './personalization.html',
  styleUrls: ['./personalization.less']
})
export class Personalization {
  mylistCount = 0;

  constructor(private _sessionStorageService : SessionStorageService) {

  }

  ngOnInit() {

    IdentityService.identity.subscribe(identity => {
      // ensure identity is loaded
      if(!identity){
        return false;
      }

      MylistStorageService.count.subscribe(count => this.mylistCount = count);
    });

  }

  resetMyList(){
    this._sessionStorageService.clear('selectedSearchRecords');
    this._sessionStorageService.clear('currentPageSearchRecords');
  }
}
