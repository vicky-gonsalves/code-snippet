/*
import {it, inject, beforeEachProviders } from '@angular/core/testing';
import {provide} from '@angular/core';
import {BaseRequestOptions, Http} from '@angular/http';
import {MockBackend} from '@angular/http/testing';

// Load the implementations that should be tested
import {BrowseMenu} from './browsemenu';
import {BrowseList} from './services/browselist';
import {ApiRequestService} from './../services/apiRequestService';

describe('BrowseMenu', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEachProviders(() => [
    BaseRequestOptions,
    MockBackend,
    provide(Http, {
      useFactory: function(backend, defaultOptions) {
        return new Http(backend, defaultOptions);
      },
      deps: [MockBackend, BaseRequestOptions]
    }),

    BrowseList,
    BrowseMenu,
    ApiRequestService
  ]);

  it('should have data', inject([ BrowseMenu ], (browseMenu) => {
    expect(browseMenu.data).toEqual({ value: '' });
  }));

 // it('should have a browselist', inject([ BrowseMenu ], (browseMenu) => {
  //  expect(!!browseMenu.browselist).toEqual(true);
  //}));

  it('should log ngOnInit', inject([ BrowseMenu ], (browsemenu) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();

    browsemenu.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
*/
