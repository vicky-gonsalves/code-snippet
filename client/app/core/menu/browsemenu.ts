import {Component,ElementRef} from '@angular/core';
import {DatabaseFilterSubscription} from '../services/databaseSubscription/databaseFilterSubscription';
import {IDatabase} from '../services/databaseSubscription/database';
import { Locale, LocaleService, LocalizationService } from 'angular2localization';

@Component({
  selector: 'browsemenu',
  templateUrl: './browsemenu.html',
  styleUrls: ['./commonmenu.less','./browsemenu.less']
})
export class BrowseMenu extends Locale {
  browseList: Array<IDatabase>;
  browseToggle= false;

  data = { value:''};

  constructor(public locale: LocaleService, public localization: LocalizationService, private _databaseFilterSubscription : DatabaseFilterSubscription,private _eref: ElementRef) {
    super(locale, localization);

    document.addEventListener('click', this.onClick.bind(this));
    _databaseFilterSubscription.getSubscribedDatabase();
  }

  ngOnInit() {
    DatabaseFilterSubscription.subscribedDatabase.subscribe(data => this.browseList = data.database);
  }

  onClick(event){

       let clickedComponent = event.target;
       let inside = false;
       do {

           if (clickedComponent === this._eref.nativeElement) {
               inside = true;
           }
           clickedComponent = clickedComponent.parentNode;
       } while (clickedComponent);


       if(inside){
          this.browseToggle=! this.browseToggle;
       }
       else
         this.browseToggle=false;
   }


}




