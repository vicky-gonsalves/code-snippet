import {Component} from '@angular/core';
import {IdentityService} from "../services/identity/identityService";

@Component({
  selector: 'apaLogo',
  templateUrl: './apaLogo.html',
  styleUrls: ['./apaLogo.less']
})
export class APALogo {
  customLogoImage: string;
  customLogoImageLink: string;

  constructor() {

  }

  ngOnInit() {
    IdentityService.identity.subscribe(identity => {
      // ensure identity is loaded
      if (!identity) {
        return false;
      }

      let isOrganization = identity.isOrganization;

      if (isOrganization && identity && identity.hasOwnProperty('profile') && identity.profile && identity.profile.hasOwnProperty('cobranding') && identity.profile.cobranding) {
        if (identity.profile.cobranding.hasOwnProperty('imagePath') && identity.profile.cobranding.imagePath && identity.profile.cobranding.imagePath.length) {
          this.customLogoImage = identity.profile.cobranding.imagePath;
        }
        if (identity.profile.cobranding.hasOwnProperty('imageLink') && identity.profile.cobranding.imageLink && identity.profile.cobranding.imageLink.length) {
          this.customLogoImageLink = identity.profile.cobranding.imageLink;
        }
      }
    });
  }
}
