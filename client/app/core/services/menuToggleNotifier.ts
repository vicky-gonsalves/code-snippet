import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class MenuToggleNotifier {
  private _toggleSubject = new Subject<boolean>();

  constructor() { }

  public get toggle$() {
    return this._toggleSubject.asObservable();
  }

  public toggle(on: boolean) {
    this._toggleSubject.next(on);
  }
}
