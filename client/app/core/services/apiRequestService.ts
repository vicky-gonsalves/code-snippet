import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class ApiRequestService {

  private _error: BehaviorSubject<string>;

  constructor(private _http: Http, private _router: Router) {
    this._error = new BehaviorSubject<string>('');
  }

  public get error() {
    return this._error.asObservable();
  }

  /**
   * createQuery - creates a query object
   * @param apiName - name of service to apply query parameters to
   * @returns the query object created
   */
  createQuery(apiName: string): Query {
    return new Query(apiName);
  }

  /**
   * execute - Sends an HTTP POST request to the server
   * @param query - query object that contains query parameter data
   * @returns {RxJS Observable}
   */
  execute(query: Query): Observable<any> {
    console.log('API: ', query.getApiName());

    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('X-Requested-With', 'XMLHttpRequest');

    return this._http.post('/api/request/' + query.getApiName(), JSON.stringify(query.buildRequest()), {headers: headers})
      .map(res => res.json())
      .map((data: any) => {
        console.log('API: ' + query.getApiName(), data);
        return data;
      }).catch((error: Response | any)=> {
        if (error && error.hasOwnProperty("_body")) {
          let errorBody = JSON.parse(error._body);

          if(error.status === 403) {
            if(errorBody.source === 'ip-check') {
              this._error.next('IP Address Error');
            } else if (errorBody.source === 'download-block') {
              this._error.next('Exceeded the number of downloading');
            } else if(errorBody.source === 'login') {
              this._router.navigateByUrl('/user/logout');
            } else if(errorBody.source == 'ftVerify') {
               window.location.href = '/fulltext/verify'
            }
          }
        }
        return Observable.throw(error);
      });
  }

  updateSessionConfirmation() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('X-Requested-With', 'XMLHttpRequest');

    return this._http.get('/user/confirmSession/', {headers: headers})
      .map(res => res.json())
      .map((data: any) => {
        console.log('API: confirmSession', data);
        return data;
      });
  }

}


export class Query {
  // name of the service to apply query parameters to
  private _apiName: string; // public by default
  // query parameters
  private _params = {};

  private _logs = {};

  constructor(apiName: string) {
    this._apiName = apiName;
  }

  /**
   * getApiName - gets name of the service to apply query parameters to
   * @returns name of service
   */
  getApiName(): string {
    return this._apiName;
  }

  /**
   * param - Allows you to set an individual parameter
   * @param key - name of the parameter
   * @param value - value of the parameter
   * @returns this - Allows for chainable calls
   */
  param(key: string, value: string) {
    if (!key || !value) {
      throw new Error("You must provide a key-value pairs for the function param(key,value)");
    }

    this._params[key] = value;

    return this;
  }

  /**
   * params - Allows you to set pass an object to set parameters
   * @param obj - Key-Value pairs of parameters
   * @returns this - Allows for chainable calls
   */
  params(obj: Object) {
    for (var param in obj) {
      if (obj.hasOwnProperty(param)) {
        this._params[param] = obj[param];
      }
    }

    return this;
  }

  /**
   * logs - fields and values for reporting
   * @param obj - Key-Value pairs of parameters
   * @returns this - Allows for chainable calls
   */
  logs(obj: Object) {
    for (var param in obj) {
      if (obj.hasOwnProperty(param)) {
        this._logs[param] = obj[param];
      }
    }

    return this;
  }

  /**
   * getAllParams - Gets all params set by user
   * @returns params object
   */
  getAllParams() {
    return this._params;
  }

  /**
   * buildRequest - The necessary data sent to the server side for request processing
   * @returns {{api: *, params: *}}
   */
  buildRequest(): Object {
    let request = {
      api: this.getApiName(),
      params: this.getAllParams()
    };

    if(Object.keys(this._logs).length){
      request['logs'] = this._logs;
    }

    return request;
  }
}
