import * as _ from 'lodash';
import {Injectable} from '@angular/core';
import {IdentityService} from './identityService';
import {Identity} from './identity';

@Injectable()
export class PermissionService {
  private _identity: Identity = new Identity();
  private _defaultIndividualAccessDB = ["PI","PA","PB","PC","PE"];

  constructor(){
    IdentityService.identity.subscribe(data => {
      // ensure identity is loaded
      if(!data){
        return false;
      }

      this._identity = data;

    });
  }

  public getDefaultIndividualDBs():string[]{
    return this._defaultIndividualAccessDB;
  }

  public hasEntitlement(code: string): boolean{
    let entitlements = _.map(this._identity.entitlements, 'code');
    return entitlements.indexOf(code.toUpperCase()) >= 0;
  }

  public hasDirectAsset(uid: string): boolean{
    return (this._identity.directProducts.indexOf(uid) >= 0);
  }

  public hasHandbook(name: string): boolean{
    return this.hasEntitlement(name);
  }

  public hasSingleHandbook(): boolean{
    let entitlements = _.map(this._identity.entitlements, 'code');
    let handbooks = _.filter(entitlements, o => {
      return o && String(o).indexOf('HBELEC') >= 0;
    });

    return ( handbooks.length === 1 );
  }

  public hasBookcollection(name: string): boolean{
    return this.hasEntitlement('PBC'+name);
  }


  public mayAccessRecord(record: any): boolean{

    // grant acecss to all Open Access records
    if(record.hasOwnProperty('HasOpenAccess') && record.HasOpenAccess === true){
      return true;
    }

    // check for direct asset access
    if( record.hasOwnProperty('UID') && this.hasDirectAsset(record.UID) ){
      return true;
    }

    // check if there is an entitlement that meets the configuration 'requirement'
    return this.entitlementsMeetRecordAccessRequirement(record);
  }

  public mayAccess(productOrGroupName: string): boolean{
    return (this.hasEntitlement(productOrGroupName) || this.hasGroupAccess(productOrGroupName));
  }


  public mayAccessAny(productOrGroupNames: string[]): boolean{
    for( let i=0; i<productOrGroupNames.length; i++){
      if(this.mayAccess(productOrGroupNames[i])){
        return true;
      }
    }

    return false;
  }

  public mayAccessAll(productOrGroupNames: string[]): boolean{
    for( let i=0; i<productOrGroupNames.length; i++){
      if(!this.mayAccess(productOrGroupNames[i])){
        return false;
      }
    }

    return true;
  }


  public entitlementsMeetRecordAccessRequirement(record: any): boolean{

    for( let i=0; i<this._identity.entitlements.length; i++){
      let product = this._identity.entitlements[i];

      for( let j=0; j<product.access.length; j++){
        let access = product.access[j];

        if( record.hasOwnProperty(access.field) ){

          // most access cases will resolve in this condition so we do this simple check for performance
          if ( record[access.field] === access.value ) {
            return true;
          }

          // BookCollection is an array, so we have to handle those differently
          if ( record[access.field] instanceof Array && record[access.field].indexOf(access.value) >= 0) {
            return true;
          }

        }
      }
    }

    return false;
  }

  public hasGroupAccess(productOrGroupName:string) {

    for( let i=0; i<this._identity.entitlements.length; i++){
      let groups = this._identity.entitlements[i].groups;
      if(groups && groups.indexOf(productOrGroupName) >= 0){
        return true;
      }
    }

    return false;
  }

  // gets all products that have a certain group, such as 'handbooks'
  public getProductsCodesByGroup(group: string){
    let products= [];

    this._identity.entitlements.forEach(product => {
      if(product.groups.indexOf(group) >= 0){
        products.push(product.code);
      }
    });

    return products;
  }

  // gets the available search databases for the user
  public getSearchDatabases(){
    let ownedDatabases = this.getProductsCodesByGroup('database');
    let dbs = ownedDatabases;

    if(this._identity.isOrganization){
      dbs = this.getProductsCodesByGroup('database');

      // if the user does not have PA, but has print subscriptions, then add PA back in.  It will get filtered appropriately on the backend.
      if(dbs.indexOf("PA") === -1 && this.hasGroupAccess('printSubscription')){
        dbs.push("PA");
      }

      // If the user does not have "PB", but has (book collections), then add "PB" back in. It will get filtered appropriately on the backend.
      if (dbs.indexOf("PB") === -1 && this.hasGroupAccess('bookCollection')) {
        dbs.push("PB");
      }

    }else{
      // individuals search the 5 main databases plus other databases they own
      dbs = _.union(this._defaultIndividualAccessDB, ownedDatabases);
    }

    return dbs;
  }

  public mayAccessAllDefaultProducts(): boolean{
    return this.mayAccessAll(this._defaultIndividualAccessDB);
  }

  public mayAccessBrowsePASection(): boolean {
    let mayAccess = true;

    if(this._identity.isOrganization && (!this.mayAccess('PA') && !this.hasGroupAccess('printSubscription'))){
      mayAccess = false;
    }

    return mayAccess;
  }

  public mayAccessBrowsePBSection() {
    let mayAccess = true;

    if (this._identity.isOrganization && !(this.mayAccess('PB') || this.hasGroupAccess('bookCollection'))) {
      mayAccess = false;
    }

    return mayAccess;
  }

  public mayAccessCitedRefSearch(): boolean {
    return this.mayAccess('PI') || this.mayAccess('PA') || this.mayAccess('PB');
  }
}
