import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/share';
import {Identity} from './identity';
import {ApiRequestService, Query} from '../apiRequestService';
import {MylistStorageService} from "../mylistStorageService";
import {PurchaseService} from "../purchase/purchaseService";

@Injectable()
export class IdentityService {
  private static _identity = new BehaviorSubject<Identity>(null);

  constructor(private _apiService: ApiRequestService, private _mylistStorageService: MylistStorageService, private _purchaseService: PurchaseService){

  }

  public static get identity() {
    return IdentityService._identity.filter(data => data != null);
  }

  public populateIdentity(): void {
    let qry:Query  = this._apiService.createQuery('session.getIdentity');
    this._apiService.execute(qry).subscribe(data => {

      // update the user's identity
      this.updateIdentity(data);

      // get the user's my list records
      if(data.isIndividual){
        this._mylistStorageService.refreshCount();
      }

      // get the cart status if this is not an organization
      if(!data.isOrganization){
        this._purchaseService.refreshCartStatus();
      }
    });
  }

  public updateIdentity(identity){
    console.log(IdentityService._identity);
    IdentityService._identity.next(identity);
  }

}
