/*
import {it, inject, beforeEachProviders } from '@angular/core/testing';
import {provide} from '@angular/core';
import {BaseRequestOptions, Http} from '@angular/http';
import {MockBackend} from '@angular/http/testing';

// Load the implementations that should be tested
import {PermissionService} from './permissionService';
import {IdentityService} from './identityService';
import {ApiRequestService} from '../apiRequestService';


//NOTE: Since IdentityService.identity is static and shared across instances, the entitlements are set within identityService.spec.ts
// and are used within this test.


describe('PermissionService', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEachProviders(() => [
    BaseRequestOptions,
    MockBackend,
    provide(Http, {
      useFactory: function(backend, defaultOptions) {
        return new Http(backend, defaultOptions);
      },
      deps: [MockBackend, BaseRequestOptions]
    }),
    ApiRequestService,
    IdentityService,
    PermissionService
  ]);

  it('should have access to PI and PB', inject([ PermissionService], (permissionService) => {
    expect(permissionService.hasEntitlement('PB')).toEqual(true);
    expect(permissionService.hasGroupAccess('printSubscription')).toEqual(false);
    expect(permissionService.hasGroupAccess('bookCollection')).toEqual(true);
    expect(permissionService.hasGroupAccess('psycscans')).toEqual(false);
    expect(permissionService.hasGroupAccess('psyctherapysubset')).toEqual(false);
  }));

  it('should have access to PB record but not PA', inject([ PermissionService ], (permissionService) => {
    expect(permissionService.mayAccessRecord({ProductCode: "PA"})).toEqual(false);
    expect(permissionService.mayAccessRecord({ProductCode: "PB"})).toEqual(true);
  }));


});

*/
