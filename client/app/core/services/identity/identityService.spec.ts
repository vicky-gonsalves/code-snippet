/*
import {it, inject, beforeEachProviders } from '@angular/core/testing';
import {provide} from '@angular/core';
import {BaseRequestOptions, Http} from '@angular/http';
import {MockBackend} from '@angular/http/testing';


// Load the implementations that should be tested
import {IdentityService} from './identityService';
import {ApiRequestService} from '../apiRequestService';

// NOTE: Since IdentityService.identity is static and shared across instances, the entitlements are set within identityService.spec.ts
// and are used within permissionService.spec.ts


describe('IdentityService', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEachProviders(() => [
    BaseRequestOptions,
    MockBackend,
    provide(Http, {
      useFactory: function(backend, defaultOptions) {
        return new Http(backend, defaultOptions);
      },
      deps: [MockBackend, BaseRequestOptions]
    }),
    ApiRequestService,
    IdentityService
  ]);

  it('should have set identity to mock user', inject([IdentityService], (identityService) => {
    identityService.updateIdentity({
      "isOrganization": false,
      "isIndividual": true,
      "name": "Mock Individual",
      "entitlements": [
        {
          "code": "PI",
          "groups": [
            "database"
          ],
          "access": [
            {
              "field": "ProductCode",
              "value": "PI"
            }
          ]
        },
        {
          "code": "PB",
          "groups": [
            "database"
          ],
          "access": [
            {
              "field": "ProductCode",
              "value": "PB"
            }
          ]
        },
        {
          "code": "JJHBELEC",
          "groups": [
            "bookCollection",
            "handbook"
          ],
          "access": [
            {
              "field": "BookCollection",
              "value": "JJHBELEC"
            }
          ]
        },
        {
          "code": "FRHBELEC",
          "groups": [
            "bookCollection",
            "handbook"
          ],
          "access": [
            {
              "field": "BookCollection",
              "value": "FRHBELEC"
            }
          ]
        }
      ]
    });

    IdentityService.identity.subscribe(identity => {
      expect(identity.isOrganization).toEqual(false);
      expect(identity.isIndividual).toEqual(true);
    });
  }));

});
*/
