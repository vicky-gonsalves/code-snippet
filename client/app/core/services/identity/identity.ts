/**
 * Client-side identity and interfaces
 */

// create identity interface
export interface IIdentity{
  athens: boolean;
  isOrganization: boolean;
  isIndividual: boolean;
  name: string;
  email: string;
  entitlements: IEntitlement[];
  directProducts: string[];
  profile: IProfile;
}

// client-side entitlement definition
interface IEntitlement {
  code: string,
  groups: string[],
  access: any
}

interface IProfile {
  defaultPage: string;
  cobranding: ICobranding;
}

interface ICobranding {
  imagePath: string;
  imageLink: string;
}


// an identity consists of both an organization indicator, an individual name, and their combined entitlements
export class Identity implements IIdentity{
  athens: boolean = false;
  isOrganization: boolean = false;
  isIndividual: boolean = false;
  name: string = '';
  email: string = '';
  entitlements: IEntitlement[] = [];
  directProducts: string[] = [];
  profile: IProfile = null;

  constructor(data?: Identity){
    if(data){
      this.athens = data.athens;
      this.isOrganization = data.isOrganization;
      this.isIndividual = data.isIndividual;
      this.name = data.name;
      this.email = data.email;
      this.entitlements = data.entitlements;
      this.directProducts = data.directProducts;
      this.profile = data.profile;

    }
  }
}
