import {Injectable} from '@angular/core';
import {SessionStorageService} from 'ng2-webstorage';
import {BehaviorSubject} from "rxjs";
import {ApiRequestService} from './apiRequestService';

@Injectable()
export class MylistStorageService {
  private static _count = new BehaviorSubject<number>(0);
  private _key = 'mylist';

  constructor(private _apiService: ApiRequestService, private _storage : SessionStorageService){

  }

  public static get count() {
    return MylistStorageService._count.asObservable();
  }

  public getUIDs(): string[]{
    return this._storage.retrieve(this._key) || [];
  }

  public removeStorage() {
    this._storage.clear(this._key);
  }

  public refreshCount(): void {
    let qry = this._apiService.createQuery('mylist.getMyListUIDs');
    this._apiService.execute(qry).subscribe(data => {
      this.save(data);
    });
  }

  public broadcastCurrentCount() {
    let uidList = this.getUIDs();
    MylistStorageService._count.next(uidList.length);
  }

  public has(uid: string){
    let uidList = this.getUIDs();

    return (uidList.indexOf(uid) >= 0);
  }

  public add(uid: string){
    let uidList = this.getUIDs();

    if(uidList.indexOf(uid) === -1){
      uidList.push(uid);
      this.save(uidList);
    }
  }

  public remove(uid: string){
    let uidList = this.getUIDs();
    let index = uidList.indexOf(uid);

    if(index >= 0){
      uidList.splice(index, 1);
      this.save(uidList);
    }
  }

  private save(uidList){
    this._storage.store(this._key, uidList);
    this.broadcastCurrentCount();
  }

}
