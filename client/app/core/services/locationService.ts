import {Injectable} from "@angular/core";
import {Location} from "@angular/common";

@Injectable()
export class LocationService {
    constructor(private _location: Location) {
    }

    public goBack() {
      this._location.back();
    }

    public updateLocation(path: string, params?: any[]) {
        let targetUrl = path;
        if(params && params.length) {
            targetUrl += this.generateQueryParams(params);
        }

        console.info('Location has changed: ', targetUrl);
        this._location.replaceState(targetUrl);
    }

    public generateSortParamString(sortOptions: any[]) {
        let sortParam = '';
        if(sortOptions) {
            for(let param of sortOptions) {
                sortParam += param.field + ' ' + param.direction + ',';
            }
            sortParam = sortParam.substr(0, sortParam.length - 1);
        }
        return sortParam;
    }

    public parseSortParam(str: string): any[] {
        let sortCriteria = [];
        let sortParams = str.split(',');
        for(let param of sortParams) {
            let fragmentedParam = param.split(' ');
            sortCriteria.push({
                field: fragmentedParam[0],
                direction: fragmentedParam[1]
            });
        }
        return sortCriteria;
    }

    public parseQueryParams(queryParams: any, targetType?: string): any {
        let result = {};

        // Default case: target is "Search Results".
        // Add more cases here as needed.
        if(queryParams) {
          if (targetType && targetType === 'cited-by') {
            result = this.parseQueryParamsForCitedBy(queryParams);
          } else {
            result = this.parseQueryParamsForSearchResults(queryParams);
          }
        }

        return result;
    }

    private generateQueryParams(params: any[]): string {
        let queryParams = '?';
        if(params) {
            for(let param of params) {
                queryParams += param.name + '=' + param.value + '&';
            }
            queryParams = queryParams.substr(0, queryParams.length - 1);
        }
        return queryParams;
    }

    private parseQueryParamsForCitedBy(queryParams: any): any {
      let result = {
        page: 1,
        rows: 25
      };

      if (queryParams['page']) {
        result.page = +queryParams['page'];
      }
      if (queryParams['display'] && (['25', '50']).indexOf(queryParams['display']) > -1) {
        result.rows = +queryParams['display'];
      }

      return result;
    }

    private parseQueryParamsForSearchResults(queryParams: any): any {
        let result = {
            id: '',
            tab: 'all',
            page: 1,
            rows: 25,
            sort: [{field: 'PublicationYearMSSort', direction: 'desc'}, {field: 'AuthorSort', direction: 'asc'}]
        };

        if(queryParams['id']) {
            result.id = queryParams['id'];
        }
        if(queryParams['tab']) {
            result.tab = queryParams['tab'];
        }
        if(queryParams['page']) {
            result.page = +queryParams['page'];
        }
        if(queryParams['display'] && (["25", "50"]).indexOf(queryParams['display']) > -1) {
            result.rows = +queryParams['display'];
        }
        if(queryParams['sort']) {
            result.sort = this.parseSortParam(queryParams['sort']);
        }

        return result;
    }
};
