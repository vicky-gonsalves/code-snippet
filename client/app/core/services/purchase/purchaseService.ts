import {Injectable} from '@angular/core';
import {CookieService} from 'angular2-cookie/core';
import {BehaviorSubject} from "rxjs";
import {Observable} from 'rxjs/Rx';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import * as _ from 'lodash';

import {IRecord} from '../../../recordDisplay/services/format/IRecord';
import {CartItem} from './cartItem';
import {ItemCodeService} from './itemCodeService';

@Injectable()
export class PurchaseService {
  private static _count = new BehaviorSubject<number>(0);
  private cartItems = [];
  private singleDayPassCodes = [];

  private readonly databases = {
    PI: {
      cartName: 'PsycINFO Day Pass',
      cartDescription: '24 hr pass to PsycINFO',
      cartImageTag: 'db_psycinfo',
      productUrl: '/search/'
    },
    PC: {
      cartName: 'PsycCRITIQUES Day Pass',
      cartDescription: '24 hr pass to PsycCRITIQUES',
      cartImageTag: 'db_psyccritiques',
      productUrl: '/PsycCRITIQUES/'
    },
    PE: {
      cartName: 'PsycEXTRA Day Pass',
      cartDescription: '24 hr pass to PsycEXTRA',
      cartImageTag: 'db_psycextra',
      productUrl: '/PsycEXTRA/'
    }
  };


  constructor(private _http: Http, private _cookieService:CookieService) {
    for(let db in this.databases) {
      this.singleDayPassCodes.push(ItemCodeService.getCode(db));
      this.databases[db].productUrl = this.getSiteHost() + this.databases[db].productUrl;
    }
  }

  public static get count() {
    return PurchaseService._count.asObservable();
  }

  public refreshCartStatus(): void {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let requestOptions = new RequestOptions({ headers: headers, withCredentials: true });
    let url = this.getMyHost() + '/apa/rest/commerce/getPsycnetCartSummary';

    // using HTTP instead of apiRequest for better proxying
    this._http.post(url, JSON.stringify({}), requestOptions)
      .toPromise()
      .then((res: Response)=>{
        let data = res.json() || {};
        this.cartItems = [];

        // convert to an array of string values instead of objects for easier lookup
        if(data.items && data.items.length){
          data.items.forEach(item => {
            let value = (item.uid === '0') ? item.itemId : item.uid;
            this.cartItems.push(value);
          });
        }
        PurchaseService._count.next(this.cartItems.length);

      }).catch((err: Response | any)=> console.error(err));
  }

  public addRecord(record: IRecord) {
    if(record) {
      let cartItem: CartItem;
      if(record.ProductCode === 'PA') {
        cartItem = this.getArticleItem(record);
      } else if(record.ProductCode === 'PB') {
        cartItem = this.getBookChapterItem(record);
      }

      if(cartItem) {
        let url = this.getMyHost() + '/apa/rest/commerce/addPsycnetProductToCart';
        this.performTransaction(url, cartItem);
      }
    }
  }

  public addDatabase(code: string) {
    // Adding all the single day pass options will be substituted by adding the combo option.
    code = (_.intersection(this.singleDayPassCodes, this.cartItems).length > 1) ? 'combo' : code;

    if(code === 'combo') {
      // Remove all SINGLE daypass items from the cart.
      let cartCopy = _.cloneDeep(this.cartItems);
      for(let item of cartCopy) {
        if(this.singleDayPassCodes.indexOf(item) >= 0) {
          let url = this.getMyHost() + '/apa/rest/commerce/removeProduct';
          this.performTransaction(url, item, 'application/text');
        }
      }
    }

    if(['PI', 'PC', 'PE', 'combo'].indexOf(code) >= 0) {
      let cartItem = code === 'combo' ? this.getComboDbItem() : this.getDatabaseItem(code);
      let url = this.getMyHost() + '/apa/rest/commerce/addPsycnetProductToCart';
      this.performTransaction(url, cartItem);
    }
  }

  public has(code: string, isDatabase?: boolean){
    code = isDatabase ? ItemCodeService.getCode(code) : code;
    return (this.cartItems.indexOf(code) >= 0);
  }

  private getMyHost(){
    return this._cookieService.get('PN_MY');
  }

  private getSiteHost() {
    return this._cookieService.get('PN_HOST');
  }

  private getArticleItem(record: IRecord): CartItem {
    return {
      sessionId: '',
      itemId: ItemCodeService.getCode(record.ProductCode),
      uid: record.UID,
      name: record.Title,
      description: record.Authors + '<br>' + record.SourcePI,
      imageUrl: 'http://www.apa.org/pubs/journals/images/' + record.PAJournalCode + '-100.gif',
      productUrl: this.getSiteHost() + '/record/' + record.UID,
      documentCode: record.PAJournalCode,
      onlineFirstPublications: (record && record['IsOFP'] === 'true') ? 'true' : 'false',
    }
  }

  private getBookChapterItem(record: IRecord): CartItem {
    return {
      sessionId: '',
      itemId: ItemCodeService.getCode(record.ProductCode),
      uid: record.UID,
      name: record.Title,
      description: record.Authors + '<br>' + record.SourcePI,
      imageUrl: this.getSiteHost() + '/assets/img/no-book-cover-image.jpg',
      productUrl: this.getSiteHost() + '/record/' + record.UID,
      documentCode: '',
      onlineFirstPublications: 'false'
    };
  }

  private getDatabaseItem(code: string) {
    return {
      sessionId: '',
      itemId: ItemCodeService.getCode(code),
      uid: '0',
      name: this.databases[code].cartName,
      description: this.databases[code].cartDescription,
      imageUrl: this.getSiteHost() + '/assets/img/purchaseHistory/' + this.databases[code].cartImageTag + '.gif',
      productUrl: this.databases[code].productUrl,
      documentCode: '',
      onlineFirstPublications: 'false'
    };
  }

  private getComboDbItem() {
    return {
      sessionId: '',
      itemId: ItemCodeService.getCode('combo'),
      uid: '0',
      name: 'Day Pass Combo',
      description: '',
      imageUrl: this.getSiteHost() + '/assets/img/purchaseHistory/db_combo.gif',
      productUrl: this.getSiteHost() + '/search/',
      documentCode: '',
      onlineFirstPublications: 'false'
    };
  }

  private performTransaction(url, data, contentType?) {
    let requestContentType = contentType || 'application/json';
    let headers = new Headers({ 'Content-Type': requestContentType });
    let requestOptions = new RequestOptions({ headers: headers, withCredentials: true });
    let postData = (requestContentType.indexOf('json')>=0) ? JSON.stringify(data) : data;

    // using HTTP instead of apiRequest for better proxying
    this._http.post(url, postData, requestOptions).toPromise()
      .then((res: Response) => this.refreshCartStatus())
      .catch((err: Response | any) => console.error('Add to cart error:', err));
  }
}
