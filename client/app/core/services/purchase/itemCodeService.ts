
export class ItemCodeService {
  private static codes = {
    'PA': '2700001',
    'PB': '4500001',
    'PI': '3500001',
    'PE': '3700001',
    'PC': '2020001',
    'combo': '4840001'
  };

  public static getCode(product){
    let code = null;

    if(ItemCodeService.codes.hasOwnProperty(product)){
      code = ItemCodeService.codes[product];
    }

    return code;
  }
}
