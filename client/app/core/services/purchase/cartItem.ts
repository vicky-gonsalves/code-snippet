
export interface ICartItem {
  sessionId: string;
  itemId: string;
  uid: string;
  name: string;
  description: string;
  imageUrl: string;
  productUrl: string;
  documentCode: string;
  onlineFirstPublications: string;
}


export class CartItem implements ICartItem{
  sessionId: string = '';
  itemId: string = '';
  uid: string = '';
  name: string = '';
  description: string = '';
  imageUrl: string = '';
  productUrl: string = '';
  documentCode: string = '';
  onlineFirstPublications: string = '';

  constructor(item?:ICartItem) {
    if(item){
      this.sessionId = item.sessionId;
      this.itemId =  item.itemId;
      this.uid = item.uid;
      this.name = item.name;
      this.description = item.description;
      this.imageUrl = item.imageUrl;
      this.productUrl = item.productUrl;
      this.documentCode = item.documentCode;
      this.onlineFirstPublications = item.onlineFirstPublications;
    }
  }

}
