import { Injectable }       from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable }      from 'rxjs/Observable';
import { IdentityService }      from './identity/identityService';
import { AuthService }      from './authService';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor( private authService: AuthService, private router: Router ) {

  }

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): Observable<boolean> | boolean {
    return IdentityService.identity.map((): boolean => {
      if (this.authService.isAuthenticated()) {
        return true;
      } else {
        this.authService.redirectToLogin( state.url );
        return false;
      }
    });
  }
}
