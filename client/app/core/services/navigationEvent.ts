import {Injectable} from '@angular/core';
import {SessionStorageService} from 'ng2-webstorage';

@Injectable()
export class NavigationEvent{

  private readonly _lookupName = "navigationEvent";

  constructor(private _storage : SessionStorageService){

  }

  /**
   * hasAction - is there a navigation event that has not been processed
   */
  public hasAction() : boolean {
    let evt = this._storage.retrieve(this._lookupName);
    return (evt);
  }

  /**
   * setEvent - sets navigation information
   * @param event - name of event
   * @param args - object of arguments
   */
  public set(event, args){
    var action = {
      event: event,
      args: args || {}
    };

    this._storage.store(this._lookupName, action);
  }

  public get(){
    return this._storage.retrieve(this._lookupName);
  }

  /**
   * clear - removes all navigation information
   */
  public clear(){
    this._storage.clear(this._lookupName);
  }

}
