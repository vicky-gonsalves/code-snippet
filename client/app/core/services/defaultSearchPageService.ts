import { Injectable }       from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable }      from 'rxjs/Observable';
import { IdentityService }      from './identity/identityService';
import { Identity } from './identity/identity';

@Injectable()
export class DefaultSearchPageService implements CanActivate {

  constructor(private _router: Router) {
  }

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): Observable<boolean> | boolean {
    return IdentityService.identity.map((identity: Identity): boolean => {
      if(identity.isOrganization && identity.profile.defaultPage === 'basic') {
        this._router.navigate(['/search/basic'], { replaceUrl: true });
        return false
      }
      return true;
    });
  }
}
