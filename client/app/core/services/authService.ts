import { Injectable } from '@angular/core';
import {IdentityService} from './identity/identityService';
import {Identity} from './identity/identity';

@Injectable()
export class AuthService{
  private _identity: Identity = new Identity();

  constructor(){
    IdentityService.identity.subscribe(data => {

      if(!data){
        return false;
      }

      this._identity = data;

    });
  }

  // checks to see if the user is individual
  public isAuthenticated(){
    return (this._identity.isIndividual);
  }

  // redirects the user to IDEM to login and get an ERIGHTS cookie
  public redirectToLogin(url){
    window.location.href = '/user/login?returnUrl=' + encodeURIComponent(url);
  }

}
