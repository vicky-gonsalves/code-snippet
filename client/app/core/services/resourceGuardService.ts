import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Identity } from './identity/identity';
import { IdentityService } from './identity/identityService';
import { PermissionService } from './identity/permissionService';

@Injectable()
export class CitedRefGuardService implements CanActivate {

  constructor(private _permission: PermissionService, private _router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return IdentityService.identity.map((identity: Identity): boolean => {
      let isAllowed = this._permission.mayAccessCitedRefSearch();
      if (!isAllowed) {
        this._router.navigate(['/']);
      }
      return isAllowed;
    });
  }
}

@Injectable()
export class PAGuardService implements CanActivate {

  constructor(private _permission: PermissionService, private _router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return IdentityService.identity.map((identity: Identity): boolean => {
      let isAllowed = !identity.isOrganization || (identity.isOrganization && (this._permission.mayAccess('PA') || this._permission.hasGroupAccess('printSubscription')) );
      if (!isAllowed) {
        this._router.navigate(['/no-access']);
      }
      return isAllowed;
    });
  }
}

@Injectable()
export class PBGuardService implements CanActivate {

  constructor(private _permission: PermissionService, private _router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return IdentityService.identity.map((identity: Identity): boolean => {
      let isAllowed = !identity.isOrganization || ( identity.isOrganization && (this._permission.mayAccess('PB') || this._permission.hasGroupAccess('handbook') || this._permission.hasGroupAccess('bookCollection') ));
      if (!isAllowed) {
        this._router.navigate(['/no-access']);
      }
      return isAllowed;
    });
  }
}

@Injectable()
export class PCGuardService implements CanActivate {

  constructor(private _permission: PermissionService, private _router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return IdentityService.identity.map((identity: Identity): boolean => {
      let isAllowed = this._permission.mayAccess('PC');
      if (!isAllowed) {
        if(identity.isOrganization) {
          this._router.navigate(['/no-access']);
        } else {
          this._router.navigate(['/buy/PC']);
        }
      }
      return isAllowed;
    });
  }
}

@Injectable()
export class PEGuardService implements CanActivate {

  constructor(private _permission: PermissionService, private _router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return IdentityService.identity.map((identity: Identity): boolean => {
      let isAllowed = this._permission.mayAccess('PE');
      if (!isAllowed) {
        if(identity.isOrganization) {
          this._router.navigate(['/no-access']);
        } else {
          this._router.navigate(['/buy/PE']);
        }
      }
      return isAllowed;
    });
  }
}

@Injectable()
export class PTGuardService implements CanActivate {

  constructor(private _permission: PermissionService, private _router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return IdentityService.identity.map((identity: Identity): boolean => {
      let isAllowed = (this._permission.mayAccess('PT'));
      if (!isAllowed) {
        this._router.navigate(['/no-access']);
      }
      return isAllowed;
    });
  }
}

@Injectable()
export class PHGuardService implements CanActivate {

  constructor(private _permission: PermissionService, private _router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return IdentityService.identity.map((identity: Identity): boolean => {
      let isAllowed = this._permission.mayAccess('PH');
      if (!isAllowed) {
        this._router.navigate(['/no-access']);
      }
      return isAllowed;
    });
  }
}

@Injectable()
export class ThesaurusGuardService implements CanActivate {

  constructor(private _router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return IdentityService.identity.map((identity: Identity): boolean => {
      let isAllowed = (identity.entitlements.length > 0);
      if (!isAllowed) {
        this._router.navigate(['/']);
      }
      return isAllowed;
    });
  }
}

@Injectable()
export class DOILandingGuardService implements CanActivate {

  constructor(private _router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return IdentityService.identity.map((identity: Identity): boolean => {
      let isAllowed = DOILandingGuardService.isAllowed(identity);
      if (!isAllowed) {
        this._router.navigate(['/']);
      }
      return isAllowed;
    });
  }

  static isAllowed(identity): boolean {
    return !identity.isIndividual && !identity.isOrganization;
  }
}
