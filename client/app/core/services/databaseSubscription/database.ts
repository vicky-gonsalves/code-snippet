export interface IDatabaseFilter{
  name : string;
  label : string;
  selected : boolean;
  showAlways : boolean;
}

export interface IDatabase{
  id : number;
  code : string;
  name : string;
  desc : string;
  route : string;
  showAlways : boolean;
  showForDirectUsers : boolean,
  fulldesc : string;
}


// an identity consists of both an organization indicator, an individual name, and their combined entitlements
export class Database{

  databaseFilter : Array<IDatabaseFilter>;
  database : Array<IDatabase>;

  constructor( database ?: Array<IDatabase>, dataFilter?: Array<IDatabaseFilter>){
      if(dataFilter){
          this.databaseFilter = dataFilter;
      }
      if(database){
          this.database = database;
      }
  }
}
