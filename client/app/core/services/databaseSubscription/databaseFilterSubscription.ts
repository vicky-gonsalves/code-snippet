/*
* This class will load athe databases available for the user and share among all the components insteadof loading at every time.
* */

import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/share';
import * as _ from 'lodash';

import {PermissionService} from '../identity/permissionService';
import {ApiRequestService, Query} from '../apiRequestService';
import {Database, IDatabaseFilter} from './database'

@Injectable()
export class DatabaseFilterSubscription {
    private static grantedDatabases = [];

    private static _subscribedDatabase = new BehaviorSubject<Database>(new Database());
    private static _subscribedSelectionChanges = new BehaviorSubject<any>(null);

    constructor(private _apiService: ApiRequestService, private _permission: PermissionService){

    }

    public static get subscribedDatabase(){
        return DatabaseFilterSubscription._subscribedDatabase.asObservable();
    }

    public getSubscribedDatabase() : void {
        let filterQuery = this._apiService.createQuery('menu.getBrowseMenu');
        this._apiService.execute(filterQuery).subscribe(
            data => {
                let database : Database;
                let filter : Array<IDatabaseFilter> = [];
                for(var i = 0 ;i < data.length; i++) {
                    filter.push( {name : data[i].desc, label : data[i].desc, selected : true, showAlways : data[i].showAlways} );
                }
                DatabaseFilterSubscription.grantedDatabases = _.map(data, (dbItem) => dbItem['code']);
                if(this._permission.mayAccess('PI') || !this._permission.mayAccessAllDefaultProducts()) {
                  DatabaseFilterSubscription.grantedDatabases.push('PI');
                }
                database= new Database(data, filter);
                this.updateDatabaseDetails(database);
            },
            error => console.log(error)
        );
    }

    private updateDatabaseDetails(subscribedDB) : void{
      DatabaseFilterSubscription._subscribedDatabase.next(subscribedDB);
    }

    public static get selectedDBChanges(){
      return DatabaseFilterSubscription._subscribedSelectionChanges.asObservable();
    }

    public static updateSelectedDBChanges(selectedDB) : void{
      DatabaseFilterSubscription._subscribedSelectionChanges.next(selectedDB);
    }

    public static resetSelectedDBChanges() : void{
      DatabaseFilterSubscription._subscribedSelectionChanges.next(DatabaseFilterSubscription.grantedDatabases);
    }
}




