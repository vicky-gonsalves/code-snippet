import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Identity} from "../services/identity/identity";
import {IdentityService} from "../services/identity/identityService";
import { Locale, LocaleService, LocalizationService } from 'angular2localization';
import {ApiRequestService, Query} from "../services/apiRequestService";
import {PurchaseService} from "../services/purchase/purchaseService";
import {CookieService} from 'angular2-cookie/core';
import {Router} from '@angular/router'

@Component({
  selector: 'identitybar',
  templateUrl: './identitybar.html',
  styleUrls: ['./identitybar.less']
})

export class IdentityBar extends Locale {
  @Input() showLoginOnly: boolean;

  @Output() comeSoon = new EventEmitter();

  identity: Identity;
  isOrganization: boolean;
  cartCount = 0;
  returnUrl: string;
  adminLinkURL: string;
  adminLinkMailTo: string;
  adminLinkText: string;

  constructor(public _router: Router,public locale: LocaleService, public localization: LocalizationService, private _apiService: ApiRequestService, private _cookieService:CookieService) {
    super(locale, localization);
    this.returnUrl = encodeURIComponent(window.location.href);
  }

  ngOnInit() {
    IdentityService.identity.subscribe(data => {
      // ensure identity is loaded
      if(!data){
        return false;
      }

      this._router.events.subscribe(() => this.returnUrl = encodeURIComponent(window.location.href));

      this.identity = data;
      this.isOrganization = this.identity.isOrganization;

      if (this.hasAdminLinkProp(data, 'url')) {
        this.adminLinkURL = data.profile['adminLink'].url;
      } else if (this.hasAdminLinkProp(data, 'mailto')) {
        this.adminLinkMailTo = data.profile['adminLink'].mailto;
      }
      if (this.hasAdminLinkProp(data, 'displayText')) {
        this.adminLinkText = data.profile['adminLink'].displayText;
      }

      PurchaseService.count.subscribe(count => this.cartCount = count);

    });
  }

  triggerComingSoon() {
    this.comeSoon.emit();
    return false;
  }

  private hasProfileProp(identity, prop:string): boolean {
    return identity && identity.hasOwnProperty('profile') && identity.profile && identity.profile.hasOwnProperty(prop) && identity.profile[prop];
  }

  private hasAdminLinkProp(identity, prop:string): boolean {
    return this.hasProfileProp(identity, 'adminLink') && identity.profile.adminLink.hasOwnProperty(prop) && identity.profile.adminLink[prop] && identity.profile.adminLink[prop].length;
  }

  // Sets a new locale & currency.
  selectLocale(language: string, country: string, currency: string): void {
    this.locale.setCurrentLocale(language, country);
    this.locale.setCurrentCurrency(currency);
    if (language != 'en') {
      this.logPageView('Languages');
    }
  }

  // the other API Requests for this page will be cached in localStorage, so we don't want to make this log request on its own so that it's not missed
  public logPageView(pageId: string) {
    let query: Query = this._apiService.createQuery('log.logEntry');
    query.logs({pageId: pageId});
    this._apiService.execute(query).subscribe();
  }

  public getMyAPALink(){
    return this._cookieService.get('PN_MY') + '/portal/home';
  }
}
