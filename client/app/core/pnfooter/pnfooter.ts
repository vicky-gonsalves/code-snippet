import { Component, Output, EventEmitter } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';

import {IdentityService} from '../services/identity/identityService';
import {IIdentity} from '../services/identity/identity';
import {ApiRequestService, Query} from "../services/apiRequestService";
import { Locale, LocalizationService } from 'angular2localization';

@Component({
  selector: 'pnfooter',
  templateUrl: './pnfooter.html',
  styleUrls: ['./pnfooter.less']
})
export class PNFooter extends Locale{
  private currentYear: number;
  private _identity: IIdentity;
  isOrganization: boolean;

  @Output() comeSoon = new EventEmitter();

  constructor(private _apiService: ApiRequestService, public localization: LocalizationService) {
    super(null, localization);
    this.currentYear = new Date().getFullYear();
  }

  ngOnInit() {
    IdentityService.identity.subscribe(data => {
      // ensure identity is loaded
      if(!data) {
        return false;
      }

      this._identity = data;
      this.isOrganization = this._identity.isOrganization;
    });
  }

  // the other API Requests for this page will be cached in localStorage, so we don't want to make this log request on its own so that it's not missed
  private logPageView(pageId: string) {
    let query: Query = this._apiService.createQuery('log.logEntry');
    query.logs({pageId: pageId});
    this._apiService.execute(query).subscribe();
  }

  triggerComingSoon() {
    this.comeSoon.emit({title: 'APA PsycNET Help', source: 'help'});
    return false;
  }
}
