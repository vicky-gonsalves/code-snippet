import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';

import { General } from "./general";
import { ContactUs } from './components/contactUs/contactUs';
import { PrivacyStatement } from './components/privacyStatement/privacyStatement';
import { TermsOfService } from './components/termsOfService/termsOfService';
import { Disclaimer } from './components/disclaimer/disclaimer';
import { Feedback } from './components/feedback/feedback';
import { AlertModule } from 'ng2-bootstrap';
import { ReCaptchaModule } from 'angular2-recaptcha';

import { routing } from './general.routes';

@NgModule({
  imports: [routing, FormsModule, AlertModule, CommonModule, ReCaptchaModule, SharedModule],
  declarations: [
    General,
    ContactUs,
    PrivacyStatement,
    TermsOfService,
    Disclaimer,
    Feedback
  ],
  providers: [],
  exports: []
})
export class GeneralModule {}
