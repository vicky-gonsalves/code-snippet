import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { General } from './general';
import { ContactUs } from './components/contactUs/contactUs';
import { PrivacyStatement } from './components/privacyStatement/privacyStatement';
import { TermsOfService } from './components/termsOfService/termsOfService';
import { Disclaimer } from './components/disclaimer/disclaimer';
import { Feedback } from './components/feedback/feedback';

export const routes: Routes = [
  {
    path: '',
    component: General,
    children: [
      {
        path: "contactUs",
        component: ContactUs
      }, {
        path: "privacy",
        component: PrivacyStatement
      }, {
        path: "tos",
        component: TermsOfService
      }, {
        path: "disclaimer",
        component: Disclaimer
      }, {
        path: "feedback",
        component: Feedback
      },
      {
        path: '',
        redirectTo: '/search',
        pathMatch: 'full'
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);

