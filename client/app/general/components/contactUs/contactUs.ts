import {Component} from '@angular/core';
import { Locale, LocaleService, LocalizationService } from 'angular2localization';

@Component({
  selector: 'contact-us',
  templateUrl: './contactUs.html',
  styleUrls: ['./contactUs.less']
})
export class ContactUs extends Locale {

 infoEmail= "mailto:psycinfo@apa.org";

  constructor(public locale: LocaleService, public localization: LocalizationService) {
    super(locale, localization);
  }

  onClick() {
    window.location.href = this.infoEmail;
  }
}
