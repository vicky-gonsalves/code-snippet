import {Component} from '@angular/core';
import {ApiRequestService} from "../../../core/services/apiRequestService";


@Component({
  selector: 'feedback',
  templateUrl: './feedback.html',
  styleUrls: ['./feedback.less']
})

export class Feedback {
  alertMessage: any;
  isCorrectForm: boolean = false;
  verified:boolean = false;
  categories =[];
  isSending = false;

  comments= {
    systemBug: {name:'System Bug', selected: false},
    searchProblem: {name:'Search Problem', selected: false},
    limiting: {name:'Limiting/Sorting', selected: false},
    results: {name:'Results', selected: false},
    save: {name:'Save/Print/Display', selected: false},
    export: {name:'Export', selected: false},
    recommended: {name:'Recommended Enhancement', selected: false}
  };

  feedbackInfo= {
      categoriesList: '',
      details:'',
      emailInfo: {
        from: '',
        name: '',
        title: '',
        institution: '',
        address: '',
        fullAddress: '',
        phone: '',
      }
    };

  constructor(private _apiService : ApiRequestService) {
  }

  public sendEmail() {
    this.isSending = true;
    this.isCorrectForm = true;
    this.getSelectedComments();
    let qry = this._apiService.createQuery('search.commentsEmail');
    let params = {};

    params = {email: this.feedbackInfo};

    qry.params(params);
    this._apiService.execute(qry).subscribe(data => {
      if (data) {
        this.isSending = false;
        this.displayResultMessage('success');
      }
    }, error => console.log(error)
    );
  }

  public displayResultMessage(type: string) {
    this.alertMessage = {
      type: type,
      timeout: 5000
    };
  }

  public handleCorrectCaptcha(captchaResponse: string) {
       if(captchaResponse.length > 0) {
         this.verified = true;
       }
    }

    public getSelectedComments() {
      this.categories.length = 0;
      for(let key in this.comments){
        if(this.comments[key].selected == true)
          this.categories.push(this.comments[key].name);
      }
      this.feedbackInfo.categoriesList = this.categories.join(', ');
    }
}
