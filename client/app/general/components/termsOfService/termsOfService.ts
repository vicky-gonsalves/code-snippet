import {Component} from '@angular/core';
import {Query, ApiRequestService} from "../../../core/services/apiRequestService";

@Component({
  selector: 'terms-of-service',
  templateUrl: './termsOfService.html',
  styleUrls: ['./termsOfService.less']
})
export class TermsOfService {

  constructor(private _apiService: ApiRequestService) {
  }

  ngOnInit() {
    this.logPageView('T&C');
  }

  // the other API Requests for this page will be cached in localStorage, so we don't want to make this log request on its own so that it's not missed
  private logPageView(pageId: string) {
    let query: Query = this._apiService.createQuery('log.logEntry');
    query.logs({pageId: pageId});
    this._apiService.execute(query).subscribe();
  }
}
