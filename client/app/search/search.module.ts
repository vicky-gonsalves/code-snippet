import { NgModule } from '@angular/core';

import { VolatileModule } from '../volatile/volatile.module';

import { SharedModule } from '../shared/shared.module';
import { SearchCommonModule } from './searchcommon.module';

import {SearchBasic} from "./forms/basic/basic";
import {BasicTrending} from "./forms/basic/components/trending/basicTrending";
import {SearchTips} from "./forms/basic/components/searchTips/searchTips";

import {Advanced} from "./forms/advanced/advanced";
import {OmitExclusiveFormField} from "./forms/advanced/pipes/omitExclusiveFormField";
import {Lookup} from "./forms/advanced/components/lookup/lookup";
import {LookupModal} from "./forms/advanced/components/lookup/lookupModal/lookupModal";
import {SearchBox} from "./forms/advanced/components/lookup/searchBox/searchBox";
import {AddToSearchService} from "./forms/advanced/components/lookup/services/addToSearchService";
import {LookupBrowseService} from "./forms/advanced/components/lookup/services/lookupBrowseService";
import {AdvRecent} from "./forms/advanced/components/recent/recent";
import {AdvancedTrending} from "./forms/advanced/components/trending/advancedTrending";



import {SearchActionBar} from "./results/components/searchActionBar/searchActionBar";
import {SearchFilterBox} from "./results/components/searchFilterBox/searchFilterBox";
import {SearchResultsButtonBar} from './results/components/buttonBar/searchResultsButtonBar';
import {SearchResults} from "./results/results";
import {SearchResultsNav} from "./results/components/searchResultsNav/searchResultsNav";
import {SearchResultsPagination} from "./results/components/searchResultsPagination/searchResultsPagination";
import {SearchResultsList} from "./results/components/searchResultsList/searchResultsList";
import {SearchResultReference} from './results/components/searchResultReference/searchResultReference';
import {SearchResultsToolsBar} from "./results/components/searchResultsToolsBar/searchResultsToolsBar";
import {SearchTabs} from "./results/components/searchTabs/searchTabs";
import {SearchWithinResultsBox} from "./results/components/searchWithinResultsBox/searchWithinResultsBox";
import {PHResults} from './results/components/phResults/phResults';
import {CitedrefsResultsList} from './results/components/citedrefsResultsList/citedrefsResultsList';
import {CitedRefsResults} from './results/citedRefsResults';
import {CitedBy} from './citedBy/citedBy';
import {SaveSearchModal} from './results/components/saveSearchModal/saveSearchModal';
import {AddTagNotes} from './myList/components/addTagNote/addTagNote';
import { FilterBox } from './myList/components/filterBox/filterBox';
import { MyListSearchTabs } from './myList/components/mylistSearchTabs/mylistSearchTabs';

import {MyList} from "./myList/myList";
import {RecentSearch} from "./recent/recentSearch";
import {CombineSearchBox} from "./recent/components/combineSearchBox/combineSearchBox";
import {InvalidSearchDialog} from "./recent/components/invalidSearchDialog/invalidSearchDialog";
import {AddToListConfirmationModal} from "./results/components/addToListConfirmationModal/addToListConfirmationModal";

// Services
import {DirectSearchService} from "./services/search/directSearchService";
import {LateralSearchService} from "./services/search/lateralSearchService";
import {SearchCollection} from './services/search/searchCollection';
import {SearchNavigation} from './services/searchNavigation';
import {SearchResultTabCalculator} from "./results/services/tab/searchResultTabCalculator";
import {ActiveSearchNotifier} from './results/services/activeSearchNotifier';
import {PhDemonstrationsService} from './results/services/phDemonstrationsService';



import { routing } from './search.routes';

@NgModule({
  imports: [
    VolatileModule,
    SharedModule,
    SearchCommonModule,
    routing
  ],
  declarations: [
    // basic search form
    SearchBasic,
    BasicTrending,
    SearchTips,

    // advanced search form
    Advanced,
    OmitExclusiveFormField,
    Lookup,
    LookupModal,
    SearchBox,
    AdvRecent,
    AdvancedTrending,

    // search result
    SearchActionBar,
    SearchFilterBox,
    SearchResultsButtonBar,
    SearchResults,
    SearchResultsNav,
    SearchResultsPagination,
    SearchResultsList,
    SearchResultReference,
    SearchResultsToolsBar,
    SearchTabs,
    SearchWithinResultsBox,
    CitedrefsResultsList,
    CitedRefsResults,
    CitedBy,
    SaveSearchModal,
    AddTagNotes,
    FilterBox,
    PHResults,
    MyList,
    MyListSearchTabs,
    RecentSearch,
    CombineSearchBox,
    InvalidSearchDialog,
    AddToListConfirmationModal
  ],
  providers: [
    AddToSearchService,
    LookupBrowseService,
    DirectSearchService,
    LateralSearchService,
    SearchCollection,
    SearchNavigation,
    SearchResultTabCalculator,
    ActiveSearchNotifier,
    PhDemonstrationsService
  ]
})
export class SearchModule {}
