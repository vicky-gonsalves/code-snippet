import {Injectable} from '@angular/core';
import {Search} from "./search/search";
import {Router} from '@angular/router';

@Injectable()
export class SearchNavigation {

  private isResultsOnly: boolean = false;
  private doNotLogResults: boolean = false;

  constructor(private router: Router){

  }

  newSearch(search:Search):void {
    // setting queryParams to an empty object will remove existing query params from URL
    this.router.navigate([this.getFormPath(search)], {queryParams: {}});
  }

  editSearch(search:Search):void {
    this.router.navigate([this.getFormPath(search)], {queryParams: {id: search.id}});
  }

  gotoResults(search:Search, params:any):void{
    if (params.resultsOnly) {
      this.isResultsOnly = true;
    }

    if(params.doNotLogResults ){
      this.doNotLogResults = true;
    }

    this.router.navigate([this.getResultsPath(search)], params);
  }

  isResultsOnlySearch() {
    let result = this.isResultsOnly;
    if (this.isResultsOnly) {
     this.isResultsOnly = false;
    }
    return result;
  }

  // resets its state after getting it's value
  doNotLogResultsSearch(){
    let result = this.doNotLogResults;
    this.doNotLogResults = false;
    return result;
  }

  // calculates the search form path based on query type
  private getFormPath(search:Search):string{
    let path = '';

    switch(search.query.source){
      case 'advanced':
        path = '/search/advanced';
        break;
      case 'easy':
        path = '/search/basic';
        break;
      case 'citedRefs':
        path = '/search/citedRefs';
        break;
      default:
        path = '/search/advanced';
    }

    return path;
  }

  // calculates the search results path based on query type
  private getResultsPath(search:Search):string{
    let path = '';

    switch(search.query.source){
      case 'advanced':
      case 'recommended':
      case 'easy':
        path = '/search/results';
        break;
      case 'citedRefs':
        path = '/search/citedRefsResults';
        break;
      default:
        path = '/';
    }

    return path;
  }

}
