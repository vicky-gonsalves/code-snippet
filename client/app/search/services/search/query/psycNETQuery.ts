import {IPsycNETQuery, ICriteria, ILimit, IFilter, IDisplayOptions, INativeOptions} from './components/interfaces';
import {Filter} from './components/filter';
import {DisplayOptions} from './components/displayOptions';
import {NativeOptions} from './components/nativeOptions';
import {SortDescriptionService} from "./sortDescriptionService";
import {PermissionService} from "../../../../core/services/identity/permissionService";

export class PsycNETQuery implements IPsycNETQuery {
  criteria: ICriteria[] = [];
  limits: ILimit[] = [];
  filter: IFilter = new Filter();
  displayOptions: IDisplayOptions = new DisplayOptions();
  nativeOptions: INativeOptions = new NativeOptions();
  databases: string[] = [];
  tab: string = 'all';
  source: string = '';
  viewMode: string = 'psycnet';
  index: string = 'psycnet';

  constructor(){
    this.applyDefaultSort();
    this.applyDefaultDatabases();
  }

  // by default, sort by PublicationYearMSSort, then by AuthorSort
  applyDefaultSort(){
    let defaultSort = [
      {code: 'y', direction: '-1'},
      {code: 'a', direction: '1'}
      ];
    this.applySort(defaultSort);
  }

  applySort(sortBy){
    this.displayOptions.sort = SortDescriptionService.createSortOptions(sortBy);
  }

  applyDefaultDatabases(){
    let permissionService = new PermissionService();
    this.databases = permissionService.getSearchDatabases();
  }
}
