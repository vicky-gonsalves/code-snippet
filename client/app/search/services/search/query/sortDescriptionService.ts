import * as _ from "lodash";
import {ISortDescription} from "./components/interfaces";

export class SortDescriptionService {
  private static options = [
    {code: 'y', field: 'PublicationYearMSSort', direction:'-1'},
    {code: 'a', field: 'AuthorSort', direction:'-1'},
    {code: 't', field: 'TitleSort', direction:'-1'},
    {code: 'r', field: 'score', direction:'-1'},
    {code: 'timesAs', field: 'CitedByCountSort', direction:'-1'},
    {code: 'timesDes', field: 'CitedByCountSort1', direction:'-1'},
    {code: 'TiAs', field: 'ArticleTitleSort', direction:'-1'},  // CitedRefs Document Title
    {code: 'TiDes', field: 'ArticleTitleSort1', direction:'-1'}, // CitedRefs Document Title
    {code: 'PYAs', field: 'PublicationYearMSSort', direction:'-1'}, // CitedRefs Year
    {code: 'PYDes', field: 'PublicationYearMSSort1', direction:'-1'}, // CitedRefs Year
    {code: 'SrAs', field: 'SourceTitleSort', direction:'1'},  // CitedRefs Source Title
    {code: 'SrDes', field: 'SourceTitleSort1', direction:'-1'}, // CitedRefs Source Title
    {code: 'CiAs', field: 'CitedInCountSort', direction:'1'}, // CitedRefs Cited In Count
    {code: 'CiDes', field: 'CitedInCountSort1', direction:'-1'},  // CitedRefs Cited In Count
    {code: 'mySeq', field: 'Seq', direction:'-1'},  // MyList sequence sort
    {code: 'release', field: 'PIPEReleaseDateSort', direction:'-1'}, // MyList sequence sort
    {code: 'c', field: 'CitedByCount', direction:'-1'}, // times cited in My List page
    {code: 'tt', field: 'TitleSort', direction:'1'}, // title in My List page
    {code: 'o', field: 'Seq', direction:'1'} // Order Added in My List page
  ];

  constructor(){}

  static getByCode(code: string) {
    return _.find(SortDescriptionService.options, ['code', code]);
  }

  static createSortOptions(params):ISortDescription[]{
    let sortDescriptions: ISortDescription[] = [];

    params.forEach(param => {
      let details = SortDescriptionService.getByCode(param.code);
      sortDescriptions.push({
        field: details.field,
        direction: this.getDirectionLabel(param.direction)
      })
    });

    return sortDescriptions;
  }

  static getDirectionLabel(direction: string) {
    return direction === '1' ? 'asc' : 'desc';
  }
}
