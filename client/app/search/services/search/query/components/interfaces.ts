
export interface IPsycNETQuery {
  criteria: ICriteria[],
  limits: ILimit[],
  filter: IFilter,
  displayOptions: IDisplayOptions,
  nativeOptions: INativeOptions,
  databases: string[],
  source: string,
  viewMode: string
}

export interface ICriteria {
  field: string;
  operator: string;
  value: string;
  source: string;
}

export interface ILimit {
  field: string;
  operator: string;
  values: string[];
}

export interface INativeOptions {
  pb: IPsycBooksNativeOptions
}

export interface IPsycBooksNativeOptions {
  type: {
    conferenceProceedings: boolean,
    handbookManual: boolean,
    referenceBook: boolean,
    classicBook: boolean,
    textbookStudyGuide: boolean
  },

  publicationType: {
    authored: boolean,
    edited: boolean
  },

  audience: {
    generalPublic: boolean,
    professional: boolean
  }
}

export interface IFilter {
  onlyShow: IOnlyShow,
  dateRange: IDateRange,
  facets: IFacetFilter[]
}

export interface IOnlyShow {
  ftOnly: boolean,
  testAvailable: boolean,
  peerReviewed: boolean,
  latestUpdate: boolean,
  impactStatement: boolean,
  openAccess: boolean
}

export interface IDateRange {
  begin: string,
  end: string,
  field: string,
  puby1: string,
  yearRange: string
}

export interface IFacetFilter {
  field: string;
  value: string;
}

export interface IDisplayOptions {
  pageSize: number,
  sort: ISortDescription[]
}

export interface ISortDescription {
  code?: string,
  field: string,
  direction: string
}

export interface IResponseParameters {
  // pagination controls
  start: number,
  rows: number,

  // facets
  facet?: boolean,
  fc?: string, // comma delimited list of facets)

  // show results
  results?: boolean,

  // highlight
  hl?: boolean

  // PT children
  child?: boolean;

  format?: string
}
