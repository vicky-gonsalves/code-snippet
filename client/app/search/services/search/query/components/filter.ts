import {IFilter, IOnlyShow, IDateRange, IFacetFilter} from './interfaces';


export class Filter implements IFilter {
  onlyShow: IOnlyShow = {
    ftOnly: false,
    testAvailable: false,
    peerReviewed: false,
    latestUpdate: false,
    impactStatement: false,
    openAccess: false
  };

  dateRange: IDateRange = {
    begin: 'eternity',
    end: 'eternity',
    field: 'PublicationYear',
    puby1: '*',
    yearRange: ''
  };

  facets: IFacetFilter[] = [];

  constructor(){

  }
}
