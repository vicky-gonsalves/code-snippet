import {INativeOptions, IPsycBooksNativeOptions} from './interfaces';

export class NativeOptions implements INativeOptions {
  pb:IPsycBooksNativeOptions = {
    type: {
      conferenceProceedings: false,
      handbookManual: false,
      referenceBook: false,
      classicBook: false,
      textbookStudyGuide: false
    },

    publicationType: {
      authored: false,
      edited: false
    },

    audience: {
      generalPublic: false,
      professional: false
    }
  };

  constructor(){

  }
}
