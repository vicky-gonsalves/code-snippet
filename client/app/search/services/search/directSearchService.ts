import * as _ from 'lodash';
import { Injectable } from '@angular/core';

import { PermissionService } from '../../../core/services/identity/permissionService';
import { Search } from "./search";
import { PsycNETQuery } from './query/psycNETQuery';
import { IResponseParameters } from "./query/components/interfaces";
import { Criteria } from "./query/components/criteria";

@Injectable()
export class DirectSearchService {
  constructor(private _permissionSvc: PermissionService) { }

  public create(fields: string, term: string, dbs: string): Search {
    let query: PsycNETQuery = new PsycNETQuery();
    query.source = 'advanced';

    // Set the search fields
    fields = fields || 'AnyField';
    let fieldsList = fields.split(',');
    let firstField = fieldsList.shift();
    query.criteria.push({ field: firstField, operator: 'OR', source: 'direct', value: term });
    for (let fieldName of fieldsList) {
      query.criteria.push({ field: fieldName, operator: 'AND', source: 'direct', value: term });
    }

    // Set the search Databases
    let userDbs = this._permissionSvc.getSearchDatabases();

    if (!dbs || dbs === 'all') {
      query.databases = userDbs;
    } else {
      // Store the databases list, after checking and filtering out any non-accessible database.
      // If such filtering results into an empty DB list, use the granted user's DB list.
      let databases = _.map(dbs.split(','), db => db.toUpperCase());
      query.databases = _.intersection(userDbs, databases);
      query.databases = query.databases.length ? query.databases : userDbs;
    }

    // Set the response specification
    let resParams: IResponseParameters = {
      start: 0,
      rows: 25,
      facet: true,
      results: true
    };

    return new Search(query, resParams);
  }
}
