import {Search} from "./search";
import * as _ from "lodash";

export class SearchCollection {

  // key-value pair of searchId -> search
  // Also, once localStorage modules are more mature (or we write our own), we can move this to localStorage for the preservation during page refreshes
  private static storage = {};

  constructor(){

  }

  static add(search: Search){
    this.storage[search.id] = search;
  }

  static update(search: Search): void{
    this.add(search);
  }

  static cloneWithNewId(search: Search): Search{
    let newSearch = new Search(search.query, search.responseParameters);
    this.add(newSearch);
    return newSearch;
  }

  static get(id: string){
    let search = null;

    if(id && this.hasSearch(id)){
      search = _.cloneDeep(this.storage[id]);
    }

    return search;
  }

  static remove(id: string) {
    delete this.storage[id];
  }

  static hasSearch(id): boolean {
    return (this.storage.hasOwnProperty(id));
  }

}
