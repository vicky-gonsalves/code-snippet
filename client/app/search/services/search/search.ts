import {PsycNETQuery} from './query/psycNETQuery';
import {IResponseParameters} from "./query/components/interfaces";
import { UUIDService } from '../../../shared/id/uuidService';

export class Search {
  id: string = '';
  query: PsycNETQuery;
  responseParameters: IResponseParameters;
  friendlyQuery: string = '';
  totalRecords: string = '0';

  // the 'metadata' property is a stash for items such as UID (record display lookup), ProduceCode, or any other data which might be helpful to identify the search
  metadata = {
    uid:  '',
    excludeFromLogs: false,

    // indicator to make sure we do not log tab or page changes
    hasBeenLogged: false,

    // Used when interacting with DOI links to get directly to (for example) PT Record Display.
    fromDoi: false,

    // directUID occurs when a user directly goes to the record display page /record/2016-22958-009
    directUID: false
  };

  constructor(query: PsycNETQuery, responseParameters: IResponseParameters){
    this.id = UUIDService.getUUID();
    this.query = query;
    this.responseParameters = responseParameters;
  }


}
