import {Injectable} from '@angular/core';
import {Search} from "./search";
import {PsycNETQuery} from './query/psycNETQuery';
import {IResponseParameters} from "./query/components/interfaces";
import {Criteria} from "./query/components/criteria";
import {Limit} from "./query/components/limit";

@Injectable()
export class LateralSearchService {

  typeCriteria = {
    a: 'Author',
    c: 'ClassificationCode',
    i: 'IndexTerms',
    k: 'Subject',
    r: 'Review Author',
    rPC: 'Review Author'  // same as 'r' but for logging purposes, only searches on PsycCRITIQUES
  };


  constructor(){

  }

  create(type:string, term:string):Search {
    let latSearch:Search = null;

    if(!this.typeCriteria.hasOwnProperty(type)){
      return latSearch;
    }

    // build the query criteria
    let pnQuery = new PsycNETQuery();

    pnQuery.source = 'advanced';

    // Only search on PT if the classification code is of PT.  This is also used for accurate counter4 logging.
    if(type == 'c' && parseInt(term) >= 5000){
      pnQuery.databases = ["PT"];
      pnQuery.limits.push(new Limit('Classification', 'is', [term]));
    } else if (type === 'c' && parseInt(term) < 5000) {
      pnQuery.limits.push(new Limit('Classification', 'is', [term]));
    } else {
      pnQuery.criteria.push(new Criteria(this.typeCriteria[type], 'OR', this.wrapTerm(type, term), 'user'));
    }

    // for review author, we should also search for 'Author'
    if (type=='r' || type=='rPC') {
      pnQuery.criteria.push( new Criteria('Author', 'OR', this.wrapTerm(type, term), 'user') );
    }

    if(type=='rPC'){
      pnQuery.databases=['PC']
    }

    let responseParams: IResponseParameters = {
      start: 0,
      rows: 25,
      facet: true,
      results: true
    };

    latSearch = new Search(pnQuery, responseParams);

    return latSearch;

  }

  wrapTerm(type, term){

    // use quotes when doing lateral searches for keywords and index terms
    let quotedTermTypes = ['k','i','c'];

    if(quotedTermTypes.indexOf(type) >=0 ){
      term = '"'+term+'"';
    } else {
      term = '{'+term+'}';
    }

    return term;
  }

}
