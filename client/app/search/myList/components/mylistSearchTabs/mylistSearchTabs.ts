import {Component, Output, EventEmitter, Input} from '@angular/core';
import {IHitcount, HitCount} from "../../../results/services/tab/searchResultTabCalculator";
import {ProductLabels} from '../../../../shared/productLabels/productLabels';
import {IdentityService} from '../../../../core/services/identity/identityService';
import {IIdentity, Identity} from '../../../../core/services/identity/identity';
import {PermissionService} from '../../../../core/services/identity/permissionService';
import {SearchCollection} from "../../../services/search/searchCollection";

@Component({
  selector: 'mylist-search-tabs',
  templateUrl: './mylistSearchTabs.html',
  styleUrls: ['./mylistSearchTabs.less']
})

export class MyListSearchTabs {
  @Input() defaultTab: string;
  @Input() tabNumbers: HitCount;
  @Input() searchId: string;
  @Output() searchTabText = new EventEmitter();
  selectedTab : string ;
  isAllVisible : boolean;

  tabDisplayOrder = ['PI','PA','PB','PE','PC','PT'];
  tabDisplay = [];

  constructor(private _permissionService : PermissionService) {
  }

  ngOnInit() {

  }

  ngOnChanges() {
    this._refreshTabStatus();
  }

  selectTab(tab) {
    this.selectedTab = tab;
    this.searchTabText.emit(tab);
  }

  private _refreshTabStatus(){
    this.tabDisplay = [];

    this.tabDisplayOrder.forEach(db => {
      let count = this._getTabCount(db);

      if(count > 0){
        this.tabDisplay.push({
          db: db,
          label: this._getLabel(db),
          count: count
        });
      }
    });

    this._markActiveTab();
  }

  private _markActiveTab(){
    let search = SearchCollection.get(this.searchId);

    this.selectedTab = search.query.tab;

    if(this.selectedTab === 'all' && this.tabDisplay.length === 1){
      this.selectedTab = this.tabDisplay[0].db;
    }

  }

  private _getTabCount(db){
    let result = 0;

    if(this.tabNumbers && this.tabNumbers.hasOwnProperty(db)){
      result = this.tabNumbers[db];
    }

    return result;
  }

  private _getLabel(db){
    let label = ProductLabels.getLabel(db);

    if(db === 'PB' && this._isBookCollectionUser()){
      label = 'APA Books';
    }

    if (db === 'PA' && this._isPrintSubscriptionUser()) {
      label = 'APA Journals';
    }

    return label;
  }

  private _isBookCollectionUser(){
    return (!this._permissionService.mayAccess('PB') && this._permissionService.hasGroupAccess('bookCollection'));
  }

  private _isPrintSubscriptionUser(){
    return (!this._permissionService.mayAccess('PA') && this._permissionService.hasGroupAccess('printSubscription'));
  }


}
