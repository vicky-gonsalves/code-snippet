/**
 * Created by NIKHILBABUM on 11/14/2016.
 */
import {Component, Input, Output, EventEmitter} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {ApiRequestService, Query} from '../../../../core/services/apiRequestService';
import { Locale, LocaleService, LocalizationService } from 'angular2localization';

import * as _ from "lodash";
@Component({
  selector: 'addTagNote',
  templateUrl: './addTagNote.html',
  styleUrls: ['./addTagNote.less']
})

export class AddTagNotes extends Locale {
  @Input() parentId: string;
  @Input() item: string;
  @Output() tagNotesAction = new EventEmitter();
  tagNotesFormData = {"notes" : "","tags":""};
  hasTagError: boolean = false;
  /*tagNotesFormData = new FormGroup({
   notes: new FormControl(),
   tags: new FormControl()
   });*/

  constructor(private _apiService: ApiRequestService, public _locale: LocaleService, public _localization: LocalizationService) {
    super(_locale, _localization);
  }

  ngOnInit() {
    if(this.item && this.item.hasOwnProperty('TagNote') && this.item['TagNote'].length){
      this.tagNotesFormData.tags = _.join(_.map(this.item['TagNote'][0]['tags'], 'tagName'),';');
      this.tagNotesFormData.notes = this.item['TagNote'][0]['notes'];
    }
  }

  checkTagsValidity(_tags) {
    let tags = _tags.split(';');
    let invalidTags = tags.filter(tag=>tag.length > 20);
    this.hasTagError = invalidTags.length;
  }

  submitTagNotesForm() {
    if (!this.hasTagError && (this.tagNotesFormData.tags.length || this.tagNotesFormData.notes.length)) {
      let query = this._apiService.createQuery('mylist.addMyListTagNotes');
      query.params({"UID": this.item['UID'],"Tags": this.tagNotesFormData.tags,"Notes": this.tagNotesFormData.notes});
      this._apiService.execute(query)
        .subscribe(
          data => {
          this.tagNotesAction.emit('refresh');
        },
          error => console.log(error)
      );
    }
  }
  close(){
    this.tagNotesAction.emit('hide');
  }
}
