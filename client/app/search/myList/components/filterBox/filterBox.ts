import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'filter-box',
  templateUrl: './filterBox.html',
  styleUrls: ['./filterBox.less']
})
export class FilterBox {

  @Input() boxTitle: string;
  @Input() tagFilter: number;
  @Input() Tags: string;
  @Output() TagFiter = new EventEmitter();

  constructor() {
  }

  ngOnInit() {

  }
  filterbyTag(TagID){
    console.log(TagID);
    this.TagFiter.emit(TagID);
  }
}
