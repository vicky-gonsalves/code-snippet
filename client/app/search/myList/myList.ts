import {Component} from '@angular/core';
import * as _ from "lodash"
import {Query,ApiRequestService} from '../../core/services/apiRequestService';
import {MenuToggleNotifier} from '../../core/services/menuToggleNotifier';
import {PsycNETQuery} from "../services/search/query/psycNETQuery";
import {IResponseParameters} from "../services/search/query/components/interfaces";
import {Criteria} from "../services/search/query/components/criteria";
import {Search} from "../services/search/search";
import {SearchCollection} from "../services/search/searchCollection";
import {ActiveSearchNotifier} from "../results/services/activeSearchNotifier";
import {HitCount, SearchResultTabCalculator} from "../results/services/tab/searchResultTabCalculator";
import {SessionStorageService} from 'ng2-webstorage';

@Component({
  selector: 'my-list',
  templateUrl: './myList.html',
  styleUrls: ['./myList.less']
})
export class MyList {
  loading : boolean;
  selectedLimit = 25;
  start = 0;
  tagFilter = 0;
  currentPage = 1;
  isSelectAll: boolean = false;
  UIDlist:any;
  Tags:any;
  query:Query;
  searchId:string;
  activeSearchChangesSubscription : any;
  tabNumbers: HitCount;
  results: any;
  facets: any = [];
  resultsSearch:Search;
  facetSearch: Search;
  filterVisible: boolean = true;
  allSelectedRecordUIDList: Array<any> = [];
  currentPageSelectedRecordUIDList : Array<any> = [];
  allRecordsOnPageUIDList : Array<any> = [];
  allDBs = ['PI', 'PA', 'PB', 'PE', 'PC', 'PT'];

  constructor(private _apiService: ApiRequestService,
              private _activeSearchNotifier : ActiveSearchNotifier,
              private _calculator : SearchResultTabCalculator,
              private _menuToggle: MenuToggleNotifier,
              private _sessionStorageService : SessionStorageService) {

    this.allSelectedRecordUIDList = this._sessionStorageService.retrieve('selectedSearchRecords') || [];

  }

  ngOnInit() {
    this.activeSearchChangesSubscription = this._activeSearchNotifier.activeSearchChanges$.subscribe();
    this.getMyList();

    this._sessionStorageService.observe('selectedSearchRecords').subscribe((list) => {
      if(list && list.length){
        this.allSelectedRecordUIDList = list;
      }else{
        this.allSelectedRecordUIDList = [];
      }
    });
    this._sessionStorageService.observe('currentPageSearchRecords').subscribe((list)=> {
      if(list && list.length)
        this.currentPageSelectedRecordUIDList = list;
      else
        this.currentPageSelectedRecordUIDList = [];
    });
  }

  filterListByTag(TagID){
    this.tagFilter = TagID;
    this.getMyList();
  }

  getMyList(){
    this.loading = true;
    let query = this._apiService.createQuery('mylist.getMyList');
    query.params({"TagID":this.tagFilter});
    this._apiService.execute(query)
      .subscribe(
        data => {
        console.log('myList Results: ', data.list);
        this.UIDlist = data.list;
        this.Tags = data.tags;
        if(this.UIDlist.length > 0) {
          this.getRecordDetails();
        }
        else {
          this.results = {};
          this.loading = false;
        }
      },
        error => console.log(error)
    );
  }
  triggerSearchTab(tab) {
    console.log('triggerSearchTab - ' + tab);
    this.currentPage = 1;
    this.resultsSearch.query.tab = tab;
    this.resultsSearch.responseParameters.start = 0;
    this.resultsSearch.responseParameters.rows = this.selectedLimit;
    this.executeFacetSearch();
    SearchCollection.update(this.resultsSearch);
    this.executeSearch();
  }
  getRecordDetails(){
    this.results = [];
    let pnQuery = new PsycNETQuery();
    let uids = [];

    this.UIDlist.forEach(rec => uids.push(rec.articleId));

    pnQuery.limits.push({field: 'UIDs', operator: 'IS', values: uids});

    pnQuery.source = 'mylist';
    let responseParams: IResponseParameters = {
      start: this.start,
      rows: this.selectedLimit,
      facet: false,
      results: true,
      hl: false
    };

    let sortBy = [{"field": "Seq", "direction": "asc"}];
    if (this.resultsSearch && this.resultsSearch.hasOwnProperty('query') && this.resultsSearch.query && this.resultsSearch.query.hasOwnProperty('displayOptions') &&
      this.resultsSearch.query.displayOptions && this.resultsSearch.query.displayOptions.hasOwnProperty('sort') && this.resultsSearch.query.displayOptions.sort) {
      sortBy = this.resultsSearch.query.displayOptions.sort;
    }

    let search = new Search(pnQuery, responseParams);
    search.query.databases = this.allDBs;



    SearchCollection.add(search);
    this.searchId = search.id;
    this.resultsSearch = SearchCollection.get(this.searchId);

    this.resultsSearch.query.displayOptions.sort = sortBy;
    this.executeSearch();
    this.executeFacetSearch(() => this.updateTabHitCount());

  }


  identifySelectAll(selected){

    let newRecordUID = _.pick(selected.newlySelected, ['UID','ProductCode']);
    let index = _.findIndex(this.allSelectedRecordUIDList, newRecordUID);
    if(index == -1){
      this.allSelectedRecordUIDList.push(newRecordUID);
      this.currentPageSelectedRecordUIDList.push(newRecordUID);
    }else{
      this.allSelectedRecordUIDList.splice( index, 1);
      let curIndex = _.findIndex(this.currentPageSelectedRecordUIDList , newRecordUID);
      if(curIndex != -1){
        this.currentPageSelectedRecordUIDList.splice( curIndex, 1);
      }
    }

    this._updateAllSelectedRecordLocalStorage();
    this._updateCurrentPageSelectedRecordLocalStorage();
    this.isSelectAll = (selected.selectedRecordList.length === this.results.results.result.doc.length);
  }

  private _updateAllSelectedRecordLocalStorage(){
    this._sessionStorageService.store("selectedSearchRecords", this.allSelectedRecordUIDList);
  }

  private _updateCurrentPageSelectedRecordLocalStorage(){
    this._sessionStorageService.store("currentPageSearchRecords", this.currentPageSelectedRecordUIDList);
  }

  setSelectAll(selectAllFag){
    if(this.results.results && this.results.results.result && this.results.results.result.doc){
      for(var search of this.results.results.result.doc){
        search["isSelected"] = selectAllFag;
        let newSelectedUID = _.pick(search, ['UID','ProductCode']);
        let index = _.findIndex(this.allSelectedRecordUIDList, newSelectedUID);
        if(selectAllFag && index == -1 ){
          this.allSelectedRecordUIDList.push(newSelectedUID);
          this.currentPageSelectedRecordUIDList.push(newSelectedUID);
        }else if (!selectAllFag && index > -1){
          this.allSelectedRecordUIDList.splice( index, 1);
          let curIndex = _.findIndex(this.currentPageSelectedRecordUIDList , newSelectedUID);
          if(curIndex != -1){
            this.currentPageSelectedRecordUIDList.splice( curIndex, 1);
          }
        }
      }
    }
    this._updateCurrentPageSelectedRecordLocalStorage();
    this._updateAllSelectedRecordLocalStorage();
    this.isSelectAll = selectAllFag;
  }

  /*Pagination changed*/
  getNewPagination(pagination) {
    if (pagination.hasOwnProperty('currentPage')) {
      this.currentPage = pagination.currentPage;
      var start = (this.selectedLimit * (pagination.currentPage-1));
      if (pagination.hasOwnProperty('ev') && pagination.ev) {
        this.resultsSearch.responseParameters.start = start;
        this.executeSearch();
      }
    }
  }

  changeRecordsPerPage(limit){
    this.selectedLimit = Number(limit);
    this.resultsSearch.responseParameters.rows = this.selectedLimit;
    this.resultsSearch.responseParameters.start = 0;
    this.currentPage = 1;
    this.executeSearch();
  }

  applyNewSortByOption(sortOptions){
    this.resultsSearch.query.displayOptions.sort = sortOptions;
    this.resultsSearch.responseParameters.start = 0;
    this.currentPage = 1;
    this.executeSearch();
  }

  private updateTabHitCount(){
    this.tabNumbers = this._calculator.calculateHitCount(this.facetSearch, this.facets);
  }

  private executeSearch(){
    this.loading = true;

    if(this.results && this.results.results && this.results.results.result){
      this.results.results.result = {};
    }

    this.selectedLimit = this.resultsSearch.responseParameters.rows;

    // reset current page selected records
    this._sessionStorageService.clear('currentPageSearchRecords');

    // calculate page starting location
    this.resultsSearch.responseParameters.start = (this.currentPage-1) * this.selectedLimit;

    this.performSearch(this.resultsSearch, (err, data) => {
      if(data){

        // update search in our collection
        this.results = this.applyRecordLevelProperties(data);

        SearchCollection.update(this.results.search);
        this.resultsSearch = SearchCollection.get(this.results.search.id);
        this._activeSearchNotifier.notifySearchChanged(this.results.search.id);
        this.searchId = this.results.search.id;

      }
      this.loading = false;
    });
  }

  private performSearch(search: Search, cb: Function) {
    let qry = this._apiService.createQuery('search.results');
    qry.params(search);

    // we don't count the My List actions for pageId or eventId for search result data.  Only the MyList page visit.
    qry.logs({
      exclude: true
    });

    this._apiService.execute(qry).subscribe(
        data => {
        cb(null , data);
      },
        error => {
        console.log(error);
        cb(error);
      }
    );
  }

  private applyRecordLevelProperties(results){
    this.allRecordsOnPageUIDList = [];
    let selectedCount: number = 0;
    if(results.results && results.results.result && results.results.result.hasOwnProperty('doc')&& results.results.result.doc){
      results.results.result.doc.forEach(record => {

        //handle email option - all record on page
        this.allRecordsOnPageUIDList.push(_.pick(record, ['UID','ProductCode']));

        //handle select checkbox
        if(this.allSelectedRecordUIDList && this.allSelectedRecordUIDList.length){
          record["isSelected"] = _.findIndex(this.allSelectedRecordUIDList, _.pick(record, ['UID','ProductCode'])) != -1;

          if(record["isSelected"]){
            this.currentPageSelectedRecordUIDList.push(_.pick(record, ['UID','ProductCode']));
          }
        }

        if(record["isSelected"]){
          selectedCount ++;
        }
      });

      this.isSelectAll = (selectedCount === results.results.result.doc.length);
      this._updateCurrentPageSelectedRecordLocalStorage();
      return results;
    }else{
      return results;
    }
  }

  private executeFacetSearch(cb?:Function){
    this.facetSearch = _.cloneDeep(this.resultsSearch);
    this.facetSearch.responseParameters.facet = true;
    this.facetSearch.responseParameters.results = false;
    this.facetSearch.query.databases = this.allDBs;
    this.facetSearch.query.source = 'mylistFacet';

    this.performSearch(this.facetSearch, (err, data) => {
      if(data){
        if(data.results.facets.length){
          this.facets = data.results.facets;
          if(cb){
            cb();
          }
        }

      } else{
        cb();
      }

    });
  }

  toggleFilterView() {
    this._menuToggle.toggle(this.filterVisible);
    this.filterVisible = !this.filterVisible;
  }

  ngOnDestroy() {
    this.activeSearchChangesSubscription.unsubscribe();
  }
}
