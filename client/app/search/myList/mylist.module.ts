import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { SearchCommonModule } from '../searchcommon.module';
import { MyList } from "./myList";

import { routing } from './mylist.routes';

@NgModule({
  imports: [SharedModule, SearchCommonModule, routing],
  declarations: [],
  providers: []
})
export class MyListModule {}
