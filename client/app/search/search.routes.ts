import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {SearchBasic} from "./forms/basic/basic";
import {Advanced} from "./forms/advanced/advanced";
import {SearchResults} from "./results/results";
import {CitedRefsResults} from "./results/citedRefsResults";
import {CitedBy} from "./citedBy/citedBy";
import {MyList} from "./myList/myList";
import {RecentSearch} from "./recent/recentSearch";
import {PHResults} from './results/components/phResults/phResults';

import {AuthGuardService} from '../core/services/authGuardService';
import { UIDGuardService } from '../shared/id/uidGuardService';
import {DefaultSearchPageService} from '../core/services/defaultSearchPageService';
import {CitedRefGuardService} from '../core/services/resourceGuardService';

const routes: Routes = [
  { path: '', component: Advanced,  canActivate: [DefaultSearchPageService] },
  { path: 'basic', component: SearchBasic},
  { path: 'advanced', component: Advanced},
  { path: 'citedRefs', loadChildren: 'app/search/forms/citedRefs/citedRefs.module#CitedRefsModule'},
  { path: 'results', component: SearchResults},
  { path: 'citedRefsResults', component: CitedRefsResults, canActivate: [CitedRefGuardService]},
  { path: 'citedBy/:uid', component: CitedBy, canActivate: [UIDGuardService] },
  { path: 'mylist', component: MyList, canActivate: [AuthGuardService] },
  { path: 'phResults', component: PHResults},
  { path: 'recent', component: RecentSearch},
  { path: 'print', loadChildren: 'app/search/print/print.module#PrintModule'},
  { path: 'display', loadChildren: 'app/recordDisplay/recordDisplay.module#RecordDisplayModule'}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
