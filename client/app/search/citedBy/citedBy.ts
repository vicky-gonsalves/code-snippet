import {Component} from "@angular/core";
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {ApiRequestService, Query} from '../../core/services/apiRequestService';
import {IResponseParameters , IOnlyShow } from "../services/search/query/components/interfaces";
import {Search} from "../services/search/search";
import {SearchCollection} from "../services/search/searchCollection";
import {PsycNETQuery} from "../services/search/query/psycNETQuery";
import {Criteria} from "../services/search/query/components/criteria";
import {ActiveSearchNotifier} from "../results/services/activeSearchNotifier";
import {LocationService} from '../../core/services/locationService';
import {SessionStorageService} from 'ng2-webstorage';
import * as _ from "lodash"

@Component({
  selector: 'cited-by',
  templateUrl: './citedBy.html',
  styleUrls: ['./citedBy.less']
})
export class CitedBy {

  loading : boolean;
  resultsSearch: Search;
  results: any;
  totalResults : number;
  isSelectAll: boolean;
  selectedLimit: number = 25;
  currentPage: number = 1;
  currentCitedRecord : any ;
  uid: string;
  activeSearchChangesSubscription : any;
  isInitialPageFromSearchResults: boolean;
  allSelectedRecordUIDList: Array<any> = [];
  currentPageSelectedRecordUIDList : Array<any> = [];
  allRecordsOnPageUIDList : Array<any> = [];
  isAllAbsOpen: boolean;

  constructor(private _apiService: ApiRequestService,
              private _activeSearchNotifier : ActiveSearchNotifier,
              private _location: Location,
              private _route: ActivatedRoute,
              private _locationService: LocationService,
              private _sessionStorageService : SessionStorageService) {

    //here component is binded like mylist page
    this.allSelectedRecordUIDList = this._sessionStorageService.retrieve('selectedSearchRecords') || [];
  }

  ngOnInit(){
    this.activeSearchChangesSubscription = this._activeSearchNotifier.activeSearchChanges$.subscribe();

    this._route.params.subscribe(routeParams => {
      this._route.queryParams.subscribe(queryParams => {

        this.uid = routeParams['uid'];

        let params = this._locationService.parseQueryParams(queryParams, 'cited-by');
        this.currentPage = params.page;
        this.selectedLimit = params.rows;

        this.isInitialPageFromSearchResults = queryParams.hasOwnProperty('sr');

        this.executeSearch();
      });
    });

    this._sessionStorageService.observe('selectedSearchRecords').subscribe((list) => {
      if(list && list.length){
        this.allSelectedRecordUIDList = list;
      }else{
        this.allSelectedRecordUIDList = [];
      }
    });
    this._sessionStorageService.observe('currentPageSearchRecords').subscribe((list)=> {
      if(list && list.length)
        this.currentPageSelectedRecordUIDList = list;
      else
        this.currentPageSelectedRecordUIDList = [];
    });

  }

  public executeSearch(){

    let filter: IOnlyShow = {
      peerReviewed : false,
      ftOnly: false,
      testAvailable: false,
      latestUpdate: false,
      openAccess: false,
      impactStatement: false
    };

    let pnQuery = new PsycNETQuery();
    pnQuery.criteria.push( new Criteria('UID', 'OR',  this.uid , 'user') );
    pnQuery.filter.onlyShow = filter;
    pnQuery.source = 'easy';

    let responseParams: IResponseParameters = {
      start: (this.selectedLimit * (this.currentPage - 1)),
      rows: this.selectedLimit
    };

    this.resultsSearch = new Search(pnQuery, responseParams);
    this._executeResultSearch();
  }

  private _executeResultSearch(){
    this.loading = true;
    if (this.results) {
      this.results = {};
    }

    //reset current page selected records
    this._sessionStorageService.clear('currentPageSearchRecords');

    this.performSearch(this.resultsSearch, (err, data) => {
      console.log('results: ', data);

      if(data.results && data.results.result && data.results.result.hasOwnProperty('doc')&& data.results.result.doc){
        this.currentCitedRecord = data.results.result.doc.shift();
        this.results = this.applyRecordLevelProperties(data);
      }
      this.totalResults = parseInt(this.results.results.result.numFound);
      // update Active Search with generated ID
      SearchCollection.add(this.results.search);
      this._activeSearchNotifier.notifySearchChanged(this.results.search.id);


      // Detect when coming from Cited Refs page so that we can log this action differently
      if(this.isInitialPageFromSearchResults){
        this._location.replaceState('/search/citedBy/'+this.uid);
        this.isInitialPageFromSearchResults = false;
      }


      this.loading = false;
    });
  }

  // execute a specific search
  public performSearch(search: Search, cb: Function) {
    let qry = this._apiService.createQuery('search.citedIn');
    qry.params(search);

    this._apiService.execute(qry).subscribe(
      data => {
        cb(null , data);
      },
        error => {
        console.log(error);
        cb(error);
      }
    );
  }

  private applyRecordLevelProperties(results){
    this.allRecordsOnPageUIDList = [];
    let selectedCount: number = 0;
    if(results.results && results.results.result && results.results.result.hasOwnProperty('doc')&& results.results.result.doc){
      results.results.result.doc.forEach(record => {

        //handle email option - all record on page
        this.allRecordsOnPageUIDList.push(_.pick(record, ['UID','ProductCode']));

        //handle select checkbox
        if(this.allSelectedRecordUIDList && this.allSelectedRecordUIDList.length){
          record["isSelected"] = _.findIndex(this.allSelectedRecordUIDList, _.pick(record, ['UID','ProductCode'])) != -1;

          if(record["isSelected"]){
            this.currentPageSelectedRecordUIDList.push(_.pick(record, ['UID','ProductCode']));
          }
        }

        if(record["isSelected"]){
          selectedCount ++;
        }
      });

      this.isSelectAll = (selectedCount === results.results.result.doc.length);
      this._updateCurrentPageSelectedRecordLocalStorage();
      return results;
    }else{
      return results;
    }
  }

  displayCitedByItem(uid) {
    this.allSelectedRecordUIDList = [];
    this._updateAllSelectedRecordLocalStorage();
    this.currentPageSelectedRecordUIDList = [];
    this._updateCurrentPageSelectedRecordLocalStorage();

    this.uid = uid;
    this._locationService.updateLocation('/search/citedBy/' + uid);
    this.selectedLimit = 25;
    this.currentPage = 1;
    this.executeSearch();
  }

  /*Pagination changed*/
  getNewPagination(pagination) {
    if (pagination.hasOwnProperty('currentPage') && pagination.currentPage !== this.currentPage) {
      this.currentPage = pagination.currentPage;
      if (pagination.hasOwnProperty('ev') && pagination.ev) {
        this.executeSearch();
      }
    }
  }

  /*apply new records page per limit*/
  changeRecordsPerPage(limit){
    if (this.selectedLimit !== +limit) {
      this.selectedLimit = +limit;
      this.currentPage = 1;
      this.isAllAbsOpen = false;
      this.executeSearch();
    }
  }

  setSelectAll(selectAllFag){
    if(this.results.results && this.results.results.result &&  this.results.results.result.hasOwnProperty('doc') && this.results.results.result.doc){
      for(var search of this.results.results.result.doc){
        search["isSelected"] = selectAllFag;
        let newSelectedUID = _.pick(search, ['UID','ProductCode']);
        let index = _.findIndex(this.allSelectedRecordUIDList, newSelectedUID);
        if(selectAllFag && index == -1 ){
          this.allSelectedRecordUIDList.push(newSelectedUID);
          this.currentPageSelectedRecordUIDList.push(newSelectedUID);
        }else if (!selectAllFag && index > -1){
          this.allSelectedRecordUIDList.splice( index, 1);
          let curIndex = _.findIndex(this.currentPageSelectedRecordUIDList , newSelectedUID);
          if(curIndex != -1){
            this.currentPageSelectedRecordUIDList.splice( curIndex, 1);
          }
        }
      }
    }
    this._updateAllSelectedRecordLocalStorage();
    this._updateCurrentPageSelectedRecordLocalStorage();
    this.isSelectAll = selectAllFag;
  }

  identifySelectAll(selected){
    let newRecordUID = _.pick(selected.newlySelected, ['UID','ProductCode']);
    let index = _.findIndex(this.allSelectedRecordUIDList, newRecordUID);
    if(index == -1){
      this.allSelectedRecordUIDList.push(newRecordUID);
      this.currentPageSelectedRecordUIDList.push(newRecordUID);
    }else{
      this.allSelectedRecordUIDList.splice( index, 1);
      let curIndex = _.findIndex(this.currentPageSelectedRecordUIDList , newRecordUID);
      if(curIndex != -1){
        this.currentPageSelectedRecordUIDList.splice( curIndex, 1);
      }
    }

    this._updateAllSelectedRecordLocalStorage();
    this._updateCurrentPageSelectedRecordLocalStorage();
    this.isSelectAll = (selected.selectedRecordList.length === this.results.results.result.doc.length);
  }

  private _updateAllSelectedRecordLocalStorage(){
    this._sessionStorageService.store("selectedSearchRecords", this.allSelectedRecordUIDList);
  }

  private _updateCurrentPageSelectedRecordLocalStorage(){
    this._sessionStorageService.store("currentPageSearchRecords", this.currentPageSelectedRecordUIDList);
  }


  setAbstractDisplayStatus() {
    this.toggleAllAbstract();
  }

  toggleAllAbstract() {
    this.isAllAbsOpen = !this.isAllAbsOpen;
    var ids = [];
    let logItems = [];

    this.results.results.result.doc.forEach(article=> {
      if(!article.hasOwnProperty('Abstract')){
        ids.push(article.UID);
      } else {
        logItems.push({
          UID: article.UID,
          ProductCode: article.ProductCode
        });
      }

      article['isAbsOpen'] = this.isAllAbsOpen;
      article['abstractLoading'] = true;
    });

    if(this.results.results.result.doc.length && ids.length) {
      let qryAbstract: Query = this._apiService.createQuery('abstract.get');
      qryAbstract.params({
        uids: ids,
        rows : ids.length
      });

      qryAbstract.logs({
        pageId: 'Within_page_RV/RC'
      });

      this._apiService.execute(qryAbstract).subscribe(
        data => {
          console.log('Abstracts: ', data);
          var abstractList = data['response'].result.doc;
          var abstractIds = _.map(abstractList, 'UID');
          this.results.results.result.doc.forEach(article=> {
            var index = abstractIds.indexOf(article['UID']);
            if(index > -1)
              article['Abstract']= abstractList[index].Abstract.English;

            article['abstractLoading'] = false;
          });
        },
        error => {
          console.log(error);
        }
      );
    }

    // The already loaded abstracts should be logged here.
    if (this.isAllAbsOpen && logItems.length) {
      let logQuery = this._apiService.createQuery('log.logEntry');
      logQuery.params({ abstractsViewedData: logItems });
      logQuery.logs({ pageId: 'Within_page_RV/RC' });
      this._apiService.execute(logQuery).subscribe();
    }
  }

  ngOnDestroy() {
    this.activeSearchChangesSubscription.unsubscribe();
  }
}
