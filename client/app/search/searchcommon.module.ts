/**
 * These are modules common between search, record display, and fulltext
 */


import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ExportRISModal } from '../recordDisplay/components/exportRISModal/exportRISModal';
import { PrintRecordModal } from './print/components/printRecordModal/printRecordModal';
import { CitationPE } from '../recordDisplay/components/citationPE/citationPE';


const declarations = [
  ExportRISModal,
  PrintRecordModal,
  CitationPE
];

@NgModule({
  imports: [SharedModule],
  declarations: [ ...declarations ],
  providers: [],
  exports:[ ...declarations ]
})
export class SearchCommonModule {}
