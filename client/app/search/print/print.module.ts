import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { SearchCommonModule } from '../searchcommon.module';
import { RecordDisplayCommonModule } from '../../recordDisplay/recordDisplayCommon.module';

import {PrintPreviewPage} from "./printPreviewPage";
import {APACitation} from "./components/apaCitation/apaCitation";
import {SearchHistory} from "./components/searchHistory/searchHistory";

import {SearchNavigation} from '../services/searchNavigation';
import {RecordFormatter} from '../../recordDisplay/services/format/recordFormatter';
import {DocumentTypeFormatter} from '../../recordDisplay/services/format/DocumentTypeFormatter';

import { routing } from './print.routes';


@NgModule({
  imports: [SharedModule, SearchCommonModule, RecordDisplayCommonModule, routing],
  declarations: [PrintPreviewPage, APACitation, SearchHistory],
  providers: [SearchNavigation, RecordFormatter, DocumentTypeFormatter]
})
export class PrintModule {}
