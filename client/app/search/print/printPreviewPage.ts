import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {SessionStorageService} from 'ng2-webstorage';
import {IRecord} from "../../recordDisplay/services/format/IRecord";
import {DocumentTypeFormatter} from '../../recordDisplay/services/format/DocumentTypeFormatter';
import {ApiRequestService, Query} from '../../core/services/apiRequestService';
import {RecordFormatter} from "../../recordDisplay/services/format/recordFormatter";
import * as _ from "lodash";
//import {TimerObservable} from "rxjs/observable/TimerObservable";

@Component({
  selector : 'print-preview-page',
  templateUrl: './printPreviewPage.html',
  styleUrls: ['./printPreviewPage.less','../../fulltext/psycnet_fulltext_print.css']
})

export class PrintPreviewPage{
  loading: boolean;
  activeSearch: any;
  printDisplay: any;
  queryParams : any;
  params: any;
  allRecords: IRecord[];
  counter: number;
  isViewChecked : boolean;
  fulltext:string;
  isPTChild: boolean;
  uid: string;

  hideCitationHeader : boolean;
  hideAbstractHeader : boolean;
  hideFullRecordHeader : boolean;
  hideCitedRefHeader : boolean;

  includeSearch : boolean;
  includeCitation : boolean;
  includeAbstract : boolean;
  includeFullRecordDisplay : boolean;
  includeCitedRefs : boolean;
  includeAPACitation : boolean;
  includeFullTextHTML: boolean;

  constructor( private _apiService: ApiRequestService,
               private _router: Router,
               private _sessionStorageService : SessionStorageService){
    console.log("In constructor");
  }

  ngOnInit() {
    console.log("Here1");
    this.counter = 0;
    this._resetState();

    console.log("Here2");

    let printObject = this._sessionStorageService.retrieve('apa-print-document');
    console.log("printObject-->" + JSON.stringify(printObject));

    if (printObject && printObject.print && printObject.print.selectedType === 'FullTextHTML') {
      this.uid = printObject.uid;
      this.printDisplay = 'FullText';

      this._getFullTextHTMLRecordByUID(printObject.uid,(accessDeniedRecordList) => {
          this.isViewChecked=true;
          //this.showPrintPreview();
      });
    }
    else if (printObject && printObject.print && printObject.print.selectedType !== 'FullTextHTML') {
      this.activeSearch = printObject.search;
      this.printDisplay = printObject.print.display;
      this.queryParams = {id: printObject.search.id};

      this._markRenderingChanges(printObject.print);
      if (printObject.isRecordDisplay) {
        if (printObject.isPTChild) {
          this.isPTChild = true;
          this.getPTRecord(printObject.UIDList, (accessDeniedRecordList) => {
            this.isViewChecked = true;
            if (accessDeniedRecordList && accessDeniedRecordList.length) {
              this._handleAccessDeniedRecords(accessDeniedRecordList);
            }
          });
        } else {
          if(Array.isArray(printObject.record)){
            this.allRecords = _.cloneDeep(printObject.record);
          } else {
            this.allRecords.push(_.cloneDeep(printObject.record));
          }
          this.isViewChecked = true;
          if (!printObject.record.HasAccess) {
            this._handleAccessDeniedRecords([printObject.record]);
          }
        }

      } else {
        this._getRecordsDetailsByUID(printObject.UIDList, (accessDeniedRecordList) => {
          this.isViewChecked = true;
          if (accessDeniedRecordList && accessDeniedRecordList.length) {
            this._handleAccessDeniedRecords(accessDeniedRecordList);
          }

        });
      }
    } else {
      this._router.navigate(['/search']);
    }

  }

  private _handleAccessDeniedRecords(accessDeniedRecordList){
    if(accessDeniedRecordList.length == this.allRecords.length){
      this.showPrintPreview();
    }else{
      this.counter = this.counter + accessDeniedRecordList.length;
    }
  }

  showPrintPreview(){
    this.counter++;
    if(this.counter == this.allRecords.length){
      window.print();
    }
  }

  ngAfterViewChecked(){
    if(this.isViewChecked && !this.includeCitedRefs){
      console.log("ngAfterViewChecked");
      this.isViewChecked = false;
      window.print();
    }
  }

  private _resetState(){
    this.allRecords= [];
    this.isViewChecked = false;
    this.includeSearch = false;

    this.includeCitation = false;
    this.includeAbstract = false;
    this.includeFullRecordDisplay = false;
    this.includeCitedRefs = false;
    this.includeAPACitation = false;
    this.includeFullTextHTML = false;

    this.hideCitationHeader = false;
    this.hideAbstractHeader = false;
    this.hideFullRecordHeader = false;
    this.hideCitedRefHeader = false;
  }

  private _markRenderingChanges(print){
    this.includeSearch = print.includeSearch;

    switch(print.display){
      case 'Citation' :
            this._hideHeaders(false, false, false, false);
            this._includePrintContents(true, false, false, false, false,false);
            break;
      case 'CitationAndAbstract' :
            this._hideHeaders(false, false, false, false);
            this._includePrintContents(true, true, false, false, false,false);
            break;
      case 'Details' :
            this._hideHeaders(false, false, true, false);
            this._includePrintContents(true, true, true, false, false,false);
            break;
      case 'DetailsWithRefs' :
            this._hideHeaders(false, false, true, true);
            this._includePrintContents(true, true, true, true, false,false);
            break;
      case 'APAStyle' :
            this._hideHeaders(true, false, false, false);
            this._includePrintContents(false, false, false, false, true,false);
            break;
      case 'FullText':
            this._hideHeaders(true,true,true,true);
            this._includePrintContents(false,false,false,false,false,true);
    }
  }

  private _includePrintContents(isCitation, isAbstract, isFullRecord, isCitedRefs, isAPACitation,isFullTextHTML){
    this.includeCitation = isCitation;
    this.includeAbstract = isAbstract;
    this.includeFullRecordDisplay = isFullRecord;
    this.includeCitedRefs = isCitedRefs;
    this.includeAPACitation = isAPACitation;
    this.includeFullTextHTML = isFullTextHTML;
  }

  private _hideHeaders(isCitation, isAbstract , isFullRecord, isCitedRefs){
    this.hideCitationHeader = isCitation;
    this.hideAbstractHeader = isAbstract;
    this.hideFullRecordHeader = isFullRecord;
    this.hideCitedRefHeader = isCitedRefs;
  }

  private _getFullTextHTMLRecordByUID(uid: string,cb){
    this.loading=true;
    var qry : Query = this._apiService.createQuery('fulltext.fulltextprint');
    let params = {uid:uid};
    qry.params(params);
    this.allRecords= [];
    let accessDinedList = [];

    console.log(qry);

    this._apiService.execute(qry).subscribe(data => {

      if(data){
        console.log("got a response for full-text from server.");
        console.log(data);
        //console.log(this.includeFullTextHTML);
        this.includeFullTextHTML=true;

        this.fulltext=data.fulltext;
        //for(let record of data.doc){
        //  let alteredRecord =  RecordFormatter.convertToPNRecord(record);
        //  if(this.includeCitedRefs){
        //    alteredRecord['isCiteOpen'] = true;
         //   if(!alteredRecord.HasAccess){
         //     accessDinedList.push(alteredRecord);
         //   }
         // }
         // this.allRecords.push( alteredRecord );
        //}
      }


      this.loading = false;
      cb(accessDinedList);
    });

  }

  private _getRecordsDetailsByUID(uidList: string[], cb){
    this.loading = true;
    var qry : Query = this._apiService.createQuery('record.fetchPrintingRecords');
    let params = {UIDList : uidList};
    qry.params(params);
    this.allRecords= [];
    let accessDinedList = [];
    let uids = uidList.map(obj => {return obj['UID'];});
    this._apiService.execute(qry).subscribe(data => {

      if(data && data.hasOwnProperty("doc") && data.doc.length){
        let recordList = data.doc;
        recordList.sort((a,b) =>{
          return uids.indexOf(a.UID) - uids.indexOf(b.UID);
        });
        for(let record of recordList){
          let alteredRecord =  RecordFormatter.convertToPNRecord(record);

          if( !(alteredRecord.hasOwnProperty('DocumentType') && alteredRecord.DocumentType.length) ) {
            let documentTypeLabel = DocumentTypeFormatter.getDocumentTypeLabel(record);
            if (documentTypeLabel.trim().length) {
              documentTypeLabel = documentTypeLabel.replace(/(\/)/g, '/ ');
              alteredRecord['DocumentTypeLabel'] = (documentTypeLabel.indexOf(';') > -1) ? documentTypeLabel.split(';')[0] : documentTypeLabel;
            }
          }

          if(this.includeCitedRefs){
            alteredRecord['isCiteOpen'] = true;
            if(!alteredRecord.HasAccess){
              accessDinedList.push(alteredRecord);
            }
          }
          this.allRecords.push( alteredRecord );
        }
      }

      this.loading = false;

      cb(accessDinedList);
    });
  }

  getPTRecord(uidList, cb) {
    this.loading = true;
    let childRecord;
    let accessDinedList = [];
    this.allRecords = [];
    let qry: Query = this._apiService.createQuery('record.fetchPrintingRecords');
    let params = {UIDList : uidList};
    qry.params(params);
    let uids = uidList.map(obj => {return obj['UID'];});
    this._apiService.execute(qry).subscribe(
      data => {
        if (data && data.doc && data.doc.length > 1) {
          let parentRecord = data.doc[0];
          for (let uid of uids) {
            let record = data.doc.filter(doc => doc.UID == uid);
            if (record.length) {
              childRecord= RecordFormatter.convertToPNRecord(record[0]);
              childRecord.Fee = parentRecord.Fee;
              childRecord.Format = parentRecord.Format;
              childRecord.Purpose = parentRecord.Purpose;
              childRecord.Abstract = parentRecord.Abstract;
              childRecord.Language = parentRecord.Language;
              childRecord.Construct = parentRecord.Construct;
              childRecord.Commercial = parentRecord.Commercial;
              childRecord.HasAbstract = parentRecord.HasAbstract;
              childRecord.Permissions = parentRecord.Permissions;
              childRecord.PTReleaseDate = parentRecord.PTReleaseDate;
              childRecord.Correspondence = parentRecord.Correspondence;
            }
            if(this.includeCitedRefs){
              childRecord['isCiteOpen'] = true;
              if(!childRecord.HasAccess){
                accessDinedList.push(childRecord);
              }
            }
            this.allRecords.push(childRecord);
          }
        }
        this.loading = false;
        cb(accessDinedList);
      },
      error => {
        this.loading = false;
        console.log('Record Error: ', error);
      }
    );
  }

  navigatePreviousState(){
    if (this.printDisplay === 'FullText') {
      this._router.navigate(['/fulltext', this.uid+'.html'], { queryParams: { fromPrint: 1 } });
    } else {
      window.history.back();
    }
  }
}
