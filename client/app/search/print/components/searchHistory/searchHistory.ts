import {Component, Input} from '@angular/core';

@Component({
  selector : 'search-history',
  templateUrl: './searchHistory.html',
  styleUrls: ['./searchHistory.less']
})

export class SearchHistory{
  @Input() searchQuery : any;

  constructor(){

  }

  ngOnInit() {
    if(this.searchQuery && this.searchQuery.databases && this.searchQuery.databases.length) {
      let phIndex = this.searchQuery.databases.indexOf('PH');
      if(phIndex >= 0) {
        this.searchQuery.databases.splice(phIndex, 1);
      }
    }
  }

}

