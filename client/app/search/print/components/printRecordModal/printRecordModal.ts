import {Component, Input, ViewChild, Optional} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap';
import {SessionStorageService} from 'ng2-webstorage';
import {IRecord} from "../../../../recordDisplay/services/format/IRecord";
import {Router} from '@angular/router';
import * as _ from "lodash";
import {ApiRequestService, Query} from "../../../../core/services/apiRequestService";
import { Identity } from "../../../../core/services/identity/identity";
import { IdentityService } from "../../../../core/services/identity/identityService";

@Component({
  selector : 'print-record-modal',
  templateUrl: './printRecordModal.html',
  styleUrls: ['./printRecordModal.less']
})

export class PrintRecordModal{
  @Input() isMyList : boolean;
  @Input() parentId:string;
  @Input() recordsPerPage : number;
  @Input() recordList : IRecord[];
  @Input('record') recordDetails: IRecord;
  @Input() allRecordsOnPageUIDList: Array<any>;
  @ViewChild('printRecordModal') public printRecordModal : ModalDirective;

  print: any;
  displayList : Array<any> = [];
  selectedRecordUIDList= [];
  currentPageSelectedRecordUIDList = [];
  currentSearchDetails : any;
  submitted: boolean;
  isFormInValid: boolean;

  showFullRecordOptions: boolean;

  constructor(private _sessionStorageService : SessionStorageService,
              private _router: Router,
              private _apiService: ApiRequestService) { }

  ngOnInit() {
    IdentityService.identity.subscribe( (identity: Identity) => {
      this.showFullRecordOptions = (identity.isIndividual || identity.isOrganization);

      this._reset();
      this._loadDisplayOptions();
    });
  }

  private _getDisplayOptions(){
    let result = [
      { name: "Citation", value: "Citation" },
      { name: "Citation & Abstract", value: "CitationAndAbstract" },
      { name: "Citation APA Style", value: "APAStyle" }
    ];

    if (this.showFullRecordOptions) {
      result.splice(2, 0, { name: "Full Record Display", value: "Details" });
      result.splice(3, 0, { name: "Full Record Display plus Cited References", value: "DetailsWithRefs" });
    }

    return _.cloneDeep(result);
  }

  private _getPsycTestOptions(){
    let result = [
      { name: "Citation", value: "Citation" },
      { name: "Citation & Description", value: "CitationAndAbstract" },
      { name: "Citation APA Style", value: "APAStyle" }
    ];

    if (this.showFullRecordOptions) {
      result.splice(2, 0, { name: "Full Record Display", value: "Details" });
      result.splice(3, 0, { name: "Full Record Display plus Cited References", value: "DetailsWithRefs" });
    }

    return _.cloneDeep(result);
  }

  private _reset(){
    this.print = {
      display: '',
      includeSearch : true,
      selectedType : this.recordDetails ? 'Selected Records' : 'Selected Records - All Pages'
    };

    this.print.display = this.showFullRecordOptions ? 'Details' : 'Citation';

    this.submitted = false;
    this.isFormInValid = false;
    this._loadDisplayOptions();
  }

  openModal(@Optional() currentSearch?:any): void {
    this._reset();
    this.currentSearchDetails = currentSearch;

    this.print.includeSearch = !this.isMyList;
    this.selectedRecordUIDList = this._sessionStorageService.retrieve('selectedSearchRecords') || [];
    this.currentPageSelectedRecordUIDList = this._sessionStorageService.retrieve('currentPageSearchRecords') || [];

    this._loadDisplayOptions();
    this.printRecordModal.show();
  }

  closeModal():void {
    this.submitted = false;
    this.printRecordModal.hide();
  }

  isOpen(): boolean{
    return this.printRecordModal.isShown;
  }

  private _getUIDList(){
    let uidList = [];

    switch(this.print.selectedType){
      case 'Selected Records':
        uidList = _.cloneDeep(this.currentPageSelectedRecordUIDList);
        break;
      case 'All Records on Page':
        uidList = _.cloneDeep(this.allRecordsOnPageUIDList);
        break;
      case 'Selected Records - All Pages':
        uidList = _.cloneDeep(this.selectedRecordUIDList);
        break;
    }

    return uidList;
  }

  printDocument(){

    let uidList = this._getUIDList();
    if(uidList && uidList.length && uidList.length <= 250){
      this.submitted = true;
      this.isFormInValid = false;

      let pageId = '';
      let recordsForLog = [];
      let params;

      if(this.checkPTChild(uidList)) {
        this.print.selectedType = 'Selected Records';
        params = {UIDList: this._getUIDList(), isPTChild: true, isRecordDisplay : true};
        recordsForLog = params.UIDList;
        pageId = 'Record_print';
      }
      else if(this._hasRecordList()){
        params = {record: this.recordList, isRecordDisplay : true};
        recordsForLog = params.record;
        pageId = 'Record_print';

      } else if(this.recordDetails && !this.recordsPerPage){
        params = {record: this.recordDetails, isRecordDisplay : true};
        recordsForLog.push(params.record);
        pageId = 'Record_print';

      } else {
        //search result display page
        params = {UIDList: this._getUIDList() , isRecordDisplay : false};
        recordsForLog = params.UIDList;
        pageId = 'SR_print';
      }
      params = _.extend({}, params, { print : this.print, search : this.currentSearchDetails });

      this._sessionStorageService.store('apa-print-document', params);


      this.logPageView(pageId, recordsForLog);

      this._router.navigate(['/search/print']);
      this.closeModal();
    }else{
      this.submitted = false;
      this.isFormInValid = true;
    }
  }

  checkPTChild (uidList) {
    let result = true;
    if (uidList && uidList.length && uidList.length <= 250) {
      let i = 0;
      while(i < uidList.length && result) {
        result = !(uidList[i].ProductCode != 'PT' || uidList[i].UID.indexOf('-000') >= 0);
        i++;
      }
    }
    return result;
  }

  private _hasRecordList(){
    return this.recordList && this.recordList.length;
  }

  onSelectionTypeChanges($event){
    this._loadDisplayOptions();
  }

  private _loadDisplayOptions(){
    let uidList = this._getUIDList();
    let displayTestOptions = false;

    for(let uid of uidList){
      if(uid['UID'].indexOf('9999') === 0){
        displayTestOptions = true;
        break;
      }
    }

    this.displayList = displayTestOptions ? this._getPsycTestOptions() : this._getDisplayOptions();
  }

  private logPageView(pageId, records){
    let nonAbstractViews = ['Citation', 'APAStyle'];
    let query:Query = this._apiService.createQuery('log.logEntry');

    // if it's a PT record, change the pageId value
    if(pageId === 'Record_print'){
      records.forEach(record => {
        if(record.ProductCode === 'PT'){
          pageId = 'Record_PT_child_print';
        }
      });
    }

    query.logs({ pageId: pageId });

    // send the selected UID and ProductCodes so that we can log abstractsViewed as long as it has an abstract
    if(nonAbstractViews.indexOf(this.print.display) === -1){
      query.params({abstractsViewedData: records});
    }


    this._apiService.execute(query).subscribe();
  }
}
