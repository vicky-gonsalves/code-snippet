import {Component, Input} from "@angular/core";
import {IRecord} from "../../../../recordDisplay/services/format/IRecord";

@Component({
  selector: 'apa-citation',
  templateUrl: './apaCitation.html',
  styleUrls: ['./apaCitation.less']
})
export class APACitation {
  @Input() record: IRecord;
  @Input() hideCitationHeader: boolean;

  constructor(){

  }

}
