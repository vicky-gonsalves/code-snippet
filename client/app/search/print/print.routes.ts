import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PrintPreviewPage} from "./printPreviewPage";

const routes: Routes = [
  { path: '', component: PrintPreviewPage }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
