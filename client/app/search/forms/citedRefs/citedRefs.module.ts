import { NgModule } from '@angular/core';

import { SharedModule } from '../../../shared/shared.module';
import {SearchNavigation} from '../../services/searchNavigation';
import {SearchCitedRefs} from "./citedRefs";


import { routing } from './citedRefs.routes';


@NgModule({
  imports: [SharedModule, routing],
  declarations: [SearchCitedRefs],
  providers: [SearchNavigation]
})
export class CitedRefsModule {}
