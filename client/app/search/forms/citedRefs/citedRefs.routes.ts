import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SearchCitedRefs} from "./citedRefs";
import {CitedRefGuardService} from '../../../core/services/resourceGuardService';

const routes: Routes = [
  { path: '', component: SearchCitedRefs, canActivate: [CitedRefGuardService] }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
