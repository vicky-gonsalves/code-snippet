import {Component} from "@angular/core";
import {LocationService} from "../../../core/services/locationService";
import {Query, ApiRequestService} from "../../../core/services/apiRequestService";
import {ActivatedRoute} from '@angular/router';
import {SessionStorageService} from 'ng2-webstorage';
import {PsycNETQuery} from "../../services/search/query/psycNETQuery";
import {IResponseParameters} from "../../services/search/query/components/interfaces";
import {Criteria} from "../../services/search/query/components/criteria";
import {Search} from "../../services/search/search";
import * as _ from "lodash";
import {SearchCollection} from "../../services/search/searchCollection";
import {SearchNavigation} from "../../services/searchNavigation";
import {LocalizationService, Locale} from "angular2localization";

@Component({
  selector: 'searchcitedrefs',
  templateUrl: './citedRefs.html',
  styleUrls: ['./citedRefs.less']
})
export class SearchCitedRefs extends Locale{
  formData:ICitedRefsFormData;
  searchId:string;

  defaultSort = [{code: 'CiDes', direction: '-1'}];
  criteriaMapping = {
    firstName: 'FirstName',
    lastName: 'LastName',
    title: 'DocumentTitle',
    source: 'SourceTitle',
    year: 'PubYear'
  };

  constructor(private _apiService:ApiRequestService,
              private _route:ActivatedRoute,
              private _locationService : LocationService,
              private searchNavigation: SearchNavigation,
              private _sessionStorageService: SessionStorageService,
              public localization: LocalizationService) {
    super(null, localization);
  }

  ngOnInit() {

    this._route.queryParams.subscribe(params => {
      this.searchId = params['id'];

      this.initializeSearchObj();

      // determine if we need to populate the form model with a recent search (Edit search)local
      this.populateFormData();

      this.logPageView();
    });

  }


  ngOnDestroy(){
    this._saveSearchFormState();
  }

  private _saveSearchFormState(){
    let pnQuery = this.formToQuery();
    let responseParams: IResponseParameters = {
      start: 0,
      rows: 25,
      facet: true,
      results: true
    };

    // add this search to our collection and store the generated search id
    let search = new Search(pnQuery, responseParams);
    SearchCollection.add(search);

    console.log("saving search form state: ", search.id);

    this._sessionStorageService.store('searchFormId', search.id);
  }

  initializeSearchObj() {
    this.formData = this.newFormData();
  }

  public newFormData():ICitedRefsFormData {
    return {
      firstName: '',
      lastName: '',
      title: '',
      source: '',
      year: ''
    };
  }

  search() {
    let pnQuery = this.formToQuery();
    let responseParams:IResponseParameters = {
      start: 0,
      rows: 25,
      facet: true,
      results: true
    };

    let search = new Search(pnQuery, responseParams);
    search.query.applySort(this.defaultSort);

    // add this search to our collection
    SearchCollection.add(search);

    // allow back button on search results page to populate search
    let params = [{
      name: 'id',
      value: search.id
    }];
    this._locationService.updateLocation('/search/citedRefs', params);

    // setting queryParams to an empty object will remove existing query params from URL
    this.searchNavigation.gotoResults(search, {queryParams: {id: search.id}});
  }

  clear() {
    this.initializeSearchObj();
  }

  // convert the basic search form model to a PsycNETQuery
  private formToQuery():PsycNETQuery {
    let formDataCopy = _.clone(this.formData);
    let pnQuery = new PsycNETQuery();
    pnQuery.source = 'citedRefs';
    pnQuery.databases = [];

    // if both first and last name are used, then apply the NEAR/2 proximity operator
    if(formDataCopy.lastName.length && formDataCopy.firstName.length){
      pnQuery.criteria.push(new Criteria(this.criteriaMapping['lastName'], 'AND', formDataCopy.lastName, 'user'));
      pnQuery.criteria.push(new Criteria(this.criteriaMapping['firstName'], 'NEAR/2', formDataCopy.firstName, 'user'));

      // remove lastName and firstName so that we can iterate over the rest
      delete formDataCopy.lastName;
      delete formDataCopy.firstName;
    }

    // iterate over the rest of the form inputs to build the query
    _.forOwn(formDataCopy, (value, key)=> {
      if (formDataCopy.hasOwnProperty(key) && this.criteriaMapping.hasOwnProperty(key) && formDataCopy[key].length > 0) {
        pnQuery.criteria.push(new Criteria(this.criteriaMapping[key], 'AND', formDataCopy[key], 'user'));
      }
    });

    return pnQuery;
  }

  // convert the PsycNETQuery data to basic search form model
  private queryToForm(query:PsycNETQuery):ICitedRefsFormData {
    let data = this.newFormData();
    let swappedCriteriaMapping = _.invert(this.criteriaMapping); //swaps key value
    // populate text input
    query.criteria.forEach(qry=> data[swappedCriteriaMapping[qry.field]] = qry.value);

    // remove firstName wildcard
    data.firstName = data.firstName.replace('*','');

    return data;
  }

  private populateFormData() {

    // if there is no 'id' url parameter, then this is a new search
    if (!this.searchId) {
      return false;
    }

    // get the recent search data and populate form
    let qry = this._apiService.createQuery('recentSearch.get');
    qry.params({id: this.searchId});

    this._apiService.execute(qry).subscribe(
      data => {

        if (data.hasOwnProperty('query')) {
          this.formData = this.queryToForm(data.query);
        }
      });
  }

  // the other API Requests for this page will be cached in localStorage, so we don't want to make this log request on its own so that it's not missed
  private logPageView(){
    let query:Query = this._apiService.createQuery('log.logEntry');
    query.logs({ pageId: 'CitedRefs' });
    this._apiService.execute(query).subscribe();
  }

}

interface ICitedRefsFormData {
  firstName:string,
  lastName:string,
  title:string,
  source:string,
  year:string
}
