import {Pipe, PipeTransform} from '@angular/core';
import * as _ from "lodash";

@Pipe({name: 'omitExclusive'})
export class OmitExclusiveFormField implements PipeTransform {

  transform(formFields: Array<any>, selectedDatabases: Array<String>): Array<any> {
    if(formFields && formFields.length  ){
      var results = [];
      for(var field of formFields){

        var isAvailable = false;
        var isExclusive = ( selectedDatabases.length===1 && field.exclusive && selectedDatabases[0] === field.exclusive );
        var isBothAbsent = ( field.omitFrom == undefined && field.exclusive == undefined );
        if(field.omitFrom && field.omitFrom.length > 0){

          for(var db of selectedDatabases ){
            if(field.omitFrom.indexOf(db) == -1){
              isAvailable = true;
              break;
            }
          }
        }else if(field.omitFrom && field.omitFrom.length == 0 && selectedDatabases.length > 0){
          isAvailable = true;
        }

        if((isAvailable || isExclusive || isBothAbsent) && _.findIndex(results, {"desc" : field.desc }) == -1){
          results.push(field);
        }
      }
      return results;
    }
    return formFields;

  }
}
