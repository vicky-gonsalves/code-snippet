export interface IAdvancedFormData {
  text: string;
  field: string;
  criteria:[{field:string,operator:string,value:string,source:string}];
  filter: {
    peerReviewed: boolean;
    ftOnly: boolean;
    testAvailable: boolean;
    latestUpdate: boolean;
    openAccess: boolean;
    impactStatement: boolean;
  },
  nativeOptions : {
    pb : {
      type: {
        conferenceProceedings: boolean,
        handbookManual: boolean,
        referenceBook: boolean,
        classicBook: boolean,
        textbookStudyGuide: boolean
      },

      publicationType: {
        authored: boolean,
        edited: boolean
      },

      audience: {
        generalPublic: boolean,
        professional: boolean
      }
    }
  },
  dateRange:{begin:string,end:string,yearRange:string,publishedIn:string};
  dateselect:number;
  limits:[{field:string,operator:string,values:string[]}];

}
