import {Component, ViewChild} from "@angular/core";
import {Router,ActivatedRoute} from '@angular/router';
import {SessionStorageService} from 'ng2-webstorage';
import {Locale, LocaleService, LocalizationService} from 'angular2localization';

import {ApiRequestService, Query} from '../../../core/services/apiRequestService';
import {Search} from "../../services/search/search";
import {SearchCollection} from "../../services/search/searchCollection";
import {PsycNETQuery} from "../../services/search/query/psycNETQuery";
import {IResponseParameters,IPsycNETQuery} from "../../services/search/query/components/interfaces";
import {IAdvancedFormData} from "./interfaces/IAdvancedFormData";
import {Criteria} from "../../services/search/query/components/criteria";
import {Limit} from "../../services/search/query/components/limit";
import {IItem} from "./components/lookup/services/searchItems";
import * as _ from "lodash";
import {IdentityService} from '../../../core/services/identity/identityService';
import {IIdentity} from '../../../core/services/identity/identity';
import {DatabaseFilterSubscription} from '../../../core/services/databaseSubscription/databaseFilterSubscription';
import {LookupModal} from './components/lookup/lookupModal/lookupModal';
import {LocationService} from "../../../core/services/locationService";
import {Observable} from "rxjs";
import {PermissionService} from '../../../core/services/identity/permissionService';

import {OmitExclusiveFormField} from './pipes/omitExclusiveFormField';

@Component({
  selector: 'advanced',
  templateUrl: './advanced.html',
  styleUrls: ['./advanced.less']
})
export class Advanced extends Locale {

  @ViewChild('lookupDialog') public lookupDialog: LookupModal;
  public formData: IAdvancedFormData;
  searchId: string;
  trendingTerm: string;
  indexTerms: string;

  public typeaheadSuggestions: string[] = [];

  FormFields: Array<any> = [];
  originalFormFields : Array<any> = [];        //some fields are getting deleted after some select and deselect operations,
  originalFormFilters : Array<any> = [];        //some filters are getting deleted after some select and deselect operations
  FormFilters: Array<any> = [];
  FilterOptions: Array<any> = [];
  limitLookup = {};
  recentSearches = [];
  selectedItems: IItem;
  productCodes:string[] = [];
  private _identity: IIdentity;
  disableTypeahead = false;
  FilterOptionsCount: any;
  customInitDbSelection = false;

  // limit field showing
  limitDisplay = {
    peerReviewed: false,
    ftOnly: false,
    testAvailable: false,
    latestUpdate: false,
    openAccess: false,
    impactStatement: false,
    book: false
  };


  private readonly daysInMillis: number = 86400000;
   // private isOpened1: boolean = false;
  //private isOpened2: boolean = false;


  constructor(public _locale: LocaleService,
              public _localization: LocalizationService,
              private _apiService:ApiRequestService,
              private router: Router,
              private _route:ActivatedRoute,
              private _locationService : LocationService,
              private _permissionService: PermissionService,
              private _sessionStorageService :SessionStorageService) {
    super(_locale, _localization);

    // configure observable typeahead listener
    this.typeaheadSuggestions = Observable.create((observer: any) => {

      let typeaheadQuery = this._apiService.createQuery('typeahead.getTypeahead');
      typeaheadQuery.params({
        term: this.formData.text
      });

      this._apiService.execute(typeaheadQuery).subscribe(
        data => {
          if (data.terms.term) {
            observer.next(data.terms.term);
          }
        },
        error => console.log(error)
      );

    });

  }

  ngOnInit() {

    this.formData = _.cloneDeep(this.newFormData());

    IdentityService.identity.subscribe(data => {

      // ensure identity is loaded
      if(!data){
        return false;
      }

      this._route.queryParams.subscribe(params => {
        this._identity = data;
        this.searchId = params['id'];
        this.trendingTerm = params['term'];
        this.indexTerms = params['indexTerms'];
        this.productCodes = [];
        let advFormFields = this.getFromSession('advFormFields') || [];
        let advFormFilters = this.getFromSession('advFormFilters') || [];

         if(params['db']) {
          this.customInitDbSelection = (this._permissionService.mayAccess(params['db']) && (this._identity.isOrganization || (this._identity.isIndividual && this._permissionService.mayAccessAllDefaultProducts()) ));
         }

        if (!advFormFields.length) {
          this.initializeFormFields();
        } else {
          this.processFormFields(advFormFields);
        }

        if (!advFormFilters.length) {
          this.initializeFormFilters();
        } else {
          this.processFormFilters(advFormFilters);
        }

        this.initializeSelectedItems();

        if (this.trendingTerm) {
          this.loadTrending();
        }

        // log the search form view
        this.logPageView();
      });
    });

  }

  ngOnDestroy(){
    this._saveSearchFormState();
  }

  private _saveSearchFormState(){
    let pnQuery = this.formToQuery();
    let responseParams: IResponseParameters = {
      start: 0,
      rows: 25,
      facet: true,
      results: true
    };

    // add this search to our collection and store the generated search id
    let search = new Search(pnQuery, responseParams);
    SearchCollection.add(search);

    console.log("saving search form state: ", search.id);

    this._sessionStorageService.store('searchFormId', search.id);
  }

  public newSelectedDatabases(event) {
    console.log(event);

    // remove the selected PH database here to simplify our logic throughout controller.  We'll add PH back in when executing the search if they have access
    this.productCodes= _.without(event,'PH');

    // show the 'latest update' feature first within the date limit if only one database is selected
    if (this.productCodes.length !== 1 && this.formData.dateRange.publishedIn === 'the latest update') {
      this.formData.dateRange.publishedIn = 'in the last 7 days';
    }

    // update limit checkboxes
    this.calculateLimits();

    //Refresh all field and filter dropdowns
    this.refreshDbFieldsAndFilters();
  }

  private refreshDbFieldsAndFilters() {
    this.setVisibleFields();
    this.setVisibleFilters();

    // (this.formData) should update its (field), (criteria) and (limits) values, whenever they are selected and do not have any match within their dropdowns.
    let field = _.find(this.FormFields, fieldItem => {return fieldItem.name === this.formData.field;});
    if(!field) {
      this.formData.field = 'AnyField';
    }

    this.formData.criteria.forEach(item => {
      let field = _.find(this.FormFields, fieldItem => {return fieldItem.name === item.field;});
      if(!field) {
        item.field = 'AnyField';
      }
    });

    this.formData.limits.forEach(item => {
      let filter = _.find(this.FormFilters, filterItem => {return filterItem.name === item.field;});
      if(!filter) {
        item.field = '';
      }
    });
  }

  private setVisibleFields() {
    this.FormFields = new OmitExclusiveFormField().transform(this.originalFormFields, this.productCodes);
  }

  private setVisibleFilters() {
    this.FormFilters = new OmitExclusiveFormField().transform(this.originalFormFilters, this.productCodes);
  }

  public initializeSelectedItems() {

    this.selectedItems = {
      type: '',
      executeImmediately: false,
      terms: [],
      operator: ''
    };

  }

  stringifyTerms(selectedItems:any){
    console.log(selectedItems);
    let newSearchTerm: string = '';
    if(selectedItems.terms.length > 1) {

      newSearchTerm = "{" + selectedItems.terms[0].trim() + "} ";
      for(let x = 1; x < selectedItems.terms.length; x++) {
        newSearchTerm = newSearchTerm + selectedItems.operator + " {" + selectedItems.terms[x].trim() + "} ";
      }
    }
    else if(selectedItems.terms.length == 1) {
      newSearchTerm = "{" + selectedItems.terms[0].trim() + "}";
    }
    return newSearchTerm;
  }


  openlookupDialog(term){
    this.initializeSelectedItems();
    console.log(term);
    var fieldType:string='';
    var fieldValue:string='';
    var fieldOperator:string='';
    switch(term) {
      case 'authors':
        fieldType='Author';
        break;
      case 'journals':
        fieldType='JournalTitle';
        break;
      case 'tests':
        fieldType='TestNames';
        break;
    }
    console.log(this.formData.field);
    console.log(fieldType);
    if(this.formData.field === fieldType){
      fieldValue = this.formData.text;
      this.formData.text ="";
      this.formData.field ="";

    }else{
      for(var i=0;i<this.formData.criteria.length;i++){
        if(this.formData.criteria[i].field === fieldType){
          fieldValue = this.formData.criteria[i].value;
          this.formData.criteria[i].value ="";
          this.formData.criteria[i].field ="";
        }
      }
    }
    if(fieldValue.length) {
      if(fieldValue.indexOf("} OR {"))
        fieldOperator = "OR";
      else if(fieldValue.indexOf("} AND {"))
        fieldOperator = "AND";
      fieldValue = fieldValue.trim().slice(1, -1);
      this.selectedItems = {
        type: term,
        executeImmediately: false,
        terms: fieldValue.split('} ' +fieldOperator+' {'),
        operator: fieldOperator
      };
    console.log(this.selectedItems);
    this.lookupDialog.openDialogModal(term, this.selectedItems);
    }
    else
      this.lookupDialog.openDialogModal(term, this.selectedItems);

  }
  /*handle author, journal and test looup modal popup closing event */
  onDialogClose(selectedItems:any) {
    console.log(selectedItems);

    this.selectedItems = selectedItems;

    if(this.selectedItems == undefined || this.selectedItems == null || this.selectedItems.terms.length == 0){
      return false;
    }

    let newSearchTerm : string='';
    newSearchTerm = this.stringifyTerms(this.selectedItems);

    var fieldType:string='';
    switch(this.selectedItems.type) {
      case 'authors':
        fieldType='Author';
        break;
      case 'journals':
        fieldType='JournalTitle';
        break;
      case 'tests':
        fieldType='TestNames';

        // if the user has clicked on the Test name, then only search on PT
        if(this.selectedItems.executeImmediately){
          this.productCodes = ["PT"];
        }
        break;
    }

    if(this.formData.text == "" && this.formData.criteria[0].value == ""){

      this.formData.text = newSearchTerm;
      this.formData.field = fieldType;
    }else{

      var last = this.formData.criteria.length -1;

      if(this.formData.criteria[last].value == ""){

        this.formData.criteria[last].value= newSearchTerm;
        this.formData.criteria[last].field= fieldType;
      }else{

        this.formData.criteria.push({ field: fieldType, operator: 'AND', value: newSearchTerm, source: 'user'});
      }
    }

    if( this.selectedItems.executeImmediately == true ){
      this.formData = _.cloneDeep(this.newFormData());
      this.formData.text = newSearchTerm;
      this.formData.field = fieldType;
      this.executeSearch();
    }
  }

  public loadTrending() {


    this.formData.text=this.trendingTerm;
    this.formData.field="AnyField";
    this.productCodes = this._sessionStorageService.retrieve("apa-trending-DB");
    this.executeSearch();


  }

  public newFormData(): IAdvancedFormData {
    return {
      text: '', field:'AnyField',
      criteria:[{field:'AnyField',operator:'AND',value:'',source:'user'}],
      filter: {
        peerReviewed: false,
        ftOnly: false,
        testAvailable: false,
        latestUpdate: false,
        openAccess: false,
        impactStatement: false
      },
      nativeOptions : {
        pb : {
          type: {
            conferenceProceedings: false,
            handbookManual: false,
            referenceBook: false,
            classicBook: false,
            textbookStudyGuide: false
          },
          publicationType: {
            authored: false,
            edited: false
          },
          audience: {
            generalPublic: false,
            professional: false
          }
        }
      },
      dateRange: {begin: '', end: '', yearRange: 'All years', publishedIn: 'in the last 7 days'},
      dateselect:1,
      limits:[{field: '',operator:'is', values:[]}]
    };
  }

  public addSearchRow() {
    this.formData.criteria.push({field:'AnyField',operator:'AND',value:'',source:'user'});
  }

  public removeSearchRow(position: number) {
    this.formData.criteria.splice(position, 1);
  }

  public addLimitRow() {
    this.formData.limits.push({field:'', operator:'is', values:[]});
  }

  public removeLimitRow(position: number) {
    this.formData.limits.splice(position, 1);
  }

  // perform Basic Search
  public executeSearch(){
    let pnQuery = this.formToQuery();
    let responseParams: IResponseParameters = {
      start: 0,
      rows: 25,
      facet: true,
      results: true
    };


    // add this search to our collection
    let search = new Search(pnQuery, responseParams);
    SearchCollection.add(search);

    //clearing slected record checkbox values in result page
    this._sessionStorageService.clear("selectedSearchRecords");
    this._sessionStorageService.clear('currentPageSearchRecords');

    // allow back button on search results page to populate search
    let params = [{
      name: 'id',
      value: search.id
    }];
    this._locationService.updateLocation('/search/advanced', params);

    // setting queryParams to an empty object will remove existing query params from URL
    this.router.navigate(['/search/results'], {queryParams: {id: search.id}});
  }

  private formToQuery():PsycNETQuery {
    let pnQuery = new PsycNETQuery();
    let arrLimits:string[] = [];

    if( this.formData.text.trim() !== ''){
      pnQuery.criteria.push( new Criteria(this.formData.field, 'OR', this.formData.text, 'user') );
    }

    let limitData = this.formData.filter;
    pnQuery.filter.onlyShow = {
      ftOnly: this.limitDisplay.ftOnly && limitData.ftOnly,
      impactStatement: this.limitDisplay.impactStatement && limitData.impactStatement,
      latestUpdate: this.limitDisplay.latestUpdate && limitData.latestUpdate,
      openAccess: this.limitDisplay.openAccess && limitData.openAccess,
      peerReviewed: this.limitDisplay.peerReviewed && limitData.peerReviewed,
      testAvailable: this.limitDisplay.testAvailable && limitData.testAvailable
    };

    if(this.isSelectedDb('PB', true)) {
      pnQuery.nativeOptions = this.formData.nativeOptions;
    }

    pnQuery.source = 'advanced';
    pnQuery.databases = this.productCodes;

    // if the user has PsycTHERAPY add it back in
    if(this._permissionService.mayAccess('PH')){
      pnQuery.databases.push('PH');
    }

    // Add in additional criteria lines from form
    for(let x=0; x<this.formData.criteria.length; x++) {
      console.log("adding criteria line to query");
      if (this.formData.criteria[x].value.trim() !== '') {
        pnQuery.criteria.push(new Criteria(this.formData.criteria[x].field, this.formData.criteria[x].operator, this.formData.criteria[x].value, 'user'));
      }
    }

    //Add in limits

    for(let i=0; i<this.formData.limits.length; i++) {
      if (this.formData.limits[i].field != '' && this.formData.limits[i].values && this.formData.limits[i].values.length) {
        arrLimits = this.formData.limits[i].values.map(value=>value.trim());

       // console.log(mystringarray);
        pnQuery.limits.push(new Limit (this.formData.limits[i].field, this.formData.limits[i].operator, arrLimits.slice()));
      }

    }

    //Process Date Params:

    switch(this.formData.dateselect) {

      case 1:
             // Year Range within n years
            let now=new Date();
            let currentyear = now.getFullYear();
            let startyear=currentyear;

             pnQuery.filter.dateRange.end=currentyear.toString();
             pnQuery.filter.dateRange.field='PublicationYear';
             pnQuery.filter.dateRange.yearRange = this.formData.dateRange.yearRange;


             switch(this.formData.dateRange.yearRange) {
               case "All years":
                 pnQuery.filter.dateRange.begin='eternity';
                 pnQuery.filter.dateRange.end='eternity';
                 pnQuery.filter.dateRange.field='PublicationYear';
                   break;

               case "1 year ago":
                 startyear=currentyear-1;
                 pnQuery.filter.dateRange.begin=startyear.toString();
                   break;

               case "3 years ago":
                 startyear=currentyear-3;
                 pnQuery.filter.dateRange.begin=startyear.toString();
                  break;

               case "5 years ago":
                 startyear=currentyear-5;
                 pnQuery.filter.dateRange.begin=startyear.toString();
                 break;
             }




             break;
      case 2:
        //Check if has year input
        if ((this.formData.dateRange.begin.length || this.formData.dateRange.end.length)) {
          //When no end year or end year length is > 4
          if ((this.formData.dateRange.begin.length && this.formData.dateRange.end.length < 4) || this.formData.dateRange.end.length > 4) {
            this.formData.dateRange.end = '9999';
          }
          //When no start year or start year length is > 4
          if (!this.formData.dateRange.begin.length || this.formData.dateRange.begin.length > 4) {
            this.formData.dateRange.begin = '0';
          }
          //Make sure end year is greater than or eqals to start year
          if ((this.formData.dateRange.begin.length && this.formData.dateRange.end.length == 4) && (parseInt(this.formData.dateRange.begin) <= parseInt(this.formData.dateRange.end))) {
            // Year Range (explicit)
            pnQuery.filter.dateRange.begin = this.formData.dateRange.begin;
            pnQuery.filter.dateRange.end = this.formData.dateRange.end;
            pnQuery.filter.dateRange.field = 'PublicationYear';
          }
        }
        break;
      case 3:
        //  Published within??
        if(this.formData.dateRange.publishedIn == 'the latest update') {
          pnQuery.filter.onlyShow.latestUpdate = true;
        }else{
          let currentDate = new Date();
          let endDate = this.formatDate(currentDate);
          let duration = +(this.formData.dateRange.publishedIn.split(' ')[3]) * this.daysInMillis;
          let startDate = this.formatDate(new Date(currentDate.getTime() - duration));

          pnQuery.filter.dateRange.begin = startDate;
          pnQuery.filter.dateRange.end = endDate;
          pnQuery.filter.dateRange.field = 'releaseDate';
        }

        break;
    }

    if(this.formData.dateselect !== 3 || this.formData.dateRange.publishedIn !== 'the latest update') {
      pnQuery.filter.onlyShow.latestUpdate = false;
    }

    return pnQuery;
  }

  // convert the PsycNETQuery data to basic search form model
  private queryToForm(query: PsycNETQuery):IAdvancedFormData {
    let data = this.newFormData();

    // add facet fielters to criteria or limits
    let facets = query.filter.facets;
    while(facets.length > 0) {
      let fieldFilter = _.find(this.originalFormFields, fieldItem => {return facets[0].field === fieldItem.name;});
      if(fieldFilter) {
        query.criteria.push(new Criteria(facets[0].field , 'AND', "\"" + facets[0].value + "\"", 'user'));
      } else {
        let fieldLimit;
        this.formulateClassificationFacet(facets[0]);
        for (let key in this.limitLookup) {
          if (this.isQueryLimit(key, facets[0])) {
            fieldLimit = this.limitLookup[key];
            break;
          }
        }
        if(fieldLimit) {
          if(query.limits.length) {
            let index = query.limits.findIndex(limit => {return fieldLimit.name === limit.field && limit.operator === "is"});
            if(index > -1) {
             query.limits[index].values.push(facets[0].value);

            } else {
            query.limits.push({field: fieldLimit.name ,operator:"is", values:[facets[0].value]});
            }
          } else {
            query.limits.push({field: fieldLimit.name ,operator:"is", values:[facets[0].value]});
          }
      } else {
        query.criteria.push(new Criteria('AnyField', 'AND', "\"" + facets[0].value + "\"", 'user'));
      }
    }
    facets.shift();
    }

    // populate text input
    if(query.criteria.length > 0){
      data.text = query.criteria[0].value;
      data.field = query.criteria[0].field.replace(/Filt/g,"");

      if (query.criteria.length > 1) {

        data.criteria[0].field = query.criteria[1].field;
        data.criteria[0].value = query.criteria[1].value;
        data.criteria[0].operator = query.criteria[1].operator;

        if (query.criteria.length > 2) {

          for (let x = 2; x < query.criteria.length; x++) {
            data.criteria.push(new Criteria(query.criteria[x].field , query.criteria[x].operator, query.criteria[x].value, 'user'))
          }
        }
      }

    }

    // populate limits
    data.filter = query.filter.onlyShow;
    data.nativeOptions = query.nativeOptions;

    //populate date params
    if(data.filter.latestUpdate){

      data.dateselect = 3;
      data.dateRange.publishedIn = "the latest update";

    } else if (query.filter.dateRange.field=='PublicationYear') {

      if (query.filter.dateRange.yearRange && query.filter.dateRange.yearRange.length){
        data.dateselect = 1;
        data.dateRange.yearRange = query.filter.dateRange.yearRange;
      } else
     // console.log("Date Params:  Publication Year");
      if (query.filter.dateRange.begin!='eternity'){
        data.dateselect=2;
        data.dateRange.begin=query.filter.dateRange.begin;
        data.dateRange.end=query.filter.dateRange.end;
      }

    }
    else{
      // console.log("Date Params:  PublishedIn");
      data.dateselect = 3;

      let dateString = query.filter.dateRange.begin;
      let startDate = new Date(+dateString.substring(0, 4), +dateString.substring(4, 6) - 1, +dateString.substring(6, 8));
      dateString = query.filter.dateRange.end;
      let endDate = new Date(+dateString.substring(0, 4), +dateString.substring(4, 6) - 1, +dateString.substring(6, 8));
      let durationInDays = (endDate.getTime() - startDate.getTime()) / this.daysInMillis;

      switch (durationInDays) {
        case 30:
          data.dateRange.publishedIn = "in the last 30 days";
          break;
        case 14:
          data.dateRange.publishedIn = "in the last 14 days";
          break;
        default:
          data.dateRange.publishedIn = "in the last 7 days";
      }
    }

    //populate only show filters.

    let myobj: {field:string , operator: string, values: string[]};


     for(let i=0; i< query.limits.length; i++) {
       if (query.limits[i].values) {

         myobj={field: query.limits[i].field,operator: query.limits[i].operator, values:query.limits[i].values};


        if (i==0) {
          data.limits[0]=myobj;
        }
          else {
          data.limits.push(myobj);
        }
      }

    }

    return data;
  }

  private formulateClassificationFacet(facetLimit) {
    if(facetLimit.field === "Classification") {
      facetLimit.value = facetLimit.value.substring(0,4);
      if(+facetLimit.value >= 5000) {
        facetLimit.field = "PTClassificationCode";
      }
    }
  }

  private isQueryLimit(index, facet) {
    return this.limitLookup[index].desc.replace(/ /g,'') === facet.field || this.limitLookup[index].desc === facet.field || this.limitLookup[index].name === facet.field;
  }

  private populateFormData() {

    // if there is no 'id' url parameter, then this is a new search
    if(!this.searchId){
      if(!this.customInitDbSelection) {
        DatabaseFilterSubscription.resetSelectedDBChanges();
      }
      return false;
    }

    // try getting it from our SearchCollection first before querying our recent searches
    let localRecentSearch = SearchCollection.get(this.searchId);

    console.log("localRecentSearch: ", localRecentSearch);

    if (localRecentSearch) {
      this.formData = this.queryToForm(localRecentSearch.query);
      this.productCodes = localRecentSearch.query.databases;
      DatabaseFilterSubscription.updateSelectedDBChanges(localRecentSearch.query.databases);
      this.getFilterValues(this.formData.limits[0].field);
    } else {

      // get the recent search data and populate form
      let qry = this._apiService.createQuery('recentSearch.get');
      qry.params({id: this.searchId});

      this._apiService.execute(qry).subscribe(
        data => {
          if (data.hasOwnProperty('query')) {
            this.formData = this.queryToForm(data.query);
            this.productCodes = data.query.databases;
            DatabaseFilterSubscription.updateSelectedDBChanges(data.query.databases);
            this.getFilterValues(this.formData.limits[0].field);
          }
        });
    }
  }

  getFromSession(key) {
    return this._sessionStorageService.retrieve(key);
  }

  storeInSession(key, data) {
    this._sessionStorageService.store(key, data);
  }

  initializeFormFields() {

      // example of getting formFields using 'command' interface
    let fieldQuery = this._apiService.createQuery('search.getAdvFormFields');
    this._apiService.execute(fieldQuery)
      .subscribe(
        data => {
          this.storeInSession("advFormFields", data);
          this.processFormFields(data);
          //this.AddRemoveTestFields();
        },
        error => console.log(error)
      );
  }



  initializeFormFilters(){

    let fieldQuery = this._apiService.createQuery('search.getAdvFormFilters');
    this._apiService.execute(fieldQuery)
      .subscribe(
        data => {
          this.storeInSession("advFormFilters", data);
          this.processFormFilters(data);
        },
        error => console.log(error)
      );
  }

  processFormFields(data) {
    this.originalFormFields = data;
    this.setVisibleFields();
    console.log(this.FormFields);
  }

  processFormFilters(data) {
    this.originalFormFilters = data;
    this.limitLookup = this.convertToLookupObject(data, 'name');
    console.log(this.FormFilters);
    // determine if we need to populate the form model with a recent search (Edit search)local
    this.populateFormData();
    this.setVisibleFilters();
  }

  getFilterValues(entry){
    //console.log(entry);
    if(entry && entry != ''){
      let localFilterOptions=_.find(this.originalFormFilters,['name',entry]);
      this.FilterOptions=localFilterOptions.options;
      this.FilterOptionsCount = this.FilterOptions.length;
    }else{
      this.FilterOptions = [];
    }
  }

  convertToLookupObject(array, prop){
    let result = {};
    array.forEach(obj => result[obj[prop]] = obj);
    return result;
  }


 //search() {
   // console.log(this.searchObj);
  //  console.log(this.formData);
  //}

  public clearForm() {
    this.formData = _.cloneDeep(this.newFormData());
  }

  filtersAreChanged() {
    let index = 0;
    while(index < this.formData.limits.length && this.formData.limits[index].field === '') {
      index++;
    }
    return index !== this.formData.limits.length;
  }

  clearFilters() {
    for(let limit of this.formData.limits) {
      limit.field = '';
      limit.operator = 'is';
      limit.values = [];
    }
  }

  private isSelectedDb(dbCode: string, onlySelected?: boolean) {
    return onlySelected ? (this.productCodes.length === 1 && this.productCodes.indexOf(dbCode) > -1) : (this.productCodes.indexOf(dbCode) > -1);
  }

  // see PNTHREEX-1244 for logic
  public calculateLimits() {

    this.limitDisplay.openAccess = (_.intersection(['PI', 'PA'], this.productCodes).length > 0);
    this.limitDisplay.impactStatement = this.limitDisplay.openAccess;

    /*
     This is achieved when any of the following conditions is met:
     - PI is selected.
     - If PI is not selected, then:
     * PA selected with either PB or PB
     */
    this.limitDisplay.peerReviewed = this.isSelectedDb('PI') || (this.isSelectedDb('PA') && _.intersection(['PB', 'PC'], this.productCodes).length > 0);

    /*
     This is achieved when any of the following conditions is met:
     - PE is selected.
     - If PE is not selected, the PI + (PA or PB or PC)
     */
    this.limitDisplay.ftOnly = this.isSelectedDb('PE') || (this.isSelectedDb('PI') && (_.intersection(['PA', 'PB', 'PC'], this.productCodes).length > 0));

    this.limitDisplay.testAvailable = this.isSelectedDb('PT');
    this.limitDisplay.book = this.isSelectedDb('PB') && this.productCodes.length === 1;

    // only show latest update when one database is selected.
    this.limitDisplay.latestUpdate = (this.productCodes.length === 1);

  }

  private formatDate(date: Date) {
    let dateString = '' + date.getFullYear();
    dateString += ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
    dateString += (date.getDate() < 10 ? '0' : '') + date.getDate();
    return dateString;
  }

  // the other API Requests for this page will be cached in localStorage, so we don't want to make this log request on its own so that it's not missed
  private logPageView(){
    let query:Query = this._apiService.createQuery('log.logEntry');
    query.logs({ pageId: 'AdvancedSearch' });
    this._apiService.execute(query).subscribe();
  }
}
