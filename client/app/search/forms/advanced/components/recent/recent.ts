import {Component} from '@angular/core';
import {ApiRequestService,Query} from '../../../../../core/services/apiRequestService';

@Component({
  selector: 'adv-recent',
  templateUrl: './recent.html',
  styleUrls: ['./recent.less']
})

export class AdvRecent {

  data:{};
  recentList:Array<any>;

  constructor(private _apiService:ApiRequestService) {
  }

  ngOnInit() {
    console.log('hello recent` component');
    this.getRecent();
  }

   /*Method to get Abstract Content*/
  getRecent() {

    if (!this.recentList) {
      let qry = this._apiService.createQuery('recentSearch.getLatest');
      qry.params({limit: 3});

      this._apiService.execute(qry)
        .subscribe(
          data => {
            let resultList = data;
            console.log(data);
           // let len = parseInt(data.response.result.numFound);

            if (resultList) {
              this.recentList = resultList.slice();
            } else {
              this.recentList = [];
            }
          }
        );
    }

  }




}
