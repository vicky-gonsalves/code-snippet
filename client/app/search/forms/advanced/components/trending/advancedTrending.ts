import {Component, Input} from '@angular/core';
import {SessionStorageService} from 'ng2-webstorage';
import {ApiRequestService,Query} from '../../../../../core/services/apiRequestService';

@Component({
  selector: 'advanced-trending',
  templateUrl: './advancedTrending.html',
  styleUrls: ['./advancedTrending.less']
})

export class AdvancedTrending {
  @Input() productCodes:string[];
  trendingList:string[] = [];

  constructor(private _apiService:ApiRequestService, private _sessionStorageService : SessionStorageService) {
  }

  ngOnInit() {
    this.trendingList = this._sessionStorageService.retrieve('trending') || [];
    if (!this.trendingList.length) {
      this.getTrending();
    }
  }

  setSelectedDBs(){
    this._sessionStorageService.store("apa-trending-DB", this.productCodes);
  }

   /*Method to get Trending Terms*/
  getTrending() {
    let query:Query = this._apiService.createQuery('search.getTrending');

    this._apiService.execute(query)
      .subscribe(
        data => {
          console.log("Trending: ", data);
          if (data && data.length) {
            this.trendingList = data;
            this._sessionStorageService.store("trending", this.trendingList);
          }
          console.log("List: ", this.trendingList);
        },
        error => console.log(error)
      );

  }

}
