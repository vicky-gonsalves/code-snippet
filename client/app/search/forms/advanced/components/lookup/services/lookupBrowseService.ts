import {Injectable} from '@angular/core';
import {ILookupOptions, ILookupParams} from "../lookupInterfaces/ILookupOptions";

@Injectable()
export class LookupBrowseService implements ILookupOptions, ILookupParams {
  private _options:ILookupOptions;

  constructor() {
  }

  getQueryParams(options:ILookupOptions):ILookupParams {
    this._options = options;
    var limit = this._options.selectedLimit;
    var offset = (this._options.selectedPage - 1) * this._options.selectedLimit; //offset starts at 0th index
    let params:ILookupParams;

    let maxPages = Math.ceil(this._options.origTotalItems / this._options.selectedLimit);

    //On first page
    if (this._options.selectedPage == 1) {
      limit += 1;
    }

    //On Middle Pages
    if (this._options.selectedPage > 1 && this._options.selectedPage < maxPages) {
      limit += 2;
      offset -= 1;
    }

    //On last page
    if (this._options.selectedPage == maxPages) {
      limit += 1;
      offset -= 1;
    }
    params = {
      limit: limit,
      offset: offset,
      prefix: this._options.selectAlpha,
      productCodes: this._options.productCodes
    };
    return params;
  }
}
