export interface IItem {
  type:string,
  executeImmediately:boolean,
  terms:string[],
  operator:string
}

export class SearchItems implements IItem {
  type:string = '';
  executeImmediately:boolean = false;
  terms:string[] = [];
  operator:string = '';

  constructor(data?:IItem) {
    if (data) {
      this.type = data.type;
      this.executeImmediately = data.executeImmediately;
      this.terms = data.terms;
      this.operator = data.operator;
    }
  }
}
