import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/share';
import {SearchItems, IItem} from './searchItems';
import * as _ from "lodash";

@Injectable()
export class AddToSearchService {
  private static _items = new BehaviorSubject<SearchItems>(new SearchItems());
  private _tempItems:IItem;

  constructor() {
  }

  public static get items() {
    return AddToSearchService._items.asObservable();
  }

  public setTempItems(previousItems:IItem) {
    let searchItem = AddToSearchService._items.value;
    if (searchItem) {
      this._tempItems = _.cloneDeep(previousItems);
    }
  }

  public resetItemsToTemporary() {
    let searchItem = AddToSearchService._items.value;
    if (searchItem && this._tempItems) {
      searchItem = _.cloneDeep(this._tempItems);
    }
    this.sendOutput(searchItem);
  }

  private sendOutput(searchItem) {
    AddToSearchService._items.next(searchItem);
  }

  public getSelection() {
    return _.cloneDeep(AddToSearchService._items.value);
  }

  public clearSelection(clearedSelection) {
    let searchItem = AddToSearchService._items.value;
    this._tempItems = _.cloneDeep(clearedSelection);
    searchItem = _.cloneDeep(clearedSelection);
  }

  public getTempSelection() {
    return _.cloneDeep(this._tempItems);
  }

  public setType(type:string) {
    let searchItem = AddToSearchService._items.value;
    searchItem.type = type;
    this.sendOutput(searchItem);
  }

  public setOperator(operator:string) {
    let searchItem = AddToSearchService._items.value;
    searchItem.operator = operator;
    this.sendOutput(searchItem);
  }

  public insertItem(item:any, type:string) {
    let searchItem = AddToSearchService._items.value;
    if (searchItem.hasOwnProperty('terms') && searchItem.terms && !this.containsValue(searchItem.terms, item.name)) {
      searchItem.terms.push(item.name);
    }
    this.sendOutput(searchItem);
  }

  public insertImmediateItem(item:any, type:string) {
    let searchItem = AddToSearchService._items.value;
    if (searchItem.hasOwnProperty('terms') && searchItem.terms) {
      searchItem.executeImmediately = true;
      searchItem.type = type;
      searchItem.terms = [item.name];
      searchItem.operator = '';
    }
    this.sendOutput(searchItem);
  }

  public removeItem(item:string) {
    let searchItem = AddToSearchService._items.value;
    if (searchItem.hasOwnProperty('terms') && searchItem.terms && searchItem.terms.length) {
      let index = searchItem.terms.indexOf(item);
      searchItem.terms.splice(index, 1);
      this.sendOutput(searchItem);
    }
  }

  public clearAll() {
    let searchItem = AddToSearchService._items.value;
    searchItem.executeImmediately = false;
    searchItem.operator = '';
    if (searchItem.hasOwnProperty('terms') && searchItem.terms && searchItem.terms.length) {
      searchItem.terms.length = 0;
      this.sendOutput(searchItem);
    }
  }

  public selectAll(list) {
    let searchItem = AddToSearchService._items.value;
    if (list && list.length && searchItem.hasOwnProperty('terms') && searchItem.terms) {
      list.forEach(item=> {
        if (!this.containsValue(searchItem.terms, item.name)) {
          searchItem.terms.push(item.name);
        }
      });
    }
    this.sendOutput(searchItem);
  }

  public unSelectAll(list) {
    let searchItem = AddToSearchService._items.value;
    list.forEach(item=> {
      if (searchItem.hasOwnProperty('terms') && searchItem.terms && searchItem.terms.length) {
        let index = searchItem.terms.indexOf(item.name);
        searchItem.terms.splice(index, 1);
      }
    });
    this.sendOutput(searchItem);
  }

  private containsValue(list, value) {
    return list.indexOf(value) > -1;
  }
}
