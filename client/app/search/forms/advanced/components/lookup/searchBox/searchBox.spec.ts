/*
import {inject, addProviders} from '@angular/core/testing';

// Load the implementations that should be tested
import {SearchBox} from './searchBox';
import {AddToSearchService} from "../services/addToSearchService";

describe('SearchBox', () => {
  var searchBox;

  beforeEach(() => addProviders([
    SearchBox,
    AddToSearchService
  ]));

  beforeEach(inject([SearchBox], (_searchBox) => {
    searchBox = _searchBox;
  }));

  it('should have an AddToSearchService Service', () => {
    expect(!!searchBox._addToSearchService).toEqual(true);
  });

  it('should init on `ngOnInit`', ()=> {
    searchBox.lookupOptions = {label: 'Authors', operators: ['OR']};
    spyOn(searchBox, 'changeOperator');
    searchBox.ngOnInit();
    expect(searchBox.operator).toEqual(searchBox.lookupOptions.operators[0]);
    expect(searchBox.changeOperator).toHaveBeenCalledWith(searchBox.operator);
    searchBox.ngOnDestroy();
  });

  it('should listen to addToSearch event', done => {
    searchBox.addToSearch.subscribe(search => {
      expect(search).toEqual(true);
      done();
    });
    searchBox.addToSearchList();
  });
});
*/
