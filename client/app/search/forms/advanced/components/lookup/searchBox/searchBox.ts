import {Component, Input, Output, EventEmitter} from "@angular/core";
import {AddToSearchService} from "../services/addToSearchService";
import {SearchItems} from "../services/searchItems";


@Component({
  selector: 'search-box',
  templateUrl: './searchBox.html',
  styleUrls: ['./searchBox.less']
})

export class SearchBox {
  @Input() lookupOptions:any;
  @Input() contentLoaded:boolean;
  @Output() addToSearch = new EventEmitter();

  addToSearchServiceSubscription:any;

  items:SearchItems;
  operator:string;

  constructor(private _addToSearchService:AddToSearchService) {
  }

  ngOnInit() {
    if (this.lookupOptions && this.lookupOptions.hasOwnProperty('operators') && this.lookupOptions.operators && this.lookupOptions.operators.length) {
      this.operator = this.lookupOptions.operators[0];
    }
    this.addToSearchServiceSubscription = AddToSearchService.items.subscribe(data => {
      this.items = data;
      if (this.items.operator && this.items.operator.length) {
        this.operator = this.items.operator;
      }
    });
    this.changeOperator(this.operator);
  }

  removeItem(item:string):void {
    this._addToSearchService.removeItem(item);
  }

  clearAll():void {
    this._addToSearchService.clearAll();
  }

  addToSearchList():void {
    this.addToSearch.emit(true);
  }

  changeOperator(operator:string):void {
    this._addToSearchService.setOperator(operator)
  }

  ngOnDestroy() {
    this.addToSearchServiceSubscription.unsubscribe();
  }
}
