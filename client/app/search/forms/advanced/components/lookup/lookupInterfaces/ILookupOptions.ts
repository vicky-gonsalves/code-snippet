/**
 * Interface for Lookup Options
 */
export interface ILookupParams {
  limit?:number,
  offset?:number,
  prefix?:string,
  productCodes?:string[],
}

export interface ILookupOptions {
  selectedLimit?:number,
  selectedPage?:number,
  origTotalItems?:number,
  selectAlpha?:string,
  productCodes?:string[]
}
