/**
 * Interface for Lookup item
 */

export interface ILookupItem {
  name?:string,
  value?:string,
  isSelected?:boolean
}
