export interface IlookupModalOptions {
  author:IOptions,
  journal:IOptions,
  test:IOptions
}

export interface IOptions {
  label:string,
  operators:string[],
  showCount:boolean
}
