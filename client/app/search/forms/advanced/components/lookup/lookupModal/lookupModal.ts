import {Component, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap';
import {AddToSearchService} from "../services/addToSearchService";
import {IlookupModalOptions} from "../lookupInterfaces/IlookupModalOptions";
import {IItem} from "../services/searchItems";

@Component({
  selector: 'lookup-modal',
  templateUrl: './lookupModal.html',
  styleUrls: ['./lookupModal.less']
})

export class LookupModal {
  @Input() productCodes:Array<string>;
  @Output() close = new EventEmitter();
  @ViewChild('childModal') public childModal:ModalDirective;
  previousSelection:IItem;
  selectedItems:any;
  lookupType:string;
  shouldReset:boolean = true;
  options:any;
  modalActivated:boolean = false;

  cleared:IItem = {
    type: '',
    executeImmediately: false,
    terms: [],
    operator: ''
  };


  lookupOptions:IlookupModalOptions = {
    author: {label: 'Authors', operators: ['OR', 'AND'], showCount: true},
    journal: {label: 'Journals', operators: ['OR'], showCount: false},
    test: {label: 'Tests', operators: ['OR', 'AND'], showCount: false}
  };

  constructor(private _addToSearchService:AddToSearchService) {
  }

  afterOpen():void {
    this.setLookupOptions();
    this.cleared.type = this.lookupType;
    this._addToSearchService.setTempItems(this.cleared);
    this._addToSearchService.clearAll();
    if (this.previousSelection && !this.previousSelection.executeImmediately) {
      this._addToSearchService.setTempItems(this.previousSelection);
      this._addToSearchService.resetItemsToTemporary();
    }
    this._addToSearchService.setType(this.lookupType);
  }

  applySelectionAndClose(selectedItems:any):void {
    this.selectedItems = selectedItems;
    this.shouldReset = false;
    this.closeModal();
  }

  openModal(lookupType:string):void {
    this.shouldReset = true;
    this.modalActivated = true;
    this.lookupType = lookupType;
    if (this.childModal) {
      this.childModal.show();
    }
    this.afterOpen();
  }

  openDialogModal(lookupType:string,existingSelection:IItem):void {
    this.shouldReset = true;
    this.modalActivated = true;
    this.lookupType = lookupType;
    if (this.childModal) {
      this.childModal.show();
    }
    this.previousSelection = existingSelection;
    this.afterOpen();
  }
  destroyModal():void {
    if (this.shouldReset) {
      //Reset in-case user already has selection and closed modal
      this.resetData();
    }
    this.lookupType = null;
    this.modalActivated = false;
  }

  resetData():void {
    this._addToSearchService.resetItemsToTemporary();
    this.selectedItems = this._addToSearchService.getTempSelection();
    this.processFinalOutputAndEmit();
  }

  processFinalOutputAndEmit():void {
    this.close.emit(this.selectedItems);
  }

  closeModal():void {
    this.processFinalOutputAndEmit();
    if (this.childModal) {
      this.childModal.hide();
    }
  }

  setSelection():void {
    let selectedItems = this._addToSearchService.getSelection();
    this.clearSelection(); //Clears service variables
    this.applySelectionAndClose(selectedItems);
  }

  clearSelection():void {
    this._addToSearchService.clearSelection(this.cleared);
  }

  setLookupOptions() {
    this.options = {title: '', options: {}};
    switch (this.lookupType) {
      case 'authors':
        this.options.title = 'Author Lookup';
        this.options.options = this.lookupOptions.author || {};
        break;

      case 'journals':
        this.options.title = 'Journal Lookup';
        this.options.options = this.lookupOptions.journal || {};
        break;

      case 'tests':
        this.options.title = 'Test Lookup';
        this.options.options = this.lookupOptions.test || {};
        break;
    }
    return this.options;
  }
}
