/*
import {inject, addProviders} from '@angular/core/testing';

// Load the implementations that should be tested
import {LookupModal} from './lookupModal';
import {AddToSearchService} from "../services/addToSearchService";

describe('LookupModal', () => {
  var lookupModal;

  beforeEach(() => addProviders([
    LookupModal,
    AddToSearchService
  ]));


  beforeEach(inject([LookupModal], (_lookupModal) => {
    lookupModal = _lookupModal;
  }));

  it('should have an AddToSearchService Service', () => {
    expect(!!lookupModal._addToSearchService).toEqual(true);
  });

  it('should have shouldReset true', () => {
    expect(lookupModal.shouldReset).toEqual(true);
  });

  it('should have an cleared', () => {
    let cleared = {
      type: '',
      executeImmediately: false,
      terms: [],
      operator: ''
    };
    expect(lookupModal.cleared).toEqual(cleared);
  });

  it('should have `shouldReset=true` after modal is opened', () => {
    lookupModal.lookupOptions = {
      author: {label: 'Authors', operators: ['AND', 'OR']},
      journal: {label: 'Journals', operators: ['OR']}
    };
    lookupModal.previousSelection = {
      type: 'authors',
      executeImmediately: true
    };
    lookupModal.openModal('journals');
    expect(lookupModal.shouldReset).toEqual(true);
  });

  it('should have `lookupType` after modal is opened', () => {
    let lookupType = 'journals';
    lookupModal.lookupOptions = {
      author: {label: 'Authors', operators: ['AND', 'OR']},
      journal: {label: 'Journals', operators: ['OR']}
    };
    lookupModal.previousSelection = {
      type: 'authors',
      executeImmediately: true
    };
    lookupModal.openModal(lookupType);
    expect(lookupModal.lookupType).toEqual(lookupType);
  });

  it('should call `afterOpen` after modal is opened', () => {
    let lookupType = 'journals';
    lookupModal.lookupOptions = {
      author: {label: 'Authors', operators: ['AND', 'OR']},
      journal: {label: 'Journals', operators: ['OR']}
    };
    lookupModal.previousSelection = {
      type: 'authors',
      executeImmediately: true
    };
    spyOn(lookupModal, 'afterOpen');
    lookupModal.openModal(lookupType);
    expect(lookupModal.afterOpen).toHaveBeenCalled();
  });

  it('should clear after modal is opened and if `executeImmediately=true`', () => {
    lookupModal.previousSelection = {
      type: 'authors',
      executeImmediately: true
    };
    lookupModal.lookupOptions = {
      author: {label: 'Authors', operators: ['AND', 'OR']},
      journal: {label: 'Journals', operators: ['OR']}
    };
    lookupModal.lookupType = 'journals';
    lookupModal.afterOpen();
    let data = lookupModal._addToSearchService.getSelection();
    let tempData = lookupModal._addToSearchService.getTempSelection();
    expect(data.executeImmediately).toEqual(false);
    expect(data.type).toEqual(lookupModal.lookupType);
    expect(lookupModal.cleared.type).toEqual(lookupModal.lookupType);
    expect(tempData).toEqual(lookupModal.cleared);
  });

  it('should set tempSelection after modal is opened and if `executeImmediately=false`', () => {
    lookupModal.previousSelection = {
      type: 'authors',
      executeImmediately: false,
      terms: ['some', 'terms'],
      operator: 'OR'
    };
    lookupModal.lookupOptions = {
      author: {label: 'Authors', operators: ['AND', 'OR']},
      journal: {label: 'Journals', operators: ['OR']}
    };
    lookupModal.lookupType = 'authors';
    lookupModal.afterOpen();
    let tempData = lookupModal._addToSearchService.getTempSelection();
    expect(tempData).toEqual(lookupModal.previousSelection);
  });

  it('should apply Selection and close modal', () => {
    let selectedItems = {
      type: 'authors',
      executeImmediately: false,
      terms: ['some', 'terms'],
      operator: 'OR'
    };
    spyOn(lookupModal, 'closeModal');
    lookupModal.applySelectionAndClose(selectedItems);
    expect(lookupModal.selectedItems).toEqual(selectedItems);
    expect(lookupModal.shouldReset).toEqual(false);
    expect(lookupModal.closeModal).toHaveBeenCalled();
  });

  it('should destroy modal, reset data and set lookupType=null', () => {
    lookupModal.shouldReset = true;
    spyOn(lookupModal, 'resetData');
    lookupModal.destroyModal();
    expect(lookupModal.resetData).toHaveBeenCalled();
    expect(lookupModal.lookupType).toEqual(null);
  });

  it('should reset data to temp selection and call processFinalOutputAndEmit', () => {
    lookupModal.lookupOptions = {
      author: {label: 'Authors', operators: ['AND', 'OR']},
      journal: {label: 'Journals', operators: ['OR']}
    };
    lookupModal.previousSelection = {
      type: 'authors',
      executeImmediately: false,
      terms: ['some', 'terms'],
      operator: 'OR'
    };
    lookupModal.selectedItems = {
      type: 'authors',
      executeImmediately: false,
      terms: [],
      operator: 'OR'
    };
    lookupModal.lookupType = 'authors';
    lookupModal.afterOpen();
    spyOn(lookupModal, 'processFinalOutputAndEmit');
    lookupModal._addToSearchService.setTempItems(lookupModal.previousSelection);
    lookupModal.resetData();
    expect(lookupModal.selectedItems).toEqual(lookupModal.previousSelection);
    expect(lookupModal.processFinalOutputAndEmit).toHaveBeenCalled();
  });

  it('should set selection', () => {
    lookupModal.cleared = {
      type: '',
      executeImmediately: false,
      terms: [],
      operator: ''
    };
    spyOn(lookupModal, 'clearSelection');
    spyOn(lookupModal, 'applySelectionAndClose');
    lookupModal.setSelection();
    expect(lookupModal.clearSelection).toHaveBeenCalled();
    expect(lookupModal.applySelectionAndClose).toHaveBeenCalled();
  });

});
*/
