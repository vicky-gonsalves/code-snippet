/*
import {inject, addProviders, fakeAsync} from '@angular/core/testing';
import {provide} from '@angular/core';
import {MockBackend, MockConnection} from '@angular/http/testing'
import {BaseRequestOptions, Http} from '@angular/http';

// Load the implementations that should be tested
import {Lookup} from './lookup';
import {ApiRequestService} from "../../../../../common/services/apiRequestService";
import {Capitalize} from "../../../../../common/services/capitalize/capitalize";
import {LookupBrowseService} from "./services/lookupBrowseService";
import {AddToSearchService} from "./services/addToSearchService";

describe('Lookup', () => {
  let lookup;

  beforeEach(() => addProviders([
    BaseRequestOptions,
    MockBackend,
    provide(Http, {
      useFactory: function (backend, defaultOptions) {
        return new Http(backend, defaultOptions);
      },
      deps: [MockBackend, BaseRequestOptions]
    }),
    Lookup,
    ApiRequestService,
    Capitalize,
    LookupBrowseService,
    AddToSearchService
  ]));


  beforeEach(inject([Lookup], (_lookup) => {
    lookup = _lookup;
  }));

  it('should have an AddToSearchService Service', () => {
    expect(!!lookup._addToSearchService).toEqual(true);
  });

  it('should have an ApiRequestService Service', () => {
    expect(!!lookup._apiService).toEqual(true);
  });

  it('should have an Capitalize Service', () => {
    expect(!!lookup._capitalizeService).toEqual(true);
  });

  it('should have an LookupBrowseService Service', () => {
    expect(!!lookup._browseService).toEqual(true);
  });

  it('should not have selectAlpha', () => {
    expect(lookup.selectAlpha).toEqual('');
  });

  it('should not have filterString', () => {
    expect(lookup.filterString).toEqual('');
  });

  it('should have selectedLimit 10', () => {
    expect(lookup.selectedLimit).toEqual(10);
  });

  it('should have selectedPage 1', () => {
    expect(lookup.selectedPage).toEqual(1);
  });

  it('should not have prependText', () => {
    expect(lookup.prependText).toEqual('');
  });

  it('should not have appendText', () => {
    expect(lookup.appendText).toEqual('');
  });

  it('should have bottomPaginationThreshold', () => {
    expect(lookup.bottomPaginationThreshold).toEqual(10);
  });

  it('should not have isBottomPaginationBarVisible', () => {
    expect(lookup.isBottomPaginationBarVisible).toEqual(false);
  });

  it('should not have isSelectAll', () => {
    expect(lookup.isSelectAll).toEqual(false);
  });

  it('should have disabledFormControls', () => {
    expect(lookup.disabledFormControls).toEqual(true);
  });

  it('should not have contentLoaded', () => {
    expect(lookup.contentLoaded).toEqual(false);
  });

  it('should emit selectItems event', done => {
    let value = true;
    lookup.selectItems.subscribe(item => {
      expect(item).toEqual(value);
      done();
    });
    lookup.addToSearch();
  });

  it('should emit executeImmediately event', done => {
    let value = true;
    lookup.selectItems.subscribe(item => {
      expect(item).toEqual(value);
      done();
    });
    lookup.executeImmediately({name: 'Some Journal', value: '122'});
  });

  it('should fetch Authors list', inject([ApiRequestService, MockBackend], fakeAsync((service:ApiRequestService) => {
      let query = service.createQuery('authorLookup.getAuthorLookup');
      let queryParams = {
        limit: 20,
        offset: 0,
        prefix: '',
        productCodes: ['PA']
      };
      query.params(queryParams);
      service.execute(query)
        .subscribe(data => {
          expect(data.response).toBeDefined();
        });
    })
  ));

  it('should fetch Journals list', inject([ApiRequestService, MockBackend], fakeAsync((service:ApiRequestService) => {
      let query = service.createQuery('journalLookup.getJournalLookup');
      let queryParams = {
        limit: 20,
        offset: 0,
        prefix: '',
        productCodes: ['PA']
      };
      query.params(queryParams);
      service.execute(query)
        .subscribe(data => {
          expect(data.response).toBeDefined();
        });
    })
  ));

  it('should init on ngOnInit', () => {
    spyOn(lookup, 'initLookup');
    lookup.ngOnInit();
    expect(lookup.initLookup).toHaveBeenCalled();
  });

});
*/
