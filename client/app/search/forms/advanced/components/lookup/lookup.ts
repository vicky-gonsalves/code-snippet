import {Component, Input, Output, EventEmitter} from "@angular/core";
import {ApiRequestService, Query} from '../../../../../core/services/apiRequestService';
import {LookupBrowseService} from "./services/lookupBrowseService";
import {ILookupItem} from "./lookupInterfaces/lLookupItem";
import {ILookupParams} from "./lookupInterfaces/ILookupOptions";
import {Capitalize} from "../../../../../shared/capitalize/capitalize";
import {AddToSearchService} from "./services/addToSearchService";
import * as _ from "lodash";

@Component({
  selector: 'lookup',
  templateUrl: './lookup.html',
  styleUrls: ['./lookup.less']
})

export class Lookup {
  @Input() lookupOptions:any;
  @Input() lookupFor:string;
  @Input() productCodes:Array<string>;
  @Output() selectItems = new EventEmitter();

  addToSearchServiceSubscription:any;

  selectedItems:string[];
  itemList:ILookupItem[];
  selectAlpha = '';
  filterString = '';
  selectedLimit = 15;
  selectedPage = 1;
  totalItems:number;
  origTotalItems:number;
  showPaginationPages = false;
  prependText = '';
  appendText = '';
  isSelectAll:boolean = false;
  disabledFormControls:boolean = true;
  loading:boolean = true;

  private pageIdMapping = {
    authors: {
      default: 'AuthLookup',
      az: 'AuthLookup_a-z',
      jumpto: 'AuthLookup_jump-to'
    },
    journals: {
      default: 'JnlLookup',
      az: 'JnlLookup_a-z',
      jumpto: 'JnlLookup_jump-to'
    },
    tests: {
      default: 'TestLookup',
      az: 'TestLookup_a-z',
      jumpto: 'TestLookup_jump-to'
    }
  };

  constructor(private _apiService:ApiRequestService, private _capitalizeService:Capitalize, private _browseService:LookupBrowseService, private _addToSearchService:AddToSearchService) {
  }

  ngOnInit() {
    this.initLookup();
    this.addToSearchServiceSubscription = AddToSearchService.items.subscribe(data=> {
      if (data.hasOwnProperty('type') && data.type && data.type == this.lookupFor && data.hasOwnProperty('terms') && data.terms) {
        this.selectedItems = data.terms.slice();
        if (this.itemList && this.itemList.length) {
          this.syncCheckUncheck();
        }
      }
    });
  }

  syncCheckUncheck() {
    let selectedCountForPage = 0;
    //Get only checked items
    let result = _.flatten(_.map(this.selectedItems, (selectedItem:string)=> {
      return _.filter(this.itemList, function(item) {
        return item.name.trim() === selectedItem.trim();
      });
    })).map(searchItem=>searchItem.name);

    //sync checked/unchecked Items
    this.itemList.forEach((item, index)=> {
      let res = _.find(result, val=> _.isEqual(item.name.trim(), val.trim()));
      if (res && res.length) {
        this.itemList[index].isSelected = true;
        ++selectedCountForPage;
      } else {
        this.itemList[index].isSelected = false;
      }
    });

    //Check/uncheck selectall based on results
    this.isSelectAll = this.itemList.length > 0 && (this.itemList.length == selectedCountForPage);
  }

  addToSearch() {
    this.selectItems.emit(true);
  }

  toggleSelection(item:ILookupItem) {
    if (item.isSelected) {
      this._addToSearchService.insertItem(item, this.lookupFor);
    } else {
      this._addToSearchService.removeItem(item.name);
    }
  }

  toggleAllSelection(selectAll:boolean) {
    if (selectAll) {
      this._addToSearchService.selectAll(this.itemList);
    } else {
      this._addToSearchService.unSelectAll(this.itemList);
    }
  }

  executeImmediately(item:ILookupItem) {
    this._addToSearchService.insertImmediateItem(item, this.lookupFor);
    this.selectItems.emit(true);
  }

  /*Returns query params*/
  getQueryParams():ILookupParams {
    let options = {
      selectedLimit: this.selectedLimit,
      selectedPage: this.selectedPage,
      origTotalItems: this.origTotalItems,
      selectAlpha: this.selectAlpha == '' ? this.getInitialFilter() : this.selectAlpha,
      productCodes: this.productCodes.length ? this.productCodes : ['PA']
    };
    return this._browseService.getQueryParams(options);
  }

  getInitialFilter(){
    let initialFilter = (this.lookupFor === 'tests') ? '*' : 'a';

    return initialFilter;
  }

  getRequestType() {
    let requestType = '';
    switch (this.lookupFor) {
      case 'authors':
        requestType = 'authorLookup.getAuthorLookup';
        break;
      case 'journals':
        requestType = 'journalLookup.getJournalLookup';
        break;
      case 'tests':
        requestType = 'testLookup.getTestsByNameLookup';
        break;
    }
    return requestType;
  }

  /*Common method to get lookup items*/
  getLookupItems(queryParams:ILookupParams) {
    let query:Query = this._apiService.createQuery(this.getRequestType());
    this.showPaginationPages = false;
    this.loading = true;
    query.params(queryParams);
    query.logs({
      pageId: this.getCurrentPageId()
    });
    this._apiService.execute(query)
      .subscribe(
        data => {
          let jList = data.response.facet.values;
          let len = parseInt(data.response.result.numFound);
          this.origTotalItems = len;
          this.totalItems = len;
          if (jList && len > 0) {
            if (len > this.selectedLimit) {
              let maxPages = Math.ceil(this.origTotalItems / this.selectedLimit);
              //On Page 1
              if (this.selectedPage == 1) {
                var nextItem = jList.pop();
                if (nextItem.name && nextItem.name.length > 0) {
                  this.appendText = this.lookupFor === 'journals'? this._capitalizeService.getCapitalized(nextItem.name) : nextItem.name;
                }
              }

              //On page > 1 && page < totalPages
              if (this.selectedPage > 1 && this.selectedPage < maxPages) {
                var prevItem = jList.shift();
                var nextItem = jList.pop();
                if (prevItem.name && prevItem.name.length > 0) {
                  this.prependText = this.lookupFor === 'journals' ? this._capitalizeService.getCapitalized(prevItem.name) : prevItem.name;
                }
                if (nextItem.name && nextItem.name.length > 0) {
                  this.appendText =  this.lookupFor === 'journals' ? this._capitalizeService.getCapitalized(nextItem.name) : nextItem.name;
                }
              }

              //On last page
              if (this.selectedPage == maxPages) {
                var prevItem = jList.shift();
                if (prevItem.name && prevItem.name.length > 0) {
                  this.prependText =  this.lookupFor === 'journals' ? this._capitalizeService.getCapitalized(prevItem.name) : prevItem.name;
                }
              }
            }

            this.itemList = jList.map(item=> {
              if (item.hasOwnProperty('name') && item.name && this.lookupFor === 'journals') {
                return {name: this._capitalizeService.getCapitalized(item.name), value: item.value};
              }
              return item;
            });
          } else {
            this.itemList = [];
          }
          this.disabledFormControls = (this.itemList.length < 1);
          this.syncCheckUncheck();
          this.loading = false;
        },
        error => console.log(error),
        () => console.log('Response From getLookupItems Complete')
      );
  }

  /*This function triggers once alphabet navigation is clicked*/
  getNewAlpha(evt) {
    if (evt && evt.length) {
      this.selectAlpha = evt;
    } else {
      this.selectAlpha = '';
    }
    this.selectedPage = 1;
    this.filterString = '';
    let queryParams = this.getQueryParams();
    this.getLookupItems(queryParams);
  }

  /*Pagination method*/
  getNewPagination(pagination) {
    if (pagination.hasOwnProperty('limit') && pagination.hasOwnProperty('currentPage')) {
      this.selectedLimit = pagination.limit;
      this.selectedPage = pagination.currentPage;
      this.applyPagination();
    }
    if (pagination.hasOwnProperty('currentPage') && !pagination.hasOwnProperty('limit')) {
      this.selectedPage = pagination.currentPage;
      if (pagination.hasOwnProperty('ev') && pagination.ev) {
        this.applyPagination();
      }
    }
  }

  /*Apply Pagination*/
  applyPagination() {
    let queryParams = this.getQueryParams();
    this.getLookupItems(queryParams);
  }

  /*Fetch Lookups*/
  fetchLookups(queryParams:any) {
    this.getLookupItems(queryParams);
  }

  initLookup() {
    let queryParams = this.getQueryParams();
    this.fetchLookups(queryParams);
  }

  // this calculates the pageId based on the length of the this.selectAlpha variable
  private getCurrentPageId(){
    let pageId = '';

    if(this.selectAlpha.length === 1){
      pageId = this.pageIdMapping[this.lookupFor].az;
    }else if( this.selectAlpha.length > 1 ){
      pageId = this.pageIdMapping[this.lookupFor].jumpto;
    } else {
      pageId = this.pageIdMapping[this.lookupFor].default;
    }

    return pageId;
  }

  ngOnDestroy() {
    this.addToSearchServiceSubscription.unsubscribe();
  }
}
