import {Component} from "@angular/core";
import {ActivatedRoute, Router} from '@angular/router';
import {SessionStorageService} from 'ng2-webstorage';
import {ApiRequestService, Query} from '../../../core/services/apiRequestService';
import {IdentityService} from '../../../core/services/identity/identityService';
import {PermissionService} from '../../../core/services/identity/permissionService';
import {IIdentity} from '../../../core/services/identity/identity';
import {DatabaseFilterSubscription} from '../../../core/services/databaseSubscription/databaseFilterSubscription';
import {Search} from "../../services/search/search";
import {SearchCollection} from "../../services/search/searchCollection";
import {PsycNETQuery} from "../../services/search/query/psycNETQuery";
import {IResponseParameters} from "../../services/search/query/components/interfaces";
import {Criteria} from "../../services/search/query/components/criteria";
import {SearchNavigation} from "../../services/searchNavigation";
import {LocationService} from '../../../core/services/locationService';
import {Observable} from "rxjs";
import * as _ from "lodash";

@Component({
  selector: 'basic',
  templateUrl: './basic.html',
  styleUrls: ['./basic.less']
})


export class SearchBasic {
  public typeaheadSuggestions: string[] = [];
  public hasPTAccess: boolean = false;
  productCodes: string[] = [];
  private _identity: IIdentity;

  // form model data
  public formData: IBasicFormData;

  // search id
  searchId: string;

  // limit field showing
  limitDisplay = {
    peerReviewed: false,
    ftOnly: false,
    testAvailable: false,
    latestUpdate: false
  };

  constructor(private _apiService: ApiRequestService,
              private _route: ActivatedRoute,
              private _locationService : LocationService,
              private searchNavigation: SearchNavigation,
              private _permissionService: PermissionService,
              private _sessionStorageService: SessionStorageService) {

    // configure observable typeahead listener
    this.typeaheadSuggestions = Observable.create((observer: any) => {

      let typeaheadQuery = this._apiService.createQuery('typeahead.getTypeahead');
      typeaheadQuery.params({
        term: this.formData.text
      });

      this._apiService.execute(typeaheadQuery).subscribe(
        data => {
          if (data.terms.term) {
            observer.next(data.terms.term);
          }
        },
        error => console.log(error)
      );

    });
  }

  ngOnInit() {

    // get a basic form model state
    this.formData = this.newFormData();

    IdentityService.identity.subscribe(data => {
      // ensure identity is loaded
      if(!data){
        return false;
      }

      this._route.queryParams.subscribe(params => {
        this._identity = data;
        this.hasPTAccess = this._permissionService.mayAccess('PT');

        this.searchId = params['id'];

        // determine if we need to populate the form model with a recent search (Edit search)local
        this.populateFormData();

        // log the search form view
        this.logPageView();
      });

    });
  }

  ngOnDestroy(){
    this._saveSearchFormState();
  }

  private _saveSearchFormState(){
    let pnQuery = this.formToQuery();
    let responseParams: IResponseParameters = {
      start: 0,
      rows: 25,
      facet: true,
      results: true
    };

    // add this search to our collection and store the generated search id
    let search = new Search(pnQuery, responseParams);
    SearchCollection.add(search);

    console.log("saving search form state: ", search.id);

    this._sessionStorageService.store('searchFormId', search.id);
  }

  public newSelectedDatabases(event) {
    console.log(event);
    //this.productCodes = event;
    this.productCodes= _.without(event,'PH');
    console.info("Basic: prodcut code 2: ", this.productCodes);

    // update limit checkboxes
    this.calculateLimits();
  }

  public calculateLimits() {
    console.info("Basic: prodcut : calculateLimits");
    /*
     This is achieved when any of the following conditions is met:
     - PI is selected.
     - If PI is not selected, then:
     * PA selected with either PB or PB
     */
    this.limitDisplay.peerReviewed = this.isSelectedDb('PI') || (this.isSelectedDb('PA') && _.intersection(['PB', 'PC'], this.productCodes).length > 0);

    /*
     This is achieved when any of the following conditions is met:
     - PE is selected.
     - If PE is not selected, the PI + (PA or PB or PC)
     */
    this.limitDisplay.ftOnly = this.isSelectedDb('PE') || (this.isSelectedDb('PI') && (_.intersection(['PA', 'PB', 'PC'], this.productCodes).length > 0));

    this.limitDisplay.testAvailable = this.hasPTAccess && this.isSelectedDb('PT');

    // show latest update when only one database is selected.
    this.limitDisplay.latestUpdate = (this.productCodes.length == 1);
  }

  public keyDownFunction(event) {
    if (event.keyCode == 13) {
      this.executeSearch();
    }
  }

  public newFormData(): IBasicFormData {
    return {
      text: '',
      filter: {
        peerReviewed: false,
        ftOnly: false,
        testAvailable: false,
        latestUpdate: false,
        openAccess: false,
        impactStatement: false
      }
    };
  }

  // perform Basic Search
  public executeSearch() {
    let pnQuery = this.formToQuery();
    let responseParams: IResponseParameters = {
      start: 0,
      rows: 25,
      facet: false,
      results: true
    };

    let search = new Search(pnQuery, responseParams);

    // add this search to our collection
    SearchCollection.add(search);

    //clearing slected record checkbox values in result page
    this._sessionStorageService.clear("selectedSearchRecords");
    this._sessionStorageService.clear('currentPageSearchRecords');

    // allow back button on search results page to populate search
    // this._location.replaceState('/search/basic?id='+search.id);

    let params = [{
      name: 'id',
      value: search.id
    }];
    this._locationService.updateLocation('/search/basic', params);

    // setting queryParams to an empty object will remove existing query params from URL
    this.searchNavigation.gotoResults(search, {queryParams: {id: search.id}});
  }

  // convert the basic search form model to a PsycNETQuery
  private formToQuery(): PsycNETQuery {
    let pnQuery = new PsycNETQuery();
    pnQuery.criteria.push(new Criteria('AnyField', 'OR', this.formData.text, 'user'));
    pnQuery.filter.onlyShow = {
      ftOnly: this.limitDisplay.ftOnly && this.formData.filter.ftOnly,
      impactStatement: false,
      latestUpdate: this.limitDisplay.latestUpdate && this.formData.filter.latestUpdate,
      openAccess: false,
      peerReviewed: this.limitDisplay.peerReviewed && this.formData.filter.peerReviewed,
      testAvailable: this.limitDisplay.testAvailable && this.formData.filter.testAvailable
    };
    pnQuery.databases = this.productCodes;
    pnQuery.source = 'easy';

    // if the user has PsycTHERAPY add it back in
    if(this._permissionService.mayAccess('PH')){
      pnQuery.databases.push('PH');
    }

    console.info("Basic: prodcut code 3: ", this.productCodes);
    return pnQuery;
  }

  // convert the PsycNETQuery data to basic search form model
  private queryToForm(query: PsycNETQuery): IBasicFormData {
    let data = this.newFormData();

    // populate text input
    if (query.criteria.length > 0) {
      data.text = query.criteria[0].value;
    }

    // populate limits
    data.filter = query.filter.onlyShow;

    return data;
  }

  private populateFormData() {

    // if there is no 'id' url parameter, then this is a new search
    if (!this.searchId) {
      DatabaseFilterSubscription.resetSelectedDBChanges();
      return false;
    }

    // try getting it from our SearchCollection first before querying our recent searches
    let localRecentSearch = SearchCollection.get(this.searchId);

    if (localRecentSearch) {
      this.formData = this.queryToForm(localRecentSearch.query);
      this.productCodes = localRecentSearch.query.databases;
      DatabaseFilterSubscription.updateSelectedDBChanges(localRecentSearch.query.databases);
      console.info("Basic: prodcut code 4: ", this.productCodes);
    } else {

      // get the recent search data and populate form
      let qry = this._apiService.createQuery('recentSearch.get');
      qry.params({id: this.searchId});

      this._apiService.execute(qry).subscribe(
        data => {

          if (data.hasOwnProperty('query')) {
            this.formData = this.queryToForm(data.query);
            this.productCodes = data.query.databases;
            DatabaseFilterSubscription.updateSelectedDBChanges(data.query.databases);
            console.info("Basic: prodcut code 5: ", this.productCodes);
          }
        });
    }

  }

  //get trending text and initiate Search
  private onTrendingListClose(selectedTrending): void {
    console.log('selected Trending: ', selectedTrending);
    this.formData.text = selectedTrending;
    this.executeSearch();
  }

  private isSelectedDb(dbCode: string, onlySelected?: boolean) {
    return onlySelected ? (this.productCodes.length === 1 && this.productCodes.indexOf(dbCode) > -1) : (this.productCodes.indexOf(dbCode) > -1);
  }

  // the other API Requests for this page will be cached in localStorage, so we don't want to make this log request on its own so that it's not missed
  private logPageView(){
    let query:Query = this._apiService.createQuery('log.logEntry');
    query.logs({ pageId: 'BasicSearch' });
    this._apiService.execute(query).subscribe();
  }
}


interface IBasicFormData {
  text: string;
  filter: {
    peerReviewed: boolean;
    ftOnly: boolean;
    testAvailable: boolean;
    latestUpdate: boolean;
    openAccess: boolean;
    impactStatement: boolean;
  }
}


