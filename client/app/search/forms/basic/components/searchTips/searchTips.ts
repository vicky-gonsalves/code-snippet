import {Component, ViewChild} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap';

@Component({
  selector: 'search-tips',
  templateUrl: './searchTips.html'
})

export class SearchTips {
  @ViewChild('childModal') public childModal: ModalDirective;


  constructor() {}

  ngOnInit() {
  }


  openModal():void {
    this.childModal.show();
  }

  closeModal():void {
    this.childModal.hide();
  }
}
