import {Component, Output, EventEmitter} from '@angular/core';
import {ApiRequestService, Query} from '../../../../../core/services/apiRequestService';
import {SessionStorageService} from "ng2-webstorage";

@Component({
  selector: 'basic-trending',
  templateUrl: './basicTrending.html',
  styleUrls: ['./basicTrending.less']
})

export class BasicTrending {
  @Output() close = new EventEmitter();
  trendingList: string[] = [];

  constructor(private _apiService:ApiRequestService, private _sessionStorageService : SessionStorageService) {
  }

  ngOnInit() {
    this.trendingList = this._sessionStorageService.retrieve('trending') || [];
    if (!this.trendingList.length) {
      this.getTrending();
    }
  }

  setSearchText(selectedTrending: string) {
    this.close.emit(selectedTrending);
  }

  getTrending() {
    let query: Query = this._apiService.createQuery('search.getTrending');

    this._apiService.execute(query)
      .subscribe(
        data => {
          if (data && data.length) {
            this.trendingList = data;
            this._sessionStorageService.store("trending", this.trendingList);
          }
        },
        error => console.log(error),
        () => {
          console.log('Response From search.getTrending Complete');
        }
      );
  }
}
