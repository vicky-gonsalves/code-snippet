import {Component, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap';

@Component({
  selector: 'invalid-search-dialog',
  templateUrl: './invalidSearchDialog.html',
})

export class InvalidSearchDialog {
  @ViewChild('childModal') public childModal: ModalDirective;



  constructor() {}

  ngOnInit() {
  }


  openModal():void {
    this.childModal.show();
  }

  closeModal():void {
    this.childModal.hide();
  }
}
