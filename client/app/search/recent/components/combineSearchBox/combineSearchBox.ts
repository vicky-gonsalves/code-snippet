import {Component, Input, Output,EventEmitter } from '@angular/core';
import {Locale, LocalizationService} from "angular2localization";

@Component({
  selector : 'combine-search-box',
  templateUrl: './combineSearchBox.html',
  styleUrls: ['./combineSearchBox.less']
})
export class CombineSearchBox extends Locale{
  @Input() selectedSearches : Array<any>;
  @Output() update = new EventEmitter();
  @Output() runSearch = new EventEmitter();

  operator: string;
  lookupOperators : Array<string>;

  constructor(public localization: LocalizationService){
    super(null, localization);
    this.operator= "AND";
    this.lookupOperators = ['AND', 'OR', 'NOT'];
  }

  ngOnInit(){

  }

  performSearch() {
    var newSearch = {
      operator : this.operator,
      searches : this.selectedSearches
    };
    this.runSearch.emit(newSearch);

  }

  removeSelectedSearchItem(count){
    this.selectedSearches.splice(count, 1);
    this.update.emit(this.selectedSearches);
  }


  clearAll(){
    this.selectedSearches = [];
    this.update.emit(this.selectedSearches);
  }
}
