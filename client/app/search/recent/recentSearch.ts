import {Component, ViewChild} from "@angular/core";
import {Router, RouterStateSnapshot} from '@angular/router';
import {ApiRequestService, Query} from '../../core/services/apiRequestService';
import {Search} from "../services/search/search";
import {SearchCollection} from "../services/search/searchCollection";
import {PsycNETQuery} from "../services/search/query/psycNETQuery";
import {IResponseParameters} from "../services/search/query/components/interfaces";
import {Criteria} from "../services/search/query/components/criteria";
import {SearchNavigation} from "../services/searchNavigation";
import {InvalidSearchDialog} from "./components/invalidSearchDialog/invalidSearchDialog";
import {SaveSearchModal} from "../results/components/saveSearchModal/saveSearchModal";

import * as _ from "lodash"
import {Locale, LocalizationService} from "angular2localization";
import {AuthService} from "../../core/services/authService";
import {NavigationEvent} from "../../core/services/navigationEvent";

@Component({
  selector: 'recent-search',
  templateUrl: './recentSearch.html',
  styleUrls: ['./recentSearch.less']
})
export class RecentSearch extends Locale {
  @ViewChild('invalidSearchDialogChild') public invalidSearchDialogChild: InvalidSearchDialog;
  @ViewChild('saveSearchDialog') public saveSearchDialog : SaveSearchModal;
  selectedSearchItems: Array<any>;
  recentSearches: Array <any>;
  isSelectAll:boolean;
  loading :boolean;
  private _snapshot: RouterStateSnapshot;
  private showPermalink = [];

  dbMap = {
    "PI" : "PsycINFO",
    "PA" : "PsycARTICLES",
    "PB" : "PsycBOOKS",
    "PC" : "PsycCRITIQUES",
    "PE" : "PsycEXTRA",
    "PT" : "PsycTESTS",
    "PH" : "PsycTHERAPY"
  };

  constructor(private _apiService :ApiRequestService,
              private _router:Router,
              private searchNavigation: SearchNavigation,
              public localization: LocalizationService,
              private _navigationEvent: NavigationEvent,
              private _authService: AuthService) {
    super(null, localization);
    this._snapshot = _router.routerState.snapshot;
  }

  ngOnInit() {
    this.selectedSearchItems = [];
    console.log('hello `RecentSearch` component');
    this.getRecentSearches();
  }

  ngAfterViewChecked() {
    //block handles saves search after login
    if (this._authService.isAuthenticated() && this._navigationEvent.hasAction()) {
      let article = this._navigationEvent.get();
      if (article.event == 'saveSearch' && article.args.hasOwnProperty('search') && article.args.search) {
        this.saveSearchDialog.openModal(article.args.search);
        this._navigationEvent.clear();
      }
    }
  }

  private getRecentSearches(){
    this.loading = true;
    let qry : Query = this._apiService.createQuery('recentSearch.list');

    this._apiService.execute(qry).subscribe( data => {
      this.recentSearches = data;
      this.loading = false;
    });
  }

  saveRecentSearch(search) {
    if (this._authService.isAuthenticated()) {
      this.saveSearchDialog.openModal(search);
    } else {
      this._navigationEvent.set('saveSearch', {'search': search});
      this._authService.redirectToLogin(this._snapshot.url);
    }
  }

  setEmailAlert(search){
    let searchId = search.id;

    let alert = {
      searchId : searchId,
      frequency : 7,
      expiresAfter : 365,
    };

    let query:Query = this._apiService.createQuery('myPsycNet.setSearchEmailAlert');
    query.params(search);
    query.params({emailAlert: alert});
    this._apiService.execute(query).subscribe(data => {
      if(data.isEmailAlertTriggered){
        this._router.navigate(['/MyPsycNET/alerts'], {queryParams: {tab: '0', searchId: searchId}});
      }
    }, error => console.log(error));
  }

  deleteRecentSearch(id){
    let qry : Query = this._apiService.createQuery('recentSearch.delete');
    qry.params({id: id});

    this._apiService.execute(qry).subscribe( data => {
      this.recentSearches = data.searches;

      if(this.selectedSearchItems.length){
        var tempList = _.map(this.selectedSearchItems, "id");
        var index = tempList.indexOf(data.removed);
        if(index > -1)
          this.selectedSearchItems.splice(index, 1);

        tempList = _.map(this.selectedSearchItems, "id");
        for (var i = 0; i < this.recentSearches.length; i++) {
          if(tempList.indexOf(this.recentSearches[i].id) > -1){
            this.recentSearches[i].isSelected = true;
          }
        }
      }
      this.setSelectAll();
    });
  }

  private validateSearchTypeandSearch(selectedSearches){
    let searchnotAllowed = true;
    let searchType = selectedSearches.searches[0].type;
    for(let i=1 ; i < selectedSearches.searches.length ; i++ ){
      if(searchType != selectedSearches.searches[i].type && (searchType =='citedRefs' ||  selectedSearches.searches[i].type =='citedRefs' )) {
        searchnotAllowed= false;
      }
    }
    if(!searchnotAllowed)
      this.invalidSearchDialogChild.openModal();
    else
      this.combineSearch(selectedSearches)

  }

  combineSearch(selectedSearches) {
    console.log('Selected Searches: ', selectedSearches);

    // make sure something is getting combined
    if(selectedSearches.length < 2){
      return false;
    }


    let pnQuery = new PsycNETQuery();
    pnQuery.displayOptions.pageSize = this.getPageSizeForCombine(selectedSearches);
    pnQuery.displayOptions.sort = this.getSortForCombine(selectedSearches) || pnQuery.displayOptions.sort;

    let databases: string[] = selectedSearches.searches[0].query.databases;
    let responseParams: IResponseParameters = {
      start: 0,
      rows: pnQuery.displayOptions.pageSize,
      facet: true,
      results: true
    };

    for(let i=0; i<selectedSearches.searches.length; i++){
      let combineSearch = selectedSearches.searches[i];

      let criteriaString = this.combineQueryAndFilterStrings(combineSearch);
      let skipFilters = (selectedSearches.operator === 'NOT' && i > 0);

      // The logic below will use the current value within 'pnQuery' if it's true.  If it's false, then it'll use the value within the 'combineSearch' variable.
      if(!skipFilters){
        pnQuery.filter.onlyShow.ftOnly = pnQuery.filter.onlyShow.ftOnly || combineSearch.query.filter.onlyShow.ftOnly;
        pnQuery.filter.onlyShow.peerReviewed = pnQuery.filter.onlyShow.peerReviewed || combineSearch.query.filter.onlyShow.peerReviewed;
        pnQuery.filter.onlyShow.testAvailable = pnQuery.filter.onlyShow.testAvailable || combineSearch.query.filter.onlyShow.testAvailable;
        pnQuery.filter.onlyShow.openAccess = pnQuery.filter.onlyShow.openAccess || combineSearch.query.filter.onlyShow.openAccess;
        pnQuery.filter.onlyShow.impactStatement = pnQuery.filter.onlyShow.impactStatement || combineSearch.query.filter.onlyShow.impactStatement;
        pnQuery.filter.onlyShow.latestUpdate = pnQuery.filter.onlyShow.latestUpdate || combineSearch.query.filter.onlyShow.latestUpdate;
      }

      // create the criteria
      pnQuery.criteria.push( new Criteria('AnyField', selectedSearches.operator, criteriaString, 'user') );

      // merge selected databases
      databases = _.union(databases, combineSearch.query.databases);
    }

    pnQuery.databases = databases;
    pnQuery.source = (selectedSearches.searches[0].type === 'citedRefs') ? 'citedRefs' : 'advanced';

    //console.log('Combine: ', pnQuery);

    // create the search and show results
    let search = new Search(pnQuery, responseParams);
    SearchCollection.add(search);

    this.searchNavigation.gotoResults(search, {queryParams: {id: search.id}});
  }

  private getPageSizeForCombine(selectedSearches){
    let defaultPageSize = 25;

    // use the first search page size but if the page sizes in all searches don't match, then use the default
    let pageSize = selectedSearches.searches[0].query.displayOptions.pageSize;

    for(let i=1 ; i < selectedSearches.searches.length ; i++ ){
      if(selectedSearches.searches[i].query.displayOptions.pageSize != pageSize) {
        pageSize = defaultPageSize;
        break;
      }
    }

    return pageSize;
  }

  private getSortForCombine(selectedSearches){

    // use the first search sort but if the page sizes in all searches don't match, then use the default
    let sort = selectedSearches.searches[0].query.displayOptions.sort;

    for(let i=1 ; i < selectedSearches.searches.length ; i++ ){
      if(selectedSearches.searches[i].query.displayOptions.sort != sort) {
        sort = null;
        break;
      }
    }

    return sort;
  }

  // this will remove limits from the filter string and combine then
  private combineQueryAndFilterStrings(search){
    let criteriaString = '';
    let queryString = search.queryParameters.q.trim();
    let filterString = search.queryParameters.fq;
    let tokens = [];
    let filterSyntaxRegEx = {
      ftOnly: 'AND HasFullText:true',
      peerReviewed: 'PublicationTypeFilt:"Peer Reviewed Journal"',
      testAvailable: 'AND \\(TestItemsAvailable:"Yes"\\)',
      openAccess: 'AND \\(OpenAccess:\\(Yes\\)\\)',
      impactStatement: 'AND \\(AbstractType:\\("Impact Statement"\\)\\)'
    };

    // remove the filter syntax from selected filters
    for (let filter in search.query.filter.onlyShow) {
      if (search.query.filter.onlyShow.hasOwnProperty(filter) && search.query.filter.onlyShow[filter]) {
        let regEx = new RegExp(filterSyntaxRegEx[filter], 'gi');
        filterString = filterString.replace(regEx, '');
      }
    }

    filterString = filterString.trim();

    if(queryString.length){
      tokens.push(queryString);
    }

    if(filterString.length){
      tokens.push(filterString);
    }

    if(tokens.length){
      criteriaString = tokens.join(' AND ');
      criteriaString = '(' + criteriaString + ')';
    }

    return criteriaString;
  }

  performNewSearch(newSearch) {

    if (newSearch.searches.length == 1) {
      this.markSearchToBeLogged(newSearch.searches[0]);
      if(newSearch.searches[0].type === 'citedRefs')
        this._router.navigate(['/search/citedRefsResults'], {queryParams: {id: newSearch.searches[0].id}});
      else
        this._router.navigate(['/search/results'], {queryParams: {id: newSearch.searches[0].id}});
    } else {
      this.validateSearchTypeandSearch(newSearch)

    }
  }
  editRecentSearch(_search){
    this.searchNavigation.editSearch(_search);
  }

  runIndividualSearch(search) {
    this.markSearchToBeLogged(search);

    if(search.type === 'citedRefs') {
      this._router.navigate(['/search/citedRefsResults'], {queryParams: {id : search.id}});
    }
    else {
      this._router.navigate(['/search/results'], {queryParams: {id : search.id}});
    }
  }

  private markSearchToBeLogged(search){
    search.metadata.hasBeenLogged = false;
    SearchCollection.update(search);
  }

  //TODO : change the logic to form like (AnyField:(braid)) AND (AnyField:(brain))
  private queryFormulation(newSearch):PsycNETQuery {
    let pnQuery = new PsycNETQuery();
    var selectedItems =  newSearch.searches;
    for(var i=0 ; i < selectedItems.length ; i++ ){

      var criteriaArray =selectedItems[i].query.criteria;
      for(var j=0 ; i < criteriaArray.length ; i++ ){
        pnQuery.criteria.push( new Criteria(criteriaArray[j].field, newSearch.operator, criteriaArray[j].value , 'user'));
      }
    }
    //pnQuery.filter.onlyShow = this.formData.filter;
    pnQuery.source = 'easy';
    return pnQuery;
  }

  updateSelectedRecords(result){
    this.selectedSearchItems = result;
    if(this.selectedSearchItems.length){
      var tempList = _.map(this.selectedSearchItems, "id");
      for (var i = 0; i < this.recentSearches.length; i++) {
        if(tempList.indexOf(this.recentSearches[i].id)== -1){
          this.recentSearches[i].isSelected = false;
        }
      }
    }else{
      this.handleSelectDeselectAll(false);
    }
    this.setSelectAll();
  }

  onSelectAllChange(ev) {
    this.handleSelectDeselectAll(this.isSelectAll);
    this.handleSelected();
  }

  private handleSelectDeselectAll(flag) {
    this.recentSearches.forEach(_item=>_item.isSelected = flag);
  }

  //set selectAll variable true/false condition
  private setSelectAll() {
    this.isSelectAll = this.selectedSearchItems.length == this.recentSearches.length;
  }

  toggleSelection(item) {
    if (item.isSelected) {
      this.selectedSearchItems.push(item);
    } else {
      this.handleSelected();
    }
    this.setSelectAll();
  }

  togglePermalink(index: any) {
    this.showPermalink[index] = !this.showPermalink[index];
    let search = this.recentSearches[index];
    if(this.showPermalink[index] && !search.permalink) {
      this.getPermaLink(search);
    }
  }

  getPermaLink(search: any) {
    console.log('here');
    console.log(search);
    if (!search.permalinkLoading) {
      search.permalinkLoading = true;
      this.getExistingPermalink(search);
    }
  }

  getExistingPermalink(search: any) {
    let qry;
    if(search.isSaved){
      qry = this._apiService.createQuery('search.getExistingPermalink');
      qry.params({savedSearchId: search.id});
    }else {
      qry = this._apiService.createQuery('search.getNewPermalink');
      qry.params(search);
    }

    this._apiService.execute(qry).subscribe(data => {
        if (data.permalink) {
          search.permalink = data.permalink;
        }
        search.permalinkLoading = false;
      },
      error => {
        console.log(error);
        search.permalinkLoading = false;
      },
      () => console.log('Response From getExistingPermalink Complete')
    );
  }

  handleSelected() {
    this.selectedSearchItems = this.recentSearches.filter(_item=>_item.isSelected);
  }

  generateRssFeed(search) {
    let qry;
    if(search.isSaved){
      qry = this._apiService.createQuery('search.getExistingRssLink');
      qry.params({searchId: search.id});
    }else{
      qry = this._apiService.createQuery('search.getNewRssLink');
      qry.params(search);
    }

    this._apiService.execute(qry).subscribe(data => {
        if (data && data.rssLink && data.rssLink.length) {
          window.open(data.rssLink);
        }
        search.loading = false;
      },
        error => {
        console.log(error);
        search.loading = false;
      },
      () => console.log('Response From getRssLink Complete')
    );
  }
}

