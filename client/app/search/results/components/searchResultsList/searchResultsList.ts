import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ApiRequestService, Query} from "../../../../core/services/apiRequestService";
import {LocationService} from "../../../../core/services/locationService";
import {SearchCollection} from "../../../services/search/searchCollection";
import {Search} from "../../../services/search/search";
import {ActiveSearchNotifier} from "../../services/activeSearchNotifier";
import { HighlightService } from '../../../../shared/highlight/highlightService';


import * as _ from "lodash";

@Component({
  selector: 'search-results-list',
  templateUrl: './searchResultsList.html',
  styleUrls: ['./searchResultsList.less']
})

export class SearchResultsList {
  @Input() articleList: any;
  @Input() selectedPage: string;
  @Input() selectedLimit: any;
  @Input() UIDlist: Array<any>;
  @Input() redirectRecordDisplay : boolean; // if true then, redirect to /record/uid
  @Input() hideNotes : boolean; // if true then, hide AddTagNote
  @Input() buttonBarComponents: Array<string>;
  @Input() searchId: string = '';
  @Input('recommendedSearch') isRecommendedSearch: boolean;
  @Input() displayShowAllAbsBtn: boolean;
  @Input() isMyList: boolean;
  @Input() isCitedBy: boolean;
  @Input() activeTab : string;
  @Output() onChangeResultSelectEvent = new EventEmitter();
  @Output() changePagination = new EventEmitter();
  @Output() refreshResults = new EventEmitter();
  @Output() TagFiter = new EventEmitter();
  @Output() citedByItemChange = new EventEmitter();

  isAllAbsOpen: boolean;
  showButtonBarComponents: Array<string>;
  showPTButtonBarComponents: Array<string>;
  showComponentsWithoutFullText: Array<string>;
  search: Search;
  activeSearchChangesSubscription: any;

  constructor(private _apiService : ApiRequestService, private _activeSearchNotifier : ActiveSearchNotifier, private _route: ActivatedRoute, private _locationService: LocationService) {
    this.showButtonBarComponents = ['abstract', 'impact', 'references', 'pdf', 'fullText', 'test', 'openAccess', 'citedBy','addToList', 'SFXLink', 'ILLLink'];
    this.showComponentsWithoutFullText = ['abstract', 'impact', 'references', 'pdf', 'openAccess', 'citedBy', 'addToList', 'SFXLink', 'ILLLink']; //if the item is a Book, or Chapter, or Encyclopedia do not show the Full text button
    this.showPTButtonBarComponents = ['abstract', 'impact', 'references', 'pdf', 'fullText', 'openAccess', 'citedBy', 'testVersions', 'test', 'addToList', 'SFXLink', 'ILLLink'];
  }

  ngOnInit() {
    if (this.buttonBarComponents && this.buttonBarComponents.length) {
      this.showButtonBarComponents = this.buttonBarComponents
    }
    // Remove the (add to my list) button from (MyList) records
    if (this.isMyList) {
      this.showComponentsWithoutFullText.pop();
      this.showPTButtonBarComponents.pop();
      this.showButtonBarComponents.pop();
      this.showComponentsWithoutFullText.push('AddTagNote');
      this.showPTButtonBarComponents.push('AddTagNote');
      this.showButtonBarComponents.push('AddTagNote');
    }
    if (this.isMyList && this.articleList.doc && this.UIDlist && this.UIDlist.length) {
      for (let item of this.articleList.doc) {
        let thisArticleTagsNotes = _.filter(this.UIDlist, function (o) {
          return o['articleId'] == item.UID;
        });
        item.TagNote = thisArticleTagsNotes;
        let thisNotes = thisArticleTagsNotes[0];
        if (thisNotes && thisNotes.hasOwnProperty('notes')) {
          item.Note = thisNotes['notes'];
        }
      }
    }



    this.activeSearchChangesSubscription = this._activeSearchNotifier.activeSearchChanges$.subscribe();
    /*this._route.queryParams.subscribe(params => {
      this.search = SearchCollection.get(params['id']);
    });*/
    this.search = SearchCollection.get(this.searchId);


    this._activeSearchNotifier.activeSearchChanges$.subscribe(id => {
      this.search = SearchCollection.get(id);
    });

    if(this.articleList && this.articleList.hasOwnProperty('doc') && this.articleList.doc.length) {
      for (let item of this.articleList.doc) {
        // Set the author names for the given item, using individual and institutional ones.
        let authors = [];
        if (item.AuthorOrig) {
          item.AuthorOrig = item.AuthorOrig.map(_author=> {
            if (_author.charAt(_author.length - 1) != '.') {
              if(_author.slice(-1) == '>') {
                let name = HighlightService.removeHighlights(_author);
                _author = name.slice(-1) != '.' ?  _author.replace(/(<\/span>)$/g, '.</span>') : _author;
              } else {
                _author += '.';
              }
            }
            return _author;
          });
          authors.push(...item.AuthorOrig);
        }
        if (item.InstitutionalAuthor) {
          authors.push(item.InstitutionalAuthor);
        }
        item.authors = authors.join('; ');

        let tempAuthor = item.authors.split(";");
        if(tempAuthor && tempAuthor.length > 2 ){
          let index = item.authors.lastIndexOf(';');
          item.authors = _.clone(item.authors.substring(0, index+1) + " &" + item.authors.substring(index+1));
        }

        // Create document type list, then check its last item if it is empty to discard it.
        if (item.hasOwnProperty('DocumentTypeLabel') && item.DocumentTypeLabel.trim().length) {
          item.DocumentTypeLabel = item.DocumentTypeLabel.replace(/(\/)/g, '/ ');
          item.documentTypesList = (item.DocumentTypeLabel.indexOf(';') > -1) ? item.DocumentTypeLabel.split(';') : [item.DocumentTypeLabel];
        }

        if (item.hasOwnProperty('documentTypesList') && item.documentTypesList.length > 1) {
          let lastType = item.documentTypesList.pop();
          if (lastType.trim().length) {
            item.documentTypesList.push(lastType);
          }
        }
        if (item.hasOwnProperty('SourcePE') && item.SourcePE.length > 1) {
          this.setPEPages(item);
        }
        if (item.hasOwnProperty('TransDocumentTitle') && item.TransDocumentTitle.length > 1) {
          this.compareTitleWithTransTitle(item);
        }
      }
    }
  }
  toggleNotes(item) {
    item.isNotesOpen = !item.isNotesOpen;
  }
  filterbyTag(TagID){
    this.TagFiter.emit(TagID);
  }

  getRDPQueryParams(index){
    if(this.isMyList) {
      return;
    }
    return {
      id: this.search.id,
      recordId: this.getRecordIndex(index),
      tab: this.search.query.tab,
      page: this.selectedPage,
      display: this.search.responseParameters.rows,
      sort: this._locationService.generateSortParamString(this.search.query.displayOptions.sort),

      // for logging events as RC
      sr:1
    };
  }

  getRecordIndex(index){
    let recIndex: number = ((+this.selectedPage-1) * +this.search.responseParameters.rows) + index+1;

    return recIndex;
  }

  getLink(item) {
    if(this.isMyList) {
      return ['/record/'+item.UID];
    }
    return ['/search/display'];
  }

  onSearchResultSelect(record){
    var selectedSearchResultItems =  _.filter(this.articleList.doc, document=>{
      return document['isSelected'];
    });

    var selected= {
      newlySelected : record,
      selectedRecordList : selectedSearchResultItems
    };

    this.onChangeResultSelectEvent.emit(selected);
  }

  setAbstractDisplayStatus() {
    this.toggleAllAbstract();
  }

  public getFormattedTitle(item: any): string {
    let title = '';
    if (item.hasOwnProperty('GivenDocumentTitle') && item.GivenDocumentTitle && item.GivenDocumentTitle.length) {
      title += item.GivenDocumentTitle;
    }
    if (item.hasOwnProperty('Acronyms') && item.Acronyms && item.Acronyms.length) {
      title += ' (' + item.Acronyms + ')';
    }
    return title;
  }

  toggleAllAbstract() {
    this.isAllAbsOpen = !this.isAllAbsOpen;
    var ids = [];
    let logItems = [];

    this.articleList.doc.forEach(article=> {
      if(!article.hasOwnProperty('Abstract')){
        ids.push(article.UID);
      } else {
        logItems.push({
          UID: article.UID,
          ProductCode: article.ProductCode
        });
      }

      article['isAbsOpen'] = this.isAllAbsOpen;
      article['abstractLoading'] = true;
    });

    if(this.articleList.doc.length && ids.length) {
      let qryAbstract: Query = this._apiService.createQuery('abstract.get');
      qryAbstract.params({
        uids: ids,
        rows : ids.length,

        // uses search criteria for abstract highlighting
        searchId: this.search.id
      });

      qryAbstract.logs({
        pageId: 'Within_page_RV/RC'
      });

      this._apiService.execute(qryAbstract).subscribe(
        data => {
          var abstractList = data['response'].result.doc;
          var abstractIds = _.map(abstractList, 'UID');
          this.articleList.doc.forEach(article=> {
            var index = abstractIds.indexOf(article['UID']);
            if(index > -1)
              article['Abstract']= abstractList[index].Abstract.English;

            article['abstractLoading'] = false;
          });
        },
        error => {
          console.log(error);
        }
      );
    }

    // The already loaded abstracts will not be logged by calling (abstract.get), so log them specially here.
    if (this.isAllAbsOpen && logItems.length) {
      let logQuery = this._apiService.createQuery('log.logEntry');
      logQuery.params({ abstractsViewedData: logItems });
      logQuery.logs({ pageId: 'Within_page_RV/RC' });
      this._apiService.execute(logQuery).subscribe();
    }
  }

  getNewPagination(pagination) {
    this.changePagination.emit(pagination);
  }

  reloadResults() {
    this.refreshResults.emit(true);
  }

  displayCitedByItem(uid) {
    this.citedByItemChange.emit(uid);
  }

// TODO: ensure that abstracts are closed on results reload
  ngOnChanges(changes){
    if(this.isAllAbsOpen &&  changes.articleList && changes.articleList.previousValue && changes.articleList.currentValue && !this.articleList.doc[0].hasOwnProperty('Abstract')){
      this.isAllAbsOpen = false;
    }
  }

  setPEPages(record) {
    if(record.SourcePE.match(/\d+-\d+/g)) {
       record.PEPages= /\d+-\d+/g.exec(record.SourcePE);
    }
    else  if(record.SourcePE.match(/\d+ p+/g)) {
     record.PEPages= /\d+ p+/g.exec(record.SourcePE);
    }
    else {
      record.PEPages = "";
    }
  }

  compareTitleWithTransTitle(record: any) {
    let transTtile = HighlightService.removeHighlights(record.TransDocumentTitle);
    let title =  HighlightService.removeHighlights(this.getFormattedTitle(record));
    record.isTransTitle = title == transTtile ? false : true;
  }

  ngOnDestroy() {
    this.activeSearchChangesSubscription.unsubscribe();
  }
}
