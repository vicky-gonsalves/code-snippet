import {Component, Input, ViewChild, Optional} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap';
import {ActivatedRoute} from "@angular/router";
import {SearchCollection} from "../../../services/search/searchCollection";
import {ApiRequestService, Query} from "../../../../core/services/apiRequestService";
import {Router} from '@angular/router';
import {Search} from "../../../services/search/search";
import * as _ from "lodash"

@Component({
  selector : 'save-search-modal',
  templateUrl: './saveSearchModal.html',
  styleUrls: ['./saveSearchModal.less']
})
export class SaveSearchModal{
  @ViewChild('saveSearchModal') public saveSearchModal : ModalDirective;
  @Input() searchId: string = '';
  currentSearch :Search;
  submitted: boolean;
  searchName: string;
  nameExistError : boolean;
  oldSearchName : string;
  constructor(private _route : ActivatedRoute,
              private _apiService : ApiRequestService,
              private _router:Router){
    this._reset();
  }

  ngOnInit(){
  }

  openModal(@Optional() currentSearch?: Search): void {
    if(currentSearch){
      this.currentSearch = currentSearch;
    }
    this._reset();
    this.saveSearchModal.show();
  }

  closeModal():void {
    this._reset();
    this.saveSearchModal.hide();
  }

  private _reset(){
    this.submitted = false;
    this.nameExistError = false;
  }

  saveSearch(){
    this.submitted = true;
    let search;
    if(this.searchId){
      search = SearchCollection.get(this.searchId);
    }else{
      search = this.currentSearch;
    }

    let qry = this._apiService.createQuery('search.saveSearch');
    qry.params(search);
    qry.params({searchName: this.searchName});

    this._apiService.execute(qry).subscribe(data => {
      if(data['EXIST'] == 1){
        this.submitted = false;
        this.nameExistError = true;
        this.oldSearchName = _.clone(this.searchName);
      }else{
        this.closeModal();
        this._router.navigate(['/MyPsycNET/savedSearches']);
      }
    });
  }
}
