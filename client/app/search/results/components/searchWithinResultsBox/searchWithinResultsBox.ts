import {Component, Input,Output, EventEmitter, SimpleChanges} from '@angular/core';

@Component({
  selector: 'search-within-results-box',
  templateUrl: './searchWithinResultsBox.html',
  styleUrls: ['./searchWithinResultsBox.less']
})
export class SearchWithinResultsBox {
  @Input() searchwithinSearchQueries ;
  @Output() searchWithinSearchevent = new EventEmitter();
  model = {q: ''};
  searchwithinSearchObj = {
    term: '',
    status: ''
  };
  constructor() {
  }

  ngOnInit() {
    console.log(this.searchwithinSearchQueries);
  }

  ngOnChanges(changes : SimpleChanges) {
    console.log('Bound data has changed: ', changes);
    if(changes.hasOwnProperty('searchwithinSearchQueries')) {
      this.model.q = '';
    }
  }

  onSubmit() {
    console.log(this.model);
    this.searchwithinSearchObj.term=this.model.q;
    this.searchwithinSearchObj.status='add';
    this.searchWithinSearchevent.emit(this.searchwithinSearchObj);
  }
  removeCriteria(term){
    console.log(term);
    this.searchwithinSearchObj.term=term;
    this.searchwithinSearchObj.status='remove';
    this.searchWithinSearchevent.emit(this.searchwithinSearchObj);
  }
}
