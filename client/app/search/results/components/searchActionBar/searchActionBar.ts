import {Component, Input, ViewChild} from '@angular/core';
import {ActivatedRoute, Router, RouterStateSnapshot} from "@angular/router";

import { Locale, LocaleService, LocalizationService } from 'angular2localization';

import {SearchCollection} from "../../../services/search/searchCollection";
import {SearchNavigation} from "../../../services/searchNavigation";
import {Query,ApiRequestService} from "../../../../core/services/apiRequestService";
import {SaveSearchModal} from "../saveSearchModal/saveSearchModal";
import {AuthService} from "../../../../core/services/authService";
import {NavigationEvent} from "../../../../core/services/navigationEvent";

@Component({
  selector: 'search-action-bar',
  templateUrl: './searchActionBar.html',
  styleUrls: ['./searchActionBar.less']
})

export class SearchActionBar extends Locale {
  @Input() totalRecordsFound: number = 0;
  @Input() friendlyQuery: string = '';
  @Input() searchId: string = '';
  @Input('citedRef') isCitedRef;

  @Input() showAllOptions: boolean;
  @Input() isPHSelectedAlone: boolean;
  @ViewChild('saveSearchDialog') saveSearchDialog: SaveSearchModal;
  private _snapshot: RouterStateSnapshot;

  loading: boolean = false;
  permalink: string;
  showPermalink = false;


  constructor(public _locale: LocaleService,
              public _localization: LocalizationService,
              private searchNavigation: SearchNavigation,
              private _route: ActivatedRoute,
              private _apiService: ApiRequestService,
              private _router: Router,
              private _navigationEvent: NavigationEvent,
              private _authService: AuthService) {
    super(_locale, _localization);
    this._snapshot = _router.routerState.snapshot;
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  ngAfterViewChecked() {
    //block handles saves search after login
    if (this._authService.isAuthenticated() && this._navigationEvent.hasAction()) {
      let article = this._navigationEvent.get();
      if (article.event == 'saveSearch') {
        this.saveSearchDialog.openModal();
        this._navigationEvent.clear();
      }
    }
  }

  editSearch() {
    let search = SearchCollection.get(this.searchId);
    if (search.query.filter.facets.length &&  search.query.source == "easy") {
      search.query.source = "advanced";
    }
    this.searchNavigation.editSearch(search);
  }

  newSearch() {
    let search = SearchCollection.get(this.searchId);
    this.searchNavigation.newSearch(search);
  }

  updateLoadingStatus(flag) {
    this.loading = flag;
  }

  togglePermalink() {
    this.showPermalink = !this.showPermalink;
    if(this.showPermalink && !this.permalink) {
      this.getPermaLink();
    }
  }

  getPermaLink() {
    if (!this.loading) {
      this.updateLoadingStatus(true);
      this.saveSearch();
    }
  }

  getRSSFeed() {
    if (!this.loading) {
      this.updateLoadingStatus(true);
      this.generateRssFeed();
    }
  }

  openSaveSearch() {
    if (this._authService.isAuthenticated()) {
      this.saveSearchDialog.openModal();
    } else {
      this._navigationEvent.set('saveSearch', null);
      this._authService.redirectToLogin(this._snapshot.url);
    }
  }

  saveSearch() {
    let search = SearchCollection.get(this.searchId);

    let qry = this._apiService.createQuery('search.getNewPermalink');
    qry.params(search);

    this._apiService.execute(qry).subscribe(data => {
        if (data.permalink) {
          this.permalink = data.permalink;
        }
        this.updateLoadingStatus(false);
      },
      error => {
        console.log(error);
        this.updateLoadingStatus(false);
      },
      () => console.log('Response From saveSearch Complete')
    );
  }

  generateRssFeed() {
    let search = SearchCollection.get(this.searchId);
    let qry = this._apiService.createQuery('search.getNewRssLink');
    qry.params(search);

    this._apiService.execute(qry).subscribe(data => {
        console.log(data);
        if (data && data.rssLink && data.rssLink.length) {
          window.open(data.rssLink);
        }
        this.updateLoadingStatus(false);
      },
      error => {
        console.log(error);
        this.updateLoadingStatus(false);
      },
      () => console.log('Response From getRssLink Complete')
    )
  }

  setEmailAlert(){

    let search = SearchCollection.get(this.searchId);
    let alert = {
      searchId : this.searchId,
      frequency : 7,
      expiresAfter : 365,
    };

    let query:Query = this._apiService.createQuery('myPsycNet.setSearchEmailAlert');
    query.params(search);
    query.params({emailAlert: alert});
    this._apiService.execute(query)
      .subscribe(
        data => {
        if(data.isEmailAlertTriggered){
          this._router.navigate(['/MyPsycNET/alerts'], {queryParams: {tab: '0', searchId: this.searchId}});
        }
      },
        error => console.log(error)
    );
  }


}
