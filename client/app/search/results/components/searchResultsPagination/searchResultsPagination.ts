import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'search-results-pagination',
  templateUrl: './searchResultsPagination.html',
  styleUrls: ['./searchResultsPagination.less']
})
export class SearchResultsPagination implements OnInit {
    @Input() displayShowAllAbsBtn: boolean;
    @Input() isAllAbsOpen: boolean;
    @Input() totalItems: string;
    @Input() selectedLimit: string;
    @Input() selectedPage: string;
    @Input() activeTab : string;
    @Input('recommendedSearch') isRecommendedSearch: boolean;

    @Output() displayAbstractsChanged = new EventEmitter();
    @Output() changePagination = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    toggleAllAbstracts() {
        this.isAllAbsOpen = !this.isAllAbsOpen;
        this.displayAbstractsChanged.emit(this.isAllAbsOpen);
    }

    ngOnChanges(changes){
        if(changes.hasOwnProperty('isAllAbsOpen') && changes.isAllAbsOpen.previousValue && changes.isAllAbsOpen.currentValue) {
          this.isAllAbsOpen = changes.isAllAbsOpen.currentValue;
        }
    }

    getNewPagination(pagination) {
        this.changePagination.emit(pagination);
    }
}
