import {Component, Output, EventEmitter, Input} from '@angular/core';
import {Router} from '@angular/router';
import {IHitcount, HitCount} from "../../services/tab/searchResultTabCalculator";
import {ProductLabels} from '../../../../shared/productLabels/productLabels';
import {IdentityService} from '../../../../core/services/identity/identityService';
import {IIdentity, Identity} from '../../../../core/services/identity/identity';
import {PermissionService} from '../../../../core/services/identity/permissionService';
import {SearchCollection} from "../../../services/search/searchCollection";

@Component({
  selector: 'search-tabs',
  templateUrl: './searchTabs.html',
  styleUrls: ['./searchTabs.less']
})

export class SearchTabs {
  @Input() defaultTab: string;
  @Input() tabNumbers: HitCount;
  @Input() searchId: string;
  @Output() searchTabText = new EventEmitter();
  selectedTab : string ;
  isAllVisible : boolean;
  isInit : boolean;

  tabDisplayOrder = ['all','PI','PA','PB','PE','PC','PT','PH'];

  private _identity: IIdentity;
  private _urlMap = {
    'PI' : '/buy/PI',
    'PC' : '/buy/PC',
    'PE' : '/buy/PE'
  };

  private isBookCollectionOnlyUser:boolean = false;

  constructor(private _router: Router,
              private _permissionService : PermissionService) {
    this.isInit = false;
  }

  ngOnInit() {
    IdentityService.identity.subscribe(data => {
      if(!data){
        return false;
      }

      this._identity = data;
      this.isInit = true;

      if(!this._isOrganizationUser() && !this._isIndividualUserOnly() && this.defaultTab === 'all') {
        this.selectedTab = 'PA';
      } else {
        this.selectedTab = this.defaultTab;
      }

      if(!this.tabNumbers) {
        this.tabNumbers = new HitCount();
        this.tabNumbers.selectedDatabases = SearchCollection.get(this.searchId).query.databases;
      }

      this.isBookCollectionOnlyUser = (this._isOrganizationUser() && !this._permissionService.mayAccess('PB') && this._permissionService.hasGroupAccess('bookCollection'));

      this._updateTabSelectionsAndVisibility();
      console.log(this.tabNumbers);
    });
  }

  ngOnChanges(changes){
    if(this.isInit && changes.tabNumbers && changes.tabNumbers.currentValue){
      this._updateTabSelectionsAndVisibility();
    }
  }

  private _updateTabSelectionsAndVisibility(){
    this._applyTabOrder();
    this.isAllVisible =  (this.tabNumbers.selectedDatabases.length > 1) && (this._isOrganizationUser() || (this._isIndividualUserOnly() && this._permissionService.mayAccessAllDefaultProducts()));
  }

  // makes sure that this.tabNumbers.selectedDatabases is always in the correct order for display
  private _applyTabOrder(){
    let orderedList = [];

    this.tabDisplayOrder.forEach(tab => {
      if(this.tabNumbers.selectedDatabases.indexOf(tab) >=0 ){
        orderedList.push(tab);
      }
    });

    this.tabNumbers.selectedDatabases = orderedList;
  }

  private _isOrganizationUser(){
    return this._identity.isOrganization == true;
  }

  private _isIndividualUserOnly(){
    return this._identity.isIndividual == true && this._identity.isOrganization == false;
  }

  private _isGuestUser(){
    return this._identity.isIndividual == false && this._identity.isOrganization == false;
  }

  getLabel(db){
    let label = ProductLabels.getLabel(db);

    if(db === 'PB' && this.isBookCollectionOnlyUser){
      label = 'APA Books';
    }

    if (db === 'PA' && (!this._permissionService.mayAccess('PA') && this._permissionService.hasGroupAccess('printSubscription'))) {
      label = 'APA Journals';
    }

    return label;
  }

  selectTab(tab) {
    /*
    * condn1 - is individual
    * condn2 - is PA or PB, then proceed to access
    * condn3 - check for access other than PA & PB
    * */
    if( tab !== 'all' && (this._isIndividualUserOnly() || this._isGuestUser()) && ["PA","PB"].indexOf(tab) == -1 && !this._permissionService.mayAccess(tab) ){
      //redirect to options to buy pages
      this._router.navigate([this._urlMap[tab]]);
      return false;
    }
    this.selectedTab = tab;
    this.searchTabText.emit(tab);
  }

  ngOnDestroy() {
  }
}
