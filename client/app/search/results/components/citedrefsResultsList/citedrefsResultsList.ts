import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ApiRequestService} from "../../../../core/services/apiRequestService";
import {SessionStorageService} from 'ng2-webstorage';
import * as _ from "lodash";
@Component({
  selector: 'citedrefs-results-list',
  templateUrl: './citedrefsResultsList.html',
  styleUrls: ['./citedrefsResultsList.less']
})

export class CitedrefsResultsList {
  @Input() articleList: any;
  @Input() selectedLimit;
  @Input() totalItems;
  @Input() selectedPage;
  @Input() loading: boolean;

  @Output() changePagination = new EventEmitter();

  constructor(private _apiService:ApiRequestService, private _sessionStorageService: SessionStorageService){

  }

  ngOnInit(){
    for(let item of this.articleList.doc) {
      // Only preserve the citation's ending full-stop (if exists) when there is no PubYear value.
      if(item.SourceTitle && item.SourceTitle.endsWith('.') && item.PubYear) {
        item.SourceTitle = item.SourceTitle.slice(0, item.SourceTitle.length - 1);
      }

      if(item.Author && item.Author!= ''){
        let tempAuthor = item.Author.split(";");
        if(tempAuthor && tempAuthor.length > 2 ){
          let index = item.Author.lastIndexOf(';');
          item.Author = _.clone(item.Author.substring(0, index+1) + " &" + item.Author.substring(index+1));
        }
      }
    }

  }

  getNewPagination(pagination) {
    this.changePagination.emit(pagination);
  }

  logCitedByAction(uid){
    this._sessionStorageService.clear('selectedSearchRecords');
    this._sessionStorageService.clear('currentPageSearchRecords');

    let query = this._apiService.createQuery('log.logEntry');
    query.logs({
      uid: uid,
      pageId: 'CitedRefs_SR_citedby',
      eventType: 'RC',
      abstractsViewedUIDs: [{
        UID: uid,
        ProductCode: 'PI'
      }]
    });
    this._apiService.execute(query).subscribe();
  }
}
