/**
 * Interface for Article item
 */

export interface IArticleItem {
  AuthorOrig:string[],
  DOI:string,
  FTCorrectionDate:string,
  GivenDocumentTitle:string,
  HasOpenAccess?:string,
  HasCitations?:string,
  HasFullText?:string,
  MonthSeason:string,
  MediaLink?:string,
  PAFirstPage:string,
  PAIssue:string,
  PAIssueCode:string,
  PAJournalCode:string,
  PAVolume:string,
  PDFLink:string,
  PICorrectionDate:string,
  PIJournalTitle:string,
  PIReleaseDate:string,
  Pagination:string,
  PublicationYear:string,
  SFXOpenURL:string,
  SourcePI?:string,
  UID:string,
  XMLLink:string,
  CitedByCount?:string,
  RightsLink?:string,
  wrap?:string,
  Abstract?:string,
  ImpactStatement?:string,
  isAbsOpen?:boolean,
  isRefOpen?:boolean,
  isImpactOpen?:boolean,
  ProductCode:string,
  isTagNoteOpen?:boolean
  isTestOpen?:boolean,
  HasOtherVersions: string,
  otherVersionsArray?:Array<any>
}
