import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ISortDescription} from '../../../services/search/query/components/interfaces';
import {SortDescriptionService} from "../../../services/search/query/sortDescriptionService";

@Component({
  selector: 'search-results-nav',
  templateUrl: './searchResultsNav.html',
  styleUrls: ['./searchResultsNav.less']
})

export class SearchResultsNav {
  @Input() id : string;
  @Input() filterVisible: boolean;
  @Input() selectedLimit: string;
  @Input() isCitedRef : boolean;
  @Input() isMyList : boolean;
  @Input() isSelectAll : boolean;
  @Input('recommendedSearch') isRecommendedSearch :boolean;
  @Input() allRecordsOnPageUIDList : Array<any>;
  @Input() allSelectedRecordUIDList : Array<any>;
  @Input() showAllToolBarOptions: boolean;
  @Input() currentSortBy: ISortDescription[];
  @Input() hideSortByOption: boolean;
  @Input() showDeleteFromMylist: boolean;
  @Output() applySortBy = new EventEmitter();
  @Output() changeRecordsPerPage = new EventEmitter();
  @Output() onChangeSelectAllEvent = new EventEmitter();
  @Output() refreshResults = new EventEmitter();


  //isSelectAll: boolean = false; //Enable when all the results are selected
  disabledFormControls: boolean = false;  //disable till results are available

  constructor() {
  }

  ngOnInit() {
  }

  toggleAllSelection(selectAll: boolean) {
    this.onChangeSelectAllEvent.emit(selectAll);
  }

  getNewSortTerm(term) {
    if(term.length){
      let sortOrder = SortDescriptionService.createSortOptions(term);
      this.applySortBy.emit(sortOrder);
    }
  }

  getNewPaginationLimit(evt){
    this.changeRecordsPerPage.emit(evt);
  }

  getFreshResults(evt){
    this.refreshResults.emit(evt);
  }

  ngOnDestroy() {
  }
}
