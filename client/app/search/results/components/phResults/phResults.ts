import {Component, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CookieService} from 'angular2-cookie/core';

import {ApiRequestService, Query} from "../../../../core/services/apiRequestService";

@Component({
  selector: 'ph-results',
  templateUrl: './phResults.html',
  styleUrls: ['./phResults.less']
})
export class PHResults {
  phHost: string;
  start = 0;
  isLoading: boolean = true;
  currentPage = 1;
  rows = 25;
  criteria: any;
  friendlyQuery: any;
  queryParams: any;
  videoList = [];
  phCount: number;
  isAllAbsOpen: boolean;
  searchId: string;

  selectedUID: string;

  constructor(private _apiService: ApiRequestService, private _route: ActivatedRoute, private _router: Router, private _cookieService: CookieService) {
  }

  ngOnInit() {
    this.phHost = this._cookieService.get('PN_THERAPY');
    this._route.queryParams.subscribe(params => {
      if (params.hasOwnProperty('id') && params['id']) {
        this.searchId = params['id'];
      }

      if (!this.searchId) {
        this._router.navigate(['/search']);
      }
    });
    this.getSearchInfoById();
  }

  private getPHVideos() {
    this.isLoading = true;
    let qry = this._apiService.createQuery('search.phResults');
    qry.params(this.queryParams);

    this._apiService.execute(qry).subscribe(
      data => {
        this.phCount = parseInt(data.response.result.numFound);
        this.videoList = data.response.result.doc;
        this.checkNoRecordsFound();
        this.isLoading = false;
      },
      error => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  checkNoRecordsFound() {
    if (this.phCount == 0) {
      this._router.navigate(['/search']);
    }
  }

  private getSearchInfoById() {
    if (!this.searchId) {
      return false;
    }

    // get the recent search data
    let qry = this._apiService.createQuery('recentSearch.get');
    qry.params({id: this.searchId});

    this._apiService.execute(qry).subscribe(
      data => {
        this.friendlyQuery = data.friendlyQuery;
        this.criteria = data.queryParameters.q;
        this.queryParams = {
          start: this.start,
          q: this.criteria
        };
        this.getPHVideos();
      });
  }

  toggleAllAbstracts() {
    if(!this.isAllAbsOpen){
      let abstractsViewed = [];

      this.videoList.forEach(record => {
        abstractsViewed.push({
          UID: record.uid,
          ProductCode: 'ph'
        });
      });


      this.logAction({
        pageId: 'Within_page_RV/RC',
        eventType: 'Abstracts-RV-RC',
        abstractsViewedUIDs: abstractsViewed
      });
    }

    this.isAllAbsOpen = !this.isAllAbsOpen;
    for (let i = 0; i < this.videoList.length; i++) {
      this.videoList[i].isAbsOpen = this.isAllAbsOpen;
    }
  }

  toggleAbstract(item) {

    if(!item.isAbsOpen){
      this.logAction({
        pageId: 'Within_page_RV/RC',
        eventType: 'Abstracts-RV-RC',
        abstractsViewedUIDs: [{
          UID: item.uid,
          ProductCode: 'ph'
        }]
      });
    }

    item.isAbsOpen = !item.isAbsOpen;
  }

  /*Pagination changed*/
  getNewPagination(pagination) {
    if (pagination.hasOwnProperty('currentPage')) {
      this.currentPage = pagination.currentPage;
      var start = (this.rows * (pagination.currentPage - 1));
      if (pagination.hasOwnProperty('ev') && pagination.ev) {
        this.queryParams.start = start;
        this.getPHVideos();
      }
    }
  }

  logRecordView(uid){
    this.logAction({
      pageId: 'SR_PTh',
      eventType: 'Abstracts-RV-RC',
      abstractsViewedUIDs: [{
        UID: uid,
        ProductCode: 'ph'
      }]
    });
  }

  logAction(params) {
    let query = this._apiService.createQuery('log.logEntry');
    query.logs(params);
    this._apiService.execute(query).subscribe();
  }

  ngOnDestroy() {
  }
}
