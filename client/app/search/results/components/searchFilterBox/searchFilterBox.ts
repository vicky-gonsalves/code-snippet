import {Component, Input, Output, EventEmitter} from '@angular/core';
import * as _ from "lodash";
import {SearchCollection} from "../../../services/search/searchCollection";
import {Search} from "../../../services/search/search";
import {ApiRequestService} from "../../../../core/services/apiRequestService";

@Component({
  selector: 'search-filter-box',
  templateUrl: './searchFilterBox.html',
  styleUrls: ['./searchFilterBox.less']
})

export class SearchFilterBox {
  @Input() searchId:string;
  @Input() filters:Array<any>;
  @Output() updateSearchTerm = new EventEmitter();
  @Input() isRecommendSearch:boolean;

  isInit :boolean;
  facetDisplay: any;

  facetOrder = ["PublicationType", "PublicationYear", "PubYear", "IndexTerms", "Affiliation", "SerialTitle","Construct","Author", "AgeGroup", "PopulationGroup",
    "InstrumentType","TestRecordType","Permissions","Methodology", "Instrumentation", "Classification"];
  labelMapping = {
    "PublicationType" : "Publication Type",
    "PublicationYear" : "Year",
    "PubYear" : "Year",
    "IndexTerms" : "Index Terms",
    "Affiliation" :"Author Affiliation",
    "SerialTitle" : "Source",
    "Construct": "Test Construct",
    "Author" : "Author",
    "TestYear":"Test Year",
    "AgeGroup": "Age Group",
    "PopulationGroup" : "Population Group",
    "InstrumentType": "Instrument Type",
    "Methodology" : "Methodology",
    "TestRecordType" : "Test Record Type",
    "Permissions" : "Permissions",
    "Instrumentation" : "Tests & Measures",
    "Classification" : "Classification"
  };

  constructor(private _apiService: ApiRequestService) {

  }

  ngOnInit() {
    this.isInit = true;
    this.populateFacetDisplay();
  }

  ngOnChanges(changes){
    if(this.isInit){
      this.populateFacetDisplay();
    }
  }

  populateFacetDisplay(){
    this.facetDisplay = [];
    if(this.filters && this.filters.length ){

      for(let i = 0; i< this.facetOrder.length; i++){
        let facetObject = _.find(this.filters, { 'name': this.facetOrder[i]});

        if(facetObject && facetObject.values && facetObject.values.length) {
          facetObject['isOpen'] = (i==0) ? true : facetObject['isOpen'];
          this.facetDisplay.push(facetObject);
        } else if(this.facetOrder[i] === "Author"){


          let search : Search = SearchCollection.get(this.searchId);

          if(search){
            this.facetDisplay.push({name: "Author", values: [], loading: true});
            let queryFacets = search.query.filter.facets;
            let facetObject = _.find(queryFacets, facet => {return facet.field === 'Author'});
            if(facetObject) {
              let self = this;
              let authorPosition = this.facetDisplay.length-1;
              this._getAuthorFacet((err, authorFacet) => {
                authorFacet['isOpen'] =true;
                self.facetDisplay.splice(authorPosition, 1, authorFacet);
              });
            }

          } else {

            this.facetDisplay.push({name: "Author", values: [], loading: true});
          }


        }
      }
    }
  }

  selectTerm(term, fieldName ) {
    term.selected = !term.selected;
    this.updateSearchTerm.emit({field: fieldName, value : term.name});
  }

  onToggleFacetFilter(filter, index){
    filter.isOpen = !filter.isOpen;
    if(filter.name == "Author" && !filter.values.length){
      var self = this;
      this._getAuthorFacet(function(err, facet){
        self.facetDisplay[index] = facet;
        self.facetDisplay[index].isOpen = true;
      });
    }
  }

  private _getAuthorFacet(cb){

    this.getSearch((err, search) => {

      if(err){
        return cb('not found');
      }

      let qry = this._apiService.createQuery('search.authorFacet');
      qry.params(search);

      this._apiService.execute(qry).subscribe(
        data => {
          console.log('authorFacet data' , data.response.facet);
          cb(null , data.response.facet);
        },
        error => {
          console.log(error);
          cb(error);
        }
      );
    });


  }

  private getSearch(cb) {

    if (!this.searchId) {
      return cb('not found');
    }

    let search : Search = SearchCollection.get(this.searchId);

    if(search){
      return cb(null, search);
    } else {
      // could be a page refresh, so get it from the server

      let qry = this._apiService.createQuery('recentSearch.get');
      qry.params({id: this.searchId});

      this._apiService.execute(qry).subscribe(
        (data: Search) => {
          if (data.hasOwnProperty('query')) {

            // add the search if it doesn't already exist
            SearchCollection.add(data);

            return cb(null, data);
          }
        });
    }


  }

  ngOnDestroy() {

  }
}
