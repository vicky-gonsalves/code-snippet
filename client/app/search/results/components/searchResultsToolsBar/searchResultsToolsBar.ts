import {Component, Input, Output, ViewChild, EventEmitter} from '@angular/core';
import { Locale, LocaleService, LocalizationService } from 'angular2localization';

import {EmailRecordModal} from '../../../../shared/emailRecordModal/emailRecordModal';
import {SearchCollection} from "../../../services/search/searchCollection";
import {Router, ActivatedRoute, RouterStateSnapshot} from "@angular/router";
import {Search} from "../../../services/search/search";
import {Query,ApiRequestService} from '../../../../core/services/apiRequestService';
import {ExportRISModal} from '../../../../recordDisplay/components/exportRISModal/exportRISModal';
import {PrintRecordModal} from '../../../print/components/printRecordModal/printRecordModal';
import {DeleteConfirmationModal} from '../../../../shared/deleteConfirmationModal/deleteConfirmationModal';
import {MylistStorageService} from '../../../../core/services/mylistStorageService';
import {AuthService} from '../../../../core/services/authService';
import {NavigationEvent} from '../../../../core/services/navigationEvent';

import * as _ from "lodash"
import {SessionStorageService} from "ng2-webstorage";
import {AddToListWarningModal} from "../../../../shared/addToListWarningModal/addToListWarningModal";
import {AddToListConfirmationModal} from "../addToListConfirmationModal/addToListConfirmationModal";

@Component({
  selector: 'search-results-tools-bar',
  templateUrl: './searchResultsToolsBar.html',
  styleUrls: ['./searchResultsToolsBar.less']
})

export class SearchResultsToolsBar extends Locale {
  @Input() isMyList : boolean;
  @Input() parentId : string;
  @Input() showAllOptions : boolean;
  @Input() showDeleteFromMylist : boolean;
  @Input() allRecordsOnPageUIDList : Array<any>;
  @Input() allSelectedRecordUIDList : Array<any>;
  @Output() refreshResult = new EventEmitter();
  @ViewChild('emailRecordDialog') emailRecordDialog : EmailRecordModal;
  @ViewChild('exportRisDialog') exportRisDialog : ExportRISModal;
  @ViewChild('printRecordDialog') printRecordDialog: PrintRecordModal;
  @ViewChild('deleteConfirmationDialog') deleteDialog : DeleteConfirmationModal;
  @ViewChild('addToListWarningDialog') addToListWarningDialog: AddToListWarningModal;
  @ViewChild('addToListConfirmationDialog') addToListConfirmationDialog: AddToListConfirmationModal;
  deleteMessage:string;
  query:Query;
  searchId : string;
  isSaving: boolean;
  private _snapshot: RouterStateSnapshot;
  mylistCount = 0;
  totalAllowedMylistCount = 250;
  confirmationMessage: string;
  numberOfDisplayedModals: string;

  constructor(public _locale: LocaleService,
              public _localization: LocalizationService,
              private _route: ActivatedRoute,
              private _apiService: ApiRequestService,
              private _mylistStorageService: MylistStorageService,
              private _navigationEvent:NavigationEvent,
              private _authService : AuthService,
              private router: Router,
              private _sessionStorageService: SessionStorageService) {
    super(_locale, _localization);
    this._snapshot = router.routerState.snapshot;
  }

  ngOnInit() {

    this.isSaving = false;
    MylistStorageService.count.subscribe(count => this.mylistCount = count);
    this._route.queryParams.subscribe(urlParams => {
      if (urlParams.hasOwnProperty('id') && urlParams['id']) {
        this.searchId = urlParams['id'];
      }
    });
  }

    ngAfterViewInit () {
    //block handles adding to my-list after login
     if(this._authService.isAuthenticated() && this._navigationEvent.hasAction()){
      let article = this._navigationEvent.get();
      if(article.args.hasOwnProperty('type') && article.args.type === 'batch') {
        let articleList;
        if(article.event == 'addMyList' && article.args.hasOwnProperty('uidList') && article.args['uidList'].length){
          this._navigationEvent.clear();
          if(this.mylistCount < this.totalAllowedMylistCount) {
            articleList= article.args['uidList'];
            let addSomeRecords = false;
            let total = this.mylistCount + articleList.length;
            if (total > this.totalAllowedMylistCount) { //if total goes above 250
              let overFlow = total - this.totalAllowedMylistCount;
              let pick = articleList.length - overFlow;
              articleList = articleList.slice(0, pick);
              addSomeRecords = true;
              this.openAddToListWarningDialog();
            }
            this.numberOfDisplayedModals = addSomeRecords ? 'two': 'one';
            this._executeAddToMyListRequest(articleList, true, ()=>{
              this.addToMyListMessage(articleList);
            });
          } else {
            this.openAddToListWarningDialog();
          }
        }
      }
     }
  }

  openEmailRecordModal(){
    let newSearch = this._getNewSearch();
    this.emailRecordDialog.openModal(newSearch);
  }

  openAddToListWarningDialog() {
    this.addToListWarningDialog.openModal();
  }

  openAddToListConfirmationDialog() {
    this.addToListConfirmationDialog.openModal();
  }

  private _getNewSearch(){
    if(this.searchId) {
      let search:Search = SearchCollection.get(this.searchId);
      let newSearch = _.pick(search, ['id', 'friendlyQuery']);
      newSearch['databases'] = search.query.databases;
      newSearch['numFound'] = search.totalRecords;
      return newSearch;
    }
    return {};
  }

  openExportRISRecordModal(){
    this.exportRisDialog.openModal();
  }

  openPrintRecordModal(){
    let newSearch = this._getNewSearch();
    this.printRecordDialog.openModal(newSearch);
  }

  addtoMylist() {
      let myList = this._mylistStorageService.getUIDs();
      let articles = _.map(this.allSelectedRecordUIDList, 'UID');
      let articleList = _.filter(articles, function (_article: string) {
        return myList.indexOf(_article) < 0;
      });
    if (this.mylistCount < this.totalAllowedMylistCount || articleList.length == 0) {
      if(articles.length && articleList.length == 0) {
        this.addToMyListMessage(articles);
      }
      if (articleList.length) {
        let total = this.mylistCount + articleList.length;
         let addSomeRecords = false;
        if (total > this.totalAllowedMylistCount) { //if total goes above 250
          let overFlow = total - this.totalAllowedMylistCount;
          let pick = articleList.length - overFlow;
          articleList = articleList.slice(0, pick);
          addSomeRecords = true;
          this.openAddToListWarningDialog();
        }
        if (this._authService.isAuthenticated()) {
          this.numberOfDisplayedModals = addSomeRecords ? 'two': 'one';
          this._executeAddToMyListRequest(articleList, false, () => {
            this.addToMyListMessage(articleList);
          });
        } else {
          this._navigationEvent.set('addMyList', {'uidList': articleList, 'type':'batch'});
          this._authService.redirectToLogin(this._snapshot.url);
        }
      }
    } else {
      this.openAddToListWarningDialog();
    }
  }

  private _executeAddToMyListRequest(articleList, dontUpdateStatus, cb){
    if(!dontUpdateStatus) {
      this.isSaving = true;
    }
    let query = this._apiService.createQuery('mylist.addMyList');
    query.params({"ArticleList": articleList});

    this._apiService.execute(query).subscribe(data => {
      if(data.isAdded) {
        for(let addedItem of articleList) {
          this._mylistStorageService.add(addedItem);
        }
      }
      console.log('myList add response: ', data);
      this.isSaving = false;
      cb();
    },
    error => {
      console.log(error);
      this.isSaving = false;
      cb();
    });
  }

  public addToMyListMessage(selectedRecords) {
    this.confirmationMessage = selectedRecords.length > 1 ? "Selected records are added to My List." : "Selected record is added to My List."
    this.openAddToListConfirmationDialog();
  }

  deletefromMylist() {
    if (this.allSelectedRecordUIDList.length) {
      this.deleteMessage = this.allSelectedRecordUIDList.length > 1 ? "Delete records from My List? Any tags or notes will be lost." : "Delete record from My List? Any tags or notes will be lost.";
      this.deleteDialog.openModal({});
    }
  }

  processDeletefromMylist(params) {
    if(params && params.isDelete) {
      let ArticleList = _.map(this.allSelectedRecordUIDList, 'UID');
      if (ArticleList.length) {
        let query = this._apiService.createQuery('mylist.deleteMyList');
        query.params({"ArticleList": ArticleList});
        this._apiService.execute(query)
          .subscribe(
            data => {
            if (data.isDeleted) {
              for(let deletedItem of this.allSelectedRecordUIDList) {
                this._mylistStorageService.remove(deletedItem.UID);
              }
              this.deleteFromSessionStorage();
              this.refreshResult.emit(true);
            }
            console.log('myList delete response: ', data);
          },
            error => console.log(error)
        );
      }
    }
  }

  deleteFromSessionStorage() {
    this.allSelectedRecordUIDList.length = 0;
    this._updateCurrentPageSelectedRecordLocalStorage();
  }

  private _updateCurrentPageSelectedRecordLocalStorage() {
    this._sessionStorageService.store("selectedSearchRecords", this.allSelectedRecordUIDList);
  }
}
