import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ApiRequestService, Query} from '../../../../core/services/apiRequestService';
import {IArticleItem} from "../searchInterfaces/IArticleItem";
import {RelatedUIDFormatter} from '../../../../shared/relatedUIDFormatter/relatedUIDFormatter'
import {IdentityService} from '../../../../core/services/identity/identityService';
import {PurchaseService} from '../../../../core/services/purchase/purchaseService';
import {PermissionService} from '../../../../core/services/identity/permissionService';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'searchresults-buttonbar',
  templateUrl: './searchResultsButtonBar.html',
  styleUrls: ['./searchResultsButtonBar.less']
})

export class SearchResultsButtonBar {
  @Input() parentId: string = '';
  @Input() searchId: string = '';
  @Input() item: IArticleItem;
  @Input() isCitedBy: boolean;
  @Input() showButtonBarComponents: Array<string>;
  @Output() refreshResults = new EventEmitter();
  @Output() citedByItemChange = new EventEmitter();

  isOrganization: boolean;
  showAbs: boolean;
  showPdf: boolean;
  showFullText: boolean;
  showCitedBy: boolean;
  showPermission: boolean;
  showAddToList: boolean;
  showTestVersions: boolean;
  showTest: boolean;
  showOpenAccess: boolean;
  showRefs: boolean;
  showAddTagNote: boolean;
  showMedia: boolean;
  hasAccess: boolean = false;
  isOnSearchResultsPage: boolean = false;
  showSFXLink: boolean;
  showILLLink: boolean;
  hasSFX: boolean=false;
  hasILL: boolean=false;
  sfxDisplayIcon: string;
  sfxDisplayText: string;
  illDisplayIcon: string;
  illDisplayText: string;

  constructor(private _apiService: ApiRequestService,
              private _permissionService: PermissionService,
              private _router: Router,
              private _route: ActivatedRoute,
              private _purchase: PurchaseService) {
  }

  ngOnInit() {
    IdentityService.identity.subscribe(data => {
      // ensure identity is loaded
      if(!data){
        return false;
      }

      this.isOrganization = (data && data.isOrganization);
      this.isOnSearchResultsPage = (this._route.snapshot.url[0].path === 'results');

      this.hasAccess = this.item.hasOwnProperty('HasAccess') ? this.item['HasAccess'] : this._permissionService.mayAccessRecord(this.item);
      this.hasSFX = this.hasProfileProp(data, 'sfx');
      this.hasILL = this.hasProfileProp(data, 'ill');

      if (this.hasSFXProp(data, 'displayIcon')) {
        this.sfxDisplayIcon = data.profile['sfx'].displayIcon;
      } else if (this.hasSFXProp(data, 'displayText')) {
        this.sfxDisplayText = data.profile['sfx'].displayText;
      }

      if (this.hasILLProp(data, 'displayIcon')) {
        this.illDisplayIcon = data.profile['ill'].displayIcon;
      } else if (this.hasILLProp(data, 'displayText')) {
        this.illDisplayText = data.profile['ill'].displayText;
      }

      this.configureComponents();
    });


  }

  private hasProfileProp(identity, prop:string): boolean {
    return identity && identity.hasOwnProperty('profile') && identity.profile && identity.profile.hasOwnProperty(prop) && identity.profile[prop];
  }

  private hasSFXProp(identity, prop:string): boolean {
    return this.hasSFX && identity.profile.sfx.hasOwnProperty(prop) && identity.profile.sfx[prop] && identity.profile.sfx[prop].length;
  }

  private hasILLProp(identity, prop:string): boolean {
    return this.hasILL && identity.profile.ill.hasOwnProperty(prop) && identity.profile.ill[prop] && identity.profile.ill[prop].length;
  }

  openLink(popupName: string, url: string) {
    window.open(url, popupName, "location=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=460,height=420");
  }

  configureComponents() {
    this.showAbs = this.checkIfContains('abstract');
    this.showPdf = this.checkIfPDFExists();
    this.showFullText = this.checkIfXMLLinkExists();
    this.showCitedBy = this.checkIfContains('citedBy') && this.hasAccess && parseInt(this.item.CitedByCount) > 0;
    this.showPermission = this.checkIfContains('permission');
    this.showAddToList = this.checkIfContains('addToList');
    this.showTestVersions = this.checkIfContains('testVersions') && this.item.HasOtherVersions && this.item.HasOtherVersions === 'true';
    this.showTest = this.checkIfContains('test') && this.checkIfPDFExistsForTests();
    this.showOpenAccess = this.checkIfContains('openAccess');
    this.showRefs = this.checkIfContains('references') && this.hasAccess && this.item.HasCitations && this.item.HasCitations === 'true';
    this.showAddTagNote = this.checkIfContains('AddTagNote');
    this.showSFXLink = this.checkIfContains('SFXLink');
    this.showILLLink = this.checkIfContains('ILLLink');

    this.showMedia = this.checkIfMediaLinkExists();
  }

  checkIfContains(comp: string) {
    return this.showButtonBarComponents.indexOf(comp) > -1;
  }

  checkIfXMLLinkExists() {
    return this.item.hasOwnProperty('XMLLink') && this.item.hasOwnProperty('HasFullText') && this.item.XMLLink && this.item.XMLLink.length && this.item.HasFullText && this.item.HasFullText === 'true';
  }

  checkIfPDFExists() {
    return this.item.hasOwnProperty('PDFLink') && this.item.hasOwnProperty('HasFullText') && this.item.hasOwnProperty('ProductCode') && this.item.ProductCode !=='PT' && this.item.PDFLink && this.item.PDFLink.length && this.item.HasFullText && this.item.HasFullText === 'true';
  }

  checkIfMediaLinkExists() {
    return this.item.hasOwnProperty('MediaLink') && this.item.hasOwnProperty('ProductCode') && this.item.ProductCode ==='PT' && this.item.MediaLink && (this.item.MediaLink.length > 0);
  }

  checkIfPDFExistsForTests() {
    return this.item.hasOwnProperty('PDFLink') && this.item.hasOwnProperty('ProductCode') && this.item.ProductCode ==='PT' && this.item.PDFLink && (this.item.PDFLink.length > 0);
  }

  /*Toggle Abstract*/
  toggleAbstract() {
    this.item.isAbsOpen = !this.item.isAbsOpen;
    if (!this.item.hasOwnProperty('Abstract')) {
      this.getAbstract();
    } else {
      // No need to fetch the abstract, just log the toggle-on event.
      if (this.item.isAbsOpen) {
        let logQuery = this._apiService.createQuery('log.logEntry');
        logQuery.params({
          abstractsViewedData: [{ UID: this.item.UID, ProductCode: this.item.ProductCode }]
        });
        logQuery.logs({ pageId: 'Within_page_RV/RC' });
        this._apiService.execute(logQuery).subscribe();
      }
    }
  }

  actTagNotes(action){
    this.toggleAddTagNote();
    if(action === 'refresh') {
      this.refreshResults.emit(true);
    }
  }
  toggleAddTagNote() {
    this.item.isTagNoteOpen = !this.item.isTagNoteOpen;
  }

  displayCitedByItemBar(record) {
    if(this.isCitedBy) {
      console.log('CitedBy button: internal call, updating content!');
      this.citedByItemChange.emit(record.UID);
      return false;
    } else {
      console.log('CitedBy button: external call, navigating out!');
      this.logCitedByAction(record);
    }
  }

  /*Method to get Abstract Content*/
  getAbstract() {
    if (this.item && this.item.hasOwnProperty('UID') && this.item.UID.length) {
      this.item['abstractLoading'] = true;

      let query: Query = this._apiService.createQuery('abstract.get');
      query.param('uid', this.item.UID);

      // used for abstract highlighting
      if(this.searchId.length){
        query.param('searchId', this.searchId);
      }

      query.logs({
        pageId: 'Within_page_RV/RC'
      });

      this._apiService.execute(query)
        .subscribe(
          data => {
            let abs = data.response.result.doc;
            if (abs.length && abs[0].hasOwnProperty('Abstract') && abs[0].Abstract) {
              this.item.Abstract = RelatedUIDFormatter.linkRelatedUIDs(data.response.result.doc[0].Abstract.English);
            } else {
              this.item.Abstract = null;
            }
            this.item['abstractLoading'] = false;
          },
          error => {
            console.log(error);
            this.item['abstractLoading'] = false;
          },
          () => console.log('Response From getAbstract Complete')
        );
    }
  }


  /*Toggle Impact Statement*/
  toggleImpactStatement() {
    this.item.isImpactOpen = !this.item.isImpactOpen;
    if (!this.item.hasOwnProperty('ImpactStatement')) {
      this.getImpactStatement();
    } else if (this.item.ImpactStatement.hasOwnProperty('English')) {
      this.item.ImpactStatement = this.item.ImpactStatement['English'];
    }
  }

  /*Toggle Test Versions*/
  toggleTestVersions(uid) {
    this.item.isTestOpen = !this.item.isTestOpen;
    // don't make another http call if we already have the data
    if (this.item.otherVersionsArray && this.item.otherVersionsArray.length) {
      return true;
    }

    if (this.item.isTestOpen) {
      this.item['otherVersionsLoading'] = true;
      let query = this._apiService.createQuery('browsePT.getOtherVersions');
      query.param('uid', uid);
      this._apiService.execute(query)
        .subscribe(
          data => {
            this.item.otherVersionsArray = [];
            if (data.response.result.doc.length) {
              data.response.result.doc.map(doc=> {
                for (var i = 0; i < doc.OtherVersions.length; i++) {
                  let otherVersion = doc.OtherVersions[i].toString().split(',');
                  this.item.otherVersionsArray[i] = {'UID': otherVersion[0], 'Title': otherVersion[1], 'Type': otherVersion[2]};
                }
              });
              this.item['otherVersionsLoading'] = false;
            }
          },
          error => {
            console.log(error);
            this.item['otherVersionsLoading'] = false;
          },
          () => console.log('Response From ApiRequestServiceForML for Get OtherVersions Complete')
        );
    }
  }

  /*Toggle References*/
  toggleReferences() {
    this.item.isRefOpen = !this.item.isRefOpen;
    //TODO get References
  }

  /*Method to get Impact Statement*/
  getImpactStatement() {
    this.item['impactLoading'] = true;
    if (this.item && this.item.hasOwnProperty('UID') && this.item.UID.length) {
      let query: Query = this._apiService.createQuery('browsePA.getImpactStatement');
      query.param('uid', this.item.UID);
      this._apiService.execute(query)
        .subscribe(
          data => {
            let imp = data.response.result.doc;
            if (imp.length && imp[0].hasOwnProperty('ImpactStatement') && imp[0].ImpactStatement) {
              this.item.ImpactStatement = data.response.result.doc[0].ImpactStatement.English;
            } else {
              this.item.ImpactStatement = null;
            }
            this.item['impactLoading'] = false;
          },
          error => console.log(error),
          () => console.log('Response From getImpactStatement Complete')
        );
    }
  }

  getHTMLQueryParams(){
    let params = {};

    if(this.isOnSearchResultsPage){
      params = {sr:1};
    }

    return params;
  }

  getPDLLink(uid){
    let path = '/fulltext/'+uid+'.pdf';

    if(this.isOnSearchResultsPage){
      path = path + '?sr=1';
    }

    return path;
  }

  getMediaLink(uid){
    return '/fulltext/' + uid + '.media';
  }

  public getDataSetLink(dataSetLink):string {
    let url = dataSetLink.split('|');
    return url[0];
  }

  logCitedByAction(record){
    let logParams = {
      uid: record.UID,
      pageId: 'CitedRefs_SR_citedby'
    };

    // Cited by links within My List are not counted
    if(this._route.snapshot.url[0].path != 'mylist'){
      logParams['eventType'] ='RC';
      logParams['abstractsViewedUIDs'] = [{
        UID: record.UID,
        ProductCode: record.ProductCode
      }];
    }

    let query = this._apiService.createQuery('log.logEntry');
    query.logs(logParams);
    this._apiService.execute(query).subscribe();
  }

  shouldPurchase(item): boolean {
    return !item.HasAccess && !this.isOrganization;
  }

  isInCart(item): boolean {
    return this._purchase.has(item.UID);
  }

  isBook(item): boolean {
    return item.HasAccess && item.ProductCode && item.ProductCode == 'PB' && item.PBID && item.UID && item.UID.split('-').pop() === '000';
  }
}
