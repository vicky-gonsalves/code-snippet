import {Component, Input} from "@angular/core";
import {ApiRequestService, Query} from "../../../../core/services/apiRequestService";
import {IArticleItem} from "../searchInterfaces/IArticleItem";
import {HighlightService} from '../../../../shared/highlight/highlightService';

@Component({
  selector: 'search-result-reference',
  templateUrl: './searchResultReference.html',
  styleUrls: ['./searchResultReference.less']
})

export class SearchResultReference {
  @Input() record: IArticleItem;
  loading: boolean;

  constructor(private _apiService: ApiRequestService) {
  }

  ngOnInit() {
    if (this.record && this.record.UID && !this.record['CitedReferences']) {
      // Get the search result references
      this.getSearchResultReferences();
    }
  }

  public updateLoadingStatus(flag: boolean) {
    this.loading = flag;
  }

  /*Returns query params*/
  public getQueryParams() {
    return {
      start: 0,
      rows: 1000,
      workId: this.record.UID,

      // the log data is used to mark abstractsViewed
      logData: [
        {
          UID: this.record.UID,
          ProductCode: this.record.ProductCode
        }
      ]
    };
  }

  public getSearchResultReferences() {
    let queryParams = this.getQueryParams();
    queryParams.workId = HighlightService.removeHighlights(queryParams.workId);
    this.fetchCitedReferences(queryParams);
  }

  public fetchCitedReferences(queryParams) {
    this.updateLoadingStatus(true);
    let qry: Query = this._apiService.createQuery('record.getCitedReferences');
    qry.params(queryParams);
    this._apiService.execute(qry).subscribe(
      data => {
        this.record['CitedReferences'] = data.response.result;
        this.updateLoadingStatus(false);
      },
      error => {
        this.updateLoadingStatus(false);
        console.log('Record Error: ', error);
      }
    );
  }

}
