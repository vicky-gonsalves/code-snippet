import {Component, ViewChild, Input} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap';

@Component({
  selector : 'add-to-list-confirmation-modal',
  templateUrl: 'addToListConfirmationModal.html',
  styleUrls: ['addToListConfirmationModal.less']
})
export class AddToListConfirmationModal{
  @Input() message: string;
  @Input() numberOfDisplayedModals: string;
  @ViewChild('addToListConfirmationModal') public addToListConfirmationModal: ModalDirective;

  openModal():void {
    this.addToListConfirmationModal.show();
  }

  closeModal():void {
    this.addToListConfirmationModal.hide();
  }
}
