import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {SearchResults} from "./results";

const routes: Routes = [
  { path: '', component: SearchResults }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
