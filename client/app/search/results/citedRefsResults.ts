import {Component} from "@angular/core";
import {Router, ActivatedRoute} from '@angular/router';
import {ApiRequestService} from "../../core/services/apiRequestService";
import {MenuToggleNotifier} from "../../core/services/menuToggleNotifier";
import {Search} from "../services/search/search";
import {Criteria} from "../services/search/query/components/criteria";
import * as _ from "lodash";
import {SearchCollection} from "../services/search/searchCollection";

@Component({
  selector: 'citedRefsResults',
  templateUrl: './citedRefsResults.html',
  styleUrls: ['./citedRefsResults.less']
})
export class CitedRefsResults {
  searchId: string;
  resultsSearch: Search;
  results: any;
  totalResults: any;
  loading: boolean;
  selectedLimit = 25;
  currentPage = 1;
  searchwithinSearchQueries : any =[];
  //tabNumbers: HitCount;
  noshow: boolean;
  isMissing = false;

  constructor(private _apiService: ApiRequestService, private _route:ActivatedRoute, private _router : Router, private _menuToggle: MenuToggleNotifier) {

  }

  ngOnInit() {
    this._route.queryParams.subscribe(params => {

      this.searchId = params['id'];

      // get the search to execute
      this.resultsSearch = SearchCollection.get(this.searchId);

      if (this.resultsSearch) {
        this.execute();
      } else {
        if (!this.searchId) {
          this._router.navigate(['/search/citedRefs']);
        }
        this.getSearchById();
      }
    });
  }

  private execute(){
    //this.tabNumbers = new HitCount();
    //this.tabNumbers.selectedDatabases = this.resultsSearch.query.databases;

    this.loading = true;
    this.selectedLimit = this.resultsSearch.responseParameters.rows;

    this.performSearch(this.resultsSearch, (err, data) => {
      //ensure that the doc array is always returned
      data.results.result.doc = data.results.result.doc || [];

      this.results = data;
      this.totalResults = parseInt(data.results.result.numFound);

      //iterate facetSearch and mark as selected based on the filters
      if(this.resultsSearch.query.filter.facets.length){
        this._markAppliedFacets();
      }
      this.searchwithinSearchQueries = this.parseSearchWithinCriteria(this.resultsSearch.query.criteria);

      // update search in our collection
      SearchCollection.update(this.results.search);

      this.loading = false;
    });
  }

  parseSearchWithinCriteria(criteria){
    let searchWithinCriteria = _.filter(criteria, ['source', 'searchWithin']);
    let terms = _.map(searchWithinCriteria, 'value');
    return terms;
  }
  // execute a specific search
  public performSearch(search: Search, cb: Function) {
    let qry = this._apiService.createQuery('search.results');
    qry.params(search);

    this._apiService.execute(qry).subscribe(
        data => {
        cb(null , data);
      },
        error => {
        console.log(error);
        cb(error);
      }
    );
  }

  private _markAppliedFacets(){
    var filterFieldList = _.map(this.resultsSearch.query.filter.facets, 'field');
    var valueList = _.map(this.resultsSearch.query.filter.facets, 'value');
    if(this.results && this.results.results && this.results.results.facets){
      for(var i = 0; i < this.results.results.facets.length; i++){
        if(filterFieldList.indexOf(this.results.results.facets[i].name) > -1 && this.results.results.facets[i].values && this.results.results.facets[i].values.length){

          for(var j = 0; j < this.results.results.facets[i].values.length; j++){
            if(valueList.indexOf(this.results.results.facets[i].values[j].name) > -1){

              this.results.results.facets[i].values[j].selected = true;
              this.results.results.facets[i].isOpen = true;
            }
          }
        }
      }
    }
  }

  private getSearchById() {

    // if there is no 'id' url parameter, then this is a new search
    if (!this.searchId) {
      return false;
    }

    // get the recent search data and populate form
    let qry = this._apiService.createQuery('recentSearch.get');
    qry.params({id: this.searchId});

    this._apiService.execute(qry).subscribe(
      (data: Search) => {
        if (data.hasOwnProperty('query')) {
          SearchCollection.add(data);
          this.resultsSearch = data;
          this.execute();
        } else {
          this.isMissing = true;
        }
      });
  }


  triggerSearchTab(tab) {

  }

  //called when facet is updated
  updateSearch($event){
    this.currentPage = 1;
    this.resultsSearch.responseParameters.start = 0;
    this.resultsSearch.responseParameters.rows = this.selectedLimit;
    var index = _.findIndex(this.resultsSearch.query.filter.facets, $event);
    if(index == -1){
      this.resultsSearch.query.filter.facets.push($event);
    }else{
      this.resultsSearch.query.filter.facets.splice(index, 1);
    }

    SearchCollection.update(this.resultsSearch);
    this.execute();
  }

  searchWithinSearch(searchWithinSearchObj){
    if(searchWithinSearchObj.status === 'add') {
      this.resultsSearch.query.criteria.push( new Criteria('AnyField', 'AND', searchWithinSearchObj.term, 'searchWithin') );
    }else if(searchWithinSearchObj.status === 'remove'){
      _.remove(this.resultsSearch.query.criteria, {value: searchWithinSearchObj.term, source: 'searchWithin'});
    }
    this.resultsSearch.responseParameters.start = 0;
    this.currentPage = 1;
    this.resultsSearch.responseParameters.rows = this.selectedLimit;

    this.resultsSearch = SearchCollection.cloneWithNewId(this.resultsSearch);
    this._router.navigate(['/search/citedRefsResults'], {queryParams: {id: this.resultsSearch.id}, replaceUrl : true});
  }

  /*apply sort option --
   * load the active search before applying the sort option
   * sort option doesn't need to load the facets counts again
   */
  applyNewSortByOption(sortOptions){
    this.resultsSearch.query.displayOptions.sort = sortOptions;
    this.resultsSearch.responseParameters.start = 0;
    this.currentPage = 1;
    this.execute();
  }

  /*apply new records page per limit*/
  changeRecordsPerPage(limit){
    this.selectedLimit = Number(limit);
    this.resultsSearch.responseParameters.rows = this.selectedLimit;
    this.resultsSearch.responseParameters.start = 0;
    this.currentPage = 1;
    this.execute();
  }

  /*Pagination changed*/
  getNewPagination(pagination) {
    if (pagination.hasOwnProperty('currentPage')) {
      this.currentPage = pagination.currentPage;
      var start = (this.selectedLimit * (pagination.currentPage-1));
      if (pagination.hasOwnProperty('ev') && pagination.ev) {
        this.resultsSearch.responseParameters.start = start;
        this.execute();
      }
    }
  }

  toggleFacetsView() {
    this.noshow = !this.noshow;
    this._menuToggle.toggle(this.noshow);
  }
}

