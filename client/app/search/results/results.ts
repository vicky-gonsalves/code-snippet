import {Component} from "@angular/core";
import {Router, ActivatedRoute} from '@angular/router';
import {SessionStorageService} from 'ng2-webstorage';
import {ApiRequestService} from "../../core/services/apiRequestService";
import {MenuToggleNotifier} from "../../core/services/menuToggleNotifier";
import {LocationService} from '../../core/services/locationService';
import {SearchCollection} from "../services/search/searchCollection";
import {Search} from "../services/search/search";
import {Criteria} from "../services/search/query/components/criteria";
import {IdentityService} from '../../core/services/identity/identityService';
import {PermissionService} from '../../core/services/identity/permissionService';
import {HitCount, SearchResultTabCalculator} from "./services/tab/searchResultTabCalculator";
import {LateralSearchService} from "../services/search/lateralSearchService";
import {DirectSearchService} from "../services/search/directSearchService";
import {SearchNavigation} from "../services/searchNavigation";
import {ActiveSearchNotifier} from "./services/activeSearchNotifier";
import {DocumentTypeFormatter} from '../../recordDisplay/services/format/DocumentTypeFormatter';
import {PhDemonstrationsService} from "./services/phDemonstrationsService";
import {HighlightService} from "../../shared/highlight/highlightService";
import {MylistStorageService} from '../../core/services/mylistStorageService';
import * as _ from "lodash";
import * as async from "async";

@Component({
  selector: 'searchresults',
  templateUrl: './results.html',
  styleUrls: ['./results.less']
})


export class SearchResults {
  searchId: string;
  resultsSearch: Search;
  facetSearch: Search;
  isMissing = false;
  results: any;
  facets: any = [];
  loading: boolean = true;
  isRequestCompleted : boolean = true;
  showError: boolean = false;
  selectedLimit = 25;
  currentPage = 1;
  tabNumbers: HitCount;
  queryParams: any;
  isSelectAll : boolean = false;
  totalResults = 0;
  isInitialize : boolean;
  isRecommendedSearch : boolean;
  activeSearchChangesSubscription : any;
  searchwithinSearchQueries : any =[];
  allSelectedRecordUIDList: Array<any> = [];
  currentPageSelectedRecordUIDList : Array<any> = [];
  allRecordsOnPageUIDList : Array<any> = [];
  PHCount =0;
  isPHSelectedAlone: boolean;
  searchOnlyForResults: boolean = false;
  noshow: boolean;
  identity: any;

  timers = {
    facets: 0,
    results: 0
  };
  isBottomBarVisible: boolean = false;

  constructor(private _apiService: ApiRequestService,
              private _route: ActivatedRoute,
              private _router : Router,
              private _locationService: LocationService,
              private _permissionService: PermissionService,
              private _calculator : SearchResultTabCalculator,
              private _lateralSearchService: LateralSearchService,
              private _directSearchService: DirectSearchService,
              private searchNavigation: SearchNavigation,
              private _activeSearchNotifier : ActiveSearchNotifier,
              private _sessionStorageService : SessionStorageService,
              private _menuToggle: MenuToggleNotifier,
              private _PhDemonstrationsService: PhDemonstrationsService,
              private _mylistStorageService: MylistStorageService) {
    this.isInitialize = true;

    this.allSelectedRecordUIDList = this._sessionStorageService.retrieve('selectedSearchRecords') || [];
  }

  ngOnInit() {

    IdentityService.identity.subscribe(data => {
      // ensure identity is loaded
      if(!data){
        return false;
      }

      this.identity = data;

      if(!data.isIndividual) {
        this._mylistStorageService.removeStorage();
        this._sessionStorageService.clear("selectedSearchRecords");
        this.allSelectedRecordUIDList = [];
      }

      console.log("ROUTE: ", this._route);
      this.activeSearchChangesSubscription = this._activeSearchNotifier.activeSearchChanges$.subscribe();

      this._route.queryParams.subscribe(params => {

        this.queryParams = _.clone(params);
        this.searchId = params['id'];
        this.currentPage = this.getPage();

        // handle any lateral searches
        this.handleLateralSearch(params);

        // handle direct search
        this.handleDirectSearch(params);

        // get the search to execute
        this.resultsSearch = SearchCollection.get(this.searchId);

        console.log('SEARCH RESULT: ', this.resultsSearch);

        if(this.resultsSearch) {

          this._summarizeInitialization();

        } else { // check query string for 'id' parameter to build search

          // user must have came directly to the results page, so go to a form
          if (!this.searchId) {
            this._router.navigate(['/search']);
          }

          this.getSearchById();
        }
      });


      this._sessionStorageService.observe('selectedSearchRecords').subscribe((list) => {
        if(list && list.length){
          this.allSelectedRecordUIDList = list;
        }else{
          this.allSelectedRecordUIDList = [];
        }
      });
      this._sessionStorageService.observe('currentPageSearchRecords').subscribe((list)=> {
        if(list && list.length)
          this.currentPageSelectedRecordUIDList = list;
        else
          this.currentPageSelectedRecordUIDList = [];
      });

    });

  }

  private _summarizeInitialization(){
    let params = this._locationService.parseQueryParams(this.queryParams);
    this.resultsSearch.query.tab = params.tab;
    this.currentPage = params.page;
    this.resultsSearch.responseParameters.rows = params.rows;
    this.resultsSearch.query.displayOptions.sort = params.sort;

    this.searchOnlyForResults = this.searchNavigation.isResultsOnlySearch();

    SearchCollection.update(this.resultsSearch);

    this.isRequestCompleted = true;
    let self = this;

    async.parallel({
      one: function(callback) {
        self.executeSearch(() => {
          callback();
        });
      },
      two: function(callback) {
        if (!self.searchOnlyForResults) {
          self.executeFacetSearch(function() {
            callback();
          });
        } else {
          callback();
        }
      }
    }, function(err, results) {
      self.isRequestCompleted = false;
      self.loading = false;
    });

  }

  private executeSearch( cb?:Function ){
    this.loading = true;

    if(this.results && this.results.results && this.results.results.result){
      this.results.results.result = {};
    }

    // State whether PH is the only selected DB, for custom content view.
    this.isPHSelectedAlone = (_.difference(this.resultsSearch.query.databases, ['PH']).length === 0);

    // never get facets as part of the results.  They are done in a separate call.
    this.resultsSearch.responseParameters.facet = false;

    // check for search type based it show/hide result page components
    this.isRecommendedSearch = (this.resultsSearch.query.source === 'recommended');

    // make sure display rows on the UI bar match what we are asking for
    this.selectedLimit = this.resultsSearch.responseParameters.rows;

    // calculate page starting location
    this.resultsSearch.responseParameters.start = (this.currentPage-1) * this.selectedLimit;

    // if the user clicks a facet, then we should not log the event and page identifiers for searching again
    this.resultsSearch.metadata.excludeFromLogs = this.searchNavigation.doNotLogResultsSearch();

    //reset current page selected records
    this._sessionStorageService.clear('currentPageSearchRecords');

    let startTime = new Date().getTime();

    this.performSearch(this.resultsSearch, (err, data) => {
      if(data) {

        if(!data.results.result.hasOwnProperty('doc')) {
          data.results.result.doc = [];
        }

        // calculate response time
        let endTime = new Date().getTime();
        this.timers.results = endTime - startTime;

        this.results = this.applyRecordLevelProperties(data);

        this.totalResults = parseInt(this.results.results.result.numFound);
        this.isBottomBarVisible = this.results.results.result.doc.length > 10;

        // update search in our collection
        SearchCollection.update(this.results.search);

        this.resultsSearch = SearchCollection.get(this.results.search.id);

        // update search within criteria
        this.searchwithinSearchQueries = this.parseSearchWithinCriteria(this.resultsSearch.query.criteria);

        if(cb){
          cb();
        }
      }else{
        this.showError = true;
        cb();
      }
    });
  }

  getPHDemonstrations(criteria: any, cb: Function) {

    // if the user does not have PH, then exit out early
    if(!this.hasPHAccess()){
      return cb();
    }

    // initial search, so we'll need to query for the count
    if(this._PhDemonstrationsService.checkInitialSearch(criteria)) {
      this._PhDemonstrationsService.setPHCriteria(criteria);
      let qry = this._apiService.createQuery('search.relatedPHRecords');
      qry.params({ q: criteria });

      this._apiService.execute(qry).subscribe(
        data => {
          this.PHCount = parseInt(data.response.result.numFound);
          this._PhDemonstrationsService.setPHDemonstrationsCount(this.PHCount);
          return cb();
        }, error => {
        return cb();
        }
      );

    } else { // use existing counts
      this.PHCount = this._PhDemonstrationsService.getPHDemonstrationsCount();
      return cb();
    }
  }


  private hasPHAccess() {
    return this._permissionService.mayAccess('PH');
  }

  private executeFacetSearch(cb?:Function){
    this.facetSearch = _.cloneDeep(this.resultsSearch);
    this.facetSearch.responseParameters.facet = true;
    this.facetSearch.responseParameters.results = false;

    // always log facet action
    this.facetSearch.metadata.excludeFromLogs = false;

    let startTime = new Date().getTime();

    this.performSearch(this.facetSearch, (err, data) => {
      if(data){

        // update facets
        if(data.results.facets.length){
          // calculate response time
          let endTime = new Date().getTime();
          this.timers.facets = endTime - startTime;

          this.facets = data.results.facets;
          this.updateFacetDisplay();

          // get PH counts
          this.getPHDemonstrations(data.search.queryParameters.q, () => {
            this.updateTabHitCount(cb);
          });
        } else if(cb){
          cb();
        }

      } else {
        if (cb) {
          cb();
        }

      }

    });
  }

  // execute a specific search
  private performSearch(search: Search, cb: Function) {
    let qry = this._apiService.createQuery('search.results');
    qry.params(search);

    // if the user is returning to this page, via record display page, then don't log this action
    if(this.queryParams.hasOwnProperty('return')){

      delete this.queryParams.return;
      qry.logs({exclude: true});
    }

    this._apiService.execute(qry).subscribe(
      data => {
        cb(null , data);
      },
      error => {
        console.log(error);
        cb(error);
      }
    );
  }

  //iterate search and mark as selected based on the filters
  private updateFacetDisplay(){

    if (this.resultsSearch.query.filter.facets.length) {
      this.markAppliedFacets();
    }

  }

  // calculate hit counts and save within the active search
  private updateTabHitCount(cb?: Function){
    let productInfo = _.find(this.facets, ['name', 'ProductName']);
    let productCountArray = productInfo['values'] || [];

    productCountArray.push({name:'PsycTHERAPY', value : "" + this.PHCount});
    this.tabNumbers = this._calculator.calculateHitCount(this.facetSearch, this.facets, productCountArray);

    if (cb) {
      cb();
    }
  }

  private markAppliedFacets(){
    let filterFieldList = _.map(this.resultsSearch.query.filter.facets, 'field');
    let valueList = _.map(this.resultsSearch.query.filter.facets, 'value');

    for(let i = 0; i < this.facets.length; i++){
      if(filterFieldList.indexOf(this.facets[i].name) > -1 && this.facets[i].values && this.facets[i].values.length){

        for(let j = 0; j < this.facets[i].values.length; j++){
          if(valueList.indexOf(this.facets[i].values[j].name) > -1){

            this.facets[i].values[j].selected = true;
            this.facets[i].isOpen = true;
          }
        }
      }
    }
  }

  private applyRecordLevelProperties(results){
    this.allRecordsOnPageUIDList = [];
    let selectedCount: number = 0;

    if(results.results && results.results.result && results.results.result.hasOwnProperty('doc')&& results.results.result.doc){
      results.results.result.doc.forEach(record => {
        //remove highlights from UID field
        record["UID"] = HighlightService.removeHighlights(record.UID);

        record["HasAccess"] = this._permissionService.mayAccessRecord(record);
        record["DocumentTypeLabel"] = DocumentTypeFormatter.getDocumentTypeLabel(record);

        //handle email option - all record on page
        this.allRecordsOnPageUIDList.push(_.pick(record, ['UID','ProductCode']));

        //handle select checkbox
        if(this.allSelectedRecordUIDList && this.allSelectedRecordUIDList.length){
          record["isSelected"] = _.findIndex(this.allSelectedRecordUIDList, _.pick(record, ['UID','ProductCode'])) != -1;

          if(record["isSelected"]){
            this.currentPageSelectedRecordUIDList.push(_.pick(record, ['UID','ProductCode']));
          }
        }

        if(record["isSelected"]){
          selectedCount ++;
        }
      });

      this.isSelectAll = (selectedCount === results.results.result.doc.length);
      this._updateCurrentPageSelectedRecordLocalStorage();
    }
    return results;
  }

  searchWithinSearch(searchWithinSearchObj){

    if(searchWithinSearchObj.status === 'add') {
      this.resultsSearch.query.criteria.push( new Criteria('AnyField', 'AND', searchWithinSearchObj.term, 'searchWithin') );
    }else if(searchWithinSearchObj.status === 'remove'){
      _.remove(this.resultsSearch.query.criteria, {value: searchWithinSearchObj.term, source: 'searchWithin'});
    }
    this.resultsSearch.query.source = "advanced";
    this.currentPage = 1;
    this.resultsSearch.responseParameters.start = 0;
    this.resultsSearch.responseParameters.rows = this.selectedLimit;

    this.resultsSearch = SearchCollection.cloneWithNewId(this.resultsSearch);
    this._router.navigate(['/search/results'], {queryParams: {id: this.resultsSearch.id}, replaceUrl : true});
  }

  parseSearchWithinCriteria(criteria){
    let searchWithinCriteria = _.filter(criteria, ['source', 'searchWithin']);
    let terms = _.map(searchWithinCriteria, 'value');
    return terms;
  }

  triggerSearchTab(tab) {
    console.log('triggerSearchTab - ' + tab);
    this.currentPage = 1;
    this.resultsSearch.query.tab = tab;
    this.resultsSearch.responseParameters.start = 0;
    this.resultsSearch.responseParameters.rows = this.selectedLimit;

    this.facets = [];

    SearchCollection.update(this.resultsSearch);
    this.performUpdatedSearch();
  }
    //called when facet is updated
  updateSearch($event){
    this.currentPage = 1;
    this.resultsSearch.responseParameters.start = 0;
    this.resultsSearch.responseParameters.rows = this.selectedLimit;

    var index = _.findIndex(this.facetSearch.query.filter.facets, $event);
    if(index == -1){
      this.resultsSearch.query.filter.facets.push($event);
    }else{
      this.resultsSearch.query.filter.facets.splice(index, 1);
    }

    // update search in our collection and generate new id to avoid issues in recent search list
    this.resultsSearch = SearchCollection.cloneWithNewId(this.resultsSearch);

    this.performUpdatedSearch(false, true);
  }

  setSelectAll(selectAllFag){
    if(this.results.results && this.results.results.result && this.results.results.result.doc){
      for(var search of this.results.results.result.doc){
        search["isSelected"] = selectAllFag;

        let newSelectedUID = _.pick(search, ['UID','ProductCode']);
        let index = _.findIndex(this.allSelectedRecordUIDList, newSelectedUID);
        if(selectAllFag && index == -1 ){
          this.allSelectedRecordUIDList.push(newSelectedUID);
          this.currentPageSelectedRecordUIDList.push(newSelectedUID);
        }else if (!selectAllFag && index > -1){
          this.allSelectedRecordUIDList.splice( index, 1);
          let curIndex = _.findIndex(this.currentPageSelectedRecordUIDList , newSelectedUID);
          if(curIndex != -1){
            this.currentPageSelectedRecordUIDList.splice( curIndex, 1);
          }
        }
      }
    }

    this._updateAllSelectedRecordLocalStorage();
    this._updateCurrentPageSelectedRecordLocalStorage();
    this.isSelectAll = selectAllFag;
  }

  identifySelectAll(selected){

    let newRecordUID = _.pick(selected.newlySelected, ['UID','ProductCode']);
    let index = _.findIndex(this.allSelectedRecordUIDList, newRecordUID);
    if(index == -1){
      this.allSelectedRecordUIDList.push(newRecordUID);
      this.currentPageSelectedRecordUIDList.push(newRecordUID);
    }else{
      this.allSelectedRecordUIDList.splice( index, 1);
      let curIndex = _.findIndex(this.currentPageSelectedRecordUIDList , newRecordUID);
      if(curIndex != -1){
        this.currentPageSelectedRecordUIDList.splice( curIndex, 1);
      }
    }

    this._updateAllSelectedRecordLocalStorage();
    this._updateCurrentPageSelectedRecordLocalStorage();
    this.isSelectAll = (selected.selectedRecordList.length === this.results.results.result.doc.length);
  }

  private _updateAllSelectedRecordLocalStorage(){
    this.allSelectedRecordUIDList = _.uniqWith(this.allSelectedRecordUIDList, _.isEqual);
    this._sessionStorageService.store("selectedSearchRecords", this.allSelectedRecordUIDList);
  }

  private _updateCurrentPageSelectedRecordLocalStorage(){
    this.currentPageSelectedRecordUIDList = _.uniqWith(this.currentPageSelectedRecordUIDList, _.isEqual);
    this._sessionStorageService.store("currentPageSearchRecords", this.currentPageSelectedRecordUIDList);
  }

  private performUpdatedSearch(resultsOnly?: boolean, applyingFacet?: boolean) {
    let params = {
      queryParams: {
        id: this.resultsSearch.id,
        tab: this.resultsSearch.query.tab,
        sort: this._locationService.generateSortParamString(this.resultsSearch.query.displayOptions.sort),
        display: this.resultsSearch.responseParameters.rows,
        page: this.currentPage
      },
      resultsOnly: !!resultsOnly,
      doNotLogResults: !!applyingFacet
    };

    this.searchNavigation.gotoResults(this.resultsSearch, params);
  }

  private getSearchById() {

    // if there is no 'id' url parameter, then this is a new search
    if (!this.searchId) {
      return false;
    }

    // get the recent search data and populate form
    let qry = this._apiService.createQuery('recentSearch.get');
    qry.params({id: this.searchId});

    this._apiService.execute(qry).subscribe(
      (data: Search) => {
        if (data.hasOwnProperty('query')) {

          data['query'].tab = this.queryParams['tab'] || 'all';

          // add the search if it doesn't already exist
          SearchCollection.add(data);
          this.resultsSearch = SearchCollection.get(data.id);
          this._activeSearchNotifier.notifySearchChanged(data.id);

          //due to no changes in url this.performUpdatedSearch(); method didn't work. So triggering manually.
          this._summarizeInitialization();
        } else {
          this.isMissing = true;
          this.loading = false;
          this.isRequestCompleted = false;
        }
      });
  }

  /*Pagination changed*/
  getNewPagination(pagination) {
    if (pagination.hasOwnProperty('currentPage')) {
      this.currentPage = pagination.currentPage;
      var start = (this.selectedLimit * (pagination.currentPage-1));
      if (pagination.hasOwnProperty('ev') && pagination.ev) {
        this.resultsSearch.responseParameters.start = start;
        SearchCollection.update(this.resultsSearch);
        this.performUpdatedSearch(true);
      }
    }
  }

  /*apply new records page per limit*/
  changeRecordsPerPage(limit){
    this.selectedLimit = Number(limit);
    this.resultsSearch.responseParameters.rows = this.selectedLimit;
    this.currentPage = Math.ceil((1 + this.resultsSearch.responseParameters.start) / this.selectedLimit);
    SearchCollection.update(this.resultsSearch);
    this.performUpdatedSearch(true);
  }

  /*apply sort option --
  * load the active search before applying the sort option
  * sort option doesn't need to load the facets counts again
  */
  applyNewSortByOption(sortOptions){
    this.resultsSearch.query.displayOptions.sort = sortOptions;
    this.resultsSearch.responseParameters.start = 0;
    this.currentPage = 1;
    SearchCollection.update(this.resultsSearch);
    this.performUpdatedSearch(true);
  }

  private handleLateralSearch(params){
    // exit out of process if there are no lateral searches defined
    if(!params.hasOwnProperty('latSearchType') || !params.hasOwnProperty('term')){
      return false;
    }

    // create a lateral search and mark it as active
    let search = this._lateralSearchService.create(params['latSearchType'], params['term']);
    SearchCollection.update(search);
    this.searchId = search.id;

    console.log("Lateral search: ", search);
  }

  private handleDirectSearch(params) {
    if (params.hasOwnProperty('type') && params['type'] === 'direct' && params.hasOwnProperty('term')) {
      let dbs = '';
      if ((this.identity.isIndividual && this._permissionService.mayAccessAllDefaultProducts()) || this.identity.isOrganization) {
        dbs = params['db'] || '';
      }
      let fields = params['fields'] || '';
      let directSearch = this._directSearchService.create(fields, params['term'], dbs);
      SearchCollection.update(directSearch);
      this.searchId = directSearch.id;

      console.log('DIRECT SEARCH:', directSearch);
    }
  }

  private getPage(){
    if(!this.queryParams.hasOwnProperty('recordId')){
      return 1;
    }

    return Math.ceil(this.queryParams['recordId']/this.selectedLimit)
  }

  editRecentSearch() {
    this.searchNavigation.editSearch(this.resultsSearch);
  }

  toggleFacetsView() {
    this.noshow = !this.noshow;
    this._menuToggle.toggle(this.noshow);
  }

  ngOnDestroy() {
    this.activeSearchChangesSubscription.unsubscribe();
  }
}
