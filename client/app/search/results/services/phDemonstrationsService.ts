import { Injectable } from '@angular/core';

@Injectable()
export class PhDemonstrationsService {
    PHCount: number;
    PHCriteria: any;
    
    constructor() {
    }

    setPHCriteria(criteria: any) {
        if (this.checkInitialSearch(criteria)) {
            this.PHCriteria = criteria;
        }
    }
    checkInitialSearch(criteria: any) {
        return criteria !== this.PHCriteria;
    }

    setPHDemonstrationsCount(count: number) {
        this.PHCount = count;
    }

    getPHDemonstrationsCount() {
        return this.PHCount;
    }

    getPHCriteria() {
        return this.PHCriteria;
    }
}