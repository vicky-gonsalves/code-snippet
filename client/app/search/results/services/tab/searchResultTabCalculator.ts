import {Injectable} from '@angular/core';
import {PermissionService} from '../../../../core/services/identity/permissionService';
import * as _ from "lodash";

export interface IHitcount{
  all: number;
  PI: number;
  PA: number;
  PB: number;
  PE: number;
  PC: number;
  PT: number;
  PH: number;
}

export class HitCount implements IHitcount{
  all: number;
  PI: number;
  PA: number;
  PB: number;
  PE: number;
  PC: number;
  PT: number;
  PH: number;
  selectedDatabases: string[];
  calculated: boolean;

  constructor(){
    this.all = 0 ;
    this.PI= 0;
    this.PA = 0;
    this.PB = 0;
    this.PC= 0;
    this.PE= 0;
    this.PT = 0;
    this.PH = 0;
    this.selectedDatabases = [];
    this.calculated = false;
  }

  public calculateTotalNumberFound(){
    this.all = this.PI + this.PA + this.PB + this.PC + this.PE + this.PT;
  }
}

@Injectable()
export class SearchResultTabCalculator {

  constructor(private _permissionService: PermissionService) {
  }

  calculateHitCount(search, facets, productCountArray?) : HitCount{
    let hitCount = this.normalizeHitCountValues(search, facets, productCountArray);

    if(search.query.source === 'mylistFacet'){
      hitCount = this.calculateMyListHitCount(search, facets, hitCount);
    } else {
      hitCount = this.calculateSearchHitCount(search, facets, hitCount);
    }

    hitCount.calculateTotalNumberFound();
    hitCount.calculated = true;

    return hitCount;
  }

  private calculateSearchHitCount(search, facets, hitCount){
    let dbs = search.query.databases;

    // subtract PA count from PI if tab is going to be shown
    if(dbs.indexOf('PA') > -1){
      hitCount.PI = hitCount.PI - hitCount.PA;
    }

    // substract PB count from PI if tab is going to be shown
    if(dbs.indexOf('PB') > -1) {
      // if the user has book collections without PB, then only use the book collection counts
      if(this.shouldApplyBookCollectionCounts(search.query.tab)){
        hitCount.PB = this.getBookCollectionCounts(facets);
      }
      hitCount.PI = hitCount.PI - hitCount.PB;
    }

    // subtract PC count from PI if tab is going to be shown
    if(dbs.indexOf('PC') > -1) {
      hitCount.PI = hitCount.PI - hitCount.PC;
    }

    return hitCount;
  }


  private calculateMyListHitCount(search, facets, hitCount){
    let dbs = search.query.databases;
    let mayAccess = {
      pa: this._permissionService.mayAccess('PA'),
      pb: this._permissionService.mayAccess('PB'),
      pc: this._permissionService.mayAccess('PC')
    };

    // subtract PA count from PI if tab is going to be shown
    if(dbs.indexOf('PA') > -1){

      // if the user has print subscriptions without PA, then only use the print subscription counts
      if (this.shouldApplyPrintSubscriptionCounts()) {
        hitCount.PA = this.getPrintSubscriptionCounts(facets);
      } else if(!mayAccess.pa) {
        // When the user does not have neither "PA" nor "printSubscription" entitlements, the results should be listed within "PI".
        hitCount.PA = 0;
      }

      hitCount.PI = hitCount.PI - hitCount.PA;
    }

    // subtract PB count from PI if tab is going to be shown
    if(dbs.indexOf('PB') > -1) {
      // if the user has book collections without PB, then only use the book collection counts
      if(this.shouldApplyBookCollectionCounts(search.query.tab)){
        hitCount.PB = this.getBookCollectionCounts(facets);
      } else if(!mayAccess.pb) {
        // When the user does not have neither "PB" nor "bookCollection" entitlements, the results should be listed within "PI".
        hitCount.PB = 0;
      }

      hitCount.PI = hitCount.PI - hitCount.PB;
    }

    // subtract PC count from PI if tab is going to be shown.  For My List, tab is only shown if user owns PC.
    if(dbs.indexOf('PC') > -1) {
      if(mayAccess.pc){
        hitCount.PI = hitCount.PI - hitCount.PC;
      } else {
        hitCount.PC = 0;
      }
    }


    console.log('HitCount: ', hitCount);
    return hitCount;
  }

  private normalizeHitCountValues(search, facets, productCountArray?) {
    let hitCount = new HitCount();
    let productInfo = _.find(facets, ['name', 'ProductName']);
    productCountArray = productCountArray || productInfo['values'];

    let dbs = search.query.databases;
    let countValue = 0;

    if(!productCountArray){
      return hitCount;
    }

    for (let j = 0; j < productCountArray.length; j++) {
      countValue = parseInt(productCountArray[j].value);

      switch (productCountArray[j].name) {
        case 'PsycINFO':
          hitCount.PI = countValue;
          break;

        // only count PA, PB, and PC if the user is explicitly searches on the databases
        case 'PsycARTICLES':
          hitCount.PA = (dbs.indexOf('PA') >= 0) ? countValue : 0;
          break;
        case 'PsycBOOKS':
          hitCount.PB = (dbs.indexOf('PB') >= 0) ? countValue : 0;
          break;
        case 'PsycCRITIQUES':
          hitCount.PC = (dbs.indexOf('PC') >= 0) ? countValue : 0;
          break;

        case 'PsycEXTRA':
          hitCount.PE = countValue;
          break;
        case 'PsycTESTS':
          hitCount.PT = countValue;
          break;
        case 'PsycTHERAPY':
          hitCount.PH = countValue;
          break;
      }
    }

    // preserve databases search for easy lookup
    hitCount.selectedDatabases = dbs;

    return hitCount;
  }

  private getPrintSubscriptionCounts(facets){
    let printSubscriptionCodes = this._permissionService.getProductsCodesByGroup('printSubscription');
    let counts = this.getCollectionHitCounts('PAJournalCode', facets, printSubscriptionCodes);

    return counts;
  }

  private getBookCollectionCounts(facets){
    let bookCollectionCodes = this._permissionService.getProductsCodesByGroup('bookCollection');
    let counts = this.getCollectionHitCounts('BookCollection', facets, bookCollectionCodes);

    return counts;
  }

  private shouldApplyPrintSubscriptionCounts(){
    return (!this._permissionService.mayAccess('PA') && this._permissionService.hasGroupAccess('printSubscription'));
  }

  private shouldApplyBookCollectionCounts(tab){
    return (!this._permissionService.mayAccess('PB') && this._permissionService.hasGroupAccess('bookCollection') && tab !== 'PT');
  }

  private getCollectionHitCounts(collectionName, facets, collectionCodes) {
    let result = 0;
    let productInfo = _.find(facets, ['name', collectionName]);

    if (!productInfo || !productInfo.hasOwnProperty('values')) {
      return result;
    }


    let products = productInfo['values'] || [];
    let codes = [];

    if (collectionName === 'PAJournalCode') {
      codes = collectionCodes;
    } else if (collectionName === 'BookCollection') {
      // remove PBC prefix so that we can compare facets against codes
      collectionCodes.forEach(code => {
        codes.push( code.replace('PBC', '') );
      });
    }

    products.forEach( product => {
      if (codes.indexOf(product.name.toUpperCase()) >= 0) {
        result = result + parseInt(product.value);
      }
    });

    return result;
  }

}
