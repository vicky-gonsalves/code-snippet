import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import {Observer} from 'rxjs/Observer';

export class ActiveSearchNotifier{

  private _isChangedId: string;
  activeSearchChanges$: Observable<any>;
  private _activeSearchChangeObserver: Observer<any>;

  constructor(){
    this.activeSearchChanges$ = new Observable(observer => this._activeSearchChangeObserver = observer).share();
  }

  notifySearchChanged(id){
    this._isChangedId = id;
    this._activeSearchChangeObserver.next(id);
  }
}
