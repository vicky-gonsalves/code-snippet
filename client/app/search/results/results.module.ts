import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { SearchCommonModule } from '../searchcommon.module';

//import {FriendlyQuery} from "./components/friendlyQuery/friendlyQuery";
import {SearchActionBar} from "./components/searchActionBar/searchActionBar";
import {SearchFilterBox} from "./components/searchFilterBox/searchFilterBox";
import {SearchResultsButtonBar} from './components/buttonBar/searchResultsButtonBar';
import {SearchResults} from "./results";
import {SearchResultsNav} from "./components/searchResultsNav/searchResultsNav";
import {SearchResultsPagination} from "./components/searchResultsPagination/searchResultsPagination";
import {SearchResultsList} from "./components/searchResultsList/searchResultsList";
import {SearchResultReference} from './components/searchResultReference/searchResultReference';
import {SearchResultsToolsBar} from "./components/searchResultsToolsBar/searchResultsToolsBar";
import {SearchTabs} from "./components/searchTabs/searchTabs";
import {SearchWithinResultsBox} from "./components/searchWithinResultsBox/searchWithinResultsBox";
import {PHResults} from './components/phResults/phResults'
import {CitedrefsResultsList} from './components/citedrefsResultsList/citedrefsResultsList';
import {CitedRefsResults} from './citedRefsResults';
import {SaveSearchModal} from './components/saveSearchModal/saveSearchModal';
import {AddToListConfirmationModal} from './components/addToListConfirmationModal/addToListConfirmationModal';
import {AddTagNotes} from '../myList/components/addTagNote/addTagNote';

// Services
import {LateralSearchService} from "../services/search/lateralSearchService";
import {SearchCollection} from '../services/search/searchCollection';
import {SearchNavigation} from '../services/searchNavigation';
import {SearchResultTabCalculator} from "./services/tab/searchResultTabCalculator";
import {ActiveSearchNotifier} from './services/activeSearchNotifier';
import {PhDemonstrationsService} from './services/phDemonstrationsService';


const declarations = [
  //FriendlyQuery,
  SearchActionBar,
  SearchFilterBox,
  SearchResultsButtonBar,
  SearchResults,
  SearchResultsNav,
  SearchResultsPagination,
  SearchResultsList,
  SearchResultReference,
  SearchResultsToolsBar,
  SearchTabs,
  SearchWithinResultsBox,
  CitedrefsResultsList,
  CitedRefsResults,
  SaveSearchModal,
  AddToListConfirmationModal,
  PHResults,
  AddTagNotes
];


import { routing } from './results.routes';
/*

@NgModule({
  imports: [SharedModule, SearchCommonModule, routing],
  declarations: [
    ...declarations
  ],
  providers: [
    LateralSearchService,
    SearchCollection,
    SearchNavigation,
    SearchResultTabCalculator,
    ActiveSearchNotifier,
    PhDemonstrationsService
  ]
})
*/
export class SearchResultModule {}


