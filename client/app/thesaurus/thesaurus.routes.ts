import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Thesaurus} from './thesaurus';
import {ThesaurusGuardService} from '../core/services/resourceGuardService';

const routes: Routes = [
  { path: '', component: Thesaurus, canActivate: [ThesaurusGuardService] },
  { path: 'item', component: Thesaurus, canActivate: [ThesaurusGuardService]}
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
