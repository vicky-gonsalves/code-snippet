import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';

import {Thesaurus} from './thesaurus';
import {SearchBoxThesaurus} from './components/searchBoxThesaurus/searchBoxThesaurus';
import {SearchBoxService} from './services/searchBoxService';
import {DecodeCurlyBraces} from './services/decodeCurlyBraces';
import {TotalNumberOfTerms} from './services/totalNumberOfTerms';

import { routing } from './thesaurus.routes';

@NgModule({
  imports: [SharedModule, routing],
  declarations: [Thesaurus, SearchBoxThesaurus],
  providers: [DecodeCurlyBraces, SearchBoxService, TotalNumberOfTerms]
})
export class ThesaurusModule {}
