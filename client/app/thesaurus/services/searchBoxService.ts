import { Injectable } from '@angular/core';
import {SessionStorageService} from "ng2-webstorage";

@Injectable()
export class SearchBoxService {
  private _terms: any[];

  constructor(private _storage: SessionStorageService) {
    let terms = this._storage.retrieve('thesaurusSelectedTerms');
    if(terms && terms.length) {
      this._terms = terms;
    } else {
      this._terms = [];
    }
  }

  get terms() {
    return this._terms;
  }

  exists(term) {
    return this._terms.indexOf(term) >= 0;
  }

  addTerm(term) {
    if (term && term.length && !this.exists(term)) {
      this._terms.push(term);
      this.save();
    }
  }

  addTerms(...terms) {
    if(terms && terms.length) {
      for(let term of terms) {
        this.addTerm(term);
      }
    }
  }

  removeTerm(term) {
    if (this.exists(term)) {
      this._terms.splice(this._terms.indexOf(term), 1);
      this.save();
    }
  }

  clearAllTerms() {
    this._terms.length = 0;
    this.save();
  }

  private save() {
    this._storage.store('thesaurusSelectedTerms', this._terms);
  }
}
