import {Injectable} from '@angular/core';

@Injectable()
export class DecodeCurlyBraces {
  constructor() {
  }

  public getArray(str: string): Array<string> {
    let results = [], re = /{([^}]+)}/g, text;
    while(text = re.exec(str)) {
      results.push(text[1]);
    }
    return results;
  }
}
