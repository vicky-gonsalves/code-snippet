import {Injectable} from '@angular/core';
import {SessionStorageService} from "ng2-webstorage";

@Injectable()
export class TotalNumberOfTerms {
  count: number;
  constructor(private _storage: SessionStorageService) {
  }

  setTotalTerms(count) {
    this.count = count;
    this._storage.store('thesaurusNumberOfTerms', this.count);
  }

  getTotalTerms() {
    if(!this.count) {
      this.count = this._storage.retrieve('thesaurusNumberOfTerms');
    }
    return this.count;
  }
}
