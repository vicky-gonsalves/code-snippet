export interface ILookupParams {
  limit?: number,
  offset?: number,
  prefix?: string,
  term?: string,
  code?: string
}
