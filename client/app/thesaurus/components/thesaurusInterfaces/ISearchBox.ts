export interface ISearchBox {
  label?: string,
  operators?: string[]
}
