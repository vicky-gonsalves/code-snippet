export interface ITerm {
  Code: string,
  Introduced: string,
  Term: string,
  isSelected?: boolean
}

export interface INotation {
  ScopeNote: string
}

export interface IThesaurusItem {
  BroaderTermList?: ITerm[],
  Code?: string,
  IndexTerm?: string,
  Introduced?: string,
  IsNarrowerTermsPresent?: string,
  MainTermCount?: string,
  NotationList?: INotation[],
  NarrowerTermList?: ITerm[],
  RelationList?: ITerm[],
  UsedFor?: ITerm[],
  Use?: ITerm[],
  isOpen?: boolean
  isSelected?: boolean,
  isNarrowerSelected?: boolean
}
