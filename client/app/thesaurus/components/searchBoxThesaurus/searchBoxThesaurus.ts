import {Component, Input, Output, EventEmitter} from "@angular/core";
import {SearchBoxService} from '../../services/searchBoxService';
import {Locale, LocalizationService} from "angular2localization";

@Component({
  selector: 'search-box-thesaurus',
  templateUrl: './searchBoxThesaurus.html',
  styleUrls: ['./searchBoxThesaurus.less']
})

export class SearchBoxThesaurus extends Locale{
  @Input() lookupOptions: any;
  @Input() contentLoaded: boolean;
  @Output() removeItems = new EventEmitter();
  @Output() addToSearch = new EventEmitter();

  operator: string;

  constructor(private _searchBoxService: SearchBoxService, public localization: LocalizationService) {
    super(null, localization);
  }

  ngOnInit() {
    if(this.lookupOptions && this.lookupOptions.hasOwnProperty('operators') && this.lookupOptions.operators && this.lookupOptions.operators.length) {
      this.operator = this.lookupOptions.operators[0];
    }
  }

  removeTerm(term: string): void {
    this.removeItems.emit([term]);
    this._searchBoxService.removeTerm(term);
  }

  public clearAll(): void {
    this.removeItems.emit(this._searchBoxService.terms);
    this._searchBoxService.clearAllTerms();
  }

  public addToSearchList(): void {
    this.addToSearch.emit({terms: this._searchBoxService.terms, operator: this.operator});
  }

  private getSelectedTerms() {
    return this._searchBoxService.terms;
  }
}
