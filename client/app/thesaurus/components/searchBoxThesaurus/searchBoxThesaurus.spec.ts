import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {NO_ERRORS_SCHEMA, DebugElement} from '@angular/core';
import {addMatchers, click} from '../../testing';

import {SearchBoxThesaurus} from "./searchBoxThesaurus";

let testHost: TestHostComponent;
let fixture: ComponentFixture<TestHostComponent>;
let searchEl: DebugElement;

describe('SearchBoxThesaurus: When there is no data', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [
        SearchBoxThesaurus, TestHostComponent
      ],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA]
    });
  });

  beforeEach(() => {
    // create TestHostComponent instead of SearchBoxThesaurus
    fixture = TestBed.createComponent(TestHostComponent);
    testHost = fixture.componentInstance;
    searchEl = fixture.debugElement.query(By.css('.SearchBoxThesaurus')); // find SearchBoxThesaurus
    fixture.detectChanges(); // trigger initial data binding
  });

  performTests();
});

function performTests() {
  it('should display label', () => {
    let h2Elem = searchEl.nativeElement.children[0].children[0];
    let expectedLabel = testHost.searchBoxOptions.label;
    expect(h2Elem.textContent).toContain(expectedLabel);
  });

  it('should add to search list', () => {
    let btnElem = fixture.debugElement.query(By.css('.btn'));
    click(btnElem);
    expect(testHost.selection).toEqual({terms: testHost.selectedTerms, operator: 'OR'});
  });

  it('should clear terms', () => {
    let btnElem = fixture.debugElement.query(By.css('.greybg'));
    click(btnElem);
    expect(testHost.toRemove).toEqual(testHost.selectedTerms);
  });
}

////// Test Host Component //////
import {Component} from '@angular/core';

@Component({
  template: `<search-box-thesaurus [lookupOptions]="searchBoxOptions" (addToSearch)="getAddToSearch($event)" (removeItems)="addRemoveItems($event, false)" [contentLoaded]="contentLoaded" [(selectedTerms)]="selectedTerms"></search-box-thesaurus>`
})
class TestHostComponent {
  searchBoxOptions = {label: 'Terms', operators: ['OR']};
  contentLoaded = true;
  selectedTerms = ['some', 'term'];
  selection = {};
  toRemove = [];

  getAddToSearch(selectedItems: any) {
    this.selection = selectedItems;
  }

  addRemoveItems(terms: string[], selected: boolean): void {
    this.toRemove = terms;
  }
}
