
/*
import {ComponentFixture, TestBed, inject, async} from '@angular/core/testing';
import {MockBackend, MockConnection} from "@angular/http/testing";
import {HttpModule, Http, XHRBackend, Response, ResponseOptions} from '@angular/http';
import {NO_ERRORS_SCHEMA, DebugElement} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {SessionStorageService} from "ng2-webstorage";

import {Thesaurus} from "./thesaurus";
import {ApiRequestService, Query} from "../core/services/apiRequestService";
//import {SessionConfirmationNotifier} from "../shared/services/session/sessionConfirmationNotifier";
import {DatabaseSelectorService} from "../shared/dbSelector/databaseSelectorService";
import {PermissionService} from "../core/services/identity/permissionService";
import {AllDatabases} from "../shared/dbSelector/allDatabases";
import {DecodeCurlyBraces} from "./services/decodeCurlyBraces";

let comp: Thesaurus;
let fixture: ComponentFixture<Thesaurus>;
let _apiService: ApiRequestService; // the TestBed injected service
let de: DebugElement;  // the DebugElement with the welcome message
let el: HTMLElement;

//@formatter:off
const makeResponseData = {"response":{"search":"http://marklogic.com/appservices/search", "xsi":"http://www.w3.org/2001/XMLSchema-instance", "xh":"http://www.w3.org/1999/xhtml", "responseHeader":{"status":"0", "QTime":"1.499207", "params":{"service":"thesaurus", "function":"/query", "q":"TermBeginsWith:A*", "start":"0", "rows":"16", "format":"json", "debug":"false", "results":"true", "facet":"false", "facet.offset":"0", "facet.limit":"11", "query":"false", "hl":"false", "child":"false", "total":"0"}}, "result":{"start":"0", "rows":"16", "numFound":"9345", "doc":[{"IndexTerm":"Abandonment", "Code":"00005", "Introduced":"1997", "NotationList":[{"ScopeNote":"Loneliness, anxiety, and emotional and psychological loss of support resulting from desertion or neglect. Used for human populations."}], "RelationList":[{"Term":"Attachment Behavior", "Code":"04355", "Introduced":"1985"}, {"Term":"Child Abuse", "Code":"08650", "Introduced":"1971"}, {"Term":"Child Neglect", "Code":"08695", "Introduced":"1988"}, {"Term":"Dependency (Personality)", "Code":"13620", "Introduced":"1967"}, {"Term":"Emotional States", "Code":"16900", "Introduced":"1973"}, {"Term":"Loneliness", "Code":"28730", "Introduced":"1973"}, {"Term":"Relationship Termination", "Code":"43680", "Introduced":"1997"}, {"Term":"Separation Anxiety", "Code":"46660", "Introduced":"1973"}, {"Term":"Separation Anxiety Disorder", "Code":"61681", "Introduced":"2015"}, {"Term":"Separation Reactions", "Code":"46670", "Introduced":"1997"}], "UsedFor":[{"Term":"Desertion", "Code":"13735", "Introduced":"1997"}], "IsNarrowerTermsPresent":"false", "MainTermCount":"494"}, {"IndexTerm":"Abdomen", "Code":"00010", "Introduced":"1973", "BroaderTermList":[{"Term":"Anatomy", "Code":"02420", "Introduced":"1967"}], "IsNarrowerTermsPresent":"false", "MainTermCount":"567"}, {"IndexTerm":"Abdominal Wall", "Code":"00020", "Introduced":"1973", "BroaderTermList":[{"Term":"Muscles", "Code":"32580", "Introduced":"1967"}], "IsNarrowerTermsPresent":"false", "MainTermCount":"93"}, {"IndexTerm":"Abducens Nerve", "Code":"00030", "Introduced":"1973", "NotationList":[{"ScopeNote":"Cranial motor nerve controlling the lateral rectus muscle of the eye."}], "BroaderTermList":[{"Term":"Cranial Nerves", "Code":"12330", "Introduced":"1973"}], "UsedFor":[{"Term":"Nerve (Abducens)", "Code":"33400", "Introduced":"1973"}], "IsNarrowerTermsPresent":"false", "MainTermCount":"77"}, {"IndexTerm":"Ability", "Code":"00070", "Introduced":"1967", "NotationList":[{"ScopeNote":"Conceptually broad term referring to the skills, talents, or qualities that enable one to perform a task. Use a more specific term if possible."}], "RelationList":[{"Term":"Ability Grouping", "Code":"00040", "Introduced":"1973"}, {"Term":"Ability Level", "Code":"00050", "Introduced":"1978"}, {"Term":"Achievement Potential", "Code":"00510", "Introduced":"1973"}, {"Term":"Competence", "Code":"10747", "Introduced":"1982"}, {"Term":"Creativity", "Code":"12380", "Introduced":"1967"}, {"Term":"Gifted", "Code":"21060", "Introduced":"1967"}, {"Term":"Intelligence", "Code":"25900", "Introduced":"1967"}, {"Term":"Performance", "Code":"37525", "Introduced":"1967"}, {"Term":"Phrenology", "Code":"60826", "Introduced":"2010"}], "UsedFor":[{"Term":"Aptitude", "Code":"03610", "Introduced":"1973"}, {"Term":"Skills", "Code":"47710", "Introduced":"1973"}, {"Term":"Talent", "Code":"51420", "Introduced":"1973"}], "NarrowerTermList":[{"Term":"Academic Aptitude", "Code":"00220", "Introduced":"1973"}, {"Term":"Cognitive Ability", "Code":"10050", "Introduced":"1973"}, {"Term":"Communication Skills", "Code":"10540", "Introduced":"1973"}, {"Term":"Employee Skills", "Code":"17130", "Introduced":"1973"}, {"Term":"Learning Ability", "Code":"27960", "Introduced":"1973"}, {"Term":"Nonverbal Ability", "Code":"34357", "Introduced":"1988"}, {"Term":"Reading Skills", "Code":"43170", "Introduced":"1973"}, {"Term":"Self-Care Skills", "Code":"46215", "Introduced":"1978"}, {"Term":"Social Skills", "Code":"48395", "Introduced":"1978"}], "IsNarrowerTermsPresent":"tue", "MainTermCount":"9368"}, {"IndexTerm":"Ability Grouping", "Code":"00040", "Introduced":"1973", "NotationList":[{"ScopeNote":"Grouping or selection of individuals for instructional or other purposes based on differences in ability or achievement."}], "RelationList":[{"Term":"Ability", "Code":"00070", "Introduced":"1967"}, {"Term":"Ability Level", "Code":"00050", "Introduced":"1978"}, {"Term":"Academic Aptitude", "Code":"00220", "Introduced":"1973"}, {"Term":"Education", "Code":"16000", "Introduced":"1967"}, {"Term":"Educational Placement", "Code":"16155", "Introduced":"1978"}, {"Term":"Grade Level", "Code":"21445", "Introduced":"1994"}, {"Term":"Special Education", "Code":"48930", "Introduced":"1967"}], "IsNarrowerTermsPresent":"false", "MainTermCount":"604"}, {"IndexTerm":"Ability Level", "Code":"00050", "Introduced":"1978", "NotationList":[{"ScopeNote":"Demonstrated level of performance. Used in academic, cognitive, perceptual, or occupational contexts, as well as an indicator of a patient's level of functioning."}], "RelationList":[{"Term":"Ability", "Code":"00070", "Introduced":"1967"}, {"Term":"Ability Grouping", "Code":"00040", "Introduced":"1973"}, {"Term":"Activities of Daily Living", "Code":"00655", "Introduced":"1991"}, {"Term":"Adaptive Testing", "Code":"00795", "Introduced":"1985"}, {"Term":"Flow (Consciousness State)", "Code":"60056", "Introduced":"2008"}], "UsedFor":[{"Term":"Functional Status", "Code":"20497", "Introduced":"2001"}, {"Term":"Level of Functioning", "Code":"28285", "Introduced":"2001"}], "IsNarrowerTermsPresent":"false", "MainTermCount":"6189"}, {"IndexTerm":"Ability Tests", "Code":"00060", "Introduced":"1973", "Use":[{"Term":"Aptitude Measures", "Code":"03630", "Introduced":"1967"}], "IsNarrowerTermsPresent":"false", "MainTermCount":""}, {"IndexTerm":"Ablation", "Code":"00080", "Introduced":"1973", "Use":[{"Term":"Lesions", "Code":"28200", "Introduced":"1967"}], "IsNarrowerTermsPresent":"false", "MainTermCount":""}, {"IndexTerm":"Abnormal Psychology", "Code":"00083", "Introduced":"2003", "NotationList":[{"ScopeNote":"Branch of psychology concerned with the study of mental disorders."}], "RelationList":[{"Term":"Mental Disorders", "Code":"30740", "Introduced":"1967"}, {"Term":"Psychopathology", "Code":"41820", "Introduced":"1967"}], "BroaderTermList":[{"Term":"Psychology", "Code":"41760", "Introduced":"1967"}], "IsNarrowerTermsPresent":"false", "MainTermCount":"1421"}, {"IndexTerm":"Aboriginal Populations", "Code":"00085", "Introduced":"2001", "Use":[{"Term":"Indigenous Populations", "Code":"24845", "Introduced":"2001"}], "IsNarrowerTermsPresent":"false", "MainTermCount":""}, {"IndexTerm":"Abortion (Attitudes Toward)", "Code":"60000", "Introduced":"2008", "NotationList":[{"ScopeNote":"Beliefs or perceptions held by individuals or organizations pertaining to the necessity, morality, and importance of induced abortions."}], "RelationList":[{"Term":"Birth Control", "Code":"06060", "Introduced":"1971"}, {"Term":"Family Planning Attitudes", "Code":"19240", "Introduced":"1973"}, {"Term":"Induced Abortion", "Code":"24950", "Introduced":"1971"}], "BroaderTermList":[{"Term":"Attitudes", "Code":"04500", "Introduced":"1967"}], "UsedFor":[{"Term":"Pro Choice", "Code":"60218", "Introduced":"2008"}, {"Term":"Pro Life", "Code":"60219", "Introduced":"2008"}], "IsNarrowerTermsPresent":"false", "MainTermCount":"472"}, {"IndexTerm":"Abortion (Induced)", "Code":"00090", "Introduced":"1973", "Use":[{"Term":"Induced Abortion", "Code":"24950", "Introduced":"1971"}], "IsNarrowerTermsPresent":"false", "MainTermCount":""}, {"IndexTerm":"Abortion (Spontaneous)", "Code":"00100", "Introduced":"1973", "Use":[{"Term":"Spontaneous Abortion", "Code":"49350", "Introduced":"1971"}], "IsNarrowerTermsPresent":"false", "MainTermCount":""}, {"IndexTerm":"Abortion Laws", "Code":"00110", "Introduced":"1973", "RelationList":[{"Term":"Induced Abortion", "Code":"24950", "Introduced":"1971"}], "BroaderTermList":[{"Term":"Laws", "Code":"27880", "Introduced":"1967"}], "IsNarrowerTermsPresent":"false", "MainTermCount":"342"}, {"IndexTerm":"ABPP", "Code":"60845", "Introduced":"2010", "Use":[{"Term":"Amyloid Precursor Protein", "Code":"60754", "Introduced":"2010"}], "IsNarrowerTermsPresent":"false", "MainTermCount":""}]}}};
//@formatter:on

describe('Thesaurus: When there is no data', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule, RouterTestingModule],
      declarations: [
        Thesaurus
      ],
      providers: [
        ApiRequestService,
        //SessionConfirmationNotifier,
        DatabaseSelectorService,
        PermissionService,
        AllDatabases,
        SessionStorageService,
        DecodeCurlyBraces,
        {provide: XHRBackend, useClass: MockBackend}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });
    fixture = TestBed.createComponent(Thesaurus);
    comp = fixture.componentInstance;

    // ApiRequestService from the root injector
    _apiService = TestBed.get(ApiRequestService);
  });

  performVariableTests();
  performPreTests();

  describe('Thesaurus: When it has response data', () => {
    let backend: MockBackend;
    let _apiService: ApiRequestService;
    let fakeData: any;
    let response: Response;

    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      fakeData = null;
     // _apiService = new ApiRequestService(http, null);
     // fakeData = makeResponseData;
      let options = new ResponseOptions({status: 200, body: {data: fakeData}});
      response = new Response(options);
    }));


    it('should have expected fake data (then)', async(inject([], () => {
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));
      let query: Query = _apiService.createQuery('thesaurus.getTermBeginsWith');
      _apiService.execute(query)
        .toPromise()
        // .then(() => Promise.reject('deliberate'))
        .then(data => {
          expect(data.data.response.result.doc.length).toBe(fakeData.response.result.doc.length, 'should have expected no. of data');
        });
    })));

    it('should have expected fake data (Observable.do)', async(inject([], () => {
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));
      let query: Query = _apiService.createQuery('thesaurus.getTermBeginsWith');
      _apiService.execute(query)
        .toPromise()
        .then(data => {
          expect(data.data.response.result.doc.length).toBe(fakeData.response.result.doc.length, 'should have expected no. of data');
        });
    })));

    it('should be OK returning no data', async(inject([], () => {
      let resp = new Response(new ResponseOptions({status: 200, body: {data: []}}));
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));
      let query: Query = _apiService.createQuery('thesaurus.getTermBeginsWith');
      _apiService.execute(query)
        .toPromise()
        .then(data => {
          expect(data.data.length).toBe(0, 'should have no data');
        });
    })));

    it('should have isBottomPaginationBarVisible true if data length is greater than bottomPaginationThreshold', async(inject([], () => {
      backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));
      let query: Query = _apiService.createQuery('thesaurus.getTermBeginsWith');
      _apiService.execute(query)
        .toPromise()
        .then(data => {
          if(data.data.response.result.doc.length > comp.bottomPaginationThreshold) {
            expect(comp.isBottomPaginationBarVisible).toBe(true);
          } else {
            expect(comp.isBottomPaginationBarVisible).toBe(false);
          }
        });
    })));
  })
});

function performVariableTests() {
  it('should have selectAlpha to be A', () => {
    expect(comp.selectAlpha === 'A').toBe(true);
  });

  it('should have isSelectAll to be false', () => {
    expect(comp.isSelectAll === false).toBe(true);
  });

  it('should have disabledFormControls to be true', () => {
    expect(comp.disabledFormControls === true).toBe(true);
  });

  it('should have contentLoaded to be false', () => {
    expect(comp.contentLoaded === false).toBe(true);
  });

  it('should have loading to be false', () => {
    expect(comp.loading === false).toBe(true);
  });

  it('should have term to be empty string', () => {
    expect(comp.term === '').toBe(true);
  });

  it('should have termCode to be empty string', () => {
    expect(comp.termCode === '').toBe(true);
  });

  it('should have selectedPage to be 1', () => {
    expect(comp.selectedPage === 1).toBe(true);
  });

  it('should have selectedPage to be 1', () => {
    expect(comp.selectedPage === 1).toBe(true);
  });

  it('should have selectedLimit to be 15', () => {
    expect(comp.selectedLimit === 15).toBe(true);
  });

  it('should have prependText to be empty string', () => {
    expect(comp.prependText === '').toBe(true);
  });

  it('should have appendText to be empty string', () => {
    expect(comp.appendText === '').toBe(true);
  });

  it('should have bottomPaginationThreshold to be 10', () => {
    expect(comp.bottomPaginationThreshold === 10).toBe(true);
  });

  it('should have isBottomPaginationBarVisible to be true', () => {
    expect(comp.isBottomPaginationBarVisible === true).toBe(true);
  });
}

function performPreTests() {
  it('can instantiate service when inject service', inject([ApiRequestService], (service: ApiRequestService) => {
    expect(service instanceof ApiRequestService).toBe(true);
  }));

  it('can instantiate service with "new"', inject([Http], (http: Http) => {
    //expect(http).not.toBeNull('http should be provided');
    //let service = new ApiRequestService(http, null);
    //expect(service instanceof ApiRequestService).toBe(true, 'new service should be ok');
  }));

  it('can provide the mockBackend as XHRBackend', inject([XHRBackend], (backend: MockBackend) => {
    expect(backend).not.toBeNull('backend should be provided');
  }));

  it('should call getThesaurusTerms when initialized', ()=> {
    spyOn(comp, 'getThesaurusTerms');
    fixture.detectChanges();
    expect(comp.getThesaurusTerms).toHaveBeenCalled();
  });

  it('should call api thesaurus.getTermBeginsWith', ()=> {
    comp.selectAlpha = 'A';
    comp.term = '';
    comp.termCode = '';
    fixture.detectChanges();
    expect(comp.getAPI()).toBe('thesaurus.getTermBeginsWith');
  });

  it('should call api thesaurus.getTermContains', ()=> {
    comp.term = 'abs';
    comp.selectAlpha = '';
    comp.termCode = '';
    fixture.detectChanges();
    expect(comp.getAPI()).toBe('thesaurus.getTermContains');
  });

  it('should call api thesaurus.getTermByCode', ()=> {
    comp.termCode = '12345';
    comp.term = '';
    comp.selectAlpha = '';
    fixture.detectChanges();
    expect(comp.getAPI()).toBe('thesaurus.getTermByCode');
  });

  it('should have query params wih prefix', ()=> {
    comp.selectAlpha = 'A';
    comp.term = '';
    comp.termCode = '';
    let params = {limit: 16, offset: 0, prefix: 'A', databases: ['PI', 'PA', 'PB', 'PC', 'PE']};
    fixture.detectChanges();
    expect(comp.getQueryParams()).toEqual(params);
  });

  it('should have query params with term', ()=> {
    comp.term = 'abs';
    comp.selectAlpha = '';
    comp.termCode = '';
    let params = {limit: 16, offset: 0, term: 'abs', databases: ['PI', 'PA', 'PB', 'PC', 'PE']};
    fixture.detectChanges();
    expect(comp.getQueryParams()).toEqual(params);
  });

  it('should have query params with term code', ()=> {
    comp.termCode = '12345';
    comp.term = '';
    comp.selectAlpha = '';
    let params = {limit: 3, offset: 0, code: '12345', databases: ['PI', 'PA', 'PB', 'PC', 'PE']};
    fixture.detectChanges();
    expect(comp.getQueryParams()).toEqual(params);
  });
}
*/
