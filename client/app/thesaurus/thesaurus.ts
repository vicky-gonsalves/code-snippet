import {Component} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {Location} from '@angular/common';
import {SessionStorageService} from "ng2-webstorage";
import {ApiRequestService, Query} from "../core/services/apiRequestService";
import {HighlightService} from "../shared/highlight/highlightService";
import {IThesaurusItem} from "./components/thesaurusInterfaces/IThesaurus";
import {ILookupParams} from "./components/thesaurusInterfaces/ILookupParams";
import {ISearchBox} from "./components/thesaurusInterfaces/ISearchBox";
import {DatabaseSelectorService} from "../shared/dbSelector/databaseSelectorService";
import {SearchCollection} from "../search/services/search/searchCollection";
import {Criteria} from "../search/services/search/query/components/criteria";
import {PsycNETQuery} from "../search/services/search/query/psycNETQuery";
import {Search} from "../search/services/search/search";
import {IResponseParameters} from "../search/services/search/query/components/interfaces";
import {DecodeCurlyBraces} from "./services/decodeCurlyBraces";
import {SearchBoxService} from './services/searchBoxService';
import {TotalNumberOfTerms} from './services/totalNumberOfTerms';
import {LocalizationService, Locale} from "angular2localization";
import {Capitalize} from "../shared/capitalize/capitalize";
import {CapitalizeWordHTMLContents}  from "../shared/pipes/capitalizeWordHTMLContents";
import {IdentityService} from '../core/services/identity/identityService';
import {Identity} from '../core/services/identity/identity';

@Component({
  selector: 'thesaurus',
  templateUrl: './thesaurus.html',
  styleUrls: ['./thesaurus.less']
})
export class Thesaurus extends Locale{
  identity: Identity;
  searchBoxOptions: ISearchBox;
  thesaurusList: IThesaurusItem[];
  prevTerm: IThesaurusItem;
  nextTerm: IThesaurusItem;
  selectedTerms: string[] = [];
  productCodes: string[];
  selectAlpha = 'A';
  totalItems: number;
  origTotalItems: number;
  showPaginationPages = false;
  isSelectAll: boolean = false;
  disabledFormControls: boolean = true;
  contentLoaded: boolean = false;
  loading: boolean = false;
  retrieved: boolean = false;
  term = '';
  termCode = '';
  filterString = '';
  selectedLimit = 15;
  selectedPage = 1;
  prependText = '';
  appendText = '';

  //hide bottom pagination if less number of result's count
  bottomPaginationThreshold: number = 10;
  isBottomPaginationBarVisible: boolean = true;

  constructor(private _apiService: ApiRequestService,
              private _databaseSelectorService: DatabaseSelectorService,
              private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _location: Location,
              private _sessionStorageService: SessionStorageService,
              private _searchBoxService: SearchBoxService,
              private _decodeCurlyBracesService: DecodeCurlyBraces,
              private _totalNumberOfTerms: TotalNumberOfTerms,
              private _capitalizeService: Capitalize,
              public localization: LocalizationService) {
    super(null, localization);
  }

  ngOnInit() {
    IdentityService.identity.subscribe(data => {
      this.identity = data;
    });

    this.searchBoxOptions = {label: 'Terms', operators: ['OR', 'AND']};
    this.productCodes = this._databaseSelectorService.getCurrentSelectedDatabases();
    if(this._router.url == '/thesaurus') {
      this.getThesaurusTerms();
      this.logPageView('TermFinder');
    }
    this._activatedRoute.queryParams.subscribe (params => {
     let logTerm = 'TermFinder';
      if(params && params['code']) {
        this.getTermByCode(params['code']);
      } else if(params && params['term'] && !params['page']) {
        this.getTermsBySearch( params['term']);
        logTerm ='TermFinder_jump-to';
      } else if(params && params['alpha'] && !params['page']) {
        this.getTermsByAlpha(params['alpha']);
        logTerm = 'TermFinder_a-z';
      } else if(params && params['page']) {
        if(params['alpha']) {
          this.getTermsByPagination(params['page'], params['alpha'], false);
        } else if(params['term']){
          this.getTermsByPagination(params['page'], params['term'], true);
        } else {
          this.getTermsByPagination(params['page'], params['total']);
        }
      } else {
        this._router.navigate(['/thesaurus']);
      }
      this.getThesaurusTerms();
      this.logPageView(logTerm);
    });
  }

  getTermByCode(code) {
    this.termCode = code;
    this.selectedLimit = 1;
    this.selectedPage = 2;
    this.term = '';
    this.selectAlpha = '';
  }

  getTermsBySearch(term) {
    this.term = term;
    if (this.termCode.length > 0) {
      this.selectedLimit = 15;
    }
    this.selectedPage = 1;
    this.selectAlpha = '';
    this.termCode = '';
  }

  getTermsByAlpha(alpha){
    this.selectAlpha = alpha;
    if (this.termCode.length > 0) {
      this.selectedLimit = 15;
    }
    this.term = '';
    this.termCode = '';
    this.selectedPage = 1;
    this.filterString = '';
  }

  getTermsByPagination(page, value?, isTerm?) {
    this.selectedPage = +page;
    this.origTotalItems = this._totalNumberOfTerms.getTotalTerms();
    this.selectedLimit = 15;
    if(value && !isTerm) {
      this.selectAlpha = value;
      this.term = '';
      this.termCode = '';
    }
    if(value && isTerm) {
      this.term = value;
      this.selectAlpha = '';
      this.termCode = '';
    }
    this.isSelectAll = false;
  }

  /*Returns query params*/
  getQueryParams(): ILookupParams {
    let limit = this.selectedLimit;
    let offset = (this.selectedPage - 1) * this.selectedLimit; //offset starts at 0th index

    //On first page
    if (this.selectedPage == 1) {
      limit += 1;
    }

    //On Middle Pages
    if (this.selectedPage > 1 && this.selectedPage < Math.ceil(this.origTotalItems / this.selectedLimit)) {
      limit += 2;
      offset -= 1;
    }

    //On last page
    if (this.selectedPage == Math.ceil(this.origTotalItems / this.selectedLimit)) {
      limit += 1;
      offset -= 1;
    }

    if (this.termCode.length) {
      limit = 3;
      offset = 0;
    }

    let params = {
      limit: limit,
      offset: offset,
      databases: this.productCodes
    };

    if (this.selectAlpha && this.selectAlpha.length) {
      params['prefix'] = this.selectAlpha;
    } else if (this.term && this.term.length) {
      params['term'] = this.term;
    } else if (this.termCode && this.termCode.length) {
      params['code'] = this.termCode;
    }

    return params;
  }

  updateLoadingStatus(flag: boolean): void {
    this.loading = flag;
  }

  public executeSearch(): void {
    this._router.navigate(['/thesaurus/item'], {queryParams: {term: this.term}});
  }

  public keyDownFunction(event): void {
    if (event.keyCode == 13) {
      this.executeSearch();
    }
  }

  addRemoveAllTerms(select: boolean): void {
    this.thesaurusList.forEach(term=> {
      if (!term.Use && Thesaurus.has(term, 'IndexTerm')) {
        term.isSelected = select;
        this.selectTermForSearch(select, term.IndexTerm);
      }
      else if (!term.MainTermCount && term.Use && term.Use.length) {
        term.isSelected = select;
        this.selectTermForSearch(select, term.Use[0].Term)
      }
    })
  }

  public toggleAllSelection(selectAll: boolean): void {
    this.addRemoveAllTerms(selectAll);
    this.isSelectAll = selectAll;
  }

  private getSelectedCount(){
    let selectionCount = 0;
    this.thesaurusList.forEach(term=> {
      selectionCount = selectionCount + this._searchBoxService.terms.filter(_term=> {
          return _term.isSelected;
        }).length;
    });

    return selectionCount;
  }

  public selectTermForSearch($event, term: string): void {
    term = HighlightService.removeHighlights(term);
    if ($event && term && term.length && !this._searchBoxService.exists(term)) {
      this._searchBoxService.addTerm(term);
      this.addRemoveItems([term], true);
    } else if (!$event && term && term.length && this._searchBoxService.exists(term)) {
      this._searchBoxService.removeTerm(term);
      this.addRemoveItems([term], false);
    }
  }

  addRemoveTermWithNarrowerTerm(thesaurus: IThesaurusItem, select: boolean): void {
    if (Thesaurus.has(thesaurus, 'IndexTerm')) {
      thesaurus.isSelected = select;
      this.selectTermForSearch(select, thesaurus.IndexTerm);
    }
    if (Thesaurus.has(thesaurus, 'NarrowerTermList')) {
      thesaurus.NarrowerTermList.forEach(term=> {
        term.isSelected = true;
        if (Thesaurus.has(term, 'Term')) {
          this.selectTermForSearch(select, term.Term);
        }
      })
    }
    thesaurus.isNarrowerSelected = select;
  }

  public toggleTermWithNarrowerSelection(thesaurus: IThesaurusItem): void {
    thesaurus.isNarrowerSelected = !thesaurus.isNarrowerSelected;
    this.addRemoveTermWithNarrowerTerm(thesaurus, thesaurus.isNarrowerSelected);
  }

  public static has(params: Object, field: string): boolean {
    return params.hasOwnProperty(field) && params[field] && params[field].length;
  }

  public addRemoveItems(terms: string[], selected: boolean): void {
    let lookupProps = ['BroaderTermList', 'RelationList', 'UsedFor', 'NarrowerTermList', 'Use'];
    let selectionCount = 0;
    this.thesaurusList.forEach(term=> {
      if (Thesaurus.has(term, 'IndexTerm') && terms.indexOf(HighlightService.removeHighlights(term.IndexTerm)) > -1) {
        term.isSelected = selected;
      }

      if(!term.MainTermCount){
        lookupProps.forEach(prop=> {
          if (Thesaurus.has(term, prop)) {
            term[prop].forEach(_term=> {
              if (Thesaurus.has(_term, 'Term') && terms.indexOf(HighlightService.removeHighlights(_term.Term)) > -1) {
                term.isSelected = selected;
                _term.isSelected = selected;
              }
            });
          }
        });
      } else if(term.MainTermCount){
        lookupProps.forEach(prop=> {
          let allNarrowerTermsSelected = true;
          if (Thesaurus.has(term, prop)) {
            term[prop].forEach(_term=> {
              if(prop === 'NarrowerTermList') {
                allNarrowerTermsSelected = allNarrowerTermsSelected && _term.isSelected;
              }

              if (Thesaurus.has(_term, 'Term') && terms.indexOf(HighlightService.removeHighlights(_term.Term)) > -1) {
                _term.isSelected = selected;
              }
            });

            if(term.hasOwnProperty('NarrowerTermList')) {
              term.isNarrowerSelected = term.isSelected && allNarrowerTermsSelected;
            }
          }
        });
      }

      if(term.isSelected){
        selectionCount++;
      }
    });

    this.isSelectAll = (selectionCount == this.thesaurusList.length);
  }

  public selectTerm(code: string): void {
    if (code && code.length) {
      this._router.navigate(['/thesaurus/item'], {queryParams: {code: code}});
    }
  }

  syncCheckUncheck(): void {
    if (!this.retrieved) {
      let searchFormId = this._sessionStorageService.retrieve('searchFormId');
      if (searchFormId) {
        let existingSearch = SearchCollection.get(searchFormId);
        if (existingSearch && existingSearch.query && existingSearch.query.criteria && existingSearch.query.criteria.length) {
          existingSearch.query.criteria.forEach(criteria=> {
            if (criteria.field && criteria.field == 'IndexTerms' && criteria.value && criteria.value.length) {
              let allTerms = this._decodeCurlyBracesService.getArray(criteria.value); //deDuplicate
              let terms = allTerms.filter(_term=>this._searchBoxService.terms.indexOf(_term) < 0);
              this._searchBoxService.addTerms(...terms);
            }
          });
        }
        this.retrieved = true;  //prevent retrieve on each pagination event
      }
    }
    this.addRemoveItems(this._searchBoxService.terms, true);
  }

  getAPI(): string {
    if (this.term.length < 1 && this.selectAlpha.length > 0 && this.termCode.length < 1) {
      return 'thesaurus.getTermBeginsWith';
    } else if (this.selectAlpha.length < 1 && this.term.length > 0 && this.termCode.length < 1) {
      return 'thesaurus.getTermContains';
    }
    return 'thesaurus.getTermByCode';
  }

  /*Common method to get Thesaurus*/
  getThesaurusItems(queryParams: ILookupParams): void {
    let api = this.getAPI();
    this.updateLoadingStatus(true);
    let query: Query = this._apiService.createQuery(api);
    this.showPaginationPages = false;
    this.contentLoaded = false;
    query.params(queryParams);
    this._apiService.execute(query)
      .subscribe(
        data => {
          let jList = data.response.result.doc;
          let len = parseInt(data.response.result.numFound);
          this.origTotalItems = len;
          this._totalNumberOfTerms.setTotalTerms(this.origTotalItems);
          this.totalItems = len;

          if (jList && len > 0) {
            if (len > this.selectedLimit) {
              let maxPages = Math.ceil(this.origTotalItems / this.selectedLimit);
              if (api != 'thesaurus.getTermByCode' || (api == 'thesaurus.getTermByCode' && len > 2)) {
                //On Page 1
                if (this.selectedPage == 1) {
                  let nextItem = jList.pop();
                  if (nextItem.IndexTerm && nextItem.IndexTerm.length > 0) {
                    this.appendText = HighlightService.removeHighlights(nextItem.IndexTerm);
                    if (this.termCode.length > 0 && nextItem.IndexTerm.charAt(0) == jList[0].IndexTerm.charAt(0)) {
                      this.nextTerm = nextItem;
                    }
                    else if (this.termCode.length > 0 && nextItem.IndexTerm.charAt(0) != jList[0].IndexTerm.charAt(0)) {
                      this.appendText = '';
                    }
                  }
                }

                //On last page
                if (this.selectedPage == maxPages) {
                  let prevItem = jList.shift();
                  if (prevItem.IndexTerm && prevItem.IndexTerm.length > 0) {
                    this.prependText = HighlightService.removeHighlights(prevItem.IndexTerm);
                    if (this.termCode.length > 0 && prevItem.IndexTerm.charAt(0) == jList[0].IndexTerm.charAt(0)) {
                      this.prevTerm = prevItem;
                    }
                    else if(this.termCode.length > 0 && prevItem.IndexTerm.charAt(0) != jList[0].IndexTerm.charAt(0)) {
                      this.prependText='';
                    }
                  }
                }

                //On page > 1 && page < totalPages
                if (this.selectedPage > 1 && this.selectedPage < maxPages) {
                  let prevItem = jList.shift();
                  let nextItem = jList.pop();
                  if (prevItem.IndexTerm && prevItem.IndexTerm.length > 0) {
                    this.prependText = HighlightService.removeHighlights(prevItem.IndexTerm);
                    if (this.termCode.length > 0 && prevItem.IndexTerm.charAt(0) == jList[0].IndexTerm.charAt(0)) {
                      this.prevTerm = prevItem;
                    }
                    else if(this.termCode.length > 0 && prevItem.IndexTerm.charAt(0) != jList[0].IndexTerm.charAt(0)) {
                      this.prependText='';
                      this.selectedPage = 1;
                    }
                  }
                  if (nextItem.IndexTerm && nextItem.IndexTerm.length > 0) {
                    this.appendText = HighlightService.removeHighlights(nextItem.IndexTerm);
                    if (this.termCode.length > 0 && nextItem.IndexTerm.charAt(0) == jList[0].IndexTerm.charAt(0)) {
                      this.nextTerm = nextItem;
                    }
                    else if (this.termCode.length > 0 && nextItem.IndexTerm.charAt(0) != jList[0].IndexTerm.charAt(0)) {
                      this.appendText = '';
                      this.selectedPage = maxPages;
                    }
                  }
                }
              } else {
                //On getTermByCode
                if (len == 2 && this.selectedPage == 2 && this.termCode.length && this.selectedLimit == 1) {
                  jList.forEach((item, index)=> {
                    if (item.Code == this.termCode && index == 0) {
                      this.selectedPage = 1; //Faking
                      this.prevTerm = this.prependText = null;
                      let nextItem = jList.pop();
                      if (nextItem.IndexTerm && nextItem.IndexTerm.length > 0) {
                        this.appendText = HighlightService.removeHighlights(nextItem.IndexTerm);
                        if (this.termCode.length > 0) {
                          this.nextTerm = nextItem;
                        }
                      }
                    } else {
                      this.selectedPage = 2; //Faking
                      this.nextTerm = this.appendText = null;
                      let prevItem = jList.shift();
                      if (prevItem.IndexTerm && prevItem.IndexTerm.length > 0) {
                        this.prependText = HighlightService.removeHighlights(prevItem.IndexTerm);
                        if (this.termCode.length > 0) {
                          this.prevTerm = prevItem;
                        }
                      }
                    }
                  })
                }
              }
              this.appendText = this._capitalizeService.getCapitalized(this.appendText);
              this.prependText = this._capitalizeService.getCapitalized(this.prependText);
            }

            this.thesaurusList = jList;
            if (this.termCode.length > 0) {
              this.selectAlpha = this.thesaurusList[0].IndexTerm.charAt(0);
              this.toggleThesaurus(this.thesaurusList[0]);
            }
          } else {
            this.thesaurusList = [];
          }
          this.disabledFormControls = (this.thesaurusList.length < 1);
          this.isBottomPaginationBarVisible = (this.thesaurusList.length > this.bottomPaginationThreshold);
          this.syncCheckUncheck();
          this.contentLoaded = true;
          this.updateLoadingStatus(false);
        },
        error => console.log(error),
        () => console.log('Response From getThesaurusItems Complete')
      );
  }

  public getNewPagination(pagination): void {
    if (this.termCode.length > 0 && pagination.hasOwnProperty('ev') && pagination.ev) {
      let termCode = '';
      if (pagination.currentPage == 1) {
        termCode = this.prevTerm.Code;
      } else if (pagination.currentPage > 1) {
        termCode = this.nextTerm.Code;
      }
      this.selectTerm(termCode);
    } else if(this.termCode.length == 0){
      if (pagination.hasOwnProperty('limit') && pagination.hasOwnProperty('currentPage')) {
        this.selectedLimit = pagination.limit;
        this.selectedPage = pagination.currentPage;
        this.applyPagination();
      }
      if (pagination.hasOwnProperty('currentPage') && !pagination.hasOwnProperty('limit')) {
        this.selectedPage = pagination.currentPage;
        if (pagination.hasOwnProperty('ev') && pagination.ev) {
          this.applyPagination();
        }
      }
    }
  }

  /*Apply Pagination*/
  applyPagination(): void {
    if( this.selectAlpha && this.selectAlpha.length) {
      this._router.navigate(['/thesaurus/item'], {queryParams: {alpha: this.selectAlpha, page: this.selectedPage}});
    } else if(this.term && this.term.length) {
      this._router.navigate(['/thesaurus/item'], {queryParams: {term: this.term, page: this.selectedPage}});
    } else {
      this._router.navigate(['/thesaurus/item'], {queryParams: {page: this.selectedPage}});
    }
  }

  /*Toggle Accordion*/
  public toggleThesaurus(thesaurus: IThesaurusItem): void {
    thesaurus.isOpen = !thesaurus.isOpen;
  }

  /*This function triggers once alphabet navigation is clicked*/
  public getNewAlpha(evt): void {
    if (evt.length) {
      this.selectAlpha = evt.toLowerCase();
    } else {
      this.selectAlpha = 'A';
    }
    this._router.navigate(['/thesaurus/item'], {queryParams: {alpha: this.selectAlpha}});
  }

  /*Fetch Thesaurus*/
  fetchThesaurus(queryParams: ILookupParams): void {
    this.getThesaurusItems(queryParams);
  }

  getThesaurusTerms(): void {
    let queryParams = this.getQueryParams();
    this.fetchThesaurus(queryParams);
  }

  stringifyTerms(selectedItems: any) {
    let newSearchTerm: string = '';
    if (selectedItems.terms.length > 1) {

      newSearchTerm = "{" + selectedItems.terms[0].trim() + "} ";
      for (let x = 1; x < selectedItems.terms.length; x++) {
        newSearchTerm = newSearchTerm + selectedItems.operator + " {" + selectedItems.terms[x].trim() + "} ";
      }
    }
    else if (selectedItems.terms.length == 1) {
      newSearchTerm = "{" + selectedItems.terms[0].trim() + "}";
    }
    return newSearchTerm;
  }

  private preparePNQuery(field: string, operator: string, terms: string, source: string): PsycNETQuery {
    let pnQuery = new PsycNETQuery();
    pnQuery.criteria.push(new Criteria(field, operator, terms, source));
    // pnQuery.filter.onlyShow = this.formData.filter;
    pnQuery.source = 'advanced';
    pnQuery.databases = this.productCodes;
    return pnQuery;
  }

  public getAddToSearch(selectedItems: any) {
    let field = 'IndexTerms';
    for(let i = 0; i < selectedItems.terms.length; i++) {
      selectedItems.terms[i] = new CapitalizeWordHTMLContents().transform(selectedItems.terms[i]);
    }
    let searchFormId = this._sessionStorageService.retrieve('searchFormId');
    let terms = this.stringifyTerms(selectedItems);
    let source = 'user';
    let id = searchFormId;
    let existingSearch;
    if (searchFormId && SearchCollection.hasSearch(searchFormId)) {
      existingSearch = SearchCollection.get(searchFormId);
    }
    if (terms.length) {
      if (searchFormId && SearchCollection.hasSearch(searchFormId) && existingSearch && existingSearch.query && existingSearch.query.criteria && existingSearch.query.criteria.length) {
        existingSearch.query.criteria.push(new Criteria(field, 'AND', terms, source));
        let newSearch = SearchCollection.cloneWithNewId(existingSearch);
        id = newSearch.id;
      } else {
        let pnQuery = this.preparePNQuery(field, 'AND', terms, source);
        let responseParams: IResponseParameters = {
          start: 0,
          rows: 25,
          facet: true,
          results: true
        };
        let search = new Search(pnQuery, responseParams);
        SearchCollection.add(search);
        id = search.id;
      }
      this._sessionStorageService.store('searchFormId', id);
      this._router.navigate(['/search/advanced'], {queryParams: {id: id}});
    }
  }

  // the other API Requests for this page will be cached in localStorage, so we don't want to make this log request on its own so that it's not missed
  private logPageView(pageId: string) {
    let query: Query = this._apiService.createQuery('log.logEntry');
    query.logs({pageId: pageId});
    this._apiService.execute(query).subscribe();
  }
}
