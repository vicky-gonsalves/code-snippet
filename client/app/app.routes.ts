import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
  { path: '', redirectTo: 'search', pathMatch: 'full' },
  { path: 'search', loadChildren: 'app/search/search.module#SearchModule'},
  { path: 'record', loadChildren: 'app/recordDisplay/recordDisplay.module#RecordDisplayModule'},
  { path: 'thesaurus', loadChildren: 'app/thesaurus/thesaurus.module#ThesaurusModule'},
  { path: 'MyPsycNET', loadChildren: 'app/myPsycNET/myPsycNET.module#MyPsycNETModule'},
  { path: 'general', loadChildren: 'app/general/general.module#GeneralModule'},

  // browse routes
  { path: 'PsycARTICLES', loadChildren: 'app/browse/browsePA/browsePA.module#BrowsePAModule'},
  { path: 'PsycBOOKS', loadChildren: 'app/browse/browsePB/browsePB.module#BrowsePBModule'},
  { path: 'PsycCRITIQUES', loadChildren: 'app/browse/browsePC/browsePC.module#BrowsePCModule'},
  { path: 'PsycEXTRA', loadChildren: 'app/browse/browsePE/browsePE.module#BrowsePEModule'},
  { path: 'PsycTESTS', loadChildren: 'app/browse/browsePT/browsePT.module#BrowsePTModule'},
  { path: 'PsycTHERAPY', loadChildren: 'app/browse/browsePTH/browsePTH.module#BrowsePTHModule'},

  { path: 'fulltext/:uid.html', loadChildren: 'app/fulltext/fulltext.module#FulltextModule'},
  { path: 'buy', loadChildren: 'app/buy/buy.module#BuyModule'},
  { path: 'doiLanding', loadChildren: 'app/doiLanding/doiLanding.module#DoiModule'},

  { path: 'no-access', loadChildren: 'app/noAccess/noAccess.module#NoAccessModule'},

  { path: 'admin', loadChildren: 'app/admin/admin.module#AdminModule'},

  {
    path : '**',
    redirectTo : 'search'
  }

];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
