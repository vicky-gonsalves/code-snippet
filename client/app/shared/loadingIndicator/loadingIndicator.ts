import {Component, Input} from '@angular/core';

@Component({
  selector : 'loading-indicator',
  templateUrl: './loadingIndicator.html',
  styleUrls: ['./loadingIndicator.less']
})
export class LoadingIndicator{
  @Input() loaderText : string
}
