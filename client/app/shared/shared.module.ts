import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { LocaleModule, LocalizationModule, LocaleService, LocalizationService  } from 'angular2localization';

import {AccordionModule, AlertModule, CollapseModule, DropdownModule, ModalModule, TooltipModule, TypeaheadModule} from 'ng2-bootstrap';

import { AllDatabases } from './dbSelector/allDatabases';
import { DatabaseSelector } from './dbSelector/databaseSelector';
import { DatabaseSelectorService } from './dbSelector/databaseSelectorService';
import { Capitalize } from './capitalize/capitalize';

import { AddToCart } from './addToCart/addToCart';
import { BreadCrumbs } from './breadcrumbs/breadcrumbs';
import { BreadcrumbService } from './breadcrumbs/breadcrumbService';
import { FriendlyQuery } from './friendlyQuery/friendlyQuery';
import { PaginationLimit } from './paginationLimit/paginationLimit';
import { ArrowPagination } from './pagination/arrowPagination';
import { NumberedPagination } from './pagination/numberedPagination';
import { TermBasedPagination } from './pagination/termBasedPagination';
import { PagerPagination } from './pagination/pagerPagination';
import { JumpTo } from './jumpto/jumpto';
import { LoadingIndicator } from './loadingIndicator/loadingIndicator';
import { SelectAll } from './selectAll/selectAll';
import { BrowseAZ } from './browseAZ/browseAZ';
import { EmailRecordModal } from './emailRecordModal/emailRecordModal';
import { DeleteConfirmationModal } from './deleteConfirmationModal/deleteConfirmationModal';
import { AddToListWarningModal } from './addToListWarningModal/addToListWarningModal';
import { PermalinkInput } from './permalinkInput/permalinkInput';
import { AddToMyList } from './addToMyList/addToMyList';
import { SortBy } from './sortBy/sortBy';
import { Social} from './social/social';
import { MissingSearch } from './missingSearch/missingSearch';
import { ToggleAllAbstract } from './toggleAllAbstract/toggleAllAbstract';

import { ProductLabels } from './productLabels/productLabels';
import { RelatedUIDFormatter } from './relatedUIDFormatter/relatedUIDFormatter';
import { HighlightService } from './highlight/highlightService';
import { DOIExtractor } from './doiExtractor/doiExtractor';
import { UUIDService } from './id/uuidService';
import { UIDGuardService } from './id/uidGuardService';

// Pipes
import {URLEncoder} from './pipes/urlEncoder';
import {StringToNumberNotation} from './pipes/stringToNumber';
import {RemoveHTMLTag} from './pipes/removeHTMLTag';
import {Truncate} from './pipes/truncate';
import {TranslationAlias} from './pipes/translationAlias';
import {WordsTruncate} from './pipes/wordsTruncate';
import {Linkify} from './pipes/linkify';
import {FormatSponsorList} from './pipes/formatSponsorList';
import {OpenURLNewWindow} from './pipes/openURLNewWindow';
import {convertDBCodeToName} from './pipes/convertDBCodeToName';
import {LinkDOI, LinkDOIInternal} from './pipes/linkDOI';
import {CapitalizeWords} from './pipes/capitalizeWords';
import {ContentOwner} from './pipes/contentOwner';
import {Safe} from './pipes/safe';
import {YearsCovered} from './pipes/yearsCovered';
import {browsePCEditorialBoard} from "../browse/browsePC/components/browsePCEditorialBoard/browsePCEditorialBoard";
import { CapitalizeWordHTMLContents } from './pipes/capitalizeWordHTMLContents';

let declarationAndExports = [

  AddToCart,
  BreadCrumbs,
  FriendlyQuery,
  DatabaseSelector,
  PaginationLimit,
  ArrowPagination,
  NumberedPagination,
  TermBasedPagination,
  PagerPagination,
  JumpTo,
  LoadingIndicator,
  SelectAll,
  BrowseAZ,
  EmailRecordModal,
  MissingSearch,
  DeleteConfirmationModal,
  AddToListWarningModal,
  PermalinkInput,
  AddToMyList,
  SortBy,
  Social,
  ToggleAllAbstract,

  // pipes
  Truncate,
  TranslationAlias,
  URLEncoder,
  StringToNumberNotation,
  RemoveHTMLTag,
  WordsTruncate,
  Linkify,
  FormatSponsorList,
  OpenURLNewWindow,
  convertDBCodeToName,
  LinkDOI,
  LinkDOIInternal,
  CapitalizeWords,
  ContentOwner,
  Safe,
  YearsCovered,
  browsePCEditorialBoard,
  CapitalizeWordHTMLContents
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,

    LocaleModule,
    LocalizationModule,

    // ng2-bootstrap
    AccordionModule,
    AlertModule,
    CollapseModule,
    DropdownModule,
    ModalModule,
    TooltipModule,
    TypeaheadModule

  ],
  declarations: [ ...declarationAndExports ],
  providers: [ BreadcrumbService, AllDatabases, DatabaseSelectorService, Capitalize, ProductLabels, RelatedUIDFormatter, HighlightService, DOIExtractor, UUIDService, UIDGuardService ],
  exports: [
    CommonModule,
    FormsModule,
    RouterModule,
    LocaleModule,
    LocalizationModule,

    AccordionModule,
    AlertModule,
    CollapseModule,
    DropdownModule,
    ModalModule,
    TooltipModule,
    TypeaheadModule,

    ...declarationAndExports
  ]
})

export class SharedModule { }
