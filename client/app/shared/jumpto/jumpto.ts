import {Component,Input,Output,EventEmitter} from '@angular/core';
import { Locale, LocaleService, LocalizationService } from 'angular2localization';

@Component({
  selector: 'jumpto',
  templateUrl: './jumpto.html',
  styleUrls: ['./jumpto.less']
})
export class JumpTo extends Locale{
  @Input() value:string = '';
  @Output() changeValue = new EventEmitter();

  constructor(public locale: LocaleService, public localization: LocalizationService) {
    super(locale, localization);
  }

  ngOnChanges(changes) {
    if(changes.hasOwnProperty('value') && changes.value.currentValue.length <=1){
      this.value = '';
    }
  }

  onSubmit() {
    this.changeValue.emit(this.value);
  }
}
