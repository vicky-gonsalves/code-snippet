import {Injectable} from '@angular/core';
import * as _ from "lodash";

@Injectable()
export class ProductLabels {

  static codeToLabel = {
    "PI": "PsycINFO",
    "PA": "PsycARTICLES",
    "PB": "PsycBOOKS",
    "PC": "PsycCRITIQUES",
    "PQ": "PsycCRITIQUES",  // 'PQ' is returned instead of 'PC' in some cases.
    "PE": "PsycEXTRA",
    "PT": "PsycTESTS",
    "PH": "PsycTHERAPY"
  };

  static labelToCode = _.invert(ProductLabels.codeToLabel);

  static getLabel(code: string): string{
    if(ProductLabels.codeToLabel.hasOwnProperty(code)){
      return ProductLabels.codeToLabel[code];
    }

    return '';
  }

  static getCode(label: string): string{
    if(ProductLabels.labelToCode.hasOwnProperty(label)){
      return ProductLabels.labelToCode[label];
    }

    return '';
  }

}
