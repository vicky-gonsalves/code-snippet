import {Component, Input, ViewChild} from '@angular/core';
import {ApiRequestService} from '../../core/services/apiRequestService';
import {MylistStorageService} from '../../core/services/mylistStorageService';
import {Router, RouterStateSnapshot} from "@angular/router";
import {HighlightService} from "../highlight/highlightService";
import {AuthService} from '../../core/services/authService';
import {NavigationEvent} from '../../core/services/navigationEvent';
import {AddToListWarningModal} from "../addToListWarningModal/addToListWarningModal";

@Component({
  selector: 'add-to-my-list',
  templateUrl: './addToMyList.html',
  styleUrls: ['./addToMyList.less']
})
export class AddToMyList {
  @Input('UID') uid: string;
  @Input() theme: string;
  isAdded: boolean;
  isSaving: boolean;
  private _snapshot: RouterStateSnapshot;
  // The theme name is based on the background color, the available ones are listed here.
  static themes = ['gray', 'white','fulltext'];
  mylistCount = 0;
  totalAllowedMylistCount = 250;
  @ViewChild('addToListWarningDialog') addToListWarningDialog: AddToListWarningModal;

  constructor(private _apiService: ApiRequestService,
              private _mylistStorageService: MylistStorageService,
              private _navigationEvent:NavigationEvent,
              private _authService : AuthService,
              private router: Router) {
    this._snapshot = router.routerState.snapshot;
  }

  ngOnInit() {
    this.isSaving = false;

    //remove any highlighting markup
    this.uid = HighlightService.removeHighlights(this.uid);

    if (!this.theme || (this.theme && AddToMyList.themes.indexOf(this.theme) === -1)) {
      this.theme = 'gray';
    }

    let uids = this._mylistStorageService.getUIDs();
    this.isAdded = (uids && uids.indexOf(this.uid) > -1);

    MylistStorageService.count.subscribe(count => {
      this.mylistCount = count;
      let uids = this._mylistStorageService.getUIDs();
      this.isAdded = (uids && uids.indexOf(this.uid) > -1);
    });
  }

  openAddToListWarningDialog() {
    this.addToListWarningDialog.openModal();
  }

  ngAfterViewInit(){
    //block handles adding to my-list after login
    if(this._authService.isAuthenticated() && this._navigationEvent.hasAction()){
      let article = this._navigationEvent.get();
      if(article.args.hasOwnProperty('type') && article.args.type === 'single') {
        let articleList;
        if(article.event == 'addToMyList' && article.args.hasOwnProperty('uidList') && article.args['uidList'].length && article.args['uidList'].indexOf(this.uid) > -1){
          this._navigationEvent.clear();
          if(this.mylistCount < this.totalAllowedMylistCount) {
            articleList= article.args['uidList'];
            let total = this.mylistCount + articleList.length;
            if (total > this.totalAllowedMylistCount) { //if total goes above 250
              let overFlow = total - this.totalAllowedMylistCount;
              let pick = articleList.length - overFlow;
              articleList = articleList.slice(0, pick);
              this.openAddToListWarningDialog();
            }
            this._executeAddToMyListAction(articleList, true);
          } else {
            this.openAddToListWarningDialog();
          }
        }
      }
    }
  }

  private applyChange() {
    if (!this.isAdded) {
      this.performAddition();
    } else {
      this.performRemoval();
    }
  }

  private performAddition(): void {
    if (this.mylistCount < this.totalAllowedMylistCount) {
      console.log('Adding to My List: ', this.uid);
      if (this.uid && this.uid.trim().length && !this.isAdded) {

        let articleList = [this.uid];
        let total = this.mylistCount + articleList.length;
        if (total > this.totalAllowedMylistCount) { //if total goes above 250
          let overFlow = total - this.totalAllowedMylistCount;
          let pick = articleList.length - overFlow;
          articleList = articleList.slice(0, pick);
          this.openAddToListWarningDialog();
        }

        if (this._authService.isAuthenticated()) {
          this._executeAddToMyListAction(articleList, false);
        } else {
          this._navigationEvent.set('addToMyList', {'uidList': articleList, 'type':'single'});
          this._authService.redirectToLogin(this._snapshot.url);
        }
      }
    } else {
      this.openAddToListWarningDialog();
    }
  }

  private _executeAddToMyListAction(articleList, dontUpdateStatus){
    if(!dontUpdateStatus) {
      this.isSaving = true;
    }
    let query = this._apiService.createQuery('mylist.addMyList');
    query.params({"ArticleList": articleList});

    this._apiService.execute(query).subscribe(data => {

      console.log('myList add response: ', data);
      this.isAdded = data.isAdded;
      if (this.isAdded) {
        this._mylistStorageService.add(articleList[0]);
      }
      this.isSaving = false;
    },
      error => {
      console.log(error);
      this.isSaving = false;
    });
  }

  private performRemoval(): void {
    this.isSaving = true;
    console.log('Removing from My List: ', this.uid);

    if (this.uid && this.uid.trim().length && this.isAdded) {
      let articleList = [this.uid];
      let query = this._apiService.createQuery('mylist.deleteMyList');

      query.params({"ArticleList": articleList});
      this._apiService.execute(query).subscribe(
        data => {
          console.log('myList delete response: ', data);
          this.isAdded = !data.isDeleted;
          if (!this.isAdded) {
            this._mylistStorageService.remove(this.uid);
          }
          this.isSaving = false;
        },
        error => {
          console.log(error);
          this.isSaving = false;
        });
    }
  }
}
