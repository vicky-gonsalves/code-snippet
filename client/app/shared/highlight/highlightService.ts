import {Injectable} from '@angular/core';

@Injectable()
export class HighlightService {
  private static regExp = /(<span class=""hilite"">|<span class='hilite'>|<span class="fq-value">|\[|\]|<\/span>)/gi;

  public static removeHighlights(text: string):string {
    let result = '';

    if(text && typeof text === 'string'){
      result = text.replace(HighlightService.regExp, '');
    }

    return result;
  }


}
