import {Component, Input, ViewChild, Optional} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap';
import {SessionStorageService} from 'ng2-webstorage';
import {IRecord} from "../../recordDisplay/services/format/IRecord";
import {ApiRequestService, Query} from "../../core/services/apiRequestService";
import { Identity } from "../../core/services/identity/identity";
import { IdentityService } from "../../core/services/identity/identityService";
import * as _ from "lodash"

@Component({
  selector : 'email-record-modal',
  templateUrl: './emailRecordModal.html',
  styleUrls: ['./emailRecordModal.less']
})

export class EmailRecordModal{
  @Input() isMyList : boolean;
  @Input() parentId : string;
  @Input() recordsPerPage : number;
  @Input('record') recordDetails: IRecord;
  @Input() recordList : IRecord[];
  @Input() allRecordsOnPageUIDList: Array<any>;
  @ViewChild('emailRecordModal') public emailRecordModal : ModalDirective;

  email: any;
  emailPattern: RegExp;
  fullTextRecord: boolean=false;
  displayList : Array<any> = [];
  selectedRecordUIDList= [];
  currentPageSelectedRecordUIDList = [];
  currentSearchDetails : any;
  submitted: boolean;
  recordUID:string;
  isFormInValid: boolean;
  showFullRecordOptions: boolean;
  public showForm: boolean = true;

  constructor(private _sessionStorageService : SessionStorageService,
              private _apiService : ApiRequestService) {
    let emailRegex = '(?:[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*)';
    this.emailPattern = new RegExp(`^${emailRegex}{1}((?:,\\s*)${emailRegex})*$`);
  }

  ngOnInit() {
    IdentityService.identity.subscribe( (identity: Identity) => {
      this.showFullRecordOptions = (identity.isIndividual || identity.isOrganization);
      this._reset();
    });
  }

  private getDisplayOptions() {
    let result = [
      { name: "Citation", value: "Citation" },
      { name: "Citation & Abstract", value: "CitationAndAbstract" },
      { name: "Citation APA Style", value: "APAStyle" }
    ];
    if (this.showFullRecordOptions) {
      result.splice(2, 0, { name: "Full Record Display", value: "Details" });
      result.splice(3, 0, { name: "Full Record Display plus Cited References", value: "DetailsWithRefs" });
    }
    return _.cloneDeep(result);
  }

  private _reset(){
    this.submitted = false;
    this.isFormInValid = false;
    this.displayList = this.getDisplayOptions();
    this.email = {
      to: '',
      from: 'noreply@apa.org',
      subject: 'APA PsycNET Search Results',
      message: '',
      display: this.showFullRecordOptions ? 'Details' : 'Citation',
      includeSearch : true,
      selectedType : 'Selected Records - All Pages'
    };
  }


  openModal(@Optional() currentSearch?:any): void {
    //console.log('got here in openModal');
    this._reset();

    this.fullTextRecord=false;
    this.currentSearchDetails = currentSearch;
    this.email.includeSearch = !this.isMyList;

    this.selectedRecordUIDList =  this._sessionStorageService.retrieve('selectedSearchRecords') || [];
    this.currentPageSelectedRecordUIDList = this._sessionStorageService.retrieve('currentPageSearchRecords') || [];

    this.emailRecordModal.show();
  }

  openFullTextModal(UID:string): void {
    console.log('got here in openFullTextModal');
    console.log(UID);
    this.recordUID=UID;
    this.fullTextRecord=true;
    this._reset();
       // this.currentSearchDetails = currentSearch;
    this.displayList =  [{ name : "FullText HTML", value : "FullText"}];
    this.email.display='FullText';
    this.email.includeSearch = false;
    //this.selectedRecordUIDList =  this._sessionStorageService.retrieve('selectedSearchRecords') || [];
    //this.currentPageSelectedRecordUIDList = this._sessionStorageService.retrieve('currentPageSearchRecords') || [];

    this.emailRecordModal.show();
  }


  closeModal():void {
    this.submitted = false;
    this.isFormInValid = false;
    this.emailRecordModal.hide();
  }

  isOpen(): boolean{
    return this.emailRecordModal.isShown;
  }

  sendEmail(){
    this.submitted = true;
    this.isFormInValid =false;
    let params = {};

    if(this._hasRecordList()){
      // PT record full record display page
      params = {record: this.recordList, isRecordDisplay : true};

    } else if(this.recordDetails && !this.recordsPerPage){
      //full record display play - other db type
      params = {record: this.recordDetails, isRecordDisplay : true};

    } else {
      //search result display page
      let uidList = this._getUIDList();

      if(uidList && uidList.length > 0 && uidList.length <= 250){
        params = {UIDList: uidList, isRecordDisplay : false};
      }else{
        this.submitted = false;
        this.isFormInValid = true;
      }
    }

    params = _.extend({}, params, { email : this.email, search : this.currentSearchDetails });

    if(this.submitted && !this.isFormInValid){

      let qry = this._apiService.createQuery('search.email');
      qry.params(params);
      this._apiService.execute(qry).subscribe(data => {

      });
      this.showForm =false;
    }
  }

  private _hasRecordList(){
    return this.recordList && this.recordList.length && this.recordList.length <= 250;
  }

  sendFullTextEmail(){

    console.log("Sending FullText Email");
    //console.log(this.recordUID);
    this.submitted=true;

    let qry = this._apiService.createQuery('fulltext.fulltextemail');
    let params = {};

    params = {uid:this.recordUID};

    params = _.extend({}, params, { email : this.email});

    console.log(params);

    qry.params(params);
    this._apiService.execute(qry).subscribe(data => {

    });
    this.showForm =false;
  }

  private _getUIDList(){
    let uidList = [];

    switch(this.email.selectedType){
      case 'Selected Records':
        uidList = _.cloneDeep(this.currentPageSelectedRecordUIDList);
        break;
      case 'All Records on Page':
        uidList = _.cloneDeep(this.allRecordsOnPageUIDList);
        break;
      case 'Selected Records - All Pages':
        uidList = _.cloneDeep(this.selectedRecordUIDList);
        break;
    }

    return uidList;
  }

  public onHidden():void {
    this.showForm = true;
  }

}
