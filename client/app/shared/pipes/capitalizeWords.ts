import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'capitalize'})
export class CapitalizeWords implements PipeTransform {
  transform(str: string): string {
    let smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;
    let words = str.split(' ');      // Get me an array of separate words

    words = words.map(function(word, index) {
      if (index > 0 && ~word.search(smallWords)) {
        return word;                 // If it's a small word, don't change it.
      }

      if (~word.lastIndexOf('.', 1)) { // If it contains periods, it's an abbreviation: Uppercase.
        return word.toUpperCase();   // lastIndexOf to ignore the last character.
      }

      if (word.indexOf('-')) { // If it contains dashes: Uppercase.
        word = word.split('-').map(_w=>_w.charAt(0).toUpperCase() + _w.substr(1)).join('-');
      }

      if (word.indexOf("'")) { // If it contains ' Capitalize only Human names
        word = word.split("'").map((_w, i)=> {
          if (i > 0 && _w.length > 1) {
            return _w.charAt(0).toUpperCase() + _w.substr(1);
          }
          return _w;
        }).join("'");
      }

      // Capitalize the fist letter if it's not a small word or abbreviation.
      return word.charAt(0).toUpperCase() + word.substr(1);
    });

    return words.join(' ');
  }
}
