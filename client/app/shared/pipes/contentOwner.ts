import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'contentOwner'})
export class ContentOwner implements PipeTransform {
  transform(value: string): string {
    let contentOwner = value.split('|');
    return contentOwner.shift();
  }
}
