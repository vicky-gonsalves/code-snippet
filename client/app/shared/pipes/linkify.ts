/**
 * Created by NIKHILBABUM on 10/14/2016.
 */

import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'linkify'})
export class Linkify implements PipeTransform {
  transform(text: string, mode?: string): string {
    let urlRegex = /(https?:\/\/[^\s]+)/g;

    let isTaggedDoi = text.match(/doi:*.*?(?=<)/g);
    let isPlainDoi = text.match(/doi:*.*/g);

    if (isTaggedDoi || isPlainDoi) {
      let matching = text.indexOf("doi:");
      let endIndex = isTaggedDoi ? text.length - 6 : text.length;
      return text.replace(text.substring(matching, endIndex), function (url) {
        // use internal DOI resolver
        let result = '';
        let doiForDisplay = url.substring(4);
        let doiLink = doiForDisplay;

        if (mode && mode === 'ptDisplay') {
          doiLink = doiLink + '?ptDisplay=1';
        }

        if (mode && mode === 'printing') {
          result = 'http://dx.doi.org/' + doiForDisplay;
        } else {
          result = '<a target="_blank" href="/doi/' + doiLink + '">' + 'http://dx.doi.org/' + doiForDisplay + '</a>';
        }
        return result;
      });
    }

    return text.replace(urlRegex, function(url) {
        return '<a target="_blank" href="' + url + '">' + url + '</a>';
    });
  }
}
