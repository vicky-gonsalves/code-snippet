import {Pipe, PipeTransform} from '@angular/core';

@Pipe({ name: 'linkDOI' })
export class LinkDOI implements PipeTransform {
    transform(doi: string) : string {
        return 'http://dx.doi.org/' + doi;
    }
}

@Pipe({ name: 'linkDOIInternal' })
export class LinkDOIInternal implements PipeTransform {
  transform(doi: string, mode?: string) : string {

    if (mode && mode === 'ptDisplay') {
      doi = doi + '?ptDisplay=1';
    }

    return '/doi/' + doi;
  }
}
