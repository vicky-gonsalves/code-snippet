import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'formatSponsor'})
export class FormatSponsorList implements PipeTransform {
  transform(text: any ): string {
    if(text){

      let sponsorString = "";

      for(let sponsorTerm in text){

        let len = sponsorTerm.indexOf(":")+1;

        if(sponsorTerm != "OtherDetails"){
          sponsorString += "<span><b>"+ sponsorTerm +":</b>" + text[sponsorTerm] + "</span><br>";
        }
      }
      return sponsorString;
    }
    return text;

  }
}
