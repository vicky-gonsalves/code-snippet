import {Pipe,PipeTransform} from '@angular/core';

@Pipe({name: 'removeHTMLTag'})
export class RemoveHTMLTag implements PipeTransform {
  constructor() {
  }

  transform(url) {
    var wrapper= document.createElement('div');
    wrapper.innerHTML= url;
    var div= wrapper.firstChild;
    return div.textContent;
  }
}

