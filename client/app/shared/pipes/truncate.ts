import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'truncate'})
export class Truncate implements PipeTransform {
  transform(text: string, stopWhere: string, length: number, transformerText): string {
    if(text && text.length  ){

      // apply defaults
      length = isNaN(length) ? 50 : length;
      stopWhere = (stopWhere === 'after') ? stopWhere : 'before';
      transformerText = transformerText || '...';

      // if it doesn't need trimmed, then exit out early
      if (text.length <= length) {
        return text;
      }

      // Look for the (space) locations, just before and just after the text length limit.
      let afterLimitIndex = 0, beforeLimitIndex = -1;
      while(afterLimitIndex !== -1 && afterLimitIndex < length) {
        beforeLimitIndex = afterLimitIndex;
        afterLimitIndex = text.indexOf(' ', afterLimitIndex + 1);
      }
      let end = stopWhere === 'after' ? afterLimitIndex : beforeLimitIndex;
      return text.substring(0, end) + transformerText;

    }
    return text;

  }
}
