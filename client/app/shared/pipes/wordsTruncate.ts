import {Pipe, PipeTransform} from '@angular/core';

@Pipe({ name: 'wordsTruncate' })
export class WordsTruncate implements PipeTransform {
    transform(text: string, wordCount: number, transformerText): string {
        if (text && text.length) {
            // Apply defaults
            wordCount = isNaN(wordCount) ? 1 : wordCount;
            transformerText = transformerText || '...';

            // Count the words using spaces, until the limit is reached, or the text ends.
            let spaceIndex = 0, wc = 0;
            while (spaceIndex > -1 && wc < wordCount) {
                spaceIndex = text.indexOf(' ', spaceIndex + 1);
                wc++;
            }
            return (spaceIndex > -1) ? text.substring(0, spaceIndex) + transformerText : text;
        }
        return text;
    }
}