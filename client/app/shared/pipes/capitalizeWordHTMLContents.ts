import {Pipe, PipeTransform} from '@angular/core';
import {CapitalizeWords } from './capitalizeWords';
@Pipe({name: 'capitalizeWordHTMLContents'})
export class CapitalizeWordHTMLContents implements PipeTransform {
  transform(str: string): string {

    var wrapper= document.createElement('div');
    wrapper.innerHTML= str;
    let capitalizedChild = this.iterateChildCapitalize(wrapper);

    return capitalizedChild.innerHTML;
  }

  private iterateChildCapitalize(wrapper){
    let children = wrapper.childNodes;

    for (let i = 0; i < children.length; i++) {
      if(i > 0 && !this.checkAbilityForCapitalize(children,i)) {
        continue;
      }else if(children[i].childNodes && children[i].childNodes.length){
          this.iterateChildCapitalize(children[i]);
      }else{
        children[i].textContent = new CapitalizeWords().transform(children[i].textContent);
      }
    }
    return wrapper;
  }

  private checkAbilityForCapitalize(children, index) {
    let result = true;
    if(!children[index-1].textContent.match(/\s/g)) {
      result = false;
    }
    return result;
  }
}
