import {Pipe, PipeTransform} from '@angular/core';

/*
 *   Build a pipe to allow ngFor to loop over objects
 */
@Pipe({name: 'string2NumberNotation'})

export class StringToNumberNotation implements PipeTransform {
  transform(value:string, args?: any[]):string {
    var commaDigit = 3;
    var input = String(value);
    if(input && input.length){
      var number= "";
      var notationNumber = input.length / commaDigit;
      for(var i = 0 ; i < notationNumber; i++){
        let str = input.substring(input.length- ((i+1)*commaDigit), input.length- (i*commaDigit));
        number = i ==0 ? str + number : str +','+ number;
      }
      return number != "" ? number : input;
    }
    return value;
  }
}
