import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'tr_alias'})
export class TranslationAlias implements PipeTransform {
  /**
   * Generates the translation key of a normal English text, the outcome of this PIPE should be used directly with the (angular2localization) module.
   * Such key should be defined in all the (locale-[lang].json) files.
   *
   * Within the normal text, white-space and other symbols should be replaced to create a convenient key, this pipe takes care of converting:
   *      Simple normal text
   * Into:
   *      SIMPLE_NORMAL_TEXT
   *
   * Also, the "&" symbol, which means (and) in normal language, is handled too:
   *      Ones & twos
   * Becomes:
   *      ONES_AND_TWOS
   *
   * More substitution pairs can be defined, within the PIPE's (substitution) member variable;
   *
   * ----------------------
   *
   * Example (1):
   * Assume the following markup exists in a HTML file:
   *    <div>Search</div>
   *
   * Given that the (locale-de.json) file is:
   *    {
   *      "SEARCH": "Suchen"
   *    }
   *
   * And German is the selected language, it can be translated as either:
   *    <div>{{ 'Search' | tr_alias | translate:lang }}</div>
   * Or
   *    <div translate>{{ 'Search' | tr_alias }}</div>
   *
   * The same applies to the component's interpolation statements, as shown in below.
   *
   * ----------------------
   *
   * Example (2): extracted from the (PNHeader) component.
   *
   * Assume the markup is:
   *
   *    <ul class="list-view">
   *      <li *ngFor="let browseDB of browseList">
   *        <a [routerLink]="[browseDB.route]">{{browseDB.name}}</span></a>
   *      </li>
   *    </ul>
   *
   * And the (locale-de.json) file is:
   *    {
   *      "HEADER": {
   *        "JOURNAL_ARTICLES": "Artikel in Fachjournalen",
   *        "TESTS_AND_MEASURES": "Tests und Messinstrumente",
   *        .....
   *      }
   *    }
   *
   * Then translating the values of the iterated list can be conducted in-place, while taking care of the JSON's nested fields, as:
   *
   *    <a [routerLink]="[browseDB.route]">{{browseDB.name | tr_alias:'HEADER' | translate:lang}}</span></a>
   * Or
   *    <a [routerLink]="[browseDB.route]" translate>{{browseDB.name | tr_alias:'HEADER'}}</span></a>
   *
   */


  /**
   * Add more symbols if necessary.
   * Note that:
   *  - The key is the symbol to be replaced, and
   *  - The value is the key's substitution.
   */
  private readonly substitution = {
    ' ': '_',
    '-': '_',
    '/': '_',
    '&': 'and',
    '(': '',
    ';': '',
    ')': '',
    '?': '',
    ':': '',
    '.': ''
  };


  /**
   * Arguments:
   *
   * text (REQUIRED)
   *      The text literal to be transformed.
   * ns [OPTIONAL]
   *      Shorthand of (namespace), the prefix that should be prepended to the output, based on the structure of the translation JSON object. If it is not set, it would be skipped.
   *
   * Returns:
   *      The translation key name.
   */
  transform(text: string, ns?: string): string {
    let result = '';

    if(text && text.length) {
      result = text;

      // Scan for <br> tag variants, and get the first preceding text to such tag.
      let breakPattern = /(?:<br(\s*\/?)>)/g;
      let matchedBreaks = result.match(breakPattern) || [];
      if(matchedBreaks.length) {
        let endIndex = result.indexOf(matchedBreaks[0]);
        result = result.substring(0, endIndex).trim();
      }

      // Aliases for (PI Classification) are named after their codes, like: "PI_C_2200".
      if(result.match(/^[0-9]{4}\ .*/g)) {
        result = 'PI_C_' + result.substring(0, 4);
      } else {
        // For (Age Groups) and the like, intervals like "(2 to 5)" are used instead of "(2-5)".
        result = result.replace(/(\w{1,})-(\d{1,})/g, '$1 to $2');

        for(let ch in this.substitution) {
          while(result.indexOf(ch) >= 0) {
            result = result.replace(ch, this.substitution[ch]);
          }
        }

        if(result.endsWith('_')) {
          result = result.substring(0, result.length - 1);
        }
      }
    }
    return (ns && ns.length && result.length) ? (ns.toUpperCase() + '.' + result.toUpperCase()) : result.toUpperCase();
  }
}
