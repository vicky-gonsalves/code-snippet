import {Pipe, PipeTransform} from '@angular/core';
import {ProductLabels} from '../productLabels/productLabels';

@Pipe({name: 'convertDBCode2Name'})
export class convertDBCodeToName implements PipeTransform {

  transform(dbCodes: string | Array<string>, allDbsExplicitListing?: boolean): string {
    let dbName = [];
    let dbList:Array<string> = [], isAllDB : boolean = true;

    if(dbCodes && dbCodes.length  ){

      if(typeof dbCodes === "string"){
        dbList = dbCodes.split(",");
      }else{
        dbList = dbCodes;
      }

      for(let db of dbList){
        let label = ProductLabels.getLabel(db.toLocaleUpperCase());
        if(label == '') {
          isAllDB = false;
          dbName.push(db.toLocaleUpperCase());
        } else{
          dbName.push(label)
        }
      }
    }
    if(dbList.length === 7 && isAllDB && !allDbsExplicitListing){
      return "All Databases";
    }else{
      return dbName.join(", ");
    }
  }
}
