import {Pipe, PipeTransform} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";

@Pipe({name: 'yearsCovered'})
export class YearsCovered implements PipeTransform {
  transform(value: string): string {
    let years = value.split('-');
    if (years.length >= 2 && !years[1].length) {
      value = years[0] + '-Present';
    }
    return value;
  }
}
