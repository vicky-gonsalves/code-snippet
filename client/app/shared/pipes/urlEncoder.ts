import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'encodeURL'})
export class URLEncoder implements PipeTransform {
  transform(value: string): string {
    if(!!value){
      return encodeURIComponent(value);
    }
    return value;
  }
}
