import {Pipe, PipeTransform, Optional} from '@angular/core';

// PMID and MESH url
const url = "http://www.ncbi.nlm.nih.gov/pubmed/?term=";
const clinicalTrialUrl = "https://clinicaltrials.gov/ct2/results?term=";

@Pipe({name: 'openNewWindow'})
export class OpenURLNewWindow implements PipeTransform {

  constructor() {
  }

  transform(term: string, @Optional() type?: String): any {
    if (term && term.length) {
      let formattedTerm = term.replace(/<[^>]*>/g, "");

      if (type && type == 'MeSH') {
        formattedTerm += "[MeSH Major Topic]";
      }
      let formattedURL = url + encodeURIComponent(formattedTerm);

      return formattedURL;
    }
    return "";
  }

  transformClinicalTrialNumber(term: string): string {
    let formattedURL = '';
    if (term && term.length) {
      formattedURL = clinicalTrialUrl + encodeURIComponent(term) + '&Search=Search';
    }
    return formattedURL;
  }
}
