import { Component } from '@angular/core';

@Component({
  selector: 'missing-search',
  templateUrl: './missingSearch.html',
  styleUrls: ['./missingSearch.less']
})
export class MissingSearch {

  constructor() { }

  ngOnInit() { }
}
