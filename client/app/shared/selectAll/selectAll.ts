import {Component, Input, Output, EventEmitter} from "@angular/core";

@Component({
  selector: 'select-all',
  templateUrl: './selectAll.html',
  styleUrls: ['./selectAll.less']
})

export class SelectAll {
  @Input() parentId : string;
  @Input() isSelectAll:boolean;
  @Input() disabledFormControls:boolean;
  @Output() toggleAllSelection = new EventEmitter();

  constructor() {
  }

  toggleSelectAll(evt):void {
    this.toggleAllSelection.emit(evt);
  }
}
