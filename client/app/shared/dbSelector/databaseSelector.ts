import * as _ from "lodash";
import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SessionStorageService} from 'ng2-webstorage';
import {IDBSelection} from './allDatabases';
import {IdentityService} from '../../core/services/identity/identityService';
import {IIdentity} from '../../core/services/identity/identity';
import {DatabaseFilterSubscription} from '../../core/services/databaseSubscription/databaseFilterSubscription';
import {DatabaseSelectorService} from "./databaseSelectorService";
import {PermissionService} from "../../core/services/identity/permissionService";
import {LocalizationService, Locale} from "angular2localization";

@Component({
  selector  : 'database-selector',
  templateUrl: './databaseSelector.html',
  styleUrls: ['./databaseSelector.less']
})

export class DatabaseSelector extends Locale {

  @Input() open: boolean;
  @Input() defaultSelectedDatabases: Array<string>;
  @Output() selectedDatabase = new EventEmitter();

  private _identity: IIdentity;
  selectAllDisabled: boolean;
  isSelectAll: boolean;
  accessibleDatabases : Array<IDBSelection> = [];

  constructor(private _storage : SessionStorageService, private _route:ActivatedRoute, private _databaseSelectorService: DatabaseSelectorService, private _permissionService: PermissionService, public localization: LocalizationService) {
    super(null, localization);
  }

  ngOnInit() {
      IdentityService.identity.subscribe(data => {
        // ensure identity is loaded
        if(!data){
          return false;
        }

        this._identity = data;

        this._route.queryParams.subscribe(params => {
          let initialDb = params['db'];
          if(initialDb) {
            if(this._permissionService.mayAccess(initialDb) && (this._identity.isOrganization || (this._identity.isIndividual && this._permissionService.mayAccessAllDefaultProducts()) )) {
              this.defaultSelectedDatabases = [initialDb];
            }
          }
        });

        this.selectAllDisabled = true;
        this.accessibleDatabases = new Array<IDBSelection>();
        this._setAuthorizedDatabases();

        DatabaseFilterSubscription.selectedDBChanges.filter(item => item !== null).subscribe(selectedDB => {
          this.defaultSelectedDatabases = selectedDB;
          this._setAuthorizedDatabases();
        });
      });
  }

  private _setAuthorizedDatabases(){
    this.accessibleDatabases = this._databaseSelectorService.getAvailableDatabases(this.defaultSelectedDatabases);
    this.selectAllDisabled  = this._getSelectAllDisableState();

    console.log("accessibleDatabases: ", this.accessibleDatabases);

    this._updateAllSelectedState();

    let selectedDBList = this._getSelected();
    this.selectedDatabase.emit(selectedDBList);

    // save to storage
    this._saveSelections(this._getSelected());
  }

  onDatabaseChange(){
    this._updateSelectedDBs();
  }

  private _updateSelectedDBs(){
    this._updateAllSelectedState();
    let selectedDBList = this._getSelected();
    this.selectedDatabase.emit(selectedDBList);
    this._saveSelections(selectedDBList);
  }

  private _getSelected(){
    let tempDBlist =  _.filter(this.accessibleDatabases, db=>{
      return db.selected;
    });
    let selectedDBList = _.map(tempDBlist, 'code');
    if(selectedDBList.indexOf('PH') === -1 && this._permissionService.mayAccess('PH')) {
      selectedDBList.push('PH');
    }
    return selectedDBList;
  }

  private _updateAllSelectedState(){
    let selectedDBList = this._getSelected();
    this.isSelectAll =  (selectedDBList.length === this.accessibleDatabases.length);
  }

  private _saveSelections(databases){
    this._storage.store('selectedDatabases', databases);
  }

  private _getSelectAllDisableState(){
    let disabledDBList =  _.filter(this.accessibleDatabases, db=>{
      return db.disabled;
    });

    return (disabledDBList.length > 0);
  }

  onSelectAllChange(){
    for (let i=0; i < this.accessibleDatabases.length; i++) {
      this.accessibleDatabases[i].selected = this.isSelectAll;
    }
    let selectedDBList = this._getSelected();
    this.selectedDatabase.emit(selectedDBList);
    this._saveSelections(selectedDBList);
  }
}
