import {Injectable} from '@angular/core';
import * as _ from "lodash";

export interface IDBSelection{
  code : string;
  name : string;
  selected : boolean;
  disabled : boolean;
  fulldesc : string;
}

@Injectable()
export class AllDatabases{

  constructor() {

  }

  private _databaseList = [
    {
      "code": "PI",
      "name": "PsycINFO",
      "selected": false,
      "disabled" : false,
      "fulldesc": "PSYC_INFO_MSG"
    },{
      "code": "PA",
      "name": "PsycARTICLES",
      "selected": false,
      "disabled" : false,
      "fulldesc": "PSYC_ARTICLES_MSG"
    }, {
      "code": "PB",
      "name": "PsycBOOKS",
      "selected": false,
      "disabled" : false,
      "fulldesc": "PSYC_BOOKS_MSG"
    }, {
      "code": "PT",
      "name": "PsycTESTS",
      "selected": false,
      "disabled" : false,
      "fulldesc": "PSYC_TESTS_MSG"
    }, {
      "code": "PC",
      "name": "PsycCRITIQUES",
      "selected": false,
      "disabled" : false,
      "fulldesc": "PSYC_CRITIQUES_MSG"
    }, {
      "code": "PE",
      "name": "PsycEXTRA",
      "selected": false,
      "disabled" : false,
      "fulldesc": "PSYC_EXTRA_MSG"
    }, {
      "code": "PH",
      "name": "PsycTHERAPY",
      "selected": false,
      "disabled" : false,
      "fulldesc": "PSYC_THERAPY_MSG"
    }
  ];

  public getDatabaseList(): Array<IDBSelection>{
    return _.cloneDeep(this._databaseList);
  }

}
