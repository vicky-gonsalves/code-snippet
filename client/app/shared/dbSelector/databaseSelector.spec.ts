/*
import {
  it,
  inject,
  describe,
  beforeEachProviders,
  TestComponentBuilder
} from '@angular/core/testing';

import {Component, provide} from '@angular/core';
import {BaseRequestOptions, Http} from '@angular/http';
import {MockBackend} from '@angular/http/testing';

import {DatabaseSelector} from './databaseSelector';
import {AllDatabases} from './allDatabases';
import {IdentityService} from '../services/identity/identityService';
import {PermissionService} from '../services/identity/permissionService';


describe('DatabaseSelector', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEachProviders(() => [
    BaseRequestOptions,
    MockBackend,
    provide(Http, {
      useFactory: function(backend, defaultOptions) {
        return new Http(backend, defaultOptions);
      },
      deps: [MockBackend, BaseRequestOptions]
    }),

    DatabaseSelector,
    AllDatabases,
    IdentityService,
    PermissionService
  ]);

  it('should have an Permission Service', inject([ DatabaseSelector ], (home) => {
    expect(!!home._permissionService).toEqual(true);
  }));

  it('should log ngOnInit', inject([ DatabaseSelector ], (home) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();

    home.ngOnInit();
    expect(console.log).toHaveBeenCalled();
  }));

});
*/
