import {Injectable} from '@angular/core';
import {SessionStorageService} from 'ng2-webstorage';
import {PermissionService} from '../../core/services/identity/permissionService';
import {AllDatabases, IDBSelection} from './allDatabases';
import {IdentityService} from "../../core/services/identity/identityService";
import {Identity} from '../../core/services/identity/identity';
import * as _ from "lodash";

@Injectable()
export class DatabaseSelectorService {
  private _identity: Identity = new Identity();

  constructor(private _permissionService : PermissionService, private _allDB : AllDatabases, private _storage : SessionStorageService){
    IdentityService.identity.subscribe(data => {
      // ensure identity is loaded
      if(!data){
        return false;
      }

      console.log("Identity: ", data);

      this._identity = data
    });
  }

  public getAvailableDatabases(defaultSelectedDatabases?: string[]): IDBSelection[]{
    let databases: IDBSelection[] = [];

    //Individual or Guest user belongs to organization
    if(this._identity.isOrganization){
      databases = this._getDatabasesBySubscription(defaultSelectedDatabases);
    }else{
      // user is individual or guest, show PI, PA, PB, PC, PE
      databases = this._getIndividualDatabases(defaultSelectedDatabases);
    }

    return databases;
  }

  public getAvailableDatabaseCodes(): string[]{
    let databases: IDBSelection[] = this.getAvailableDatabases();
    let codes = [];
    databases.forEach(db => codes.push(db.code));

    return codes;
  }

  public getCurrentSelectedDatabases(): string[]{
    let selected = this._storage.retrieve('selectedDatabases');

    // if the user hasn't gone to a search form yet, then return the available databases
    if(!selected){
      selected = this.getAvailableDatabaseCodes();
    }

    return selected;
  }



  /*
   * For individuals only (or guests users with -1 for an ID):
   *   a.  "pi,pa,pb,pc,pe" options will be shown with the select all option
   *   b.  Everything will be selected by default and the user CANNOT unselect anything unless they own ALL of PI, PB, PA, PC, PE.
   *
   */
  private _getIndividualDatabases(defaultSelectedDatabases?: string[]): IDBSelection[]{
    let databases: IDBSelection[] = [];
    let availableDatabase = this._allDB.getDatabaseList();
    let individualDbs = _.map(this._getDatabasesBySubscription(defaultSelectedDatabases), 'code');
    let mergedIndividualPlusDefault = _.union(individualDbs, this._permissionService.getDefaultIndividualDBs());
    let mayAccessAllRegularProducts = this._permissionService.mayAccessAllDefaultProducts();

    for (let i = 0; i < availableDatabase.length; i++) {
      if (mergedIndividualPlusDefault.indexOf(availableDatabase[i].code) > -1 ) {
        let db = availableDatabase[i];

        // determine if there are databases that should be selected by default, such as when editing a search
        if(defaultSelectedDatabases && defaultSelectedDatabases.length){
          db.selected = defaultSelectedDatabases.indexOf(db.code) > -1;
        }else{
          db.selected = true;
        }

        db.disabled = !mayAccessAllRegularProducts;

        databases.push(db);
      }
    }

    return databases;
  }

  /*
   * 1. For organization users OR if they have the CPAJournals entitlement OR may access all "pi,pa,pb,pc,pe"
   *   a. Display only databases that the user has access to
   *   b. User will be able to select and deselect any option
   *   c. Show 'select all' option only if more than one database option
   */
  private _getDatabasesBySubscription(defaultSelectedDatabases?: string[]): IDBSelection[]{
    let databases = _.filter(this._allDB.getDatabaseList() , db=>{

      // determine if there are databases that should be selected by default, such as when editing a search
      if(defaultSelectedDatabases && defaultSelectedDatabases.length){
        db.selected = defaultSelectedDatabases.indexOf(db.code) > -1;
      }else{
        db.selected = true;
      }

      // If the user has a handbook or book collection entitlement, but does not have PB, then display 'APA Books' as the label in place of 'PsycBOOKS'.
      if(db.code === "PA" && this._isPsycArticlesNameChangeRequired()) {
        db.name = "APA Journals";
        return true;
      }

      //If the user has a print subscription but does not have PA, then display 'APA Journals' as the label in place of 'PsycARTICLES'.
      if(db.code === "PB" && this._isPsycBookNameChangeRequired()){
        db.name = "APA Books";
        return true;
      }

      return this._permissionService.mayAccess(db.code);
    });

    return databases;
  }

  private _isPsycArticlesNameChangeRequired(){
    return !this._permissionService.mayAccess('PA') && this._permissionService.hasGroupAccess('printSubscription');
  }

  private _isPsycBookNameChangeRequired(){
    return !this._permissionService.mayAccess('PB') && (this._permissionService.hasGroupAccess('handbook') || this._permissionService.hasGroupAccess('bookCollection'));
  }

}
