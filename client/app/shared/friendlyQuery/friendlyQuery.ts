import { Component, Input } from '@angular/core';

@Component({
  selector: 'friendly-query',
  templateUrl: './friendlyQuery.html',
  styleUrls: ['./friendlyQuery.less']
})

export class FriendlyQuery {
  @Input() text: string = '';
}
