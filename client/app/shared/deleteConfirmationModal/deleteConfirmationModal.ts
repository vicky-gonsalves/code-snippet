import {Component, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap';

@Component({
  selector : 'delete-confirmation-modal',
  templateUrl: './deleteConfirmationModal.html',
  styleUrls: ['./deleteConfirmationModal.less']
})

export class DeleteConfirmationModal{
  @ViewChild('deleteConfirmationModal') public deleteConfirmationModal : ModalDirective;
  @Input() userMessage: string ;
  @Input() isMyList: boolean;
  @Output() onModalHide = new EventEmitter();

  deleteData: any;

  openModal(deleteObj):void {
    this.deleteData = deleteObj;
    this.deleteConfirmationModal.show();
  }

  closeModal():void {
    this.deleteConfirmationModal.hide();
  }

  isOpen(): boolean{
    return this.deleteConfirmationModal.isShown;
  }

  confirmDelete(){
    this.onModalHide.emit({deleteData:this.deleteData, isDelete: true});
    this.closeModal();
  }

}
