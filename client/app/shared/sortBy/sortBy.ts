import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ISortDescription} from '../../search/services/search/query/components/interfaces';

import * as _ from "lodash";

@Component({
  selector: 'sort-by',
  templateUrl: './sortBy.html',
  styleUrls: ['./sortBy.less']
})

export class SortBy {
  @Input() parentId : string;
  @Input() currentSortOption : ISortDescription[];
  @Input() isCitedRef : boolean;
  @Input() isMyList : boolean;
  @Output() changeSortBy = new EventEmitter();
  terms = [{name: 'Year', value: 'y'}, {name: 'Author', value: 'a'}, {name: 'Title', value: 't'}, {name: 'Relevance', value: 'r'}, {name: 'Times Cited', value: 'timesDes'}];
  model = {};

  sortFieldMap = {
    'PublicationYearMSSort':'y',
    'AuthorSort':'a',
    'TitleSort':'t',
    'score':'r',
    'CitedByCountSort1':'timesDes'
  };

  sortOptionMap = {
    y : [{code: 'y', direction: '-1'},{code: 'a', direction: '1'}],
    a : [{code: 'a', direction: '1'},{code: 'y', direction: '-1'}],
    t : [{code: 't', direction: '1'},{code: 'y', direction: '-1'}],
    r : [{code: 'r', direction: '-1'},{code: 'y', direction: '-1'}],
    timesDes : [{code: 'timesDes', direction: '-1'},{code: 'y', direction: '-1'}]
  };

  citedSortTerms = [{name: 'Times Cited', value: 'CiDes'},{name: 'Year Ascending', value: 'PYAs'},{name: 'Year Descending', value: 'PYDes'}];


  citedSortFieldMap = {
    'CitedInCountSort1':'CiDes',
    'PublicationYearMSSort': 'PYAs',
    'PublicationYearMSSort1':'PYDes'
  };

  citedSortOptionMap = {
    CiDes : [{code: 'CiDes', direction: '-1'}],
    PYAs : [{code: 'PYAs', direction:'-1'}], // CitedRefs Year
    PYDes : [{code: 'PYDes', direction:'-1'}]
  };
  mylistSortTerms = [{name: 'Year', value: 'y'}, {name: 'Author', value: 'a'}, {name: 'Title', value: 't'},  {name: 'Times Cited', value: 'timesDes'}, {name: 'Order Added', value: 'o'}];
  mylistSortFieldMap = {
    'PublicationYearMSSort':'y',
    'Seq':'o',
    'AuthorSort':'a',
    'TitleSort':'t',
    'CitedByCountSort1':'timesDes'
  };

  mylistSortOptionMap = {
    o : [{code: 'o', direction: '1'}],
    y : [{code: 'y', direction: '-1'},{code: 'a', direction: '1'}],
    a : [{code: 'a', direction: '1'},{code: 'y', direction: '-1'}],
    t : [{code: 't', direction: '1'},{code: 'y', direction: '-1'}],
    timesDes : [{code: 'timesDes', direction: '-1'},{code: 'y', direction: '-1'}]
  };
  constructor(){
  }

  ngOnInit(){
    if(this.currentSortOption && this.currentSortOption.length){
      this._updateCurrentSortModel();
    }else{
      if(this.isCitedRef){
        this.model = this.citedSortTerms[0].value;
      }
      else if(this.isMyList){
        this.model = this.mylistSortTerms[0].value;
      }
      else{
        this.model =  this.terms[0].value;
      }

    }
  }

  ngOnChanges(changes){
    if(changes.currentSortOption.currentValue && changes.currentSortOption.currentValue.length){
      this._updateCurrentSortModel();
    }
  }

  private _updateCurrentSortModel(){
    var temp;
    if(this.isCitedRef){
      let code = this.citedSortFieldMap[this.currentSortOption[0].field]
      temp = _.find(this.citedSortTerms, ['value', code]);
    }
    else if(this.isMyList){
      let code = this.mylistSortFieldMap[this.currentSortOption[0].field]
      temp = _.find(this.mylistSortTerms, ['value', code]);
    }
    else{
      let code = this.sortFieldMap[this.currentSortOption[0].field];
      temp = _.find(this.terms, ['value', code]);
    }
    this.model = temp.value;
  }

  onLimitChange(newValue) {
    if(this.isCitedRef){
      var selectedSort =   this.citedSortOptionMap[newValue];
    }
    else if(this.isMyList){
      var selectedSort =   this.mylistSortOptionMap[newValue];
    }
    else{
      var selectedSort =  this.sortOptionMap[newValue];
    }

    this.changeSortBy.emit(selectedSort);
  }

  ngOnDestroy() {
  }
}
