import {Component, ElementRef, Input} from '@angular/core';

@Component({
  selector: 'permalink-input',
  templateUrl: './permalinkInput.html',
  styleUrls: ['./permalinkInput.less']
})

export class PermalinkInput {
  @Input() permalink: string = '';

  constructor(public element: ElementRef) {
  }

  ngOnInit() {
    setTimeout(()=> {
      this.selectText();
    }, 10);
  }

  selectText() {
    if (this.permalink && this.permalink.length) {
      let el = this.element.nativeElement.querySelector('input');
      el.select();
      el.setSelectionRange(0, this.permalink.length);
    }
  }
}
