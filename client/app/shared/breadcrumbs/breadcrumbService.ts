import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import {Observer} from 'rxjs/Observer';

export class BreadcrumbService {
  private _breadCrumb: Array<any>;
  private _currentPage: any;

  breadCrumbChange$: Observable<any>;
  currentPageChange$: Observable<any>;

  private _breadCrumbChangeObserver: Observer<any>;
  private _currentPageChangeObserver: Observer<any>;

  constructor() {
    this.breadCrumbChange$ = new Observable(observer => this._breadCrumbChangeObserver = observer).share();
    this.currentPageChange$ = new Observable(observer => this._currentPageChangeObserver = observer).share();
  }

  changeBreadCrumb(data) {
    this._breadCrumb = data;
    this._breadCrumbChangeObserver.next(data);
  }

  changeCurrentPage(data) {
    this._currentPage = data;
    this._currentPageChangeObserver.next(data);
  }
}
