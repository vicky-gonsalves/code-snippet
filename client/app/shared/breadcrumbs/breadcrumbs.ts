import {Component, Input} from '@angular/core';
import { Locale, LocaleService, LocalizationService } from 'angular2localization';

@Component({
  selector: 'breadcrumbs',
  templateUrl: './breadcrumbs.html',
  styleUrls: ['./breadcrumbs.less']
})
export class BreadCrumbs extends Locale {
  @Input() breadcrumblist: Array<any>;
  @Input() page: any;

  constructor(public locale: LocaleService, public localization: LocalizationService) {
    super(locale, localization);
  }
}
