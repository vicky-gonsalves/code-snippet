import {Component, ViewChild, Input} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap';

@Component({
  selector : 'add-to-list-warning-modal',
  templateUrl: 'addToListWarningModal.html',
  styleUrls: ['addToListWarningModal.less']
})
export class AddToListWarningModal{
  @Input() count : number;
  @ViewChild('addToListWarningModal') public addToListWarningModal: ModalDirective;

  openModal():void {
    this.addToListWarningModal.show();
  }

  closeModal():void {
    this.addToListWarningModal.hide();
  }

  isOpen(): boolean{
    return this.addToListWarningModal.isShown;
  }
}
