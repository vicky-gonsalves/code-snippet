import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'toggle-all-abstract',
  templateUrl: './toggleAllAbstract.html',
  styleUrls: ['./toggleAllAbstract.less']
})
export class ToggleAllAbstract {
  @Input() isAllAbsOpen: boolean;
  @Output() displayAbstractsChanged = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  toggleAllAbstracts() {
    this.isAllAbsOpen = !this.isAllAbsOpen;
    this.displayAbstractsChanged.emit(this.isAllAbsOpen);
  }

  ngOnChanges(changes) {
    if (changes.hasOwnProperty('isAllAbsOpen') && changes.isAllAbsOpen.previousValue && changes.isAllAbsOpen.currentValue) {
      this.isAllAbsOpen = changes.isAllAbsOpen.currentValue;
    }
  }
}
