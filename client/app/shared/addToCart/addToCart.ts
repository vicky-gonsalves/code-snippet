import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AdditionState } from './state';

import { IdentityService } from '../../core/services/identity/identityService';
import { Identity } from '../../core/services/identity/identity';

@Component({
  selector: 'add-to-cart',
  templateUrl: './addToCart.html',
  styleUrls: ['./addToCart.less']
})
export class AddToCart {

  /*
    The (item) value, for now, is either:
    - A UID,
    - A DB Code, or
    - The phrase "combo", for the day pass DB collection (3 in 1).
  */
  @Input() item: any;
  @Input() price: number;
  @Input() isAdded: boolean;
  @Input() state: AdditionState;
  @Input() doiLandingPage: boolean;

  @Output() addItemToCart = new EventEmitter();

  private identity: Identity;

  constructor() {}

  ngOnInit() {
    console.info('Add to cart:', this.item, 'at the price of', this.price);

    IdentityService.identity.subscribe((identity: Identity) => {
      this.identity = identity || new Identity();
    });
  }

  addToCart() {
    this.addItemToCart.emit(true);
  }

  isIdle() {
    return this.state === AdditionState.Idle;
  }
  isWorking() {
    return this.state === AdditionState.Working;
  }
  isFinished() {
    return this.state === AdditionState.Success;
  }
}
