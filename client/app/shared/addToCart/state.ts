export const enum AdditionState {
  Idle,
  Working,
  Success
}
