/*
import {it, inject, beforeEachProviders } from '@angular/core/testing';
import {provide} from '@angular/core';
import {RouteRegistry, ROUTER_PRIMARY_COMPONENT} from '@angular/router-deprecated';
import {SpyLocation} from '@angular/common/testing';
import {Location} from '@angular/common';

// Load the implementations that should be tested
import {PaginationLimit} from './paginationLimit';

describe('PaginationLimit', () => {
  var paginationLimit;

  // provide our implementations or mocks to the dependency injector
  beforeEachProviders(() => [
    PaginationLimit,
    RouteRegistry,
    provide(Location, {useClass: SpyLocation}),
    provide(ROUTER_PRIMARY_COMPONENT, {useValue: PaginationLimit})
  ]);

  beforeEach(inject([PaginationLimit], _paginationLimit => {
    paginationLimit = _paginationLimit;
  }));


  it('should change pagination limit', done => {
    paginationLimit.changePagination.subscribe(limit => {
      expect(limit).toEqual(50);
      done();
    });
    paginationLimit.onLimitChange(50);
  });


});
*/
