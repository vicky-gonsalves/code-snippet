import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'paginationlimit',
  templateUrl: './paginationLimit.html',
  styleUrls: ['./paginationLimit.less']
})
export class PaginationLimit {
  @Input() parentId : string;
  @Input() selectedLimit: string;
  @Input() displayLimits: Array<string>; //User defined limitOptions
  @Output() changePagination = new EventEmitter();

  limitOptions = ['25', '50'];

  constructor() {

  }

  ngOnInit() {
    console.log("this.parentId: ", this.parentId);
    if (this.displayLimits && this.displayLimits.length > 0) {
      this.limitOptions = this.displayLimits.slice();
    }

    // If the user did not input anything, then make the first limitOption the default value
    if (!this.selectedLimit) {
      this.selectedLimit = this.limitOptions[0];
    }
  }

  onLimitChange(newValue) {
    this.changePagination.emit(newValue);
  }

}
