import { Component, Input } from '@angular/core';

@Component({
  selector: 'social',
  templateUrl: './social.html',
  styleUrls: ['./social.less']
})
export class Social {
  @Input() urlInfo;
  @Input() title;
  encodedUrl: string;

  constructor() { }

  ngOnInit() {
    this.encodedUrl = encodeURIComponent(this.urlInfo);
  }

  openDialog(type, url) {
    let fullUrl;
    let height = 500;
    let width = 600;
    let left = (screen.width - width) / 2;
    let top = (screen.height - height) / 2;
    let windowSettings = 'width=' + width + ', height=' + height + ', top=' + top + ', left=' + left;
    windowSettings += ', directories=no' + ', location=no' + ', menubar=no' + ', resizable=yes' + ', scrollbars=yes' + ', status=no' + ', toolbar=no';

    if(type === 'Twitter') {
      let desc = '';
      desc = this.title ? encodeURIComponent(this.title) : '';
      fullUrl = url + desc + '&amp;url=' + this.encodedUrl;
    }
    else {
      fullUrl = url + this.encodedUrl;
    }

    window.open(fullUrl, 'targetWindow', windowSettings);
    return false;
  }

}
