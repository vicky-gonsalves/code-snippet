import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { UID } from './uid';

@Injectable()
export class UIDGuardService implements CanActivate {
  constructor(private _router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let result = true;

    // The relevant routes' params are usually named (uid), except for the full-text route, which is (uid.html)
    let uid = route.params['uid'] || route.params['uid.html'] || '';
    uid = uid.replace('.html', '');

    if (!UID.isWellFormed(uid)) {
      this._router.navigate(['/']);
      result = false;
    }
    return result;
  }
}
