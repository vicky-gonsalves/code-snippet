export class UID {
  private static formsPatterns = [
    /^\d{4}-\d{5}-\d{3}$/,  // General format for PI, PA, PB, PC and PT records.
    /^\d{9}-\d{3}$/,        // Special format for PE records.
    /^\d{4}-\d{8}$/,        // Special format, given within the requirements.
  ];

  // Check the input against all possible patterns.
  static isWellFormed(value: string): boolean {
    let isMatched = false;
    let index = 0;
    while (!isMatched && index < UID.formsPatterns.length) {
      let match = value.match(UID.formsPatterns[index]) || [];
      isMatched = match.length > 0;
      index++;
    }
    return isMatched;
  }
}
