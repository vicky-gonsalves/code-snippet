import {Injectable} from '@angular/core';

@Injectable()

export class RelatedUIDFormatter  {
  constructor() {
  }

  /*this will find all occurrences of the <RelatedUID> and replace it with the link to corresponding record display*/
  static linkRelatedUIDs (text:string):string {
    let RelatedUIDs = text.match(/<RelatedUID?([^\/> ]+)\s*([^>]*)\s*(?:\/>|>(.*?)<\/RelatedUID>)/g);
    if(RelatedUIDs && RelatedUIDs.length) {
      for (let x = 0; x < RelatedUIDs.length; x++) {
        let UID = RelatedUIDs[x].replace(/<\/?[^>]+(>|$)/g, "");
        text = text.replace(RelatedUIDs[x], '<a href="record/' + UID + '" >' + UID + '</a>');
      }
    }
      return text;
  }
}
