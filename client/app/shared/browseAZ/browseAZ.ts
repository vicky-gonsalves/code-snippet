import {Component,Input,Output,EventEmitter} from '@angular/core';

@Component({
  selector: 'browseaz',
  templateUrl: './browseAZ.html',
  styleUrls: ['./browseAZ.less']
})
export class BrowseAZ {

  @Input() selectAlpha:string;
  @Output() changeValue = new EventEmitter();

  alphabetList : Array<string> = ['A' ,'B' ,'C' ,'D' ,'E' ,'F' ,'G' ,'H' ,'I' ,'J' ,'K' ,'L' ,'M' ,'N' ,'O' ,'P' ,'Q' ,'R' ,'S' ,'T' ,'U' ,'V' ,'W' ,'X' ,'Y' ,'Z'];

  constructor() {}

  ngOnInit() {

      //console.log("Alpha=" + this.selectAlpha);
      if (this.selectAlpha === undefined || this.selectAlpha=='*') {
        //this.selectAlpha = 'A';
        this.selectAlpha = '';
      }
  }

  isActive(letter:string) {
      return letter == this.selectAlpha.toUpperCase();
  }

  /*
  * This method will pass the value to the component which is using the browseaz component
  * */
  isClicked(letter:string) {
      if (letter != this.selectAlpha) {
          this.selectAlpha = letter;
          this.changeValue.emit(letter);
      }
  }
}
