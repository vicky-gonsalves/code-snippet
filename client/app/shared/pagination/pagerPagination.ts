import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Pagination} from './pagination';

@Component({
  selector : 'pagerPagination',
  templateUrl: './pagerPagination.html',
  styleUrls: ['./pagination.less']
})
export class PagerPagination extends Pagination{

    @Input() totalItems:number;
    @Input() selectedPage:number;
    @Input() selectedLimit:number;

    @Output() changePagination = new EventEmitter();

    pager:any = {};

    constructor() {
      super();
    }

    ngOnInit() {
        this.setPage(this.selectedPage);
    }

    ngOnChanges(changes) {
        if (this.totalItems !== undefined && this.selectedPage !== undefined) {
            this.setPage(this.selectedPage);
        }
    }

    setPage(page, ev?:string) {
        var result = super.setPage( page, this.totalItems, this.selectedLimit);
        if(result){
          this.pager = result;
          this.changePagination.emit({currentPage: this.pager.currentPage, ev: ev});
        }
    }


    isNextDisabled () : boolean{
        return this.pager && this.pager.currentPage && this.pager.totalPages && this.pager.currentPage === this.pager.totalPages;
    }

    isPreviousDisabled () : boolean{
        return this.pager && this.pager.currentPage  && this.pager.currentPage === 1;
    }

}
