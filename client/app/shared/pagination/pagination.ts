
export class Pagination {

    constructor() {
    }

    /**
     * Creates a range of numbers in an array, starting at a specified number and
     * ending before a different specified number.
     * @param {number} start  Indicates what number should be used as the first
     *     number in the returned array.  If this is the only number argument
     *     supplied, this will be used as the edge and 0 will be used as the start.
     * @param {number=} edge  Indicates the first number that should not appear in
     *     the range of numbers.  If this number preceeds the start in the range
     *     (taking into account the step), an empty array will be returned.  If not
     *     specified and not inferred this defaults to 0.
     * @param {number=} step  Indicates the difference between one number and the
     *     subsequent number placed in the returned array.  If not specified this
     *     defaults to 1.
     * @return {!Array.<number>}  Array of numbers in the specified range.
     */
    range(start:number, edge?:number, step?:number) {
      // If only one number was passed in make it the edge and 0 the start.
        if (arguments.length == 1) {
          edge = start;
          start = 0;
        }

        // Validate the edge and step numbers.
        edge = edge || 0;
        step = step || 1;

        // Create the array of numbers, stopping before the edge.
        let ret = [];
        for (ret = []; (edge - start) * step > 0; start += step) {
          ret.push(start);
        }
        return ret;
    }

    getPager(totalItems?:number, currentPage = 1, pageSize = 10) {

        // calculate total pages
        let totalPages = Math.ceil(totalItems / pageSize);

        let startPage, endPage;
        if (totalPages <= 5) {
            // less than 5 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
          // more than 5 total pages so calculate start and end pages
            if (currentPage <= 3) {
              startPage = 1;
              endPage = 5;
            } else if (currentPage + 2 >= totalPages) {
              startPage = totalPages - 4;
              endPage = totalPages;
            } else {
              startPage = currentPage - 2;
              endPage = currentPage + 2;
            }
        }

        // calculate start and end item indexes
        let startIndex = (currentPage - 1) * pageSize;
        let endIndex = startIndex + pageSize;

        // create an array of pages to ng-repeat in the pager control
        let pages = this.range(startPage, endPage + 1);

        // return object with all pager properties required by the view

        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }

    setPage(page, totalItems, selectedLimit, ev?:string) : any{

        let totalPages = Math.ceil(totalItems / selectedLimit);
        if (page < 1 || page > totalPages) {
            return false;
        }

        // get pager object from service
        return this.getPager(totalItems, page, selectedLimit);
    }

}
