import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'arrow-pagination',
  templateUrl: './arrowPagination.html',
  styleUrls: ['./pagination.less']
})
export class ArrowPagination {
  @Input() totalPages: number;
  @Input() currentPage: number;

  @Output() changePagination = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  setPrevPage(ev?: string) {
    if (!this.isPreviousDisabled()) {
      this.changePagination.emit({currentPage: this.currentPage - 1, ev: ev});
    }
  }

  setNextPage(ev?: string) {
    if (!this.isNextDisabled()) {
      this.changePagination.emit({currentPage: this.currentPage + 1, ev: ev});
    }
  }

  isNextDisabled(): boolean {
    return this.currentPage >= 1 && this.totalPages && this.currentPage === this.totalPages;
  }

  isPreviousDisabled(): boolean {
    return this.currentPage <= 1;
  }
}

