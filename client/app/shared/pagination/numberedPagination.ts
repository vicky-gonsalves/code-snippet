import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Pagination} from './pagination';

@Component({
  selector : 'numberedPagination',
  templateUrl: './numberedPagination.html',
  styleUrls: ['./pagination.less']
})
export class NumberedPagination extends Pagination{

  @Input() totalItems:number;
  @Input() selectedPage:number;
  @Input() selectedLimit:number;

  @Output() changePagination = new EventEmitter();

  pager:any = {};

  constructor() {
    super();
  }

  ngOnInit() {
    this.setPage(this.selectedPage);
  }

  ngOnChanges(changes) {
    if (this.totalItems !== undefined && this.selectedPage !== undefined) {
      this.setPage(this.selectedPage);
    }
  }

  setPage(page, ev?:string) {
    var result = super.setPage( page, this.totalItems, this.selectedLimit);
    if(result){
      this.pager = result;
      this.changePagination.emit({currentPage: this.pager.currentPage, ev: ev});
    }
  }
}

