import {Injectable} from '@angular/core';

@Injectable()
export class DOIExtractor {
  public static extract(text: string) {
    let index = text.indexOf('doi:');
    let extracted = {text: null, doi: null};
    if(index > -1) {
      extracted = {text: text.substring(0, index), doi: text.substring(index + 4)};
    } else {
      extracted = {text: text, doi: null};
    }
    return extracted;
  }
}
