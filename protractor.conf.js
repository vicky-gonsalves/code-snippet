// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

/*global jasmine */
var SpecReporter = require('jasmine-spec-reporter');

exports.config = {
  allScriptsTimeout: 11000,

  // You can run an individual suite by executing the following command:  npm run e2e -- --suite browsePA
  suites: {
    browseAll: [
      './e2e/browse/browsePA/**/*.e2e-spec.ts',
      './e2e/browse/browsePB/**/*.e2e-spec.ts',
      './e2e/browse/browsePC/**/*.e2e-spec.ts',
      './e2e/browse/browsePE/**/*.e2e-spec.ts',
      './e2e/browse/browsePT/**/*.e2e-spec.ts',
      './e2e/browse/browsePTH/**/*.e2e-spec.ts'
    ],
    browsePA: './e2e/browse/browsePA/**/*.e2e-spec.ts',
    browsePB: './e2e/browse/browsePB/**/*.e2e-spec.ts',
    browsePC: './e2e/browse/browsePC/**/*.e2e-spec.ts',
    browsePE: './e2e/browse/browsePE/**/*.e2e-spec.ts',
    browsePT: './e2e/browse/browsePT/**/*.e2e-spec.ts',
    browsePTH: './e2e/browse/browsePTH/**/*.e2e-spec.ts',

    fulltext: './e2e/fulltext/**/*.e2e-spec.ts',
    fulltextPA: './e2e/fulltext/fulltextPA/*.e2e-spec.ts',
    fulltextPC: './e2e/fulltext/fulltextPC/*.e2e-spec.ts',
    buy: './e2e/buy/**/*.e2e-spec.ts',

    fulltext: './e2e/fulltext/**/*.e2e-spec.ts',
    fulltextPA: './e2e/fulltext/fulltextPA/*.e2e-spec.ts',
    fulltextPC: './e2e/fulltext/fulltextPC/*.e2e-spec.ts',

    // will split this up when there are more tests for search
    search: './e2e/search/**/*.e2e-spec.ts',
    basicSearch : './e2e/search/forms/basic/**/*.e2e-spec.ts',
    advancedSearch : './e2e/search/forms/advanced/**/*.e2e-spec.ts',

    citedRefsForm: './e2e/search/forms/citedRefs/*.e2e-spec.ts',
    citedRefsResults: './e2e/search/results/citedRefsResults/*.e2e-spec.ts',
    citedBy: './e2e/search/citedBy/*.e2e-spec.ts',

    recordDisplay: './e2e/recordDisplay/*.e2e-spec.ts',
    ptRecordDisplay: './e2e/recordDisplay/pt/*.e2e-spec.ts',

    myList: './e2e/search/myList/*.e2e-spec.ts',

    doiLanding: './e2e/doiLanding/**/*.e2e-spec.ts',

    localization: './e2e/localization/*.e2e-spec.ts',

    recentSearches: './e2e/recentSearches/*.e2e-spec.ts',
    thesaurus: './e2e/thesaurus/*.e2e-spec.ts',
    myPsycNET: './e2e/myPsycNET/**/*.e2e-spec.ts'
  },
  capabilities: {
    'browserName': 'chrome'
  },
  directConnect: true,
  baseUrl: 'http://localhost:9000/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  useAllAngular2AppRoots: true,
  beforeLaunch: function() {
    require('ts-node').register({
      project: 'e2e'
    });
  },
  onPrepare: function() {
    jasmine.getEnv().addReporter(new SpecReporter());

    // required to detect elements on the page or else everything timesout.  This is a bug with protractor when setTimeout and setInterval is used within an app (we use them for session dialogs)
    browser.ignoreSynchronization =true;
  }
};
